﻿#nullable enable

using DataSources;
using Utils;

namespace TestDataSource;

/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
	public const string ACCOUNT_ID = "phoenixtest",
						USERNAME   = "admin",
						PASSWORD   = "enarc";

	public MainWindow()
	{
		InitializeComponent();

		Tasks.RunVoid( async () =>
					   {
						   Azure.Slot = Azure.SLOT.ALPHA_TRW;
						   var Result = await Azure.LogIn( "DataSource Tester", ACCOUNT_ID, USERNAME, PASSWORD );

						   if( Result.Status != Azure.AZURE_CONNECTION_STATUS.OK )
							   throw new Exception( "Cannot login to PhoenixTest" );

						   Dispatcher.Invoke( () =>
										      {
											      var CompanyDataSource = new CompanyDataSource();
											      CompanyComboBox.ItemsSource = CompanyDataSource;
/*
											      foreach( var Co in CompanyDataSource )
												      Console.WriteLine( Co );
*/
										      } );
					   } );
	}
}