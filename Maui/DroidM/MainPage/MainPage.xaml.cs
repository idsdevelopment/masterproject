using ViewModelsBase;

namespace DroidM;

public partial class MainPage : ContentPage
{
	public static           MainPage            Instance { get; private set; } = null!;
	private static readonly LinkedList<View>    ViewStack           = [];
	private static readonly WaitingForShipments WaitingForShipments = new();
	private static          MainPageViewModel   Model               = null!;

	public MainPage()
	{
		Instance                                 = this;
		TripManagement.Globals.GetStringResource = DroidM.Resources.Get.StringResource;
		TripManagement.Globals.GetColorResource  = DroidM.Resources.Get.ColorResource;

		InitializeComponent();
		var M = Model = (MainPageViewModel)Root.BindingContext;

		TripManagement.Globals.OnWaitingForTrips = waiting =>
		                                           {
			                                           ( (AViewModelBase)M ).Dispatcher?.Invoke( () =>
			                                                                                     {
				                                                                                     if( waiting )
					                                                                                     PushView( WaitingForShipments );
				                                                                                     else
					                                                                                     PopView();
			                                                                                     } );
		                                           };
	}

	public static void Run( ContentView view )
	{
		( (AViewModelBase)Model ).Dispatcher?.Invoke( () =>
		                                              {
			                                              Instance.LoadArea.Content = view;
		                                              } );
	}

	public static bool Run( Type view )
	{
		if( view.IsSubclassOf( typeof( ContentView ) ) )
		{
			var ViewInstance = (ContentView)Activator.CreateInstance( view )!;
			Run( ViewInstance );
			return true;
		}
		Error( "Invalid View Type" );
		return false;
	}

	public static void Run( string viewName, bool saveAsLastProgram = false )
	{
		var ViewType = Type.GetType( viewName );

		if( ViewType is not null )
		{
			if( Run( ViewType ) && saveAsLastProgram )
				MainPageViewModel.Instance.CurrentPage = viewName;
		}
		else
			Error( "Invalid View Name" );
	}

	public static void RunLastProgram( string defaultView )
	{
		var CurrentPage = MainPageViewModel.Instance.CurrentPage;
		Run( CurrentPage.IsNotNullOrWhiteSpace() ? CurrentPage : defaultView, true );
	}

	public static void PushView( ContentView newView )
	{
		var CurrentView = Instance.LoadArea.Content;

		if( CurrentView is not null )
		{
			ViewStack.AddLast( CurrentView );
			Run( newView );
		}
	}

	public static void PopView()
	{
		if( ViewStack.Count > 0 )
		{
			var LastView = ViewStack.Last?.Value;

			if( LastView is ContentView View )
			{
				ViewStack.RemoveLast();
				Run( View );
			}
			else
				Error( "Invalid View Type" );
		}
		else
			Error( "No View to Pop" );
	}

	public static void Error( string message ) => Instance.DisplayAlert( "Error", message, "Ok" );
}