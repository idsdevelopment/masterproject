﻿namespace DroidM.Views;

internal class MainPageViewModel : ViewModelBase
{
	public static MainPageViewModel Instance { get; private set; } = null!;

#region Page Management
	[Setting]
	public string CurrentPage
	{
		get { return Get( () => CurrentPage, "" ); }
		set { Set( () => CurrentPage, value ); }
	}

	[DependsUpon350( nameof( CurrentPage ) )]
	public void WhenCurrentPageChanges()
	{
		SaveSettings();
	}
#endregion

#region OnLine
	private const string ONLINE_IMAGE  = "online32x32.png",
	                     OFFLINE_IMAGE = "offline32x32.png";

	public MainPageViewModel()
	{
		Instance = this;
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		TripManagement.Globals.OnPing.Add( ( sender, online ) =>
		                    {
			                    OnLine = online;
		                    } );
	}

	public bool OnLine
	{
		get { return Get( () => OnLine, true ); }
		set { Set( () => OnLine, value ); }
	}

	[DependsUpon( nameof( OnLine ) )]
	public void WhenOnlineChanges()
	{
		OnlineImage = OnLine ? ONLINE_IMAGE : OFFLINE_IMAGE;
	}

	public string OnlineImage
	{
		get { return Get( () => OnlineImage, ONLINE_IMAGE ); }
		set { Set( () => OnlineImage, value ); }
	}
#endregion
}