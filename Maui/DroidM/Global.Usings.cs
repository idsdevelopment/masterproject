﻿global using TripManagement;
global using DroidM.Views;
global using Utils;

// Types
global using OBSERVABLE_TRIP_DICTIONARY = Utils.ObservableDictionary<string, TripManagement.TripDisplayEntry>;
global using ViewModelBase = MauiViewModel.ViewModelBase;
global using TRIP_COLLECTION = System.Collections.ObjectModel.ObservableCollection<TripManagement.TripDisplayEntry>;