﻿using System.Collections;
using System.Collections.Specialized;

namespace DroidM.BaseViewModels;

internal abstract class BaseTripViewModel : ViewModelBase
{
	public OBSERVABLE_TRIP_DICTIONARY TripDisplayEntries { get; } = TripManagement.Globals.TripDisplayEntries;

	protected virtual void OnTripsAdd( IList newItems, int newStartingIndex, IList? oldItems, int oldStartingIndex )
	{
	}

	protected virtual void OnTripsReplace( IList newItems, int newStartingIndex, IList? oldItems, int oldStartingIndex )
	{
	}

	protected virtual void OnTripsMove( IList newItems, int newStartingIndex, IList? oldItems, int oldStartingIndex )
	{
	}

	protected virtual void OnTripsRemove( IList? oldItems, int oldStartingIndex )
	{
	}

	protected virtual void OnTripsClear()
	{
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		TripManagement.Globals.TripDisplayEntriesChanged.Add( ( _, args ) =>
		                                                      {
			                                                      switch( args.Action )
			                                                      {
			                                                      case NotifyCollectionChangedAction.Add when args.NewItems is not null:
				                                                      OnTripsAdd( args.NewItems, args.NewStartingIndex, args.OldItems, args.OldStartingIndex );
				                                                      break;

			                                                      case NotifyCollectionChangedAction.Remove:
				                                                      OnTripsRemove( args.OldItems, args.OldStartingIndex );
				                                                      break;

			                                                      case NotifyCollectionChangedAction.Reset:
				                                                      OnTripsClear();
				                                                      break;

			                                                      case NotifyCollectionChangedAction.Replace when args.NewItems is not null:
				                                                      OnTripsReplace( args.NewItems, args.NewStartingIndex, args.OldItems, args.OldStartingIndex );
				                                                      break;

			                                                      case NotifyCollectionChangedAction.Move when args.NewItems is not null:
				                                                      OnTripsMove( args.NewItems, args.NewStartingIndex, args.OldItems, args.OldStartingIndex );
				                                                      break;
			                                                      }
		                                                      } );
	}
}