﻿namespace DroidM.Resources;

public class Get
{
	public static string StringResource( string key ) => GetResource<string>( key ) ?? "";

	public static Color ColorResource( string key ) => GetResource<Color>( key ) ?? Colors.Red;

	public static DataTemplate? DataTemplateResource( string key ) => GetResource<DataTemplate>( key );

	private static T? GetResource<T>( string name ) => Application.Current is { } Current ? (T)Current.Resources[ name ] ?? default : default;
}