﻿using Android.App;
using Android.Content.PM;

namespace DroidM
{
	[Activity( Label = "IDS Mobile",
	           Theme = "@style/Maui.SplashTheme",
	           MainLauncher = true,
	           AlwaysRetainTaskState = true,
	           LaunchMode = LaunchMode.SingleTop,
	           ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden
	         )]
	public class MainActivity : MauiAppCompatActivity
	{
	}
}