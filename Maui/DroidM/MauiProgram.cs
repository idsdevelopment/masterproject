﻿using Microsoft.Extensions.Logging;
using Microsoft.Maui.LifecycleEvents;

namespace DroidM
{
	public static class MauiProgram
	{
		public static MauiApp CreateMauiApp()
		{
			TripManagement.Listeners.Ping.Initialise(); // Force the static constructor to run.

			var Builder = MauiApp.CreateBuilder();

			Builder
				.UseMauiApp<App>()
				.ConfigureFonts( fonts =>
				                 {
					                 fonts.AddFont( "OpenSans-Regular.ttf", "OpenSansRegular" );
					                 fonts.AddFont( "OpenSans-Semibold.ttf", "OpenSansSemibold" );
				                 } )

				// Add this to configure lifecycle events
				.ConfigureLifecycleEvents( lifecycle =>
				                           {
				                           } );

		#if DEBUG
			Builder.Logging.AddDebug();
		#endif

			return Builder.Build();
		}
	}
}