﻿namespace DroidM;

internal class CurrentVersion
{
	private const string VERSION      = "1.00",
	                     PROGRAM_NAME = "IDS Droid(M)";

	// ReSharper disable once InconsistentNaming
	public static string PROGRAM_VERSION
	{
		get { return _PROGRAM_VERSION ??= $"{PROGRAM_NAME} Version: {VERSION}"; }
	}

	private static string? _PROGRAM_VERSION;
}