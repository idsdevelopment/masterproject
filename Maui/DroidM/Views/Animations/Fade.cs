﻿namespace DroidM.Views.Animations;

public static class Animations
{
	public static async Task FadeIn( this Layout parent, View showingElement, View hidingElement )
	{
		var Children = parent.Children;

		if( Children.Contains( showingElement ) )
		{
			// Remove the child and add it back, effectively bringing it to the front
			Children.Remove( showingElement );
			Children.Add( showingElement );
		}

		await Task.WhenAll(
		                   showingElement.FadeTo( 1, 500, Easing.CubicOut ),
		                   hidingElement.FadeTo( 0, 500, Easing.CubicIn )
		                  );
	}
}