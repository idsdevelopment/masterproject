﻿using System.Windows.Input;
using TripManagement.AlternateViews;

namespace DroidM.Views.PickupDeliveries.NoLocation;

internal class PickupDeliveriesViewModel : ViewModelBase
{
	public string PickupText   = "",
	              DeliveryText = "";


	private readonly bool AllowUnderScans = TripManagement.Globals.Preferences.AllowUnderScans,
	                      AllowOverScans  = TripManagement.Globals.Preferences.AllowOverScans;

#region Totals
	private decimal TotalScanned,
	                TotalRemaining,
	                TotalExpected;

	private void UpdateTotals()
	{
		TotalScanned  = 0;
		TotalExpected = 0;

		if( LocationItems is not null )
		{
			foreach( var Trip in LocationItems )
			{
				var T = (ScannedTripDisplayEntry)Trip;
				TotalScanned  += T.Scanned;
				TotalExpected += T.Pieces;
			}
		}

		TotalRemaining = TotalExpected - TotalScanned;

		DisplayTotalScanned   = TotalScanned.ToString( "N0" );
		DisplayTotalRemaining = TotalRemaining.ToString( "N0" );
		DisplayTotalExpected  = TotalExpected.ToString( "N0" );
	}
#endregion

#region Trips
	public SCANNED_TRIP_COLLECTION? LocationItems
	{
		get { return Get( () => LocationItems, (SCANNED_TRIP_COLLECTION?)null ); }
		set { Set( () => LocationItems, value ); }
	}

	[DependsUpon( nameof( LocationItems ) )]
	public void WhenLocationItemsChange()
	{
		AcceptButtonText = LocationItems is { Count: > 0 } Li && Li[ 0 ].IsDelivery ? DeliveryText : PickupText;

		if( LocationItems is not null )
		{
			UpdateTotals();
			Show?.Invoke( SHOW_STATE.LOCATION_TRIPS );
		}
	}
#endregion

#region Scanned
	public string DisplayTotalScanned
	{
		get { return Get( () => DisplayTotalScanned, "0" ); }
		set { Set( () => DisplayTotalScanned, value ); }
	}

	public string DisplayTotalRemaining
	{
		get { return Get( () => DisplayTotalRemaining, "0" ); }
		set { Set( () => DisplayTotalRemaining, value ); }
	}

	public string DisplayTotalExpected
	{
		get { return Get( () => DisplayTotalExpected, "0" ); }
		set { Set( () => DisplayTotalExpected, value ); }
	}
#endregion

#region View Visibility
	public enum SHOW_STATE
	{
		MAIN_VIEW,
		LOCATION_TRIPS
	}

	public Action<SHOW_STATE>? Show;
#endregion

#region Cancel
	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public void Execute_OnCancel()
	{
		Show?.Invoke( SHOW_STATE.MAIN_VIEW );
	}
#endregion

#region Accept
	public string AcceptButtonText
	{
		get { return Get( () => AcceptButtonText, "" ); }
		set { Set( () => AcceptButtonText, value ); }
	}

	public bool AcceptEnable
	{
		get { return Get( () => AcceptEnable, AllowUnderScans ); }
		set { Set( () => AcceptEnable, value ); }
	}

	public ICommand OnAccept => Commands[ nameof( OnAccept ) ];

	public bool CanExecute_OnAccept() => AcceptEnable;

	public void Execute_OnAccept()
	{
	}
#endregion
}