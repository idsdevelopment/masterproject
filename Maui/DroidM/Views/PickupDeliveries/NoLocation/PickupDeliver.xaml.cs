using DroidM.Resources;
using DroidM.Views.Animations;

namespace DroidM.Views.PickupDeliveries.NoLocation;

public class HeaderLabel : Label
{
	public HeaderLabel()
	{
		TextColor = Get.ColorResource( "Gray300" );
		FontSize  = 10;

		VerticalOptions       = LayoutOptions.Center;
		VerticalTextAlignment = TextAlignment.Center;

		Margin = new Thickness( 0, 0, 10, 0 );
	}
}

public class MinorHeaderLabel : HeaderLabel
{
	public MinorHeaderLabel()
	{
		TextColor = Get.ColorResource( "Gray100" );
	}
}

public partial class PickupDeliver : BaseView
{
	public PickupDeliver()
	{
		InitializeComponent();
		var Model = (PickupDeliveriesViewModel)Root.BindingContext;

		Model.Show = async state =>
		             {
			             switch( state )
			             {
			             case PickupDeliveriesViewModel.SHOW_STATE.MAIN_VIEW:
				             await ShowMainView();
				             break;

			             case PickupDeliveriesViewModel.SHOW_STATE.LOCATION_TRIPS:
				             await ShowLocationTrips();
				             break;
			             }
		             };

		Model.PickupText   = Get.StringResource( "AcceptPickup" );
		Model.DeliveryText = Get.StringResource( "AcceptDelivery" );
	}

	private Task ShowLocationTrips()
	{
		var L = LocationTripView;
		L.IsVisible = true;
		var M = MainTripView;
		M.IsVisible = false;

		return Root.FadeIn( L, M );
	}

	private Task ShowMainView()
	{
		var L = LocationTripView;
		L.IsVisible = false;
		var M = MainTripView;
		M.IsVisible = true;

		return Root.FadeIn( M, L );
	}
}