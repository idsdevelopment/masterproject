namespace DroidM.Views;

public partial class ScannerView : BaseView
{
	public new View? Content
	{
		get => ViewContent.Content;
		set => ViewContent.Content = value;
	}

	public ScannerView()
	{
		InitializeComponent();
	}
}