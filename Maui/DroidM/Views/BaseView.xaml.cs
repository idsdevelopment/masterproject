namespace DroidM.Views;

public partial class BaseView : ContentView
{
	public BaseView()
	{
		InitializeComponent();
	}

	public Task<bool> DisplayAlert( string? title,
	                                string message,
	                                string? accept,
	                                string cancel,
	                                FlowDirection flowDirection ) => MainPage.Instance.DisplayAlert( title, message, accept, cancel, flowDirection );

	public Task DisplayAlert( string title, string message, string cancel ) => DisplayAlert( title, message, null, cancel, FlowDirection.MatchParent );

	public static void RunLastProgram( string? defaultView )
	{
		if( defaultView is not null )
			MainPage.RunLastProgram( defaultView );
		else
			MainPage.Error( "Null View Name" );
	}

	public static void Run( ContentView view )
	{
		MainPage.Run( view );
	}

	public static void Run( string? view )
	{
		if( view is not null )
			MainPage.Run( view );
		else
			MainPage.Error( "Null View Name" );
	}
}