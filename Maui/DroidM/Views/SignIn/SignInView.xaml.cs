using DroidM.Resources;
using DroidM.Views.PickupDeliveries.NoLocation;

namespace DroidM.Views.SignIn;

public partial class SignInView : BaseView
{
	public SignInView()
	{
		InitializeComponent();
		var Model = (SignInViewModel)BindingContext;

		Model.OnSignInError = () => DisplayAlert( Get.StringResource( "SignInError" ),
		                                          Get.StringResource( "InvalidCredentials" ),
		                                          Get.StringResource( "Ok" ) );

		Model.OnSignInOk = ( carrierId, userName ) =>
		                   {
			                   Globals.CarrierId = carrierId;
			                   Globals.UserName  = userName;

			                   IsVisible = false; // Stop display flashing

			                   TripManagement.Globals.GetTrips( userName, () =>
			                                                              {
				                                                              Run( typeof( PickupDeliver ).FullName );
			                                                              } );
		                   };

		Loaded += ( _, _ ) =>
		          {
			          if( string.IsNullOrWhiteSpace( Account.Text ) )
				          Account.Focus();

			          else if( string.IsNullOrWhiteSpace( UserName.Text ) )
				          UserName.Focus();

			          else
				          Password.Focus();
		          };
	}
}