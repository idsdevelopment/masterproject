﻿using System.Windows.Input;
using AzureRemoteService;

namespace DroidM.Views.SignIn;

internal sealed class SignInViewModel : ViewModelBase
{
	[Setting]
	public string CarrierId
	{
		get { return Get( () => CarrierId, "" ); }
		set { Set( () => CarrierId, value ); }
	}


	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}


	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}

	private volatile bool OnLine;

	protected override void OnInitialised()
	{
		base.OnInitialised();

		TripManagement.Globals.OnPing.Add( ( _, online ) =>
		                                   {
			                                   if( online != OnLine )
			                                   {
				                                   OnLine = online;
				                                   WhenInputChanges();
			                                   }
		                                   } );
	}

#region Button
	public Action?                 OnSignInError;
	public Action<string, string>? OnSignInOk;

	private bool SigningIn;

	[DependsUpon250( nameof( CarrierId ) )]
	[DependsUpon250( nameof( UserName ) )]
	[DependsUpon250( nameof( Password ) )]
	public void WhenInputChanges()
	{
		Enabled = !SigningIn && OnLine && CarrierId.IsNotNullOrWhiteSpace() && UserName.IsNotNullOrWhiteSpace() && Password.IsNotNullOrWhiteSpace();
	}

	public bool Enabled
	{
		get { return Get( () => Enabled, false ); }
		set { Set( () => Enabled, value ); }
	}

	public ICommand Login => Commands[ nameof( Login ) ];

	public bool CanExecute_Login() => Enabled;

	public async void Execute_Login()
	{
		SigningIn = true;
		WhenInputChanges();

		try
		{
			SaveSettings();
			var Cid    = CarrierId;
			var UName  = UserName;
			var Pw     = Password;
			var Client = new Azure.InternalClient( CurrentVersion.PROGRAM_VERSION, Cid, UName );
			Azure.Client = Client;
			var Result = await Client.LogIn( Pw );

			if( Result == Azure.AZURE_CONNECTION_STATUS.OK )
				OnSignInOk?.Invoke( Cid, UName );
			else
				OnSignInError?.Invoke();
		}
		finally
		{
			SigningIn = false;
			WhenInputChanges();
		}
	}
}
#endregion