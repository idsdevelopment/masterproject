using TripManagement.AlternateViews;

namespace DroidM.Views.Controls.Trips;

public partial class MainTripView : ContentView
{
	public MainTripView()
	{
		InitializeComponent();

		TripView.Trips = TripManagement.Globals.TripDisplayEntries.Collection;
		var Model = (MainTripViewModel)Root.BindingContext;
		Model.OnLocationItemsChanged = items => LocationItems = items;
	}

#region Company Items
	public static readonly BindableProperty LocationItemsProperty = BindableProperty.Create( nameof( LocationItems ),
	                                                                                         typeof( SCANNED_TRIP_COLLECTION ),
	                                                                                         typeof( MainTripView ),
	                                                                                         default( SCANNED_TRIP_COLLECTION ),
	                                                                                         BindingMode.OneWayToSource
	                                                                                       );

	public SCANNED_TRIP_COLLECTION LocationItems
	{
		get => (SCANNED_TRIP_COLLECTION)GetValue( LocationItemsProperty );
		private set => SetValue( LocationItemsProperty, value );
	}
#endregion
}