using System.Windows.Input;
using TripManagement.AlternateViews;

namespace DroidM.Views.Controls.Trips;

public partial class TripView : BaseView
{
	private readonly TripViewViewModel Model;

	public TripView()
	{
		InitializeComponent();
		var R   = Root;
		var M   = Model = (TripViewViewModel)R.BindingContext;
		var Res = R.Resources;

		M.BasicView        = (DataTemplate)Res[ "BasicView" ];
		M.ScanningTripView = (DataTemplate)Res[ "ScanningView" ];

		M.OnTapped             = OnTapped;
		M.OnLocationItemTapped = OnLocationTapped;
	}


#region Tapped
#region Item Tapped
	private void OnTapped( TripDisplayEntry? selectItem )
	{
		SelectedItem = selectItem;

		if( selectItem is not null && TapCommand is { } T && T.CanExecute( selectItem ) )
			T.Execute( selectItem );
	}


	public static readonly BindableProperty TapCommandProperty = BindableProperty.Create( nameof( TapCommand ),
	                                                                                      typeof( ICommand ),
	                                                                                      typeof( TripView ) );

	public ICommand TapCommand
	{
		get => (ICommand)GetValue( TapCommandProperty );
		set => SetValue( TapCommandProperty, value );
	}
#endregion

#region Location Tapped
	private void OnLocationTapped( ScannedTripDisplayEntry? selectItem )
	{
		SelectedLocationItem = selectItem;

		if( selectItem is not null && LocationTapCommand is { } T && T.CanExecute( selectItem ) )
			T.Execute( selectItem );
	}

	public static readonly BindableProperty LocationTapCommandProperty = BindableProperty.Create( nameof( LocationTapCommand ),
	                                                                                              typeof( ICommand ),
	                                                                                              typeof( TripView ) );

	public ICommand LocationTapCommand
	{
		get => (ICommand)GetValue( LocationTapCommandProperty );
		set => SetValue( LocationTapCommandProperty, value );
	}
#endregion
#endregion


#region Trips
	public static readonly BindableProperty TripsProperty = BindableProperty.Create( nameof( Trips ),
	                                                                                 typeof( TRIP_COLLECTION ),
	                                                                                 typeof( TripView ),
	                                                                                 propertyChanged: TripsPropertyChanged );

	private static void TripsPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is TripView TripView && newValue is TRIP_COLLECTION Trips )
			TripView.Model.Trips = Trips;
	}

	public TRIP_COLLECTION Trips
	{
		get => (TRIP_COLLECTION)GetValue( TripsProperty );
		set => SetValue( TripsProperty, value );
	}
#endregion

#region Selected Item
	public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create( nameof( SelectedItem ),
	                                                                                        typeof( TripDisplayEntry ),
	                                                                                        typeof( TripView ) );

	public TripDisplayEntry? SelectedItem
	{
		get => (TripDisplayEntry?)GetValue( SelectedItemProperty );
		set => SetValue( SelectedItemProperty, value );
	}


	public static readonly BindableProperty SelectedLocationItemProperty = BindableProperty.Create( nameof( SelectedLocationItem ),
	                                                                                                typeof( ScannedTripDisplayEntry ),
	                                                                                                typeof( TripView ) );

	public ScannedTripDisplayEntry? SelectedLocationItem
	{
		get => (ScannedTripDisplayEntry?)GetValue( SelectedLocationItemProperty );
		set => SetValue( SelectedLocationItemProperty, value );
	}
#endregion
}