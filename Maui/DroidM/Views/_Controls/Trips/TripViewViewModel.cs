﻿using System.Windows.Input;
using TripManagement.AlternateViews;

namespace DroidM.Views.Controls.Trips;

internal class TripViewViewModel : ViewModelBase
{
#region Trip
	public TRIP_COLLECTION Trips
	{
		get { return Get( () => Trips, [] ); }
		set { Set( () => Trips, value ); }
	}

	[DependsUpon( nameof( Trips ) )]
	public void WhenTripsChange()
	{
		Template = Trips switch
		           {
			           SCANNED_TRIP_COLLECTION => ScanningTripView,
			           not null                => BasicView,
			           _                       => null
		           };
	}
#endregion


#region Tapped Event
	public Action<TripDisplayEntry?>? OnTapped;

	public ICommand TappedCommand => Commands[ nameof( TappedCommand ) ];

	public void Execute_TappedCommand( TripDisplayEntry tappedItem )
	{
		OnTapped?.Invoke( tappedItem );
	}
#endregion

#region LocationItemTapped
	public Action<ScannedTripDisplayEntry?>? OnLocationItemTapped;
	public ICommand                          LocationItemTappedCommand => Commands[ nameof( LocationItemTappedCommand ) ];

	public void Execute_LocationItemTappedCommand( ScannedTripDisplayEntry tappedItem )
	{
		OnLocationItemTapped?.Invoke( tappedItem );
	}
#endregion

#region Data Templates
	public DataTemplate? BasicView,
	                     ScanningTripView;

	public DataTemplate? Template
	{
		get { return Get( () => Template, (DataTemplate?)null ); }
		set { Set( () => Template, value ); }
	}
#endregion
}