﻿using System.Windows.Input;
using TripManagement.AlternateViews;

namespace DroidM.Views.Controls.Trips;

internal class MainTripViewModel : ViewModelBase
{
#region Location Items
	public Action<SCANNED_TRIP_COLLECTION>? OnLocationItemsChanged;
#endregion

#region Tapped
	public ICommand OnTappedCommand => Commands[ nameof( OnTappedCommand ) ];

	public void Execute_OnTappedCommand( TripDisplayEntry selected )
	{
		var LocationItems = new SCANNED_TRIP_COLLECTION();
		var Trips         = TripManagement.Globals.TripDisplayEntries.Collection;

		var IsPickup   = selected.IsPickup;
		var IsDelivery = selected.IsDelivery;

		foreach( var Trip in Trips )
		{
			if( ( ( IsPickup && Trip.IsPickup ) || ( IsDelivery && Trip.IsDelivery ) ) && selected.IsSameCompany( Trip ) )
				LocationItems.Add( new ScannedTripDisplayEntry( Trip ) );
		}

		OnLocationItemsChanged?.Invoke( LocationItems );
	}
#endregion
}