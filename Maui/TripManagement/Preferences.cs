﻿namespace TripManagement;

public partial class Globals
{
	public class Preferences
	{
		// ReSharper disable once InconsistentNaming
		internal static DevicePreferences _Preferences = null!;

		public static bool AllowUnderScans => _Preferences.AllowUnderScans;
		public static bool AllowOverScans  => _Preferences.AllowOverScans;
	}
}