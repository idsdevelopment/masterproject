﻿#nullable enable

// ReSharper disable UseWithExpressionToCopyStruct
#pragma warning disable CA1416

namespace TBarcode;

public sealed partial class TrwBarcode
{
	private void DrawBar( Pen pen )
	{
		Graphics!.DrawLine( pen, TopPoint, BottomPoint );
	}

	private void MovePen( int penWidth, int distance )
	{
		var X = TopPoint.X + ( penWidth * distance );
		TopPoint.X    = X;
		BottomPoint.X = X;
	}

	private void MovePenNarrow( int distance )
	{
		MovePen( ThinPenWidth, distance );
	}

	private void MovePenWide( int distance )
	{
		MovePen( ThickPenWidth, distance );
	}

	private void DrawLine()
	{
		DrawBar( ThinPen! );
	}

	private void DrawNarrow( bool isBar )
	{
		if( isBar )
			DrawLine();

		MovePenNarrow( 1 );
	}

	private void DrawWide( bool isBar )
	{
		if( isBar )
			DrawBar( ThickPen! );

		MovePenWide( 1 );
	}

	private void MovePen()
	{
		MovePenNarrow( 1 );
	}

	private void DrawLineAndMove()
	{
		DrawNarrow( true );
	}

	private void Do101()
	{
		DrawLineAndMove();
		MovePen();
		DrawLineAndMove();
	}

	private void Do01010()
	{
		MovePen(); // 01010
		Do101();
		MovePen();
	}

	private void Do010()
	{
		MovePen();
		DrawLineAndMove();
		MovePen();
	}

	private void Do01()
	{
		MovePen();
		DrawLineAndMove();
	}

	private void Do1011()
	{
		Do101();
		DrawLineAndMove();
	}

	private int GetOffset() => TopPoint.X - Padding.Left;

#region Text
	// Changes the current font size
	private SizeF SizeText( int maxWidth, int totalChars, int maxHeight = 30 )
	{
		SizeF Measure;

		var G = Graphics!;

		if( totalChars <= 0 )
			totalChars = 1;

		var S = new string( '0', totalChars );

		for( var I = 1;; ++I )
		{
			FontSize = I;
			Measure  = G.MeasureString( S, Font );

			if( ( Measure.Height >= maxHeight ) || ( Measure.Width >= maxWidth ) )
				break;
		}
		return Measure;
	}

	private void DrawCodeTextRect( string code, RectangleF rect )
	{
		var       G  = Graphics!;
		using var Bc = new SolidBrush( BackgroundColor );
		G.FillRectangle( Bc, rect );

		using var Tc = new SolidBrush( TextColor );
		G.DrawString( code, Font, Tc, new PointF( rect.X, rect.Y ) );
	}

	private RectangleF GetTextArea( SizeF measure, int startOffset, int endOffset )
	{
		var CenterX = startOffset + ( ( endOffset - startOffset ) / 2 );
		var BottomX = CenterX - ( measure.Width / 2 );
		var BottomY = AdjustedHeight - measure.Height - ( measure.Height / 15 );
		return new RectangleF( BottomX, BottomY, measure.Width, measure.Height );
	}

	private RectangleF GetTextArea( string code, int startOffset, int endOffset ) => GetTextArea( Graphics!.MeasureString( code, Font ), startOffset, endOffset );

	private void AutoDrawText( int startOffset, int endOffset, string code, int maxHeight, bool fullFill = false )
	{
		SizeText( endOffset - startOffset, code.Length, maxHeight );
		var Rect = GetTextArea( code, startOffset, endOffset );

		if( fullFill )
		{
			var FillRect = new RectangleF( 0, Rect.Y, AdjustedWidth, Rect.Height );

			using var Bc = new SolidBrush( BackgroundColor );
			Graphics!.FillRectangle( Bc, FillRect );
		}
		DrawCodeTextRect( code, Rect );
	}
#endregion
}