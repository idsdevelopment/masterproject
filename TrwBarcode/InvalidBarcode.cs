﻿#nullable enable

#pragma warning disable CA1416

namespace TBarcode;

public sealed partial class TrwBarcode
{
	internal void InvalidBarcode()
	{
		using var Pen = new Pen( Color.Red, ThickPenWidth );
		var       G   = Graphics!;
		G.DrawLine( Pen, 0, 0, AdjustedWidth, AdjustedHeight );
		G.DrawLine( Pen, AdjustedWidth, 0, 0, AdjustedHeight );
	}
}