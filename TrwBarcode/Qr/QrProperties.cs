﻿#nullable enable

namespace TrwBarcode.Qr;

public sealed class QrProperties
{
#region Debugging
	public bool Debug { get; set; }
#endregion

	internal QrProperties( TBarcode.TrwBarcode owner )
	{
		Owner = owner;
	}

	private readonly TBarcode.TrwBarcode Owner;

#region Modules
	public   int MinSize { get; set; } = 2;
	internal int Modules => 21 + ( ( _Version - 1 ) * 4 );

	internal int ModulesSize
	{
		get
		{
			var Size = Math.Max( Owner.Height, Owner.Width );
			var M    = Modules + 8; // 4 blank each size
			var W    = Size / M;

			var Ms = MinSize;

			if( W < Ms )
			{
				W    = Ms;
				Size = Ms * M;
			}

			Owner.ThickBarWidth = W;
			Owner.ThickBarWidth = W;

			Owner.Height = Size;
			Owner.Width  = Size;

			return W;
		}
	}
#endregion

#region Mask Pattern
	public MASK_PATTERN MaskPatternUsed { get; internal set; } = MASK_PATTERN.ZERO;

	private MASK_PATTERN _MaskPattern = MASK_PATTERN.AUTO;

	public MASK_PATTERN MaskPattern
	{
		get => _MaskPattern;
		set
		{
			switch( value )
			{
			case MASK_PATTERN.AUTO:
			case MASK_PATTERN.DEBUG_NO_MASK:
				break;

			case < MASK_PATTERN.ZERO or > MASK_PATTERN.SEVEN:
				value = MASK_PATTERN.AUTO;
				break;
			}
			_MaskPattern = value;
		}
	}
#endregion

#region Version
	public byte Version
	{
		get => _Version;
		set
		{
			_Version = value switch
					   {
						   < 1  => 1,
						   > 40 => 40,
						   _    => value
					   };
		}
	}

	private byte _Version;
#endregion

#region Encoding
	public bool IsUtf8 = false;

	public enum ENCODING : byte
	{
		AUTO         = 0b0000,
		NUMERIC      = 0b0001,
		ALPHANUMERIC = 0b0010,
		BINARY       = 0b0100
	}

	public (ENCODING Encoding, object Data) Data = ( ENCODING.AUTO, new object() );

	public enum ERROR_CORRECTION : byte
	{
		LOW,
		MEDIUM,
		QUARTILE,
		HIGH,
		AUTO,
		L = LOW,
		M = MEDIUM,
		Q = QUARTILE,
		H = HIGH
	}

	public enum MASK_PATTERN : sbyte
	{
		DEBUG_NO_MASK = -2,
		AUTO,
		ZERO,
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN
	}

	public ERROR_CORRECTION ErrorCorrection { get; set; } = ERROR_CORRECTION.QUARTILE;
#endregion
}