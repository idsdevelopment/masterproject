﻿// ReSharper disable BuiltInTypeReferenceStyle

namespace TrwBarcode.Qr;

internal class BitBuffer
{
	private const byte BITS = 8;

	private const ushort BUFFER_BITS = sizeof( uint ) * BITS;

	public uint Length => (uint)( ( Index * BITS ) + BitIndex );

	internal BitBuffer( ushort bitLimit )
	{
		var BufferSize = bitLimit / BITS;

		if( ( bitLimit % 8 ) != 0 )
			BufferSize += 1;

		Buffer = new byte[ BufferSize ];
	}

	private byte BitIndex;

	private readonly byte[] Buffer;

	private uint   BufferedBits;
	private ushort Index;


	public byte[] ReadAll()
	{
		Flush();
		return Buffer;
	}

	public void Flush()
	{
		if( BitIndex > 0 )
			WriteBits( 0, (byte)( BITS - BitIndex ) );
	}

	public void WriteBits( byte[] bits )
	{
		foreach( var B in bits )
			WriteBits( B, BITS );
	}


	public void WriteBits( uint bits, byte numberOfBitToWrite )
	{
		if( numberOfBitToWrite > BUFFER_BITS )
			numberOfBitToWrite = (byte)BUFFER_BITS;

		bits &= ~( uint.MaxValue << numberOfBitToWrite );

		BufferedBits |= bits << (byte)( BUFFER_BITS - BitIndex - numberOfBitToWrite );

		BitIndex += numberOfBitToWrite;

		while( BitIndex >= BITS )
		{
			var Byte = (byte)( BufferedBits >>> ( BUFFER_BITS - BITS ) ); // Take the Top Byte
			Buffer[ Index++ ] =   Byte;
			BufferedBits      <<= BITS; // Push off the top bits
			BitIndex          -=  BITS;
		}
	}
}