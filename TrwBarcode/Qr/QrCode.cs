﻿#nullable enable

#pragma warning disable CA1416

// ReSharper disable InconsistentNaming

namespace TrwBarcode.Qr;

internal sealed class QrCode
{
	private const byte FREE            = 0,
	                   _               = 1,
	                   X               = 2,
	                   RESERVED        = 3,
	                   RESERVED7       = 4,
	                   SPACER          = 5,
	                   TIMING          = 6,
	                   VERTICAL_TIMING = 7,
	                   TIMING_SPACER   = 8,
	                   ALIGNMENT       = 9,
	                   FINDER          = 10,
	                   FORMAT          = 11,
	                   MASKED_         = 12,
	                   MASKED_X        = 13,
	                   DARK_SPOT       = 14,
	                   VERSION_7       = 15,
	                   s               = SPACER,
	                   F               = FINDER,
	                   A               = ALIGNMENT,
	                   f               = FORMAT,
	                   r               = RESERVED,
	                   r7              = RESERVED7,
	                   v7              = VERSION_7;

	private const int ENCODING_BITS = 4,
	                  BITS          = 8;

	internal QrCode( TBarcode.TrwBarcode owner, QrProperties properties )
	{
		var Version = properties.Version;

		var InvalidBarcode = false;

		try
		{
			// ReSharper disable once InconsistentNaming
			var (Encoding, Data) = properties.Data;

			var  Invalid = false;
			uint Length  = 0;

			var ErrorCorrection = properties.ErrorCorrection;

			// AlphaNumeric or Numeric contains invalid characters swap to byte mode
			if( Encoding is QrProperties.ENCODING.ALPHANUMERIC or QrProperties.ENCODING.NUMERIC or QrProperties.ENCODING.AUTO )
			{
				if( Data is string Str )
				{
					var Default  = System.Text.Encoding.Default;
					var DefBytes = Default.GetBytes( Str );

					if( !properties.IsUtf8 )
					{
						var Iso      = System.Text.Encoding.GetEncoding( "ISO-8859-1" );
						var IsoBytes = System.Text.Encoding.Convert( Default, Iso, DefBytes );
						Str  = Iso.GetString( IsoBytes );
						Data = Str;
					}
					else
					{
						var Utf8      = System.Text.Encoding.UTF8;
						var Utf8Bytes = System.Text.Encoding.Convert( Default, Utf8, DefBytes );
						Str  = Utf8.GetString( Utf8Bytes );
						Data = Str;
					}

					foreach( var C in Str )
					{
						if( CharacterEncodingTable[ C ] == INVALID )
						{
							if( Encoding != QrProperties.ENCODING.AUTO )
								Encoding = QrProperties.ENCODING.BINARY;

							var Bytes = new byte[ Str.Length ];

							for( var Index = 0; Index < Str.Length; Index++ )
								Bytes[ Index ] = (byte)Str[ Index ];

							Data = Bytes;
							break;
						}
					}
				}
				else
					Invalid = true;
			}

			if( Encoding == QrProperties.ENCODING.AUTO )
			{
				switch( Data )
				{
				case string Str:
					Length = (uint)Str.Length;

					var OnlyNumeric = true;

					foreach( var C in Str )
					{
						if( C is not (>= '0' and <= '9') )
						{
							OnlyNumeric = false;
							break;
						}
					}
					Encoding = OnlyNumeric ? QrProperties.ENCODING.NUMERIC : QrProperties.ENCODING.ALPHANUMERIC;
					break;

				case sbyte[] Sb:
					Length   = (uint)Sb.Length;
					Encoding = QrProperties.ENCODING.BINARY;
					break;

				case byte[] B:
					Length   = (uint)B.Length;
					Encoding = QrProperties.ENCODING.BINARY;
					break;
				}

				ushort BestMatch = 0;

				var Smallest = int.MaxValue;

				foreach( var Info in ErrorCorrectionCodeWordBlockInformation )
				{
					if( ( ErrorCorrection == QrProperties.ERROR_CORRECTION.AUTO ) || ( (QrProperties.ERROR_CORRECTION)( Info.Key % 10 ) == ErrorCorrection ) )
					{
#pragma warning disable IDE0042 // Deconstruct variable declaration
						var Value = Info.Value;
#pragma warning restore IDE0042 // Deconstruct variable declaration

						var MaxLen = Encoding switch
						             {
							             QrProperties.ENCODING.ALPHANUMERIC => Value.AlphaNumericLimit,
							             QrProperties.ENCODING.NUMERIC      => Value.NumericLimit,
							             _                                  => Value.BinaryLimit
						             };

						var Diff = MaxLen - (int)Length;

						if( Diff >= 0 )
						{
							if( Diff < Smallest )
							{
								Smallest  = Diff;
								BestMatch = Info.Key;
							}
						}
					}
				}

				ErrorCorrection = (QrProperties.ERROR_CORRECTION)( BestMatch % 10 );
				Version         = (byte)( BestMatch / 10 );

				if( Version == 0 )
					Invalid = true;
				else
				{
					properties.Version         = Version;
					properties.ErrorCorrection = ErrorCorrection;
				}
			}

			InvalidBarcode |= Invalid;

			if( !Invalid )
			{
				var (DataBitLimit,
					_,
					EccCodewordsPerBlock,
					NumberOfBlocksInGroup1,
					NumberOfDataCodewordsInGroup1Block,
					NumberOfBlocksInGroup2,
					NumberOfDataCodewordsInGroup2Block,
					_, _, _) = ErrorCorrectionCodeWordBlockInformation[ (ushort)( ( Version * 10 ) + (byte)ErrorCorrection ) ];

				// Packed bytes stream
				var BitBuffer = new BitBuffer( DataBitLimit );

				BitBuffer.WriteBits( (byte)Encoding, ENCODING_BITS );

			#region Write The Header
				// Build Type / Length

				switch( Encoding )
				{
				case QrProperties.ENCODING.BINARY when Data is sbyte[] Sb:
					Length = (uint)Sb.Length;
					var BinBytes = new byte[ Length ];

					for( var Index = 0; Index < Sb.Length; Index++ )
						BinBytes[ Index ] = (byte)Sb[ Index ];

					Data = BinBytes;
					goto Binary;

				case QrProperties.ENCODING.BINARY when Data is byte[] Bytes:
					Length = (uint)Bytes.Length;
				Binary:

					switch( Version )
					{
					case >= 1 and <= 9: // 8 bit packing
						BitBuffer.WriteBits( Length, 8 );
						break;

					case <= 40: // 16 bit packing (ushort is 16 bits)
						BitBuffer.WriteBits( Length, 16 );
						break;

					default:
						InvalidBarcode = true;
						break;
					}
					break;

				case QrProperties.ENCODING.ALPHANUMERIC when Data is string Str:
					Length = (uint)Str.Length;

					switch( Version )
					{
					case >= 1 and <= 9:
						BitBuffer.WriteBits( Length, 9 );
						break;

					case <= 26:
						BitBuffer.WriteBits( Length, 11 );
						break;

					case <= 40:
						BitBuffer.WriteBits( Length, 13 );
						break;

					default:
						InvalidBarcode = true;
						break;
					}
					break;

				case QrProperties.ENCODING.NUMERIC when Data is string Str:
					Length = (uint)Str.Length;

					switch( Version )
					{
					case >= 1 and <= 9:
						BitBuffer.WriteBits( Length, 10 );
						break;

					case <= 26:
						BitBuffer.WriteBits( Length, 12 );
						break;

					case <= 40:
						BitBuffer.WriteBits( Length, 14 );
						break;

					default:
						InvalidBarcode = true;
						break;
					}
					break;

				default:
					InvalidBarcode = true;
					break;
				}
			#endregion

				if( !InvalidBarcode )
				{
				#region Pack The Data
					switch( Encoding )
					{
					case QrProperties.ENCODING.BINARY:
						BitBuffer.WriteBits( (byte[])Data );
						break;

					case QrProperties.ENCODING.NUMERIC: // Pack Into 10 Bits
						var Ndx = 0;
						var Str = (string)Data;
						Length = (uint)Str.Length;

						for( var Count = Length / 3; Count-- > 0; )
						{
							var Value = (uint)( ( CharacterEncodingTable[ (byte)Str[ Ndx++ ] ] * 100 )
							                    + ( CharacterEncodingTable[ (byte)Str[ Ndx++ ] ] * 10 )
							                    + CharacterEncodingTable[ (byte)Str[ Ndx++ ] ] );

							BitBuffer.WriteBits( Value, 10 );
						}

						switch( Length - Ndx )
						{
						case 2:
							var Value = (uint)( ( CharacterEncodingTable[ (byte)Str[ Ndx++ ] ] * 10 )
							                    + CharacterEncodingTable[ (byte)Str[ Ndx ] ] );

							BitBuffer.WriteBits( Value, 7 );
							break;

						case 1:
							Value = CharacterEncodingTable[ (byte)Str[ Ndx ] ];
							BitBuffer.WriteBits( Value, 4 );
							break;
						}
						break;

					case QrProperties.ENCODING.ALPHANUMERIC:
						Ndx = 0;
						Str = (string)Data;

						for( var Count = Length / 2; Count-- > 0; )
						{
							var Value = (uint)( ( CharacterEncodingTable[ (byte)Str[ Ndx++ ] ] * 45 )
							                    + CharacterEncodingTable[ (byte)Str[ Ndx++ ] ] );

							BitBuffer.WriteBits( Value, 11 );
						}

						if( ( Length - Ndx ) == 1 )
							BitBuffer.WriteBits( CharacterEncodingTable[ (byte)Str[ Ndx ] ], 6 );
						break;
					}

					// Set The Terminator
					var CurrentBits = BitBuffer.Length;

					if( CurrentBits < DataBitLimit )
					{
						var Dif = DataBitLimit - (int)CurrentBits;
						BitBuffer.WriteBits( 0, (byte)( Dif < 4 ? Dif : 4 ) );
					}

					BitBuffer.Flush();
				#endregion

				#region Do The Ecc
					// Pad for ECC
					CurrentBits = BitBuffer.Length;
					var Diff = DataBitLimit - (int)CurrentBits;

					if( Diff < 0 )
						InvalidBarcode = true;
					else
					{
						var Even = true;

						while( Diff > 0 )
						{
							var Val = (uint)( Even ? 0b11101100 : 0b00010001 );
							BitBuffer.WriteBits( Val, BITS );
							Even =  !Even;
							Diff -= BITS;
						}

						var AllBytes = BitBuffer.ReadAll();
						var ALen     = AllBytes.Length;

						var ANdx        = 0;
						var TotalBlocks = 0;

						var BlockList = new List<byte[]>();

					#region Build the Blocks
						byte[][] BlocksInGroup( byte numberOfBlocksInGroup, byte numberOfByteInBlock )
						{
							TotalBlocks += numberOfBlocksInGroup;

							var Blocks = new byte[ numberOfBlocksInGroup ][];

							for( byte I = 0; I < numberOfBlocksInGroup; I++ )
							{
								var Block = new byte[ numberOfByteInBlock ];
								BlockList.Add( Block );

								for( var Ndx = 0; Ndx < numberOfByteInBlock; )
									Block[ Ndx++ ] = ANdx < ALen ? AllBytes[ ANdx++ ] : (byte)0;
								Blocks[ I ] = Block;
							}
							return Blocks;
						}

						var NumGroups = NumberOfBlocksInGroup2 > 0 ? 2 : 1;

						// [Groups][Blocks][Bytes]
						var GroupBlocks = new byte[ NumGroups ][][];

						for( byte I = 0; I < NumGroups; I++ )
						{
							GroupBlocks[ I ] = I == 0 ? BlocksInGroup( NumberOfBlocksInGroup1, NumberOfDataCodewordsInGroup1Block )
								                   : BlocksInGroup( NumberOfBlocksInGroup2, NumberOfDataCodewordsInGroup2Block );
						}
					#endregion

					#region Build the Ecc
						var Generator = GeneratorPolynomials[ EccCodewordsPerBlock ];
						var XorMask   = new byte[ EccCodewordsPerBlock ];

						var EccBlocks = new byte[ TotalBlocks ][];
						var EccNdx    = 0;

						foreach( var Block in GroupBlocks )
						{
							foreach( var CWords in Block )
							{
								var BlockLength = CWords.Length;
								var CodeWords   = new byte[ BlockLength + EccCodewordsPerBlock ]; // Add Zeros Buffer
								Array.Copy( CWords, CodeWords, BlockLength );

								for( var CNdx = 0; CNdx < BlockLength; )
								{
									var CodeWord = CodeWords[ CNdx++ ];

									if( CodeWord != 0 )
									{
										var Exponent = IntegerToExponent[ CodeWord ];

										for( var J = 0; J < EccCodewordsPerBlock; J++ )
											XorMask[ J ] = ExponentToInteger[ (byte)( ( Generator[ J ] + Exponent ) % 255 ) ];

										var XNdx = CNdx;

										foreach( var Mask in XorMask )
											CodeWords[ XNdx++ ] ^= Mask;
									}
								}
								var Ecc = new byte[ EccCodewordsPerBlock ];
								Array.Copy( CodeWords, BlockLength, Ecc, 0, EccCodewordsPerBlock );

								EccBlocks[ EccNdx++ ] = Ecc;
							}
						}
					#endregion

					#region Interleave the data
						var BufferSize = 0;
						var Max        = 0;

						void Size( IEnumerable<byte[]> block )
						{
							foreach( var Block in block )
							{
								var L = Block.Length;
								BufferSize += L;
								Max        =  Math.Max( Max, L );
							}
						}

						Size( BlockList );
						Size( EccBlocks );

						var InterleaveBuffer = new byte[ BufferSize + 1 ]; // Zero Pad End
						var INdx             = 0;

						void Interleave( IReadOnlyList<byte[]> blocks )
						{
							var Len = blocks.Count;

							for( var BlockOff = 0; BlockOff < Max; BlockOff++ )
							{
								for( var Index = 0; Index < Len; Index++ )
								{
									var Block = blocks[ Index ];
									var BLen  = Block.Length;

									if( ( BlockOff < BLen ) && ( BLen <= Max ) )
										InterleaveBuffer[ INdx++ ] = Block[ BlockOff ];
								}
							}
						}

						Interleave( BlockList );
						Interleave( EccBlocks );
					#endregion

					#region Define the Drawing Area
						var Modules = properties.Modules;

						var Canvas = new byte[ Modules ][];

						for( var I = 0; I < Modules; I++ )
							Canvas[ I ] = new byte[ Modules ];

					#region Add The Finder Patterns
						void PlaceFinderPattern( int x, int y )
						{
							foreach( var Line in FinderPattern )
								Array.Copy( Line, 0, Canvas[ y++ ], x, 7 );
						}

						PlaceFinderPattern( 0, 0 );
						PlaceFinderPattern( Modules - 7, 0 );
						PlaceFinderPattern( 0, Modules - 7 );

						void FillRow( int x, int y, int width, byte value )
						{
							var Row = Canvas[ y ];

							for( var W = 0; W++ < width; )
								Row[ x++ ] = value;
						}

						void FillColumn( int x, int y, int length, int width, byte value )
						{
							for( var L = 0; L++ < length; )
								FillRow( x, y++, width, value );
						}

						FillRow( 0, 7, 8, SPACER );
						FillColumn( 7, 0, 7, 1, SPACER );

						var M8 = Modules - 8;

						FillRow( M8, 7, 8, SPACER );
						FillColumn( M8, 0, 8, 1, SPACER );

						FillRow( 0, M8, 8, SPACER );
						FillColumn( 7, M8, 8, 1, SPACER );
					#endregion

					#region Add the Alignment Patterns
						var TopLeft    = new Rectangle( 0, 0, 8, 8 ); // Include Spacers
						var TopRight   = new Rectangle( M8, 0, 8, 8 );
						var BottomLeft = new Rectangle( 0, M8, 8, 8 );

						if( Version > 1 )
						{
							var Bytes1 = AlignmentPlacement[ Version - 2 ];
							ALen = Bytes1.Length;

							for( var I = 0; I < ALen; I++ )
							{
								for( var J = 0; J < ALen; J++ )
								{
									var Xa = Bytes1[ I ];
									var Ya = Bytes1[ J ];

									if( !TopLeft.Contains( Xa, Ya ) && !TopRight.Contains( Xa, Ya ) && !BottomLeft.Contains( Xa, Ya ) )
									{
										Xa -= 2; // Corner
										Ya -= 2;

										foreach( var Line in AlignmentPattern )
											Array.Copy( Line, 0, Canvas[ Ya++ ], Xa, 5 );
									}
								}
							}
						}
					#endregion

					#region Reserved
						FillRow( 0, 8, 9, RESERVED );
						FillColumn( 8, 0, 9, 1, RESERVED );
						FillRow( M8, 8, 8, RESERVED );
						FillColumn( 8, M8, 8, 1, RESERVED );

						if( Version >= 7 )
						{
							FillColumn( M8 - 3, 0, 6, 3, RESERVED7 );
							FillColumn( 0, M8 - 3, 3, 6, RESERVED7 );
						}
					#endregion

					#region Draw the Timing Lines
						var White = false;
						var Xa1   = 8;

						var Row6 = Canvas[ 6 ];

						// Horizontal 
						while( Xa1 < M8 )
						{
							Row6[ Xa1++ ] = White ? TIMING_SPACER : TIMING;
							White         = !White;
						}

						White = false;

						var Ya1 = 8;

						byte[] Row;

						// Vertical
						while( Ya1 < M8 )
						{
							Row      = Canvas[ Ya1++ ];
							Row[ 6 ] = White ? TIMING_SPACER : VERTICAL_TIMING;
							White    = !White;
						}
					#endregion

						//Draw The Dark Module
						Canvas[ Modules - 8 ][ 8 ] = DARK_SPOT;
					#endregion

					#region Plot The Data
						var M1       = Modules - 1;
						var Xi       = M1; // Bottom Right Corner
						var CurrentX = M1;
						var Yi       = Xi;
						Row = Canvas[ Yi ];

						var Underflow = new byte[ 1 ];

						var RightSide = true;
						var Up        = true;

						void UpdateCurrentX()
						{
							RightSide =  true;
							CurrentX  -= CurrentX == 8 ? 3 : 2; // Skip Timing Band
							Xi        =  CurrentX;
						}

						ref byte GetNext()
						{
							while( true )
							{
								var XNdx = RightSide ? Xi : Xi - 1;

								if( XNdx < 0 )
									return ref Underflow[ 0 ]; // In case of error

								void Next()
								{
									RightSide = !RightSide;

									if( RightSide )
									{
										if( Up )
										{
											if( --Yi < 0 )
											{
												Up = false;
												UpdateCurrentX();
												Yi = 0;
											}
										}
										else // Down
										{
											if( ++Yi >= Modules )
											{
												Up = true;
												UpdateCurrentX();
												Yi = M1;
											}
										}
										Row = Canvas[ Yi ];
									}
								}

								var B = Row[ XNdx ];

								switch( B )
								{
								case FREE:
									var SaveRow = Row;
									Next();
									return ref SaveRow[ XNdx ];

								default:
									Next();
									break;
								}
							}
						}

						foreach( var B in InterleaveBuffer )
						{
							for( var Bit = BITS; Bit > 0; )
								GetNext() = ( B & ( 1 << --Bit ) ) != 0 ? X : _;
						}
					#endregion

					#region Mask Pattern
						var CLen       = Canvas.Length;
						var MaskCanvas = new byte[ CLen, CLen ];

						void CopyToMaskCanvas( int mask )
						{
							for( var I = 0; I < CLen; I++ )
							{
								var SourceArray = Canvas[ I ];

								for( var J = 0; J < CLen; J++ )
									MaskCanvas[ I, J ] = SourceArray[ J ];
							}

						#region Ecc / Mask Format
							var EccMask = mask == (int)QrProperties.MASK_PATTERN.DEBUG_NO_MASK
								              ? DebugNoMask
								              : EccMaskFormat[ (ushort)( ( (ushort)ErrorCorrection * 10 ) + mask ) ];

							MaskCanvas[ 8, 0 ] = EccMask[ 0 ];
							MaskCanvas[ 8, 1 ] = EccMask[ 1 ];
							MaskCanvas[ 8, 2 ] = EccMask[ 2 ];
							MaskCanvas[ 8, 3 ] = EccMask[ 3 ];
							MaskCanvas[ 8, 4 ] = EccMask[ 4 ];
							MaskCanvas[ 8, 5 ] = EccMask[ 5 ];
							MaskCanvas[ 8, 7 ] = EccMask[ 6 ]; // Skip Timing Mark
							MaskCanvas[ 8, 8 ] = EccMask[ 7 ];

							// Other Side
							var Ndx = M8;
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 7 ];
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 8 ];
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 9 ];
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 10 ];
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 11 ];
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 12 ];
							MaskCanvas[ 8, Ndx++ ] = EccMask[ 13 ];
							MaskCanvas[ 8, Ndx ]   = EccMask[ 14 ];

							MaskCanvas[ 0, 8 ] = EccMask[ 14 ];
							MaskCanvas[ 1, 8 ] = EccMask[ 13 ];
							MaskCanvas[ 2, 8 ] = EccMask[ 12 ];
							MaskCanvas[ 3, 8 ] = EccMask[ 11 ];
							MaskCanvas[ 4, 8 ] = EccMask[ 10 ];
							MaskCanvas[ 5, 8 ] = EccMask[ 9 ]; // Skip Timing Mark
							MaskCanvas[ 7, 8 ] = EccMask[ 8 ];

							Ndx                    = M8 + 1; // Skip Black Spot
							MaskCanvas[ Ndx++, 8 ] = EccMask[ 6 ];
							MaskCanvas[ Ndx++, 8 ] = EccMask[ 5 ];
							MaskCanvas[ Ndx++, 8 ] = EccMask[ 4 ];
							MaskCanvas[ Ndx++, 8 ] = EccMask[ 3 ];
							MaskCanvas[ Ndx++, 8 ] = EccMask[ 2 ];
							MaskCanvas[ Ndx++, 8 ] = EccMask[ 1 ];
							MaskCanvas[ Ndx, 8 ]   = EccMask[ 0 ];

							if( Version >= 7 )
							{
								var VInfo = Version7Information[ Version - 7 ];

								var M11 = Modules - 11;

								// Top Right
								MaskCanvas[ 0, M11 ]     = VInfo[ 17 ];
								MaskCanvas[ 0, M11 + 1 ] = VInfo[ 16 ];
								MaskCanvas[ 0, M11 + 2 ] = VInfo[ 15 ];
								MaskCanvas[ 1, M11 ]     = VInfo[ 14 ];
								MaskCanvas[ 1, M11 + 1 ] = VInfo[ 13 ];
								MaskCanvas[ 1, M11 + 2 ] = VInfo[ 12 ];
								MaskCanvas[ 2, M11 ]     = VInfo[ 11 ];
								MaskCanvas[ 2, M11 + 1 ] = VInfo[ 10 ];
								MaskCanvas[ 2, M11 + 2 ] = VInfo[ 9 ];
								MaskCanvas[ 3, M11 ]     = VInfo[ 8 ];
								MaskCanvas[ 3, M11 + 1 ] = VInfo[ 7 ];
								MaskCanvas[ 3, M11 + 2 ] = VInfo[ 6 ];
								MaskCanvas[ 4, M11 ]     = VInfo[ 5 ];
								MaskCanvas[ 4, M11 + 1 ] = VInfo[ 4 ];
								MaskCanvas[ 4, M11 + 2 ] = VInfo[ 3 ];
								MaskCanvas[ 5, M11 ]     = VInfo[ 2 ];
								MaskCanvas[ 5, M11 + 1 ] = VInfo[ 1 ];
								MaskCanvas[ 5, M11 + 2 ] = VInfo[ 0 ];

								// Bottom Left
								MaskCanvas[ M11, 0 ]     = VInfo[ 17 ];
								MaskCanvas[ M11 + 1, 0 ] = VInfo[ 16 ];
								MaskCanvas[ M11 + 2, 0 ] = VInfo[ 15 ];
								MaskCanvas[ M11, 1 ]     = VInfo[ 14 ];
								MaskCanvas[ M11 + 1, 1 ] = VInfo[ 13 ];
								MaskCanvas[ M11 + 2, 1 ] = VInfo[ 12 ];
								MaskCanvas[ M11, 2 ]     = VInfo[ 11 ];
								MaskCanvas[ M11 + 1, 2 ] = VInfo[ 10 ];
								MaskCanvas[ M11 + 2, 2 ] = VInfo[ 9 ];
								MaskCanvas[ M11, 3 ]     = VInfo[ 8 ];
								MaskCanvas[ M11 + 1, 3 ] = VInfo[ 7 ];
								MaskCanvas[ M11 + 2, 3 ] = VInfo[ 6 ];
								MaskCanvas[ M11, 4 ]     = VInfo[ 5 ];
								MaskCanvas[ M11 + 1, 4 ] = VInfo[ 4 ];
								MaskCanvas[ M11 + 2, 4 ] = VInfo[ 3 ];
								MaskCanvas[ M11, 5 ]     = VInfo[ 2 ];
								MaskCanvas[ M11 + 1, 5 ] = VInfo[ 1 ];
								MaskCanvas[ M11 + 2, 5 ] = VInfo[ 0 ];
							}
						#endregion
						}

						void Invert( int x, int y )
						{
							var B = MaskCanvas[ y, x ];

							switch( B )
							{
							// ReSharper disable once RedundantVerbatimPrefix
							case @_:
								B = MASKED_;
								break;

							case X:
								B = MASKED_X;
								break;

							default:
								return;
							}
							MaskCanvas[ y, x ] = B;
						}

						void Mask0()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									if( ( ( Cx + Cy ) % 2 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask1()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									if( ( Cy % 2 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask2()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									if( ( Cx % 3 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask3()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									if( ( ( Cx + Cy ) % 3 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask4()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									if( ( ( Math.Floor( (float)Cy / 2 ) + Math.Floor( (float)Cx / 3 ) ) % 2 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask5()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									var Rc = Cy * Cx;

									if( ( ( Rc % 2 ) + ( Rc % 3 ) ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask6()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									var Rc = Cy * Cx;

									if( ( ( ( Rc % 2 ) + ( Rc % 3 ) ) % 2 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						void Mask7()
						{
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; Cx++ )
								{
									if( ( ( ( ( Cy + Cx ) % 2 ) + ( ( Cy * Cx ) % 3 ) ) % 2 ) == 0 )
										Invert( Cx, Cy );
								}
							}
						}

						bool IsBlack( byte b )
						{
							var Black = b is X or MASKED_ or FORMAT or FINDER or ALIGNMENT or TIMING or VERTICAL_TIMING or DARK_SPOT or VERSION_7;
							return Black;
						}

						int Condition1()
						{
							var  Penalty = 0;
							var  Count   = 0;
							bool Black;

							void DoBlack( int x, int y )
							{
								Count = 0;
								Black = IsBlack( MaskCanvas[ y, x ] );
							}

							void DoCount()
							{
								if( Count >= 5 )
									Penalty += Count - 2;
								Count = 1;
							}

							void DoMask( int x, int y )
							{
								var B = MaskCanvas[ y, x ];

								switch( B )
								{
								case X:
								case MASKED_:
								case FORMAT:
								case VERSION_7:
								case FINDER:
								case ALIGNMENT:
								case TIMING:
								case VERTICAL_TIMING:
								case DARK_SPOT:
									if( Black )
										Count++;
									else
									{
										Black = true;
										DoCount();
									}
									break;

								default:
									if( !Black )
										Count++;
									else
									{
										Black = false;
										DoCount();
									}
									break;
								}
							}

							// Horizontal
							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								DoBlack( Cy, 0 );

								for( var Cx = 0; Cx < Modules; Cx++ )
									DoMask( Cx, Cy );

								DoCount();
							}
							DoCount();

							// Vertical
							for( var Cx = 0; Cx < Modules; Cx++ )
							{
								DoBlack( 0, Cx );

								for( var Cy = 0; Cy < Modules; Cy++ )
									DoMask( Cx, Cy );

								DoCount();
							}
							DoCount();

							return Penalty;
						}

						int Condition2()
						{
							var Penalty = 0;
							var Max1    = Modules - 1;

							for( var Cy = 0; Cy < Max1; Cy++ )
							{
								var Cy1 = Cy + 1;

								for( var Cx = 0; Cx < Max1; Cx++ )
								{
									var Cx1 = Cx + 1;

									// All Four Same Colour
									var B     = MaskCanvas[ Cy, Cx ];
									var Black = IsBlack( B );

									if( ( IsBlack( MaskCanvas[ Cy, Cx1 ] ) == Black )
									    && ( IsBlack( MaskCanvas[ Cy1, Cx ] ) == Black )
									    && ( IsBlack( MaskCanvas[ Cy1, Cx1 ] ) == Black ) )
										Penalty += 3;
								}
							}
							return Penalty;
						}

						int Condition3()
						{
							var Penalty = 0;
							var M11     = Modules - 11;

							bool IsPatternX( int x, int y )
							{
								var Black = IsBlack( MaskCanvas[ y, x ] );

								if( Black )
								{
									// X_XXX_X___
									return !IsBlack( MaskCanvas[ y, x + 1 ] )
									       && IsBlack( MaskCanvas[ y, x + 2 ] )
									       && IsBlack( MaskCanvas[ y, x + 3 ] )
									       && IsBlack( MaskCanvas[ y, x + 4 ] )
									       && !IsBlack( MaskCanvas[ y, x + 5 ] )
									       && IsBlack( MaskCanvas[ y, x + 6 ] )
									       && !IsBlack( MaskCanvas[ y, x + 7 ] )
									       && !IsBlack( MaskCanvas[ y, x + 8 ] )
									       && !IsBlack( MaskCanvas[ y, x + 9 ] )
									       && !IsBlack( MaskCanvas[ y, x + 10 ] );
								}

								//___X_XXX_X
								return !IsBlack( MaskCanvas[ y, x + 1 ] )
								       && !IsBlack( MaskCanvas[ y, x + 2 ] )
								       && !IsBlack( MaskCanvas[ y, x + 3 ] )
								       && IsBlack( MaskCanvas[ y, x + 4 ] )
								       && !IsBlack( MaskCanvas[ y, x + 5 ] )
								       && IsBlack( MaskCanvas[ y, x + 6 ] )
								       && IsBlack( MaskCanvas[ y, x + 7 ] )
								       && IsBlack( MaskCanvas[ y, x + 8 ] )
								       && !IsBlack( MaskCanvas[ y, x + 9 ] )
								       && IsBlack( MaskCanvas[ y, x + 10 ] );
							}

							bool IsPatternY( int x, int y )
							{
								var Black = IsBlack( MaskCanvas[ y, x ] );

								if( Black )
								{
									// X_XXX_X___
									return !IsBlack( MaskCanvas[ y + 1, x ] )
									       && IsBlack( MaskCanvas[ y + 2, x ] )
									       && IsBlack( MaskCanvas[ y + 3, x ] )
									       && IsBlack( MaskCanvas[ y + 4, x ] )
									       && !IsBlack( MaskCanvas[ y + 5, x ] )
									       && IsBlack( MaskCanvas[ y + 6, x ] )
									       && !IsBlack( MaskCanvas[ y + 7, x ] )
									       && !IsBlack( MaskCanvas[ y + 8, x ] )
									       && !IsBlack( MaskCanvas[ y + 9, x ] )
									       && !IsBlack( MaskCanvas[ y + 10, x ] );
								}

								//___X_XXX_X
								return !IsBlack( MaskCanvas[ y + 1, x ] )
								       && !IsBlack( MaskCanvas[ y + 2, x ] )
								       && !IsBlack( MaskCanvas[ y + 3, x ] )
								       && IsBlack( MaskCanvas[ y + 4, x ] )
								       && !IsBlack( MaskCanvas[ y + 5, x ] )
								       && IsBlack( MaskCanvas[ y + 6, x ] )
								       && IsBlack( MaskCanvas[ y + 7, x ] )
								       && IsBlack( MaskCanvas[ y + 8, x ] )
								       && !IsBlack( MaskCanvas[ y + 9, x ] )
								       && IsBlack( MaskCanvas[ y + 10, x ] );
							}

							for( var Cy = 0; Cy < M11; Cy++ )
							{
								for( var Cx = 0; Cx < M11; ++Cx )
								{
									if( IsPatternX( Cx, Cy ) )
										Penalty += 40;

									if( IsPatternY( Cx, Cy ) )
										Penalty += 40;
								}
							}
							return Penalty;
						}

						int Condition4()
						{
							var Black = 0;
							var Space = 0;

							for( var Cy = 0; Cy < Modules; Cy++ )
							{
								for( var Cx = 0; Cx < Modules; ++Cx )
								{
									if( IsBlack( MaskCanvas[ Cy, Cx ] ) )
										Black += 1;
									else
										Space += 1;
								}
							}
							var Total        = (float)( Black + Space );
							var PercentBlack = ( Black / Total ) * 100;
							var RoundedDown  = (int)( PercentBlack / 5 ) * 5;
							var Next5Up      = RoundedDown + 5;

							var Val1 = Math.Abs( RoundedDown - 50 ) / 5;
							var Val2 = Math.Abs( Next5Up - 50 ) / 5;

							var Penalty = Math.Min( Val1, Val2 ) * 10;
							return Penalty;
						}

						var BestPenalty = ( Penalty: int.MaxValue, Mask: -1 );

						var Mp = properties.MaskPattern;

						if( Mp == QrProperties.MASK_PATTERN.AUTO )
						{
							for( var Mask = 0; Mask < 8; Mask++ )
							{
								CopyToMaskCanvas( Mask );

								switch( Mask )
								{
								case 0:
									Mask0();
									break;

								case 1:
									Mask1();
									break;

								case 2:
									Mask2();
									break;

								case 3:
									Mask3();
									break;

								case 4:
									Mask4();
									break;

								case 5:
									Mask5();
									break;

								case 6:
									Mask6();
									break;

								case 7:
									Mask7();
									break;
								}

								var TotalPenalty = 0;

								for( var Cond = 0; Cond < 4; )
								{
									var Score = Cond++ switch
									            {
										            0 => Condition1(),
										            1 => Condition2(),
										            2 => Condition3(),
										            _ => Condition4()
									            };

									TotalPenalty += Score;

									if( TotalPenalty >= BestPenalty.Penalty )
										goto Loop;
								}

								if( TotalPenalty < BestPenalty.Penalty )
									BestPenalty = ( TotalPenalty, Mask );
							Loop: ;
							}
						}
						else
							BestPenalty.Mask = (int)Mp;

						CopyToMaskCanvas( BestPenalty.Mask );

						QrProperties.MASK_PATTERN MaskPatternUsed;

						switch( BestPenalty.Mask )
						{
						case (int)QrProperties.MASK_PATTERN.DEBUG_NO_MASK:
							MaskPatternUsed = QrProperties.MASK_PATTERN.DEBUG_NO_MASK;
							break;

						case 0:
							Mask0();
							MaskPatternUsed = QrProperties.MASK_PATTERN.ZERO;
							break;

						case 1:
							Mask1();
							MaskPatternUsed = QrProperties.MASK_PATTERN.ONE;
							break;

						case 2:
							Mask2();
							MaskPatternUsed = QrProperties.MASK_PATTERN.TWO;
							break;

						case 3:
							Mask3();
							MaskPatternUsed = QrProperties.MASK_PATTERN.THREE;
							break;

						case 4:
							Mask4();
							MaskPatternUsed = QrProperties.MASK_PATTERN.FOUR;
							break;

						case 5:
							Mask5();
							MaskPatternUsed = QrProperties.MASK_PATTERN.FIVE;
							break;

						case 6:
							Mask6();
							MaskPatternUsed = QrProperties.MASK_PATTERN.SIX;
							break;

						case 7:
							Mask7();
							MaskPatternUsed = QrProperties.MASK_PATTERN.SEVEN;
							break;

						default:
							MaskPatternUsed = QrProperties.MASK_PATTERN.ZERO;
							break;
						}

						properties.MaskPatternUsed = MaskPatternUsed;

						for( var My = 0; My < Modules; My++ )
						{
							var Rw = Canvas[ My ];

							for( var Mx = 0; Mx < Modules; Mx++ )
								Rw[ Mx ] = MaskCanvas[ My, Mx ];
						}
					#endregion

					#region Draw The Qr Code
						var Graphics = owner.Graphics!;
						var Bitmap   = owner.Bitmap!;

						var Height       = Bitmap.Height;
						var ModulePixels = Height / Modules;

						var BarColor = owner.BarColor;

						Pen NewPen( Color color )
						{
							return new Pen( color, ModulePixels );
						}

						using var Pen             = NewPen( BarColor );
						var       BackgroundColor = owner.BackgroundColor;

						using var BackgroundBrush = new SolidBrush( BackgroundColor );

						Pen ReservedPen       = null!,
						    Reserved7Pen      = null!,
						    Version7Pen       = null!,
						    SpacerPen         = null!,
						    VerticalTimingPen = null!,
						    TimingPen         = null!,
						    TimingSpacerPen   = null!,
						    AlignmentPen      = null!,
						    FinderPen         = null!,
						    FormatPen         = null!,
						    FreePen           = null!,
						    DarkSpotPen       = null!,
						    BlankPen          = null!,
						    GridPen           = null!;

						Font MaskFont = null!;
						var  FontSize = 0;

						var Debug = properties.Debug;

						if( Debug )
						{
							ReservedPen       = NewPen( Color.YellowGreen );
							Reserved7Pen      = NewPen( Color.DeepSkyBlue );
							Version7Pen       = NewPen( Color.Blue );
							SpacerPen         = NewPen( Color.AntiqueWhite );
							VerticalTimingPen = NewPen( Color.Red );
							TimingPen         = NewPen( Color.Gold );
							TimingSpacerPen   = NewPen( Color.LightSalmon );
							AlignmentPen      = NewPen( Color.Orange );
							FinderPen         = NewPen( Color.Goldenrod );
							FormatPen         = NewPen( Color.DarkGreen );
							FreePen           = NewPen( Color.HotPink );
							BlankPen          = NewPen( Color.Khaki );
							DarkSpotPen       = NewPen( Color.Navy );

							GridPen             = new Pen( Color.White, 1 );
							GridPen.DashStyle   = DashStyle.Custom;
							GridPen.DashPattern = [2F, 2F];

							MaskFont = new Font( "Courier New", FontSize = (int)Math.Ceiling( (float)ModulePixels / 3 ), FontStyle.Bold, GraphicsUnit.Pixel );
						}

						var Region = new Region( new Rectangle( 0, 0, Height, Height ) );

						Graphics.Clip = Region;
						Graphics.FillRegion( BackgroundBrush, Region );

						var YPixels = (int)Math.Ceiling( (float)ModulePixels / 2 ); // The actual line is in the center of the pen

						var M4 = ModulePixels / 4;

						foreach( var C in Canvas )
						{
							var XPixels = 0;

							void Draw( Pen drawPen )
							{
								Graphics.DrawLine( drawPen, XPixels, YPixels, XPixels + ModulePixels, YPixels );
							}

							void DrawMasked( Brush brush )
							{
								Graphics.DrawString( "M", MaskFont, brush, XPixels + FontSize, YPixels - M4 );
							}

							foreach( var B in C )
							{
								switch( B )
								{
								case VERTICAL_TIMING when Debug:
									Draw( VerticalTimingPen );
									break;

								case TIMING when Debug:
									Draw( TimingPen );
									break;

								case DARK_SPOT when Debug:
									Draw( DarkSpotPen );
									break;

								case VERSION_7 when Debug:
									Draw( Version7Pen );
									break;

								case X:
								case TIMING:
								case VERTICAL_TIMING:
								case DARK_SPOT:
								case VERSION_7:
									Draw( Pen );
									break;

								case MASKED_:
									Draw( Pen );

									if( Debug )
										DrawMasked( Brushes.White );
									break;

								default:
									if( Debug )
									{
										switch( B )
										{
										case FREE:
											Draw( FreePen );
											break;

										// ReSharper disable once RedundantVerbatimPrefix
										case @_:
											Draw( BlankPen );
											break;

										case MASKED_X:
											Draw( BlankPen );
											DrawMasked( Brushes.Black );
											break;

										case RESERVED:
											Draw( ReservedPen );
											break;

										case RESERVED7:
											Draw( Reserved7Pen );
											break;

										case SPACER:
											Draw( SpacerPen );
											break;

										case TIMING:
											Draw( TimingPen );
											break;

										case VERTICAL_TIMING:
											Draw( VerticalTimingPen );
											break;

										case TIMING_SPACER:
											Draw( TimingSpacerPen );
											break;

										case ALIGNMENT:
											Draw( AlignmentPen );
											break;

										case FINDER:
											Draw( FinderPen );
											break;

										case FORMAT:
											Draw( FormatPen );
											break;
										}
									}
									else
									{
										switch( B )
										{
										case FORMAT:
										case FINDER:
										case ALIGNMENT:
										case TIMING:
										case MASKED_:
										case VERSION_7:
											Draw( Pen );
											break;
										}
									}
									break;
								}
								XPixels += ModulePixels;
							}
							YPixels += ModulePixels;
						}

						if( Debug )
						{
							var Y1 = 0;
							var M  = Modules * ModulePixels;
							M1 = Modules + 1;

							for( var Cy = 0; Cy < M1; ++Cy, Y1 += ModulePixels )
							{
								var X1 = 0;

								for( var Cx = 0; Cx < M1; ++Cx, X1 += ModulePixels )
									Graphics.DrawLine( GridPen, X1, 0, X1, M );

								Graphics.DrawLine( GridPen, 0, Y1, M, Y1 );
							}
							ReservedPen.Dispose();
							Reserved7Pen.Dispose();
							SpacerPen.Dispose();
							VerticalTimingPen.Dispose();
							TimingPen.Dispose();
							TimingSpacerPen.Dispose();
							AlignmentPen.Dispose();
							FinderPen.Dispose();
							FormatPen.Dispose();
							FreePen.Dispose();
							BlankPen.Dispose();
							GridPen.Dispose();
							DarkSpotPen.Dispose();
						}
					#endregion
					}
				#endregion
				}
			}
		}
		catch
		{
			InvalidBarcode = true;
		}

		if( InvalidBarcode )
			owner.InvalidBarcode();
	}

#region Static Data
	private const byte INVALID = 0xff;
	private const byte __      = INVALID;

	internal static readonly byte[] CharacterEncodingTable =
	[
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		36, __, __, __, 37, 38, __, __, __, __, 39, 40, __, 41, 42, 43,
		00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 44, __, __, __, __, __,
		__, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
		25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __,
		__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __
	];

	// Version 7 to 40
	public static byte[][] Version7Information =
	[
		[r7, r7, r7, v7, v7, v7, v7, v7, r7, r7, v7, r7, r7, v7, r7, v7, r7, r7],
		[r7, r7, v7, r7, r7, r7, r7, v7, r7, v7, v7, r7, v7, v7, v7, v7, r7, r7],
		[r7, r7, v7, r7, r7, v7, v7, r7, v7, r7, v7, r7, r7, v7, v7, r7, r7, v7],
		[r7, r7, v7, r7, v7, r7, r7, v7, r7, r7, v7, v7, r7, v7, r7, r7, v7, v7],
		[r7, r7, v7, r7, v7, v7, v7, r7, v7, v7, v7, v7, v7, v7, r7, v7, v7, r7],
		[r7, r7, v7, v7, r7, r7, r7, v7, v7, v7, r7, v7, v7, r7, r7, r7, v7, r7],
		[r7, r7, v7, v7, r7, v7, v7, r7, r7, r7, r7, v7, r7, r7, r7, v7, v7, v7],
		[r7, r7, v7, v7, v7, r7, r7, v7, v7, r7, r7, r7, r7, r7, v7, v7, r7, v7],
		[r7, r7, v7, v7, v7, v7, v7, r7, r7, v7, r7, r7, v7, r7, v7, r7, r7, r7],
		[r7, v7, r7, r7, r7, r7, v7, r7, v7, v7, r7, v7, v7, v7, v7, r7, r7, r7],
		[r7, v7, r7, r7, r7, v7, r7, v7, r7, r7, r7, v7, r7, v7, v7, v7, r7, v7],
		[r7, v7, r7, r7, v7, r7, v7, r7, v7, r7, r7, r7, r7, v7, r7, v7, v7, v7],
		[r7, v7, r7, r7, v7, v7, r7, v7, r7, v7, r7, r7, v7, v7, r7, r7, v7, r7],
		[r7, v7, r7, v7, r7, r7, v7, r7, r7, v7, v7, r7, v7, r7, r7, v7, v7, r7],
		[r7, v7, r7, v7, r7, v7, r7, v7, v7, r7, v7, r7, r7, r7, r7, r7, v7, v7],
		[r7, v7, r7, v7, v7, r7, v7, r7, r7, r7, v7, v7, r7, r7, v7, r7, r7, v7],
		[r7, v7, r7, v7, v7, v7, r7, v7, v7, v7, v7, v7, v7, r7, v7, v7, r7, r7],
		[r7, v7, v7, r7, r7, r7, v7, v7, v7, r7, v7, v7, r7, r7, r7, v7, r7, r7],
		[r7, v7, v7, r7, r7, v7, r7, r7, r7, v7, v7, v7, v7, r7, r7, r7, r7, v7],
		[r7, v7, v7, r7, v7, r7, v7, v7, v7, v7, v7, r7, v7, r7, v7, r7, v7, v7],
		[r7, v7, v7, r7, v7, v7, r7, r7, r7, r7, v7, r7, r7, r7, v7, v7, v7, r7],
		[r7, v7, v7, v7, r7, r7, v7, v7, r7, r7, r7, r7, r7, v7, v7, r7, v7, r7],
		[r7, v7, v7, v7, r7, v7, r7, r7, v7, v7, r7, r7, v7, v7, v7, v7, v7, v7],
		[r7, v7, v7, v7, v7, r7, v7, v7, r7, v7, r7, v7, v7, v7, r7, v7, r7, v7],
		[r7, v7, v7, v7, v7, v7, r7, r7, v7, r7, r7, v7, r7, v7, r7, r7, r7, r7],
		[v7, r7, r7, r7, r7, r7, v7, r7, r7, v7, v7, v7, r7, v7, r7, v7, r7, v7],
		[v7, r7, r7, r7, r7, v7, r7, v7, v7, r7, v7, v7, v7, v7, r7, r7, r7, r7],
		[v7, r7, r7, r7, v7, r7, v7, r7, r7, r7, v7, r7, v7, v7, v7, r7, v7, r7],
		[v7, r7, r7, r7, v7, v7, r7, v7, v7, v7, v7, r7, r7, v7, v7, v7, v7, v7],
		[v7, r7, r7, v7, r7, r7, v7, r7, v7, v7, r7, r7, r7, r7, v7, r7, v7, v7],
		[v7, r7, r7, v7, r7, v7, r7, v7, r7, r7, r7, r7, v7, r7, v7, v7, v7, r7],
		[v7, r7, r7, v7, v7, r7, v7, r7, v7, r7, r7, v7, v7, r7, r7, v7, r7, r7],
		[v7, r7, r7, v7, v7, v7, r7, v7, r7, v7, r7, v7, r7, r7, r7, r7, r7, v7],
		[v7, r7, v7, r7, r7, r7, v7, v7, r7, r7, r7, v7, v7, r7, v7, r7, r7, v7]
	];

	private static readonly byte[] DebugNoMask = [r, r, r, r, r, r, r, r, r, r, r, r, r, r, r];

	// Ecc * 10 + Mask
	private static readonly Dictionary<ushort, byte[]> EccMaskFormat = new()
	                                                                   {
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 0, [f, f, f, r, f, f, f, f, f, r, r, r, f, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 1, [f, f, f, r, r, f, r, f, f, f, f, r, r, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 2, [f, f, f, f, f, r, f, f, r, f, r, f, r, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 3, [f, f, f, f, r, r, r, f, r, r, f, f, f, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 4, [f, f, r, r, f, f, r, r, r, f, r, f, f, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 5, [f, f, r, r, r, f, f, r, r, r, f, f, r, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 6, [f, f, r, f, f, r, r, r, f, r, r, r, r, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.LOW * 10 ) + 7, [f, f, r, f, r, r, f, r, f, f, f, r, f, f, r] },

		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 0, [f, r, f, r, f, r, r, r, r, r, f, r, r, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 1, [f, r, f, r, r, r, f, r, r, f, r, r, f, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 2, [f, r, f, f, f, f, r, r, f, f, f, f, f, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 3, [f, r, f, f, r, f, f, r, f, r, r, f, r, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 4, [f, r, r, r, f, r, f, f, f, f, f, f, r, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 5, [f, r, r, r, r, r, r, f, f, r, r, f, f, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 6, [f, r, r, f, f, f, f, f, r, r, f, r, f, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.MEDIUM * 10 ) + 7, [f, r, r, f, r, f, r, f, r, f, r, r, r, r, r] },

		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 0, [r, f, f, r, f, r, f, r, f, r, f, f, f, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 1, [r, f, f, r, r, r, r, r, f, f, r, f, r, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 2, [r, f, f, f, f, f, f, r, r, f, f, r, r, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 3, [r, f, f, f, r, f, r, r, r, r, r, r, f, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 4, [r, f, r, r, f, r, r, f, r, f, f, r, f, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 5, [r, f, r, r, r, r, f, f, r, r, r, r, r, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 6, [r, f, r, f, f, f, r, f, f, r, f, f, r, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.QUARTILE * 10 ) + 7, [r, f, r, f, r, f, f, f, f, f, r, f, f, r, f] },

		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 0, [r, r, f, r, f, f, r, f, r, r, r, f, r, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 1, [r, r, f, r, r, f, f, f, r, f, f, f, f, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 2, [r, r, f, f, f, r, r, f, f, f, r, r, f, f, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 3, [r, r, f, f, r, r, f, f, f, r, f, r, r, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 4, [r, r, r, r, f, f, f, r, f, f, r, r, r, f, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 5, [r, r, r, r, r, f, r, r, f, r, f, r, f, r, f] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 6, [r, r, r, f, f, r, f, r, r, r, r, f, f, r, r] },
		                                                                   { ( (ushort)QrProperties.ERROR_CORRECTION.HIGH * 10 ) + 7, [r, r, r, f, r, r, r, r, r, f, f, f, r, f, f] }
	                                                                   };

	private static readonly byte[][] AlignmentPlacement =
	[
		// Starts at Version 2
		[6, 18],
		[6, 22],
		[6, 26],
		[6, 30],
		[6, 34],
		[6, 22, 38],
		[6, 24, 42],
		[6, 26, 46],
		[6, 28, 50],
		[6, 30, 54],
		[6, 32, 58],
		[6, 34, 62],
		[6, 26, 46, 66],
		[6, 26, 48, 70],
		[6, 26, 50, 74],
		[6, 30, 54, 78],
		[6, 30, 56, 82],
		[6, 30, 58, 86],
		[6, 34, 62, 90],
		[6, 28, 50, 72, 94],
		[6, 26, 50, 74, 98],
		[6, 30, 54, 78, 102],
		[6, 28, 54, 80, 106],
		[6, 32, 58, 84, 110],
		[6, 30, 58, 86, 114],
		[6, 34, 62, 90, 118],
		[6, 26, 50, 74, 98, 122],
		[6, 30, 54, 78, 102, 126],
		[6, 26, 52, 78, 104, 130],
		[6, 30, 56, 82, 108, 134],
		[6, 34, 60, 86, 112, 138],
		[6, 30, 58, 86, 114, 142],
		[6, 34, 62, 90, 118, 146],
		[6, 30, 54, 78, 102, 126, 150],
		[6, 24, 50, 76, 102, 128, 154],
		[6, 28, 54, 80, 106, 132, 158],
		[6, 32, 58, 84, 110, 136, 162],
		[6, 26, 54, 82, 110, 138, 166],
		[6, 30, 58, 86, 114, 142, 170]
	];

	public static readonly byte[][] FinderPattern =
	[
		[F, F, F, F, F, F, F],
		[F, s, s, s, s, s, F],
		[F, s, F, F, F, s, F],
		[F, s, F, F, F, s, F],
		[F, s, F, F, F, s, F],
		[F, s, s, s, s, s, F],
		[F, F, F, F, F, F, F]
	];

	public static readonly byte[][] AlignmentPattern =
	[
		[A, A, A, A, A],
		[A, s, s, s, A],
		[A, s, A, s, A],
		[A, s, s, s, A],
		[A, A, A, A, A]
	];

	private static readonly byte[] ExponentToInteger =
	[
		1, 2, 4, 8, 16, 32, 64, 128, 29, 58, 116, 232, 205, 135, 19, 38,
		76, 152, 45, 90, 180, 117, 234, 201, 143, 3, 6, 12, 24, 48, 96, 192,
		157, 39, 78, 156, 37, 74, 148, 53, 106, 212, 181, 119, 238, 193, 159, 35,
		70, 140, 5, 10, 20, 40, 80, 160, 93, 186, 105, 210, 185, 111, 222, 161,
		95, 190, 97, 194, 153, 47, 94, 188, 101, 202, 137, 15, 30, 60, 120, 240,
		253, 231, 211, 187, 107, 214, 177, 127, 254, 225, 223, 163, 91, 182, 113, 226,
		217, 175, 67, 134, 17, 34, 68, 136, 13, 26, 52, 104, 208, 189, 103, 206,
		129, 31, 62, 124, 248, 237, 199, 147, 59, 118, 236, 197, 151, 51, 102, 204,
		133, 23, 46, 92, 184, 109, 218, 169, 79, 158, 33, 66, 132, 21, 42, 84,
		168, 77, 154, 41, 82, 164, 85, 170, 73, 146, 57, 114, 228, 213, 183, 115,
		230, 209, 191, 99, 198, 145, 63, 126, 252, 229, 215, 179, 123, 246, 241, 255,
		227, 219, 171, 75, 150, 49, 98, 196, 149, 55, 110, 220, 165, 87, 174, 65,
		130, 25, 50, 100, 200, 141, 7, 14, 28, 56, 112, 224, 221, 167, 83, 166,
		81, 162, 89, 178, 121, 242, 249, 239, 195, 155, 43, 86, 172, 69, 138, 9,
		18, 36, 72, 144, 61, 122, 244, 245, 247, 243, 251, 235, 203, 139, 11, 22,
		44, 88, 176, 125, 250, 233, 207, 131, 27, 54, 108, 216, 173, 71, 142, 1
	];

	private static readonly byte[] IntegerToExponent =
	[
		_, 0, 1, 25, 2, 50, 26, 198, 3, 223, 51, 238, 27, 104, 199, 75, // Zero not used
		4, 100, 224, 14, 52, 141, 239, 129, 28, 193, 105, 248, 200, 8, 76, 113,
		5, 138, 101, 47, 225, 36, 15, 33, 53, 147, 142, 218, 240, 18, 130, 69,
		29, 181, 194, 125, 106, 39, 249, 185, 201, 154, 9, 120, 77, 228, 114, 166,
		6, 191, 139, 98, 102, 221, 48, 253, 226, 152, 37, 179, 16, 145, 34, 136,
		54, 208, 148, 206, 143, 150, 219, 189, 241, 210, 19, 92, 131, 56, 70, 64,
		30, 66, 182, 163, 195, 72, 126, 110, 107, 58, 40, 84, 250, 133, 186, 61,
		202, 94, 155, 159, 10, 21, 121, 43, 78, 212, 229, 172, 115, 243, 167, 87,
		7, 112, 192, 247, 140, 128, 99, 13, 103, 74, 222, 237, 49, 197, 254, 24,
		227, 165, 153, 119, 38, 184, 180, 124, 17, 68, 146, 217, 35, 32, 137, 46,
		55, 63, 209, 91, 149, 188, 207, 205, 144, 135, 151, 178, 220, 252, 190, 97,
		242, 86, 211, 171, 20, 42, 93, 158, 132, 60, 57, 83, 71, 109, 65, 162,
		31, 45, 67, 216, 183, 123, 164, 118, 196, 23, 73, 236, 127, 12, 111, 246,
		108, 161, 59, 82, 41, 157, 85, 170, 251, 96, 134, 177, 187, 204, 62, 90,
		203, 89, 95, 176, 156, 169, 160, 81, 11, 245, 22, 235, 122, 117, 44, 215,
		79, 174, 213, 233, 230, 231, 173, 232, 116, 214, 244, 234, 168, 80, 88, 175
	];

	private static readonly byte[] Generator7 =
	[
		87, 229, 146, 149, 238, 102, 21
	];

	private static readonly byte[] Generator10 =
	[
		251, 67, 46, 61, 118, 70, 64, 94, 32, 45
	];

	private static readonly byte[] Generator13 =
	[
		74, 152, 176, 100, 86, 100, 106, 104, 130, 218, 206, 140, 78
	];

	private static readonly byte[] Generator15 =
	[
		8, 183, 61, 91, 202, 37, 51, 58, 58, 237, 140, 124, 5, 99, 105
	];

	private static readonly byte[] Generator16 =
	[
		120, 104, 107, 109, 102, 161, 76, 3, 91, 191, 147, 169, 182, 194, 225, 120
	];

	private static readonly byte[] Generator17 =
	[
		43, 139, 206, 78, 43, 239, 123, 206, 214, 147, 24, 99, 150, 39, 243, 163,
		136
	];

	private static readonly byte[] Generator18 =
	[
		215, 234, 158, 94, 184, 97, 118, 170, 79, 187, 152, 148, 252, 179, 5, 98,
		96, 153
	];

	private static readonly byte[] Generator20 =
	[
		17, 60, 79, 50, 61, 163, 26, 187, 202, 180, 221, 225, 83, 239, 156, 164,
		212, 212, 188, 190
	];

	private static readonly byte[] Generator22 =
	[
		210, 171, 247, 242, 93, 230, 14, 109, 221, 53, 200, 74, 8, 172, 98, 80,
		219, 134, 160, 105, 165, 231
	];

	private static readonly byte[] Generator24 =
	[
		229, 121, 135, 48, 211, 117, 251, 126, 159, 180, 169, 152, 192, 226, 228, 218,
		111, 0, 117, 232, 87, 96, 227, 21
	];

	private static readonly byte[] Generator26 =
	[
		173, 125, 158, 2, 103, 182, 118, 17, 145, 201, 111, 28, 165, 53, 161, 21,
		245, 142, 13, 102, 48, 227, 153, 145, 218, 70
	];

	private static readonly byte[] Generator28 =
	[
		168, 223, 200, 104, 224, 234, 108, 180, 110, 190, 195, 147, 205, 27, 232, 201,
		21, 43, 245, 87, 42, 195, 212, 119, 242, 37, 9, 123
	];

	private static readonly byte[] Generator30 =
	[
		41, 173, 145, 152, 216, 31, 179, 182, 50, 48, 110, 86, 239, 96, 222, 125,
		42, 173, 226, 193, 224, 130, 156, 37, 251, 216, 238, 40, 192, 180
	];

	private static readonly byte[] Generator32 =
	[
		10, 6, 106, 190, 249, 167, 4, 67, 209, 138, 138, 32, 242, 123, 89, 27,
		120, 185, 80, 156, 38, 60, 171, 60, 28, 222, 80, 52, 254, 185, 220, 241
	];

	private static readonly byte[] Generator34 =
	[
		111, 77, 146, 94, 26, 21, 108, 19, 105, 94, 113, 193, 86, 140, 163, 125,
		58, 158, 229, 239, 218, 103, 56, 70, 114, 61, 183, 129, 167, 13, 98, 62,
		129, 51
	];

	private static readonly byte[] Generator36 =
	[
		200, 183, 98, 16, 172, 31, 246, 234, 60, 152, 115, 0, 167, 152, 113, 248,
		238, 107, 18, 63, 218, 37, 87, 210, 105, 177, 120, 74, 121, 196, 117, 251,
		113, 233, 30, 120
	];

	private static readonly byte[] Generator40 =
	[
		59, 116, 79, 161, 252, 98, 128, 205, 128, 161, 247, 57, 163, 56, 235, 106,
		53, 26, 187, 174, 226, 104, 170, 7, 175, 35, 181, 114, 88, 41, 47, 163,
		125, 134, 72, 20, 232, 53, 35, 15
	];

	private static readonly byte[] Generator42 =
	[
		250, 103, 221, 230, 25, 18, 137, 231, 0, 3, 58, 242, 221, 191, 110, 84,
		230, 8, 188, 106, 96, 147, 15, 131, 139, 34, 101, 223, 39, 101, 213, 199,
		237, 254, 201, 123, 171, 162, 194, 117, 50, 96
	];

	private static readonly byte[] Generator44 =
	[
		190, 7, 61, 121, 71, 246, 69, 55, 168, 188, 89, 243, 191, 25, 72, 123,
		9, 145, 14, 247, 1, 238, 44, 78, 143, 62, 224, 126, 118, 114, 68, 163,
		52, 194, 217, 147, 204, 169, 37, 130, 113, 102, 73, 181
	];

	private static readonly byte[] Generator46 =
	[
		112, 94, 88, 112, 253, 224, 202, 115, 187, 99, 89, 5, 54, 113, 129, 44,
		58, 16, 135, 216, 169, 211, 36, 1, 4, 96, 60, 241, 73, 104, 234, 8,
		249, 245, 119, 174, 52, 25, 157, 224, 43, 202, 223, 19, 82, 15
	];

	private static readonly byte[] Generator48 =
	[
		228, 25, 196, 130, 211, 146, 60, 24, 251, 90, 39, 102, 240, 61, 178, 63,
		46, 123, 115, 18, 221, 111, 135, 160, 182, 205, 107, 206, 95, 150, 120, 184,
		91, 21, 247, 156, 140, 238, 191, 11, 94, 227, 84, 50, 163, 39, 34, 108
	];

	private static readonly byte[] Generator50 =
	[
		232, 125, 157, 161, 164, 9, 118, 46, 209, 99, 203, 193, 35, 3, 209, 111,
		195, 242, 203, 225, 46, 13, 32, 160, 126, 209, 130, 160, 242, 215, 242, 75,
		77, 42, 189, 32, 113, 65, 124, 69, 228, 114, 235, 175, 124, 170, 215, 232,
		133, 205
	];

	private static readonly byte[] Generator52 =
	[
		116, 50, 86, 186, 50, 220, 251, 89, 192, 46, 86, 127, 124, 19, 184, 233,
		151, 215, 22, 14, 59, 145, 37, 242, 203, 134, 254, 89, 190, 94, 59, 65,
		124, 113, 100, 233, 235, 121, 22, 76, 86, 97, 39, 242, 200, 220, 101, 33,
		239, 254, 116, 51
	];

	private static readonly byte[] Generator54 =
	[
		183, 26, 201, 84, 210, 221, 113, 21, 46, 65, 45, 50, 238, 184, 249, 225,
		102, 58, 209, 218, 109, 165, 26, 95, 184, 192, 52, 245, 35, 254, 238, 175,
		172, 79, 123, 25, 122, 43, 120, 108, 215, 80, 128, 201, 235, 8, 153, 59,
		101, 31, 198, 76, 31, 156
	];

	private static readonly byte[] Generator56 =
	[
		106, 120, 107, 157, 164, 216, 112, 116, 2, 91, 248, 163, 36, 201, 202, 229,
		6, 144, 254, 155, 135, 208, 170, 209, 12, 139, 127, 142, 182, 249, 177, 174,
		190, 28, 10, 85, 239, 184, 101, 124, 152, 206, 96, 23, 163, 61, 27, 196,
		247, 151, 154, 202, 207, 20, 61, 10
	];

	private static readonly byte[] Generator58 =
	[
		82, 116, 26, 247, 66, 27, 62, 107, 252, 182, 200, 185, 235, 55, 251, 242,
		210, 144, 154, 237, 176, 141, 192, 248, 152, 249, 206, 85, 253, 142, 65, 165,
		125, 23, 24, 30, 122, 240, 214, 6, 129, 218, 29, 145, 127, 134, 206, 245,
		117, 29, 41, 63, 159, 142, 233, 125, 148, 123
	];

	private static readonly byte[] Generator60 =
	[
		107, 140, 26, 12, 9, 141, 243, 197, 226, 197, 219, 45, 211, 101, 219, 120,
		28, 181, 127, 6, 100, 247, 2, 205, 198, 57, 115, 219, 101, 109, 160, 82,
		37, 38, 238, 49, 160, 209, 121, 86, 11, 124, 30, 181, 84, 25, 194, 87,
		65, 102, 190, 220, 70, 27, 209, 16, 89, 7, 33, 240
	];

	private static readonly byte[] Generator62 =
	[
		65, 202, 113, 98, 71, 223, 248, 118, 214, 94, 0, 122, 37, 23, 2, 228,
		58, 121, 7, 105, 135, 78, 243, 118, 70, 76, 223, 89, 72, 50, 70, 111,
		194, 17, 212, 126, 181, 35, 221, 117, 235, 11, 229, 149, 147, 123, 213, 40,
		115, 6, 200, 100, 26, 246, 182, 218, 127, 215, 36, 186, 110, 106
	];

	private static readonly byte[] Generator64 =
	[
		45, 51, 175, 9, 7, 158, 159, 49, 68, 119, 92, 123, 177, 204, 187, 254,
		200, 78, 141, 149, 119, 26, 127, 53, 160, 93, 199, 212, 29, 24, 145, 156,
		208, 150, 218, 209, 4, 216, 91, 47, 184, 146, 47, 140, 195, 195, 125, 242,
		238, 63, 99, 108, 140, 230, 242, 31, 204, 11, 178, 243, 217, 156, 213, 231
	];

	private static readonly byte[] Generator66 =
	[
		5, 118, 222, 180, 136, 136, 162, 51, 46, 117, 13, 215, 81, 17, 139, 247,
		197, 171, 95, 173, 65, 137, 178, 68, 111, 95, 101, 41, 72, 214, 169, 197,
		95, 7, 44, 154, 77, 111, 236, 40, 121, 143, 63, 87, 80, 253, 240, 126,
		217, 77, 34, 232, 106, 50, 168, 82, 76, 146, 67, 106, 171, 25, 132, 93,
		45, 105
	];

	private static readonly byte[] Generator68 =
	[
		247, 159, 223, 33, 224, 93, 77, 70, 90, 160, 32, 254, 43, 150, 84, 101,
		190, 205, 133, 52, 60, 202, 165, 220, 203, 151, 93, 84, 15, 84, 253, 173,
		160, 89, 227, 52, 199, 97, 95, 231, 52, 177, 41, 125, 137, 241, 166, 225,
		118, 2, 54, 32, 82, 215, 175, 198, 43, 238, 235, 27, 101, 184, 127, 3,
		5, 8, 163, 238
	];


	private static readonly Dictionary<byte, byte[]> GeneratorPolynomials = new()
	                                                                        {
		                                                                        { 7, Generator7 },
		                                                                        { 10, Generator10 },
		                                                                        { 13, Generator13 },
		                                                                        { 15, Generator15 },
		                                                                        { 16, Generator16 },
		                                                                        { 17, Generator17 },
		                                                                        { 18, Generator18 },
		                                                                        { 20, Generator20 },
		                                                                        { 22, Generator22 },
		                                                                        { 24, Generator24 },
		                                                                        { 26, Generator26 },
		                                                                        { 28, Generator28 },
		                                                                        { 30, Generator30 },
		                                                                        { 32, Generator32 },
		                                                                        { 34, Generator34 },
		                                                                        { 36, Generator36 },
		                                                                        { 40, Generator40 },
		                                                                        { 42, Generator42 },
		                                                                        { 44, Generator44 },
		                                                                        { 46, Generator46 },
		                                                                        { 48, Generator48 },
		                                                                        { 50, Generator50 },
		                                                                        { 52, Generator52 },
		                                                                        { 54, Generator54 },
		                                                                        { 56, Generator56 },
		                                                                        { 58, Generator58 },
		                                                                        { 60, Generator60 },
		                                                                        { 62, Generator62 },
		                                                                        { 64, Generator64 },
		                                                                        { 66, Generator66 },
		                                                                        { 68, Generator68 }
	                                                                        };

	// Version * 10 + ERROR_CORRECTION
	private static readonly Dictionary<ushort,
			(
			ushort BitLimit,
			ushort TotalCodeWords,
			byte EccCodewordsPerBlock,
			byte NumberOfBlocksInGroup1,
			byte NumberOfDataCodewordsInInGroup1Block,
			byte NumberOfBlocksInInGroup2,
			byte NumberOfDataCodewordsInInGroup2Block,
			ushort NumericLimit,
			ushort AlphaNumericLimit,
			ushort BinaryLimit
			)>
		ErrorCorrectionCodeWordBlockInformation = new()
		                                          {
			                                          { ( 1 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 152, 19, 7, 1, 19, 0, 0, 41, 25, 17 ) },
			                                          { ( 1 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 128, 16, 10, 1, 16, 0, 0, 34, 20, 14 ) },
			                                          { ( 1 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 104, 13, 13, 1, 13, 0, 0, 27, 16, 11 ) },
			                                          { ( 1 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 72, 9, 17, 1, 9, 0, 0, 17, 10, 7 ) },

			                                          { ( 2 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 272, 34, 10, 1, 34, 0, 0, 77, 47, 32 ) },
			                                          { ( 2 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 224, 28, 16, 1, 28, 0, 0, 63, 38, 26 ) },
			                                          { ( 2 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 176, 22, 22, 1, 22, 0, 0, 48, 29, 20 ) },
			                                          { ( 2 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 128, 16, 28, 1, 16, 0, 0, 34, 20, 14 ) },

			                                          { ( 3 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 440, 55, 15, 1, 55, 0, 0, 127, 77, 53 ) },
			                                          { ( 3 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 352, 44, 26, 1, 44, 0, 0, 101, 61, 42 ) },
			                                          { ( 3 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 272, 34, 18, 2, 17, 0, 0, 77, 47, 32 ) },
			                                          { ( 3 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 208, 26, 22, 2, 13, 0, 0, 58, 35, 24 ) },

			                                          { ( 4 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 640, 80, 20, 1, 80, 0, 0, 187, 114, 78 ) },
			                                          { ( 4 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 512, 64, 18, 2, 32, 0, 0, 149, 90, 62 ) },
			                                          { ( 4 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 384, 48, 26, 2, 24, 0, 0, 111, 67, 46 ) },
			                                          { ( 4 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 288, 36, 16, 4, 9, 0, 0, 82, 50, 34 ) },

			                                          { ( 5 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 864, 108, 26, 1, 108, 0, 0, 255, 154, 106 ) },
			                                          { ( 5 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 688, 86, 24, 2, 43, 0, 0, 202, 122, 84 ) },
			                                          { ( 5 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 496, 62, 18, 2, 15, 2, 16, 144, 87, 60 ) },
			                                          { ( 5 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 368, 46, 22, 2, 11, 2, 12, 106, 64, 44 ) },

			                                          { ( 6 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 1088, 136, 18, 2, 68, 0, 0, 322, 195, 134 ) },
			                                          { ( 6 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 864, 108, 16, 4, 27, 0, 0, 255, 154, 106 ) },
			                                          { ( 6 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 608, 76, 24, 4, 19, 0, 0, 178, 108, 74 ) },
			                                          { ( 6 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 480, 60, 28, 4, 15, 0, 0, 139, 84, 58 ) },

			                                          { ( 7 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 1248, 156, 20, 2, 78, 0, 0, 370, 224, 154 ) },
			                                          { ( 7 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 992, 124, 18, 4, 31, 0, 0, 293, 178, 122 ) },
			                                          { ( 7 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 704, 88, 18, 2, 14, 4, 15, 207, 125, 86 ) },
			                                          { ( 7 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 528, 66, 26, 4, 13, 1, 14, 154, 93, 64 ) },

			                                          { ( 8 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 1552, 194, 24, 2, 97, 0, 0, 461, 279, 192 ) },
			                                          { ( 8 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 1232, 154, 22, 2, 38, 2, 39, 365, 221, 152 ) },
			                                          { ( 8 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 880, 110, 22, 4, 18, 2, 19, 259, 157, 108 ) },
			                                          { ( 8 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 688, 86, 26, 4, 14, 2, 15, 202, 122, 84 ) },

			                                          { ( 9 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 1856, 232, 30, 2, 116, 0, 0, 552, 335, 230 ) },
			                                          { ( 9 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 1456, 182, 22, 3, 36, 2, 37, 432, 262, 180 ) },
			                                          { ( 9 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 1056, 132, 20, 4, 16, 4, 17, 312, 189, 130 ) },
			                                          { ( 9 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 800, 100, 24, 4, 12, 4, 13, 235, 143, 98 ) },

			                                          { ( 10 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 2192, 274, 18, 2, 68, 2, 69, 652, 395, 271 ) },
			                                          { ( 10 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 1728, 216, 26, 4, 43, 1, 44, 513, 311, 213 ) },
			                                          { ( 10 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 1232, 154, 24, 6, 19, 2, 20, 364, 221, 151 ) },
			                                          { ( 10 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 976, 122, 28, 6, 15, 2, 16, 288, 174, 119 ) },

			                                          { ( 11 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 2592, 324, 20, 4, 81, 0, 0, 772, 468, 321 ) },
			                                          { ( 11 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 2032, 254, 30, 1, 50, 4, 51, 604, 366, 251 ) },
			                                          { ( 11 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 1440, 180, 28, 4, 22, 4, 23, 427, 259, 177 ) },
			                                          { ( 11 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 1120, 140, 24, 3, 12, 8, 13, 331, 200, 137 ) },

			                                          { ( 12 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 2960, 370, 24, 2, 92, 2, 93, 883, 535, 367 ) },
			                                          { ( 12 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 2320, 290, 22, 6, 36, 2, 37, 691, 419, 287 ) },
			                                          { ( 12 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 1648, 206, 26, 4, 20, 6, 21, 489, 296, 203 ) },
			                                          { ( 12 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 1264, 158, 28, 7, 14, 4, 15, 374, 227, 155 ) },

			                                          { ( 13 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 3424, 428, 26, 4, 107, 0, 0, 1022, 619, 425 ) },
			                                          { ( 13 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 2672, 334, 22, 8, 37, 1, 38, 796, 483, 331 ) },
			                                          { ( 13 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 1952, 244, 24, 8, 20, 4, 21, 580, 352, 241 ) },
			                                          { ( 13 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 1440, 180, 22, 12, 11, 4, 12, 427, 259, 177 ) },

			                                          { ( 14 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 3688, 461, 30, 3, 115, 1, 116, 1101, 667, 458 ) },
			                                          { ( 14 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 2920, 365, 24, 4, 40, 5, 41, 871, 528, 362 ) },
			                                          { ( 14 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 2088, 261, 20, 11, 16, 5, 17, 621, 376, 258 ) },
			                                          { ( 14 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 1576, 197, 24, 11, 12, 5, 13, 468, 283, 194 ) },

			                                          { ( 15 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 4184, 523, 22, 5, 87, 1, 88, 1250, 758, 520 ) },
			                                          { ( 15 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 3320, 415, 24, 5, 41, 5, 42, 991, 600, 412 ) },
			                                          { ( 15 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 2360, 295, 30, 5, 24, 7, 25, 703, 426, 292 ) },
			                                          { ( 15 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 1784, 223, 24, 11, 12, 7, 13, 530, 321, 220 ) },

			                                          { ( 16 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 4712, 589, 24, 5, 98, 1, 99, 1408, 854, 586 ) },
			                                          { ( 16 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 3624, 453, 28, 7, 45, 3, 46, 1082, 656, 450 ) },
			                                          { ( 16 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 2600, 325, 24, 15, 19, 2, 20, 775, 470, 322 ) },
			                                          { ( 16 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 2024, 253, 30, 3, 15, 13, 16, 602, 365, 250 ) },

			                                          { ( 17 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 5176, 647, 28, 1, 107, 5, 108, 1548, 938, 644 ) },
			                                          { ( 17 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 4056, 507, 28, 10, 46, 1, 47, 1212, 734, 504 ) },
			                                          { ( 17 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 2936, 367, 28, 1, 22, 15, 23, 876, 531, 364 ) },
			                                          { ( 17 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 2264, 283, 28, 2, 14, 17, 15, 674, 408, 280 ) },

			                                          { ( 18 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 5768, 721, 30, 5, 120, 1, 121, 1725, 1046, 718 ) },
			                                          { ( 18 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 4504, 563, 26, 9, 43, 4, 44, 1346, 816, 560 ) },
			                                          { ( 18 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 3176, 397, 28, 17, 22, 1, 23, 948, 574, 394 ) },
			                                          { ( 18 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 2504, 313, 28, 2, 14, 19, 15, 746, 452, 310 ) },

			                                          { ( 19 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 6360, 795, 28, 3, 113, 4, 114, 1903, 1153, 792 ) },
			                                          { ( 19 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 5016, 627, 26, 3, 44, 11, 45, 1500, 909, 624 ) },
			                                          { ( 19 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 3560, 445, 26, 17, 21, 4, 22, 1063, 644, 442 ) },
			                                          { ( 19 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 2728, 341, 26, 9, 13, 16, 14, 813, 493, 338 ) },

			                                          { ( 20 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 6888, 861, 28, 3, 107, 5, 108, 2061, 1249, 858 ) },
			                                          { ( 20 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 5352, 669, 26, 3, 41, 13, 42, 1600, 970, 666 ) },
			                                          { ( 20 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 3880, 485, 30, 15, 24, 5, 25, 1159, 702, 482 ) },
			                                          { ( 20 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 3080, 385, 28, 15, 15, 10, 16, 919, 557, 382 ) },

			                                          { ( 21 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 7456, 932, 28, 4, 116, 4, 117, 2232, 1352, 929 ) },
			                                          { ( 21 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 5712, 714, 26, 17, 42, 0, 0, 1708, 1035, 711 ) },
			                                          { ( 21 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 4096, 512, 28, 17, 22, 6, 23, 1224, 742, 509 ) },
			                                          { ( 21 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 3248, 406, 30, 19, 16, 6, 17, 969, 587, 403 ) },

			                                          { ( 22 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 8048, 1006, 28, 2, 111, 7, 112, 2409, 1460, 1003 ) },
			                                          { ( 22 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 6256, 782, 28, 17, 46, 0, 0, 1872, 1134, 779 ) },
			                                          { ( 22 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 4544, 568, 30, 7, 24, 16, 25, 1358, 823, 565 ) },
			                                          { ( 22 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 3536, 442, 24, 34, 13, 0, 0, 1056, 640, 439 ) },

			                                          { ( 23 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 8752, 1094, 30, 4, 121, 5, 122, 2620, 1588, 1091 ) },
			                                          { ( 23 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 6880, 860, 28, 4, 47, 14, 48, 2059, 1248, 857 ) },
			                                          { ( 23 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 4912, 614, 30, 11, 24, 14, 25, 1468, 890, 611 ) },
			                                          { ( 23 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 3712, 464, 30, 16, 15, 14, 16, 1108, 672, 461 ) },

			                                          { ( 24 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 9392, 1174, 30, 6, 117, 4, 118, 2812, 1704, 1171 ) },
			                                          { ( 24 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 7312, 914, 28, 6, 45, 14, 46, 2188, 1326, 911 ) },
			                                          { ( 24 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 5312, 664, 30, 11, 24, 16, 25, 1588, 963, 661 ) },
			                                          { ( 24 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 4112, 514, 30, 30, 16, 2, 17, 1228, 744, 511 ) },

			                                          { ( 25 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 10208, 1276, 26, 8, 106, 4, 107, 3057, 1853, 1273 ) },
			                                          { ( 25 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 8000, 1000, 28, 8, 47, 13, 48, 2395, 1451, 997 ) },
			                                          { ( 25 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 5744, 718, 30, 7, 24, 22, 25, 1718, 1041, 715 ) },
			                                          { ( 25 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 4304, 538, 30, 22, 15, 13, 16, 1286, 779, 535 ) },

			                                          { ( 26 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 10960, 1370, 28, 10, 114, 2, 115, 3283, 1990, 1367 ) },
			                                          { ( 26 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 8496, 1062, 28, 19, 46, 4, 47, 2544, 1542, 1059 ) },
			                                          { ( 26 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 6032, 754, 28, 28, 22, 6, 23, 1804, 1094, 751 ) },
			                                          { ( 26 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 4768, 596, 30, 33, 16, 4, 17, 1425, 864, 593 ) },

			                                          { ( 27 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 11744, 1468, 30, 8, 122, 4, 123, 3517, 2132, 1465 ) },
			                                          { ( 27 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 9024, 1128, 28, 22, 45, 3, 46, 2701, 1637, 1125 ) },
			                                          { ( 27 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 6464, 808, 30, 8, 23, 26, 24, 1933, 1172, 805 ) },
			                                          { ( 27 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 5024, 628, 30, 12, 15, 28, 16, 1501, 910, 625 ) },

			                                          { ( 28 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 12248, 1531, 30, 3, 117, 10, 118, 3669, 2223, 1528 ) },
			                                          { ( 28 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 9544, 1193, 28, 3, 45, 23, 46, 2857, 1732, 1190 ) },
			                                          { ( 28 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 6968, 871, 30, 4, 24, 31, 25, 2085, 1263, 868 ) },
			                                          { ( 28 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 5288, 661, 30, 11, 15, 31, 16, 1581, 958, 658 ) },

			                                          { ( 29 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 13048, 1631, 30, 7, 116, 7, 117, 3909, 2369, 1628 ) },
			                                          { ( 29 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 10136, 1267, 28, 21, 45, 7, 46, 3035, 1839, 1264 ) },
			                                          { ( 29 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 7288, 911, 30, 1, 23, 37, 24, 2181, 1322, 908 ) },
			                                          { ( 29 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 5608, 701, 30, 19, 15, 26, 16, 1677, 1016, 698 ) },

			                                          { ( 30 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 13880, 1735, 30, 5, 115, 10, 116, 4158, 2520, 1732 ) },
			                                          { ( 30 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 10984, 1373, 28, 19, 47, 10, 48, 3289, 1994, 1370 ) },
			                                          { ( 30 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 7880, 985, 30, 15, 24, 25, 25, 2358, 1429, 982 ) },
			                                          { ( 30 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 5960, 745, 30, 23, 15, 25, 16, 1782, 1080, 742 ) },

			                                          { ( 31 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 14744, 1843, 30, 13, 115, 3, 116, 4417, 2677, 1840 ) },
			                                          { ( 31 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 11640, 1455, 28, 2, 46, 29, 47, 3486, 2113, 1452 ) },
			                                          { ( 31 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 8264, 1033, 30, 42, 24, 1, 25, 2473, 1499, 1030 ) },
			                                          { ( 31 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 6344, 793, 30, 23, 15, 28, 16, 1897, 1150, 790 ) },

			                                          { ( 32 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 15640, 1955, 30, 17, 115, 0, 0, 4686, 2840, 1952 ) },
			                                          { ( 32 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 12328, 1541, 28, 10, 46, 23, 47, 3693, 2238, 1538 ) },
			                                          { ( 32 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 8920, 1115, 30, 10, 24, 35, 25, 2670, 1618, 1112 ) },
			                                          { ( 32 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 6760, 845, 30, 19, 15, 35, 16, 2022, 1226, 842 ) },

			                                          { ( 33 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 16568, 2071, 30, 17, 115, 1, 116, 4965, 3009, 2068 ) },
			                                          { ( 33 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 13048, 1631, 28, 14, 46, 21, 47, 3909, 2369, 1628 ) },
			                                          { ( 33 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 9368, 1171, 30, 29, 24, 19, 25, 2805, 1700, 1168 ) },
			                                          { ( 33 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 7208, 901, 30, 11, 15, 46, 16, 2157, 1307, 898 ) },

			                                          { ( 34 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 17528, 2191, 30, 13, 115, 6, 116, 5253, 3183, 2188 ) },
			                                          { ( 34 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 13800, 1725, 28, 14, 46, 23, 47, 4134, 2506, 1722 ) },
			                                          { ( 34 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 9848, 1231, 30, 44, 24, 7, 25, 2949, 1787, 1228 ) },
			                                          { ( 34 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 7688, 961, 30, 59, 16, 1, 17, 2301, 1394, 958 ) },

			                                          { ( 35 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 18448, 2306, 30, 12, 121, 7, 122, 5529, 3351, 2303 ) },
			                                          { ( 35 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 14496, 1812, 28, 12, 47, 26, 48, 4343, 2632, 1809 ) },
			                                          { ( 35 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 10288, 1286, 30, 39, 24, 14, 25, 3081, 1867, 1283 ) },
			                                          { ( 35 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 7888, 986, 30, 22, 15, 41, 16, 2361, 1431, 983 ) },

			                                          { ( 36 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 19472, 2434, 30, 6, 121, 14, 122, 5836, 3537, 2431 ) },
			                                          { ( 36 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 15312, 1914, 28, 6, 47, 34, 48, 4588, 2780, 1911 ) },
			                                          { ( 36 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 10832, 1354, 30, 46, 24, 10, 25, 3244, 1966, 1351 ) },
			                                          { ( 36 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 8432, 1054, 30, 2, 15, 64, 16, 2524, 1530, 1051 ) },

			                                          { ( 37 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 20528, 2566, 30, 17, 122, 4, 123, 6153, 3729, 2563 ) },
			                                          { ( 37 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 15936, 1992, 28, 29, 46, 14, 47, 4775, 2894, 1989 ) },
			                                          { ( 37 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 11408, 1426, 30, 49, 24, 10, 25, 3417, 2071, 1423 ) },
			                                          { ( 37 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 8768, 1096, 30, 24, 15, 46, 16, 2625, 1591, 1093 ) },

			                                          { ( 38 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 21616, 2702, 30, 4, 122, 18, 123, 6479, 3927, 2699 ) },
			                                          { ( 38 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 16816, 2102, 28, 13, 46, 32, 47, 5039, 3054, 2099 ) },
			                                          { ( 38 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 12016, 1502, 30, 48, 24, 14, 25, 3599, 2181, 1499 ) },
			                                          { ( 38 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 9136, 1142, 30, 42, 15, 32, 16, 2735, 1658, 1139 ) },

			                                          { ( 39 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 22496, 2812, 30, 20, 117, 4, 118, 6743, 4087, 2809 ) },
			                                          { ( 39 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 17728, 2216, 28, 40, 47, 7, 48, 5313, 3220, 2213 ) },
			                                          { ( 39 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 12656, 1582, 30, 43, 24, 22, 25, 3791, 2298, 1579 ) },
			                                          { ( 39 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 9776, 1222, 30, 10, 15, 67, 16, 2927, 1774, 1219 ) },

			                                          { ( 40 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.LOW, ( 23648, 2956, 30, 19, 118, 6, 119, 7089, 4296, 2953 ) },
			                                          { ( 40 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.MEDIUM, ( 18672, 2334, 28, 18, 47, 31, 48, 5596, 3391, 2331 ) },
			                                          { ( 40 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.QUARTILE, ( 13328, 1666, 30, 34, 24, 34, 25, 3993, 2420, 1663 ) },
			                                          { ( 40 * 10 ) + (ushort)QrProperties.ERROR_CORRECTION.HIGH, ( 10208, 1276, 30, 20, 15, 61, 16, 3057, 1852, 1273 ) }
		                                          };
#endregion
}