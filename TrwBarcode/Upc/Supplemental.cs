﻿#nullable enable
#pragma warning disable CA1416

namespace TBarcode;

public partial class TrwBarcode
{
	private bool DrawSupplemental( int quietZone )
	{
		var Ok   = true;
		var Code = "";

		void DrawChar2( char c, PARITY par )
		{
			var Pa = par == PARITY.EVEN ? UPCE_EVEN : UPCE_ODD;

			foreach( var B in Pa[ c - '0' ] )
			{
				if( B == 1 )
					DrawLine();
				MovePen();
			}
		}

		int CalcNdx( string code )
		{
			var Len = code.Length;

			var Odd = 0;

			for( var I = Len - 1; I >= 0; I -= 2 )
				Odd += code[ I ] - '0';

			var Even = 0;

			for( var I = Len - 2; I >= 0; I -= 2 )
				Even += code[ I ] - '0';

			return ( Odd * 3 + Even * 9 ) % 10;
		}

		MovePenNarrow( quietZone * 2 );

		var BarcodeStart = GetOffset();

		switch( Supplement )
		{
		case SUPPLEMENT.SUPP2:
			Code = SupplementalText.StripCode( 2 );
			int Value;

			if( Code == "" )
			{
				Ok    = false;
				Value = 0;
			}
			else if( !int.TryParse( Code, out Value ) )
			{
				Ok    = false;
				Value = 0;
			}

			var ParityA = SUP2[ Value & 3 ];
			Do1011();

			void Dc2( int ndx )
			{
				DrawChar2( Code[ ndx ], ParityA[ ndx ] );
			}

			Dc2( 0 );
			Do01();
			Dc2( 1 );

			break;

		case SUPPLEMENT.SUPP5:
			Do1011();

			Code = SupplementalText.StripCode( 5 );
			var Ndx = CalcNdx( Code );
			var Len = Code.Length;

			for( var I = 0; I < Len; I++ )
			{
				DrawChar2( Code[ I ], SUP5[ Ndx ][ I ] );

				if( I < 4 )
					Do01();
			}
			break;

		default:
			return true;
		}

		var Length    = Code.Length - 1; // No CSum
		var MaxHeight = AdjustedHeight / 12;

		var BarcodeEnd = GetOffset();

		var Info = SizeText( BarcodeEnd - BarcodeStart, Length, MaxHeight );

		var PTop = Padding.Top;
		var H    = Info.Height + PTop;

		var       G               = Graphics!;
		using var BackgroundBrush = new SolidBrush( BackgroundColor );

		var Rect = new RectangleF( BarcodeStart, 0, BarcodeEnd, H );
		G.FillRectangle( BackgroundBrush, Rect );

		var Rect2 = GetTextArea( Info, BarcodeStart, BarcodeEnd );
		Rect2.Y = Rect.Y + PTop;

		DrawCodeTextRect( Code, Rect2 );

		return Ok;
	}
}