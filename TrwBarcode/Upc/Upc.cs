﻿// ReSharper disable InconsistentNaming

namespace TBarcode;

public partial class TrwBarcode
{
	private enum PARITY
	{
		EVEN,
		ODD
	}

	private static int UpcCheckSum( string text )
	{
		var Len = text.Length;

		var Odd = 0;

		for( var Ndx = Len - 1; Ndx >= 0; Ndx -= 2 )
			Odd += text[ Ndx ] - '0';

		var Even = 0;

		for( var Ndx = Len - 2; Ndx >= 0; Ndx -= 2 )
			Even += text[ Ndx ] - '0';

		var Val  = ( Odd * 3 ) + Even;
		var Val2 = ( ( Val + 9 ) / 10 ) * 10;

		return Val2 - Val;
	}

	private string BarcodeToUpcCheckSumString( int requiredLength )
	{
		var Code = Barcode.StripCode( requiredLength );
		var Len  = Code.Length;

		if( Len < requiredLength )
			Code = $"{Code}{new string( '0', requiredLength - Len )}";

		var CSum = (char)( UpcCheckSum( Code ) + '0' );
		return $"{Code}{CSum}";
	}


#region UPC Static Data
#region Suplimental 2 and 5
	private static readonly PARITY[][] SUP2 =
	[
		[PARITY.ODD, PARITY.ODD],
		[PARITY.ODD, PARITY.EVEN],
		[PARITY.EVEN, PARITY.ODD],
		[PARITY.EVEN, PARITY.EVEN]
	];


	private static readonly PARITY[][] SUP5 =
	[
		[PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.ODD], //0
		[PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.ODD], //1
		[PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.ODD], //2
		[PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.ODD, PARITY.EVEN], //3
		[PARITY.ODD, PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.ODD], //4
		[PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.EVEN, PARITY.ODD], //5
		[PARITY.ODD, PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.EVEN], //6
		[PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.ODD], //7
		[PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.EVEN], //8
		[PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.EVEN]  //9
	];
#endregion

#region UPCA
	private static readonly byte[][] UPCA =
	[
		[0, 0, 0, 1, 1, 0, 1], //	0
		[0, 0, 1, 1, 0, 0, 1], //	1
		[0, 0, 1, 0, 0, 1, 1], //	2
		[0, 1, 1, 1, 1, 0, 1], //	3
		[0, 1, 0, 0, 0, 1, 1], //	4
		[0, 1, 1, 0, 0, 0, 1], //	5
		[0, 1, 0, 1, 1, 1, 1], // 	6
		[0, 1, 1, 1, 0, 1, 1], //	7
		[0, 1, 1, 0, 1, 1, 1], //	8
		[0, 0, 0, 1, 0, 1, 1]  //	9
	];
#endregion

#region UPCE
	private static readonly PARITY[][] UPCE_CHECK =
	[
		[PARITY.EVEN, PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.ODD], //	0
		[PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.ODD], //	1
		[PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.ODD], //	2
		[PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.ODD, PARITY.EVEN], //	3
		[PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.EVEN, PARITY.ODD, PARITY.ODD], //	4
		[PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.EVEN, PARITY.ODD], //	5
		[PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.EVEN], //	6
		[PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.ODD], //	7
		[PARITY.EVEN, PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.EVEN], //	8
		[PARITY.EVEN, PARITY.ODD, PARITY.ODD, PARITY.EVEN, PARITY.ODD, PARITY.EVEN]  //	9
	];

	private static readonly byte[][] UPCE_ODD =
	[
		[0, 0, 0, 1, 1, 0, 1], //	0
		[0, 0, 1, 1, 0, 0, 1], //	1
		[0, 0, 1, 0, 0, 1, 1], //	2
		[0, 1, 1, 1, 1, 0, 1], //	3
		[0, 1, 0, 0, 0, 1, 1], //	4
		[0, 1, 1, 0, 0, 0, 1], //	5
		[0, 1, 0, 1, 1, 1, 1], //	6
		[0, 1, 1, 1, 0, 1, 1], //	7
		[0, 1, 1, 0, 1, 1, 1], //	8
		[0, 0, 0, 1, 0, 1, 1]  //	9
	];

	private static readonly byte[][] UPCE_EVEN =
	[
		[0, 1, 0, 0, 1, 1, 1], //	0
		[0, 1, 1, 0, 0, 1, 1], //	1
		[0, 0, 1, 1, 0, 1, 1], //	2
		[0, 1, 0, 0, 0, 0, 1], //	3
		[0, 0, 1, 1, 1, 0, 1], //	4
		[0, 1, 1, 1, 0, 0, 1], //	5
		[0, 0, 0, 0, 1, 0, 1], //	6
		[0, 0, 1, 0, 0, 0, 1], //	7
		[0, 0, 0, 1, 0, 0, 1], //	8
		[0, 0, 1, 0, 1, 1, 1]  //	9
	];
#endregion
#endregion
}