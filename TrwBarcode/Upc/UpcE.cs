﻿#nullable enable
#pragma warning disable CA1416



// ReSharper disable ReplaceSubstringWithRangeIndexer
namespace TBarcode;

public partial class TrwBarcode
{
	private const int UPCE_QUIET_ZONE = 10;

	private void UpcE( BARCODE_TYPE upcType )
	{
		var Code = Barcode.StripCode( 12 );
		var Len  = Code.Length;

		string PrintText;

		var IsInvalid = false;

		var CheckSum         = 0;
		var ChecksumSupplied = false;

		switch( Len )
		{
		case 12:
			Code = Code.Substring( 0, 11 );
			goto case 11;

		case 11: // https://www.barcodefaq.com/barcode-properties/symbologies/upc-e/

			ChecksumSupplied = true;
			CheckSum         = UpcCheckSum( Code );

			bool ConditionA()
			{
				var C = Code[ 10 ];

				if( C is >= '5' and <= '9' && Code[ 5 ] is not '0' && Code[ 6 ] is '0' && Code[ 7 ] is '0' && Code[ 8 ] is '0' && Code[ 9 ] is '0' )
				{
					Code = $"{Code[ 0 ]}{Code[ 1 ]}{Code[ 2 ]}{Code[ 3 ]}{Code[ 4 ]}{Code[ 5 ]}{Code[ 10 ]}";
					return true;
				}
				return false;
			}

			bool ConditionB()
			{
				if( Code[ 4 ] is not '0' && Code[ 5 ] is '0' && Code[ 6 ] is '0' && Code[ 7 ] is '0' && Code[ 8 ] is '0' && Code[ 9 ] is '0' )
				{
					Code = $"{Code[ 0 ]}{Code[ 1 ]}{Code[ 2 ]}{Code[ 3 ]}{Code[ 4 ]}{Code[ 10 ]}4";
					return true;
				}
				return false;
			}

			bool ConditionC()
			{
				if( Code[ 3 ] is >= '0' and <= '2' && Code[ 4 ] is '0' && Code[ 5 ] is '0' && Code[ 6 ] is '0' && Code[ 7 ] is '0' )
				{
					Code = $"{Code[ 0 ]}{Code[ 1 ]}{Code[ 2 ]}{Code[ 8 ]}{Code[ 9 ]}{Code[ 10 ]}{Code[ 3 ]}";
					return true;
				}
				return false;
			}

			bool ConditionD()
			{
				if( Code[ 3 ] is >= '3' and <= '9' && Code[ 4 ] is '0' && Code[ 5 ] is '0' && Code[ 6 ] is '0' && Code[ 7 ] is '0' && Code[ 8 ] is '0' )
				{
					Code = $"{Code[ 0 ]}{Code[ 1 ]}{Code[ 2 ]}{Code[ 3 ]}{Code[ 9 ]}{Code[ 10 ]}3";
					return true;
				}
				return false;
			}

			if( !ConditionA() && !ConditionB() && !ConditionC() && !ConditionD() )
				goto Invalid;
			goto case 7;

		case 6:
			PrintText = Code;
			break;

		case 7: // Override Upc type
			upcType   = Code[ 0 ] == '0' ? BARCODE_TYPE.UPCE_0 : BARCODE_TYPE.UPCE_1;
			Code      = Code.Substring( 1 );
			PrintText = Code;
			break;

		case 8:
			Code = Code.Substring( 0, Code.Length - 1 );
			goto case 7;

		default:
		Invalid:
			IsInvalid = true;
			PrintText = "005500";
			break;
		}

		if( !ChecksumSupplied )
			CheckSum = UpcCheckSum( Code );

		MovePenNarrow( UPCE_QUIET_ZONE );
		var QuietEnd = GetOffset();

		Do101();

		var BarcodeStart = GetOffset();

		Len = PrintText.Length;

		for( var I = 0;
			 I < Len;
			 I++ )
		{
			var Pa = UPCE_CHECK[ CheckSum ][ I ] switch
					 {
						 PARITY.EVEN => upcType == BARCODE_TYPE.UPCE_0 ? UPCE_EVEN : UPCE_ODD,
						 PARITY.ODD  => upcType == BARCODE_TYPE.UPCE_1 ? UPCE_EVEN : UPCE_ODD,
						 _           => throw new ArgumentOutOfRangeException()
					 };

			foreach( var B in Pa[ PrintText[ I ] - '0' ] )
			{
				if( B == 1 )
					DrawLine();

				MovePen();
			}
		}

		Do010();
		var BarcodeEnd = GetOffset();
		Do101();
		var CheckSumStart = GetOffset();

		var Diff = BarcodeEnd - BarcodeStart;
		Len = PrintText.Length;

		var Size = SizeText( Diff, Len, AdjustedHeight / 10 );

		var Mode = upcType == BARCODE_TYPE.UPCE_0 ? "0" : "1";

		var WidthOfOneChar = (int)( Size.Width / Len );
		var Rect           = GetTextArea( Mode, QuietEnd - ( WidthOfOneChar + ( WidthOfOneChar / 2 ) ), QuietEnd );
		DrawCodeTextRect( Mode, Rect );

		Rect = GetTextArea( PrintText, BarcodeStart, BarcodeEnd );

		using var Bc = new SolidBrush( BackgroundColor );

		Graphics!.FillRectangle( Bc, Rect with
									 {
										 X = Rect.X - ( ThinBarWidth * 5 ),
										 Width = Rect.Width + ( ThinBarWidth * 9 )
									 } );

		DrawCodeTextRect( PrintText, Rect );
		var CSum = ( (char)( CheckSum + '0' ) ).ToString();

		CheckSumStart += ThickBarWidth * 2;
		Rect          =  GetTextArea( CSum, CheckSumStart, CheckSumStart + WidthOfOneChar );
		DrawCodeTextRect( CSum, Rect );

		if( IsInvalid )
			InvalidBarcode();
	}
}