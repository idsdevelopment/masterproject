﻿#nullable enable
#pragma warning disable CA1416

// ReSharper disable ReplaceSubstringWithRangeIndexer

namespace TBarcode;

public partial class TrwBarcode
{
	private const int UPCA_QUIET_ZONE = 10;

	private void UpcA()
	{
		var Code = BarcodeToUpcCheckSumString( 11 );

		var LeftSideStart = GetOffset();

		MovePenNarrow( UPCA_QUIET_ZONE );

		var StartOffset = GetOffset();

		var State = BARCODE_STATE.PREFIX;

		var LeftTextStart   = 0;
		var LeftSideTextEnd = 0;
		var RightTextStart  = 0;

		for( var Index = 0; ( Index < Code.Length ) && ( Index <= 11 ); Index++ )
		{
			switch( State )
			{
			case BARCODE_STATE.PREFIX: // Index 0
				Do101();
				LeftTextStart = GetOffset();
				State         = BARCODE_STATE.LEFT_SIDE;
				goto case BARCODE_STATE.LEFT_SIDE;

			case BARCODE_STATE.LEFT_SIDE:
			case BARCODE_STATE.RIGHT_SIDE:
				var Ndx = Code[ Index ] - '0';

				foreach( var B in UPCA[ Ndx ] )
				{
					switch( B )
					{
					case 0 when State == BARCODE_STATE.RIGHT_SIDE:
						DrawLine();
						break;

					case 1 when State == BARCODE_STATE.LEFT_SIDE:
						DrawLine();
						break;
					}
					MovePen();
				}

				if( Index == 5 )
				{
					LeftSideTextEnd = GetOffset();
					Do01010();
					RightTextStart = GetOffset();
					State          = BARCODE_STATE.RIGHT_SIDE;
				}
				break;
			}
		}
		var RightSideTextEnd = GetOffset();
		Do101();
		var EndOffset = GetOffset();
		var Len       = Code.Length;

		if( Len < 12 )
			AutoDrawText( StartOffset, EndOffset, Code, AdjustedHeight / 10, ExtendTextArea );
		else
		{
			var Info = SizeText( EndOffset - StartOffset, Len, AdjustedHeight / 9 );

			var Ch2 = Info.Height / 2;

			var Bottom = AdjustedHeight - Ch2;

			var       Rect            = new RectangleF( LeftSideStart, Bottom - Padding.Bottom, AdjustedWidth, Bottom );
			using var BackgroundBrush = new SolidBrush( BackgroundColor );
			var       G               = Graphics!;

			G.FillRectangle( BackgroundBrush, Rect with
											  {
												  Y = Rect.Y + ( Ch2 / 2 )
											  } );

			var WidthOfOneChar = Info.Width / Len;

			void DoText( float x, string text )
			{
				Rect.X     = x;
				Rect.Width = text.Length * WidthOfOneChar;
				DrawCodeTextRect( text, Rect );
			}

			Rect.Y = ( Rect.Y - Ch2 ) + ( Ch2 / 2 );

			//Lead in char
			DoText( LeftSideStart, Code.Substring( 0, 1 ) );

			void DoCenteredText( int areaStart, int areaEnd, string text )
			{
				Rect.X     = areaStart;
				Rect.Width = areaEnd - areaStart;
				G.FillRectangle( BackgroundBrush, Rect );
				var Center        = areaStart + ( ( areaEnd - areaStart ) / 2 );
				var HalfTextWidth = ( text.Length * WidthOfOneChar ) / 2;
				DoText( Center - HalfTextWidth, text );
			}

			DoCenteredText( LeftTextStart + ThickBarWidth, LeftSideTextEnd + ThickBarWidth, Code.Substring( 1, 5 ) );
			DoCenteredText( RightTextStart, RightSideTextEnd - ThinBarWidth, Code.Substring( 6, 5 ) );

			//Checksum
			DoText( EndOffset + ThickBarWidth, Code.Substring( 11, 1 ) );
		}
	}
}