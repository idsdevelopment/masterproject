﻿using System.Text;

namespace TBarcode;

public sealed partial class TrwBarcode
{
	private const int CODE39_QUIET_ZONE = 5;

	private static readonly Dictionary<char, byte[]> Code39Dictionary = new()
	                                                                    {
		                                                                    { '0', [0, 0, 0, 1, 1, 0, 1, 0, 0] },
		                                                                    { '1', [1, 0, 0, 1, 0, 0, 0, 0, 1] },
		                                                                    { '2', [0, 0, 1, 1, 0, 0, 0, 0, 1] },
		                                                                    { '3', [1, 0, 1, 1, 0, 0, 0, 0, 0] },
		                                                                    { '4', [0, 0, 0, 1, 1, 0, 0, 0, 1] },
		                                                                    { '5', [1, 0, 0, 1, 1, 0, 0, 0, 0] },
		                                                                    { '6', [0, 0, 1, 1, 1, 0, 0, 0, 0] },
		                                                                    { '7', [0, 0, 0, 1, 0, 0, 1, 0, 1] },
		                                                                    { '8', [1, 0, 0, 1, 0, 0, 1, 0, 0] },
		                                                                    { '9', [0, 0, 1, 1, 0, 0, 1, 0, 0] },
		                                                                    { 'A', [1, 0, 0, 0, 0, 1, 0, 0, 1] },
		                                                                    { 'B', [0, 0, 1, 0, 0, 1, 0, 0, 1] },
		                                                                    { 'C', [1, 0, 1, 0, 0, 1, 0, 0, 0] },
		                                                                    { 'D', [0, 0, 0, 0, 1, 1, 0, 0, 1] },
		                                                                    { 'E', [1, 0, 0, 0, 1, 1, 0, 0, 0] },
		                                                                    { 'F', [0, 0, 1, 0, 1, 1, 0, 0, 0] },
		                                                                    { 'G', [0, 0, 0, 0, 0, 1, 1, 0, 1] },
		                                                                    { 'H', [1, 0, 0, 0, 0, 1, 1, 0, 0] },
		                                                                    { 'I', [0, 0, 1, 0, 0, 1, 1, 0, 0] },
		                                                                    { 'J', [0, 0, 0, 0, 1, 1, 1, 0, 0] },
		                                                                    { 'K', [1, 0, 0, 0, 0, 0, 0, 1, 1] },
		                                                                    { 'L', [0, 0, 1, 0, 0, 0, 0, 1, 1] },
		                                                                    { 'M', [1, 0, 1, 0, 0, 0, 0, 1, 0] },
		                                                                    { 'N', [0, 0, 0, 0, 1, 0, 0, 1, 1] },
		                                                                    { 'O', [1, 0, 0, 0, 1, 0, 0, 1, 0] },
		                                                                    { 'P', [0, 0, 1, 0, 1, 0, 0, 1, 0] },
		                                                                    { 'Q', [0, 0, 0, 0, 0, 0, 1, 1, 1] },
		                                                                    { 'R', [1, 0, 0, 0, 0, 0, 1, 1, 0] },
		                                                                    { 'S', [0, 0, 1, 0, 0, 0, 1, 1, 0] },
		                                                                    { 'T', [0, 0, 0, 0, 1, 0, 1, 1, 0] },
		                                                                    { 'U', [1, 1, 0, 0, 0, 0, 0, 0, 1] },
		                                                                    { 'V', [0, 1, 1, 0, 0, 0, 0, 0, 1] },
		                                                                    { 'W', [1, 1, 1, 0, 0, 0, 0, 0, 0] },
		                                                                    { 'X', [0, 1, 0, 0, 1, 0, 0, 0, 1] },
		                                                                    { 'Y', [1, 1, 0, 0, 1, 0, 0, 0, 0] },
		                                                                    { 'Z', [0, 1, 1, 0, 1, 0, 0, 0, 0] },
		                                                                    { '-', [0, 1, 0, 0, 0, 0, 1, 0, 1] },
		                                                                    { '.', [1, 1, 0, 0, 0, 0, 1, 0, 0] },
		                                                                    { ' ', [0, 1, 1, 0, 0, 0, 1, 0, 0] },
		                                                                    { '*', [0, 1, 0, 0, 1, 0, 1, 0, 0] },
		                                                                    { '$', [0, 1, 0, 1, 0, 1, 0, 0, 0] },
		                                                                    { '/', [0, 1, 0, 1, 0, 0, 0, 1, 0] },
		                                                                    { '+', [0, 1, 0, 0, 0, 1, 0, 1, 0] },
		                                                                    { '%', [0, 0, 0, 1, 0, 1, 0, 1, 0] }
	                                                                    };

	private static readonly string[] Code39Extended =
	[
		"%U", "$A", "$B", "$C", "$D", "$E", "$F", "$G", // 0..7
		"$H", "$I", "$J", "$K", "$L", "$M", "$N", "$O", // 8..15
		"$P", "$Q", "$R", "$S", "$T", "$U", "$V", "$W", // 16..23
		"$X", "$Y", "$Z", "%A", "%B", "%C", "%D", "%E", // 24..31
		" ", "/A", "/B", "/C", "/D", "/E", "/F", "/G",  // 32..39
		"/H", "/I", "/J", "/K", "/L", "-", ".", "/O",   // 40..47
		"0", "1", "2", "3", "4", "5", "6", "7",         // 48..55
		"8", "9", "/Z", "%F", "%G", "%H", "%I", "%J",   // 56..63
		"%V", "A", "B", "C", "D", "E", "F", "G",        // 64..71
		"H", "I", "J", "K", "L", "M", "N", "O",         // 72..79
		"P", "Q", "R", "S", "T", "U", "V", "W",         // 80..87
		"X", "Y", "Z", "%K", "%L", "%M", "%N", "%O",    // 88..95
		"%W", "+A", "+B", "+C", "+D", "+E", "+F", "+G", // 96..103
		"+H", "+I", "+J", "+K", "+L", "+M", "+N", "+O", // 104..111
		"+P", "+Q", "+R", "+S", "+T", "+U", "+V", "+W", // 112..119
		"+X", "+Y", "+Z", "%P", "%Q", "%R", "%S", "%T", // 120..127
		"%X", "%Y"                                      // 128..129
	];

	private void Code39( bool extended )
	{
		var Code    = Barcode;
		var CSum    = '0';
		var HasCSum = OptionalCheckSum;

		if( HasCSum )
		{
			var Check = 0;

			foreach( var C in Code )
				Check += C;

			CSum = (char)( ( Check % 43 ) + '0' );
			Code = Code + CSum;
		}

		var CodeBuilder = new StringBuilder( "*" );

		if( extended )
		{
			var Upper = Code39Extended.Length;

			foreach( var C in Code )
			{
				var Ndx = (int)C;

				if( Ndx < Upper )
					CodeBuilder.Append( Code39Extended[ Ndx ] );
			}
		}
		else
		{
			foreach( var C in Code.ToUpper() )
			{
				if( ( C != '*' ) && Code39Dictionary.ContainsKey( C ) )
					CodeBuilder.Append( C );
			}
		}

		CodeBuilder.Append( "*" );

		Code = CodeBuilder.ToString();

		MovePenNarrow( CODE39_QUIET_ZONE );

		var CodeStart = GetOffset();

		foreach( var C in Code )
		{
			var IsBar = true;

			if( Code39Dictionary.TryGetValue( C, out var Pattern ) )
			{
				foreach( var B in Pattern )
				{
					if( B == 0 )
						DrawNarrow( IsBar );
					else
						DrawWide( IsBar );

					IsBar = !IsBar;
				}
				MovePenNarrow( Code39CharacterGap );
			}
		}

		var CodeEnd = GetOffset();

		if( extended )
		{
			Code = $"*{Barcode}";

			if( HasCSum )
				Code = $"{Code}{CSum}";

			Code = $"{Code}*";
		}

		AutoDrawText( CodeStart, CodeEnd, Code, AdjustedHeight / 10, true );
	}
}