﻿// ReSharper disable InconsistentNaming

namespace TBarcode;

public sealed partial class TrwBarcode
{
	private const int CODE128B_QUIET_ZONE = 10;

	private void Code128B()
	{
		MovePenNarrow( CODE128B_QUIET_ZONE ); // 10x Quiet Zone
		var StartOffset = GetOffset();

		//Calculate the Checksum
		var CSum = 104; // Start Value For 128B

		var Code = Barcode;
		var Len  = Code.Length;

		for( var Ndx = 0; Ndx < Len; Ndx++ )
			CSum += Code128DiDictionary[ Code[ Ndx ] ].Value * ( Ndx + 1 );

		var ByteCSum = (byte)( CSum % 103 );

		var CharCSum = ' ';

		foreach( var Code128 in Code128DiDictionary )
		{
			if( Code128.Value.Value == ByteCSum )
			{
				CharCSum = Code128.Key;
				break;
			}
		}

		foreach( var C in $"{CODE_128B_START}{Code}{CharCSum}{CODE_128B_STOP}" )
		{
			foreach( var P in Code128DiDictionary[ C ].Pattern )
				DrawNarrow( P != 0 );
		}

		// Terminator
		DrawNarrow( true );
		DrawNarrow( true );
		var EndOffset = GetOffset();

		AutoDrawText( StartOffset, EndOffset, Barcode, AdjustedHeight / 5, ExtendTextArea );
	}

#region Code 128B Static Data
	private const char CODE_128B_START = char.MaxValue,
	                   CODE_128B_STOP  = (char)( CODE_128B_START - 1 ),
	                   DELETE          = (char)0x10;

	private struct Code128
	{
 #pragma warning disable CS0649
		internal byte   Value;
		internal byte[] Pattern;
#pragma warning restore CS0649
	}

	private static readonly Dictionary<char, Code128> Code128DiDictionary = new()
	                                                                        {
		                                                                        { DELETE, new Code128 { Value = 95, Pattern = [1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0] } }, //Delete

		                                                                        { ' ', new Code128 { Value  = 0, Pattern  = [1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0] } },
		                                                                        { '!', new Code128 { Value  = 1, Pattern  = [1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0] } },
		                                                                        { '"', new Code128 { Value  = 2, Pattern  = [1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0] } },
		                                                                        { '#', new Code128 { Value  = 3, Pattern  = [1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0] } },
		                                                                        { '$', new Code128 { Value  = 4, Pattern  = [1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0] } },
		                                                                        { '%', new Code128 { Value  = 5, Pattern  = [1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0] } },
		                                                                        { '&', new Code128 { Value  = 6, Pattern  = [1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0] } },
		                                                                        { '\'', new Code128 { Value = 7, Pattern  = [1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0] } },
		                                                                        { '(', new Code128 { Value  = 8, Pattern  = [1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0] } },
		                                                                        { ')', new Code128 { Value  = 9, Pattern  = [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0] } },
		                                                                        { '*', new Code128 { Value  = 10, Pattern = [1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0] } },
		                                                                        { '+', new Code128 { Value  = 11, Pattern = [1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0] } },
		                                                                        { ',', new Code128 { Value  = 12, Pattern = [1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0] } },
		                                                                        { '-', new Code128 { Value  = 13, Pattern = [1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0] } },
		                                                                        { '.', new Code128 { Value  = 14, Pattern = [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0] } },
		                                                                        { '/', new Code128 { Value  = 15, Pattern = [1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0] } },
		                                                                        //
		                                                                        { '0', new Code128 { Value = 16, Pattern = [1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0] } },
		                                                                        { '1', new Code128 { Value = 17, Pattern = [1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0] } },
		                                                                        { '2', new Code128 { Value = 18, Pattern = [1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0] } },
		                                                                        { '3', new Code128 { Value = 19, Pattern = [1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0] } },
		                                                                        { '4', new Code128 { Value = 20, Pattern = [1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0] } },
		                                                                        { '5', new Code128 { Value = 21, Pattern = [1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0] } },
		                                                                        { '6', new Code128 { Value = 22, Pattern = [1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0] } },
		                                                                        { '7', new Code128 { Value = 23, Pattern = [1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0] } },
		                                                                        { '8', new Code128 { Value = 24, Pattern = [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0] } },
		                                                                        { '9', new Code128 { Value = 25, Pattern = [1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0] } },
		                                                                        //
		                                                                        { ':', new Code128 { Value = 26, Pattern = [1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0] } },
		                                                                        { ';', new Code128 { Value = 27, Pattern = [1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0] } },
		                                                                        { '<', new Code128 { Value = 28, Pattern = [1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0] } },
		                                                                        { '=', new Code128 { Value = 29, Pattern = [1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0] } },
		                                                                        { '>', new Code128 { Value = 30, Pattern = [1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0] } },
		                                                                        { '?', new Code128 { Value = 31, Pattern = [1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0] } },
		                                                                        { '@', new Code128 { Value = 32, Pattern = [1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0] } },
		                                                                        //
		                                                                        { 'A', new Code128 { Value = 33, Pattern = [1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0] } },
		                                                                        { 'B', new Code128 { Value = 34, Pattern = [1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0] } },
		                                                                        { 'C', new Code128 { Value = 35, Pattern = [1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0] } },
		                                                                        { 'D', new Code128 { Value = 36, Pattern = [1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0] } },
		                                                                        { 'E', new Code128 { Value = 37, Pattern = [1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0] } },
		                                                                        { 'F', new Code128 { Value = 38, Pattern = [1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0] } },
		                                                                        { 'G', new Code128 { Value = 39, Pattern = [1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0] } },
		                                                                        { 'H', new Code128 { Value = 40, Pattern = [1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0] } },
		                                                                        { 'I', new Code128 { Value = 41, Pattern = [1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0] } },
		                                                                        { 'J', new Code128 { Value = 42, Pattern = [1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0] } },
		                                                                        { 'K', new Code128 { Value = 43, Pattern = [1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0] } },
		                                                                        { 'L', new Code128 { Value = 44, Pattern = [1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0] } },
		                                                                        { 'M', new Code128 { Value = 45, Pattern = [1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0] } },
		                                                                        { 'N', new Code128 { Value = 46, Pattern = [1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0] } },
		                                                                        { 'O', new Code128 { Value = 47, Pattern = [1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0] } },
		                                                                        { 'P', new Code128 { Value = 48, Pattern = [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0] } },
		                                                                        { 'Q', new Code128 { Value = 49, Pattern = [1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0] } },
		                                                                        { 'R', new Code128 { Value = 50, Pattern = [1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0] } },
		                                                                        { 'S', new Code128 { Value = 51, Pattern = [1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0] } },
		                                                                        { 'T', new Code128 { Value = 52, Pattern = [1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0] } },
		                                                                        { 'U', new Code128 { Value = 53, Pattern = [1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0] } },
		                                                                        { 'V', new Code128 { Value = 54, Pattern = [1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0] } },
		                                                                        { 'W', new Code128 { Value = 55, Pattern = [1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0] } },
		                                                                        { 'X', new Code128 { Value = 56, Pattern = [1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0] } },
		                                                                        { 'Y', new Code128 { Value = 57, Pattern = [1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0] } },
		                                                                        { 'Z', new Code128 { Value = 58, Pattern = [1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0] } },
		                                                                        //
		                                                                        { '[', new Code128 { Value  = 59, Pattern = [1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0] } },
		                                                                        { '\\', new Code128 { Value = 60, Pattern = [1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0] } },
		                                                                        { ']', new Code128 { Value  = 61, Pattern = [1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0] } },
		                                                                        { '^', new Code128 { Value  = 62, Pattern = [1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0] } },
		                                                                        { '_', new Code128 { Value  = 63, Pattern = [1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0] } },
		                                                                        { '`', new Code128 { Value  = 64, Pattern = [1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0] } },
		                                                                        //
		                                                                        { 'a', new Code128 { Value = 65, Pattern = [1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0] } },
		                                                                        { 'b', new Code128 { Value = 66, Pattern = [1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0] } },
		                                                                        { 'c', new Code128 { Value = 67, Pattern = [1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0] } },
		                                                                        { 'd', new Code128 { Value = 68, Pattern = [1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0] } },
		                                                                        { 'e', new Code128 { Value = 69, Pattern = [1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0] } },
		                                                                        { 'f', new Code128 { Value = 70, Pattern = [1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0] } },
		                                                                        { 'g', new Code128 { Value = 71, Pattern = [1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0] } },
		                                                                        { 'h', new Code128 { Value = 72, Pattern = [1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0] } },
		                                                                        { 'i', new Code128 { Value = 73, Pattern = [1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0] } },
		                                                                        { 'j', new Code128 { Value = 74, Pattern = [1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0] } },
		                                                                        { 'k', new Code128 { Value = 75, Pattern = [1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0] } },
		                                                                        { 'l', new Code128 { Value = 76, Pattern = [1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0] } },
		                                                                        { 'm', new Code128 { Value = 77, Pattern = [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0] } },
		                                                                        { 'n', new Code128 { Value = 78, Pattern = [1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0] } },
		                                                                        { 'o', new Code128 { Value = 79, Pattern = [1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0] } },
		                                                                        { 'p', new Code128 { Value = 80, Pattern = [1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0] } },
		                                                                        { 'q', new Code128 { Value = 81, Pattern = [1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0] } },
		                                                                        { 'r', new Code128 { Value = 82, Pattern = [1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0] } },
		                                                                        { 's', new Code128 { Value = 83, Pattern = [1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0] } },
		                                                                        { 't', new Code128 { Value = 84, Pattern = [1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0] } },
		                                                                        { 'u', new Code128 { Value = 85, Pattern = [1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0] } },
		                                                                        { 'v', new Code128 { Value = 86, Pattern = [1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0] } },
		                                                                        { 'w', new Code128 { Value = 87, Pattern = [1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0] } },
		                                                                        { 'x', new Code128 { Value = 88, Pattern = [1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0] } },
		                                                                        { 'y', new Code128 { Value = 89, Pattern = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0] } },
		                                                                        { 'z', new Code128 { Value = 90, Pattern = [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0] } },
		                                                                        //
		                                                                        { '{', new Code128 { Value = 91, Pattern = [1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0] } },
		                                                                        { '|', new Code128 { Value = 92, Pattern = [1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0] } },
		                                                                        { '}', new Code128 { Value = 93, Pattern = [1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0] } },
		                                                                        { '~', new Code128 { Value = 94, Pattern = [1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0] } },
		                                                                        //
		                                                                        { CODE_128B_START, new Code128 { Value = 0, Pattern = [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0] } },
		                                                                        { CODE_128B_STOP, new Code128 { Value  = 0, Pattern = [1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0] } }
	                                                                        };
#endregion
}