﻿#nullable enable

// ReSharper disable ReplaceSubstringWithRangeIndexer
// ReSharper disable InconsistentNaming
// ReSharper disable PossibleLossOfFraction

#pragma warning disable CA1416

using System.Drawing.Imaging;
using System.Text;
using TrwBarcode.Qr;

namespace TBarcode;

public static class Extensions
{
	public static string MaxLength( this string? value, int maxLength ) => value is null ? "" : value.Substring( 0, Math.Min( value.Length, maxLength ) );

	public static string StripCode( this string? text, int length )
	{
		if( text is not null )
		{
			var Result = new StringBuilder();

			foreach( var C in text )
			{
				if( C is >= '0' and <= '9' )
					Result.Append( C );
			}

			return Result.ToString().MaxLength( length );
		}
		return "";
	}
}

public sealed partial class TrwBarcode
{
	public enum BARCODE_TYPE
	{
		UPCA,
		UPCE_0,
		UPCE_1,
		EAN13,
		EAN8,
		CODE39,
		CODE39_EXT,
		IINT_2of5,
		AUST_POSTAL,
		ITF14,
		CODE128_B,
		QR
	}


	public enum SUPPLEMENT
	{
		SUPP_NONE,
		SUPP2,
		SUPP5
	}

#region Static Data
#endregion

#region Barcode Type
	public BARCODE_TYPE BarcodeType { get; set; } = BARCODE_TYPE.CODE128_B;
	public SUPPLEMENT   Supplement  { get; set; } = SUPPLEMENT.SUPP_NONE;

	private string CleanupBarcode( string? text )
	{
		if( text is not null )
		{
			var Sb = new StringBuilder();

			string DoCode128B()
			{
				foreach( var C in text )
				{
					if( C is >= ' ' and <= '~' or (char)0x10 )
						Sb.Append( C );
				}
				return Sb.ToString();
			}

			return BarcodeType switch
			       {
				       BARCODE_TYPE.CODE128_B => DoCode128B(),
				       _                      => text
			       };
		}

		return "";
	}
#endregion

#region Layout Options
	private const int       PADDING = 3;
	public        Rectangle Padding        { get; set; } = new( PADDING, PADDING, PADDING, PADDING );
	public        int       Height         { get; set; }
	public        int       Width          { get; set; }
	public        bool      ExtendTextArea { get; set; } = true;
#endregion

#region Barcode
	private string _Barcode = "";

	public string Barcode

	{
		get => _Barcode;
		set => _Barcode = CleanupBarcode( value );
	}

	public bool OptionalCheckSum   { get; set; }
	public int  Code39CharacterGap { get; set; } = 1;

	public string SupplementalText { get; set; } = "";


	public Font Font { get; set; } = new( "Courier New", 20, FontStyle.Bold, GraphicsUnit.Pixel );

#region Qr Code
	private QrProperties? _QrProperties;
	public  QrProperties  QrProperties => _QrProperties ??= new QrProperties( this );
#endregion

	private float FontSize

	{
		set

		{
			var F = Font;
			Font = new Font( F.FontFamily, value, F.Style, F.Unit );
		}
	}
#endregion

#region Canvas
	public enum IMAGE_FORMAT
	{
		BMP,
		PNG,
		JPEG,
		JPG = JPEG
	}

	public MemoryStream AsMemoryStream( IMAGE_FORMAT format ) => Draw( format );

	public string AsBase64String( IMAGE_FORMAT format )
	{
		using var Stream = AsMemoryStream( format );
		return Convert.ToBase64String( Stream.GetBuffer() );
	}

	public Image AsImage( IMAGE_FORMAT format )
	{
		using var Stream = AsMemoryStream( format );
		return Image.FromStream( Stream );
	}

	private const int THIN_PEN_WIDTH  = 2,
	                  THICK_PEN_WIDTH = 4;

	public Color BackgroundColor { get; set; } = Color.White;
	public Color BarColor        { get; set; } = Color.Black;
	public Color TextColor       { get; set; } = Color.Black;
	public int   ThinBarWidth    { get; set; } = THIN_PEN_WIDTH;
	public int   ThickBarWidth   { get; set; } = THICK_PEN_WIDTH;

	private int AdjustWidth()
	{
		int CalcWidthLength( int quietZoneSize, int stripesPerChar, int extraCharacters, int codeLength )
		{
			var ThinWidth      = ThinBarWidth;
			var CharacterWidth = ThinWidth * stripesPerChar;
			var Pad            = Padding;

			return Pad.Left + Padding.Right
			                + ( extraCharacters * CharacterWidth )
			                + ( codeLength * CharacterWidth )
			                + ( quietZoneSize * 2 * ThinWidth ); // Quiet Zones
		}

		int CalcWidth( int quietZoneSize, int stripesPerChar, int extraCharacters )
		{
			return CalcWidthLength( quietZoneSize, stripesPerChar, extraCharacters, Barcode.Length );
		}

		var Supp = Supplement;

		int Defaults()
		{
			var W = Width;

			if( W < 300 )
			{
				W     = 300;
				Width = W;
			}

			var H = Height;

			if( H < 300 )
			{
				H      = 300;
				Height = Height;
			}

			AdjustedHeight = H;
			AdjustedWidth  = W;

			return W;
		}

		return BarcodeType switch
		       {
			       BARCODE_TYPE.CODE128_B                           => CalcWidth( CODE128B_QUIET_ZONE, 11, 3 ),
			       BARCODE_TYPE.EAN8 when Supp is SUPPLEMENT.SUPP2  => CalcWidthLength( EAN8_QUIET_ZONE * 2, 7, 1, 11 ),
			       BARCODE_TYPE.EAN8 when Supp is SUPPLEMENT.SUPP5  => CalcWidthLength( EAN8_QUIET_ZONE * 2, 7, 1, 15 ),
			       BARCODE_TYPE.EAN8                                => CalcWidthLength( EAN8_QUIET_ZONE, 7, 1, 8 ),
			       BARCODE_TYPE.EAN13 when Supp is SUPPLEMENT.SUPP2 => CalcWidth( EAN13_QUIET_ZONE * 4, 7, 1 ),
			       BARCODE_TYPE.EAN13 when Supp is SUPPLEMENT.SUPP5 => CalcWidth( EAN13_QUIET_ZONE * 4, 7, 5 ),
			       BARCODE_TYPE.EAN13                               => CalcWidth( EAN13_QUIET_ZONE, 7, 0 ),
			       BARCODE_TYPE.UPCA                                => CalcWidth( UPCA_QUIET_ZONE, 7, 2 ),
			       BARCODE_TYPE.UPCE_0                              => CalcWidthLength( UPCE_QUIET_ZONE, 6, 2, 6 ),
			       BARCODE_TYPE.UPCE_1                              => CalcWidthLength( UPCE_QUIET_ZONE, 6, 2, 6 ),
			       BARCODE_TYPE.CODE39                              => CalcWidth( CODE39_QUIET_ZONE, 21, 0 ),
			       BARCODE_TYPE.CODE39_EXT                          => CalcWidth( CODE39_QUIET_ZONE, 12, 3 ),
			       _                                                => Defaults()
		       };
	}

	private MemoryStream Draw( IMAGE_FORMAT imageFormat )
	{
		// Initialise the Drawing Area
		AdjustedWidth  = AdjustWidth();
		AdjustedHeight = Height;

		try
		{
			using( Bitmap = new Bitmap( AdjustedWidth, AdjustedHeight ) )
			{
				using( Graphics = Graphics.FromImage( Bitmap ) )
				{
					var BackColor = BackgroundColor;
					Graphics.Clear( BackColor );

					var Pad = Padding;
					Graphics.Clip = new Region( new Rectangle( Pad.Left, Pad.Top, AdjustedWidth - Pad.Right, AdjustedHeight - Pad.Bottom ) );

					ThickPenWidth = ThickBarWidth;
					ThinPenWidth  = ThinBarWidth;

					var BColor = BarColor;

					using( ThickPen = new Pen( BColor, ThickPenWidth ) )
					{
						using( ThinPen = new Pen( BColor, ThinPenWidth ) )
						{
							TopPoint    = new Point( Pad.X, 0 );
							BottomPoint = new Point( Pad.X, AdjustedHeight );

							var BType = BarcodeType;

							switch( BType )
							{
							case BARCODE_TYPE.CODE128_B:
								Code128B();
								break;

							case BARCODE_TYPE.EAN8:
								Ean8();
								break;

							case BARCODE_TYPE.EAN13:
								Ean13();
								break;

							case BARCODE_TYPE.UPCA:
								UpcA();
								break;

							case BARCODE_TYPE.UPCE_0:
							case BARCODE_TYPE.UPCE_1:
								UpcE( BType );
								break;

							case BARCODE_TYPE.CODE39:
								Code39( false );
								break;

							case BARCODE_TYPE.CODE39_EXT:
								Code39( true );
								break;

							case BARCODE_TYPE.QR:
								new QrCode( this, QrProperties );
								break;
							}

							var Memory = new MemoryStream();

							Bitmap.Save( Memory, imageFormat switch
							                     {
								                     IMAGE_FORMAT.PNG  => ImageFormat.Png,
								                     IMAGE_FORMAT.JPEG => ImageFormat.Jpeg,
								                     IMAGE_FORMAT.BMP  => ImageFormat.Bmp,
								                     _                 => ImageFormat.Bmp
							                     } );
							Memory.Position = 0;
							return Memory;
						}
					}
				}
			}
		}
		finally
		{
			ThickPen = null;
			ThickPen = null;
			Graphics = null;
			Bitmap   = null;
		}
	}
#endregion
}