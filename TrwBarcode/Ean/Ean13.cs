﻿

// ReSharper disable ReplaceSubstringWithRangeIndexer
#pragma warning disable CA1416

namespace TBarcode;

public sealed partial class TrwBarcode
{
	private const int EAN13_QUIET_ZONE = 8;

	private void Ean13()
	{
		void DrawBars( char c, EAN_FLAG flag )
		{
			var Index = c - '0';

			foreach( var Flag in flag switch
							     {
								     EAN_FLAG.LA => EAN_LEFT_A[ Index ],
								     _           => EAN_LEFT_B[ Index ]
							     } )
			{
				if( Flag == 0 )
					MovePen();
				else
					DrawLineAndMove();
			}
		}

		var FlagIndex = 0;
		var Code      = BarcodeToUpcCheckSumString( 12 );
		MovePenNarrow( EAN13_QUIET_ZONE );

		var EndOfMargin    = GetOffset();
		var LeftSideStart  = 0;
		var CenterStart    = 0;
		var RightSideStart = 0;

		var State = BARCODE_STATE.PREFIX;

		for( var Index = 0; ( Index < Code.Length ) && ( Index <= 12 ); Index++ )
		{
			var C = Code[ Index ];

			switch( State )
			{
			case BARCODE_STATE.PREFIX: // Index 0
				Do101();
				LeftSideStart = GetOffset();
				FlagIndex     = C - '0';
				State         = BARCODE_STATE.SECOND_FLAG;
				break;

			case BARCODE_STATE.SECOND_FLAG: // Index 1
				DrawBars( C, EAN_FLAG.LA );
				State = BARCODE_STATE.LEFT_SIDE;
				break;

			case BARCODE_STATE.LEFT_SIDE: // Index 2..6
				DrawBars( C, EAN_FLAGS[ FlagIndex ][ Index - 2 ] );

				if( Index >= 6 )
				{
					CenterStart = GetOffset();
					Do01010();
					RightSideStart = GetOffset();

					State = BARCODE_STATE.RIGHT_SIDE;
				}
				break;

			case BARCODE_STATE.RIGHT_SIDE: // Index 7..12
				var Ndx = C - '0';

				foreach( var B in EAN_RIGHT[ Ndx ] )
				{
					switch( B )
					{
					case 0:
						MovePen();
						break;

					default:
						DrawLineAndMove();
						break;
					}
				}
				break;
			}
		}

		var RightSideEnd = GetOffset();
		Do101();

		var Invalid = !DrawSupplemental( EAN13_QUIET_ZONE );

		var Len            = Code.Length;
		var Info           = SizeText( RightSideEnd - LeftSideStart, Len, AdjustedHeight / 10 );
		var WidthOfOneChar = Info.Width / Len;
		var HeightAdjust   = Info.Height / 3;

		var G = Graphics!;

		using var BackgroundBrush = new SolidBrush( BackgroundColor );

		var Rect = new RectangleF( 0, AdjustedHeight - 1 - HeightAdjust, AdjustedWidth, HeightAdjust );
		G.FillRectangle( BackgroundBrush, Rect );

		Rect = new RectangleF( LeftSideStart + ThinBarWidth, AdjustedHeight - 1 - Info.Height, ( CenterStart - LeftSideStart ) + ThinBarWidth, Info.Height );
		G.FillRectangle( BackgroundBrush, Rect );

		Rect.X     = RightSideStart;
		Rect.Width = ( RightSideEnd - RightSideStart ) + ThinBarWidth;
		G.FillRectangle( BackgroundBrush, Rect );

		void DoText( float x, string text )
		{
			Rect.X     = x;
			Rect.Width = text.Length * WidthOfOneChar;
			DrawCodeTextRect( text, Rect );
		}

		//Lead in char
		DoText( EndOfMargin - ( WidthOfOneChar + ThinBarWidth ), Code.Substring( 0, 1 ) );

		void DoCenteredText( int areaStart, int areaEnd, string text )
		{
			var Center        = areaStart + ( ( areaEnd - areaStart ) / 2 );
			var HalfTextWidth = ( text.Length * WidthOfOneChar ) / 2;
			DoText( Center - HalfTextWidth, text );
		}

		var Half = ( Len - 1 ) / 2;
		DoCenteredText( LeftSideStart, CenterStart, Code.Substring( 1, Half ) );
		DoCenteredText( RightSideStart, RightSideEnd, Code.Substring( Half + 1 ) );

		if( Invalid )
			InvalidBarcode();
	}
}