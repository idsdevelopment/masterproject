﻿#nullable enable

#pragma warning disable CA1416

// ReSharper disable ReplaceSubstringWithRangeIndexer

namespace TBarcode;

public sealed partial class TrwBarcode
{
	private const int EAN8_QUIET_ZONE = 5;

	private void Ean8()
	{
		var Code = BarcodeToUpcCheckSumString( 7 );

		MovePenNarrow( EAN8_QUIET_ZONE );

		var LeftSideStart  = 0;
		var LeftSideEnd    = 0;
		var RightSideStart = 0;

		var State = BARCODE_STATE.PREFIX;

		for( var Index = 0; ( Index < Code.Length ) && ( Index <= 12 ); Index++ )
		{
			var Ndx = Code[ Index ] - '0';

			switch( State )
			{
			case BARCODE_STATE.PREFIX: // Index 0
				Do101();
				LeftSideStart = GetOffset();
				State         = BARCODE_STATE.LEFT_SIDE;
				goto case BARCODE_STATE.LEFT_SIDE;

			case BARCODE_STATE.LEFT_SIDE:
				foreach( var B in EAN_LEFT_A[ Ndx ] )
				{
					switch( B )
					{
					case 0:
						MovePen();
						break;

					default:
						DrawLineAndMove();
						break;
					}
				}

				if( Index >= 3 )
				{
					LeftSideEnd = GetOffset();
					Do01010();
					RightSideStart = GetOffset();
					State          = BARCODE_STATE.RIGHT_SIDE;
				}
				break;

			case BARCODE_STATE.RIGHT_SIDE: // Index 7..12
				foreach( var B in EAN_RIGHT[ Ndx ] )
				{
					switch( B )
					{
					case 0:
						MovePen();
						break;

					default:
						DrawLineAndMove();
						break;
					}
				}
				break;
			}
		}

		var RightSideEnd = GetOffset();
		Do101();

		if( !DrawSupplemental( EAN8_QUIET_ZONE ) )
			InvalidBarcode();

		var Len       = Code.Length - 1; // No CSum
		var MaxHeight = AdjustedHeight / 12;

		var Info = SizeText( RightSideEnd - LeftSideStart, Len, MaxHeight );

		var       G               = Graphics!;
		using var BackgroundBrush = new SolidBrush( BackgroundColor );

		var Ac2 = AdjustedHeight - ( Info.Height / 2 ) - Padding.Top;

		var Rect = new RectangleF( 0, Ac2, AdjustedWidth, Ac2 );
		G.FillRectangle( BackgroundBrush, Rect );

		var PrintText = Code.Substring( 0, 4 );

		LeftSideStart += ThinBarWidth;
		var AHeight = AdjustedHeight - 1 - Info.Height;

		Rect = new RectangleF( LeftSideStart, AHeight, ( LeftSideEnd - LeftSideStart ) + ThinBarWidth, Info.Height );
		G.FillRectangle( BackgroundBrush, Rect );

		var Info2 = Info with
					{
						Width = Info.Width / 2
					};

		Rect = GetTextArea( Info2, LeftSideStart, LeftSideEnd );
		DrawCodeTextRect( PrintText, Rect );

		Rect = new RectangleF( RightSideStart, AHeight, RightSideEnd - RightSideStart, Info.Height );
		G.FillRectangle( BackgroundBrush, Rect );
		Rect = GetTextArea( Info2, RightSideStart, RightSideEnd );
		DrawCodeTextRect( Code.Substring( 4, 4 ), Rect );
	}
}