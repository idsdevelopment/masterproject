﻿namespace TBarcode;

public sealed partial class TrwBarcode
{
	private enum EAN_FLAG
	{
		LA,
		LB
	}

	private enum BARCODE_STATE
	{
		PREFIX,
		SECOND_FLAG,
		LEFT_SIDE,

		RIGHT_SIDE
		//SUFFIX
	}

#region EAN Static Data
	private static readonly EAN_FLAG[][] EAN_FLAGS =
	[
		[EAN_FLAG.LA, EAN_FLAG.LA, EAN_FLAG.LA, EAN_FLAG.LA, EAN_FLAG.LA], //	0
		[EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LB], //	1
		[EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LB], //	2
		[EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LA], //	3
		[EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LB], //	4
		[EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LA, EAN_FLAG.LB], //	5
		[EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LA], //	6
		[EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LB], //	7
		[EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LA], //	8
		[EAN_FLAG.LB, EAN_FLAG.LB, EAN_FLAG.LA, EAN_FLAG.LB, EAN_FLAG.LA]  //	9
	];

	private static readonly byte[][] EAN_LEFT_A =
	[
		[0, 0, 0, 1, 1, 0, 1], //	0
		[0, 0, 1, 1, 0, 0, 1], //	1
		[0, 0, 1, 0, 0, 1, 1], //	2
		[0, 1, 1, 1, 1, 0, 1], //	3
		[0, 1, 0, 0, 0, 1, 1], //	4
		[0, 1, 1, 0, 0, 0, 1], //	5
		[0, 1, 0, 1, 1, 1, 1], //	6
		[0, 1, 1, 1, 0, 1, 1], //	7
		[0, 1, 1, 0, 1, 1, 1], //	8
		[0, 0, 0, 1, 0, 1, 1]  //	9
	];


	private static readonly byte[][] EAN_LEFT_B =
	[
		[0, 1, 0, 0, 1, 1, 1], //	0
		[0, 1, 1, 0, 0, 1, 1], //	1
		[0, 0, 1, 1, 0, 1, 1], //	2
		[0, 1, 0, 0, 0, 0, 1], //	3
		[0, 0, 1, 1, 1, 0, 1], //	4
		[0, 1, 1, 1, 0, 0, 1], //	5
		[0, 0, 0, 0, 1, 0, 1], //	6
		[0, 0, 1, 0, 0, 0, 1], //	7
		[0, 0, 0, 1, 0, 0, 1], //	8
		[0, 0, 1, 0, 1, 1, 1]  //	9
	];

	private static readonly byte[][] EAN_RIGHT =
	[
		[1, 1, 1, 0, 0, 1, 0], //	0
		[1, 1, 0, 0, 1, 1, 0], //	1
		[1, 1, 0, 1, 1, 0, 0], //	2
		[1, 0, 0, 0, 0, 1, 0], //	3
		[1, 0, 1, 1, 1, 0, 0], //	4
		[1, 0, 0, 1, 1, 1, 0], //	5
		[1, 0, 1, 0, 0, 0, 0], //	6
		[1, 0, 0, 0, 1, 0, 0], //	7
		[1, 0, 0, 1, 0, 0, 0], //	8
		[1, 1, 1, 0, 1, 0, 0]  //	9
	];
#endregion
}