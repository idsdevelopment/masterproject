﻿#nullable enable

namespace TBarcode;

public sealed partial class TrwBarcode
{
	internal Bitmap?   Bitmap;
	internal Graphics? Graphics;

	// ReSharper disable once NotAccessedField.Local
	private Pen? ThickPen,
				 ThinPen;

	private Point TopPoint,
				  BottomPoint;


	private int ThickPenWidth,
				ThinPenWidth,
				AdjustedWidth,
				AdjustedHeight;
}