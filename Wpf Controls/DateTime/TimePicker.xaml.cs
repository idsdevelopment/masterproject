﻿#nullable enable

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace WpfControls
{
	/// <summary>
	///     Interaction logic for TimePicker.xaml
	/// </summary>
	public partial class TimePicker
	{
		private bool LoadingValue
		{
			get => _LoadingValue || !Initialised;
			set => _LoadingValue = value;
		}

		public TimePicker()
		{
			InitializeComponent();
		}

		private enum SPIN_STATE
		{
			HOURS,
			MINS,
			SECS,
			AM_PM
		}

		private SPIN_STATE State = SPIN_STATE.HOURS;

		private bool _LoadingValue;
		private bool Initialised;

		private bool InChangeValue;

		public delegate void ValueChangedEvent( TimePicker sender, DateTime newValue );

		private static void ShowSecondsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
		{
			if( dependencyObject is TimePicker Picker && dependencyPropertyChangedEventArgs.NewValue is bool Show )
			{
				if( !Show )
				{
					Picker.Seconds.Visibility        = Visibility.Collapsed;
					Picker.SecondsDivider.Visibility = Visibility.Collapsed;
				}
				else
				{
					Picker.Seconds.Visibility        = Visibility.Visible;
					Picker.SecondsDivider.Visibility = Visibility.Visible;
				}
			}
		}

		private static void AmPmPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
		{
			if( dependencyObject is TimePicker Picker )
				Picker.Hours.MaximumValue = (bool)dependencyPropertyChangedEventArgs.NewValue ? 11 : 23;
		}

		private static void ValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
		{
			if( dependencyObject is TimePicker Picker )
			{
				if( !Picker.InChangeValue )
				{
					Picker.InChangeValue = true;

					try
					{
						var Value = (DateTime)dependencyPropertyChangedEventArgs.NewValue;
						var Hour  = Value.Hour;

						if( Picker.AmPm )
						{
							string Text;

							if( Hour >= 12 )
							{
								Hour -= 12;
								Text =  "PM";
							}
							else
								Text = "AM";

							Picker.AmPmInput.Text = Text;
						}

						Picker.Hours.Value   = Hour;
						Picker.Minutes.Value = Value.Minute;
						Picker.Seconds.Value = Value.Second;
						Picker.Value         = Value;
					}
					finally
					{
						Picker.InChangeValue = false;
					}
				}
			}
		}

		private void ResetBackgrounds()
		{
			Hours.Background     = Background;
			Minutes.Background   = Background;
			Seconds.Background   = Background;
			AmPmInput.Background = Background;
		}

		private void Hours_GotFocus( object sender, RoutedEventArgs e )
		{
			ResetBackgrounds();
			State            = SPIN_STATE.HOURS;
			Hours.Background = SelectedBackground;
		}

		private void Minutes_GotFocus( object sender, RoutedEventArgs e )
		{
			ResetBackgrounds();
			State              = SPIN_STATE.MINS;
			Minutes.Background = SelectedBackground;
		}

		private void Seconds_GotFocus( object sender, RoutedEventArgs e )
		{
			ResetBackgrounds();
			State              = SPIN_STATE.SECS;
			Seconds.Background = SelectedBackground;
		}

		private void AmPmInput_GotFocus( object sender, RoutedEventArgs e )
		{
			ResetBackgrounds();
			AmPmInput.SelectAll();
			State                = SPIN_STATE.AM_PM;
			AmPmInput.Background = SelectedBackground;
		}

		private void Spinner_SpinUpClick( object sender, EventArgs e )
		{
			switch( State )
			{
			case SPIN_STATE.HOURS:
				Value = Value.AddHours( 1 );
				break;

			case SPIN_STATE.MINS:
				Value = Value.AddMinutes( 1 );
				break;

			case SPIN_STATE.SECS:
				if( ShowSeconds )
					Value = Value.AddSeconds( 1 );
				break;

			case SPIN_STATE.AM_PM:
				if( AmPm )
					Value = Value.AddHours( 12 );
				break;
			}
		}

		private void Spinner_SpinDownClick( object sender, EventArgs e )
		{
			var V = Value.AddDays( 1 ); // Stop -ve underflow

			switch( State )
			{
			case SPIN_STATE.HOURS:
				Value = V.AddHours( -1 );
				break;

			case SPIN_STATE.MINS:
				Value = V.AddMinutes( -1 );
				break;

			case SPIN_STATE.SECS:
				if( ShowSeconds )
					Value = V.AddSeconds( -1 );
				break;

			case SPIN_STATE.AM_PM:
				if( AmPm )
					Value = V.AddHours( -12 );
				break;
			}
		}

		private void AmPmInput_PreviewKeyDown( object sender, KeyEventArgs e )
		{
			if( e.Key != Key.Tab )
			{
				e.Handled = true;
				Value     = Value.AddHours( 12 );
			}
		}

		private void AmPmInput_PreviewKeyUp( object sender, KeyEventArgs e )
		{
			e.Handled = true;
		}

		private void UserControl_Loaded( object sender, RoutedEventArgs e )
		{
			Initialised = true;
		}

		private void AmPmInput_MouseWheel( object sender, MouseWheelEventArgs e )
		{
			e.Handled = true;
			Value     = Value.AddHours( 12 );
		}

		internal void Hours_ValueChanged( NumericInput _, decimal value )
		{
			if( !LoadingValue )
			{
				var V = Value;
				Value = V.AddDays( 1 ).AddHours( -V.Hour ).AddHours( (int)value );
			}
		}

		internal void Minutes_ValueChanged( NumericInput _, decimal value )
		{
			if( !LoadingValue )
			{
				var V = Value;
				Value = V.AddDays( 1 ).AddMinutes( -V.Minute ).AddMinutes( (int)value );
			}
		}

		internal void Seconds_ValueChanged( NumericInput _, decimal value )
		{
			if( !LoadingValue )
			{
				var V = Value;
				Value = V.AddDays( 1 ).AddSeconds( -V.Second ).AddHours( (int)value );
			}
		}

		private void AmPmInput_LostFocus( object sender, RoutedEventArgs e )
		{
			ResetBackgrounds();
		}

		public event ValueChangedEvent? ValueChanged;

		[Category( "Options" )]
		public bool AmPm
		{
			get => (bool)GetValue( AmPmProperty );
			set => SetValue( AmPmProperty, value );
		}

		// Using a DependencyProperty as the backing store for AmPm.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedBackgroundProperty =
			DependencyProperty.Register( nameof( SelectedBackground ), typeof( Brush ), typeof( TimePicker ), new FrameworkPropertyMetadata( Brushes.PowderBlue ) );

		public static readonly DependencyProperty ShowSecondsProperty =
			DependencyProperty.Register( nameof( ShowSeconds ), typeof( bool ), typeof( TimePicker ), new FrameworkPropertyMetadata( true, ShowSecondsPropertyChangedCallback ) );

		public static readonly DependencyProperty AmPmProperty =
			DependencyProperty.Register( nameof( AmPm ), typeof( bool ), typeof( TimePicker ), new FrameworkPropertyMetadata( true, AmPmPropertyChangedCallback ) );

		[Category( "Brush" )]
		public Brush SelectedBackground
		{
			get => (Brush)GetValue( SelectedBackgroundProperty );
			set => SetValue( SelectedBackgroundProperty, value );
		}

		// Using a DependencyProperty as the backing store for SelectedBackground.  This enables animation, styling, binding, etc...


		[Category( "Options" )]
		public bool ShowSeconds
		{
			get => (bool)GetValue( ShowSecondsProperty );
			set => SetValue( ShowSecondsProperty, value );
		}

		// Using a DependencyProperty as the backing store for ShowSeconds.  This enables animation, styling, binding, etc...


		[Category( "Options" )]
		public DateTime Value
		{
			get
			{
				var RetVal = new DateTime( ( (DateTime)GetValue( ValueProperty ) ).TimeOfDay.Ticks );

				if( !ShowSeconds )
					RetVal = RetVal.AddSeconds( -RetVal.Second );
				return RetVal;
			}

			set
			{
				if( !LoadingValue )
				{
					LoadingValue = true;

					try
					{
						SetValue( ValueProperty, value );

						ValueChanged?.Invoke( this, Value );
					}
					finally
					{
						LoadingValue = false;
					}
				}
			}
		}

		// Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register( nameof( Value ), typeof( DateTime ), typeof( TimePicker ), new FrameworkPropertyMetadata( new DateTime(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ValuePropertyChangedCallback ) );
	}
}