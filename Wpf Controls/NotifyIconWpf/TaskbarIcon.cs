﻿#nullable enable

// hardcodet.net NotifyIcon for WPF
// Copyright (c) 2009 - 2013 Philipp Sumi
// Contact and Information: http://www.hardcodet.net
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the Code Project Open License (CPOL);
// either version 1.0 of the License, or (at your option) any later
// version.
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE


using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;
using System.Windows.Threading;
using Wpf.TaskbarNotification.Interop;
using Brushes = System.Windows.Media.Brushes;
using Point = Wpf.TaskbarNotification.Interop.Point;

namespace Wpf.TaskbarNotification
{
	/// <summary>
	///     A WPF proxy to for a taskbar icon (NotifyIcon) that sits in the system's
	///     taskbar notification area ("system tray").
	/// </summary>
	public partial class TaskbarIcon : FrameworkElement, IDisposable
	{
	#region Construction
		/// <summary>
		///     Initializes the taskbar icon and registers a message listener
		///     in order to receive events from the taskbar area.
		/// </summary>
		public TaskbarIcon()
		{
			// using dummy sink in design mode
			MessageSink = Util.IsDesignMode
				              ? WindowMessageSink.CreateEmpty()
				              : new WindowMessageSink( NotifyIconVersion.WIN95 );

			// init icon data structure
			IconData = NotifyIconData.CreateDefault( MessageSink.MessageWindowHandle );

			// create the taskbar icon
			CreateTaskbarIcon();

			// register event listeners
			MessageSink.MouseEventReceived        += OnMouseEvent;
			MessageSink.TaskbarCreated            += OnTaskbarCreated;
			MessageSink.ChangeToolTipStateRequest += OnToolTipChange;
			MessageSink.BalloonToolTipChanged     += OnBalloonToolTipChanged;

			// init single click / balloon timers
			SingleClickTimer  = new Timer( DoSingleClickAction! );
			BalloonCloseTimer = new Timer( CloseBalloonCallback! );

			// register listener in order to get notified when the application closes
			if( Application.Current is not null )
				Application.Current.Exit += OnExit;
		}
	#endregion

		private readonly object LockObject = new();

	#region Process Incoming Mouse Events
		/// <summary>
		///     Processes mouse events, which are bubbled
		///     through the class' routed events, trigger
		///     certain actions (e.g. show a popup), or
		///     both.
		/// </summary>
		/// <param name="me">Event flag.</param>
		private void OnMouseEvent( MouseEvent me )
		{
			if( IsDisposed ) return;

			switch( me )
			{
			case MouseEvent.MOUSE_MOVE:
				RaiseTrayMouseMoveEvent();

				// immediately return - there's nothing left to evaluate
				return;

			case MouseEvent.ICON_RIGHT_MOUSE_DOWN:
				RaiseTrayRightMouseDownEvent();
				break;

			case MouseEvent.ICON_LEFT_MOUSE_DOWN:
				RaiseTrayLeftMouseDownEvent();
				break;

			case MouseEvent.ICON_RIGHT_MOUSE_UP:
				RaiseTrayRightMouseUpEvent();
				break;

			case MouseEvent.ICON_LEFT_MOUSE_UP:
				RaiseTrayLeftMouseUpEvent();
				break;

			case MouseEvent.ICON_MIDDLE_MOUSE_DOWN:
				RaiseTrayMiddleMouseDownEvent();
				break;

			case MouseEvent.ICON_MIDDLE_MOUSE_UP:
				RaiseTrayMiddleMouseUpEvent();
				break;

			case MouseEvent.ICON_DOUBLE_CLICK:
				// cancel single click timer
				SingleClickTimer.Change( Timeout.Infinite, Timeout.Infinite );

				// bubble event
				RaiseTrayMouseDoubleClickEvent();
				break;

			case MouseEvent.BALLOON_TOOL_TIP_CLICKED:
				RaiseTrayBalloonTipClickedEvent();
				break;

			default:
				throw new ArgumentOutOfRangeException( nameof( me ), "Missing handler for mouse event flag: " + me );
			}

			// get mouse coordinates
			var CursorPosition = new Point();

			if( MessageSink.Version == NotifyIconVersion.VISTA )
			{
				// physical cursor position is supported for Vista and above
				WinApi.GetPhysicalCursorPos( ref CursorPosition );
			}
			else
				WinApi.GetCursorPos( ref CursorPosition );

			CursorPosition = TrayInfo.GetDeviceCoordinates( CursorPosition );

			var IsLeftClickCommandInvoked = false;

			// show popup, if requested
			if( me.IsMatch( PopupActivation ) )
			{
				if( me == MouseEvent.ICON_LEFT_MOUSE_UP )
				{
					// show popup once we are sure it's not a double click
					SingleClickTimerAction = () =>
					                         {
						                         LeftClickCommand.ExecuteIfEnabled( LeftClickCommandParameter, LeftClickCommandTarget );
						                         ShowTrayPopup( CursorPosition );
					                         };
					SingleClickTimer.Change( DoubleClickWaitTime, Timeout.Infinite );
					IsLeftClickCommandInvoked = true;
				}
				else
				{
					// show popup immediately
					ShowTrayPopup( CursorPosition );
				}
			}

			// show context menu, if requested
			if( me.IsMatch( MenuActivation ) )
			{
				if( me == MouseEvent.ICON_LEFT_MOUSE_UP )
				{
					// show context menu once we are sure it's not a double click
					SingleClickTimerAction = () =>
					                         {
						                         LeftClickCommand.ExecuteIfEnabled( LeftClickCommandParameter, LeftClickCommandTarget );
						                         ShowContextMenu( CursorPosition );
					                         };
					SingleClickTimer.Change( DoubleClickWaitTime, Timeout.Infinite );
					IsLeftClickCommandInvoked = true;
				}
				else
				{
					// show context menu immediately
					ShowContextMenu( CursorPosition );
				}
			}

			// make sure the left click command is invoked on mouse clicks
			if( ( me == MouseEvent.ICON_LEFT_MOUSE_UP ) && !IsLeftClickCommandInvoked )
			{
				// show context menu once we are sure it's not a double click
				SingleClickTimerAction =
					() => { LeftClickCommand.ExecuteIfEnabled( LeftClickCommandParameter, LeftClickCommandTarget ); };
				SingleClickTimer.Change( DoubleClickWaitTime, Timeout.Infinite );
			}
		}
	#endregion

	#region Context Menu
		/// <summary>
		///     Displays the <see cref="ContextMenu" /> if it was set.
		/// </summary>
		private void ShowContextMenu( Point cursorPosition )
		{
			if( IsDisposed ) return;

			// raise preview event no matter whether context menu is currently set
			// or not (enables client to set it on demand)
			var Args = RaisePreviewTrayContextMenuOpenEvent();

			if( Args is not null && Args.Handled )
				return;

			if( ContextMenu is null )
				return;

			// use absolute positioning. We need to set the coordinates, or a delayed opening
			// (e.g. when left-clicked) opens the context menu at the wrong place if the mouse
			// is moved!
			ContextMenu.Placement        = PlacementMode.AbsolutePoint;
			ContextMenu.HorizontalOffset = cursorPosition.X;
			ContextMenu.VerticalOffset   = cursorPosition.Y;
			ContextMenu.IsOpen           = true;

			var Handle = IntPtr.Zero;

			// try to get a handle on the context itself
			if( PresentationSource.FromVisual( ContextMenu ) is HwndSource Source )
				Handle = Source.Handle;

			// if we don't have a handle for the popup, fall back to the message sink
			if( Handle == IntPtr.Zero )
				Handle = MessageSink.MessageWindowHandle;

			// activate the context menu or the message window to track deactivation - otherwise, the context menu
			// does not close if the user clicks somewhere else. With the message window
			// fallback, the context menu can't receive keyboard events - should not happen though
			WinApi.SetForegroundWindow( Handle );

			// bubble event
			RaiseTrayContextMenuOpenEvent();
		}
	#endregion

	#region Single Click Timer event
		/// <summary>
		///     Performs a delayed action if the user requested an action
		///     based on a single click of the left mouse.<br />
		///     This method is invoked by the <see cref="SingleClickTimer" />.
		/// </summary>
		private void DoSingleClickAction( object state )
		{
			if( IsDisposed ) return;

			// run action
			var Action = SingleClickTimerAction;

			if( Action != null )
			{
				// cleanup action
				SingleClickTimerAction = null;

				// switch to UI thread
				this.GetDispatcher().Invoke( Action );
			}
		}
	#endregion

	#region Set Version (API)
		/// <summary>
		///     Sets the version flag for the <see cref="IconData" />.
		/// </summary>
		private void SetVersion()
		{
			IconData.VersionOrTimeout = (uint)NotifyIconVersion.VISTA;
			var Status = WinApi.Shell_NotifyIcon( NotifyCommand.SET_VERSION, ref IconData );

			if( !Status )
			{
				IconData.VersionOrTimeout = (uint)NotifyIconVersion.WIN2000;
				Status                    = Util.WriteIconData( ref IconData, NotifyCommand.SET_VERSION );
			}

			if( !Status )
			{
				IconData.VersionOrTimeout = (uint)NotifyIconVersion.WIN95;
				Status                    = Util.WriteIconData( ref IconData, NotifyCommand.SET_VERSION );
			}

			if( !Status )
				Debug.Fail( "Could not set version" );
		}
	#endregion

	#region Members
		/// <summary>
		///     Represents the current icon data.
		/// </summary>
		private NotifyIconData IconData;

		/// <summary>
		///     Receives messages from the taskbar icon.
		/// </summary>
		private readonly WindowMessageSink MessageSink;

		/// <summary>
		///     An action that is being invoked if the
		///     <see cref="SingleClickTimer" /> fires.
		/// </summary>
		private Action? SingleClickTimerAction;

		/// <summary>
		///     A timer that is used to differentiate between single
		///     and double clicks.
		/// </summary>
		private readonly Timer SingleClickTimer;

		/// <summary>
		///     The time we should wait for a double click.
		/// </summary>
		private int DoubleClickWaitTime => NoLeftClickDelay ? 0 : WinApi.GetDoubleClickTime();

		/// <summary>
		///     A timer that is used to close open balloon tooltips.
		/// </summary>
		private readonly Timer BalloonCloseTimer;

		/// <summary>
		///     Indicates whether the taskbar icon has been created or not.
		/// </summary>
		public bool IsTaskbarIconCreated { get; private set; }

		/// <summary>
		///     Indicates whether custom tooltips are supported, which depends
		///     on the OS. Windows Vista or higher is required in order to
		///     support this feature.
		/// </summary>
		public bool SupportsCustomToolTips => MessageSink.Version == NotifyIconVersion.VISTA;


		/// <summary>
		///     Checks whether a non-tooltip popup is currently opened.
		/// </summary>
		private bool IsPopupOpen
		{
			get
			{
				var Popup   = TrayPopupResolved;
				var Menu    = ContextMenu;
				var Balloon = CustomBalloon;

				return ( Popup is not null && Popup.IsOpen ) || ( Menu is not null && Menu.IsOpen ) || ( Balloon is not null && Balloon.IsOpen );
			}
		}
	#endregion

	#region Custom Balloons
		/// <summary>
		///     A delegate to handle customer popup positions.
		/// </summary>
		public delegate Point GetCustomPopupPosition();

		/// <summary>
		///     Specify a custom popup position
		/// </summary>
		public GetCustomPopupPosition? CustomPopupPosition { get; set; }

		/// <summary>
		///     Returns the location of the system tray
		/// </summary>
		/// <returns>Point</returns>
		public static Point GetPopupTrayPosition() => TrayInfo.GetTrayLocation();

		/// <summary>
		///     Shows a custom control as a tooltip in the tray location.
		/// </summary>
		/// <param name="balloon"></param>
		/// <param name="animation">An optional animation for the popup.</param>
		/// <param name="timeout">
		///     The time after which the popup is being closed.
		///     Submit null in order to keep the balloon open indefinitely
		/// </param>
		/// <exception cref="ArgumentNullException">
		///     If <paramref name="balloon" />
		///     is a null reference.
		/// </exception>
		public void ShowCustomBalloon( UIElement balloon, PopupAnimation animation, int? timeout )
		{
			var Dispatcher1 = this.GetDispatcher();

			if( !Dispatcher1.CheckAccess() )
			{
				var Action = new Action( () => ShowCustomBalloon( balloon, animation, timeout ) );
				Dispatcher1.Invoke( DispatcherPriority.Normal, Action );
				return;
			}

			if( balloon == null ) throw new ArgumentNullException( nameof( balloon ) );

			if( timeout.HasValue && ( timeout < 500 ) )
			{
				var Msg = "Invalid timeout of {0} milliseconds. Timeout must be at least 500 ms";
				Msg = string.Format( Msg, timeout );
				throw new ArgumentOutOfRangeException( nameof( timeout ), Msg );
			}

			EnsureNotDisposed();

			// make sure we don't have an open balloon
			lock( LockObject )
				CloseBalloon();

			// create an invisible popup that hosts the UIElement
			var Popup = new Popup
			            {
				            AllowsTransparency = true
			            };

			// provide the popup with the taskbar icon's data context
			UpdateDataContext( Popup, null, DataContext );

			// don't animate by default - developers can use attached events or override
			Popup.PopupAnimation = animation;

			// in case the balloon is cleaned up through routed events, the
			// control didn't remove the balloon from its parent popup when
			// if was closed the last time - just make sure it doesn't have
			// a parent that is a popup

			if( LogicalTreeHelper.GetParent( balloon ) is Popup Parent1 )
			{
				Parent1.Child = null;
				var Msg = "Cannot display control [{0}] in a new balloon popup - that control already has a parent. You may consider creating new balloons every time you want to show one.";
				Msg = string.Format( Msg, balloon );
				throw new InvalidOperationException( Msg );
			}

			Popup.Child = balloon;

			//don't set the PlacementTarget as it causes the popup to become hidden if the
			//TaskbarIcon's parent is hidden, too...
			//popup.PlacementTarget = this;

			Popup.Placement = PlacementMode.AbsolutePoint;
			Popup.StaysOpen = true;

			var Position = CustomPopupPosition?.Invoke() ?? GetPopupTrayPosition();
			Popup.HorizontalOffset = Position.X - 1;
			Popup.VerticalOffset   = Position.Y - 1;

			//store reference
			lock( LockObject )
				SetCustomBalloon( Popup );

			// assign this instance as an attached property
			SetParentTaskbarIcon( balloon, this );

			// fire attached event
			RaiseBalloonShowingEvent( balloon, this );

			// display item
			Popup.IsOpen = true;

			if( timeout.HasValue )
			{
				// register timer to close the popup
				BalloonCloseTimer.Change( timeout.Value, Timeout.Infinite );
			}
		}


		/// <summary>
		///     Resets the closing timeout, which effectively
		///     keeps a displayed balloon message open until
		///     it is either closed programmatically through
		///     <see cref="CloseBalloon" /> or due to a new
		///     message being displayed.
		/// </summary>
		public void ResetBalloonCloseTimer()
		{
			if( IsDisposed ) return;

			lock( LockObject )

				//reset timer in any case
				BalloonCloseTimer.Change( Timeout.Infinite, Timeout.Infinite );
		}


		/// <summary>
		///     Closes the current <see cref="CustomBalloon" />, if the
		///     property is set.
		/// </summary>
		public void CloseBalloon()
		{
			if( IsDisposed ) return;

			var Dispatcher1 = this.GetDispatcher();

			if( !Dispatcher1.CheckAccess() )
			{
				Action Action = CloseBalloon;
				Dispatcher1.Invoke( DispatcherPriority.Normal, Action );
				return;
			}

			lock( LockObject )
			{
				// reset timer in any case
				BalloonCloseTimer.Change( Timeout.Infinite, Timeout.Infinite );

				// reset old popup, if we still have one
				var Popup = CustomBalloon;

				var Element = Popup?.Child;

				// announce closing
				var EventArgs = RaiseBalloonClosingEvent( Element, this );

				if( EventArgs is not null && !EventArgs.Handled )
				{
					// if the event was handled, clear the reference to the popup,
					// but don't close it - the handling code has to manage this stuff now

					// close the popup
					if( Popup is not null )
					{
						Popup.IsOpen = false;

						// remove the reference of the popup to the balloon in case we want to reuse
						// the balloon (then added to a new popup)
						Popup.Child = null;
					}

					// reset attached property
					if( Element is not null )
						SetParentTaskbarIcon( Element, null );
				}

				// remove custom balloon anyway
				SetCustomBalloon( null );
			}
		}


		/// <summary>
		///     Timer-invoke event which closes the currently open balloon and
		///     resets the <see cref="CustomBalloon" /> dependency property.
		/// </summary>
		private void CloseBalloonCallback( object state )
		{
			if( IsDisposed ) return;

			// switch to UI thread
			Action Action = CloseBalloon;
			this.GetDispatcher().Invoke( Action );
		}
	#endregion

	#region ToolTips
		/// <summary>
		///     Displays a custom tooltip, if available. This method is only
		///     invoked for Windows Vista and above.
		/// </summary>
		/// <param name="visible">Whether to show or hide the tooltip.</param>
		private void OnToolTipChange( bool visible )
		{
			// if we don't have a tooltip, there's nothing to do here...

			if( visible )
			{
				if( IsPopupOpen )
				{
					// ignore if we are already displaying something down there
					return;
				}

				var Args = RaisePreviewTrayToolTipOpenEvent();

				if( Args is not null && Args.Handled )
					return;

				var Tr = TrayToolTipResolved;

				if( Tr is not null )
					Tr.IsOpen = true;

				// raise attached event first
				RaiseToolTipOpenedEvent( TrayToolTip );

				// bubble routed event
				RaiseTrayToolTipOpenEvent();
			}
			else
			{
				var Args = RaisePreviewTrayToolTipCloseEvent();

				if( Args is not null && Args.Handled )
					return;

				// raise attached event first
				RaiseToolTipCloseEvent( TrayToolTip );

				var Tr = TrayToolTipResolved;

				if( Tr is not null )
					Tr.IsOpen = false;

				// bubble event
				RaiseTrayToolTipCloseEvent();
			}
		}


		/// <summary>
		///     Creates a <see cref="ToolTip" /> control that either
		///     wraps the currently set <see cref="TrayToolTip" />
		///     control or the <see cref="ToolTipText" /> string.<br />
		///     If <see cref="TrayToolTip" /> itself is already
		///     a <see cref="ToolTip" /> instance, it will be used directly.
		/// </summary>
		/// <remarks>
		///     We use a <see cref="ToolTip" /> rather than
		///     <see cref="Popup" /> because there was no way to prevent a
		///     popup from causing cyclic open/close commands if it was
		///     placed under the mouse. ToolTip internally uses a Popup of
		///     its own, but takes advance of Popup's internal <see cref="UIElement.IsHitTestVisible" />
		///     property which prevents this issue.
		/// </remarks>
		private void CreateCustomToolTip()
		{
			// check if the item itself is a tooltip
			var Tt = TrayToolTip as ToolTip;

			switch( Tt )
			{
			case null when TrayToolTip != null:
				// create an invisible wrapper tooltip that hosts the UIElement
				Tt = new ToolTip
				     {
					     Placement = PlacementMode.Mouse,

					     // do *not* set the placement target, as it causes the popup to become hidden if the
					     // TaskbarIcon's parent is hidden, too. At runtime, the parent can be resolved through
					     // the ParentTaskbarIcon attached dependency property:
					     // PlacementTarget = this;

					     // make sure the tooltip is invisible
					     HasDropShadow   = false,
					     BorderThickness = new Thickness( 0 ),
					     Background      = Brushes.Transparent,

					     // setting the 
					     StaysOpen = true,
					     Content   = TrayToolTip
				     };
				break;

			default:
				if( !string.IsNullOrWhiteSpace( ToolTipText ) )
				{
					// create a simple tooltip for the ToolTipText string
					Tt = new ToolTip
					     {
						     Content = ToolTipText
					     };
				}
				break;
			}

			// the tooltip explicitly gets the DataContext of this instance.
			// If there is no DataContext, the TaskbarIcon assigns itself
			UpdateDataContext( Tt, null, DataContext );

			// store a reference to the used tooltip
			SetTrayToolTipResolved( Tt );
		}


		/// <summary>
		///     Sets tooltip settings for the class depending on defined
		///     dependency properties and OS support.
		/// </summary>
		private void WriteToolTipSettings()
		{
			const IconDataMembers FLAGS = IconDataMembers.TIP;
			IconData.ToolTipText = ToolTipText;

			if( MessageSink.Version == NotifyIconVersion.VISTA )
			{
				// we need to set a tooltip text to get tooltip events from the
				// taskbar icon
				if( string.IsNullOrEmpty( IconData.ToolTipText ) )
				{
					// if we have not tooltip text but a custom tooltip, we
					// need to set a dummy value (we're displaying the ToolTip control, not the string)
					IconData.ToolTipText = "ToolTip";
				}
			}

			// update the tooltip text
			Util.WriteIconData( ref IconData, NotifyCommand.MODIFY, FLAGS );
		}
	#endregion

	#region Custom Popup
		/// <summary>
		///     Creates a <see cref="ToolTip" /> control that either
		///     wraps the currently set <see cref="TrayToolTip" />
		///     control or the <see cref="ToolTipText" /> string.<br />
		///     If <see cref="TrayToolTip" /> itself is already
		///     a <see cref="ToolTip" /> instance, it will be used directly.
		/// </summary>
		/// <remarks>
		///     We use a <see cref="ToolTip" /> rather than
		///     <see cref="Popup" /> because there was no way to prevent a
		///     popup from causing cyclic open/close commands if it was
		///     placed under the mouse. ToolTip internally uses a Popup of
		///     its own, but takes advance of Popup's internal <see cref="UIElement.IsHitTestVisible" />
		///     property which prevents this issue.
		/// </remarks>
		private void CreatePopup()
		{
			// check if the item itself is a popup

			if( TrayPopup is not Popup Popup  )
			{
				// create an invisible popup that hosts the UIElement
				Popup = new Popup
				        {
					        AllowsTransparency = true,

					        // don't animate by default - developers can use attached events or override
					        PopupAnimation = PopupAnimation.None,

					        // the CreateRootPopup method outputs binding errors in the debug window because
					        // it tries to bind to "Popup-specific" properties in case they are provided by the child.
					        // We don't need that so just assign the control as the child.
					        Child = TrayPopup,

					        // do *not* set the placement target, as it causes the popup to become hidden if the
					        // TaskbarIcon's parent is hidden, too. At runtime, the parent can be resolved through
					        // the ParentTaskbarIcon attached dependency property:
					        // PlacementTarget = this;

					        Placement = PlacementMode.AbsolutePoint,
					        StaysOpen = false
				        };
			}

			// the popup explicitly gets the DataContext of this instance.
			// If there is no DataContext, the TaskbarIcon assigns itself
			UpdateDataContext( Popup, null, DataContext );

			// store a reference to the used tooltip
			SetTrayPopupResolved( Popup );
		}


		/// <summary>
		///     Displays the <see cref="TrayPopup" /> control if it was set.
		/// </summary>
		private void ShowTrayPopup( Point cursorPosition )
		{
			if( IsDisposed ) return;

			// raise preview event no matter whether popup is currently set
			// or not (enables client to set it on demand)
			var Args = RaisePreviewTrayPopupOpenEvent();

			if( Args is not null && Args.Handled )
				return;

			// use absolute position, but place the popup centered above the icon
			if( TrayPopupResolved is not null )
			{
				TrayPopupResolved.Placement        = PlacementMode.AbsolutePoint;
				TrayPopupResolved.HorizontalOffset = cursorPosition.X;
				TrayPopupResolved.VerticalOffset   = cursorPosition.Y;

				// open popup
				TrayPopupResolved.IsOpen = true;

				var Handle = IntPtr.Zero;

				if( TrayPopupResolved.Child is not null )
				{
					// try to get a handle on the popup itself (via its child)
					if( PresentationSource.FromVisual( TrayPopupResolved.Child ) is HwndSource Source )
						Handle = Source.Handle;
				}

				// if we don't have a handle for the popup, fall back to the message sink
				if( Handle == IntPtr.Zero ) Handle = MessageSink.MessageWindowHandle;

				// activate either popup or message sink to track deactivation.
				// otherwise, the popup does not close if the user clicks somewhere else
				WinApi.SetForegroundWindow( Handle );
			}

			// raise attached event - item should never be null unless developers
			// changed the CustomPopup directly...
			RaisePopupOpenedEvent( TrayPopup );

			// bubble routed event
			RaiseTrayPopupOpenEvent();
		}
	#endregion

	#region Balloon Tips
		/// <summary>
		///     Bubbles events if a balloon ToolTip was displayed
		///     or removed.
		/// </summary>
		/// <param name="visible">
		///     Whether the ToolTip was just displayed
		///     or removed.
		/// </param>
		private void OnBalloonToolTipChanged( bool visible )
		{
			if( visible )
				RaiseTrayBalloonTipShownEvent();
			else
				RaiseTrayBalloonTipClosedEvent();
		}

		/// <summary>
		///     Displays a balloon tip with the specified title,
		///     text, and icon in the taskbar for the specified time period.
		/// </summary>
		/// <param name="title">The title to display on the balloon tip.</param>
		/// <param name="message">The text to display on the balloon tip.</param>
		/// <param name="symbol">A symbol that indicates the severity.</param>
		public void ShowBalloonTip( string title, string message, BalloonIcon symbol )
		{
			lock( LockObject )
				ShowBalloonTip( title, message, symbol.GetBalloonFlag(), IntPtr.Zero );
		}

		/// <summary>
		///     Displays a balloon tip with the specified title,
		///     text, and a custom icon in the taskbar for the specified time period.
		/// </summary>
		/// <param name="title">The title to display on the balloon tip.</param>
		/// <param name="message">The text to display on the balloon tip.</param>
		/// <param name="customIcon">A custom icon.</param>
		/// <param name="largeIcon">True to allow large icons (Windows Vista and later).</param>
		/// <exception cref="ArgumentNullException">
		///     If <paramref name="customIcon" />
		///     is a null reference.
		/// </exception>
		public void ShowBalloonTip( string title, string message, Icon customIcon, bool largeIcon = false )
		{
			if( customIcon == null ) throw new ArgumentNullException( nameof( customIcon ) );

			lock( LockObject )
			{
				var Flags = BalloonFlags.USER;

				if( largeIcon )
				{
					// ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
					Flags |= BalloonFlags.LARGE_ICON;
				}

				ShowBalloonTip( title, message, Flags, customIcon.Handle );
			}
		}


		/// <summary>
		///     Invokes <see cref="WinApi.Shell_NotifyIcon" /> in order to display
		///     a given balloon ToolTip.
		/// </summary>
		/// <param name="title">The title to display on the balloon tip.</param>
		/// <param name="message">The text to display on the balloon tip.</param>
		/// <param name="flags">Indicates what icon to use.</param>
		/// <param name="balloonIconHandle">
		///     A handle to a custom icon, if any, or
		///     <see cref="IntPtr.Zero" />.
		/// </param>
		private void ShowBalloonTip( string title, string message, BalloonFlags flags, IntPtr balloonIconHandle )
		{
			EnsureNotDisposed();

			IconData.BalloonText  = message;
			IconData.BalloonTitle = title;

			IconData.BalloonFlags            = flags;
			IconData.CustomBalloonIconHandle = balloonIconHandle;
			Util.WriteIconData( ref IconData, NotifyCommand.MODIFY, IconDataMembers.INFO | IconDataMembers.ICON );
		}


		/// <summary>
		///     Hides a balloon ToolTip, if any is displayed.
		/// </summary>
		public void HideBalloonTip()
		{
			EnsureNotDisposed();

			// reset balloon by just setting the info to an empty string
			IconData.BalloonText = IconData.BalloonTitle = string.Empty;
			Util.WriteIconData( ref IconData, NotifyCommand.MODIFY, IconDataMembers.INFO );
		}
	#endregion

	#region Create / Remove Taskbar Icon
		/// <summary>
		///     Recreates the taskbar icon if the whole taskbar was
		///     recreated (e.g. because Explorer was shut down).
		/// </summary>
		private void OnTaskbarCreated()
		{
			IsTaskbarIconCreated = false;
			CreateTaskbarIcon();
		}


		/// <summary>
		///     Creates the taskbar icon. This message is invoked during initialization,
		///     if the taskbar is restarted, and whenever the icon is displayed.
		/// </summary>
		private void CreateTaskbarIcon()
		{
			lock( LockObject )
			{
				if( IsTaskbarIconCreated )
					return;

				const IconDataMembers MEMBERS = IconDataMembers.MESSAGE
				                                | IconDataMembers.ICON
				                                | IconDataMembers.TIP;

				//write initial configuration
				var Status = Util.WriteIconData( ref IconData, NotifyCommand.ADD, MEMBERS );

				if( !Status )
				{
					// couldn't create the icon - we can assume this is because explorer is not running (yet!)
					// -> try a bit later again rather than throwing an exception. Typically, if the windows
					// shell is being loaded later, this method is being re-invoked from OnTaskbarCreated
					// (we could also retry after a delay, but that's currently YAGNI)
					return;
				}

				//set to most recent version
				SetVersion();
				MessageSink.Version = (NotifyIconVersion)IconData.VersionOrTimeout;

				IsTaskbarIconCreated = true;
			}
		}

		/// <summary>
		///     Closes the taskbar icon if required.
		/// </summary>
		private void RemoveTaskbarIcon()
		{
			lock( LockObject )
			{
				// make sure we didn't schedule a creation

				if( !IsTaskbarIconCreated )
					return;

				Util.WriteIconData( ref IconData, NotifyCommand.DELETE, IconDataMembers.MESSAGE );
				IsTaskbarIconCreated = false;
			}
		}
	#endregion


	#region Dispose / Exit
		/// <summary>
		///     Set to true as soon as <c>Dispose</c> has been invoked.
		/// </summary>
		public bool IsDisposed { get; private set; }


		/// <summary>
		///     Checks if the object has been disposed and
		///     raises a <see cref="ObjectDisposedException" /> in case
		///     the <see cref="IsDisposed" /> flag is true.
		/// </summary>
		private void EnsureNotDisposed()
		{
			if( IsDisposed ) throw new ObjectDisposedException( Name ?? GetType().FullName );
		}


		/// <summary>
		///     Disposes the class if the application exits.
		/// </summary>
		private void OnExit( object sender, EventArgs e )
		{
			Dispose();
		}


		/// <summary>
		///     This destructor will run only if the <see cref="Dispose()" />
		///     method does not get called. This gives this base class the
		///     opportunity to finalize.
		///     <para>
		///         Important: Do not provide destructor in types derived from this class.
		///     </para>
		/// </summary>
		~TaskbarIcon()
		{
			Dispose( false );
		}


		/// <summary>
		///     Disposes the object.
		/// </summary>
		/// <remarks>
		///     This method is not virtual by design. Derived classes
		///     should override <see cref="Dispose(bool)" />.
		/// </remarks>
		public void Dispose()
		{
			Dispose( true );

			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SuppressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize( this );
		}


		/// <summary>
		///     Closes the tray and releases all resources.
		/// </summary>
		/// <summary>
		///     <c>Dispose(bool disposing)</c> executes in two distinct scenarios.
		///     If disposing equals <c>true</c>, the method has been called directly
		///     or indirectly by a user's code. Managed and unmanaged resources
		///     can be disposed.
		/// </summary>
		/// <param name="disposing">
		///     If disposing equals <c>false</c>, the method
		///     has been called by the runtime from inside the finalizer and you
		///     should not reference other objects. Only unmanaged resources can
		///     be disposed.
		/// </param>
		/// <remarks>
		///     Check the <see cref="IsDisposed" /> property to determine whether
		///     the method has already been called.
		/// </remarks>
		private void Dispose( bool disposing )
		{
			// don't do anything if the component is already disposed
			if( IsDisposed || !disposing ) return;

			lock( LockObject )
			{
				IsDisposed = true;

				// de-register application event listener
				if( Application.Current != null )
					Application.Current.Exit -= OnExit;

				// stop timers
				SingleClickTimer.Dispose();
				BalloonCloseTimer.Dispose();

				// dispose message sink
				MessageSink.Dispose();

				// remove icon
				RemoveTaskbarIcon();
			}
		}
	#endregion
	}
}