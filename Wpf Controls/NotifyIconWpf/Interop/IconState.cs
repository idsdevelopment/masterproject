#nullable enable

namespace Wpf.TaskbarNotification.Interop
{
	/// <summary>
	///     The state of the icon - can be set to
	///     hide the icon.
	/// </summary>
	public enum IconState
	{
		/// <summary>
		///     The icon is visible.
		/// </summary>
		VISIBLE = 0x00,

		/// <summary>
		///     Hide the icon.
		/// </summary>
		HIDDEN = 0x01

		// The icon is shared - currently not supported, thus commented out.
		//Shared = 0x02
	}
}