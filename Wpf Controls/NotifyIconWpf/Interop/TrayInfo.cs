﻿#nullable enable

// Some interop code taken from Mike Marshall's AnyForm

namespace Wpf.TaskbarNotification.Interop
{
	/// <summary>
	///     Resolves the current tray position.
	/// </summary>
	public static class TrayInfo
	{
		/// <summary>
		///     Gets the position of the system tray.
		/// </summary>
		/// <returns>Tray coordinates.</returns>
		public static Point GetTrayLocation()
		{
			const int SPACE = 2;
			var       Info  = new AppBarInfo();
			Info.GetSystemTaskBarPosition();

			var RcWorkArea = Info.WorkArea;

			int X = 0,
			    Y = 0;

			switch( Info.Edge )
			{
			case AppBarInfo.ScreenEdge.LEFT:
				X = RcWorkArea.Right + SPACE;
				Y = RcWorkArea.Bottom;
				break;

			case AppBarInfo.ScreenEdge.BOTTOM:
				X = RcWorkArea.Right;
				Y = RcWorkArea.Bottom - RcWorkArea.Height - SPACE;
				break;

			case AppBarInfo.ScreenEdge.TOP:
				X = RcWorkArea.Right;
				Y = RcWorkArea.Top + RcWorkArea.Height + SPACE;
				break;

			case AppBarInfo.ScreenEdge.RIGHT:
				X = RcWorkArea.Right - RcWorkArea.Width - SPACE;
				Y = RcWorkArea.Bottom;
				break;
			}

			return GetDeviceCoordinates( new Point { X = X, Y = Y } );
		}

		/// <summary>
		///     Recalculates OS coordinates in order to support WPFs coordinate
		///     system if OS scaling (DPIs) is not 100%.
		/// </summary>
		/// <param name="point">Point</param>
		/// <returns>Point</returns>
		public static Point GetDeviceCoordinates( Point point )
		{
			var X = SystemInfo.DpiFactorX;

			if( X is 0 )
				X = 1;

			var Y = SystemInfo.DpiFactorY;

			if( Y is 0 )
				Y = 1;

			return new Point
			       {
				       X = (int)( point.X / X ),
				       Y = (int)( point.Y / Y )
			       };
		}
	}
}