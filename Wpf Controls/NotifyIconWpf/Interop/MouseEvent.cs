﻿#nullable enable

namespace Wpf.TaskbarNotification.Interop
{
    /// <summary>
    /// Event flags for clicked events.
    /// </summary>
    public enum MouseEvent
    {
        /// <summary>
        /// The mouse was moved withing the
        /// taskbar icon's area.
        /// </summary>
        MOUSE_MOVE,

        /// <summary>
        /// The right mouse button was clicked.
        /// </summary>
        ICON_RIGHT_MOUSE_DOWN,

        /// <summary>
        /// The left mouse button was clicked.
        /// </summary>
        ICON_LEFT_MOUSE_DOWN,

        /// <summary>
        /// The right mouse button was released.
        /// </summary>
        ICON_RIGHT_MOUSE_UP,

        /// <summary>
        /// The left mouse button was released.
        /// </summary>
        ICON_LEFT_MOUSE_UP,

        /// <summary>
        /// The middle mouse button was clicked.
        /// </summary>
        ICON_MIDDLE_MOUSE_DOWN,

        /// <summary>
        /// The middle mouse button was released.
        /// </summary>
        ICON_MIDDLE_MOUSE_UP,

        /// <summary>
        /// The taskbar icon was double clicked.
        /// </summary>
        ICON_DOUBLE_CLICK,

        /// <summary>
        /// The balloon tip was clicked.
        /// </summary>
        BALLOON_TOOL_TIP_CLICKED
    }
}