﻿#nullable enable

// Some interop code taken from Mike Marshall's AnyForm

using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Wpf.TaskbarNotification.Interop
{
	/// <summary>
	///     This contains the logic to access the location of the app bar and communicate with it.
	/// </summary>
	public class AppBarInfo
	{
/*        
        [DllImport("user32.dll")]
        private static extern int SystemParametersInfo(uint uiAction, uint uiParam,
            IntPtr pvParam, uint fWinIni);
*/
		private const int ABM_GETTASKBARPOS = 0x00000005;

		/// <summary>
		///     A value that specifies an edge of the screen.
		/// </summary>
		public enum ScreenEdge
		{
			/// <summary>
			///     Undefined
			/// </summary>
			UNDEFINED = -1,

			/// <summary>
			///     Left edge.
			/// </summary>
			LEFT = 0,

			/// <summary>
			///     Top edge.
			/// </summary>
			TOP = 1,

			/// <summary>
			///     Right edge.
			/// </summary>
			RIGHT = 2,

			/// <summary>
			///     Bottom edge.
			/// </summary>
			BOTTOM = 3
		}

		/// <summary>
		///     Get on which edge the app bar is located
		/// </summary>
		public ScreenEdge Edge => (ScreenEdge)MData.uEdge;

		/// <summary>
		///     Get the working area
		/// </summary>
		public Rectangle WorkArea => GetRectangle( MData.rc );


		private Appbardata MData;

		[StructLayout( LayoutKind.Sequential )]
		private struct Appbardata
		{
			public           uint   cbSize;
			private readonly IntPtr hWnd;
			private readonly uint   uCallbackMessage;
			public readonly  uint   uEdge;
			public readonly  Rect   rc;
			private readonly int    lParam;
		}


		[StructLayout( LayoutKind.Sequential )]
		private readonly struct Rect
		{
			public readonly int left;
			public readonly int top;
			public readonly int right;
			public readonly int bottom;
		}

		[DllImport( "user32.dll", CharSet = CharSet.Unicode )]
		private static extern IntPtr FindWindow( string lpClassName, string lpWindowName );

		[DllImport( "shell32.dll" )]
		private static extern uint SHAppBarMessage( uint dwMessage, ref Appbardata data );

		private static Rectangle GetRectangle( Rect rc ) => new( rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top );

		/// <summary>
		///     Update the location of the appbar with the specified classname and window name.
		/// </summary>
		/// <param name="strClassName">string</param>
		/// <param name="strWindowName">string</param>
		private void GetPosition( string strClassName, string strWindowName )
		{
			MData        = new Appbardata();
			MData.cbSize = (uint)Marshal.SizeOf( MData.GetType() );

			var HWnd = FindWindow( strClassName, strWindowName );

			if( HWnd != IntPtr.Zero )
			{
				var UResult = SHAppBarMessage( ABM_GETTASKBARPOS, ref MData );

				if( UResult != 1 )
					throw new Exception( "Failed to communicate with the given AppBar" );
			}
			else
				throw new Exception( "Failed to find an AppBar that matched the given criteria" );
		}

		/// <summary>
		///     Updates the system taskbar position
		/// </summary>
		public void GetSystemTaskBarPosition()
		{
			GetPosition( "Shell_TrayWnd", null! );
		}
	}
}