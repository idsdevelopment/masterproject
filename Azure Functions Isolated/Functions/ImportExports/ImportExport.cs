namespace AzureFunctionsIsolated;

// ReSharper disable once InconsistentNaming
public class ImportExport
{
	public ImportExport( ILoggerFactory _ )
	{
	}

	[Function( nameof( ImportExport ) )]
	public async Task Run( [TimerTrigger( "0 * * * * *" )] TimerInfo myTimer )
	{
		var (Ok, Client) = await Globals.Login();

		if( Ok )
			await Client.RequestCheckForImportsAndExports();
	}
}