// ReSharper disable UnusedParameter.Local

namespace AzureFunctionsIsolated;

// ReSharper disable once InconsistentNaming
public class CheckIds1Connection
{
	private readonly ILogger Logger;

	public CheckIds1Connection( ILoggerFactory logger )
	{
		Logger = logger.CreateLogger<CheckIds1Connection>();
	}

	[Function( nameof( CheckIds1Connection ) )]
	public async Task Run( [TimerTrigger( "0 */10 * * * *" )] TimerInfo myTimer )
	{
		var (Ok, Client) = await Globals.Login();

		if( Ok )
			await Client.RequestCheckMetroIds1Connection();
	}
}