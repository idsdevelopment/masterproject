﻿using AzureFunctionsIsolated.Functions.Customers.Pml;
using Protocol.Data;
using Utils;

// ReSharper disable UnusedParameter.Local

namespace AzureFunctionsIsolated;

public class CleanupOldVerifiedTrips
{
	public CleanupOldVerifiedTrips( ILoggerFactory _ )
	{
	}

	[Function( nameof( CleanupOldVerifiedTrips ) )]
	public async Task Run( [TimerTrigger( "0 0 * * * *" )] TimerInfo myTimer )
	{
		var (Ok, Client) = await new PmlLogin().Login();

		if( Ok )
		{
			var OzTime = Globals.Timezones.AusEasternStandardTime;

			var Date = OzTime.EndOfDay().AddMonths( -1 );

			var Text = $"Finalising Satchels On/Before: {Date:f}\r\n\r\n";

			var IdListAndProgram = await Client.RequestFinaliseManuallyVerifiedSatchels( Date );

			var Count = IdListAndProgram.TripIds.Count;

			if( Count > 0 )
			{
				Text += $"Finalised Satchels ({Count}):\r\n";

				foreach( var TripId in IdListAndProgram.TripIds )
					Text += $"{TripId}\r\n";
			}
			else
				Text += "No Satchels Finalised\r\n";

			var Message = new WebLog
			              {
				              CarrierId   = PmlLogin.CARRIER_ID.Capitalise(),
				              Program     = IdListAndProgram.Program.Capitalise(),
				              Message     = Text,
				              LogDateTime = OzTime
			              };

			await Client.RequestLogWebJob( Message );
		}
	}
}