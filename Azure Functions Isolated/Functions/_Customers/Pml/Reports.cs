using AzureFunctionsIsolated.Functions.Customers.Pml;

// ReSharper disable UnusedParameter.Local

namespace AzureFunctionsIsolated;

// ReSharper disable once InconsistentNaming
public class Ccb2BReport
{
	private readonly ILogger Logger;

	public Ccb2BReport( ILoggerFactory loggerFactory )
	{
		Logger = loggerFactory.CreateLogger<Ccb2BReport>();
	}

	[Function( nameof( Ccb2BReport ) )]
	public async Task Run( [TimerTrigger( "0 1 * * * *" )] TimerInfo myTimer )
	{
		var (Ok, Client) = await new PmlLogin().Login();

		Logger.Log( "Pml CCB2B Report.", Ok, Client );

		if( Ok )
			await Client.RequestPML_ExportToCCB2B();
	}
}