﻿namespace AzureFunctionsIsolated.Functions.Customers.Pml;

public class PmlLogin
{
	public const string CARRIER_ID = "pml",
	                    USERNAME   = "foxlin789",
	                    PASSWORD   = "390equator";

	public virtual string CarrierId => CARRIER_ID;
	public Task<LOGIN_RESULT> Login() => Globals.Login( CarrierId, USERNAME, PASSWORD );
}