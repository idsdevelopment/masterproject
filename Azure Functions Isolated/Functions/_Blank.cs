namespace AzureFunctionsIsolated;

// ReSharper disable once InconsistentNaming
public class _Blank
{
	private readonly ILogger Logger;

	public _Blank( ILoggerFactory loggerFactory )
	{
		Logger = loggerFactory.CreateLogger<_Blank>();
	}

	[Function( nameof( _Blank ) )]
	public void Run( [TimerTrigger( "0 */5 * * * *" )] TimerInfo myTimer )
	{
		Logger.LogInformation( $"C# Timer trigger function executed at: {DateTime.Now}" );

		if( myTimer.ScheduleStatus is not null )
			Logger.LogInformation( $"Next timer schedule at: {myTimer.ScheduleStatus.Next}" );
	}
}