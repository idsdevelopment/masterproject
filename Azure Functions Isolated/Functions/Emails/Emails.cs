﻿// ReSharper disable UnusedParameter.Local

namespace AzureFunctionsIsolated;

public class SendEmails
{
	private readonly ILogger Logger;

	public SendEmails( ILoggerFactory loggerFactory )
	{
		Logger = loggerFactory.CreateLogger<SendEmails>();
	}

	[Function( nameof( SendEmails ) )]
	public async Task Run( [TimerTrigger( "30 * * * * *" )] TimerInfo myTimer )
	{
		var (Ok, Client) = await Globals.Login();

		Logger.Log( "Send Emails.", Ok, Client );

		if( Ok )
			await Client.RequestProcessEmails();
	}
}

// ReSharper disable once InconsistentNaming
public class CleanupEmails
{
	public CleanupEmails( ILoggerFactory _ )
	{
	}

	[Function( nameof( CleanupEmails ) )]
	public async Task Run( [TimerTrigger( "0 0 8 * * *" )] TimerInfo myTimer )
	{
		var (Ok, Client) = await Globals.Login();

		if( Ok )
			await Client.RequestCleanupSentEmails();
	}
}