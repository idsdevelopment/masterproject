﻿namespace AzureFunctionsIsolated;

internal static class Extensions
{
	public static void Log( this ILogger logger, string message )
	{
		logger.LogInformation( $"""

		                        -----------------------------------
		                        {DateTime.Now} {message}.
		                        -----------------------------------
		                        """ );
	}

	public static void Log( this ILogger logger, string message, bool loginOk )
	{
		Log( logger, $"""
		              {message}.
		              Login Status: {( loginOk ? "Ok" : "Fail" )}
		              """ );
	}

	public static void Log( this ILogger logger, string message, bool loginOk, InternalClient client )
	{
		Log( logger, $"""
		              {message}
		              Url: {client.BaseAddress}
		              """, loginOk );
	}
}