﻿namespace AzureFunctionsIsolated;

public partial class Globals
{
	public const string SCHEDULES = "Schedules";

	public static async Task<LOGIN_RESULT> Login()
	{
		try
		{
			var Ok = await MultiSession.LogInDelayed( SCHEDULES );

			if( Ok )
			{
				var Client = MultiSession.Client[ "" ];
				return ( Client is not null, Client! );
			}
		}
		catch( Exception E )
		{
			Debug.WriteLine( E );
		}
		return ( false, null! );
	}

	public static async Task<LOGIN_RESULT> Login( string carrierId, string userName, string password )
	{
		try
		{
			var Ok = await MultiSession.LogInDelayed( SCHEDULES, userName, carrierId, password );

			if( Ok )
			{
				var Client = MultiSession.Client[ carrierId ];
				return ( Client is not null, Client! );
			}
		}
		catch( Exception E )
		{
			Debug.WriteLine( E );
		}
		return ( false, null! );
	}
}