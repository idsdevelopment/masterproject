// Global using directives

global using System.Diagnostics;
global using Microsoft.Azure.Functions.Worker;
global using Microsoft.Extensions.Logging;
global using static AzureRemoteService.Azure;
global using LOGIN_RESULT = (bool Ok, AzureRemoteService.Azure.InternalClient Client);