using Microsoft.Azure.Functions.Worker.Builder;
using Microsoft.Extensions.Hosting;

var Builder = FunctionsApplication.CreateBuilder( args );

Builder.ConfigureFunctionsWebApplication();

// Application Insights isn't enabled by default. See https://aka.ms/AAt8mw4.
// builder.Services
//     .AddApplicationInsightsTelemetryWorkerService()
//     .ConfigureFunctionsApplicationInsights();

Builder.Build().Run();