﻿#nullable enable

using System.Windows.Media;

namespace WindowsUtils;

public static class ColourExtensions
{
	private static void ColorToHsb( Color color, out float hue, out float saturation, out float brightness )
	{
		var R = color.R / 255f;
		var G = color.G / 255f;
		var B = color.B / 255f;

		var Max = Math.Max( R, Math.Max( G, B ) );
		var Min = Math.Min( R, Math.Min( G, B ) );

		var Delta = Max - Min;

		// Calculate brightness
		brightness = Max;

		// Calculate saturation
		saturation = Max == 0 ? 0 : Delta / Max;

		// Calculate hue
		if( Delta == 0 )
			hue = 0; // Hue is undefined, so it can be set to any value in this case
		else if( Max == R )
			hue = ( ( G - B ) / Delta ) % 6;
		else if( Max == G )
			hue = ( ( B - R ) / Delta ) + 2;
		else // max == b
			hue = ( ( R - G ) / Delta ) + 4;

		hue *= 60; // Convert hue to degrees

		if( hue < 0 )
			hue += 360; // Ensure hue is within the valid range (0 to 360)
	}

	private static Color HsbToColor( float hue, float saturation, float brightness, byte alpha )
	{
		var C = brightness * saturation;
		var X = C * ( 1 - Math.Abs( ( ( hue / 60 ) % 2 ) - 1 ) );
		var M = brightness - C;

		float R, G, B;

		switch( hue )
		{
		case >= 0 and < 60:
			R = C;
			G = X;
			B = 0;
			break;

		case >= 60 and < 120:
			R = X;
			G = C;
			B = 0;
			break;

		case >= 120 and < 180:
			R = 0;
			G = C;
			B = X;
			break;

		case >= 180 and < 240:
			R = 0;
			G = X;
			B = C;
			break;

		case >= 240 and < 300:
			R = X;
			G = 0;
			B = C;
			break;

		// hue >= 300 && hue < 360
		default:
			R = C;
			G = 0;
			B = X;
			break;
		}

		var Red   = ClampByte( Math.Round( ( R + M ) * 255 ) );
		var Green = ClampByte( Math.Round( ( G + M ) * 255 ) );
		var Blue  = ClampByte( Math.Round( ( B + M ) * 255 ) );

		return Color.FromArgb( alpha, Red, Green, Blue );
	}

	private static byte ClampByte( double value )
	{
		return value switch
			   {
				   < 0   => 0,
				   > 255 => 255,
				   _     => (byte)value
			   };
	}

	public static Color MakeColorLighter( this Color color, float factor )
	{
		ColorToHsb( color, out var Hue, out var Saturation, out var Brightness );

		Brightness += ( 1 - Brightness ) * factor;
		Saturation -= Saturation * factor;

		return HsbToColor( Hue, Saturation, Brightness, color.A );
	}
}