﻿using Records;
using DeviceTripUpdate = Records.DeviceTripUpdate;
using GpsPoint = Records.GpsPoint;
using Trip = Records.Trip;

namespace Client;

public interface IClient
{
	static IClient Instance { get; protected set; } = null!;

	Task ReceivedByDevice( ReceivedByDevice receivedByDevice );
	Task ReadByDriver( ReadByDriver readByDriver );
	Task UpdateTrip( DeviceTripUpdate update );

	Task<IList<Trip>> GetTripsForDriver( string driverCode );
	Task UpdateGpsPointsForDriver( string driverCode, IList<GpsPoint> gpsPoints );
}

// ReSharper disable once InconsistentNaming
public abstract class AIClient : IClient
{
	protected AIClient()
	{
		IClient.Instance = this;
	}

	public abstract Task ReceivedByDevice( ReceivedByDevice receivedByDevice );
	public abstract Task ReadByDriver( ReadByDriver readByDriver );
	public abstract Task UpdateTrip( DeviceTripUpdate update );
	public abstract Task<IList<Trip>> GetTripsForDriver( string driverCode );
	public abstract Task UpdateGpsPointsForDriver( string driverCode, IList<GpsPoint> gpsPoints );
}