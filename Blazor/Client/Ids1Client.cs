﻿using Ids1RemoteService;
using Records;
using DeviceTripUpdate = Records.DeviceTripUpdate;
using GpsPoint = Records.GpsPoint;
using Trip = Records.Trip;

namespace Client;

internal class Ids1Client : AIClient
{
	public override Task ReceivedByDevice( ReceivedByDevice receivedByDevice )
	{
		var Trip = receivedByDevice.Trip;

		switch( Trip.OriginalTripType )
		{
		case Trip.ORIGINAL_TRIP_TYPE.REMOTE_TRIP:
			{
				var Rt = (remoteTrip?)Trip;

				if( Rt is not null )
				{
					Rt = Rt with
					     {
						     redirect = true
					     };
					return Ids1RemoteService.Client.Instance.UpdateTrip( Rt );
				}
			}
			break;

		case Trip.ORIGINAL_TRIP_TYPE.REMOTE_TRIP_DETAILED:
			{
				var Rt = (remoteTripDetailed?)Trip;

				if( Rt is not null )
				{
					Rt = Rt with
					     {
						     redirect = true
					     };

					return Ids1RemoteService.Client.Instance.UpdateTrip( Rt );
				}
			}
			break;
		}
		return Task.CompletedTask;
	}

	public override Task ReadByDriver( ReadByDriver readByDriver )
	{
		var Trip = readByDriver.Trip;

		switch( Trip.OriginalTripType )
		{
		case Trip.ORIGINAL_TRIP_TYPE.REMOTE_TRIP:
			{
				var Rt = (remoteTrip?)Trip;

				if( Rt is not null )
				{
					Rt = Rt with
					     {
						     carCharge = true
					     };
					return Ids1RemoteService.Client.Instance.UpdateTrip( Rt );
				}
			}
			break;

		case Trip.ORIGINAL_TRIP_TYPE.REMOTE_TRIP_DETAILED:
			{
				var Rt = (remoteTripDetailed?)Trip;

				if( Rt is not null )
				{
					Rt = Rt with
					     {
						     carCharge = true
					     };
					return Ids1RemoteService.Client.Instance.UpdateTrip( Rt );
				}
			}
			break;
		}
		return Task.CompletedTask;
	}

	public override Task UpdateTrip( DeviceTripUpdate update ) => throw new NotImplementedException();

	public override Task<IList<Trip>> GetTripsForDriver( string driverCode ) => throw new NotImplementedException();

	public override Task UpdateGpsPointsForDriver( string driverCode, IList<GpsPoint> gpsPoints ) => throw new NotImplementedException();
}