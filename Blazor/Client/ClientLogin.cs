﻿using AzureRemoteService;
using Utils;

namespace Client;

public class ClientLogin
{
	public enum LOGIN_STATUS
	{
		TIME_ERROR,
		FAILED,
		SUCCESS
	}

	public static async Task<( LOGIN_STATUS Status, string UserName, bool TestServer)> Login( string program, string carrierId, string username, string password, bool forceIds1 = false )
	{
		carrierId = carrierId.TrimToUpper();
		username  = username.TrimToUpper();

		Azure.AZURE_CONNECTION_STATUS Status;

		if( !forceIds1 )
			( Status, _ ) = await Azure.LogIn( program, carrierId, username, password );
		else
			Status = Azure.AZURE_CONNECTION_STATUS.ERROR;

		switch( Status )
		{
		case Azure.AZURE_CONNECTION_STATUS.OK:
			new AzureClient();
			return ( LOGIN_STATUS.SUCCESS, username, Azure.DebugServer );

		case Azure.AZURE_CONNECTION_STATUS.TIME_ERROR:
			return ( LOGIN_STATUS.TIME_ERROR, username, false );

		default:
		#if DEBUG
			const bool FORCE_JB_TEST = true;
		#else
			const bool FORCE_JB_TEST = false;
		#endif

			var (Ok, UserName, JbTest) = await Ids1RemoteService.Client.Login( program, carrierId, username, password, FORCE_JB_TEST );

			if( Ok )
			{
				new Ids1Client();
				return ( LOGIN_STATUS.SUCCESS, UserName, JbTest );
			}

			break;
		}

		return ( LOGIN_STATUS.FAILED, username, false );
	}
}