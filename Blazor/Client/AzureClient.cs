﻿using AzureRemoteService;
using Records;
using DeviceTripUpdate = Records.DeviceTripUpdate;
using GpsPoint = Records.GpsPoint;
using GpsPoints = Protocol.Data.GpsPoints;
using Trip = Records.Trip;

namespace Client;

internal class AzureClient : AIClient
{
	public override Task ReceivedByDevice( ReceivedByDevice receivedByDevice ) => Azure.Client.RequestTripReceivedByDevice( receivedByDevice.Program, receivedByDevice.Trip.TripId );

	public override Task ReadByDriver( ReadByDriver readByDriver ) => Azure.Client.RequestTripReadByDriver( readByDriver.Program, readByDriver.Trip.TripId );

	public override Task UpdateTrip( DeviceTripUpdate update ) => Azure.Client.RequestUpdateTripFromDevice( update );

	public override async Task<IList<Trip>> GetTripsForDriver( string driverCode ) => new List<Trip>( from T in await Azure.Client.RequestGetTripsForDriver( driverCode )
	                                                                                                  select new Trip( T ) );

	public override Task UpdateGpsPointsForDriver( string driverCode, IList<GpsPoint> gpsPoints ) => Azure.Client.RequestGps( new GpsPoints( from G in gpsPoints
	                                                                                                                                    select new Protocol.Data.GpsPoint
	                                                                                                                                           {
		                                                                                                                                           Latitude      = G.Latitude,
		                                                                                                                                           Longitude     = G.Longitude,
		                                                                                                                                           LocalDateTime = G.DateTime
	                                                                                                                                           } ) );
}