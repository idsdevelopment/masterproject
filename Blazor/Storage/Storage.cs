﻿using Microsoft.JSInterop;
using Newtonsoft.Json;
using Utils;

namespace Storage;

public class Storage
{
	private static IJSRuntime? JsRuntime;

	public static void Initialise( IJSRuntime? jsRuntime )
	{
		JsRuntime = jsRuntime;
	}


	public static async Task<bool> SaveAsync<T>( string key, T value )
	{
		try
		{
			if( JsRuntime is { } J )
			{
				var SaveString = JsonConvert.SerializeObject( value );
				SaveString = Encryption.Encrypt( SaveString );
				await J.InvokeVoidAsync( "localStorageHelper.save", key, SaveString );
				return true;
			}
		}
		catch( Exception E )
		{
			Console.WriteLine( E );
		}
		return false;
	}

	public static async Task<T?> LoadAsync<T>( string key )
	{
		try
		{
			if( JsRuntime is { } J )
			{
				var JsonString = await J.InvokeAsync<string>( "localStorageHelper.load", key );

				if( JsonString.IsNotNullOrWhiteSpace() )
				{
					var Decrypt = Encryption.DecryptDetailed( JsonString );

					if( Decrypt is { Ok: true } )
					{
						var RetVal = JsonConvert.DeserializeObject<T>( Decrypt.Value );
						return RetVal;
					}
				}
			}
		}
		catch( Exception E )
		{
			Console.WriteLine( E );
		}
		return default;
	}
}