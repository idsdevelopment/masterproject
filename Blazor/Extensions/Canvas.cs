﻿using System.Drawing;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace Extensions;

public class CanvasReference
{
	private readonly ElementReference Element;
	private readonly IJSRuntime       JsRuntime;

	private async Task ResizeCanvasToDisplaySize()
	{
		await JsRuntime.InvokeVoidAsync( "resizeCanvasToDisplaySize", Element );
	}

	public CanvasReference( IJSRuntime jsRuntime, ElementReference element )
	{
		JsRuntime = jsRuntime;
		Element   = element;

		async void AttachListenResize()
		{
			try
			{
				await ResizeCanvasToDisplaySize();
			}
			catch
			{
			}
		}

		AttachListenResize();
	}

	public static implicit operator ElementReference( CanvasReference canvasReference ) => canvasReference.Element;

	public async Task UpdateCanvasSize()
	{
		await JsRuntime.InvokeVoidAsync( "setCanvasSize", Element );
	}

	public async Task SetFillColorAsync( string color )
	{
		await JsRuntime.InvokeVoidAsync( "setCanvasFillColor", Element, color );
	}

	public async Task SetFillColorAsync( Color color )
	{
		await SetFillColorAsync( color.AsString() );
	}

	public async Task ClearCanvasAsync()
	{
		await JsRuntime.InvokeVoidAsync( "clearCanvas", Element );
	}

	public async Task SetPenPropertiesAsync( int width, string color )
	{
		await JsRuntime.InvokeVoidAsync( "setPenProperties", Element, width, color );
	}

	public async Task SetPenPropertiesAsync( int width, Color color )
	{
		await SetPenPropertiesAsync( width, color.AsString() );
	}

	public async Task MoveToPointAsync( int x, int y )
	{
		await JsRuntime.InvokeVoidAsync( "moveToPoint", Element, x, y );
	}

	public async Task LineToPointAsync( int x, int y )
	{
		await JsRuntime.InvokeVoidAsync( "lineToPoint", Element, x, y );
	}

	public async Task<MousePosition?> GetMousePositionAsync( MouseEventArgs e )
	{
		var Temp = await JsRuntime.GetMousePositionAsync( Element, e );
		return Temp;
	}
}