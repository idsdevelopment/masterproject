﻿namespace Extensions;

public class VisualStates
{
	public const string DISABLED = "disabled";

	public static ATTRIBUTES Disabled( ATTRIBUTES attributes, bool disabled )
	{
		if( disabled )
			attributes[ DISABLED ] = DISABLED;
		else
			attributes.Remove( DISABLED );

		return attributes;
	}

	public static ATTRIBUTES Disabled( bool disabled ) => disabled ? new ATTRIBUTES { { DISABLED, DISABLED } } : new ATTRIBUTES();

	public static string Visible( bool visible ) => visible ? "visible" : "collapse";

	public static bool Visible( bool newState, ref bool visible, ref string state )
	{
		if( newState != visible )
		{
			visible = newState;
			state   = Visible( visible );
			return true;
		}
		return false;
	}

	public static string Display( bool display, string initialType ) => display ? initialType : "none";

	public static bool Display( bool newState, ref bool display, ref string state, string initialType )
	{
		if( newState != display )
		{
			display = newState;
			state   = Display( display, initialType );
			return true;
		}
		return false;
	}

	public static bool DisplayBlock( bool newState, ref bool display, ref string state ) => Display( newState, ref display, ref state, "block" );
	public static string DisplayBlock( bool state ) => state ? "block" : "none";

	public static bool DisplayFlex( bool newState, ref bool display, ref string state ) => Display( newState, ref display, ref state, "flex" );
	public static string DisplayFlex( bool state ) => state ? "flex" : "none";
}