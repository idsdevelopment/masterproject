﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace Extensions;

public class Dimensions
{
	public int Width  { get; set; }
	public int Height { get; set; }
}

public class MousePosition
{
	public int X { get; set; }
	public int Y { get; set; }
}

public static class JsRuntimeExtensions
{
	public static async Task<Dimensions?> GetViewportSizeAsync( this IJSRuntime jsRuntime ) => await jsRuntime.InvokeAsync<Dimensions>( "getViewportSize" );
	public static async Task<Dimensions?> GetElementSizeAsync( this IJSRuntime jsRuntime, ElementReference element ) => await jsRuntime.InvokeAsync<Dimensions>( "getElementSize", element );
	public static async Task SetElementSizeAsync( this IJSRuntime jsRuntime, ElementReference element, int height, int width ) => await jsRuntime.InvokeVoidAsync( "setElementSize", element, height, width );

	public static async Task<Dimensions?> GetElementDisplaySizeAsync( this IJSRuntime jsRuntime, ElementReference element ) => await jsRuntime.InvokeAsync<Dimensions>( "getElementDisplaySize", element );

	public static async Task<MousePosition?> GetMousePositionAsync( this IJSRuntime jsRuntime, ElementReference element, MouseEventArgs e )
	{
		var ClientX = (int)e.ClientX;
		var ClientY = (int)e.ClientY;
		var Temp    = await jsRuntime.InvokeAsync<MousePosition>( "getMousePosition", element, ClientX, ClientY );
		return Temp;
	}

	public static async void GoBack( this IJSRuntime jsRuntime )
	{
		try
		{
			await jsRuntime.InvokeVoidAsync( "goBack" );
		}
		catch
		{
		}
	}
}