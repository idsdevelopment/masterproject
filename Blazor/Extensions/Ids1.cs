﻿using Utils;

namespace Extensions;

public static class Ids1
{
	public enum STATUS
	{
		UNSET,
		NEW,
		ACTIVE,
		DISPATCHED,
		PICKED_UP,
		DELIVERED,
		VERIFIED,
		POSTED,
		DELETED,
		SCHEDULED,
		UNDELIVERED     = -3,
		CREATE_NEW_TRIP = -4
	}

	public static Protocol.Data.STATUS ToStatus( this int status ) => (STATUS)status switch
	                                                                  {
		                                                                  STATUS.UNSET      => Protocol.Data.STATUS.UNSET,
		                                                                  STATUS.NEW        => Protocol.Data.STATUS.NEW,
		                                                                  STATUS.ACTIVE     => Protocol.Data.STATUS.ACTIVE,
		                                                                  STATUS.DISPATCHED => Protocol.Data.STATUS.DISPATCHED,
		                                                                  STATUS.PICKED_UP  => Protocol.Data.STATUS.PICKED_UP,
		                                                                  STATUS.DELIVERED  => Protocol.Data.STATUS.DELIVERED,
		                                                                  STATUS.VERIFIED   => Protocol.Data.STATUS.VERIFIED,
		                                                                  STATUS.POSTED     => Protocol.Data.STATUS.POSTED,
		                                                                  STATUS.DELETED    => Protocol.Data.STATUS.DELETED,
		                                                                  STATUS.SCHEDULED  => Protocol.Data.STATUS.SCHEDULED,
		                                                                  _                 => Protocol.Data.STATUS.UNSET
	                                                                  };

	public static string ToAddressBarcode( this string companyName ) => ( companyName.Between( "(", ")" ) ?? "" ).Trim();
}