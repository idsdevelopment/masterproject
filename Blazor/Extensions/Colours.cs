﻿using System.Drawing;

namespace Extensions;

public static class ColourExtensions
{
	public static string AsString( this Color color ) => $"rgba({color.R}, {color.G}, {color.B}, {color.A / 255.0})";

	public static string AsString( this Color? color ) => ( color ?? Color.Red ).AsString();
}