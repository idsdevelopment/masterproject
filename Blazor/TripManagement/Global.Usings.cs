﻿global using System.Runtime.CompilerServices;
global using Protocol.Data;
global using Utils;
global using OBSERVABLE_TRIP_DICTIONARY = Utils.ObservableDictionary<string, TripManagement.TripDisplayEntry>;
global using TRIP_COLLECTION = System.Collections.ObjectModel.ObservableCollection<TripManagement.TripDisplayEntry>;