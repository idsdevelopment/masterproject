﻿using AzureRemoteService;
using TripManagement.Packets;

namespace TripManagement.Listeners;

public class TripListener
{
	protected const int ERROR_TIMEOUT_MS      = 10_000,
	                    TIME_TO_LIVE_IN_HOURS = 24 * 3;

	private static bool IsListening;

	private static string TimeToLiveInHours => TIME_TO_LIVE_IN_HOURS.ToString();

	private static async void ProcessMessage( List<TripUpdateMessage> messages )
	{
		try
		{
			foreach( var Message in messages )
			{
				switch( Message.Action )
				{
				case TripUpdateMessage.ACTION.UPDATE_STATUS:
					foreach( var TripId in Message.TripIdList )
					{
						if( Message.Status is STATUS.DELETED or STATUS.FINALISED or STATUS.LIMBO or STATUS.VERIFIED or STATUS.POSTED )
							Globals.OnRemoveTrip.Value = TripId;
						else
							Globals.OnTripStatusChange.Value = ( TripId, Message.Status, Message.Status1, Message.ReceivedByDevice, Message.ReadByDriver );
					}
					break;

				case TripUpdateMessage.ACTION.UPDATE_TRIP when Message.Status is STATUS.ACTIVE or STATUS.DELETED: // Bounced Trip	 or Deleted
					foreach( var TripId in Message.TripIdList )
						Globals.OnRemoveTrip.Value = TripId;
					break;

				case TripUpdateMessage.ACTION.UPDATE_TRIP:
					while( true )
					{
						try
						{
							var Trps = await Azure.Client.RequestGetTrips( new TripIdList( Message.TripIdList )
							                                               {
								                                               Program = Globals.CurrentVersion.PROGRAM
							                                               } );

							if( Trps is not null )
							{
								var ToUpdate = new List<Trip>();

								foreach( var Trip in Trps )
								{
									Trip.Modified = true;

									var TripId = Trip.TripId;

									if( !Trip.ReceivedByDevice )
										ToUpdate.Add( Trip );

									switch( Trip.Status1 )
									{
									case STATUS.NEW:
									case STATUS.DELETED:
									case STATUS.FINALISED:
									case STATUS.VERIFIED:
									case STATUS.LIMBO:
									case STATUS.POSTED:
										Globals.OnRemoveTrip.Value = TripId;
										break;

									default:
										Globals.OnUpdateTrip.Value = Trip;
										break;
									}
								}

								foreach( var Trip in ToUpdate )
								{
									var Received = new ReceivedByDevice
									               {
										               Value = new Records.ReceivedByDevice( Globals.CurrentVersion.PROGRAM,
										                                                     new Records.Trip( Trip ) )
									               };

									await Update.UpdateTrip( Received );
								}
							}
							break;
						}
						catch
						{
							Thread.Sleep( ERROR_TIMEOUT_MS );
						}
					}
					break;
				}
			}
		}
		catch( Exception Ex )
		{
		#if DEBUG
			Console.WriteLine( Ex );
		#endif
		}
	}

	public static void StartListening( string userName )
	{
		if( !IsListening )
		{
			try
			{
				var User = userName.NullTrim().ToLower();

				if( User != "" )
				{
					Tasks.RunVoid( () =>
					               {
						               Azure.Client.ListenTripUpdate( Messaging.DRIVERS, User, TimeToLiveInHours, ProcessMessage );
					               } );

					IsListening = true;
				}
			}
			catch( Exception E )
			{
			#if DEBUG
				Console.WriteLine( E );
			#endif
			}
		}
	}
}