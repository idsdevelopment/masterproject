﻿using AzureRemoteService;

// ReSharper disable InconsistentNaming

namespace TripManagement.Listeners;

public class Ping : Poller
{
	private const  int   PING_INTERVAL_IN_SECONDS = 5;
	private static Ping? Instance;

	public Ping() : base( PING_INTERVAL_IN_SECONDS )
	{
	}

	public static void Initialise()
	{
		Instance ??= new Ping();
	}


	protected override async Task Elapsed()
	{
		void SetOnline( bool online )
		{
			Globals.OnPing.Value = online;
		}

		try
		{
			var OnLine = ( await Azure.Client.RequestPing() ).Compare( "OK", StringComparison.OrdinalIgnoreCase ) == 0;
			SetOnline( OnLine );
		}
		catch( Exception E )
		{
		#if DEBUG
			Console.WriteLine( E );
		#endif

			SetOnline( false );
		}
	}
}