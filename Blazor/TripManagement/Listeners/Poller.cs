﻿using Timer = System.Timers.Timer;

namespace TripManagement.Listeners;

public abstract class Poller
{
	private readonly Timer PollTimer;

	private bool InPoll;

	protected Poller( int pollIntervalInSeconds )
	{
		PollTimer = new Timer( pollIntervalInSeconds * 1_000 );

		PollTimer.Elapsed += async ( _, _ ) =>
		                     {
			                     if( !InPoll )
			                     {
				                     InPoll = true;

				                     try
				                     {
					                     await Elapsed();
				                     }
				                     finally
				                     {
					                     InPoll = false;
				                     }
			                     }
		                     };
		PollTimer.Start();
	}

	protected abstract Task Elapsed();
}