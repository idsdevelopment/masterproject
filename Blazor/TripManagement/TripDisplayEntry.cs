﻿using System.ComponentModel;
using System.Drawing;
using Extensions;
using Trip = Records.Trip;

// ReSharper disable InconsistentNaming

namespace TripManagement;

public class TripDisplayEntry : INotifyPropertyChanged
{
	protected Trip _Trip;

	public TripDisplayEntry( Trip trip )
	{
		_Trip = trip;

		var SColors = Globals.ServiceLevelColors.GetValueOrDefault( trip.ServiceLevel.TrimToUpper(),
		                                                            ( BackgroundColor: Color.Black, ForegroundColor: Color.White ) );
		ServiceLevelBackground = SColors.BackgroundColor;
		ServiceLevelForeground = SColors.ForegroundColor;
	}

	public TripDisplayEntry( Protocol.Data.Trip trip ) : this( new Trip( trip ) )
	{
	}

	public TripDisplayEntry( TripDisplayEntry trip )
	{
		_Trip = trip._Trip with { };

		ServiceLevelBackground = trip.ServiceLevelBackground;
		ServiceLevelForeground = trip.ServiceLevelForeground;
	}

	public void Update( Trip trip )
	{
		_Trip = trip;

		var SColors = Globals.ServiceLevelColors.GetValueOrDefault( trip.ServiceLevel.TrimToUpper(),
		                                                            ( BackgroundColor: Color.Black, ForegroundColor: Color.White ) );
		ServiceLevelBackground          = SColors.BackgroundColor;
		ServiceLevelForeground          = SColors.ForegroundColor;
		_Status                         = trip.Status;
		_ServiceLevelBackgroundAsString = _ServiceLevelForegroundAsString = null;
		_Pieces                         = _Weight                         = _Scanned = _Remaining = null;
		_PickupTime                     = _DeliveryTime                   = _DueTime = null;
		_ReadByDriver                   = null;
	}

	public void Update( Protocol.Data.Trip trip )
	{
		Update( new Trip( trip ) );
	}

	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	protected bool SetField<T>( ref T field, T value, [CallerMemberName] string? propertyName = null )
	{
		if( EqualityComparer<T>.Default.Equals( field, value ) )
			return false;
		field = value;
		OnPropertyChanged( propertyName );
		return true;
	}

	public event PropertyChangedEventHandler? PropertyChanged;

#region Status
	private STATUS _Status = STATUS.UNSET;

	public STATUS Status
	{
		get
		{
			if( _Status == STATUS.UNSET )
				_Status = _Trip.Status;

			return _Status;
		}
		set
		{
			if( SetField( ref _Status, value ) )
			{
				_Trip.Status = value;
				OnPropertyChanged();
			}
		}
	}
#endregion

#region Pop / Pod
	public string POP
	{
		get => _Trip.POP;
		set => _Trip.POP = value;
	}

	public string POD
	{
		get => _Trip.POD;
		set => _Trip.POD = value;
	}
#endregion

#region IsVisible
	private bool _IsVisible = true;

	public bool IsVisible
	{
		get => _IsVisible;
		set => SetField( ref _IsVisible, value );
	}
#endregion

#region Properties
	public string TripId => _Trip.TripId;

#region Status
	public bool IsPickup   => _Trip.Status < STATUS.PICKED_UP;
	public bool IsDelivery => _Trip.Status >= STATUS.PICKED_UP;
#endregion

#region Colours
	private static string GetColorAsString( Color? color, ref string? colorAsString )
	{
		return colorAsString ??= color.AsString();
	}
#endregion

#region Reference
	public string Reference
	{
		get => _Trip.Reference;
		set
		{
			if( _Trip.Reference != value )
			{
				_Trip.Reference = value;
				OnPropertyChanged();
			}
		}
	}
#endregion

#region Notes
	public string PickupNotes   => _Trip.PickupNotes.Trim();
	public string DeliveryNotes => _Trip.DeliveryNotes.Trim();
	public string DisplayNotes  => IsPickup ? PickupNotes : DeliveryNotes;
#endregion

#region Service Level
	public string ServiceLevel           => _Trip.ServiceLevel;
	public Color  ServiceLevelBackground { get; private set; }
	public Color  ServiceLevelForeground { get; private set; }

	private string? _ServiceLevelBackgroundAsString,
	                _ServiceLevelForegroundAsString;

	public string ServiceLevelBackgroundAsString => GetColorAsString( ServiceLevelBackground, ref _ServiceLevelBackgroundAsString );

	public string ServiceLevelForegroundAsString => GetColorAsString( ServiceLevelForeground, ref _ServiceLevelForegroundAsString );
#endregion

#region Package Type
	public string PackageType => _Trip.PackageType;
#endregion

#region Pieces / Weight	/ Scanned
	private string? _Pieces, _Weight, _Scanned, _Remaining;

	public decimal Pieces
	{
		get
		{
			var P = _Trip.Pieces;
			return P > 0 ? P : 1;
		}
		set
		{
			if( _Trip.Pieces != value )
			{
				_Trip.Pieces = value;
				OnPropertyChanged();
			}
		}
	}

	public decimal Weight
	{
		get => _Trip.Weight;
		set
		{
			if( _Trip.Weight != value )
			{
				_Trip.Weight = value;
				OnPropertyChanged();
			}
		}
	}

	private static string DecimalAsString( decimal value, ref string? str )
	{
		return str ??= $"{value:N0}";
	}

	public string PiecesAsString => DecimalAsString( Pieces, ref _Pieces );
	public string WeightAsString => DecimalAsString( Weight, ref _Weight );

	private decimal _Scanned1;


	public decimal Remaining { get; private set; }

	public void ResetScanned()
	{
		_Scanned1 = -1;
		Scanned   = 0;
	}

	public bool BeingScanned;

	public decimal Scanned
	{
		get => _Scanned1;
		set
		{
			if( SetField( ref _Scanned1, value ) )
			{
				_Scanned   = null;
				_Remaining = null;
				Remaining  = Pieces - value;
				OnPropertyChanged( nameof( ScannedAsString ) );
				OnPropertyChanged( nameof( RemainingAsString ) );
			}
		}
	}

	public string RemainingAsString => DecimalAsString( Remaining, ref _Remaining );
	public string ScannedAsString   => DecimalAsString( _Scanned1, ref _Scanned );
#endregion

#region Date / Time
	private string? _PickupTime, _DeliveryTime, _DueTime;

	private static string ShortDateTimeToString( DateTimeOffset? dateTime, ref string? str )
	{
		dateTime ??= DateTimeOffset.Now;
		return str ??= $"{dateTime:ddd h:mm tt}";
	}

	private static string DateTimeToString( DateTimeOffset? dateTime, ref string? str )
	{
		dateTime ??= DateTimeOffset.Now;
		return str ??= $"{dateTime:D}";
	}

	public string PickupDeliveryDateTimeAsString => IsPickup ? ShortDateTimeToString( _Trip.PickupTime, ref _PickupTime )
		                                                : ShortDateTimeToString( _Trip.DeliveryTime, ref _DeliveryTime );

	public string DueTimeAsString => DateTimeToString( _Trip.DueTime, ref _DueTime );
#endregion

#region Read By Driver
	private bool? _ReadByDriver;

	public bool ReadByDriver
	{
		get { return _ReadByDriver ??= _Trip.ReadByDriver; }
		set
		{
			if( SetField( ref _ReadByDriver, value ) )
			{
				_Trip.ReadByDriver = value;
				OnPropertyChanged();
			}
		}
	}
#endregion

#region Company
	public string PickupCompanyName   => _Trip.PickupCompanyName.Trim();
	public string DeliveryCompanyName => _Trip.DeliveryCompanyName.Trim();
	public string DisplayCompanyName  => IsPickup ? PickupCompanyName : DeliveryCompanyName;


	public string PickupLocation   => _Trip.PickupAddressBarcode.Trim();
	public string DeliveryLocation => _Trip.DeliveryAddressBarcode.Trim();
	public string DisplayLocation  => IsPickup ? PickupLocation : DeliveryLocation;

	public string PickupCity   => _Trip.PickupAddressCity.Trim();
	public string DeliveryCity => _Trip.DeliveryAddressCity.Trim();
	public string DisplayCity  => IsPickup ? PickupCity : DeliveryCity;

#region Account
	public string AccountId => _Trip.AccountId.Trim();
#endregion

#region Address
	public string PickupAddress   => _Trip.PickupAddressLine1.Trim();
	public string DeliveryAddress => _Trip.DeliveryAddressLine1.Trim();

	public string DisplayAddress
	{
		get
		{
			string Addr, City;

			if( IsPickup )
			{
				Addr = PickupAddress;
				City = PickupCity;
			}
			else
			{
				Addr = DeliveryAddress;
				City = DeliveryCity;
			}
			return $"{Addr}  {City}";
		}
	}
#endregion

	public bool IsSameCompany( TripDisplayEntry other )
	{
		int Compare;

		if( IsPickup )
		{
			if( PickupLocation.IsNotNullOrWhiteSpace() && other.PickupLocation.IsNotNullOrWhiteSpace() )
				Compare = string.Compare( PickupLocation, other.PickupLocation, StringComparison.OrdinalIgnoreCase );
			else
				Compare = string.Compare( PickupCompanyName, other.PickupCompanyName, StringComparison.OrdinalIgnoreCase );
		}
		else
		{
			if( DeliveryLocation.IsNotNullOrWhiteSpace() && other.DeliveryLocation.IsNotNullOrWhiteSpace() )
				Compare = string.Compare( DeliveryLocation, other.DeliveryLocation, StringComparison.OrdinalIgnoreCase );
			else
				Compare = string.Compare( DeliveryCompanyName, other.DeliveryCompanyName, StringComparison.OrdinalIgnoreCase );
		}
		return Compare == 0;
	}
#endregion


#region Convertors
	public static implicit operator TripDisplayEntry( Trip trip ) => new( trip );
	public static implicit operator Trip( TripDisplayEntry entry ) => entry._Trip;
#endregion
#endregion
}