﻿using Trip = Records.Trip;

namespace TripManagement.AlternateViews;

public class ScannedTripDisplayEntry : TripDisplayEntry
{
	public ScannedTripDisplayEntry( Trip trip ) : base( trip )
	{
		AllowOverScanning  = Globals.Preferences.AllowOverScans;
		AllowUnderScanning = Globals.Preferences.AllowUnderScans;
	}

#region Scanning
	public bool AllowOverScanning  { get; }
	public bool AllowUnderScanning { get; }
#endregion

#region Visible
	private bool _IsVisible = true;

	public new bool IsVisible
	{
		get => _IsVisible;
		set
		{
			if( SetField( ref _IsVisible, value ) )
				OnPropertyChanged();
		}
	}
#endregion

#region Scanned
	private decimal _Scanned;

	public new decimal Scanned
	{
		get => _Scanned;
		set
		{
			if( SetField( ref _Scanned, value ) )
			{
				Balance = _Trip.Pieces - _Scanned;
				OnPropertyChanged( nameof( DisplayScanned ) );

				IsVisible = Balance != 0;
			}
		}
	}

	public string DisplayScanned => Scanned.ToString( "N0" );
#endregion

#region Balance
	private decimal _Balance;

	public decimal Balance
	{
		get => _Balance;
		set
		{
			if( SetField( ref _Balance, value ) )
			{
				IsVisible = ( _Balance != 0 ) || AllowOverScanning;
				OnPropertyChanged( nameof( DisplayBalance ) );
			}
		}
	}

	public string DisplayBalance => Balance.ToString( "N0" );
#endregion
}

public class SCANNED_TRIP_COLLECTION : TRIP_COLLECTION
{
	public void Add( ScannedTripDisplayEntry value )
	{
		base.Add( value );
	}
}