﻿using System.Collections.Specialized;
using System.Drawing;
using AzureRemoteService;
using Client;
using Events;
using Records;
using DeviceTripUpdate = Records.DeviceTripUpdate;
using Trip = Records.Trip;

namespace TripManagement;

using SERVICE_LEVEL_COLORS = (Color BackgroundColor, Color ForegroundColor);
using STATUS_UPDATE = (string TripId, STATUS Status, STATUS1 Status1, bool ReceivedByDevice, bool ReadByDriver);

public partial class Globals
{
	public const int RETRY_DELAY = 30_000;

	// ReSharper disable once InconsistentNaming
	public static           string                     CURRENT_VERSION    = "";
	private static readonly Lock                       TripLock           = new();
	public static readonly  OBSERVABLE_TRIP_DICTIONARY TripDisplayEntries = InitCollection();

	public static Action<bool>? OnWaitingForTrips;

	public static Func<string, string>? GetStringResource;
	public static Func<string, Color>?  GetColorResource;


	private static bool HaveEvents;


	private static OBSERVABLE_TRIP_DICTIONARY InitCollection()
	{
		var Collection = new OBSERVABLE_TRIP_DICTIONARY();
		Collection.CollectionChanged += CollectionOnCollectionChanged;
		return Collection;
	}

	private static void CollectionOnCollectionChanged( object? sender, NotifyCollectionChangedEventArgs e )
	{
		TripDisplayEntriesChanged.Value = e;
	}

	public static void UpdateTrip( string tripId, STATUS status, string reference, string popPod,
	                               decimal pieces, decimal weight )
	{
		lock( TripLock )
		{
			if( TripDisplayEntries.TryGetValue( tripId, out var Entry ) )
			{
				Entry.Status = status;

				switch( status )
				{
				case STATUS.PICKED_UP:
					Entry.POP = popPod;
					break;

				case STATUS.DELIVERED:
				case STATUS.VERIFIED:
					Entry.POD = popPod;
					break;
				}

				Entry.Reference = reference;
				Entry.Pieces    = pieces;
				Entry.Weight    = weight;
			}
		}
	}

	public static void UpdateTrip( DeviceTripUpdate t )
	{
		UpdateTrip( t.TripId, t.Status, t.Reference, t.PopPod, t.Pieces, t.Weight );
	}

	public static void UpdateTrip( Packets.DeviceTripUpdate t )
	{
		if( t.Value is { } T )
			UpdateTrip( T );
	}

	public static void GetTrips( string driverCode, Action<OBSERVABLE_TRIP_DICTIONARY> onHaveTrips )
	{
		if( !HaveEvents )
		{
			OnRemoveTrip.Add( ( _, tripId ) =>
			                  {
				                  lock( TripLock )
					                  TripDisplayEntries.Remove( tripId );

				                  OnTripChange.Value = true;
			                  } );

			OnTripStatusChange.Add( ( _, update ) =>
			                        {
				                        lock( TripLock )
				                        {
					                        if( TripDisplayEntries.TryGetValue( update.TripId, out var Entry ) )
						                        Entry.Status = update.Status;
				                        }
				                        OnTripChange.Value = true;
			                        } );

			OnUpdateTrip.Add( ( _, trip ) =>
			                  {
				                  var TripId = trip.TripId;

				                  lock( TripLock )
				                  {
					                  var Td = TripDisplayEntries;

					                  if( Td.ContainsKey( TripId ) )
						                  Td.Remove( TripId );

					                  Td.Add( TripId, new TripDisplayEntry( trip ) );
				                  }
				                  OnTripChange.Value = true;
			                  } );

			HaveEvents = true;
		}

		lock( TripLock )
		{
			if( TripDisplayEntries.Count > 0 )
			{
				onHaveTrips( TripDisplayEntries );
				return;
			}
		}

		Tasks.RunVoid( async () =>
		               {
			               void WaitingForTrips( bool waiting )
			               {
				               OnWaitingForTrips?.Invoke( waiting );
			               }

			               await Azure.Retry( async () =>
			                                  {
				                                  var HasTrips = false;

				                                  WaitingForTrips( true );

				                                  var TokenSource = new CancellationTokenSource();
				                                  var Token       = TokenSource.Token;

				                                  void CheckForCancellation()
				                                  {
					                                  if( Token.IsCancellationRequested )
					                                  {
						                                  lock( TripLock )
							                                  TripDisplayEntries.Clear();

						                                  Token.ThrowIfCancellationRequested();
					                                  }
				                                  }

				                                  try
				                                  {
					                                  Task[]       Tsks;
					                                  IList<Trip>? Trips = null;

					                                  if( !HaveServiceLevelColors )
					                                  {
						                                  Tsks = new Task[ 2 ];

						                                  Tsks[ 1 ] = Task.Run( async () =>
						                                                        {
							                                                        try
							                                                        {
								                                                        Preferences._Preferences = await Azure.Client.RequestDevicePreferences();

								                                                        CheckForCancellation();

								                                                        var Colors = await Azure.Client.RequestGetServiceLevelColours();

								                                                        foreach( var C in Colors )
								                                                        {
									                                                        CheckForCancellation();

									                                                        ServiceLevelColors[ C.ServiceLevel.TrimToUpper() ] = ( Color.FromArgb( (int)C.Background_ARGB ),
									                                                                                                               Color.FromArgb( (int)C.Foreground_ARGB ) );
								                                                        }
								                                                        HaveServiceLevelColors = true;
							                                                        }
							                                                        catch( Exception E ) when( E is not OperationCanceledException )
							                                                        {
								                                                        await TokenSource.CancelAsync();
							                                                        }
						                                                        }, Token );
					                                  }
					                                  else
						                                  Tsks = new Task[ 1 ];

					                                  Tsks[ 0 ] = Task.Run( async () =>
					                                                        {
						                                                        try
						                                                        {
							                                                        Trips = await IClient.Instance.GetTripsForDriver( driverCode );
						                                                        }
						                                                        catch( Exception E ) when( E is not OperationCanceledException )
						                                                        {
							                                                        await TokenSource.CancelAsync();
						                                                        }
					                                                        }, Token );

					                                  await Task.WhenAll( Tsks );

					                                  lock( TripLock )
						                                  TripDisplayEntries.Clear();

					                                  if( Trips is not null )
					                                  {
						                                  foreach( var Trip in Trips )
						                                  {
							                                  if( !Trip.ReceivedByDevice )
							                                  {
								                                  await Update.UpdateTrip( new Packets.ReceivedByDevice( new ReceivedByDevice( CURRENT_VERSION, Trip ) ) );
								                                  Trip.ReceivedByDevice = true;
							                                  }

							                                  lock( TripLock )
								                                  TripDisplayEntries.Add( Trip.TripId, new TripDisplayEntry( Trip ) );
						                                  }
					                                  }

					                                  lock( TripLock )
						                                  HasTrips = TripDisplayEntries.Count > 0;
				                                  }
				                                  finally
				                                  {
					                                  if( HasTrips )
					                                  {
						                                  WaitingForTrips( false );

						                                  lock( TripLock )
							                                  onHaveTrips( TripDisplayEntries );
					                                  }
				                                  }

				                                  return HasTrips;
			                                  }, int.MaxValue, RETRY_DELAY ); // Retry forever
		               } );
	}

	public static string StringResource( string key ) => GetStringResource?.Invoke( key ) ?? "";

	public static Color ColorResource( string key ) => GetColorResource?.Invoke( key ) ?? Color.Transparent;

#region Service Levels
	private static           bool                                     HaveServiceLevelColors;
	internal static readonly Dictionary<string, SERVICE_LEVEL_COLORS> ServiceLevelColors = new();
#endregion

#region Events
	public static readonly WeakEvent<NotifyCollectionChangedEventArgs> TripDisplayEntriesChanged = new();

	public static readonly WeakEvent<bool> OnPing = new();

	public static readonly WeakEvent<string>             OnRemoveTrip       = new();
	public static readonly WeakEvent<STATUS_UPDATE>      OnTripStatusChange = new();
	public static readonly WeakEvent<Protocol.Data.Trip> OnUpdateTrip       = new();
	public static readonly WeakEvent<bool>               OnTripChange       = new();
#endregion
}