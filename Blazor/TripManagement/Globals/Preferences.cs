﻿namespace TripManagement;

public partial class Globals
{
	public class Preferences
	{
		public static bool AllowUnderScans => _Preferences.AllowUnderScans;
		public static bool AllowOverScans  => _Preferences.AllowOverScans;

		public static bool EditPieces    => _Preferences.EditPiecesWeight;
		public static bool EditWeight    => _Preferences.EditPiecesWeight;
		public static bool EditReference => _Preferences.EditReference;

		public static bool RequirePop => _Preferences.RequirePop;
		public static bool RequirePod => _Preferences.RequirePod;

		public static bool RequirePodSignature => _Preferences.RequirePodSignature;
		public static bool RequirePopSignature => _Preferences.RequirePopSignature;

		// ReSharper disable once InconsistentNaming
		internal static DevicePreferences _Preferences = null!;
	}
}