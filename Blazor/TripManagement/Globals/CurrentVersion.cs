﻿namespace TripManagement;

public partial class Globals
{
	public class CurrentVersion
	{
		public const string PROGRAM         = "Web App",
		                    VERSION         = "1.01",
		                    PROGRAM_VERSION = PROGRAM + " " + VERSION;
	}
}