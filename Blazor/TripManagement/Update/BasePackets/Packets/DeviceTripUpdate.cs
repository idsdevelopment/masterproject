﻿using TripManagement.BasePackets;

namespace TripManagement.Packets;

public class DeviceTripUpdate : UpdateBase<Records.DeviceTripUpdate>
{
	public DeviceTripUpdate()
	{
	}

	public DeviceTripUpdate( Records.DeviceTripUpdate update ) : base( update )
	{
	}
}