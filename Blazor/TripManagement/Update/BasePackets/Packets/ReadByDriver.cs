﻿using TripManagement.BasePackets;

namespace TripManagement.Packets;

public class ReadByDriver : UpdateBase<Records.ReadByDriver>
{
	public ReadByDriver()
	{
	}

	public ReadByDriver( Records.ReadByDriver readByDriver ) : base( readByDriver )
	{
	}
}