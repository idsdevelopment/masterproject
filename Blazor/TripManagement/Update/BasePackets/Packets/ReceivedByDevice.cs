﻿using TripManagement.BasePackets;

namespace TripManagement.Packets;

public class ReceivedByDevice : UpdateBase<Records.ReceivedByDevice>
{
	public ReceivedByDevice()
	{
	}

	public ReceivedByDevice( Records.ReceivedByDevice receivedByDevice ) : base( receivedByDevice )
	{
	}
}