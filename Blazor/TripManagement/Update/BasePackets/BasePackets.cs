﻿namespace TripManagement.BasePackets;

public abstract class UpdateBase;

public abstract class UpdateBase<T> : UpdateBase
{
	public T? Value { get; set; }

	protected UpdateBase()
	{
	}

	protected UpdateBase( T value )
	{
		Value = value;
	}

	public static implicit operator T( UpdateBase<T> update ) => update.Value!;
}