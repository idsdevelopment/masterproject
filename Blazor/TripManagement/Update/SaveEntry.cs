﻿using Newtonsoft.Json;

namespace TripManagement;

public partial class Update
{
	internal class SaveEntry
	{
		public Type?  ClassType   { get; set; }
		public string EntryAsJson { get; set; } = "";

		[JsonIgnore]
		public object? AsObject => ClassType is not null ? JsonConvert.DeserializeObject( EntryAsJson, ClassType ) : null;

		internal SaveEntry()
		{
		}

		internal SaveEntry( object obj )
		{
			ClassType   = obj.GetType();
			EntryAsJson = JsonConvert.SerializeObject( obj );
		}
	}
}