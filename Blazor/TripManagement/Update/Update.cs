﻿using System.Timers;
using Client;
using TripManagement.BasePackets;
using TripManagement.Packets;
using DeviceTripUpdate = TripManagement.Packets.DeviceTripUpdate;
using Timer = System.Timers.Timer;

namespace TripManagement;

using UPDATE_LIST = LinkedList<Update.SaveEntry>;

public partial class Update
{
	private const           string      STORAGE_KEY     = "TripUpdates";
	private static readonly Lock        TripUpdatesLock = new();
	private static          UPDATE_LIST TripUpdates     = [];
	private static readonly Timer       UpdateTimer     = new( 10_000 );
	private static          bool        InUpdate;

	private static async void UpdateTimerOnElapsed( object? sender, ElapsedEventArgs? e )
	{
		try
		{
			if( !InUpdate )
			{
				InUpdate = true;
				TripUpdatesLock.Enter();

				try
				{
					await LoadAsync();

					var Client = IClient.Instance;

					while( TripUpdates.Count > 0 )
					{
						var Value  = TripUpdates.First!.Value;
						var Update = Value.AsObject;

						switch( Update )
						{
						case ReceivedByDevice ReceivedByDevice:
							await Client.ReceivedByDevice( ReceivedByDevice );
							break;

						case ReadByDriver ReadByDriver:
							await Client.ReadByDriver( ReadByDriver );
							break;

						case DeviceTripUpdate DeviceTripUpdate:
							await Client.UpdateTrip( DeviceTripUpdate );
							break;
						}
						TripUpdates.RemoveFirst();
						await SaveAsync();
					}
				}
				catch( Exception E )
				{
				#if DEBUG
					Console.WriteLine( E );
				#endif
				}
				finally
				{
					TripUpdatesLock.Exit();
					InUpdate = false;
				}
			}
		}
		catch
		{
		}
	}

	private static async Task LoadAsync()
	{
		TripUpdatesLock.Enter();

		try
		{
			var Updates = await Storage.Storage.LoadAsync<UPDATE_LIST>( STORAGE_KEY );
			TripUpdates = Updates ?? [];
		}
		catch
		{
		}
		finally
		{
			TripUpdatesLock.Exit();
		}
	}

	private static async Task SaveAsync()
	{
		TripUpdatesLock.Enter();

		try
		{
			await Storage.Storage.SaveAsync( STORAGE_KEY, TripUpdates );
		}
		catch
		{
		}
		finally
		{
			TripUpdatesLock.Exit();
		}
	}

	public static async Task Initialize()
	{
		await LoadAsync();
		UpdateTimer.Elapsed   += UpdateTimerOnElapsed;
		UpdateTimer.AutoReset =  true;
		UpdateTimer.Start();
	}

	public static async Task UpdateTrip( UpdateBase update )
	{
		lock( TripUpdatesLock )
			TripUpdates.AddLast( new SaveEntry( update ) );

		await SaveAsync();

		_ = Tasks.RunVoid( () =>
		                   {
			                   UpdateTimerOnElapsed( null, null );
		                   } );
	}
}