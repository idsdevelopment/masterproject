﻿using WeakEvent;

namespace Events;

public class WeakEvent<TEventArg>
{
#region Value
	public TEventArg? Value
	{
		get;
		set
		{
			field = value;

			if( value is not null )
				RaiseEvent( this, value );
		}
	}
#endregion

	protected virtual void RaiseEvent( object? sender, TEventArg b )
	{
		EventSource.Raise( sender, b );
	}

#region Events
	private readonly WeakEventSource<TEventArg> EventSource = new();

	private event EventHandler<TEventArg> OnEvent
	{
		add => EventSource.Subscribe( value );
		remove => EventSource.Unsubscribe( value );
	}

	public WeakEvent<TEventArg> Add( EventHandler<TEventArg> eventAction )
	{
		OnEvent += eventAction;
		return this;
	}

	public WeakEvent<TEventArg> Remove( EventHandler<TEventArg> eventAction )
	{
		OnEvent -= eventAction;
		return this;
	}

	public static WeakEvent<TEventArg> operator +( WeakEvent<TEventArg> weakEvent, EventHandler<TEventArg> handler ) => weakEvent.Add( handler );
	public static WeakEvent<TEventArg> operator -( WeakEvent<TEventArg> weakEvent, EventHandler<TEventArg> handler ) => weakEvent.Remove( handler );
#endregion
}