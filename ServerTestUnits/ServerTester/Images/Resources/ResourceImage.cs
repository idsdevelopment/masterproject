﻿namespace ServerTester.Image.Resources;

internal class ResourceImage
{
	private static Assembly? MyAssembly;
	public static Stream Get( string imageName ) => ( MyAssembly ??= Assembly.GetExecutingAssembly() ).GetManifestResourceStream( $"ServerTester.Images.Resources.{imageName.Trim()}" ) ?? throw new Exception( "Failed To Get Image" );

	public static byte[] AustralianFlagBytes()
	{
		var Stream = Get( "Australia.png" );
		var L      = Stream.Length;
		var Bytes  = new byte[ L ];
		// ReSharper disable once MustUseReturnValue
		Stream.Read( Bytes, 0, (int)L );
		return Bytes;
	}
}