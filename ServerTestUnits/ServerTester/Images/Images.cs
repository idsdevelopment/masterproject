﻿using ServerTester.Image.Resources;
using static AzureRemoteService.Azure;

namespace ServerTester.Storage;

internal class AzureClient
{
	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await Ids1LoopBack.AzureClient.Client( onLoginOk );
}

[TestClass]
public class Images
{
	public const string TEST_PATH = "Test";

	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var Name = $"{await client.RequestGetNextTripId()}.png";

			                          var Image = new Protocol.Data.Image
			                                      {
				                                      DateTime = DateTimeOffset.Now,
				                                      MetaData = new Dictionary<string, string>
				                                                 {
					                                                 { "Australian", "Flag" }
				                                                 },
				                                      Name   = $"{TEST_PATH}/{Name}",
				                                      Status = STATUS.PICKED_UP,
				                                      Bytes  = ResourceImage.AustralianFlagBytes()
			                                      };

			                          await client.RequestUploadImage( [Image] );

			                          var AzureImages = await client.RequestDownloadImage( TEST_PATH, Image.Name );

			                          switch( AzureImages.Count )
			                          {
			                          case 0:
				                          Assert.Fail( "No images returned" );
				                          break;

			                          case > 1:
				                          Assert.Fail( "Too many images returned" );
				                          break;

			                          default:
				                          var AImage = AzureImages[ 0 ];

				                          if( AImage.Name != Name )
					                          Assert.Fail( "Image name doesn't match" );
				                          else if( AImage.Status != STATUS.PICKED_UP )
					                          Assert.Fail( "Incorrect image status" );
				                          else if( !ArrayExtensions.ArrayCompare( Image.Bytes, AImage.Bytes ) )
					                          Assert.Fail( "Images are different" );
				                          else if( AImage.DateTime != Image.DateTime )
					                          Assert.Fail( "Images dates are different" );
				                          else
				                          {
					                          var Equal = Image.MetaData.Count == AImage.MetaData.Count;

					                          if( Equal )
					                          {
						                          foreach( var Vp in Image.MetaData )
						                          {
							                          var K = Vp.Key;
							                          var V = Vp.Value;

							                          if( !AImage.MetaData.TryGetValue( K, out var Value ) || ( V != Value ) )
							                          {
								                          Equal = false;
								                          break;
							                          }
						                          }
					                          }

					                          if( !Equal )
						                          Assert.Fail( "Meta Data is different" );
				                          }
				                          break;
			                          }
		                          } );
	}
}