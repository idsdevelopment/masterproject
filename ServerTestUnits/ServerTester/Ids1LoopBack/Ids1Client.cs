﻿using System.ServiceModel;
using ServerTester.Ids1ServiceReference;

namespace ServerTester.Ids1LoopBack;

public class EndPoints
{
	public const string JBTEST2 = "http://jbtest2.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    OZ      = "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    REPORTS = "http://reports.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    CUSTOM  = "http://custom.internetdispatcher.org:8080/ids-beans/IDSWS";
}

// ReSharper disable once InconsistentNaming
public class IDSClient : DSWSClient
{
	public readonly string AuthToken;

	public string CarrierId = "",
	              UserName  = "";

	public IDSClient( string authToken, string endPoint ) : base( new BasicHttpBinding( BasicHttpSecurityMode.None ), new EndpointAddress( endPoint ) )
	{
		AuthToken = authToken;
	}

	public class Ids1Service
	{
		public static IDSClient Client( bool useJbTest, string carrierId, string userName, string password )
		{
			var Cid = carrierId.Trim();

			var AToken = $"{Cid}-{userName}-{password}";

			var EndPoint = useJbTest ? EndPoints.JBTEST2 : EndPoints.CUSTOM;

			var RetVal = new IDSClient( AToken, EndPoint );
			RetVal.CarrierId = carrierId;
			RetVal.UserName  = userName;
			return RetVal;
		}


		public static void Client( string carrierId, string userName, string password, Action<IDSClient> onLoginOk, Action? onLoginFail = null, bool stripTest = false )
		{
			try
			{
				using var C = Client( false, carrierId, userName, password );
				onLoginOk( C );
			}
			catch
			{
				try
				{
					onLoginFail?.Invoke();
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}
				throw;
			}
		}

		public static async Task Client( string carrierId, string userName, string password, Func<IDSClient, Task> onLoginOk, Action? onLoginFail = null, bool stripTest = false )
		{
			try
			{
				using var C = Client( false, carrierId, userName, password );
				await onLoginOk( C );
			}
			catch
			{
				try
				{
					onLoginFail?.Invoke();
				}
				catch( Exception Exception )
				{
					Console.WriteLine( Exception );
				}
				throw;
			}
		}


		public static void Client( Action<IDSClient> onLoginOk, Action? onLoginFail = null, bool stripTest = false )
		{
			Client( Globals.CARRIER_ID, Globals.USERNAME, Globals.PASSWORD, onLoginOk, onLoginFail );
		}

		public static async Task Client( Func<IDSClient, Task> onLoginOk, Action? onLoginFail = null, bool stripTest = false )
		{
			await Client( Globals.CARRIER_ID, Globals.USERNAME, Globals.PASSWORD, onLoginOk, onLoginFail );
		}
	}
}