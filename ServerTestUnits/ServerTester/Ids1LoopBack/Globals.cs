﻿namespace ServerTester.Ids1LoopBack;

internal class Globals
{
	internal const string PROGRAM_NAME = "Loop Back Test",
	                      CARRIER_ID   = "LoopBackTest",
	                      USERNAME     = "admin",
	                      PASSWORD     = "admin",
	                      //
	                      PICKUP_COMPANY   = "looppickup",
	                      DELIVERY_COMPANY = "loopdelivery",
	                      BILLING_COMPANY  = "loopbilling";

	internal static readonly string[] ALL_ACCOUNTS = [PICKUP_COMPANY, DELIVERY_COMPANY, BILLING_COMPANY];

	internal static readonly (string UserName, string Password) SOAP_LOGIN = ( "AzureWebService", "8y7b25b798538793b98" );
}