﻿using ServerTester.Ids1ServiceReference;

namespace ServerTester.Ids1LoopBack;

public static class Extensions
{
#region Ids1
	public static remoteAccountValue[]? GetAllIds1Accounts( this IDSClient client ) => client.findAllAccountsForCarrier( new findAllAccountsForCarrier
	                                                                                                                     {
		                                                                                                                     authToken = client.AuthToken
	                                                                                                                     } );

	public static (string Account, bool Ok)[] CheckIds1Accounts( this IDSClient client, string[] accounts )
	{
		var Result = new (string Account, bool Ok)[ accounts.Length ];

		if( client.GetAllIds1Accounts() is { } Accounts )
		{
			var Ndx = 0;

			foreach( var AccountId in accounts )
			{
				var Ok = ( from A in Accounts
				           where A.accountId == AccountId
				           select true ).FirstOrDefault();
				Result[ Ndx++ ] = ( AccountId, Ok );
			}
		}
		return Result;
	}

	public static (bool Ok, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress) Ids1Trip ) GetIds1Trip( this IDSClient client, string tripId )
	{
		var Ids1Trip = client.findTrip( new findTrip
		                                {
			                                authToken = client.AuthToken,
			                                tripId    = tripId
		                                } );

		if( Ids1Trip is { @return: { } Remote } )
		{
			(bool Ok, remoteAddress Address) GetAddress( string addressId )
			{
				var Ids1Address = client.findAddress( new findAddress
				                                      {
					                                      authToken = client.AuthToken,
					                                      addressId = addressId
				                                      } );

				return Ids1Address is { @return: { } Address } ? ( true, Address ) : ( false, null! );
			}

			var (Ok, PickupAddress) = GetAddress( Remote.pickupAddressId );

			if( Ok )
			{
				( Ok, var DeliveryAddress ) = GetAddress( Remote.deliveryAddressId );

				if( Ok )
					return ( true, ( Remote, PickupAddress, DeliveryAddress ) );
			}
		}
		return ( false, ( null!, null!, null! ) );
	}

	public static (bool Ok, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress) Ids1Trip) GetTripInIds1( this IDSClient client, string tripId, STATUS statusCheck = STATUS.UNKNOWN )
	{
		var StatusFail = false;

		// Try for 10 Minutes
		(bool, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress)) PollingTest()
		{
			var Result = client.GetIds1Trip( tripId );

			bool CheckStatusFail()
			{
				return !( StatusFail = ( statusCheck != STATUS.UNKNOWN ) && ( (STATUS)Result.Ids1Trip.Trip.status != statusCheck ) );
			}

			if( Result.Ok && !CheckStatusFail() )
				Result.Ok = false;

			return Result;
		}

		var Result = Tasks.WaitUntil( 10 * 60_000, 10_000, PollingTest );

		if( !Result.Ok )
			Assert.Fail( StatusFail ? $"Status Change Failed For Trip: {tripId}" : $"Cannot Find Trip ({tripId}) In Ids1." );

		return Result;
	}

#region Date / Time
	internal static readonly DateTime Epoch = DateTimeExtensions.Epoch;
	internal static bool Specified( this DateTimeOffset? time ) => time >= Epoch;
	internal static DateTime ServerTime( this DateTimeOffset? time ) => time?.AsPacificStandardTime().DateTime ?? DateTimeExtensions.Epoch;
	internal static DateTime ServerTime( this DateTimeOffset time ) => time.AsPacificStandardTime().DateTime;
#endregion

	public static remoteTripDetailed ToRemoteTripDetailed( this TripUpdate t )
	{
		var Now = DateTimeOffset.Now.LocalTimeToServerTime();

		var DeliveredTime = t.VerifiedTime.ServerTime();

		var PickupTime          = t.PickupTime.ServerTime();
		var PickupTimeSpecified = t.PickupTime.Specified();

		const string RESELLER_ID = Globals.CARRIER_ID;

		if( !int.TryParse( t.Board.Trim(), out var Board ) )
			Board = 0;

		return new remoteTripDetailed
		       {
			       resellerId = RESELLER_ID,
			       accountId  = t.AccountId,

			       status      = (int)t.Status1.ToIds1Status( t.Status2 ),
			       tripId      = t.TripId,
			       lastUpdated = DateTimeExtensions.UnixTicksMilliSeconds(),

			       pieces = (int)t.Pieces,
			       weight = (float)t.Weight,
			       height = 0,
			       length = 0,
			       width  = 0,

			       driver = t.Driver,
			       POD    = true,

			       billingNotes = t.BillingNotes,
			       board        = Board,

			       callTakerId       = t.CallTakerId,
			       callTime          = t.CallTime.ServerTime(),
			       callTimeSpecified = t.CallTime.Specified(),
			       caller            = t.CallerName,
			       callerEmail       = t.CallerEmail,
			       callerPhone       = t.CallerPhone,

			       carChargeAmount       = 0,
			       clientReference       = t.Reference,
			       currentZone           = t.CurrentZone,
			       dangerousGoods        = t.DangerousGoods,
			       declaredValueAmount   = 0,
			       deadlineTime          = t.DueTime.ServerTime(),
			       deadlineTimeSpecified = t.DueTime.Specified(),

			       deliveryResellerId = RESELLER_ID,
			       deliveryAccountId  = t.AccountId,
			       deliveryAddressId  = "",
			       deliveryCompany    = t.DeliveryCompanyName,
			       deliveryStreet     = t.DeliveryAddressAddressLine1,
			       deliverySuite      = t.DeliveryAddressSuite,
			       deliveryCity       = t.DeliveryAddressCity,
			       deliveryProvince   = t.DeliveryAddressRegion,
			       deliveryPc         = t.DeliveryAddressPostalCode,
			       deliveryCountry    = t.DeliveryAddressCountry,

			       deliveredTime          = DeliveredTime,
			       deliveredTimeSpecified = true,

			       deliveryArriveTime          = DeliveredTime,
			       deliveryArriveTimeSpecified = true,

			       deliveryAmount      = 0,
			       deliveryContact     = t.DeliveryContact,
			       deliveryDescription = "",
			       deliveryEmail       = t.DeliveryAddressEmailAddress,
			       deliveryNotes       = t.DeliveryNotes,
			       deliveryPhone       = t.DeliveryAddressPhone,
			       deliveryZone        = t.DeliveryZone,

			       disbursementAmount                = 0,
			       discountAmount                    = 0,
			       disputed                          = false,
			       dontFinalizeDate                  = Epoch,
			       dontFinalizeDateSpecified         = false,
			       dontFinalizeFlag                  = false,
			       dontFinalizeFlagSpecified         = false,
			       driverAssignTime                  = Epoch,
			       driverAssignTimeSpecified         = false,
			       driverPaidCommissionDate          = Epoch,
			       driverPaidCommissionDateSpecified = false,
			       insuranceAmount                   = 0,
			       internalId                        = null,
			       invoiceId                         = 0,
			       invoiced                          = false,
			       isCashTrip                        = false,
			       isCashTripReconciled              = false,
			       isDriverPaid                      = false,
			       mailDrop                          = false,
			       miscAmount                        = 0,
			       modified                          = true,
			       modifiedTripDate                  = Now,
			       modifiedTripDateSpecified         = true,
			       modifiedTripFlag                  = true,
			       modifiedTripFlagSpecified         = true,
			       noGoodsAmount                     = 0,
			       packageType                       = t.PackageType,
			       pallets                           = "",

			       pickupResellerId = RESELLER_ID,
			       pickupAccountId  = t.PickupAccountId,
			       pickupAddressId  = "",
			       pickupCompany    = t.PickupCompanyName,
			       pickupSuite      = t.PickupAddressSuite,
			       pickupStreet     = t.PickupAddressAddressLine1,
			       pickupCity       = t.PickupAddressCity,
			       pickupProvince   = t.PickupAddressRegion,
			       pickupPc         = t.PickupAddressPostalCode,
			       pickupCountry    = t.PickupAddressCountry,

			       pickupTime                = PickupTime,
			       pickupArriveTime          = PickupTime,
			       pickupTimeSpecified       = PickupTimeSpecified,
			       pickupArriveTimeSpecified = PickupTimeSpecified,

			       pickupContact                    = t.PickupContact,
			       pickupDescription                = "",
			       pickupEmail                      = t.PickupAddressEmailAddress,
			       pickupNotes                      = t.PickupNotes,
			       pickupPhone                      = t.PickupAddressPhone,
			       pickupZone                       = t.PickupZone,
			       podName                          = t.Status1 <= STATUS.PICKED_UP ? t.POP : t.POD,
			       priorityInvoiceId                = 0,
			       priorityInvoiceIdSpecified       = false,
			       priorityStatus                   = 0,
			       priorityStatusSpecified          = false,
			       processedByIdsRouteDate          = Epoch,
			       processedByIdsRouteDateSpecified = false,
			       readyTime                        = t.ReadyTime.ServerTime(),
			       readyTimeSpecified               = t.ReadyTime.Specified(),
			       readyTimeString                  = "",
			       readyToInvoiceFlag               = false,
			       readyToInvoiceFlagSpecified      = false,
			       receivedByIdsRouteDate           = Epoch,
			       receivedByIdsRouteDateSpecified  = false,
			       redirectAmount                   = 0,
			       returnTrip                       = false,
			       serviceLevel                     = t.ServiceLevel,
			       sigFilename                      = "",
			       skipUpdateCommonAddress          = true,
			       sortOrder                        = "",
			       stopGroupNumber                  = "",
			       totalAmount                      = 0,
			       totalFixedAmount                 = 0,
			       totalPayrollAmount               = 0,
			       totalTaxAmount                   = 0,
			       waitTimeAmount                   = 0,
			       waitTimeMins                     = 0,
			       weightAmount                     = 0,
			       weightOrig                       = 0,

			       carCharge = t.ReceivedByDevice,
			       redirect  = t.ReadByDriver
		       };
	}

	public static (bool Ok, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress)) CreateIds1Trip( this IDSClient client, TripUpdate t )
	{
		try
		{
			var Trip = t.ToRemoteTripDetailed();

			client.addTrip( new addTrip
			                {
				                authToken            = client.AuthToken,
				                selectedChargeValues = Array.Empty<string>(),
				                selectedCharges      = Array.Empty<string>(),
				                trip                 = Trip
			                } );

			var TripId = t.TripId;

			var Result = Tasks.WaitUntil( 60_000, 10_000, () => client.GetIds1Trip( TripId ) );

			if( !Result.Ok )
				Assert.Fail( $"Cannot Find Trip In Ids1: {TripId}" );

			return Result;
		}
		catch( Exception Exception )
		{
			Assert.Fail( $"Cannot Create Trip In Ids1: {Exception}" );
		}

		return ( false, ( null!, null!, null! ) );
	}

	public static remoteTripDetailed ToRemoteTripDetailed( this remoteTrip t, remoteAddress pickupAddress, remoteAddress deliveryAddress ) => new()
	                                                                                                                                          {
		                                                                                                                                          resellerId = t.resellerId,
		                                                                                                                                          accountId  = t.accountId,

		                                                                                                                                          status      = t.status,
		                                                                                                                                          tripId      = t.tripId,
		                                                                                                                                          lastUpdated = DateTimeExtensions.UnixTicksMilliSeconds(),

		                                                                                                                                          pieces = t.pieces,
		                                                                                                                                          weight = t.weight,
		                                                                                                                                          height = 0,
		                                                                                                                                          length = 0,
		                                                                                                                                          width  = 0,

		                                                                                                                                          driver = t.driver,
		                                                                                                                                          POD    = true,

		                                                                                                                                          billingNotes = t.billingNotes,
		                                                                                                                                          board        = t.board,

		                                                                                                                                          callTakerId       = t.callTakerId,
		                                                                                                                                          callTime          = t.callTime,
		                                                                                                                                          callTimeSpecified = t.callTimeSpecified,
		                                                                                                                                          caller            = t.caller,
		                                                                                                                                          callerEmail       = t.callerEmail,
		                                                                                                                                          callerPhone       = t.callerPhone,

		                                                                                                                                          carChargeAmount       = 0,
		                                                                                                                                          clientReference       = t.clientReference,
		                                                                                                                                          currentZone           = t.currentZone,
		                                                                                                                                          dangerousGoods        = t.dangerousGoods,
		                                                                                                                                          declaredValueAmount   = 0,
		                                                                                                                                          deadlineTime          = t.deadlineTime,
		                                                                                                                                          deadlineTimeSpecified = t.deadlineTimeSpecified,

		                                                                                                                                          deliveryResellerId = t.resellerId,
		                                                                                                                                          deliveryAccountId  = deliveryAddress.accountId,
		                                                                                                                                          deliveryAddressId  = t.deliveryAddressId,
		                                                                                                                                          deliveryCompany    = t.deliveryCompany,
		                                                                                                                                          deliveryStreet     = deliveryAddress.street,
		                                                                                                                                          deliverySuite      = deliveryAddress.suite,
		                                                                                                                                          deliveryCity       = deliveryAddress.city,
		                                                                                                                                          deliveryProvince   = deliveryAddress.province,
		                                                                                                                                          deliveryPc         = deliveryAddress.pc,
		                                                                                                                                          deliveryCountry    = deliveryAddress.country,

		                                                                                                                                          deliveredTime          = t.deliveredTime,
		                                                                                                                                          deliveredTimeSpecified = t.deliveredTimeSpecified,

		                                                                                                                                          deliveryArriveTime          = t.deliveryArriveTime,
		                                                                                                                                          deliveryArriveTimeSpecified = t.deliveryArriveTimeSpecified,

		                                                                                                                                          deliveryAmount      = t.deliveryAmount,
		                                                                                                                                          deliveryContact     = t.deliveryContact,
		                                                                                                                                          deliveryDescription = "",
		                                                                                                                                          deliveryEmail       = t.deliveryEmail,
		                                                                                                                                          deliveryNotes       = t.deliveryNotes,
		                                                                                                                                          deliveryPhone       = t.deliveryPhone,
		                                                                                                                                          deliveryZone        = t.deliveryZone,

		                                                                                                                                          disbursementAmount                = t.disbursementAmount,
		                                                                                                                                          discountAmount                    = t.discountAmount,
		                                                                                                                                          disputed                          = t.disputed,
		                                                                                                                                          dontFinalizeDate                  = t.dontFinalizeDate,
		                                                                                                                                          dontFinalizeDateSpecified         = t.dontFinalizeDateSpecified,
		                                                                                                                                          dontFinalizeFlag                  = t.dontFinalizeFlag,
		                                                                                                                                          dontFinalizeFlagSpecified         = t.dontFinalizeFlagSpecified,
		                                                                                                                                          driverAssignTime                  = t.driverAssignTime,
		                                                                                                                                          driverAssignTimeSpecified         = t.driverAssignTimeSpecified,
		                                                                                                                                          driverPaidCommissionDate          = t.driverPaidCommissionDate,
		                                                                                                                                          driverPaidCommissionDateSpecified = t.driverPaidCommissionDateSpecified,
		                                                                                                                                          insuranceAmount                   = t.insuranceAmount,
		                                                                                                                                          internalId                        = "",
		                                                                                                                                          invoiceId                         = t.invoiceId,
		                                                                                                                                          invoiced                          = t.invoiced,
		                                                                                                                                          isCashTrip                        = t.isCashTrip,
		                                                                                                                                          isCashTripReconciled              = t.isCashTripReconciled,
		                                                                                                                                          isDriverPaid                      = t.isDriverPaid,
		                                                                                                                                          mailDrop                          = t.mailDrop,
		                                                                                                                                          miscAmount                        = t.miscAmount,
		                                                                                                                                          modified                          = t.modified,
		                                                                                                                                          modifiedTripDate                  = t.modifiedTripDate,
		                                                                                                                                          modifiedTripDateSpecified         = t.modifiedTripDateSpecified,
		                                                                                                                                          modifiedTripFlag                  = true,
		                                                                                                                                          modifiedTripFlagSpecified         = true,
		                                                                                                                                          noGoodsAmount                     = 0,
		                                                                                                                                          packageType                       = t.packageType,
		                                                                                                                                          pallets                           = t.pallets,

		                                                                                                                                          pickupResellerId = t.resellerId,
		                                                                                                                                          pickupAccountId  = pickupAddress.accountId,
		                                                                                                                                          pickupAddressId  = t.pickupAddressId,
		                                                                                                                                          pickupCompany    = t.pickupCompany,
		                                                                                                                                          pickupSuite      = pickupAddress.suite,
		                                                                                                                                          pickupStreet     = pickupAddress.street,
		                                                                                                                                          pickupCity       = pickupAddress.city,
		                                                                                                                                          pickupProvince   = pickupAddress.province,
		                                                                                                                                          pickupPc         = pickupAddress.pc,
		                                                                                                                                          pickupCountry    = pickupAddress.country,

		                                                                                                                                          pickupTime                = t.pickupTime,
		                                                                                                                                          pickupTimeSpecified       = t.pickupTimeSpecified,
		                                                                                                                                          pickupArriveTime          = t.pickupArriveTime,
		                                                                                                                                          pickupArriveTimeSpecified = t.pickupArriveTimeSpecified,

		                                                                                                                                          pickupContact     = t.pickupContact,
		                                                                                                                                          pickupDescription = "",
		                                                                                                                                          pickupEmail       = t.pickupEmail,
		                                                                                                                                          pickupNotes       = t.pickupNotes,
		                                                                                                                                          pickupPhone       = t.pickupPhone,
		                                                                                                                                          pickupZone        = t.pickupZone,
		                                                                                                                                          podName           = t.podName,

		                                                                                                                                          priorityInvoiceId                = t.priorityInvoiceId,
		                                                                                                                                          priorityInvoiceIdSpecified       = t.priorityInvoiceIdSpecified,
		                                                                                                                                          priorityStatus                   = t.priorityStatus,
		                                                                                                                                          priorityStatusSpecified          = t.priorityStatusSpecified,
		                                                                                                                                          processedByIdsRouteDate          = t.processedByIdsRouteDate,
		                                                                                                                                          processedByIdsRouteDateSpecified = t.processedByIdsRouteDateSpecified,

		                                                                                                                                          readyTime                       = t.readyTime,
		                                                                                                                                          readyTimeSpecified              = t.readyTimeSpecified,
		                                                                                                                                          readyTimeString                 = t.readyTimeString,
		                                                                                                                                          readyToInvoiceFlag              = t.readyToInvoiceFlag,
		                                                                                                                                          readyToInvoiceFlagSpecified     = t.readyToInvoiceFlagSpecified,
		                                                                                                                                          receivedByIdsRouteDate          = t.receivedByIdsRouteDate,
		                                                                                                                                          receivedByIdsRouteDateSpecified = t.receivedByIdsRouteDateSpecified,
		                                                                                                                                          redirectAmount                  = t.redirectAmount,
		                                                                                                                                          returnTrip                      = t.returnTrip,
		                                                                                                                                          serviceLevel                    = t.serviceLevel,
		                                                                                                                                          sigFilename                     = t.sigFilename,
		                                                                                                                                          skipUpdateCommonAddress         = t.skipUpdateCommonAddress,
		                                                                                                                                          sortOrder                       = t.sortOrder,
		                                                                                                                                          stopGroupNumber                 = t.stopGroupNumber,
		                                                                                                                                          totalAmount                     = t.totalAmount,
		                                                                                                                                          totalFixedAmount                = t.totalFixedAmount,
		                                                                                                                                          totalPayrollAmount              = t.totalPayrollAmount,
		                                                                                                                                          totalTaxAmount                  = t.totalTaxAmount,
		                                                                                                                                          waitTimeAmount                  = t.waitTimeAmount,
		                                                                                                                                          waitTimeMins                    = t.waitTimeMins,
		                                                                                                                                          weightAmount                    = t.weightAmount,
		                                                                                                                                          weightOrig                      = t.weightOrig,

		                                                                                                                                          carCharge = t.carCharge,
		                                                                                                                                          redirect  = t.redirect
	                                                                                                                                          };

	public static bool UpdateIds1Trip( this IDSClient client, remoteTrip t,
	                                   remoteAddress pickupAddress, remoteAddress deliveryAddress,
	                                   string billingNotes, string pickupNotes, string deliveryNotes )
	{
		try
		{
			t.billingNotes  = billingNotes;
			t.pickupNotes   = pickupNotes;
			t.deliveryNotes = deliveryNotes;

			client.updateTripDetailedQuickWithAddressNotes( new updateTripDetailedQuickWithAddressNotes
			                                                {
				                                                authToken            = client.AuthToken,
				                                                _tripVal             = t.ToRemoteTripDetailed( pickupAddress, deliveryAddress ),
				                                                billingAddressNotes  = billingNotes,
				                                                deliveryAddressNotes = deliveryNotes,
				                                                pickupAddressNotes   = pickupNotes
			                                                } );

			var (Ok, _) = Tasks.WaitUntil( 10 * 60_000, 10_000, () =>
			                                                    {
				                                                    var Result = client.GetIds1Trip( t.tripId );

				                                                    if( Result.Ok )
				                                                    {
					                                                    var (_, (Trip, _, _)) = Result;

					                                                    Result.Ok = ( t.status == Trip.status )
					                                                                && ( Trip.billingNotes == billingNotes )
					                                                                && ( Trip.pickupNotes == pickupNotes )
					                                                                && ( Trip.deliveryNotes == deliveryNotes );
				                                                    }
				                                                    return Result;
			                                                    } );

			if( !Ok )
				Assert.Fail( $"Ids1 Trip Not Updated: {t.tripId}" );
			else
				return true;
		}
		catch( Exception Exception )
		{
			Assert.Fail( $"Cannot Update Trip In Ids1: {Exception}" );
		}
		return false;
	}
#endregion

#region Core
	public static async Task<(bool Ok, Dictionary<string, CompanyAddress> Companies)> CheckCoreAccounts( this Azure.InternalClient azureClient )
	{
		var IsOk = true;

		var Accounts = await azureClient.RequestGetCustomerCodeList().ConfigureAwait( false );

		foreach( var Account in Globals.ALL_ACCOUNTS )
		{
			if( !Accounts.Contains( Account ) )
			{
				Assert.Fail( $"Cannot Find Azure Account: {Account}" );
				IsOk = false;
			}
		}

		if( IsOk )
		{
			var Customers = new Dictionary<string, CompanyAddress>();

			foreach( var Account in Globals.ALL_ACCOUNTS )
			{
				var Addr = await azureClient.RequestGetResellerCustomerCompany( Account ).ConfigureAwait( false );

				if( Addr.CompanyName.IsNullOrWhiteSpace() )
				{
					Assert.Fail( $"Cannot Find Azure Address for Account: {Account}" );
					IsOk = false;
				}
				Customers[ Account ] = Addr;
			}
			return ( IsOk, Customers );
		}
		return ( false, new Dictionary<string, CompanyAddress>() );
	}

	public static async Task<(bool Ok, TripUpdate Trip)> CreateTrip( this Azure.InternalClient azureClient, Dictionary<string, CompanyAddress> companies, string board = "" )
	{
		try
		{
			var NextTripId = await azureClient.RequestGetNextTripId().ConfigureAwait( false );

			var Trip = global::Globals.CreateTrip( Globals.CARRIER_ID, NextTripId, companies, board );

			await azureClient.RequestAddUpdateTrip( Trip ).ConfigureAwait( false );

			return ( true, Trip );
		}
		catch( Exception Exception )
		{
			Assert.Fail( $"Azure Exception Updating Trip: {Exception}" );
		}
		return ( false, null! );
	}

	public static async Task<(bool Ok, Trip Trip)> GetTripInCore( this Azure.InternalClient azureClient, string tripId, STATUS status = STATUS.UNKNOWN )
	{
		var Result = await Tasks.WaitUntil( 10 * 60_000, 10_000, async () =>
		                                                         {
			                                                         var Trip = await azureClient.RequestGetTrip( new GetTrip
			                                                                                                      {
				                                                                                                      TripId = tripId
			                                                                                                      } ).ConfigureAwait( false );

			                                                         if( Trip.Ok && ( status != STATUS.UNKNOWN ) && ( Trip.Status1 != status ) )
				                                                         Trip.Ok = false;

			                                                         return ( Trip.Ok, Trip );
		                                                         } ).ConfigureAwait( false );

		if( !Result.Ok )
			Assert.Fail( $"Cannot Find Trip In Core: {tripId}" );

		return Result;
	}
#endregion
}

[TestClass]
public class LoopBackTest
{
	public async Task<(bool Ok, TripUpdate CoreTrip, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress) Ids1Trip)> CreateAndCheckTrip( IDSClient ids1Client, Azure.InternalClient azureClient, Dictionary<string, CompanyAddress> companies )
	{
		var (Ok, CoreTrip) = await azureClient.CreateTrip( companies );

		if( Ok )
		{
			var TripId = CoreTrip.TripId;

			var Ids1Trip = ids1Client.GetTripInIds1( TripId );

			CheckIds1Trip( CoreTrip, Ids1Trip );

			return ( true, CoreTrip, Ids1Trip.Ids1Trip );
		}
		return ( false, null!, ( null!, null!, null! ) );
	}

	public async Task<(bool, Dictionary<string, CompanyAddress> Companies)> CheckCoreFunctions( IDSClient ids1Client, Azure.InternalClient azureClient )
	{
		if( CheckIds1Accounts( ids1Client ) )
		{
			var (Ok, Companies) = await azureClient.CheckCoreAccounts();

			if( Ok )
			{
				( Ok, var CoreTrip, _ ) = await CreateAndCheckTrip( ids1Client, azureClient, Companies );

				if( Ok )
				{
					var TripId = CoreTrip.TripId;

					// Modify trip
					CoreTrip.Status1 = STATUS.DISPATCHED;
					await azureClient.RequestAddUpdateTrip( CoreTrip );

					var Ids1Trip = ids1Client.GetTripInIds1( TripId, STATUS.DISPATCHED );

					CheckIds1Trip( CoreTrip, Ids1Trip );

					TripId          = $"{TripId}-Ids1";
					CoreTrip.TripId = TripId;
					Ids1Trip        = ids1Client.CreateIds1Trip( CoreTrip );
					CheckIds1Trip( CoreTrip, Ids1Trip );

					( Ok, CoreTrip ) = await azureClient.GetTripInCore( TripId );

					if( Ok )
					{
						var (_, (RemoteTrip, PickupAddress, DeliveryAddress)) = Ids1Trip;

						if( Compare( CoreTrip, RemoteTrip, PickupAddress, DeliveryAddress ) )
						{
							RemoteTrip.status = (int)STATUS.PICKED_UP;

							ids1Client.UpdateIds1Trip( RemoteTrip,
							                           PickupAddress, DeliveryAddress,
							                           "New Billing Note", "New Pickup Note", "New Delivery Note" );
							return ( true, Companies );
						}
					}
				}
			}
		}
		return ( false, null! );
	}

	public async Task CheckDroidFunctions( IDSClient ids1Client, Azure.InternalClient azureClient, Dictionary<string, CompanyAddress> companies )
	{
		var (Ok, CoreTrip, _) = await CreateAndCheckTrip( ids1Client, azureClient, companies );

		if( Ok )
		{
			var TripId = CoreTrip.TripId;

			await azureClient.RequestUpdateTripFromDevice( new DeviceTripUpdate( Globals.PROGRAM_NAME )
			                                               {
				                                               TripId             = TripId,
				                                               Status             = STATUS.VERIFIED,
				                                               Status1            = CoreTrip.Status2,
				                                               UndeliverableNotes = "Droid Undeliverable Note",
				                                               Pieces             = CoreTrip.Pieces,
				                                               Weight             = CoreTrip.Weight,
				                                               Reference          = CoreTrip.Reference,
				                                               PopPod             = CoreTrip.POD,
				                                               Signature          = new Signature(),
				                                               UpdateLatitude     = 0,
				                                               UpdateLongitude    = 0,
				                                               UpdateTime         = DateTimeOffset.Now
			                                               } );

			( Ok, var Trip ) = await azureClient.GetTripInCore( TripId, STATUS.VERIFIED );

			if( Ok )
			{
				Thread.Sleep( 10_000 );

				( Ok, var (RemoteTrip, PickupAddress, DeliveryAddress) ) = ids1Client.GetTripInIds1( TripId );

				if( Ok )
					Compare( Trip, RemoteTrip, PickupAddress, DeliveryAddress );
			}
		}
	}

	[TestMethod]
	public async Task Execute()
	{
		await IDSClient.Ids1Service.Client( async ids1Client =>
		                                    {
			                                    await AzureClient.Client( async azureClient =>
			                                                              {
				                                                              var (Ok, Companies) = await CheckCoreFunctions( ids1Client, azureClient );

				                                                              if( Ok )
					                                                              await CheckDroidFunctions( ids1Client, azureClient, Companies );
			                                                              } );
		                                    } );
	}

	private static void CheckIds1Trip( TripUpdate trip, (bool Ok, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress) Ids1Trip) ids1Trip )
	{
		if( !ids1Trip.Ok || !Compare( trip, ids1Trip.Ids1Trip ) )
			Assert.Fail( "Ids1 Trip Failed To Compare" );
	}

#region Ids1
	private static bool CheckIds1Accounts( IDSClient ids1Client )
	{
		var Result = true;

		foreach( var Account in ids1Client.CheckIds1Accounts( Globals.ALL_ACCOUNTS ) )
		{
			if( !Account.Ok )
			{
				Assert.Fail( $"Cannot Find Ids1 Account: {Account.Account}" );
				Result = false;
			}
		}
		return Result;
	}
#endregion

#region Compare
	private static bool Cmp( string s, string s1, string field )
	{
		if( s != s1 )
		{
			Assert.Fail( $"Field: {field} Didn't Compare. \"{s}\", \"{s1}\"" );
			return false;
		}
		return true;
	}

	private static bool Cmp( STATUS s, int s1, string field )
	{
		var S1 = (STATUS)s1;

		if( s != S1 )
		{
			Assert.Fail( $"Field: {field} Didn't Compare. \"{s.AsString()}\", \"{S1.AsString()}\"" );
			return false;
		}
		return true;
	}

	private static bool Cmp( decimal v, int v1, string field )
	{
		if( v != v1 )
		{
			Assert.Fail( $"Field: {field} Didn't Compare. \"{v}\", \"{v1}\"" );
			return false;
		}
		return true;
	}

	private static bool Cmp( decimal v, float v1, string field )
	{
		if( v != (decimal)v1 )
		{
			Assert.Fail( $"Field: {field} Didn't Compare. \"{v}\", \"{v1}\"" );
			return false;
		}
		return true;
	}

	public static bool Compare( TripUpdate t, (remoteTrip Trip, remoteAddress pickupAddress, remoteAddress deliveryAddress) ids1Trip ) => Compare( t, ids1Trip.Trip, ids1Trip.pickupAddress, ids1Trip.deliveryAddress );

	public static bool Compare( TripUpdate t, remoteTrip t1, remoteAddress pA, remoteAddress pD ) => Cmp( t.TripId, t1.tripId, "Trip Id" )
	                                                                                                 && Cmp( t.Status1, t1.status, "Status" )
	                                                                                                 //
	                                                                                                 && Cmp( t.AccountId, t1.accountId, "Account Id" )
	                                                                                                 //
	                                                                                                 && Cmp( t.PickupCompanyName, t1.pickupCompany, "Pickup Company" )
	                                                                                                 && Cmp( t.PickupAddressSuite, pA.suite, "Pickup Suite" )
	                                                                                                 && Cmp( t.PickupAddressAddressLine1, pA.street, "Pickup Street" )
	                                                                                                 && Cmp( t.PickupAddressCity, pA.city, "Pickup City" )
	                                                                                                 && Cmp( t.PickupAddressRegion, pA.province, "Pickup Region" )
	                                                                                                 && Cmp( t.PickupAddressCountry, pA.country, "Pickup Country" )
	                                                                                                 && Cmp( t.PickupAddressPostalCode, pA.pc, "Pickup Postal Code" )
	                                                                                                 && Cmp( t.PickupNotes, t1.pickupNotes, "Pickup Notes" )
	                                                                                                 //
	                                                                                                 && Cmp( t.DeliveryCompanyName, t1.deliveryCompany, "Delivery Company" )
	                                                                                                 && Cmp( t.DeliveryAddressSuite, pD.suite, "Delivery Suite" )
	                                                                                                 && Cmp( t.DeliveryAddressAddressLine1, pD.street, "Delivery Street" )
	                                                                                                 && Cmp( t.DeliveryAddressCity, pD.city, "Delivery City" )
	                                                                                                 && Cmp( t.DeliveryAddressRegion, pD.province, "Delivery Region" )
	                                                                                                 && Cmp( t.DeliveryAddressCountry, pD.country, "Delivery Country" )
	                                                                                                 && Cmp( t.DeliveryAddressPostalCode, pD.pc, "Delivery Postal Code" )
	                                                                                                 && Cmp( t.DeliveryNotes, t1.deliveryNotes, "Delivery Notes" )
	                                                                                                 //
	                                                                                                 && Cmp( Globals.CARRIER_ID, t1.billingCompany, "Billing Company" ) // Ids1 puts in the carrier Id
	                                                                                                 && Cmp( t.BillingNotes, t1.billingNotes, "Billing Notes" )
	                                                                                                 //
	                                                                                                 && Cmp( t.Reference, t1.clientReference, "Reference" )
	                                                                                                 && Cmp( t.PackageType, t1.packageType, "Package Type" )
	                                                                                                 && Cmp( t.ServiceLevel, t1.serviceLevel, "Service Level" )
	                                                                                                 //
	                                                                                                 && Cmp( t.Pieces, t1.pieces, "Pieces" )
	                                                                                                 && Cmp( t.Weight, t1.weight, "Weight" );
#endregion
}