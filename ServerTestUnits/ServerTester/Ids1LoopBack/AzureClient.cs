﻿using static AzureRemoteService.Azure;

namespace ServerTester.Ids1LoopBack;

internal class AzureClient
{
	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( Globals.CARRIER_ID, Globals.USERNAME, Globals.PASSWORD, onLoginOk );
	public static async Task Client( SLOT slot, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( slot, Globals.CARRIER_ID, Globals.USERNAME, Globals.PASSWORD, onLoginOk );
}