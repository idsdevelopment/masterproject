﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.PostCodes;

[TestClass]
public class PostcodeZones
{
	private const string ROUTE_NAME   = "Test Route",
	                     POSTCODE     = "Test Postal Code",
	                     ZONE1        = "Zone1",
	                     ZONE2        = "Zone2",
	                     ZONE3        = "Zone3",
	                     ZONE4        = "Zone4",
	                     ABBREV_ZONE1 = "Z1",
	                     ABBREV_ZONE2 = "Z2",
	                     ABBREV_ZONE3 = "Z3",
	                     ABBREV_ZONE4 = "Z4";


	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var PostalRoute = new PostcodesToZones
			                                            {
				                                            Program           = Globals.PROGRAM,
				                                            RouteName         = ROUTE_NAME,
				                                            PostalCode        = POSTCODE,
				                                            Zone1             = ZONE1,
				                                            Zone2             = ZONE2,
				                                            Zone3             = ZONE3,
				                                            Zone4             = ZONE4,
				                                            AbbreviationZone1 = ABBREV_ZONE1,
				                                            AbbreviationZone2 = ABBREV_ZONE2,
				                                            AbbreviationZone3 = ABBREV_ZONE3,
				                                            AbbreviationZone4 = ABBREV_ZONE4
			                                            };

			                          await client.RequestAddUpdatePostalZones( PostalRoute );
			                          Thread.Sleep( 1_000 ); // Give Server Time

			                          var PRoutes = await client.RequestGetPostalZones( ROUTE_NAME, POSTCODE, ZONE1 );

			                          if( PRoutes.Count != 1 )
				                          Assert.Fail( "Too many records" );

			                          var P = PRoutes[ 0 ];

			                          if( ( P.RouteName != ROUTE_NAME ) || ( P.PostalCode != POSTCODE )
			                                                            || ( P.Zone1 != ZONE1 )
			                                                            || ( P.Zone2 != ZONE2 )
			                                                            || ( P.Zone3 != ZONE3 )
			                                                            || ( P.Zone4 != ZONE4 ) )
				                          Assert.Fail( "Record does note match" );

			                          PRoutes = await client.RequestGetPostalZones( "*", "*", "*" );

			                          if( PRoutes.Count != 1 )
				                          Assert.Fail( "Too many records" );

			                          await client.RequestDeletePostalZones( ROUTE_NAME, POSTCODE );
			                          Thread.Sleep( 1_000 ); // Give Server Time

			                          PRoutes = await client.RequestGetPostalZones( ROUTE_NAME, POSTCODE, "" );

			                          if( PRoutes.Count != 0 )
				                          Assert.Fail( "Records not deleted" );
		                          } );
	}
}