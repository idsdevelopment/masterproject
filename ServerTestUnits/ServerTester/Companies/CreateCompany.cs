﻿namespace ServerTester.Companies;

[TestClass]
public class CreateCompany
{
	private const string TEST_COMPANY   = "PmlPlay",
	                     ADMIN          = "Admin",
	                     ADMIN_PASSWORD = "Admin";

	[TestMethod]
	public async Task Execute()
	{
		await TestLogin.Client( Azure.SLOT.ALPHA_TRW, $"ids/{TEST_COMPANY}", "Terry", "T",
		                        async client =>
		                        {
			                        //  var IsNew = await client.RequestIsNewCarrier();

			                        var ProcId = await client.RequestCreateCarrier( TEST_COMPANY,
			                                                                        ADMIN,
			                                                                        ADMIN_PASSWORD );

			                        while( await client.RequestIsProcessRunning( ProcId ) )
				                        Thread.Sleep( 10_000 );
		                        }
		                      );
	}
}