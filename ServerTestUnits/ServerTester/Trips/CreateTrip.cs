﻿public static class Globals
{
	public static TripUpdate CreateTrip( string carrierId, string tripId, Dictionary<string, CompanyAddress> companies, string board = "" )
	{
		var PickupCo   = companies[ ServerTester.Ids1LoopBack.Globals.PICKUP_COMPANY ];
		var DeliveryCo = companies[ ServerTester.Ids1LoopBack.Globals.DELIVERY_COMPANY ];
		var BillingCo  = companies[ ServerTester.Ids1LoopBack.Globals.BILLING_COMPANY ];

		var Now = DateTimeOffset.Now;

		var TripUpdate = new TripUpdate
		                 {
			                 Program      = ServerTester.Ids1LoopBack.Globals.PROGRAM_NAME,
			                 LastModified = DateTime.Now,

			                 AccountId                   = carrierId,
			                 TripId                      = tripId,
			                 Barcode                     = "",
			                 Status1                     = STATUS.ACTIVE,
			                 BillingAccountId            = ServerTester.Ids1LoopBack.Globals.BILLING_COMPANY,
			                 BillingAddressBarcode       = "",
			                 BillingCompanyName          = BillingCo.CompanyName,
			                 BillingAddressSuite         = BillingCo.Suite,
			                 BillingAddressAddressLine1  = BillingCo.AddressLine1,
			                 BillingAddressAddressLine2  = BillingCo.AddressLine2,
			                 BillingAddressCity          = BillingCo.City,
			                 BillingAddressRegion        = BillingCo.Region,
			                 BillingAddressPostalCode    = BillingCo.PostalCode,
			                 BillingAddressPostalBarcode = BillingCo.PostalBarcode,
			                 BillingAddressCountry       = BillingCo.Country,
			                 BillingAddressCountryCode   = BillingCo.CountryCode,
			                 BillingAddressVicinity      = BillingCo.Vicinity,
			                 BillingAddressEmailAddress  = BillingCo.EmailAddress,
			                 BillingAddressEmailAddress1 = BillingCo.EmailAddress1,
			                 BillingAddressEmailAddress2 = BillingCo.EmailAddress2,
			                 BillingAddressFax           = BillingCo.Fax,
			                 BillingAddressLatitude      = BillingCo.Latitude,
			                 BillingAddressLongitude     = BillingCo.Longitude,
			                 BillingAddressMobile        = BillingCo.Mobile,
			                 BillingAddressMobile1       = BillingCo.Mobile1,
			                 BillingAddressNotes         = BillingCo.Notes,
			                 BillingAddressPhone         = BillingCo.Phone,
			                 BillingAddressPhone1        = BillingCo.Phone1,
			                 BillingContact              = BillingCo.ContactName,
			                 BillingNotes                = BillingCo.Notes,

			                 PickupAccountId            = ServerTester.Ids1LoopBack.Globals.PICKUP_COMPANY,
			                 PickupAddressBarcode       = "",
			                 PickupCompanyName          = PickupCo.CompanyName,
			                 PickupAddressSuite         = PickupCo.Suite,
			                 PickupAddressAddressLine1  = PickupCo.AddressLine1,
			                 PickupAddressAddressLine2  = PickupCo.AddressLine2,
			                 PickupAddressCity          = PickupCo.City,
			                 PickupAddressRegion        = PickupCo.Region,
			                 PickupAddressPostalCode    = PickupCo.PostalCode,
			                 PickupAddressPostalBarcode = PickupCo.PostalBarcode,
			                 PickupAddressCountry       = PickupCo.Country,
			                 PickupAddressCountryCode   = PickupCo.CountryCode,
			                 PickupAddressVicinity      = PickupCo.Vicinity,
			                 PickupAddressEmailAddress  = PickupCo.EmailAddress,
			                 PickupAddressEmailAddress1 = PickupCo.EmailAddress1,
			                 PickupAddressEmailAddress2 = PickupCo.EmailAddress2,
			                 PickupAddressFax           = PickupCo.Fax,
			                 PickupAddressLatitude      = PickupCo.Latitude,
			                 PickupAddressLongitude     = PickupCo.Longitude,
			                 PickupAddressMobile        = PickupCo.Mobile,
			                 PickupAddressMobile1       = PickupCo.Mobile1,
			                 PickupAddressNotes         = PickupCo.Notes,
			                 PickupAddressPhone         = PickupCo.Phone,
			                 PickupAddressPhone1        = PickupCo.Phone1,
			                 PickupContact              = PickupCo.ContactName,
			                 PickupNotes                = PickupCo.Notes,

			                 DeliveryAccountId            = ServerTester.Ids1LoopBack.Globals.DELIVERY_COMPANY,
			                 DeliveryAddressBarcode       = "",
			                 DeliveryCompanyName          = DeliveryCo.CompanyName,
			                 DeliveryAddressSuite         = DeliveryCo.Suite,
			                 DeliveryAddressAddressLine1  = DeliveryCo.AddressLine1,
			                 DeliveryAddressAddressLine2  = DeliveryCo.AddressLine2,
			                 DeliveryAddressCity          = DeliveryCo.City,
			                 DeliveryAddressRegion        = DeliveryCo.Region,
			                 DeliveryAddressPostalCode    = DeliveryCo.PostalCode,
			                 DeliveryAddressPostalBarcode = DeliveryCo.PostalBarcode,
			                 DeliveryAddressCountry       = DeliveryCo.Country,
			                 DeliveryAddressCountryCode   = DeliveryCo.CountryCode,
			                 DeliveryAddressVicinity      = DeliveryCo.Vicinity,
			                 DeliveryAddressEmailAddress  = DeliveryCo.EmailAddress,
			                 DeliveryAddressEmailAddress1 = DeliveryCo.EmailAddress1,
			                 DeliveryAddressEmailAddress2 = DeliveryCo.EmailAddress2,
			                 DeliveryAddressFax           = DeliveryCo.Fax,
			                 DeliveryAddressLatitude      = DeliveryCo.Latitude,
			                 DeliveryAddressLongitude     = DeliveryCo.Longitude,
			                 DeliveryAddressMobile        = DeliveryCo.Mobile,
			                 DeliveryAddressMobile1       = DeliveryCo.Mobile1,
			                 DeliveryAddressNotes         = DeliveryCo.Notes,
			                 DeliveryAddressPhone         = DeliveryCo.Phone,
			                 DeliveryAddressPhone1        = DeliveryCo.Phone1,
			                 DeliveryContact              = DeliveryCo.ContactName,
			                 DeliveryNotes                = DeliveryCo.Notes,

			                 Board                    = board,
			                 BroadcastToDispatchBoard = true,
			                 BroadcastToDriver        = true,
			                 BroadcastToDriverBoard   = true,

			                 CallTakerId = ServerTester.Ids1LoopBack.Globals.CARRIER_ID,
			                 CallTime    = Now,
			                 CallerEmail = "",
			                 CallerName  = ServerTester.Ids1LoopBack.Globals.CARRIER_ID,

			                 CallerPhone        = "",
			                 ClaimTime          = DateTimeOffset.MinValue,
			                 CurrentZone        = "",
			                 OriginalPieceCount = 10,
			                 Pieces             = 10,
			                 Reference          = "Loop Back Test",
			                 ServiceLevel       = "Service Level",
			                 PackageType        = "Package Type",
			                 Weight             = 10
		                 };

		return TripUpdate;
	}
}