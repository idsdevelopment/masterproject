﻿namespace ServerTester.Trips;

internal static class TripExtensions
{
	public static async Task<Trip> GetTrip( this Azure.InternalClient client, string tripId ) => await client.RequestGetTrip( new GetTrip
	                                                                                                                          {
		                                                                                                                          Signatures = false,
		                                                                                                                          TripId     = tripId
	                                                                                                                          } );
}