﻿namespace ServerTester.Zones;

[TestClass]
public class Zones
{
	private const string ZONE_NAME = "AAAA";

	[TestMethod]
	public async Task Execute()
	{
		await TestLogin.Client( async client =>
		                        {
			                        async Task<Protocol.Data.Zones> GetZones()
			                        {
				                        Thread.Sleep( 1_000 );
				                        return await client.RequestZones();
			                        }

			                        async Task<Protocol.Data.Zones> UpdateZones( Protocol.Data.Zones zones )
			                        {
				                        await client.RequestAddUpdateZones( zones );
				                        return await GetZones();
			                        }

			                        var Zones = await UpdateZones( new Protocol.Data.Zones( Globals.PROGRAM )
			                                                       {
				                                                       new()
				                                                       {
					                                                       Abbreviation = "A",
					                                                       Name         = ZONE_NAME,
					                                                       Id           = 0,
					                                                       SortIndex    = 1,
					                                                       ZoneId       = 0
				                                                       }
			                                                       } );

			                        switch( Zones.Count )
			                        {
			                        case 0:
				                        Assert.Fail( "No Records Found" );
				                        break;

			                        case > 1:
				                        Assert.Fail( $"Too Many Records Found: {Zones.Count}" );
				                        break;

			                        case 1 when Zones[ 0 ].Name == ZONE_NAME:
				                        Zones[ 0 ].SortIndex = 2;
				                        Zones                = await UpdateZones( Zones );

				                        switch( Zones.Count )
				                        {
				                        case 0:
					                        Assert.Fail( "No Records Found" );
					                        break;

				                        case > 1:
					                        Assert.Fail( $"Too Many Records Found: {Zones.Count}" );
					                        break;

				                        case 1 when ( Zones[ 0 ].Name == ZONE_NAME ) && ( Zones[ 0 ].SortIndex == 2 ):
					                        await client.RequestDeleteZones( [ZONE_NAME] );

					                        Zones = await GetZones();

					                        if( Zones.Count != 0 )
						                        Assert.Fail( "Zone not deleted" );
					                        break;

				                        default:
					                        Assert.Fail( $"Zone Name Or Index Doesn't Match: {Zones[ 0 ].Name}, Index={Zones[ 0 ].SortIndex}" );
					                        break;
				                        }

				                        break;

			                        default:
				                        Assert.Fail( $"Zone Name Doesn't Match: {Zones[ 0 ].Name}" );
				                        break;
			                        }
		                        } );
	}
}