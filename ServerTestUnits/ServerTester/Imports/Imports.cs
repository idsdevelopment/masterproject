﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.Imports;

[TestClass]
public class Imports
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                                {
			                                                await client.RequestCheckForImportsAndExports();
		                                                } );
	}
}