﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.Emails;

[TestClass]
public class TriggerEmailSend
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                                {
			                                                await client.RequestProcessEmails();
		                                                } );
	}
}