﻿using static AzureRemoteService.Azure;

namespace ServerTester._Customers.Azure;

internal class AzureLogin
{
	public const string AZURE_USER           = "AzureWebService",
	                    WEB_SERVICE_PASSWORD = "8y7b25b798538793b98";

	public static async Task Client( string carrierId, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( carrierId, AZURE_USER, WEB_SERVICE_PASSWORD, onLoginOk );
	public static async Task Client( SLOT slot, string carrierId, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( slot, carrierId, AZURE_USER, WEB_SERVICE_PASSWORD, onLoginOk );
}