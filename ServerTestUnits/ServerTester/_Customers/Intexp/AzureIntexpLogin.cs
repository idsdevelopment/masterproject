﻿using ServerTester._Customers.Azure;
using static AzureRemoteService.Azure;

namespace ServerTester._Customers.Intexp;

internal class AzureIntexpLogin
{
	public const string ACCOUNT_ID = "intexp";

	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await AzureLogin.Client( ACCOUNT_ID, onLoginOk );
	public static async Task Client( SLOT slot, Func<InternalClient, Task> onLoginOk ) => await AzureLogin.Client( slot, ACCOUNT_ID, onLoginOk );
}