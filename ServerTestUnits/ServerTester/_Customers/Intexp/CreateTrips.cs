﻿namespace ServerTester._Customers.Intexp;

[TestClass]
public class CreateTrips
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureIntexpLogin.Client( async client =>
		                               {
			                               await client.RequestAddUpdateTripAutoCreate( new TripUpdate() );
		                               } );
	}
}