﻿using ServerTester.Ids1LoopBack;

namespace ServerTester._Customers.Metro;

[TestClass]
public class CheckIds1
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                                   {
			                                                                   await client.RequestCheckMetroIds1Connection();
		                                                                   } );
	}
}