﻿using ServerTester._Customers.PhoenixTest;

namespace ServerTester._Customers.Phoenix;

[TestClass]
public class GetAccounts
{
	[TestMethod]
	public async Task Execute()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var Accounts = await client.RequestGetCustomerCodeCompanyNameList();
			                               Console.WriteLine( $"Count: {Accounts.Count}" );

			                               foreach( var Account in Accounts )
				                               Console.WriteLine( $"Account: {Account.CustomerCode} -- {Account.CompanyName}" );
		                               } );
	}
}