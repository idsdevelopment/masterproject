﻿namespace ServerTester._Customers.PhoenixTest;

[TestClass]
public class TripCreate
{
	[TestMethod]
	public async Task CreateTrip()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var TripId = await client.RequestGetNextTripId();

			                               var Update = new TripUpdate( Globals.PROGRAM )
			                                            {
				                                            AccountId           = "Terry2",
				                                            TripId              = TripId,
				                                            Status1             = STATUS.ACTIVE,
				                                            Pieces              = 1,
				                                            Weight              = 1,
				                                            PickupCompanyName   = "Pickup Company",
				                                            DeliveryCompanyName = "Delivery Company",
				                                            Reference           = "123456",
				                                            ServiceLevel        = "Service Level",
				                                            PackageType         = "Package Type"
			                                            };
			                               await client.RequestAddUpdateTrip( Update );
		                               } );
	}
}