﻿using ServerTester.Trips;

namespace ServerTester._Customers.PhoenixTest;

[TestClass]
public class GetTrip
{
	[TestMethod]
	public async Task Execute()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var Trip = await client.GetTrip( "J649265597397" );
			                               Console.WriteLine( $"Pod: {Trip.POD}\r\nPop: {Trip.POP}\r\nCallTakerId: {Trip.CallTakerId}" );
		                               } );
	}
}