﻿namespace ServerTester._Customers.PhoenixTest;

[TestClass]
public class AccountCreate
{
	[TestMethod]
	public async Task CreateAccount()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var Co = new Company
			                                        {
				                                        CompanyName = "Terry2's Company",
				                                        Enabled     = true,

				                                        Barcode      = "666666666666",
				                                        Suite        = "Suite",
				                                        AddressLine1 = "Address Line 1",
				                                        AddressLine2 = "Address Line 2",
				                                        City         = "City",
				                                        Region       = "Region",
				                                        PostalCode   = "123456",
				                                        Country      = "Country",
				                                        CountryCode  = "CO",
				                                        Phone        = "1234567890",
				                                        Mobile       = "123",
				                                        ContactName  = "Contact",
				                                        EmailAddress = "123@666.com",
				                                        ZoneName     = "Zone"
			                                        };

			                               await client.RequestAddUpdatePrimaryCompany( new AddUpdatePrimaryCompany( Globals.PROGRAM )
			                                                                            {
				                                                                            BillingCompany     = Co,
				                                                                            ShippingCompany    = Co,
				                                                                            CustomerCode       = "Terry2",
				                                                                            Password           = Encryption.ToTimeLimitedToken( "Password" ),
				                                                                            SuggestedLoginCode = "Terry",
				                                                                            Company            = Co,
				                                                                            UserName           = "Terry"
			                                                                            } );
		                               } );
	}
}