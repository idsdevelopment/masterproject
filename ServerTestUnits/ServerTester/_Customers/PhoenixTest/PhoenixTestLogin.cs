﻿using static AzureRemoteService.Azure;

namespace ServerTester._Customers.PhoenixTest;

internal class PhoenixTestLogin
{
	public const string ACCOUNT_ID = "phoenixtest",
	                    USERNAME   = "admin",
	                    PASSWORD   = "enarc";

	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
	public static async Task Client( SLOT slot, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( slot, ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
}