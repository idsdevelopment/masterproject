﻿namespace ServerTester._Customers.PhoenixTest;

[TestClass]
public class GetCustomerCompaniesDetailed
{
	[TestMethod]
	public async Task Execute()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var Result = await client.RequestGetCustomerCompaniesDetailed( "shieldmasonryltd5" );
		                               } );
	}
}

[TestClass]
public class GetResellerCustomerCompany
{
	[TestMethod]
	public async Task Execute()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var Result = await client.RequestGetResellerCustomerCompany( "0741779bcltd" );
		                               } );
	}
}