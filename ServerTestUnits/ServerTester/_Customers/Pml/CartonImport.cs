﻿using Utils.Csv;

namespace ServerTester._Customers.Pml;

[TestClass]
public class CartonImport
{
	private const string IMPORT_FILE = @"C:\Temp\ReturnableProductsDetails-QA.csv";

	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                           {
			                                                           var Csv = new Csv( new FileStream( IMPORT_FILE, FileMode.Open ) );

			                                                           var RowCount = Csv.RowCount;

			                                                           if( RowCount > 1 )
			                                                           {
				                                                           var Cartons = new InventoryList();

				                                                           const int DESCRIPTION     = 1,
				                                                                     PACKAGE_BARCODE = 2,
				                                                                     CARTON_BARCODE  = 3,
				                                                                     QUANTITY        = 4;

				                                                           for( var Row = 1; Row < RowCount; Row++ )
				                                                           {
					                                                           var R = Csv[ Row ];

					                                                           Cartons.Add( new Inventory
					                                                                        {
						                                                                        Barcode         = R[ CARTON_BARCODE ].AsString,
						                                                                        InventoryCode   = R[ PACKAGE_BARCODE ].AsString,
						                                                                        PackageQuantity = R[ QUANTITY ].AsDecimal,
						                                                                        Description     = R[ DESCRIPTION ].AsString
					                                                                        } );
				                                                           }

				                                                           var ProcessId = await client.RequestAddUpdateCartons( Cartons );

				                                                           do
					                                                           await Task.Delay( 5_000 );
				                                                           while( await client.RequestIsProcessRunning( ProcessId ) );
			                                                           }
		                                                           } );
	}
}