﻿using static AzureRemoteService.Azure;

namespace ServerTester._Customers.Pml;

public class PmlTest
{
	private const string ACCOUNT_ID = "PmlTest",
	                     USERNAME   = "foxlin789",
	                     PASSWORD   = "390equator";

	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
	public static async Task Client( SLOT slow, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( slow, ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
}

public class PmlPlay
{
	private const string ACCOUNT_ID = "PmlPlay",
	                     USERNAME   = "foxlin789",
	                     PASSWORD   = "390equator";

	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
	public static async Task Client( SLOT slow, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( slow, ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
}

public class PmlLive
{
	private const string ACCOUNT_ID = "Pml",
	                     USERNAME   = "foxlin789",
	                     PASSWORD   = "390equator";

	public static async Task Client( Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
	public static async Task Client( SLOT slow, Func<InternalClient, Task> onLoginOk ) => await TestLogin.Client( slow, ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
}