﻿using static AzureRemoteService.Azure;

namespace ServerTester._Customers.Pml;

[TestClass]
public class PmlLiveClient
{
	private const string ACCOUNT_ID = "Pml",
	                     USERNAME   = "foxlin789",
	                     PASSWORD   = "390equator";

	public static async Task Client( Func<InternalClient, Task> onLoginOk )
	{
		await TestLogin.Client( ACCOUNT_ID, USERNAME, PASSWORD, onLoginOk );
	}
}