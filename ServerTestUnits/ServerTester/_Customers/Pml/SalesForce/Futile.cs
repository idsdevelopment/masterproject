﻿namespace ServerTester._Customers.Pml.SalesForce;

[TestClass]
public class Futile
{
	[TestMethod]
	public async Task Execute()
	{
		await PmlTest.Client( async client =>
		                  {
			                  var ImportedTrips = await client.RequestGetTripsStartingWith( new SearchTripsByStatus
			                                                                                {
				                                                                                StartStatus = STATUS.ACTIVE,
				                                                                                EndStatus   = STATUS.DISPATCHED,
				                                                                                TripId      = "IMP"
			                                                                                } );

			                  if( ImportedTrips.Count > 0 )
			                  {
				                  var Trip = ImportedTrips[ 0 ];

				                  await client.RequestPML_Futile( new Protocol.Data._Customers.Pml.Futile
				                                                  {
					                                                  TripIds     = [Trip.TripId],
					                                                  DateTime    = DateTimeOffset.Now,
					                                                  Reason      = "Server Test",
					                                                  ProgramName = "Server Test",
					                                                  Status      = STATUS.DELETED
				                                                  } );
			                  }
			                  else
				                  Assert.Fail( "No imported trips found" );
		                  } );
	}
}