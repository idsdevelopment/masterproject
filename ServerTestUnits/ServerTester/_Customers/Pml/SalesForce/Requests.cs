﻿using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Imports.Pml.Order;
using Imports.Pml.ReturnOrderAck;
using Imports.Pml.Schemas.RemoveOrderFromQueue;
using Constants = Imports.Pml.Constants;
using TripList = Imports.TripList;

namespace ServerTester._Customers.Pml.SalesForce;

internal class Requests
{
#if !DEBUG
	private static readonly HttpClient Client = new();
#else
	private static readonly HttpClient Client = new(
	                                                new CustomHttpMessageHandler()
	                                               );

	public class CustomHttpMessageHandler : DelegatingHandler
	{
		public CustomHttpMessageHandler()
		{
			InnerHandler = new HttpClientHandler
			               {
				               ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
			               };
		}

		protected override async Task<HttpResponseMessage> SendAsync( HttpRequestMessage request, CancellationToken cancellationToken )
		{
			Debug.WriteLine( $"Request: {request}" );

			if( request.Content is not null )
				Debug.WriteLine( $"Request Content: {await request.Content.ReadAsStringAsync()}" );

			// Call the inner handler
			var Response = await base.SendAsync( request, cancellationToken );

			Debug.WriteLine( $"Response: {Response}" );

			if( Response.Content is { } Content )
				Debug.WriteLine( $"Response Content: {await Content.ReadAsStringAsync()}" );

			return Response;
		}
	}
#endif

	public static HttpRequestMessage NewReturnOrderMessage() => NewMessage( HttpMethod.Get, Constants.TESTING_RETURN_ORDER_URL );
	public static HttpRequestMessage NewAcknowledgeOrderMessage() => NewMessage( HttpMethod.Delete, Constants.TESTING_RETURN_ORDER_URL );

	public static async Task AcknowledgeOrders( TripList trips )
	{
		var Message = NewAcknowledgeOrderMessage();

		List<string> ToRemove = [];

		foreach( var Trip in trips )
		{
			Message.Content                     = new StringContent( new DeleteOrders( Trip.ReceiptHandle ).ToXml() );
			Message.Content.Headers.ContentType = new MediaTypeHeaderValue( "application/xml" );

			var Response = await Client.SendAsync( Message );
			var Content  = await Response.Content.ReadAsStringAsync();

			if( Content.FromXmlSuccessResponse() is not { IsSuccess: true } )
				ToRemove.Add( Trip.MessageId );
		}

		// Remove the failed orders
		for( var Ndx = trips.Count - 1; Ndx >= 0; Ndx-- )
		{
			if( ToRemove.Contains( trips[ Ndx ].MessageId ) )
				trips.RemoveAt( Ndx );
		}
	}

	public static async Task GetOrders()
	{
		try
		{
			var Message  = NewReturnOrderMessage();
			var Response = await Client.SendAsync( Message );
			var Content  = await Response.Content.ReadAsStringAsync();
			Content = WebUtility.HtmlDecode( Content ); // To Readable XML

			// Fix the XML
			const string XML_HEADER = "<?xml version='1.0' encoding='UTF-8'?>";

			Console.WriteLine( $"""
			                    Received Content (Before fixing):
			                    {Content}

			                    """ );

			Content = $"{XML_HEADER}{Content.Replace( XML_HEADER, "" )}";

			Console.WriteLine( $"""
			                    Fixed Content:
			                    {Content}

			                    """ );

			var Trips = Content.ToTrips();

			if( Trips is not { Count: > 0 } )
				Assert.Fail( "Failed To Convert To Trip" );
			else
				await AcknowledgeOrders( Trips );
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
			Assert.Fail();
		}
	}

	private static HttpRequestMessage NewMessage( HttpMethod method, string uri ) => new()
	                                                                                 {
		                                                                                 RequestUri = new Uri( uri ),
		                                                                                 Method     = method,
		                                                                                 Headers =
		                                                                                 {
			                                                                                 { Constants.CLIENT_ID_KEY, Constants.TESTING_CLIENT_ID },
			                                                                                 { Constants.SECRET_CODE_KEY, Constants.TESTING_SECRET_CODE },
			                                                                                 { Constants.CHANNEL_KEY, Constants.CHANNEL_VALUE }
		                                                                                 }
	                                                                                 };
}

[TestClass]
public class Test
{
	[TestMethod]
	public async Task TestGetOrders()
	{
		await Requests.GetOrders();
	}
}