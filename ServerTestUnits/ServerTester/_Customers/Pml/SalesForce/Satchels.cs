﻿using Protocol.Data._Customers.Pml;

namespace ServerTester._Customers.Pml.SalesForce;

[TestClass]
public class Satchels
{
	[TestMethod]
	public async Task Execute()
	{
		await PmlTest.Client( async client =>
		                  {
			                  var ImportedTrips = await client.RequestGetTripsStartingWith( new SearchTripsByStatus
			                                                                                {
				                                                                                StartStatus = STATUS.ACTIVE,
				                                                                                EndStatus   = STATUS.DISPATCHED,
				                                                                                TripId      = "IMP"
			                                                                                } );

			                  if( ImportedTrips.Count > 0 )
			                  {
				                  var Trip = ImportedTrips[ 0 ];

				                  await client.RequestPML_UpdateSatchel( new Satchel
				                                                         {
					                                                         ProgramName = "Server Test",
					                                                         SatchelId   = $"AUDR{new Random().Next( 900000, 999999 )}",
					                                                         TripIds     = [Trip.TripId],
					                                                         Driver      = "IDSSupport",
					                                                         Packages    = Trip.Packages,
					                                                         PickupTime  = DateTimeOffset.Now,
					                                                         Signature   = new Signature()
				                                                         } );
			                  }
			                  else
				                  Assert.Fail( "No imported trips found" );
		                  } );
	}
}