﻿namespace ServerTester._Customers.Pml.SalesForce;

// ReSharper disable once InconsistentNaming
[TestClass]
public class ExportCCB2BCsv
{
	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                               {
			                                                               await client.RequestPML_ExportToCCB2B();
		                                                               } );
	}
}