﻿using Utils.Csv;

namespace ServerTester._Customers.Pml;

[TestClass]
public class SecondaryAccountLocationBarcodeImport
{
	private const string IMPORT_FILE = @"C:\Temp\GLN-Lookup-QA.csv";

	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                               {
			                                                               try
			                                                               {
				                                                               var Csv = new Csv( new FileStream( IMPORT_FILE, FileMode.Open ) );

				                                                               var RowCount = Csv.RowCount;

				                                                               if( RowCount > 1 )
				                                                               {
					                                                               var Dict = new AccountToLocationBarcode
					                                                                          {
						                                                                          Purge = true
					                                                                          };

					                                                               const int ACCOUNT  = 1,
					                                                                         LOCATION = 0;

					                                                               for( var Row = 1; Row < RowCount; Row++ )
					                                                               {
						                                                               var R = Csv[ Row ];
						                                                               Dict[ R[ ACCOUNT ].AsString ] = R[ LOCATION ].AsString;
					                                                               }

					                                                               var ProcessId = await client.RequestImportSecondaryAccountLocationBarcodes( Dict );

					                                                               do
						                                                               await Task.Delay( 5_000 );
					                                                               while( await client.RequestIsProcessRunning( ProcessId ) );
				                                                               }
			                                                               }
			                                                               catch( Exception Exception )
			                                                               {
				                                                               Assert.Fail( Exception.Message );
			                                                               }
		                                                               } );
	}
}