﻿using ServerTester._Customers.PhoenixTest;

namespace ServerTester.Staff;

[TestClass]
public class Staff
{
	[TestMethod]
	public async Task GetStaffAndRoles()
	{
		await PhoenixTestLogin.Client( async client =>
		                               {
			                               var Staff = await client.RequestGetStaffAndRoles();

			                               foreach( var S in Staff )
			                               {
				                               Console.WriteLine( S.StaffId );

				                               foreach( var Role in S.Roles )
					                               Console.WriteLine( $"-->{Role.Name}" );
			                               }
		                               } );
	}
}