﻿namespace ServerTester.Mapping;

[TestClass]
public class ValidateAddresses
{
	[TestMethod]
	public async Task Execute()
	{
		await TestLogin.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                              {
			                                              var Result = await client.RequestValidateAddresses( Globals.Addresses );

			                                              if( Result is null )
				                                              Assert.Fail( "Result is null" );
			                                              else if( Result.Count == 0 )
				                                              Assert.Fail( "Result is empty" );
			                                              else
			                                              {
				                                              var Fail = false;

				                                              foreach( var Item in Result )
				                                              {
					                                              Fail |= !Item.IsValid;

					                                              Console.WriteLine( Item.IsValid
						                                                                 ? $"{Item.AddressLine1} is valid"
						                                                                 : $"{Item.AddressLine1} is invalid" );
				                                              }

				                                              if( Fail )
					                                              Assert.Fail( "One or more addresses are invalid" );
			                                              }
		                                              }
		                      );
	}
}