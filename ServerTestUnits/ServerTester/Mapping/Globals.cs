﻿using HtmlAgilityPack;

namespace ServerTester.Mapping;

public static class Globals
{
	public static readonly AddressValidationList Addresses =
	[
		new AddressValidation
		{
			AddressLine1 = "17A Nursery St",
			City         = "Hornsby",
			Region       = "NSW",
			PostalCode   = "2077",
			Country      = "Australia"
		},

		new AddressValidation
		{
			AddressLine1 = "20 Pretoria Pde",
			City         = "Hornsby",
			Region       = "NSW",
			PostalCode   = "2077",
			Country      = "Australia"
		},

		new AddressValidation
		{
			AddressLine1 = "115 Pacific Highway",
			City         = "Hornsby",
			Region       = "NSW",
			PostalCode   = "2077",
			Country      = "Australia"
		}
	];

	public static readonly RouteOptimisationAddresses Route = new( Addresses );

	public static string ConvertHtmlToPlainText( this string htmlContent )
	{
		var HtmlDocument = new HtmlDocument();
		HtmlDocument.LoadHtml( htmlContent );
		return HtmlDocument.DocumentNode.InnerText;
	}
}