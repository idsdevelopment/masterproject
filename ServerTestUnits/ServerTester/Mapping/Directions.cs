﻿namespace ServerTester.Mapping;

[TestClass]
public class Directions
{
	[TestMethod]
	public async Task Execute()
	{
		await TestLogin.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                              {
			                                              var Result = await client.RequestDirections( Globals.Route );

			                                              if( Result is not null )
			                                              {
				                                              if( Result.Count > 0 )
				                                              {
					                                              foreach( var D in Result )
					                                              {
						                                              foreach( var R in D.Route )
						                                              {
							                                              foreach( var Step in R.Steps )
							                                              {
								                                              var Text = Step.HtmlInstructions.ConvertHtmlToPlainText();

								                                              if( Text.IsNotNullOrWhiteSpace() )
								                                              {
									                                              var Start = Step.StartLocation;
									                                              Console.WriteLine( $"{Start.Latitude:N6}, {Start.Longitude:N6} - {Text}" );
								                                              }
							                                              }
						                                              }
					                                              }
				                                              }
				                                              else
					                                              Console.WriteLine( "Nothing found." );
			                                              }
			                                              else
				                                              Console.WriteLine( "Result is null." );
		                                              }
		                      );
	}
}