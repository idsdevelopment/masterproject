﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.OverridesRedirects;

[TestClass]
public class OverridesRedirects
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( async client =>
		                          {
			                          await client.RequestAddUpdatePackageOverride( new OverridePackage
			                                                                        {
				                                                                        FromPackageTypeId = 99,
				                                                                        ToPackageTypeId   = 99,
				                                                                        CompanyId         = 1,
				                                                                        CompanyCode       = "",
				                                                                        SortIndex         = -1
			                                                                        } );
		                          } );
	}
}