﻿namespace ServerTester.WebJobs;

[TestClass]
public class Log
{
	[TestMethod]
	public async Task WriteLog()
	{
		await TestLogin.Client( async client =>
		                        {
			                        var Message = new WebLog
			                                      {
				                                      CarrierId = TestLogin.AccountId,
				                                      Program   = TestLogin.VERSION,
				                                      Message   = "Starting Web Log"
			                                      };

			                        await client.RequestLogWebJob( Message );

			                        Thread.Sleep( 1_000 );

			                        Message.Message = "Ending Web Log";

			                        await client.RequestLogWebJob( Message );
		                        } );
	}

	[TestMethod]
	public async Task GetCarriers()
	{
		await TestLogin.Client( async client =>
		                        {
			                        var Carriers = await client.RequestGetWebJobCarrierList();

			                        foreach( var Carrier in Carriers )
				                        Console.WriteLine( Carrier );
		                        } );
	}

	[TestMethod]
	public async Task GetCarrierLogs()
	{
		await TestLogin.Client( async client =>
		                        {
			                        var Logs = await client.RequestGetWebJobList( TestLogin.AccountId );

			                        foreach( var Log in Logs )
				                        Console.WriteLine( Log );
		                        } );
	}

	[TestMethod]
	public async Task GetLogs()
	{
		await TestLogin.Client( async client =>
		                        {
			                        var Logs = await client.RequestGetWebJobs( TestLogin.AccountId, TestLogin.VERSION );

			                        foreach( var Log in Logs )
				                        Console.WriteLine( Log );
		                        } );
	}

	[TestMethod]
	public async Task GetLogContents()
	{
		await TestLogin.Client( async client =>
		                        {
			                        var Logs = await client.RequestGetWebJobs( TestLogin.AccountId, TestLogin.VERSION );
			                        var C    = Logs.Count;

			                        if( C > 0 )
			                        {
				                        var Contents = await client.RequestGetWebJobContents( TestLogin.AccountId, TestLogin.VERSION, Logs[ C - 1 ] );
				                        Console.WriteLine( Contents );
			                        }
		                        } );
	}
}