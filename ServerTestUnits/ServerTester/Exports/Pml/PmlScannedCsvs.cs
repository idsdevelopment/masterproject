﻿using Utils.Csv;

// ReSharper disable InconsistentNaming

namespace ServerTester._Customers.Pml;

[TestClass]
public class PmlScannedCsvs
{
	internal class WorkingTrip
	{
		public string            TripId             { get; }
		public string            Barcode            { get; }
		public decimal           Pieces             { get; set; }
		public decimal           OriginalPieceCount { get; }
		public List<TripPackage> Packages           { get; }

		public string BillingNotes { get; set; }

		public DateTimeOffset CallTime { get; set; }

		public bool IsMaster { get; }

		public STATUS Status { get; }

		public bool CountError { get; set; }

		public WorkingTrip( Trip trip )
		{
			TripId       = trip.TripId;
			Status       = trip.Status1;
			BillingNotes = trip.BillingNotes;
			CallTime     = trip.CallTime ?? DateTimeOffset.MinValue;

			IsMaster = !trip.Reference.Contains( "->" );

			var P = trip.Packages;
			Packages = P;
			var Item0 = P[ 0 ].Items[ 0 ];

			Barcode            = Item0.Barcode;
			OriginalPieceCount = Item0.Original;
			Pieces             = Item0.Pieces;
		}
	}


	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                               {
			                                                               var FDate = new DateTime( 2024, 4, 1 );
			                                                               var TDate = new DateTime( 2024, 8, 31 );

			                                                               var SydneyTimeZone = TimeZoneInfo.FindSystemTimeZoneById( "AUS Eastern Standard Time" );
			                                                               var Offset         = SydneyTimeZone.GetUtcOffset( FDate );

			                                                               var FromDate = new DateTimeOffset( FDate, Offset );
			                                                               var ToDate   = new DateTimeOffset( TDate, Offset ).EndOfDay();

			                                                               var OTrips = await client.RequestSearchTrips( new SearchTrips
			                                                                                                             {
				                                                                                                             StartStatus = STATUS.NEW,
				                                                                                                             EndStatus   = STATUS.LIMBO,
				                                                                                                             FromDate    = FromDate,
				                                                                                                             ToDate      = ToDate
			                                                                                                             } );
			                                                               var WorkingTrips = OTrips.Select( t => new WorkingTrip( t ) ).ToList();

			                                                               var AurtDict = new Dictionary<string, List<WorkingTrip>>();

			                                                               foreach( var WorkingTrip in WorkingTrips )
			                                                               {
				                                                               var Split = WorkingTrip.BillingNotes.Split( ['\r', '\n', ' '], StringSplitOptions.RemoveEmptyEntries );

				                                                               foreach( var S in Split )
				                                                               {
					                                                               if( S.StartsWith( "AURT" ) )
					                                                               {
						                                                               if( !AurtDict.TryGetValue( S, out var Trips ) )
							                                                               AurtDict[ S ] = Trips = [];

						                                                               Trips.Add( WorkingTrip );
						                                                               break;
					                                                               }
				                                                               }
			                                                               }

			                                                               // Should put AUDRs first as others start with 'O'
			                                                               var Flattened = ( from A in AurtDict
			                                                                                 orderby A.Key
			                                                                                 select ( A.Key, ( from V in A.Value
			                                                                                                   orderby V.TripId, V.Barcode
			                                                                                                   select V ).ToList() ) ).ToList();

			                                                               foreach( var (_, Trips) in Flattened )
			                                                               {
				                                                               if( Trips.Count > 0 )
				                                                               {
					                                                               var Trip = Trips[ 0 ];
					                                                               var Audr = Trip.TripId;

					                                                               if( Audr.StartsWith( "AUDR" ) )
						                                                               Trips.RemoveAt( 0 );
				                                                               }
			                                                               }

			                                                               // Fix up the O trip values
			                                                               foreach( var (_, Trips) in Flattened )
			                                                               {
				                                                               foreach( var Trip in Trips )
					                                                               Trip.Pieces = 0;

				                                                               foreach( var Trip in Trips )
				                                                               {
					                                                               if( Trip.IsMaster )
					                                                               {
						                                                               foreach( var TripPackage in Trip.Packages )
						                                                               {
							                                                               var Items  = TripPackage.Items;
							                                                               var Count  = Items.Count;
							                                                               var TCount = Trips.Count;

							                                                               Trip.CountError = Count != TCount;
							                                                               Count           = Math.Min( Count, TCount );

							                                                               foreach( var TripItem in Items )
							                                                               {
								                                                               var Barcode = TripItem.Barcode;

								                                                               for( var Ndx = 0; Ndx < Count; Ndx++ )
								                                                               {
									                                                               try
									                                                               {
										                                                               var T = Trips[ Ndx ];

										                                                               if( T.Barcode == Barcode )
										                                                               {
											                                                               T.Pieces += TripItem.Pieces;
											                                                               break;
										                                                               }
									                                                               }
									                                                               catch( Exception Exception )
									                                                               {
										                                                               Console.WriteLine( Exception );
									                                                               }
								                                                               }
							                                                               }
						                                                               }
					                                                               }
				                                                               }
			                                                               }

			                                                               var Csv = new Csv();

			                                                               var RowNdx = 0;

			                                                               var Row = Csv[ RowNdx++ ];
			                                                               Row[ 0 ].AsString = "AURT";
			                                                               Row[ 1 ].AsString = "Date";
			                                                               Row[ 2 ].AsString = "Shipment ID";
			                                                               Row[ 3 ].AsString = "Barcode";
			                                                               Row[ 4 ].AsString = "Original Quantity";
			                                                               Row[ 5 ].AsString = "Scanned Quantity";
			                                                               Row[ 6 ].AsString = "Status";
			                                                               Row[ 7 ].AsString = "Status As Text";
			                                                               Row[ 8 ].AsString = "Has Difference";
			                                                               Row[ 9 ].AsString = "Count Error";

			                                                               foreach( var (Aurt, Trips) in Flattened )
			                                                               {
				                                                               Row = Csv[ RowNdx++ ];

				                                                               Row[ 0 ].AsString = Aurt;

				                                                               foreach( var Trip in Trips )
				                                                               {
					                                                               Row[ 1 ].AsString = $"{Trip.CallTime:s}";
					                                                               Row[ 2 ].AsString = Trip.TripId;

					                                                               Row[ 3 ].AsString  = Trip.Barcode;
					                                                               Row[ 4 ].AsDecimal = Trip.OriginalPieceCount;
					                                                               Row[ 5 ].AsDecimal = Trip.Pieces;
					                                                               Row[ 6 ].AsDecimal = (decimal)Trip.Status;
					                                                               Row[ 7 ].AsString  = Trip.Status.AsString();

					                                                               Row[ 8 ].AsString = Trip.Pieces != Trip.OriginalPieceCount ? "**********" : "";
					                                                               Row[ 9 ].AsString = Trip.CountError ? "**********" : "";

					                                                               Row               = Csv[ RowNdx++ ];
					                                                               Row[ 0 ].AsString = "";
				                                                               }
				                                                               RowNdx--;
			                                                               }

			                                                               Csv.WriteToFile( @"c:\Temp\PmlAurt.csv" );
		                                                               } );
	}
}