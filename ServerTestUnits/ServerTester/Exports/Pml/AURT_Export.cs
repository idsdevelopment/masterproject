﻿// ReSharper disable InconsistentNaming

using Utils.Csv;

namespace ServerTester._Customers.Pml;

[TestClass]
public class AURT_Export
{
	[TestMethod]
	public async Task Execute()
	{
		const string CSV_PATH = @"E:\Temp\PmLExport.Csv";

		try
		{
			var FromDate = new DateTimeOffset( new DateTime( 2021, 1, 25 ) );
			var ToDate   = DateTimeOffset.Now.EndOfDay();

			await _Customers.Pml.PmlTest.Client( async client =>
			                                 {
				                                 var Trips = await client.RequestSearchTrips( new SearchTrips
				                                                                              {
					                                                                              FromDate    = FromDate,
					                                                                              ToDate      = ToDate,
					                                                                              StartStatus = STATUS.NEW,
					                                                                              EndStatus   = STATUS.LIMBO
				                                                                              } );

				                                 var Ordered = ( from T in Trips
				                                                 orderby T.CallTime
				                                                 let AURT = T.BillingNotes.Trim()
				                                                 group new { T, AURT } by AURT
				                                                 into G
				                                                 let Rec = G.First()
				                                                 let TripIds = ( from T1 in G
				                                                                 select T1.T.TripId
				                                                               ).ToList()
				                                                 select new
				                                                        {
					                                                        Rec.T.CallTime,
					                                                        Rec.AURT,
					                                                        TripIds
				                                                        } ).ToList();

				                                 var Csv = new Csv();

				                                 var RowNumber = 0;

				                                 foreach( var Rec in Ordered )
				                                 {
					                                 var Row = Csv[ RowNumber++ ];
					                                 Row[ 0 ] = $"{Rec.CallTime:s}";
					                                 Row[ 1 ] = Rec.AURT;
					                                 Row[ 2 ] = Rec.TripIds.Count.ToString();
					                                 Row[ 3 ] = string.Join( ", ", Rec.TripIds );
				                                 }

				                                 Csv.WriteToFile( CSV_PATH );
			                                 } );
		}
		catch( Exception Exception )
		{
			Assert.Fail( Exception.Message );
		}
	}
}