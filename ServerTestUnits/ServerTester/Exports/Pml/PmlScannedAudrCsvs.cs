﻿using Utils.Csv;

// ReSharper disable InconsistentNaming

namespace ServerTester._Customers.Pml;

[TestClass]
public class PmlScannedAudrCsvs
{
	internal class WorkingTrip
	{
		public string  TripId             { get; }
		public string  Barcode            { get; } = "";
		public decimal OriginalPieceCount { get; }

		public bool IsMaster { get; }

		public STATUS Status { get; }

		public string Reference { get; }

		public string AURT = "",
		              AUDR = "";

		public string BillingNotes;

		public DateTimeOffset CallTime;

		public decimal Scanned,
		               Disbursed;

		public WorkingTrip( Trip trip )
		{
			TripId       = trip.TripId;
			Status       = trip.Status1;
			BillingNotes = trip.BillingNotes;
			CallTime     = trip.CallTime ?? DateTimeOffset.MinValue;
			Reference    = trip.Reference;

			IsMaster = !trip.Reference.Contains( "->" );

			foreach( var TripPackage in trip.Packages )
			{
				var IsFirst = true;

				foreach( var Item in TripPackage.Items )
				{
					var BCode = Item.Barcode;

					if( IsFirst )
					{
						IsFirst            = false;
						Barcode            = BCode;
						OriginalPieceCount = Item.Original;
					}

					var ItemPieces = Item.Pieces;

					if( !Items.TryGetValue( BCode, out var Pieces ) )
					{
						Items[ BCode ] = new ScannedResidual
						                 {
							                 Scanned  = ItemPieces,
							                 Residual = ItemPieces
						                 };
					}
					else
					{
						Pieces.Scanned  += ItemPieces;
						Pieces.Residual += ItemPieces;
					}
				}
			}
		}

	#region Item
		public class ScannedResidual
		{
			public decimal Scanned,
			               Residual;
		}

		public Dictionary<string, ScannedResidual> Items = [];
	#endregion
	}


	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                               {
			                                                               var FDate = new DateTime( 2024, 4, 1 );
			                                                               var TDate = new DateTime( 2024, 8, 31 );

			                                                               var SydneyTimeZone = TimeZoneInfo.FindSystemTimeZoneById( "AUS Eastern Standard Time" );
			                                                               var Offset         = SydneyTimeZone.GetUtcOffset( FDate );

			                                                               var FromDate = new DateTimeOffset( FDate, Offset );
			                                                               var ToDate   = new DateTimeOffset( TDate, Offset ).EndOfDay();

			                                                               var OTrips = await client.RequestSearchTrips( new SearchTrips
			                                                                                                             {
				                                                                                                             StartStatus = STATUS.NEW,
				                                                                                                             EndStatus   = STATUS.LIMBO,
				                                                                                                             FromDate    = FromDate,
				                                                                                                             ToDate      = ToDate
			                                                                                                             } );
			                                                               var WorkingTrips = OTrips.Select( t => new WorkingTrip( t ) ).ToList();

			                                                               var AurtDict = new Dictionary<string, List<WorkingTrip>>();
			                                                               var TripDict = new Dictionary<string, WorkingTrip>();
			                                                               var AudrDict = new Dictionary<string, WorkingTrip>();

			                                                               foreach( var WorkingTrip in WorkingTrips )
			                                                               {
				                                                               var TripId = WorkingTrip.TripId;

				                                                               TripDict[ TripId ] = WorkingTrip;

				                                                               if( TripId.StartsWith( "AUDR" ) )
					                                                               AudrDict[ TripId ] = WorkingTrip;
				                                                               else
				                                                               {
					                                                               var Split = WorkingTrip.BillingNotes.Split( ['\r', '\n', ' '], StringSplitOptions.RemoveEmptyEntries );

					                                                               foreach( var S in Split )
					                                                               {
						                                                               if( S.StartsWith( "AURT" ) )
						                                                               {
							                                                               WorkingTrip.AURT = S;

							                                                               if( !AurtDict.TryGetValue( S, out var Trips ) )
								                                                               AurtDict[ S ] = Trips = [];

							                                                               Trips.Add( WorkingTrip );
							                                                               break;
						                                                               }
					                                                               }
				                                                               }
			                                                               }

			                                                               // Match Audr to Aurt
			                                                               foreach( var KeyVal in AudrDict )
			                                                               {
				                                                               var AudrTrip = KeyVal.Value;
				                                                               var Audr     = KeyVal.Key;

				                                                               var TripIds = AudrTrip.Reference.Split( ',' );

				                                                               foreach( var TripId in TripIds )
				                                                               {
					                                                               if( TripDict.TryGetValue( TripId, out var Trip ) )
						                                                               Trip.AUDR = Audr;
				                                                               }
			                                                               }

			                                                               // Now fill in the values actually scanned
			                                                               foreach( var KeyPair in AurtDict )
			                                                               {
				                                                               var AurtTrips = KeyPair.Value;

				                                                               var Audr = "";

				                                                               foreach( var WorkingTrip in AurtTrips )
				                                                               {
					                                                               Audr = WorkingTrip.AUDR;

					                                                               if( Audr != "" )
						                                                               break;
				                                                               }

				                                                               if( Audr != "" )
				                                                               {
					                                                               var AudrTrip = AudrDict[ Audr ];

					                                                               foreach( var AurtTrip in AurtTrips )
					                                                               {
						                                                               var BCode = AurtTrip.Barcode;

						                                                               if( AudrTrip.Items.TryGetValue( BCode, out var Pieces ) )
						                                                               {
							                                                               AurtTrip.Scanned = Pieces.Scanned;

							                                                               var Residual = Pieces.Residual;

							                                                               if( Residual > 0 )
							                                                               {
								                                                               var Allowed = Math.Min( AurtTrip.OriginalPieceCount, Residual );
								                                                               AurtTrip.Disbursed = Allowed;

								                                                               Pieces.Residual -= Allowed;
							                                                               }
						                                                               }
					                                                               }
				                                                               }
				                                                               else
					                                                               AurtTrips[ 0 ].AUDR = "**** NO AUDR ****";
			                                                               }

			                                                               // Should put AUDRs first as others start with 'O'
			                                                               var Flattened = ( from A in AurtDict
			                                                                                 orderby A.Key
			                                                                                 select ( A.Key, ( from V in A.Value
			                                                                                                   orderby V.IsMaster descending, V.TripId
			                                                                                                   select V ).ToList() ) ).ToList();

			                                                               var Csv = new Csv();

			                                                               var RowNdx = 0;

			                                                               var Row = Csv[ RowNdx++ ];
			                                                               Row[ 0 ].AsString  = "AURT";
			                                                               Row[ 1 ].AsString  = "Date";
			                                                               Row[ 2 ].AsString  = "Shipment ID";
			                                                               Row[ 3 ].AsString  = "AUDR";
			                                                               Row[ 4 ].AsString  = "Barcode";
			                                                               Row[ 5 ].AsString  = "Original Quantity";
			                                                               Row[ 6 ].AsString  = "Scanned Quantity";
			                                                               Row[ 7 ].AsString  = "Disbursed";
			                                                               Row[ 8 ].AsString  = "Status";
			                                                               Row[ 9 ].AsString  = "Status As Text";
			                                                               Row[ 10 ].AsString = "Has Difference";

			                                                               foreach( var (Aurt, Trips) in Flattened )
			                                                               {
				                                                               Row = Csv[ RowNdx++ ];

				                                                               Row[ 0 ].AsString = Aurt;

				                                                               foreach( var Trip in Trips )
				                                                               {
					                                                               Row[ 1 ].AsString = $"{Trip.CallTime:s}";
					                                                               Row[ 2 ].AsString = Trip.TripId;

					                                                               Row[ 3 ].AsString  = Trip.AUDR;
					                                                               Row[ 4 ].AsString  = Trip.Barcode;
					                                                               Row[ 5 ].AsDecimal = Trip.OriginalPieceCount;
					                                                               Row[ 6 ].AsDecimal = Trip.Scanned;
					                                                               Row[ 7 ].AsDecimal = Trip.Disbursed;
					                                                               Row[ 8 ].AsDecimal = (decimal)Trip.Status;
					                                                               Row[ 9 ].AsString  = Trip.Status.AsString();

					                                                               Row[ 10 ].AsString = Trip.Scanned != Trip.OriginalPieceCount ? "**********" : "";

					                                                               Row               = Csv[ RowNdx++ ];
					                                                               Row[ 0 ].AsString = "";
				                                                               }
				                                                               RowNdx--;
			                                                               }

			                                                               Csv.WriteToFile( @"c:\Temp\PmlAurtAudr.csv" );
		                                                               } );
	}
}