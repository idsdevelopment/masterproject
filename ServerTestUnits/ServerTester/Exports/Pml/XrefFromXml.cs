﻿namespace ServerTester._Customers.Pml;

[TestClass]
public class XrefFromXml
{
	public const string XML = """
	                          <?xml version='1.0' encoding='UTF-8'?>
	                          <response-data>
	                            <messages>
	                              <message>
	                                <MessageId>f8d71044-8623-498f-925b-d3277340d678</MessageId>
	                                <ReceiptHandle>AQEBHYoEmVKTdjtjZpBNr0Loez4Ls5BNd2uaD99RKZILSK4b6dwNZqavZOaom9/cl0EOFEZ93PPcU2hChIlQ/GNRicu+Cvey2sA/PG2JTveKq2O9PTt94hNDfmI42M+ueWsMgC/1M7juzFdGWXgR3YsA5aVMUKyaFsxJU4pBkZPx0YsilsWBvdy3HfaCKcLT8DUj6urCctiSiUrAtw+FtzsGomtKqDPHgJZTpGGvY6IHEHgDSGvgh5P2wocK9mwg2F38a12FxYA1IDg6orpH8FkXVw9ptTUBMCgSlP7QcdlD6v2oHQpon8yrqcdVmfrvKRmE1yv7q66NRcTsCMSbYFP0LeeiCbJKwzm2NfMaMBpfvvVOfOM42xAD/q/uVVM3LDrJ99SMDqFuV5sn4Rd3x4Z57T+NN0qlNSgGPfgK2S+x6mc=</ReceiptHandle>
	                                <Body>
	                          <Order>
	                            <SellerId>50146</SellerId>
	                            <Storefront>AustraliaStore</Storefront>
	                            <CustomerSellerID>9377770009177</CustomerSellerID>
	                            <OrderName>O-0027301515</OrderName>
	                            <OrderNumber>28301515</OrderNumber>
	                            <OrderDate>2024-09-03</OrderDate>
	                            <OrderStatus>Ready To Be Sent</OrderStatus>
	                            <OrderTimestamp>2024-09-02T22:40:23.000+0000</OrderTimestamp>
	                            <SendPaperInvoice>false</SendPaperInvoice>
	                            <TotalAmountWithoutTax>1010.14</TotalAmountWithoutTax>
	                            <ReturnReason>Delisted / Planogram Change</ReturnReason>
	                            <ReturnReasonCode>056</ReturnReasonCode>
	                            <TotalGrossAmount>1010.14</TotalGrossAmount>
	                            <PMISAPCustomerNumber>70039907</PMISAPCustomerNumber>
	                            <PMISAPSoldToNumber>10135043</PMISAPSoldToNumber>
	                            <AWSQueueIdentifier>50146</AWSQueueIdentifier>
	                            <BillToAddress>152 QUEEN ST 4807 AYR Australia AU</BillToAddress>
	                            <ShipToAddress>152 QUEEN ST 4807 AYR Australia AU</ShipToAddress>
	                            <BillToAddressStructure>
	                              <BillingStreet>152 QUEEN ST</BillingStreet>
	                              <BillingPostalCode>4807</BillingPostalCode>
	                              <BillingCity>AYR</BillingCity>
	                              <BillingStateCode>QLD</BillingStateCode>
	                              <BillingCountry>Australia</BillingCountry>
	                              <BillingCountryCode>AU</BillingCountryCode>
	                            </BillToAddressStructure>
	                            <ShipToAddressStructure>
	                              <ShippingStreet>152 QUEEN ST</ShippingStreet>
	                              <ShippingPostalCode>4807</ShippingPostalCode>
	                              <ShippingCity>AYR</ShippingCity>
	                              <ShippingStateCode>QLD</ShippingStateCode>
	                              <ShippingCountry>Australia</ShippingCountry>
	                              <ShippingCountryCode>AU</ShippingCountryCode>
	                            </ShipToAddressStructure>
	                            <AccountName>CIGNALL BURDEKIN</AccountName>
	                            <AccountNumber>AU10135043</AccountNumber>
	                            <NumberOfShippingBoxes>1</NumberOfShippingBoxes>
	                            <OrderItems>
	                              <OrderItem>
	                                <SKU>50146_9310704911320</SKU>
	                                <SKUName>Craftsman RYO 25g Brunswick Blend - Zip Lock</SKUName>
	                                <POMCode>9310704911320</POMCode>
	                                <DeliveryItemNumber>216809361</DeliveryItemNumber>
	                                <Quantity>11</Quantity>
	                                <UnitOfMeasure>Pack</UnitOfMeasure>
	                                <SubAmount>476.07</SubAmount>
	                              </OrderItem>
	                              <OrderItem>
	                                <SKU>50146_93450423</SKU>
	                                <SKUName>Marlboro 25's Red + Firm Filter</SKUName>
	                                <POMCode>93450423</POMCode>
	                                <DeliveryItemNumber>216809362</DeliveryItemNumber>
	                                <Quantity>9</Quantity>
	                                <UnitOfMeasure>Pack</UnitOfMeasure>
	                                <SubAmount>389.90</SubAmount>
	                              </OrderItem>
	                              <OrderItem>
	                                <SKU>50146_93450430</SKU>
	                                <SKUName>Marlboro 25's Gold + Firm Filter</SKUName>
	                                <POMCode>93450430</POMCode>
	                                <DeliveryItemNumber>216809363</DeliveryItemNumber>
	                                <Quantity>2</Quantity>
	                                <UnitOfMeasure>Pack</UnitOfMeasure>
	                                <SubAmount>86.65</SubAmount>
	                              </OrderItem>
	                              <OrderItem>
	                                <SKU>50146_9310704918206</SKU>
	                                <SKUName>Marlboro Crafted 20's Full Flavour</SKUName>
	                                <POMCode>9310704918206</POMCode>
	                                <DeliveryItemNumber>216809364</DeliveryItemNumber>
	                                <Quantity>1</Quantity>
	                                <UnitOfMeasure>Pack</UnitOfMeasure>
	                                <SubAmount>28.76</SubAmount>
	                              </OrderItem>
	                              <OrderItem>
	                                <SKU>50146_9310704918220</SKU>
	                                <SKUName>Marlboro Crafted 20's Smooth Flavour</SKUName>
	                                <POMCode>9310704918220</POMCode>
	                                <DeliveryItemNumber>216809365</DeliveryItemNumber>
	                                <Quantity>1</Quantity>
	                                <UnitOfMeasure>Pack</UnitOfMeasure>
	                                <SubAmount>28.76</SubAmount>
	                              </OrderItem>
	                            </OrderItems>
	                            <RecordType>Return Order</RecordType>
	                            <extOrderId>28301515</extOrderId>
	                          </Order></Body>
	                              </message>
	                            </messages>
	                          </response-data>
	                          """;

	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( AzureRemoteService.Azure.SLOT.ALPHA_TRW, async client =>
		                                                               {
			                                                               await client.RequestPml_RecreateXrefFromXml( XML );
		                                                               } );
	}
}