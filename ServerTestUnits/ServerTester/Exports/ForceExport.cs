﻿using ServerTester._Customers.Pml;

namespace ServerTester.Exports;

[TestClass]
public class ForceExport
{
	[TestMethod]
	public async Task Execute()
	{
		const string PREFIX  = "IMP00";
		const string TRIP_ID = "31391787";

		await PmlLive.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                            {
			                                            await client.RequestPushTripToExport( $"{PREFIX}{TRIP_ID}" );
		                                            } );
	}
}

[TestClass]
public class ForceExportMany
{
	private const string PREFIX = "IMP00";

	private readonly string[] TripIds =
	[
		"31154221",
		"31157207",
		"31184778"
	];

	[TestMethod]
	public async Task Execute()
	{
		await PmlLive.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                            {
			                                            foreach( var TripId in TripIds )
				                                            await client.RequestPushTripToExport( $"{PREFIX}{TripId}" );
		                                            } );
	}
}