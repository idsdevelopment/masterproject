﻿using System.Diagnostics;
using ServerTester._Customers.PhoenixTest;
using File = System.IO.File;

namespace ServerTester.Exports.Sage;

[TestClass]
public class Sage300
{
	[TestMethod]
	public async Task Execute()
	{
		await PhoenixTestLogin.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                                     {
			                                                     var Csv = await client.RequestExportCsv( new ExportArgs
			                                                                                              {
				                                                                                              ExportName = "Sage300",
				                                                                                              StringArg1 = "1" // Starting Invoice Number
			                                                                                              } );

			                                                     if( Csv.IsNotNullOrWhiteSpace() )
			                                                     {
				                                                     var FileName = $"{Path.GetTempFileName()}.csv";

				                                                     File.WriteAllText( FileName, Csv );
				                                                     Process.Start( FileName )?.WaitForExit();

				                                                     try
				                                                     {
					                                                     File.Delete( FileName );
				                                                     }
				                                                     catch( Exception Exception )
				                                                     {
					                                                     Console.WriteLine( Exception );
				                                                     }
			                                                     }
			                                                     else
				                                                     Assert.Fail( "Failed to export" );
		                                                     } );
	}
}