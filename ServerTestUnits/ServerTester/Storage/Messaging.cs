﻿using ServerTester.Ids1LoopBack;

// ReSharper disable AccessToModifiedClosure

namespace ServerTester.Storage;

[TestClass]
public class Messaging
{
	private const string BOARD = "TestBoard";

	[TestMethod]
	public async Task Execute()
	{
		await Ids1LoopBack.AzureClient.Client( async client =>
		                                       {
			                                       bool HaveTrip;
			                                       var  Trips = new Dictionary<STATUS, HashSet<string>>();

			                                       void AddTrip( TripUpdate trip )
			                                       {
				                                       var              Status = trip.Status1;
				                                       HashSet<string>? TripIds;

				                                       lock( Trips )
				                                       {
					                                       if( !Trips.TryGetValue( Status, out TripIds ) )
						                                       Trips.Add( Status, TripIds = [] );
				                                       }

				                                       lock( TripIds )
					                                       TripIds.Add( trip.TripId );
			                                       }

			                                       await Tasks.RunVoid( () =>
			                                                            {
				                                                            void OnTripMessage( List<TripUpdateMessage> tripUpdates )
				                                                            {
					                                                            if( tripUpdates.Count == 0 )
						                                                            Assert.Fail( "No Trips in Message" );

					                                                            var Status = STATUS.DELETED;

					                                                            foreach( var Tu in tripUpdates )
					                                                            {
						                                                            var TripIdList = Tu.TripIdList;

						                                                            if( TripIdList.Count == 0 )
							                                                            Assert.Fail( "No TripsIds in Message" );

						                                                            Status = Tu.Status;

						                                                            lock( Trips )
						                                                            {
							                                                            if( !Trips.TryGetValue( Status, out var TripIdSet ) )
								                                                            Assert.Fail( $"Unexpected Status: {Status.AsString()}, ({string.Join( ", ", TripIdList )})" );

							                                                            foreach( var Id in TripIdList )
							                                                            {
								                                                            if( !TripIdSet.Contains( Id ) )
									                                                            Assert.Fail( $"Couldn't find matching trip: {Id}, {Tu.Status1.AsString()}" );
								                                                            else
								                                                            {
									                                                            TripIdSet.Remove( Id );

									                                                            if( TripIdSet.Count == 0 )
										                                                            Trips.Remove( Status );
								                                                            }
							                                                            }
						                                                            }
					                                                            }

					                                                            // Give last Ack time before shutting down
					                                                            if( Status == STATUS.DELETED )
					                                                            {
						                                                            Tasks.RunVoid( 1_000, () =>
						                                                                                  {
							                                                                                  HaveTrip = true;
						                                                                                  } );
					                                                            }
					                                                            else
						                                                            HaveTrip = true;
				                                                            }

				                                                            client.ListenTripUpdate( Protocol.Data.Messaging.DISPATCH_BOARD, client.UserName, "1", OnTripMessage );
			                                                            } );

			                                       var (Ok, Companies) = await client.CheckCoreAccounts();

			                                       if( !Ok )
				                                       Assert.Fail( "Cannot find companies" );

			                                       var Time = false;

			                                       void Start()
			                                       {
				                                       Time = false;

				                                       Tasks.RunVoid( 2 * 60 * 60 * 1000, () =>
				                                                                          {
					                                                                          Time = true;
				                                                                          } );
			                                       }

			                                       TripUpdate Trip = null!;

			                                       async Task NewTrip()
			                                       {
				                                       ( Ok, Trip ) = await client.CreateTrip( Companies, BOARD );

				                                       if( !Ok )
					                                       Assert.Fail( "Cannot create new trip." );
				                                       else
					                                       AddTrip( Trip );
			                                       }

			                                       void WaitForTrip()
			                                       {
				                                       HaveTrip = false;
				                                       var I = 60 * 60 * 1000; // On Minute in Ms

				                                       do
					                                       Thread.Sleep( 100 );
				                                       while( !HaveTrip && ( ( I -= 100 ) > 0 ) );

				                                       if( I <= 0 )
					                                       Assert.Fail( $"Lost Trip. TripId: {Trip.TripId}" );
			                                       }

			                                       async Task UpdateTripNoWait( STATUS status )
			                                       {
				                                       Trip.Status1 = status;
				                                       AddTrip( Trip );
				                                       await client.RequestAddUpdateTrip( Trip );
			                                       }

			                                       async Task UpdateTrip( STATUS status )
			                                       {
				                                       await UpdateTripNoWait( status );
				                                       WaitForTrip();
			                                       }

			                                       for( Start(); !Time; )
			                                       {
				                                       await NewTrip();

				                                       if( Ok )
				                                       {
					                                       WaitForTrip();

					                                       await UpdateTrip( STATUS.DISPATCHED );
					                                       await UpdateTrip( STATUS.PICKED_UP );
					                                       await UpdateTrip( STATUS.DELIVERED );
					                                       await UpdateTrip( STATUS.DELETED );
				                                       }
			                                       }

			                                       async Task UpdateTrip250( STATUS status )
			                                       {
				                                       Thread.Sleep( 250 );
				                                       await UpdateTripNoWait( status );
			                                       }

			                                       // Blast Test
			                                       for( Start(); !Time; )
			                                       {
				                                       await NewTrip();

				                                       await UpdateTrip250( STATUS.DISPATCHED );
				                                       await UpdateTrip250( STATUS.PICKED_UP );
				                                       await UpdateTrip250( STATUS.DELIVERED );
				                                       await UpdateTrip250( STATUS.DELETED );
			                                       }

			                                       while( true )
			                                       {
				                                       lock( Trips )
				                                       {
					                                       if( Trips.Count == 0 )
						                                       break;
				                                       }
				                                       Thread.Sleep( 1_000 );
			                                       }

			                                       client.CancelTripUpdateListen();
		                                       } );
	}
}