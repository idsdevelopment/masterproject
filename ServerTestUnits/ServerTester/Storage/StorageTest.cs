﻿namespace ServerTester.Storage;

[TestClass]
public class Storage
{
	private const string TEST_BLOB          = "TestBlob.txt",
	                     TEST_APPEND_BLOB   = "TestAppendBlob.txt",
	                     TEST_METADATA      = "TestMetaData",
	                     TEST_METADATA_DATA = "TestMeta Data",
	                     TEST_DATA          = "Test Data\r\n",
	                     QUEUE_DATA         = "Queue Data\r\n",
	                     QUEUE_DATA1        = "Queue Data 1\r\n",
	                     QUEUE_DATA2        = "Queue Data 2\r\n",
	                     QUEUE_NAME         = "TestQueue",
	                     TABLE_NAME         = "TestTable",
	                     TABLE_PARTITION    = "TestPartition",
	                     TABLE_ROW          = "TestRow";


	[TestMethod]
	public async Task Execute()
	{
		await Ids1LoopBack.AzureClient.Client( async client =>
		                                       {
			                                       var TestData = new StorageTest
			                                                      {
				                                                      Storage = StorageTest.STORAGE.BLOB,
				                                                      Name    = TEST_BLOB
			                                                      };

			                                       StorageTest? Result = null;

			                                       async Task UpdateStorage()
			                                       {
				                                       Result = await client.RequestTestStorage( TestData );
				                                       StorageError();
			                                       }

			                                       string StorageType()
			                                       {
				                                       return TestData!.Storage switch
				                                              {
					                                              StorageTest.STORAGE.BLOB        => "Blob",
					                                              StorageTest.STORAGE.APPEND_BLOB => "Append Blob",
					                                              StorageTest.STORAGE.QUEUE       => "Queue",
					                                              StorageTest.STORAGE.TABLE       => "Table",
					                                              _                               => "?????"
				                                              };
			                                       }

			                                       void StorageError()
			                                       {
				                                       if( !Result.Ok )
				                                       {
					                                       var Action = Result.Action switch
					                                                    {
						                                                    StorageTest.ACTION.PUT          => "Putting to",
						                                                    StorageTest.ACTION.PUT_METADATA => "Putting Metadata to",
						                                                    StorageTest.ACTION.GET          => "Getting from",
						                                                    StorageTest.ACTION.GET_METADATA => "Getting Metadata from",
						                                                    StorageTest.ACTION.DELETE       => "Deleting from",
						                                                    StorageTest.ACTION.LIST         => "Listing",
						                                                    _                               => "?????"
					                                                    };

					                                       Assert.Fail( $"Error {Action} {StorageType()} Storage:\r\n{Result.ErrorMessage}" );
				                                       }
			                                       }

			                                       async Task TestMetaData()
			                                       {
				                                       TestData.Action = StorageTest.ACTION.PUT_METADATA;

				                                       TestData.MetaData = new Dictionary<string, string>
				                                                           {
					                                                           { TEST_METADATA, TEST_METADATA_DATA }
				                                                           };
				                                       await UpdateStorage();

				                                       TestData.MetaData = new Dictionary<string, string>();
				                                       TestData.Action   = StorageTest.ACTION.GET_METADATA;
				                                       await UpdateStorage();

				                                       if( Result is not { MetaData: { Count: > 0 } M } || !M.TryGetValue( TEST_METADATA, out var Md ) || ( Md != TEST_METADATA_DATA ) )
					                                       Assert.Fail( $"{StorageType()} metadata does not compare." );
			                                       }

			                                       async Task BlobStorageTest()
			                                       {
				                                       TestData.Action = StorageTest.ACTION.PUT;

				                                       TestData.Data = [TEST_DATA];

				                                       await UpdateStorage();

				                                       TestData.Action = StorageTest.ACTION.GET;
				                                       TestData.Data   = [];
				                                       await UpdateStorage();

				                                       switch( Result )
				                                       {
				                                       case { Data: { Count: > 0 } } R when R.Data[ 0 ] == TEST_DATA:
					                                       break;

				                                       default:
					                                       Assert.Fail( $"{StorageType()} data does not compare." );
					                                       break;
				                                       }

				                                       await TestMetaData();

				                                       TestData.Action = StorageTest.ACTION.DELETE_BLOB;
				                                       await UpdateStorage();
			                                       }

			                                       await BlobStorageTest();

			                                       TestData.Storage = StorageTest.STORAGE.APPEND_BLOB;
			                                       TestData.Name    = TEST_APPEND_BLOB;
			                                       await BlobStorageTest();

			                                       TestData.Action = StorageTest.ACTION.DELETE_CONTAINER;
			                                       await UpdateStorage();

			                                       async Task QueueTest()
			                                       {
				                                       TestData.Storage = StorageTest.STORAGE.QUEUE;
				                                       TestData.Action  = StorageTest.ACTION.PUT;
				                                       TestData.Name    = QUEUE_NAME;

				                                       TestData.Data =
				                                       [
					                                       QUEUE_DATA,
					                                       QUEUE_DATA1,
					                                       QUEUE_DATA2
				                                       ];
				                                       await UpdateStorage();

				                                       TestData.Action = StorageTest.ACTION.GET;
				                                       TestData.Data   = [];

				                                       for( var I = 0; I < 3; I++ )
				                                       {
					                                       await UpdateStorage();

					                                       var Cmp = I switch
					                                                 {
						                                                 0 => QUEUE_DATA,
						                                                 1 => QUEUE_DATA1,
						                                                 2 => QUEUE_DATA2,
						                                                 _ => ""
					                                                 };

					                                       if( ( Result!.Data.Count <= 0 ) || ( Result.Data[ 0 ] != Cmp ) )
						                                       Assert.Fail( "Queue does not compare." );
				                                       }

				                                       await TestMetaData();

				                                       TestData.Action = StorageTest.ACTION.DELETE;
				                                       await UpdateStorage();
			                                       }

			                                       await QueueTest();

			                                       async Task TableTest()
			                                       {
				                                       TestData.Storage = StorageTest.STORAGE.TABLE;
				                                       TestData.Action  = StorageTest.ACTION.PUT;
				                                       TestData.Name    = TABLE_NAME;

				                                       TestData.Data =
				                                       [
					                                       TABLE_PARTITION,
					                                       TABLE_ROW,
					                                       TEST_DATA
				                                       ];
				                                       await UpdateStorage();

				                                       TestData.Data =
				                                       [
					                                       TABLE_PARTITION,
					                                       TABLE_ROW
				                                       ];

				                                       TestData.Action = StorageTest.ACTION.GET;
				                                       await UpdateStorage();

				                                       if( ( Result!.Data.Count <= 0 ) || ( Result.Data[ 0 ] != TEST_DATA ) )
					                                       Assert.Fail( "Table does not compare." );

				                                       TestData.Action = StorageTest.ACTION.DELETE;
				                                       await UpdateStorage();
			                                       }

			                                       await TableTest();

			                                       TestData.Storage      = StorageTest.STORAGE.SYSTEM_LOG;
			                                       TestData.Name         = Globals.PROGRAM;
			                                       TestData.ErrorMessage = "System Log Test";
			                                       await UpdateStorage();
		                                       } );
	}
}