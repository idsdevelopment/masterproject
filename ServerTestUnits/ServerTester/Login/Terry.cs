﻿namespace ServerTester.Login;

public static class TestLogin
{
	public const string VERSION = "Server Tester V1.00";

	public static string AccountId = "Terry",
	                     UserName  = "000001",
	                     Password  = "123456";

	private static bool LoggedIn;

	private static Azure.InternalClient? _Client;

	public static async Task Client( Azure.SLOT slot, string accountId, string userName, string password, Func<Azure.InternalClient, Task> onLoginOk, Action? onLoginFail = null )
	{
		Azure.Slot = slot;
		await Client( accountId, userName, password, onLoginOk, onLoginFail );
	}

	public static async Task Client( string accountId, string userName, string password, Func<Azure.InternalClient, Task>? onLoginOk, Action? onLoginFail = null )
	{
		void CannotLogin()
		{
			Assert.Fail( $"Cannot login to {accountId}" );
		}

		try
		{
			if( !LoggedIn )
			{
				var (Status, InternalClient) = await Azure.LogIn( VERSION, accountId, userName, password );

				if( Status != Azure.AZURE_CONNECTION_STATUS.OK )
					Assert.Fail( "Failed to login" );
				else
				{
					_Client  = InternalClient;
					LoggedIn = true;
				}
			}
		}
		catch
		{
			CannotLogin();
			return;
		}

		if( LoggedIn )
		{
			if( onLoginOk is not null )
				await onLoginOk.Invoke( _Client! );
		}
		else
		{
			if( onLoginFail is not null )
				onLoginFail();
			else
				CannotLogin();
		}
	}

	public static async Task Client( Func<Azure.InternalClient, Task> onLoginOk, Action? onLoginFail = null ) => await Client( AccountId, UserName, Password, onLoginOk, onLoginFail );
	public static async Task Client( Azure.SLOT slot, Func<Azure.InternalClient, Task> onLoginOk, Action? onLoginFail = null ) => await Client( slot, AccountId, UserName, Password, onLoginOk, onLoginFail );
}