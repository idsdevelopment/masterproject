﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.Login;

[TestClass]
public class TwoCodeLogin
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( Azure.SLOT.ALPHA_TRW, async _ =>
		                                                {
			                                                var Result = await Azure.LogIn( Globals.PROGRAM, "UserAA", "usera!" );

			                                                if( Result.Status == Azure.AZURE_CONNECTION_STATUS.ERROR )
				                                                Assert.Fail( "Cannot login" );
			                                                else
			                                                {
				                                                var Prefs = await Azure.Client.RequestPreferences();

				                                                if( Prefs is null )
					                                                Assert.Fail( "Cannot get preferences" );
			                                                }
		                                                } );
	}
}