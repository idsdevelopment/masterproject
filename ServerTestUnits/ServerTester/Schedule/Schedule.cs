﻿namespace ServerTester.Schedules;

[TestClass]
public class Schedule
{
	[TestMethod]
	public async Task Execute()
	{
		await TestLogin.Client( async client =>
		                        {
			                        await client.RequestStartSchedule();
		                        } );
	}
}