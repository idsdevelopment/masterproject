﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.Exceptions;

[TestClass]
public class SystemLog
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( Azure.SLOT.ALPHA_TRW, async client =>
		                                                {
			                                                await client.RequestThrowSystemLogException( "Server Tester" );
		                                                } );
	}
}