﻿// ReSharper disable InconsistentNaming

namespace SOAPServer;

public partial class SOAP
{
	[TestMethod]
	public void Echo()
	{
		const string MESSAGE = "Test Message";

		try
		{
			var Result = new SoapClient().Echo( "ServerTester-yyyyyy-zzzzz", MESSAGE );

			if( Result != MESSAGE )
				Assert.Fail( $"Expected: {MESSAGE},  Received:{Result}" );
		}
		catch( Exception Exception )
		{
			Assert.Fail( Exception.Message );
		}
	}
}