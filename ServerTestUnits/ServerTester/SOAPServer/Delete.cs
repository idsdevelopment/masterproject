﻿using ServerTester.Ids1LoopBack;
using ServerTester.SOAPService;

namespace SOAPServer;

public static class Extensions
{
	internal static readonly DateTime Epoch = DateTimeExtensions.Epoch;

	public static remoteTripDetailed ToRemoteTripDetailed( this TripUpdate t, STATUS status )
	{
		var Now = DateTimeOffset.Now.LocalTimeToServerTime();

		var DeliveredTime = t.VerifiedTime.ServerTime();

		var PickupTime          = t.PickupTime.ServerTime();
		var PickupTimeSpecified = t.PickupTime.Specified();

		const string RESELLER_ID = ServerTester.Ids1LoopBack.Globals.CARRIER_ID;

		if( !int.TryParse( t.Board.Trim(), out var Board ) )
			Board = 0;

		return new remoteTripDetailed
		       {
			       resellerId = RESELLER_ID,
			       accountId  = t.AccountId,

			       status      = (int)status,
			       tripId      = t.TripId,
			       lastUpdated = DateTimeExtensions.UnixTicksMilliSeconds(),

			       pieces = (int)t.Pieces,
			       weight = (float)t.Weight,
			       height = 0,
			       length = 0,
			       width  = 0,

			       driver = t.Driver,
			       POD    = true,

			       billingNotes = t.BillingNotes,
			       board        = Board,

			       callTakerId = t.CallTakerId,
			       callTime    = t.CallTime.ServerTime(),
			       caller      = t.CallerName,
			       callerEmail = t.CallerEmail,
			       callerPhone = t.CallerPhone,

			       carChargeAmount       = 0,
			       clientReference       = t.Reference,
			       currentZone           = t.CurrentZone,
			       dangerousGoods        = t.DangerousGoods,
			       declaredValueAmount   = 0,
			       deadlineTime          = t.DueTime.ServerTime(),
			       deadlineTimeSpecified = t.DueTime.Specified(),

			       deliveryResellerId = RESELLER_ID,
			       deliveryAccountId  = t.AccountId,
			       deliveryAddressId  = "",
			       deliveryCompany    = t.DeliveryCompanyName,
			       deliveryStreet     = t.DeliveryAddressAddressLine1,
			       deliverySuite      = t.DeliveryAddressSuite,
			       deliveryCity       = t.DeliveryAddressCity,
			       deliveryProvince   = t.DeliveryAddressRegion,
			       deliveryPc         = t.DeliveryAddressPostalCode,
			       deliveryCountry    = t.DeliveryAddressCountry,

			       deliveredTime          = DeliveredTime,
			       deliveredTimeSpecified = true,

			       deliveryArriveTime          = DeliveredTime,
			       deliveryArriveTimeSpecified = true,

			       deliveryAmount      = 0,
			       deliveryContact     = t.DeliveryContact,
			       deliveryDescription = "",
			       deliveryEmail       = t.DeliveryAddressEmailAddress,
			       deliveryNotes       = t.DeliveryNotes,
			       deliveryPhone       = t.DeliveryAddressPhone,
			       deliveryZone        = t.DeliveryZone,

			       disbursementAmount                = 0,
			       discountAmount                    = 0,
			       disputed                          = false,
			       dontFinalizeDate                  = Epoch,
			       dontFinalizeDateSpecified         = false,
			       dontFinalizeFlag                  = false,
			       dontFinalizeFlagSpecified         = false,
			       driverAssignTime                  = Epoch,
			       driverAssignTimeSpecified         = false,
			       driverPaidCommissionDate          = Epoch,
			       driverPaidCommissionDateSpecified = false,
			       insuranceAmount                   = 0,
			       internalId                        = null,
			       invoiceId                         = 0,
			       invoiced                          = false,
			       isCashTrip                        = false,
			       isCashTripReconciled              = false,
			       isDriverPaid                      = false,
			       mailDrop                          = false,
			       miscAmount                        = 0,
			       modified                          = true,
			       modifiedTripDate                  = Now,
			       modifiedTripDateSpecified         = true,
			       modifiedTripFlag                  = true,
			       modifiedTripFlagSpecified         = true,
			       noGoodsAmount                     = 0,
			       packageType                       = t.PackageType,
			       pallets                           = "",

			       pickupResellerId = RESELLER_ID,
			       pickupAccountId  = t.PickupAccountId,
			       pickupAddressId  = "",
			       pickupCompany    = t.PickupCompanyName,
			       pickupSuite      = t.PickupAddressSuite,
			       pickupStreet     = t.PickupAddressAddressLine1,
			       pickupCity       = t.PickupAddressCity,
			       pickupProvince   = t.PickupAddressRegion,
			       pickupPc         = t.PickupAddressPostalCode,
			       pickupCountry    = t.PickupAddressCountry,

			       pickupTime                = PickupTime,
			       pickupArriveTime          = PickupTime,
			       pickupTimeSpecified       = PickupTimeSpecified,
			       pickupArriveTimeSpecified = PickupTimeSpecified,

			       pickupContact                    = t.PickupContact,
			       pickupDescription                = "",
			       pickupEmail                      = t.PickupAddressEmailAddress,
			       pickupNotes                      = t.PickupNotes,
			       pickupPhone                      = t.PickupAddressPhone,
			       pickupZone                       = t.PickupZone,
			       podName                          = t.Status1 <= STATUS.PICKED_UP ? t.POP : t.POD,
			       priorityInvoiceId                = 0,
			       priorityInvoiceIdSpecified       = false,
			       priorityStatus                   = 0,
			       priorityStatusSpecified          = false,
			       processedByIdsRouteDate          = Epoch,
			       processedByIdsRouteDateSpecified = false,
			       readyTime                        = t.ReadyTime.ServerTime(),
			       readyTimeSpecified               = t.ReadyTime.Specified(),
			       readyTimeString                  = "",
			       readyToInvoiceFlag               = false,
			       readyToInvoiceFlagSpecified      = false,
			       receivedByIdsRouteDate           = Epoch,
			       receivedByIdsRouteDateSpecified  = false,
			       redirectAmount                   = 0,
			       returnTrip                       = false,
			       serviceLevel                     = t.ServiceLevel,
			       sigFilename                      = "",
			       skipUpdateCommonAddress          = true,
			       sortOrder                        = "",
			       stopGroupNumber                  = "",
			       totalAmount                      = 0,
			       totalFixedAmount                 = 0,
			       totalPayrollAmount               = 0,
			       totalTaxAmount                   = 0,
			       waitTimeAmount                   = 0,
			       waitTimeMins                     = 0,
			       weightAmount                     = 0,
			       weightOrig                       = 0,

			       carCharge = t.ReceivedByDevice,
			       redirect  = t.ReadByDriver
		       };
	}
}

public partial class SOAP
{
	[TestClass]
	public class Delete //  !!!!!!!!! Not Finished
	{
		private const string AUTH_TOKEN = "loopbacktest-Admin-admin";

		[TestMethod]
		public async Task Execute()
		{
			await AzureClient.Client( async azureClient =>
			                          {
				                          var (Ok, Companies) = await azureClient.CheckCoreAccounts();

				                          if( Ok )
				                          {
					                          ( Ok, var TripUpdate ) = await azureClient.CreateTrip( Companies );

					                          if( Ok )
					                          {
						                          // ReSharper disable once InconsistentNaming
						                          var SOAPClient = new SoapClient();

						                          await SOAPClient.updateTripDetailedQuickAsync( AUTH_TOKEN, TripUpdate.ToRemoteTripDetailed( STATUS.DELETED ), false );
					                          }
				                          }
				                          else
					                          Assert.Fail( "Failed to get default companies" );
			                          } );
		}
	}
}