﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.SoapLogs;

[TestClass]
public class Logs
{
	[TestMethod]
	public async Task WriteLog()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var Message = new WebLog
			                                        {
				                                        CarrierId = TestLogin.AccountId,
				                                        Program   = TestLogin.VERSION,
				                                        Message   = "Starting SOAP Log"
			                                        };

			                          await client.RequestLogSOAP( Message );

			                          Thread.Sleep( 1_000 );

			                          Message.Message = "Ending SOAP Log";

			                          await client.RequestLogSOAP( Message );
		                          } );
	}

	[TestMethod]
	public async Task GetCarriers()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var Carriers = await client.RequestGetSOAPCarrierList();

			                          foreach( var Carrier in Carriers )
				                          Console.WriteLine( Carrier );
		                          } );
	}

	[TestMethod]
	public async Task GetCarrierLogs()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var Logs = await client.RequestGetSOAPLogList( TestLogin.AccountId );

			                          foreach( var Log in Logs )
				                          Console.WriteLine( Log );
		                          } );
	}

	[TestMethod]
	public async Task GetLogs()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var Logs = await client.RequestGetSOAPLogs( TestLogin.AccountId, TestLogin.VERSION );

			                          foreach( var Log in Logs )
				                          Console.WriteLine( Log );
		                          } );
	}

	[TestMethod]
	public async Task GetLogContents()
	{
		await AzureClient.Client( async client =>
		                          {
			                          var Logs = await client.RequestGetSOAPLogs( TestLogin.AccountId, TestLogin.VERSION );
			                          var C    = Logs.Count;

			                          if( C > 0 )
			                          {
				                          var Contents = await client.RequestGetSOAPLogContents( TestLogin.AccountId, TestLogin.VERSION, Logs[ C - 1 ] );
				                          Console.WriteLine( Contents );
			                          }
		                          } );
	}
}