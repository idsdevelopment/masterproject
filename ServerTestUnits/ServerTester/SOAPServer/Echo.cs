﻿// ReSharper disable InconsistentNaming

using SOAPServer;

namespace ServerTester.SOAPServer;

public class SOAP
{
	[TestMethod]
	public void ThrowServerError()
	{
		const string MESSAGE = "Test Error";

		try
		{
			new SoapClient().ThrowSoapErrorToAzure( MESSAGE );
		}
		catch
		{
			return;
		}
		Assert.Fail( "Should have had exception" );
	}
}