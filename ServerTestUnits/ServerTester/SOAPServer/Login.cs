﻿namespace SOAPServer;

public partial class SOAP
{
	[TestMethod]
	public void Login()
	{
		try
		{
			var Client = new SoapClient();

			for( var I = 0; I < 100; I++ )
			{
				var Result = Client.LoginTest( "PhoenixTest-Ids1WebService-8y7b25b798538793b98" );

				if( string.Compare( Result, "OK", StringComparison.OrdinalIgnoreCase ) != 0 )
					Assert.Fail( "Failed To Login" );
			}
		}
		catch( Exception Exception )
		{
			Assert.Fail( Exception.Message );
		}
	}
}