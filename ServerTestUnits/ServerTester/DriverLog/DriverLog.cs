﻿using ServerTester.Ids1LoopBack;

namespace ServerTester.DriverLogs;

[TestClass]
public class Log
{
	[TestMethod]
	public async Task Execute()
	{
		await AzureClient.Client( async azureClient =>
		                          {
			                          var (Ok, Companies) = await azureClient.CheckCoreAccounts();

			                          if( Ok )
			                          {
				                          ( Ok, var TripUpdate ) = await azureClient.CreateTrip( Companies );

				                          if( Ok )
				                          {
					                          await azureClient.RequestUpdateTripFromDevice( new DeviceTripUpdate( Globals.PROGRAM )
					                                                                         {
						                                                                         TripId     = TripUpdate.TripId,
						                                                                         Pieces     = 10,
						                                                                         PopPod     = "--Pop Pod--",
						                                                                         Reference  = "--Reference--",
						                                                                         Signature  = new Signature(),
						                                                                         Status     = STATUS.VERIFIED,
						                                                                         UpdateTime = DateTimeOffset.Now
					                                                                         } );
				                          }
			                          }
			                          else
				                          Assert.Fail( "Failed to get default companies" );
		                          } );
	}
}