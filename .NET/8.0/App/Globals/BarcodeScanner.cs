﻿using System.Collections.Specialized;
using Events;

namespace App;

public partial class Globals
{
	public class BarcodeScanner
	{
		public static bool Initialised;

		public static Components.BarcodeScanner.BarcodeScanner Instance { get; set; } = null!;

	#region IsVisible
		public static bool IsVisible
		{
			get => Instance.IsVisible;
			set => Instance.IsVisible = value;
		}
	#endregion

	#region IsScanning
		public static bool IsScanning
		{
			get => Instance.IsScanning;
			set => Instance.IsScanning = value;
		}
	#endregion

	#region Placeholder
		public static string Placeholder
		{
			get => Instance.Placeholder;
			set => Instance.Placeholder = value;
		}
	#endregion

	#region Events
		public static readonly WeakEvent<string> OnBarcode = new();
	#endregion
	}
}