﻿namespace App;

public partial class Globals
{
	public class Login
	{
		public static string CarrierId  = "", UserName = "";
		public static bool   TestServer = false;
		public static string Driver => UserName;
	}
}