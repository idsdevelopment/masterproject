﻿namespace App;

public partial class Globals
{
	public class CurrentVersion
	{
		public const string PROGRAM         = "Web App",
		                    VERSION         = "1.00",
		                    PROGRAM_VERSION = PROGRAM + " " + VERSION;
	}
}