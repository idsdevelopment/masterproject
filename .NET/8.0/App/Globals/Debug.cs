﻿namespace App;

public partial class Globals
{
	public class Debug
	{
	#if DEBUG
		public static readonly bool IsDebug = true;
	#else
		public static readonly bool IsDebug = false;
	#endif
		public static bool IsTesting = true;
	}
}