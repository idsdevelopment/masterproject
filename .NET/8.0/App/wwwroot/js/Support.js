﻿//
// Viewport
//
function getViewportSize() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    const height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    return {
        width: width,
        height: height
    };
}

//
// Elements
//
function getElementSize(element) {
    if (!element) return null;
    return {
        width: element.width,
        height: element.height
    };
}

function setElementSize(element, width, height) {
    if (!element) return;
    element.width = width;
    element.height = height;
}

function getElementDisplaySize(element) {
    if (element) {
        const rect = element.getBoundingClientRect();
        return {
            width: rect.width,
            height: rect.height
        };
    }
    return null;
}

function getMousePosition(element, clientX, clientY) {
    if (!element) return null;
    const rect = element.getBoundingClientRect();
    const retval = {
        x: Math.round(clientX - rect.left),
        y: Math.round(clientY - rect.top)
    };
    return retval;
}

//
// Canvas
//
function checkCanvas(canvas) {
    return (canvas && (canvas instanceof HTMLCanvasElement));
}

function setCanvasSize(canvas) {
    if (checkCanvas(canvas)) {
        const ctx = canvas.getContext("2d");
        const dpr = window.devicePixelRatio || 1;
        const rect = canvas.getBoundingClientRect();
        canvas.width = rect.width * dpr;
        canvas.height = rect.height * dpr;
        ctx.scale(dpr, dpr);
    }
}

function resizeCanvasToDisplaySize(canvas) {
    if (checkCanvas(canvas)) {
        function resize() {
            setCanvasSize(canvas);
        }

        window.addEventListener('resize', resize);
        resize(); // Initial call to set the size
    }
}

function clearCanvas(canvas) {
    if (checkCanvas(canvas)) {
        const ctx = canvas.getContext("2d");
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
}

function setCanvasFillColor(canvas, color) {
    if (checkCanvas(canvas)) {
        const ctx = canvas.getContext("2d");
        ctx.fillStyle = color;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
}

function setPenProperties(canvas, width, color) {
    if (checkCanvas(canvas)) {
        const ctx = canvas.getContext("2d");
        ctx.lineWidth = width;
        ctx.strokeStyle = color;
    }
}

function moveToPoint(canvas, x, y) {
    if (checkCanvas(canvas)) {
        const ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(x, y);
    }
}

function lineToPoint(canvas, x, y) {
    if (checkCanvas(canvas)) {
        const ctx = canvas.getContext("2d");
        ctx.lineTo(x, y);
        ctx.stroke();
    }
}

//Storage
window.localStorageHelper = {
    save: function(key, value) {
        localStorage.setItem(key, value);
    },
    load: function(key) {
        return localStorage.getItem(key);
    }
};

// Geo Location
window.getGeolocation = function() {
    return new Promise((resolve, reject) => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    resolve({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    });
                },
                (error) => {
                    reject(error);
                }
            );
        } else {
            reject(new Error("Geolocation is not supported by this browser."));
        }
    });
};

// Input
window.filterAlphaAndSpaces = function(element) {
    if (element) {
        element.addEventListener("input",
            function(event) {
                const value = event.target.value;
                event.target.value = value.replace(/[^a-zA-Z\s]/g, "");
            });
    }
};