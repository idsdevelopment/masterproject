using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var Builder = WebAssemblyHostBuilder.CreateDefault( args );
Builder.RootComponents.Add<App.App>( "#app" );
Builder.RootComponents.Add<HeadOutlet>( "head::after" );

var Services = Builder.Services;
Services.AddScoped( sp => new HttpClient { BaseAddress = new Uri( Builder.HostEnvironment.BaseAddress ) } );

await Builder.Build().RunAsync();