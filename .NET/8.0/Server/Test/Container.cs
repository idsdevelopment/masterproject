﻿#nullable enable

using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
	[TestClass]
	public class Container
	{
		[TestMethod]
		public void CreateContainer()
		{
			var Blob = AppendBlob.Blob;
			var (Status, _) = Blob.CreateContainer();

			if( Status == HttpStatusCode.Created )
				Console.WriteLine( "Create Blob Container Passed." );
			else
				Assert.Fail( $"Create Blob Container Failed. Status: {Status}" );
		}


		[TestMethod]
		public void DeleteContainer()
		{
			var Blob = AppendBlob.Blob;
			var (Status, _) = Blob.DeleteContainer();

			if( Status == HttpStatusCode.OK )
				Console.WriteLine( "Delete Blob Container Passed." );
			else
				Assert.Fail( $"Delete Blob Container Failed. Status: {Status}" );
		}

		[TestMethod]
		public void ListContainers()
		{
			var Blob = AppendBlob.Blob;

			var (Status, Items, _) = Blob.ListContainers();

			if( Status == HttpStatusCode.OK )
			{
				foreach( var Item in Items )
					Console.WriteLine( Item );
			}
			else
				Assert.Fail( $"List Blob Containers Failed. Status: {Status}" );
		}
	}
}