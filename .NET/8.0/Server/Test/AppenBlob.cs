#nullable enable

using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;

namespace Test
{
	[TestClass]
	public class AppendBlob
	{
		public static BlobStorage Blob => new BlobStorage( "idsroute", "CMwLTPrxnUvpNJG3CJeT7ZryaaWWLBz1ikBLh7oyRgcCmuAZazxO5ZApBw5NO2IQJPQk1sFEBwe5UwjdZQoI/Q==" )
		                                  {
			                                  ContainerName = Constants.CONTAINER_NAME,
			                                  Name          = Constants.APPEND_BLOB_NAME,
			                                  BlobType      = BlobStorage.BLOB_TYPE.APPEND
		                                  };


		[TestMethod]
		public void Append()
		{
			var B      = Blob;
			var Result = B.PutBlob( "1" );

			if( Result.Status == HttpStatusCode.OK )
			{
				Result = B.PutBlob( "2345678901234567890" );

				if( Result.Status == HttpStatusCode.OK )
					Console.WriteLine( "Append Blob Passed." );
				else
					Assert.Fail( $"Second Put To Append Blob Failed. Status: {Result.Status}" );
			}
			else
				Assert.Fail( $"Initial Put to Append Blob failed. Status: {Result.Status}" );
		}
	}
}