﻿#nullable enable


namespace Test
{
	public class Constants
	{
		public const string CONTAINER_NAME   = "test-blobs",
		                    APPEND_BLOB_NAME = "Append.Log",
							BLOCK_BLOB_NAME = "Block.Log";
	}
}