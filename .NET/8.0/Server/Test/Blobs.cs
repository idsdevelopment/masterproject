﻿#nullable enable

using System;
using System.Linq;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;

namespace Test
{
	[TestClass]
	public class Blobs
	{
		[TestMethod]
		public void Exists()
		{
			var Blob = AppendBlob.Blob;
			var (Ok, BlobType) = Blob.Exists;
			Console.WriteLine( Ok ? $"Blob Exists and Type = {BlobType}" : "Blob Doesn't Exist" );
		}

		[TestMethod]
		public void GetBlobUnicode()
		{
			var Blob = AppendBlob.Blob;
			var (Status, Content, _, _) = Blob.GetBlobUnicode();

			if( Status == HttpStatusCode.OK )
				Console.WriteLine( $"Get Blob Content : {Content}" );
			else
				Assert.Fail( $"Get Blob Content Failed. Status: {Status}" );
		}

		[TestMethod]
		public void ListBlob()
		{
			var Blob = AppendBlob.Blob;
			var (Status, BlobNames, _, _) = Blob.ListBlobs();

			if( Status == HttpStatusCode.OK )
				Console.WriteLine( $"ListBlobs Found Blob Names: {string.Join( ", ", ( from B in BlobNames select $"{B.Name} {B.Type}" ).ToList() )}" );
			else
				Assert.Fail( $"ListBlobs Failed. Status: {Status}" );
		}
	}
}