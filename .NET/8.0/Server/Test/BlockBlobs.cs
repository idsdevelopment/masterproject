﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;

namespace Test
{
	[TestClass]
	public class BlockBlobs
	{
		public static BlobStorage Blob => new BlobStorage( "idsroute", "CMwLTPrxnUvpNJG3CJeT7ZryaaWWLBz1ikBLh7oyRgcCmuAZazxO5ZApBw5NO2IQJPQk1sFEBwe5UwjdZQoI/Q==" )
		                                  {
			                                  ContainerName = Constants.CONTAINER_NAME,
			                                  Name          = Constants.BLOCK_BLOB_NAME,
			                                  BlobType      = BlobStorage.BLOB_TYPE.BLOCK
		                                  };

		[TestMethod]
		public void PutBlock()
		{
			var (Status, _, _) = Blob.PutBlock( "123456", "Test Data" );

			if( Status == HttpStatusCode.OK )
				Console.WriteLine( "Put Blob Block Passed." );
			else
				Assert.Fail( $"Put Blob Block failed. Status: {Status}" );
		}
	}
}
