﻿// ReSharper disable MemberHidesStaticFromOuterClass

using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Xml;
using Ids1WebService.Logs;
using Timer = System.Timers.Timer;

// ReSharper disable ExplicitCallerInfoArgument

namespace Ids1WebService;

public class EndPoints
{
	public const string JBTEST2 = "http://jbtest2.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    OZ      = "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    REPORTS = "http://reports.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    CUSTOM  = "http://custom.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    DRIVERS = "http://drivers.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    USERS   = "http://users.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    EAST1   = "http://east1.internetdispatcher.org:8080/ids-beans/IDSWS";
}

public class MessageLoggingInspector : IClientMessageInspector, IDispatchMessageInspector
{
	public void AfterReceiveReply( ref Message reply, object correlationState )
	{
	#if DEBUG
		// Log the reply message body
		var Received = reply.ToString();

		Debug.WriteLine( $"""

		                  Received reply:
		                  {Received}

		                  """ );
	#endif
	}

	public object? BeforeSendRequest( ref Message request, IClientChannel channel )
	{
	#if DEBUG
		// Log the request message body
		Debug.WriteLine( $"""

		                  Sending request:
		                  {request}

		                  """ );
	#endif
		return null;
	}

	public object? AfterReceiveRequest( ref Message request, IClientChannel channel, InstanceContext instanceContext )
	{
	#if DEBUG
		// Log the request message body
		Debug.WriteLine( $"""

		                  Received request:
		                  {request}

		                  """ );
	#endif
		return null;
	}

	public void BeforeSendReply( ref Message reply, object correlationState )
	{
	#if DEBUG
		// Log the reply message body
		Console.WriteLine( $"""

		                    Sending reply:
		                    {reply}

		                    """ );
	#endif
	}
}

public class MessageLoggingBehavior : IEndpointBehavior
{
	public void AddBindingParameters( ServiceEndpoint endpoint, BindingParameterCollection bindingParameters )
	{
	}

	public void ApplyClientBehavior( ServiceEndpoint endpoint, ClientRuntime clientRuntime )
	{
	#if DEBUG
		clientRuntime.ClientMessageInspectors.Add( new MessageLoggingInspector() );
	#endif
	}

	public void ApplyDispatchBehavior( ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher )
	{
		// This method is not needed for client-side behaviors
	}

	public void Validate( ServiceEndpoint endpoint )
	{
	}
}

// ReSharper disable once InconsistentNaming
public partial class IDSClient : DSWSClient
{
	private const int _1MB                      = 1_048_576, // 1 MB
	                  MAX_PACKET_SIZE           = _1MB * 100,
	                  MAX_NAME_TABLE_CHAR_COUNT = _1MB,
	                  MAX_ARRAY_LENGTH          = _1MB;

	private const string ALL = "ALL";

	internal static readonly (string UserName, string Password) SOAP_LOGIN = ( "AzureWebService", "8y7b25b798538793b98" );

	public IDSClient( string authToken, string endPoint ) : base( new BasicHttpBinding( BasicHttpSecurityMode.None )
	                                                              {
		                                                              TextEncoding           = Encoding.Unicode, // UTF-16
		                                                              MaxReceivedMessageSize = MAX_PACKET_SIZE,
		                                                              ReaderQuotas = new XmlDictionaryReaderQuotas
		                                                                             {
			                                                                             MaxStringContentLength = MAX_PACKET_SIZE,
			                                                                             MaxBytesPerRead        = MAX_PACKET_SIZE,
			                                                                             MaxArrayLength         = MAX_ARRAY_LENGTH,
			                                                                             MaxNameTableCharCount  = MAX_NAME_TABLE_CHAR_COUNT,
			                                                                             MaxDepth               = 32
		                                                                             }
	                                                              },
	                                                              new EndpointAddress( endPoint ) )
	{
	#if DEBUG
		Endpoint.EndpointBehaviors.Add( new MessageLoggingBehavior() );
	#endif
		AuthToken = authToken;
	}

	public class Ids1Service
	{
		private static readonly MemoryCache<string, CacheRecord> ClientCache = new();

		internal class CacheRecord
		{
			public IDSClient Client
			{
				get
				{
					lock( LockObject )
					{
						while( Locked )
							Thread.Sleep( 1 );

						Locked = true;
						Delay.Start();
						return _Client;
					}
				}
			}

			public string EndPoint
			{
				get
				{
					lock( LockObject )
						return _Endpoint;
				}
			}

			private static readonly object LockObject = new();

			private readonly IDSClient _Client;

			private readonly string _Endpoint;

			private readonly Timer Delay = new( 10 )
			                               {
				                               AutoReset = false
			                               };

			private bool Locked;

			internal CacheRecord( IDSClient client, string endPoint )
			{
				_Client   = client;
				_Endpoint = endPoint;

				Delay.Elapsed += ( _, _ ) =>
				                 {
					                 Locked = false;
				                 };
			}

			~CacheRecord()
			{
				lock( LockObject )
					Delay.Dispose();
			}
		}

		public static IDSClient Client( IRequestContext context, bool stripTest = false )
		{
			var Prefs = context.CarrierServicePreferences;
			var Cid   = context.CarrierId.Trim();

			if( stripTest )
				Cid = Cid.ReplaceLastOccurrence( "test", "", StringComparison.OrdinalIgnoreCase );

			//Ids circuit breaker login
			var AToken = $"{Cid}-{SOAP_LOGIN.UserName}-{SOAP_LOGIN.Password}";

			var Key = CacheKey( AToken );
			var (Ok, CacheRecord) = ClientCache[ Key ];

			string EndPoint;

			if( Prefs.UseJbTest )
				EndPoint = EndPoints.JBTEST2;
			else if( Prefs.IsPml )
				EndPoint = EndPoints.OZ;
			else
				EndPoint = EndPoints.CUSTOM;

			if( Ok )
			{
				if( CacheRecord.EndPoint == EndPoint ) // Probably Jbtest Switch Changed
					return CacheRecord.Client;

				ClientCache.Remove( Key );
			}

			ClientCache.AddSliding( Key, CacheRecord = new CacheRecord( new IDSClient( AToken, EndPoint ), EndPoint ), new TimeSpan( 0, 10, 0 ) );
			return CacheRecord.Client;
		}

		private static string CacheKey( string authToken ) => $"Login-{authToken}";
	}

	public partial class Ids1
	{
		public static void DeleteTrip( IRequestContext context, string tripId, string program )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).DeleteTrip( sendContext, tripId );
			                           } );
		}

		public static void PickupTrip( IRequestContext context, string tripId, string driver, DateTimeOffset? pickupTime, string pop, decimal pieceCount )
		{
			Ids1Service.Client( context ).PickupTrip( context, tripId, driver, pickupTime, pop, pieceCount );
		}

		public static void PickupTrip( IRequestContext context, string tripId, string program, DateTimeOffset? dateTime )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).PickupTrip( sendContext, tripId, dateTime );
			                           } );
		}

		public static void DeliverTrip( IRequestContext context, string tripId, string program, DateTimeOffset? dateTime )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).DeliverTrip( sendContext, tripId, dateTime );
			                           } );
		}

		public static void VerifyTrip( IRequestContext context, string tripId, string program, DateTimeOffset? dateTime )
		{
			RunVoid( context, program, sendContext =>
			                           {
				                           Ids1Service.Client( sendContext ).VerifyTrip( sendContext, tripId, dateTime );
			                           } );
		}


		private static void RunVoid( IRequestContext context, string program, Action<IRequestContext> action, [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
		{
			var SendContext = context.Clone();

			Tasks.RunVoid( () =>
			               {
				               try
				               {
					               action( SendContext );
				               }
				               catch( Exception Exception )
				               {
					               SendContext.SystemLogException( Exception, $"Context: {SendContext.CarrierId} - {SendContext.UserName}\r\n{program},\r\nCaller: {caller},\r\nFile Name: {fileName},\r\n{lineNumber}" );
				               }
			               } );
		}
	}

	public void DeleteTrip( IRequestContext context, string tripId )
	{
		var Packet = new updateTripStatusOnly
		             {
			             authToken = AuthToken,
			             accountId = "",
			             tripId    = tripId,
			             status    = (int)IDS1_STATUS.DELETED
		             };

		LogAndRetryError( context, nameof( DeleteTrip ), Packet, () =>
		                                                         {
			                                                         updateTripStatusOnly( Packet );
		                                                         },
		                  tripId );
	}

	public void UpdateTripStatusAndTime( IRequestContext context, string tripId, STATUS status, STATUS1 status1, DateTimeOffset? dateTime, string pickupNote, string deliveryNote, string billingNote )
	{
		var Packet = new pmlUpdateTripStatusAndTime
		             {
			             authToken     = AuthToken,
			             accountId     = "",
			             tripId        = tripId,
			             status        = (int)status.ToIds1Status( status1 ),
			             date          = dateTime?.DateTime ?? DateTime.MinValue,
			             dateSpecified = dateTime is not null,
			             pickupNote    = pickupNote,
			             deliveryNote  = deliveryNote,
			             billingNote   = billingNote
		             };

		LogAndRetryError( context, nameof( UpdateTripStatusAndTime ), Packet, () =>
		                                                                      {
			                                                                      pmlUpdateTripStatusAndTime( Packet );
		                                                                      },
		                  tripId );
	}

	public void UpdateTripStatusAndTime( IRequestContext context, string tripId, STATUS status, DateTimeOffset? dateTime, string pickupNote, string deliveryNote, string billingNote )
	{
		UpdateTripStatusAndTime( context, tripId, status, STATUS1.UNSET, dateTime, pickupNote, deliveryNote, billingNote );
	}


	public void PickupTrip( IRequestContext context, string tripId, string driver, DateTimeOffset? pickupTime, string pop, decimal pieceCount )
	{
		var Packet = new pickupTripWithPieces
		             {
			             authToken           = AuthToken,
			             tripId              = tripId,
			             driver              = driver,
			             pickupTime          = ServerTime( pickupTime ),
			             pickupTimeSpecified = true,
			             pop                 = pop,
			             pieces              = (int)pieceCount
		             };

		LogAndRetryError( context, nameof( PickupTrip ), Packet, () =>
		                                                         {
			                                                         pickupTripWithPieces( Packet );
		                                                         }, tripId );
	}

	public (List<Trip>?, DateTime LastDateTime) GetUpdatedTripsSince( IRequestContext context,
	                                                                  string carrierId, string userName, string password,
	                                                                  DateTime lastRun )
	{
		var LastRun = lastRun;
		var Now     = DateTime.UtcNow;
		var Diff    = Now - LastRun;

		if( Diff.TotalDays > 30 ) // Don't go back more than 30 days
		{
			LastRun = Now.AddDays( -30 ).StartOfDay();
			Diff    = Now - LastRun;
		}

		var Seconds = (int)Diff.TotalSeconds;

		try
		{
			var Trips = findTripsDetailedUpdatedSinceDateUnlimited( new findTripsDetailedUpdatedSinceDateUnlimited

			                                                        {
				                                                        authToken            = $"{carrierId}-{userName}-{password}",
				                                                        allKey               = ALL,
				                                                        zones                = ALL,
				                                                        lowStatus            = (int)IDS1_STATUS.UNSET,
				                                                        upperStatus          = (int)IDS1_STATUS.SCHEDULED,
				                                                        startDate            = LastRun,
				                                                        startDateSpecified   = true,
				                                                        numberOfSecondsLimit = Seconds
			                                                        } );

			if( Trips is not null )
			{
				var Trps = Trips.Select( t => t.ToTrip() ).ToList();
				var Last = Trps.Count > 0 ? Trps.Max( t => t.LastModified ).AddSeconds( 1 ) : Now;

				if( Last > LastRun )
					LastRun = Last;

				return ( Trps, LastRun );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return ( null, lastRun );
	}

	public void PickupTrip( IRequestContext context, string tripId, DateTimeOffset? dateTime )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.PICKED_UP, dateTime, "", "", "" );
	}

	public void DeliverTrip( IRequestContext context, string tripId, DateTimeOffset? dateTime )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.DELIVERED, dateTime, "", "", "" );
	}

	public void VerifyTrip( IRequestContext context, string tripId, DateTimeOffset? dateTime )
	{
		UpdateTripStatusAndTime( context, tripId, STATUS.VERIFIED, dateTime, "", "", "" );
	}

	internal static DateTime ServerTime( DateTimeOffset? time ) => time?.AsPacificStandardTime().DateTime ?? DateTimeExtensions.Epoch;

#region Error Retry
	public class MyLockClass
	{
	}

	// ReSharper disable once InconsistentNaming
	private static readonly string[] KEY_PHRASES =
	[
		"Java",
		"meta data"
	];

	protected static void LogAndRetryError( IRequestContext context, string functionName, object ids1Object, Action action, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		ToIds1Log.Log( context, functionName, ids1Object );
		RetryError( context, action, tripId, caller, fileName, lineNumber );
	}

	protected static T? LogAndRetryError<T>( IRequestContext context, string functionName, object ids1Object, Func<T> func, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		ToIds1Log.Log( context, functionName, ids1Object );
		var Temp = RetryError( context, func, tripId, caller, fileName, lineNumber );

		if( Temp is not null )
			ToIds1Log.Log( context, $"{functionName} - Returned Value", Temp );

		return Temp;
	}

	private static T? InternalRetryError<T>( IRequestContext context, Func<T>? func = null, Action? action = null, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		T? Result = default;

		var RequestTime = DateTime.UtcNow;

		var LockObject = SemaphoreQueue.SemaphoreQueueByTypeAndId<MyLockClass>( context.CarrierId.TrimToLower() );

		try
		{
			while( true )
			{
				try
				{
					if( action is not null )
						action();
					else if( func is not null )
						Result = func();
					break;
				}
				catch( Exception Exception )
				{
					void LogException()
					{
						var E = tripId.IsNotNullOrWhiteSpace() ? new Exception( $"Trip Id: {tripId}", Exception ) : Exception;
						context.SystemLogException( E, caller, fileName, lineNumber );
					}

					var Message = Exception.Message;

					if( KEY_PHRASES.Any( word => Message.Contains( word, StringComparison.OrdinalIgnoreCase ) ) )
					{
						LogException();
						break;
					}

					var Now = DateTime.UtcNow;

					if( ( Now - RequestTime ).TotalHours >= 2 )
					{
						LogException();
						context.SystemLogException( new Exception( "Send To Ids1 Retry - Tried for 2 hours. Give up." ) );
						break;
					}
					Thread.Sleep( 60_000 );
				}
			}
		}
		finally
		{
			// Min 500ms between ids1 requests
			Tasks.RunVoid( 500, () =>
			                    {
				                    LockObject.Release();
			                    } );
		}
		return Result;
	}

	protected static void RetryError( IRequestContext context, Action action, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		InternalRetryError<object>( context, null, action, tripId, caller, fileName, lineNumber );
	}

	protected static T? RetryError<T>( IRequestContext context, Func<T>? func = null, string tripId = "", [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 ) => InternalRetryError( context, func, null, tripId, caller, fileName, lineNumber );
#endregion

#region AuthToken
	private string _AuthToken = "";


	public string AuthToken
	{
		get => _AuthToken;
		set
		{
			var Parts = value.Split( new[] { '-' }, StringSplitOptions.RemoveEmptyEntries );

			if( Parts.Length == 3 )
				value = $"{Parts[ 0 ].ToIds1CarrierId()}-{Parts[ 1 ].Trim()}-{Parts[ 2 ]}";

			_AuthToken = value;
		}
	}
#endregion
}