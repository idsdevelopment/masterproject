﻿namespace Ids1WebService._Customers.Metro;

public class PingServers
{
	private const string METRO      = "Metro",
	                     ADMIN_USER = "admin",
	                     ADMIN_PASS = "tromeadmin";

	private const string AUTH_TOKEN = $"{METRO}-{ADMIN_USER}-{ADMIN_PASS}";

	[Flags]
	public enum PING_RESULT
	{
		OK,
		DRIVERS_FAIL = 1,
		USERS_FAIL   = 1 << 1,
		EAST1_FAIL   = 1 << 2
	}

	public static PING_RESULT Ping()
	{
		var Result = PING_RESULT.OK;

		try
		{
			if( !CheckConnection( EndPoints.DRIVERS ) )
				Result |= PING_RESULT.DRIVERS_FAIL;

			if( !CheckConnection( EndPoints.USERS ) )
				Result |= PING_RESULT.USERS_FAIL;

			if( !CheckConnection( EndPoints.EAST1 ) )
				Result |= PING_RESULT.EAST1_FAIL;
		}
		catch
		{
		}
		return Result;
	}

	private static bool CheckConnection( string endPoint )
	{
		try
		{
			var Client = new IDSClient( AUTH_TOKEN, endPoint );

			var Result = Client.findUserForLogin( new findUserForLogin
			                                      {
				                                      authToken    = AUTH_TOKEN,
				                                      accountId    = METRO,
				                                      userIdToFind = ADMIN_USER,
				                                      userPass     = ADMIN_PASS
			                                      } )?.@return is not null;
			return Result;
		}
		catch
		{
		}
		return false;
	}
}