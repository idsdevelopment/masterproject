﻿using Microsoft.AspNetCore.Hosting;

namespace Hosting;

public static class Environment
{
	public static IWebHostEnvironment? HostingEnvironment;

	public static string ServerName
	{
		get { return _ServerName ??= System.Environment.GetEnvironmentVariable( "COMPUTERNAME" ) ?? "UNKNOWN"; }
	}

	public static string SlotName
	{
		get { return _SlotName ??= System.Environment.GetEnvironmentVariable( "WEBSITE_SLOT_NAME" ) ?? "UNKNOWN"; }
	}

	private static string? ContentRootPath;

	private static string? _ServerName, _SlotName;

	public static string MapPath( string path )
	{
		if( ContentRootPath is not { } CPath )
		{
			if( HostingEnvironment is null )
				return path;

			ContentRootPath = CPath = HostingEnvironment.ContentRootPath.TrimEnd( '\\' );
		}
		path = path.TrimStart( '/', '\\' );
		return path == "" ? CPath : $@"{CPath}\{path}";
	}
}