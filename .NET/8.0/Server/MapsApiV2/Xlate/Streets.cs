﻿using Utils.Xlate;

namespace MapsApiV2.Xlate;

internal static class Streets
{
	public static List<AddressValidation> FixAbbreviation( List<AddressValidation> addresses )
	{
		return addresses.Select( a =>
		                         {
			                         a.AddressLine1 = a.AddressLine1.FixAbbreviation();
			                         return a;
		                         } )
		                .ToList();
	}
}