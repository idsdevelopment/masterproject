﻿namespace MapsApiV2.Xlate;

internal static class Regions
{
	public static void FixupCountryRegion( AddressData address )
	{
		var Country = address.Country;

		if( Country.IsNullOrWhiteSpace() )
			Country = address.CountryCode;

		var Region = address.State;

		if( Region.IsNullOrWhiteSpace() )
			Region = address.StateCode;

		string CountryName = "", CountryCode = "", RegionName = "", RegionCode = "";

		Utils.Xlate.Region.FixupCountryRegion( Country, Region,
		                                       ref CountryName, ref CountryCode,
		                                       ref RegionName, ref RegionCode );

		address.Country     = CountryName;
		address.CountryCode = CountryCode;
		address.State       = RegionName;
		address.StateCode   = RegionCode;
	}


	public static void FixupCountryRegion( AddressValidation address )
	{
		var Country = address.Country;

		if( Country.IsNullOrWhiteSpace() )
			Country = address.CountryCode;

		var Region = address.Region;

		if( Region.IsNullOrWhiteSpace() )
			Region = address.RegionCode;

		string CountryName = "", CountryCode = "", RegionName = "", RegionCode = "";

		Utils.Xlate.Region.FixupCountryRegion( Country, Region,
		                                       ref CountryName, ref CountryCode,
		                                       ref RegionName, ref RegionCode );

		address.Country     = CountryName;
		address.CountryCode = CountryCode;
		address.Region      = RegionName;
		address.RegionCode  = RegionCode;
	}

	public static List<AddressValidation> FixupCountryRegion( List<AddressValidation> addresses )
	{
		foreach( var Address in addresses )
			FixupCountryRegion( Address );

		return addresses;
	}


	public static AddressList FixupCountryRegion( AddressList addresses )
	{
		foreach( var Address in addresses )
			FixupCountryRegion( Address );

		return addresses;
	}
}