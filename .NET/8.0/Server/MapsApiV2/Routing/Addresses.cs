﻿using MapsApiV2.Xlate;

namespace MapsApiV2;

public partial class Routing
{
	public static async Task<List<CompanyMapAddress>> GetAddresses( IRequestContext context, Companies companies )
	{
		var MapsApi = MapsApiV2.Create( context );
		var Result  = new List<CompanyMapAddress>();

		try
		{
			foreach( var Company in companies )
			{
				(AddressList Addresses, bool RateLimited) Rec;

				var Retries = 10;

				while( true )
				{
					Rec = await MapsApi.FindLocationByAddress( Company.ToAddressData() );

					if( !Rec.RateLimited || ( Retries-- == 0 ) )
						break;

					await Task.Delay( 1_000 );
				}

				if( Rec.Addresses.Count > 0 )
					Result.Add( Rec.Addresses[ 0 ].ToCompanyMapAddress() );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}

	public static async Task<AddressValidList> ValidateAddresses( IRequestContext context, AddressValidationList addresses )
	{
		List<AddressValidation> Distinct;

		switch( addresses.Count )
		{
		case 0:
			return [];

		case 1:
			Distinct = addresses;
			break;

		default:
			Distinct = ( from A in addresses
			             let CoName = A.CompanyName.TrimToUpper()
			             let A1 = A.AddressLine1.TrimToUpper()
			             let City = A.City.TrimToUpper()
			             let Region = A.Region.TrimToUpper()
			             let Country = A.Country.TrimToUpper()
			             orderby Country, Region, City, A1, CoName
			             group A by new { CoName, A1, City, Region, Country }
			             into G
			             select G.First() ).ToList();
			break;
		}

		var Result = new AddressValidList( Streets.FixAbbreviation( Regions.FixupCountryRegion( Distinct ) ) );

		try
		{
			var Addresses = new Companies();

			Addresses.AddRange(
			                   from A in addresses
			                   select new Company
			                          {
				                          CompanyName  = A.CompanyName,
				                          Suite        = A.Suite,
				                          AddressLine1 = A.AddressLine1,
				                          AddressLine2 = A.AddressLine2,
				                          City         = A.City,
				                          Region       = A.Region,
				                          RegionCode   = A.RegionCode,
				                          Country      = A.Country,
				                          CountryCode  = A.CountryCode,
				                          PostalCode   = A.PostalCode,
				                          Latitude     = A.Latitude,
				                          Longitude    = A.Longitude
			                          }
			                  );

			var Recs = await GetAddresses( context, Addresses );

			foreach( var Addr in Result )
			{
				Addr.CompanyName  = Addr.CompanyName.TrimToUpper();
				Addr.Suite        = Addr.Suite.TrimToUpper();
				Addr.AddressLine1 = Addr.AddressLine1.TrimToUpper();
				Addr.AddressLine2 = Addr.AddressLine2.TrimToUpper();
				Addr.City         = Addr.City.TrimToUpper();
				Addr.Region       = Addr.Region.TrimToUpper();
				Addr.RegionCode   = Addr.RegionCode.TrimToUpper();
				Addr.Country      = Addr.Country.TrimToUpper();
				Addr.CountryCode  = Addr.CountryCode.TrimToUpper();
				Addr.PostalCode   = Addr.PostalCode.TrimToUpper();

				foreach( var MapAddress in Recs )
				{
					if( Addr.AddressLine1.CompareEqualsIgnoreCase( MapAddress.AddressLine1 )
					    && Addr.City.CompareEqualsIgnoreCase( MapAddress.City )
					    && Addr.Region.CompareEqualsIgnoreCase( MapAddress.Region )
					    && Addr.RegionCode.CompareEqualsIgnoreCase( MapAddress.RegionCode )
					    && Addr.Country.CompareEqualsIgnoreCase( MapAddress.Country ) )
					{
						Addr.IsValid   = true;
						Addr.Latitude  = MapAddress.Latitude;
						Addr.Longitude = MapAddress.Longitude;

						if( Addr.PostalCode == "" )
							Addr.PostalCode = MapAddress.PostalCode;

						break;
					}
				}
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}
}