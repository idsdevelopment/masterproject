﻿using Ids1WebService;

namespace Imports.Ids1;

internal abstract class Ids1Client : AIClient
{
	public override string AccountTimeZone => TimeZones.PACIFIC;

	public override string UserName => _UserName;
	public override string Password => _Password;

	protected readonly string _UserName,
	                          _Password;

	internal Ids1Client( string carrierId, string userName, string password ) : base( carrierId )
	{
		_UserName = userName;
		_Password = password;
	}

	public override Task<TripList> ImportTrips( IRequestContext context )
	{
		var IdsClient = IDSClient.Ids1Service.Client( context );
		var (Trips, LastRunDateTime) = IdsClient.GetUpdatedTripsSince( context, Account, UserName, Password, LastRun );

		if( Trips is { Count: > 0 } )
		{
			LastRun = LastRunDateTime;
			return Task.FromResult( new TripList( Trips ) );
		}
		return Task.FromResult<TripList>( [] );
	}

	/*
	public override async Task<bool> ExportTrip( IRequestContext context, TripExport export, Trip? trip )
	{
		try
		{
		}
		catch( Exception Exception )
		{
			Context.SystemLogExceptionProgram( Program, Exception );
		}
		return false;
	}
	*/
}