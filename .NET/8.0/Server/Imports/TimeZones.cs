﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imports
{
	internal class TimeZones
	{
		public const string MOUNTAIN           = "Mountain Standard Time",
		                    PACIFIC            = "Pacific Standard Time",
		                    CENTRAL            = "Central Standard Time",
		                    EASTERN            = "Eastern Standard Time",
		                    AUSTRALIAN_EASTERN = "AUS Eastern Standard Time";
	}
}