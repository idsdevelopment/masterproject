﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class CompanyAddressGroup
{
    public int GroupNumber { get; set; }

    public long CompanyId { get; set; }
}
