﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Preference
{
    public int UsersPreferenceId { get; set; }

    public bool Enabled { get; set; }

    public int IntValue { get; set; }

    public string StringValue { get; set; } = null!;

    public float DoubleValue { get; set; }

    public DateTimeOffset DateTimeValue { get; set; }
}
