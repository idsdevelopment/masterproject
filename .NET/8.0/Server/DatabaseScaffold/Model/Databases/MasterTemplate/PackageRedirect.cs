﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class PackageRedirect
{
    public int Id { get; set; }

    public long CompanyId { get; set; }

    public int FromZoneId { get; set; }

    public int ToZoneId { get; set; }

    public int ServiceLevelId { get; set; }

    public int PackageTypeId { get; set; }

    public int OverrideId { get; set; }

    public decimal Charge { get; set; }
}
