﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Address
{
    public long Id { get; set; }

    public string SecondaryId { get; set; } = null!;

    public string Barcode { get; set; } = null!;

    public string PostalBarcode { get; set; } = null!;

    public string Suite { get; set; } = null!;

    public string AddressLine1 { get; set; } = null!;

    public string AddressLine2 { get; set; } = null!;

    public string Vicinity { get; set; } = null!;

    public string City { get; set; } = null!;

    public string Region { get; set; } = null!;

    public string PostalCode { get; set; } = null!;

    public string Country { get; set; } = null!;

    public string CountryCode { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public string Phone1 { get; set; } = null!;

    public string Mobile { get; set; } = null!;

    public string Mobile1 { get; set; } = null!;

    public string Fax { get; set; } = null!;

    public string Contact { get; set; } = null!;

    public string EmailAddress { get; set; } = null!;

    public string EmailAddress1 { get; set; } = null!;

    public string EmailAddress2 { get; set; } = null!;

    public decimal Latitude { get; set; }

    public decimal Longitude { get; set; }

    public string Notes { get; set; } = null!;

    public DateTime LastModified { get; set; }

    public long ZoneId { get; set; }
}
