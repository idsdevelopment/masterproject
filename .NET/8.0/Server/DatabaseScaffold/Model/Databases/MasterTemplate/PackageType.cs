﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class PackageType
{
    public int Id { get; set; }

    public string Description { get; set; } = null!;

    public decimal MinVisibleWeight { get; set; }

    public decimal MaxVisibleWeight { get; set; }

    public string Label { get; set; } = null!;

    public bool HoursXRate { get; set; }

    public bool WeightXRate { get; set; }

    public bool PiecesXRate { get; set; }

    public bool ExcessRate { get; set; }

    public bool DistanceXRate { get; set; }

    public string Formula { get; set; } = null!;

    public bool UsePiecesForCutoff { get; set; }

    public bool Cumulative { get; set; }

    public bool LimitMaxToNextCutoff { get; set; }

    public bool HideOnTripEntry { get; set; }

    public bool SimpleEnabled { get; set; }

    public bool FormulasEnabled { get; set; }

    public bool CutoffsEnabled { get; set; }

    public short SortOrder { get; set; }

    public bool Deleted { get; set; }
}
