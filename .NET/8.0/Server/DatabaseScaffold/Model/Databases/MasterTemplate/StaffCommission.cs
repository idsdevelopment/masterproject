﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class StaffCommission
{
    public byte Type { get; set; }

    public string StaffId { get; set; } = null!;

    public DateTimeOffset DateTime { get; set; }

    public string Reference { get; set; } = null!;

    public int Id { get; set; }

    public int SortOrder { get; set; }

    public string TripId { get; set; } = null!;

    public string ServiceLevel { get; set; } = null!;

    public decimal BaseValue { get; set; }

    public decimal ExtendedValue { get; set; }

    public byte DetailsType { get; set; }

    public string DetailsAsJson { get; set; } = null!;

    public bool CommissionPaid { get; set; }
}
