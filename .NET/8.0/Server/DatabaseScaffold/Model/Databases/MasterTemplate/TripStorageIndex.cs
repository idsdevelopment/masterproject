﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class TripStorageIndex
{
    public string TripId { get; set; } = null!;

    public DateTimeOffset CallTime { get; set; }

    public DateTimeOffset Lastupdated { get; set; }

    public string Reference { get; set; } = null!;

    public string AccountId { get; set; } = null!;

    public byte Status1 { get; set; }

    public byte Status2 { get; set; }

    public byte Status3 { get; set; }

    public string PickupCompanyName { get; set; } = null!;

    public string DeliveryCompanyName { get; set; } = null!;

    public DateTimeOffset PickupedTime { get; set; }

    public DateTimeOffset VerifiedTime { get; set; }
}
