﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class RouteAddress
{
    public long RouteId { get; set; }

    public long CompanyId { get; set; }

    public bool Option1 { get; set; }

    public bool Option2 { get; set; }

    public bool Option3 { get; set; }

    public bool Option4 { get; set; }

    public bool Option5 { get; set; }

    public bool Option6 { get; set; }

    public bool Option7 { get; set; }

    public bool Option8 { get; set; }
}
