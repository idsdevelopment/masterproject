﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Trip
{
    public byte[] ConcurrencyToken { get; set; } = null!;

    public DateTime LastModified { get; set; }

    public string TripId { get; set; } = null!;

    public string Barcode { get; set; } = null!;

    public string AccountId { get; set; } = null!;

    public int Status1 { get; set; }

    public int Status2 { get; set; }

    public int Status3 { get; set; }

    public int Status4 { get; set; }

    public int Status5 { get; set; }

    public int Status6 { get; set; }

    public string Reference { get; set; } = null!;

    public DateTimeOffset CallTime { get; set; }

    public string CallerPhone { get; set; } = null!;

    public string CallerEmail { get; set; } = null!;

    public DateTimeOffset ReadyTime { get; set; }

    public DateTimeOffset DueTime { get; set; }

    public string DriverCode { get; set; } = null!;

    public decimal Weight { get; set; }

    public decimal Pieces { get; set; }

    public decimal Volume { get; set; }

    public string Measurement { get; set; } = null!;

    public decimal OriginalPieceCount { get; set; }

    public byte[] MissingPieces { get; set; } = null!;

    public bool ReceivedByDevice { get; set; }

    public bool ReadByDriver { get; set; }

    public bool DangerousGoods { get; set; }

    public bool HasItems { get; set; }

    public string UNClass { get; set; } = null!;

    public bool HasDangerousGoodsDocuments { get; set; }

    public string Board { get; set; } = null!;

    public string PackageType { get; set; } = null!;

    public string ServiceLevel { get; set; } = null!;

    public string POP { get; set; } = null!;

    public string POD { get; set; } = null!;

    public long PickupCompanyId { get; set; }

    public string PickupCompanyAccountId { get; set; } = null!;

    public string PickupCompanyName { get; set; } = null!;

    public string PickupAddressBarcode { get; set; } = null!;

    public string PickupAddressPostalBarcode { get; set; } = null!;

    public string PickupAddressSuite { get; set; } = null!;

    public string PickupAddressAddressLine1 { get; set; } = null!;

    public string PickupAddressAddressLine2 { get; set; } = null!;

    public string PickupAddressVicinity { get; set; } = null!;

    public string PickupAddressCity { get; set; } = null!;

    public string PickupAddressRegion { get; set; } = null!;

    public string PickupAddressPostalCode { get; set; } = null!;

    public string PickupAddressCountry { get; set; } = null!;

    public string PickupAddressCountryCode { get; set; } = null!;

    public string PickupAddressPhone { get; set; } = null!;

    public string PickupAddressPhone1 { get; set; } = null!;

    public string PickupAddressMobile { get; set; } = null!;

    public string PickupAddressMobile1 { get; set; } = null!;

    public string PickupAddressFax { get; set; } = null!;

    public string PickupAddressEmailAddress { get; set; } = null!;

    public string PickupAddressEmailAddress1 { get; set; } = null!;

    public string PickupAddressEmailAddress2 { get; set; } = null!;

    public string PickupContact { get; set; } = null!;

    public decimal PickupAddressLatitude { get; set; }

    public decimal PickupAddressLongitude { get; set; }

    public string PickupAddressNotes { get; set; } = null!;

    public string PickupNotes { get; set; } = null!;

    public long DeliveryCompanyId { get; set; }

    public string DeliveryCompanyAccountId { get; set; } = null!;

    public string DeliveryCompanyName { get; set; } = null!;

    public string DeliveryAddressBarcode { get; set; } = null!;

    public string DeliveryAddressPostalBarcode { get; set; } = null!;

    public string DeliveryAddressSuite { get; set; } = null!;

    public string DeliveryAddressAddressLine1 { get; set; } = null!;

    public string DeliveryAddressAddressLine2 { get; set; } = null!;

    public string DeliveryAddressVicinity { get; set; } = null!;

    public string DeliveryAddressCity { get; set; } = null!;

    public string DeliveryAddressRegion { get; set; } = null!;

    public string DeliveryAddressPostalCode { get; set; } = null!;

    public string DeliveryAddressCountry { get; set; } = null!;

    public string DeliveryAddressCountryCode { get; set; } = null!;

    public string DeliveryAddressPhone { get; set; } = null!;

    public string DeliveryAddressPhone1 { get; set; } = null!;

    public string DeliveryAddressMobile { get; set; } = null!;

    public string DeliveryAddressMobile1 { get; set; } = null!;

    public string DeliveryAddressFax { get; set; } = null!;

    public string DeliveryAddressEmailAddress { get; set; } = null!;

    public string DeliveryAddressEmailAddress1 { get; set; } = null!;

    public string DeliveryAddressEmailAddress2 { get; set; } = null!;

    public string DeliveryContact { get; set; } = null!;

    public decimal DeliveryAddressLatitude { get; set; }

    public decimal DeliveryAddressLongitude { get; set; }

    public string DeliveryAddressNotes { get; set; } = null!;

    public string DeliveryNotes { get; set; } = null!;

    public long BillingCompanyId { get; set; }

    public string BillingCompanyAccountId { get; set; } = null!;

    public string BillingCompanyName { get; set; } = null!;

    public string BillingAddressBarcode { get; set; } = null!;

    public string BillingAddressPostalBarcode { get; set; } = null!;

    public string BillingAddressSuite { get; set; } = null!;

    public string BillingAddressAddressLine1 { get; set; } = null!;

    public string BillingAddressAddressLine2 { get; set; } = null!;

    public string BillingAddressVicinity { get; set; } = null!;

    public string BillingAddressCity { get; set; } = null!;

    public string BillingAddressRegion { get; set; } = null!;

    public string BillingAddressPostalCode { get; set; } = null!;

    public string BillingAddressCountry { get; set; } = null!;

    public string BillingAddressCountryCode { get; set; } = null!;

    public string BillingAddressPhone { get; set; } = null!;

    public string BillingAddressPhone1 { get; set; } = null!;

    public string BillingAddressMobile { get; set; } = null!;

    public string BillingAddressMobile1 { get; set; } = null!;

    public string BillingAddressFax { get; set; } = null!;

    public string BillingAddressEmailAddress { get; set; } = null!;

    public string BillingAddressEmailAddress1 { get; set; } = null!;

    public string BillingAddressEmailAddress2 { get; set; } = null!;

    public string BillingContact { get; set; } = null!;

    public decimal BillingAddressLatitude { get; set; }

    public decimal BillingAddressLongitude { get; set; }

    public string BillingAddressNotes { get; set; } = null!;

    public string BillingNotes { get; set; } = null!;

    public string CurrentZone { get; set; } = null!;

    public string PickupZone { get; set; } = null!;

    public string DeliveryZone { get; set; } = null!;

    public DateTimeOffset PickupTime { get; set; }

    public double PickupLatitude { get; set; }

    public double PickupLongitude { get; set; }

    public DateTimeOffset DeliveredTime { get; set; }

    public double DeliveredLatitude { get; set; }

    public double DeliveredLongitude { get; set; }

    public DateTimeOffset VerifiedTime { get; set; }

    public double VerifiedLatitude { get; set; }

    public double VerifiedLongitude { get; set; }

    public DateTimeOffset ClaimedTime { get; set; }

    public double ClaimedLatitude { get; set; }

    public double ClaimedLongitude { get; set; }

    public int TripItemsType { get; set; }

    public string TripItemsAsJson { get; set; } = null!;

    public string TripChargesAsJson { get; set; } = null!;

    public string Location { get; set; } = null!;

    public string CallerName { get; set; } = null!;

    public string CallTakerId { get; set; } = null!;

    public bool IsQuote { get; set; }

    public string UndeliverableNotes { get; set; } = null!;

    public short ExtensionType { get; set; }

    public string ExtensionAsJson { get; set; } = null!;

    public DateTimeOffset FinalisedTime { get; set; }

    public double FinalisedLatitude { get; set; }

    public double FinalisedLongitude { get; set; }

    public short DevicePickupSortOrder { get; set; }

    public short DeviceDeliverySortOrder { get; set; }

    public bool DriverCommissionCalculated { get; set; }

    public string Group0 { get; set; } = null!;

    public string Group1 { get; set; } = null!;

    public string Group2 { get; set; } = null!;

    public string Group3 { get; set; } = null!;

    public string Group4 { get; set; } = null!;

    public string Route { get; set; } = null!;

    public string DriverNotes { get; set; } = null!;

    public DateTimeOffset PickupWaitTimeStart { get; set; }

    public int PickupWaitTimeDurationInSeconds { get; set; }

    public DateTimeOffset DeliveryWaitTimeStart { get; set; }

    public int DeliveryWaitTimeDurationInSeconds { get; set; }

    public DateTimeOffset PickupArriveTime { get; set; }

    public DateTimeOffset DeliveryArriveTime { get; set; }

    public long InvoiceNumber { get; set; }

    public decimal TotalAmount { get; set; }

    public decimal TotalTaxAmount { get; set; }

    public decimal DiscountAmount { get; set; }

    public decimal TotalFixedAmount { get; set; }

    public decimal TotalPayrollAmount { get; set; }

    public decimal DeliveryAmount { get; set; }

    public int Pallets { get; set; }

    public string Schedule { get; set; } = null!;

    public string ImportReference { get; set; } = null!;
}
