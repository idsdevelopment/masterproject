﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Configuration
{
    public byte[] ConcurrencyToken { get; set; } = null!;

    public Guid Id { get; set; }

    public int CurrentVersion { get; set; }

    public Guid CompanyAddressId { get; set; }

    public string StorageAccount { get; set; } = null!;

    public string StorageAccountKey { get; set; } = null!;

    public string StorageTableEndpoint { get; set; } = null!;

    public string StorageFileEndpoint { get; set; } = null!;

    public string StorageBlobEndpoint { get; set; } = null!;

    public long LastIds1AccountTick { get; set; }

    public long LastIds1TripTick { get; set; }

    public long LastIds1AddressTick { get; set; }

    public DateTime LastDistanceCleanup { get; set; }

    public DateTime LastMessagingCleanup { get; set; }

    public long NextInvoiceNumber { get; set; }
}
