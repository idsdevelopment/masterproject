﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Database.Model.Databases.MasterTemplate;

public partial class MasterTemplateContext : DbContext
{
    public MasterTemplateContext(DbContextOptions<MasterTemplateContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Address> Addresses { get; set; }

    public virtual DbSet<ChargeDetail> ChargeDetails { get; set; }

    public virtual DbSet<Company> Companies { get; set; }

    public virtual DbSet<CompanyAddress> CompanyAddresses { get; set; }

    public virtual DbSet<CompanyAddressGroup> CompanyAddressGroups { get; set; }

    public virtual DbSet<CompanyAddressGroupName> CompanyAddressGroupNames { get; set; }

    public virtual DbSet<CompanyToShift> CompanyToShifts { get; set; }

    public virtual DbSet<Configuration> Configurations { get; set; }

    public virtual DbSet<Export> Exports { get; set; }

    public virtual DbSet<Inventory> Inventories { get; set; }

    public virtual DbSet<Invoice> Invoices { get; set; }

    public virtual DbSet<PackageOverride> PackageOverrides { get; set; }

    public virtual DbSet<PackageRedirect> PackageRedirects { get; set; }

    public virtual DbSet<PackageType> PackageTypes { get; set; }

    public virtual DbSet<PackageTypeCutoff> PackageTypeCutoffs { get; set; }

    public virtual DbSet<PostalCodeToRoute> PostalCodeToRoutes { get; set; }

    public virtual DbSet<Preference> Preferences { get; set; }

    public virtual DbSet<RateMatrix> RateMatrices { get; set; }

    public virtual DbSet<ResellerCustomer> ResellerCustomers { get; set; }

    public virtual DbSet<ResellerCustomerCompany> ResellerCustomerCompanies { get; set; }

    public virtual DbSet<ResellerCustomerCompanySummary> ResellerCustomerCompanySummaries { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<Route> Routes { get; set; }

    public virtual DbSet<RouteAddress> RouteAddresses { get; set; }

    public virtual DbSet<ServiceLevel> ServiceLevels { get; set; }

    public virtual DbSet<ServiceLevelPackageType> ServiceLevelPackageTypes { get; set; }

    public virtual DbSet<Staff> Staff { get; set; }

    public virtual DbSet<StaffCommission> StaffCommissions { get; set; }

    public virtual DbSet<StaffNote> StaffNotes { get; set; }

    public virtual DbSet<StaffRole> StaffRoles { get; set; }

    public virtual DbSet<StaffShift> StaffShifts { get; set; }

    public virtual DbSet<StaffZone> StaffZones { get; set; }

    public virtual DbSet<Trip> Trips { get; set; }

    public virtual DbSet<TripStorageIndex> TripStorageIndices { get; set; }

    public virtual DbSet<XRef> XRefs { get; set; }

    public virtual DbSet<Zone> Zones { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Address>(entity =>
        {
            entity.HasIndex(e => e.Barcode, "IX_Addresses_ByBarcode");

            entity.HasIndex(e => e.SecondaryId, "IX_Addresses_BySecondaryId");

            entity.HasIndex(e => new { e.Suite, e.AddressLine1 }, "IX_Addresses_BySuiteAndAddressLine1");

            entity.HasIndex(e => e.LastModified, "IX_Addresses_LastModified");

            entity.Property(e => e.AddressLine1)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.AddressLine2)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.Barcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.City)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.Contact).HasDefaultValue("");
            entity.Property(e => e.Country)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.CountryCode)
                .HasMaxLength(3)
                .HasDefaultValue("");
            entity.Property(e => e.EmailAddress).HasDefaultValue("");
            entity.Property(e => e.EmailAddress1).HasDefaultValue("");
            entity.Property(e => e.EmailAddress2).HasDefaultValue("");
            entity.Property(e => e.Fax)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.LastModified).HasDefaultValueSql("(sysdatetime())");
            entity.Property(e => e.Latitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Longitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Mobile)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.Mobile1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.Notes).HasDefaultValue("");
            entity.Property(e => e.Phone)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.Phone1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PostalBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PostalCode)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.Region)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.SecondaryId)
                .HasMaxLength(100)
                .HasDefaultValue("")
                .IsFixedLength();
            entity.Property(e => e.Suite)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Vicinity)
                .HasMaxLength(100)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<ChargeDetail>(entity =>
        {
            entity.HasKey(e => e.ChargeId).HasName("PK__tmp_ms_x__17FC361B2440EB10");

            entity.Property(e => e.ChargeId)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.AccountId)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Formula).HasDefaultValue("");
            entity.Property(e => e.Label)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Max)
                .HasDefaultValue(999999m)
                .HasColumnType("decimal(10, 4)");
            entity.Property(e => e.Min).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.OverridenPackage)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Text)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Value).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<Company>(entity =>
        {
            entity.ToTable("Company", tb => tb.HasTrigger("Trigger_OnCompanyDelete"));

            entity.HasIndex(e => e.CompanyName, "IX_Company_ByCompanyName");

            entity.HasIndex(e => e.EmailInvoice, "IX_Company_EmailInvoice");

            entity.HasIndex(e => e.LastBilled, "IX_Company_LastBilled");

            entity.HasIndex(e => e.LastModified, "IX_Company_LastModified");

            entity.HasIndex(e => e.LocationBarcode, "IX_Company_LocationBarcode");

            entity.HasIndex(e => new { e.MultipleTripsOnInvoice, e.BillingPeriod }, "IX_Company_Multiple");

            entity.Property(e => e.BillToCompany)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.CompanyName)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.CompanyNumber)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.CreditLimit)
                .HasDefaultValue(10000m)
                .HasColumnType("decimal(10, 4)");
            entity.Property(e => e.DisplayCompanyName)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.InvoiceOverride)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.LastBilled).HasDefaultValueSql("(sysdatetime())");
            entity.Property(e => e.LastModified).HasDefaultValueSql("(sysdatetime())");
            entity.Property(e => e.LocationBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.UserName)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<CompanyAddress>(entity =>
        {
            entity.HasKey(e => new { e.CompanyId, e.AddressId });

            entity.ToTable(tb => tb.HasTrigger("Trigger_OnDeleteCompanyAddresses"));

            entity.HasIndex(e => new { e.AddressId, e.CompanyId }, "AK_CompanyAddresses_ByAddressCompanyId").IsUnique();

            entity.HasIndex(e => e.CompanyId, "IX_CompanyAddresses_CompanyName");
        });

        modelBuilder.Entity<CompanyAddressGroup>(entity =>
        {
            entity.HasKey(e => new { e.GroupNumber, e.CompanyId });
        });

        modelBuilder.Entity<CompanyAddressGroupName>(entity =>
        {
            entity.HasKey(e => e.GroupNumber).HasName("PK__CompanyA__8BA4751BBF5D2769");

            entity.ToTable(tb => tb.HasTrigger("Trigger_DeleteCompanyAddressGroupNames"));

            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<CompanyToShift>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC0736F828B5");

            entity.ToTable("CompanyToShift");

            entity.HasIndex(e => new { e.CompanyId, e.ShiftId }, "IX_CompanyToShift_ByCompanyId");

            entity.HasIndex(e => new { e.ShiftId, e.CompanyId }, "IX_CompanyToShift_ByShiftId");
        });

        modelBuilder.Entity<Configuration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC07059F5FA0");

            entity.ToTable("Configuration");

            entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");
            entity.Property(e => e.CompanyAddressId).HasDefaultValueSql("(newsequentialid())");
            entity.Property(e => e.ConcurrencyToken)
                .IsRowVersion()
                .IsConcurrencyToken();
            entity.Property(e => e.LastDistanceCleanup).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.LastMessagingCleanup).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.StorageAccount)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageAccountKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageBlobEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageFileEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageTableEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Export>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC078727E014");

            entity.HasIndex(e => new { e.DataType, e.Code, e.Id }, "IX_Exports_Code_DataTYpe").IsUnique();

            entity.Property(e => e.Code)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Exported)
                .HasDefaultValueSql("(getutcdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.String1).HasDefaultValue("");
            entity.Property(e => e.String2).HasDefaultValue("");
            entity.Property(e => e.String3).HasDefaultValue("");
            entity.Property(e => e.String4).HasDefaultValue("");
            entity.Property(e => e.String5).HasDefaultValue("");
        });

        modelBuilder.Entity<Inventory>(entity =>
        {
            entity.HasKey(e => e.InventoryCode).HasName("PK__tmp_ms_x__E1E5B5D9FE7BB6FF");

            entity.ToTable("Inventory");

            entity.HasIndex(e => new { e.Barcode, e.InventoryCode }, "IX_Inventory_Barcode").IsUnique();

            entity.HasIndex(e => new { e.IsContainer, e.Barcode }, "IX_Inventory_IsContainerBarcode");

            entity.HasIndex(e => new { e.IsContainer, e.InventoryCode }, "IX_Inventory_IsContainerInventory");

            entity.HasIndex(e => e.ShortDescription, "IX_Inventory_ShortDescription");

            entity.Property(e => e.InventoryCode)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Barcode)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.BinX)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.BinY)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.BinZ)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DecimalData1).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData2).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData3).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData4).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.LongDescription).HasDefaultValue("");
            entity.Property(e => e.PackageQuantity).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.PackageType)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.ShortDescription)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.StringData1).HasDefaultValue("");
            entity.Property(e => e.StringData2).HasDefaultValue("");
            entity.Property(e => e.StringData3).HasDefaultValue("");
            entity.Property(e => e.StringData4).HasDefaultValue("");
            entity.Property(e => e.UNClass)
                .HasMaxLength(6)
                .HasDefaultValue("");
            entity.Property(e => e.UnitVolume).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.UnitWeight).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<Invoice>(entity =>
        {
            entity.HasKey(e => e.InvoiceNumber).HasName("PK__tmp_ms_x__D776E9807A767461");

            entity.ToTable("Invoice");

            entity.HasIndex(e => new { e.BillingCompany, e.InvoiceDateTime }, "IX_Invoice_BillingCompany");

            entity.HasIndex(e => e.BillingCompanyId, "IX_Invoice_BillingCompanyId");

            entity.HasIndex(e => new { e.BillingPeriod, e.BillingCompany }, "IX_Invoice_BillingPeriod");

            entity.HasIndex(e => e.IsCreditNote, "IX_Invoice_CreditNode");

            entity.HasIndex(e => new { e.EmailInvoice, e.BillingCompany }, "IX_Invoice_Email");

            entity.HasIndex(e => new { e.InvoiceFromDateTime, e.InvoiceToDateTime }, "IX_Invoice_FromTo");

            entity.HasIndex(e => e.InvoiceDateTime, "IX_Invoice_InvoiceDate");

            entity.Property(e => e.BillingCompany)
                .HasMaxLength(100)
                .HasDefaultValue("")
                .IsFixedLength();
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.EmailSubject)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.InvoiceDateTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.InvoiceFromDateTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.InvoiceToDateTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.NonTaxableValue).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.Residual).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TaxableValue).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalCharges).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalDiscount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalTaxA).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalTaxB).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalValue).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TripChargesAsJson).HasDefaultValue("");
            entity.Property(e => e.TripIdsAsJson).HasDefaultValue("");
        });

        modelBuilder.Entity<PackageOverride>(entity =>
        {
            entity.ToTable(tb => tb.HasTrigger("Trigger_PackageOverrides"));

            entity.HasIndex(e => new { e.CompanyId, e.FromPackageTypeId, e.ToPackageTypeId }, "IX_PackageOverrides_CompanyId").IsUnique();
        });

        modelBuilder.Entity<PackageRedirect>(entity =>
        {
            entity.ToTable("PackageRedirect");

            entity.HasIndex(e => new { e.CompanyId, e.FromZoneId, e.ToZoneId, e.ServiceLevelId, e.PackageTypeId, e.OverrideId }, "IX_PackageRedirect_Key").IsUnique();

            entity.HasIndex(e => e.OverrideId, "IX_PackageRedirect_OverrideId");

            entity.Property(e => e.Charge).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<PackageType>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC075EC8F9F3");

            entity.ToTable("PackageType", tb => tb.HasTrigger("Trigger_OnPackageTypeDelete"));

            entity.HasIndex(e => e.Description, "IX_PackageType_ByPackageType");

            entity.Property(e => e.Description)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Formula).HasDefaultValue("");
            entity.Property(e => e.Label)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.MaxVisibleWeight).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.MinVisibleWeight).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<PackageTypeCutoff>(entity =>
        {
            entity.HasKey(e => e.PackageTypeId).HasName("PK__tmp_ms_x__0557DC5012530C7E");

            entity.ToTable("PackageTypeCutoff");

            entity.HasIndex(e => e.ToPackageType, "IX_PackageTypeCutoff_ToPackageType");

            entity.Property(e => e.PackageTypeId).ValueGeneratedNever();
            entity.Property(e => e.CutoffAmount).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<PostalCodeToRoute>(entity =>
        {
            entity.HasKey(e => new { e.RouteId, e.PostalCode });

            entity.ToTable("PostalCodeToRoute");

            entity.HasIndex(e => e.PostalCode, "IX_PostalCodeToRoute_PostCode");

            entity.HasIndex(e => e.RouteId, "IX_PostalCodeToRoute_RouteId");

            entity.HasIndex(e => new { e.Zone1, e.PostalCode }, "IX_PostalCodeToRoute_Zone1");

            entity.HasIndex(e => new { e.Zone2, e.PostalCode }, "IX_PostalCodeToRoute_Zone2");

            entity.HasIndex(e => new { e.Zone3, e.PostalCode }, "IX_PostalCodeToRoute_Zone3");

            entity.HasIndex(e => new { e.Zone4, e.PostalCode }, "IX_PostalCodeToRoute_Zone4");

            entity.Property(e => e.PostalCode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Zone1)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Zone2)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Zone3)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Zone4)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Preference>(entity =>
        {
            entity.HasKey(e => e.UsersPreferenceId).HasName("PK__tmp_ms_x__EB56366685E8AF5C");

            entity.Property(e => e.UsersPreferenceId).ValueGeneratedNever();
            entity.Property(e => e.DateTimeValue).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.StringValue).HasDefaultValue("");
        });

        modelBuilder.Entity<RateMatrix>(entity =>
        {
            entity.HasKey(e => new { e.FromZoneId, e.ToZoneId, e.ServiceLevelId, e.PackageTypeId, e.VehicleTypeId });

            entity.ToTable("RateMatrix");

            entity.HasIndex(e => new { e.CompanyId, e.IsOverride }, "IX_RateMatrix_Company_Override");

            entity.HasIndex(e => e.FromZoneId, "IX_RateMatrix_FromZone");

            entity.HasIndex(e => e.PackageTypeId, "IX_RateMatrix_PackageType");

            entity.HasIndex(e => e.ServiceLevelId, "IX_RateMatrix_ServiceLevel");

            entity.HasIndex(e => e.ToZoneId, "IX_RateMatrix_ToZone");

            entity.HasIndex(e => e.VehicleTypeId, "IX_RateMatrix_VehicleType");

            entity.Property(e => e.CompanyId).HasDefaultValue(-1L);
            entity.Property(e => e.Formula)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.Value).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<ResellerCustomer>(entity =>
        {
            entity.HasKey(e => e.CustomerCode).HasName("PK__tmp_ms_x__06678520EE4EDCF3");

            entity.ToTable(tb => tb.HasTrigger("Trigger_OnCustomersDelete"));

            entity.HasIndex(e => e.BoolOption1, "IX_ResellerCustomers_BoolOption1");

            entity.HasIndex(e => e.CompanyName, "IX_ResellerCustomers_ByCompanyName");

            entity.HasIndex(e => e.LastUpdated, "IX_ResellerCustomers_ByLastUpdated");

            entity.HasIndex(e => e.LoginCode, "IX_ResellerCustomers_ByLoginCode");

            entity.Property(e => e.CustomerCode).HasMaxLength(50);
            entity.Property(e => e.CompanyName)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DateTimeOption1).HasDefaultValueSql("(sysdatetimeoffset())");
            entity.Property(e => e.DateTimeOption2).HasDefaultValueSql("(sysdatetimeoffset())");
            entity.Property(e => e.DateTimeOption3).HasDefaultValueSql("(sysdatetimeoffset())");
            entity.Property(e => e.DateTimeOption4).HasDefaultValueSql("(sysdatetimeoffset())");
            entity.Property(e => e.DateTimeOption5).HasDefaultValueSql("(sysdatetimeoffset())");
            entity.Property(e => e.DecimalOption1).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DecimalOption2).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DecimalOption3).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DecimalOption4).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DecimalOption5).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.LastUpdated)
                .IsRowVersion()
                .IsConcurrencyToken();
            entity.Property(e => e.LoginCode)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.ShiftRate)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.StringOption1).HasDefaultValue("");
            entity.Property(e => e.StringOption2).HasDefaultValue("");
            entity.Property(e => e.StringOption3).HasDefaultValue("");
            entity.Property(e => e.StringOption4).HasDefaultValue("");
            entity.Property(e => e.StringOption5).HasDefaultValue("");
            entity.Property(e => e.UserName)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<ResellerCustomerCompany>(entity =>
        {
            entity.HasKey(e => new { e.CustomerCode, e.CompanyId }).HasName("PK_ResellerCustomerAddresses");

            entity.ToTable(tb => tb.HasTrigger("Trigger_OnResellerCustomerAddressDelete"));

            entity.HasIndex(e => e.CompanyId, "IX_ResellerCustomerCompanies_CompanyId");

            entity.HasIndex(e => e.CustomerCode, "IX_ResellerCustomerCompanies_CustomerCode");

            entity.Property(e => e.CustomerCode).HasMaxLength(50);
        });

        modelBuilder.Entity<ResellerCustomerCompanySummary>(entity =>
        {
            entity.HasKey(e => new { e.CompanyId, e.AddressId });

            entity.ToTable("ResellerCustomerCompanySummary");

            entity.HasIndex(e => e.CompanyName, "IX_ResellerCustomerCompanySummary_ByCompanyName");

            entity.HasIndex(e => e.CustomerCode, "IX_ResellerCustomerCompanySummary_ByCustomerCode");

            entity.HasIndex(e => new { e.CustomerCode, e.CompanyName }, "IX_ResellerCustomerCompanySummary_ByCustomerCodeAndCompanyName");

            entity.HasIndex(e => e.LocationBarcode, "IX_ResellerCustomerCompanySummary_ByLocationBarcode");

            entity.Property(e => e.AddressLine1)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.AddressLine2)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.City)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.CompanyName)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.CountryCode)
                .HasMaxLength(3)
                .HasDefaultValue("");
            entity.Property(e => e.CustomerCode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DisplayCompanyName)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.LocationBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PostalCode)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.Region)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.Suite)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.ToTable(tb => tb.HasTrigger("Trigger_OnDeleteRole"));

            entity.HasIndex(e => e.RoleName, "IX_Roles_RoleName").IsUnique();

            entity.Property(e => e.JsonObject).HasDefaultValue("");
            entity.Property(e => e.RoleName)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Route>(entity =>
        {
            entity.ToTable(tb => tb.HasTrigger("Trigger_OnRouteDelete"));

            entity.HasIndex(e => e.CustomerCode, "IX_Routes_ByCustomer");

            entity.HasIndex(e => new { e.CustomerCode, e.Name }, "IX_Routes_ByCustomerAndName");

            entity.HasIndex(e => e.ShowOnMobileApp, "IX_Routes_ShowOnMobile");

            entity.Property(e => e.BillingCompany).HasMaxLength(50);
            entity.Property(e => e.Commission).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.CustomerCode).HasMaxLength(50);
            entity.Property(e => e.EndTime).HasDefaultValueSql("(sysdatetime())");
            entity.Property(e => e.Name).HasMaxLength(50);
            entity.Property(e => e.StartTime).HasDefaultValueSql("(sysdatetime())");
        });

        modelBuilder.Entity<RouteAddress>(entity =>
        {
            entity.HasKey(e => new { e.RouteId, e.CompanyId });
        });

        modelBuilder.Entity<ServiceLevel>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC07692DD2A2");

            entity.ToTable(tb => tb.HasTrigger("Trigger_OnServiceLevelDelete"));

            entity.HasIndex(e => e.Name, "IX_Table_ServiceLevel");

            entity.Property(e => e.Background_ARGB).HasDefaultValueSql("(0xFFFFFFFF)");
            entity.Property(e => e.Foreground_ARGB)
                .HasDefaultValueSql("(0xFF000000)")
                .HasColumnName("Foreground ARGB");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<ServiceLevelPackageType>(entity =>
        {
            entity.HasKey(e => new { e.ServiceLevelId, e.PackageTypeId });

            entity.HasIndex(e => e.PackageTypeId, "IX_ServiceLevelPackageTypes_PackageType");
        });

        modelBuilder.Entity<Staff>(entity =>
        {
            entity.ToTable(tb => tb.HasTrigger("Trigger_Staff"));

            entity.HasIndex(e => new { e.Enabled, e.StaffId }, "IX_Staff_Enabled");

            entity.HasIndex(e => new { e.Type, e.StaffId }, "IX_Staff_StaffId");

            entity.Property(e => e.CommissionFormula)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.EmergencyEmail)
                .HasMaxLength(360)
                .HasDefaultValue("");
            entity.Property(e => e.EmergencyFirstName)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.EmergencyLastName)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.EmergencyMobile)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.EmergencyPhone)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.FirstName)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.MainCommission1).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.MainCommission2).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.MainCommission3).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.MiddleNames)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.OverrideCommission1).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission10).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission11).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission12).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission2).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission3).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission4).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission5).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission6).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission7).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission8).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.OverrideCommission9).HasColumnType("decimal(12, 3)");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.StaffId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.TaxNumber)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.TotalCommission).HasColumnType("decimal(12, 3)");
        });

        modelBuilder.Entity<StaffCommission>(entity =>
        {
            entity.HasKey(e => new { e.Type, e.StaffId, e.DateTime, e.Reference, e.Id });

            entity.ToTable("StaffCommission");

            entity.HasIndex(e => e.CommissionPaid, "IX_StaffCommission_ByCommissionPaid");

            entity.HasIndex(e => new { e.Reference, e.StaffId }, "IX_StaffCommission_ByReferenceStaffId");

            entity.HasIndex(e => e.TripId, "IX_StaffCommission_ByTripId");

            entity.Property(e => e.StaffId)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.DateTime).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.Reference)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.BaseValue).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.DetailsAsJson).HasDefaultValue("");
            entity.Property(e => e.ExtendedValue).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.ServiceLevel)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.TripId)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<StaffNote>(entity =>
        {
            entity.HasKey(e => new { e.Type, e.StaffId, e.NoteName });

            entity.Property(e => e.StaffId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.NoteName)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Note).HasDefaultValue("");
        });

        modelBuilder.Entity<StaffRole>(entity =>
        {
            entity.HasIndex(e => new { e.Type, e.StaffId, e.Id }, "IX_StaffRoles_StaffId_RoleId");

            entity.Property(e => e.ConcurrencyToken)
                .IsRowVersion()
                .IsConcurrencyToken();
            entity.Property(e => e.StaffId)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<StaffShift>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC0735CEE723");

            entity.ToTable("StaffShift", tb => tb.HasTrigger("Trigger_StaffShiftDelete"));

            entity.HasIndex(e => e.Shift, "IX_EmployeeShift_ByShift").IsUnique();

            entity.HasIndex(e => new { e.Start, e.End }, "IX_StaffShift_ByTime");

            entity.HasIndex(e => e.ServiceLevelId, "IX_StaffShift_ServiceLevel");

            entity.Property(e => e.EndDay).HasDefaultValue((short)-1);
            entity.Property(e => e.Formula).HasDefaultValue("");
            entity.Property(e => e.Shift)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.StartDay).HasDefaultValue((short)-1);
        });

        modelBuilder.Entity<StaffZone>(entity =>
        {
            entity.HasKey(e => new { e.Type, e.StaffId, e.ZoneName });

            entity.Property(e => e.StaffId).HasMaxLength(50);
            entity.Property(e => e.ZoneName)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Trip>(entity =>
        {
            entity.HasKey(e => e.TripId).HasName("PK__tmp_ms_x__51DC713E002EF032");

            entity.ToTable(tb => tb.HasTrigger("Trigger_Trips_OnDelete"));

            entity.HasIndex(e => new { e.BillingCompanyName, e.Status1, e.InvoiceNumber }, "IX_Trips_BillingCoStatusInvoice");

            entity.HasIndex(e => new { e.AccountId, e.TripId }, "IX_Trips_ByAccountCode");

            entity.HasIndex(e => new { e.DeliveredTime, e.Status1, e.TripId }, "IX_Trips_ByDeliveredTimeAndStatus");

            entity.HasIndex(e => new { e.DriverCode, e.TripId }, "IX_Trips_ByDriverCode");

            entity.HasIndex(e => new { e.TripItemsType, e.TripId }, "IX_Trips_ByItemsType_TripId");

            entity.HasIndex(e => new { e.PickupTime, e.Status1, e.TripId }, "IX_Trips_ByPickupTimeAndStatus");

            entity.HasIndex(e => new { e.CallTime, e.Status1 }, "IX_Trips_CallTimeStatus");

            entity.HasIndex(e => new { e.DeliveredTime, e.Status1 }, "IX_Trips_DeliveredTimeStatus");

            entity.HasIndex(e => e.DeliveryCompanyId, "IX_Trips_DeliveryAddressId");

            entity.HasIndex(e => new { e.DeliveryCompanyAccountId, e.Status1 }, "IX_Trips_DeliveryCompanyAccountId_Status");

            entity.HasIndex(e => new { e.DriverCommissionCalculated, e.Status1, e.PickupTime }, "IX_Trips_DriverCommissionCalculatedStatusPickupDate");

            entity.HasIndex(e => new { e.DueTime, e.Status1 }, "IX_Trips_DueTimeStatus");

            entity.HasIndex(e => new { e.ImportReference, e.AccountId }, "IX_Trips_ImportReference");

            entity.HasIndex(e => e.LastModified, "IX_Trips_LastModified");

            entity.HasIndex(e => e.Location, "IX_Trips_Location");

            entity.HasIndex(e => e.PickupCompanyId, "IX_Trips_PickupAddressId");

            entity.HasIndex(e => new { e.PickupCompanyAccountId, e.Status1 }, "IX_Trips_PickupCompanyAccountId_Status");

            entity.HasIndex(e => new { e.PickupTime, e.Status1 }, "IX_Trips_PickupTimeStatus");

            entity.HasIndex(e => new { e.Reference, e.AccountId }, "IX_Trips_Reference");

            entity.HasIndex(e => new { e.Status1, e.Board }, "IX_Trips_StatusAndBoard");

            entity.HasIndex(e => new { e.VerifiedTime, e.Status1 }, "IX_Trips_VerifiedTimeStatus");

            entity.Property(e => e.TripId)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.AccountId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Barcode)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressAddressLine1)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressAddressLine2)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressCity)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressCountry)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressCountryCode)
                .HasMaxLength(3)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressEmailAddress)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressEmailAddress1)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressEmailAddress2)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressFax)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressLatitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.BillingAddressLongitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.BillingAddressMobile)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressMobile1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressNotes).HasDefaultValue("");
            entity.Property(e => e.BillingAddressPhone)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressPhone1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressPostalBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressPostalCode)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressRegion)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressSuite)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.BillingAddressVicinity)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingCompanyAccountId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.BillingCompanyId).HasDefaultValue(-1L);
            entity.Property(e => e.BillingCompanyName)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.BillingContact)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.BillingNotes).HasDefaultValue("");
            entity.Property(e => e.Board)
                .HasMaxLength(255)
                .HasDefaultValue("0");
            entity.Property(e => e.CallTakerId)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.CallTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.CallerEmail)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.CallerName)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.CallerPhone)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.ClaimedTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.ConcurrencyToken)
                .IsRowVersion()
                .IsConcurrencyToken();
            entity.Property(e => e.CurrentZone)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveredTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.DeliveryAddressAddressLine1)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressAddressLine2)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressCity)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressCountry)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressCountryCode).HasMaxLength(3);
            entity.Property(e => e.DeliveryAddressEmailAddress)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressEmailAddress1)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressEmailAddress2)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressFax)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressLatitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DeliveryAddressLongitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DeliveryAddressMobile)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressMobile1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressNotes).HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressPhone)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressPhone1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressPostalBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressPostalCode)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressRegion)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressSuite)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAddressVicinity)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryAmount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.DeliveryArriveTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.DeliveryCompanyAccountId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryCompanyId).HasDefaultValue(-1L);
            entity.Property(e => e.DeliveryCompanyName)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryContact)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.DeliveryNotes).HasDefaultValue("");
            entity.Property(e => e.DeliveryWaitTimeStart).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.DeliveryZone)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DiscountAmount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.DriverCode)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.DriverNotes).HasDefaultValue("");
            entity.Property(e => e.DueTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.ExtensionAsJson).HasDefaultValue("");
            entity.Property(e => e.ExtensionType).HasDefaultValue((short)-1);
            entity.Property(e => e.FinalisedTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.Group0)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Group1)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Group2)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Group3)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Group4)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.ImportReference)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.LastModified).HasDefaultValueSql("(sysdatetime())");
            entity.Property(e => e.Location)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Measurement)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.MissingPieces)
                .HasMaxLength(255)
                .HasDefaultValueSql("((0))");
            entity.Property(e => e.OriginalPieceCount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.POD)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.POP)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PackageType)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressAddressLine1)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressAddressLine2)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressCity)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressCountry)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressCountryCode)
                .HasMaxLength(3)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressEmailAddress)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressEmailAddress1)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressEmailAddress2)
                .HasMaxLength(320)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressFax)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressLatitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.PickupAddressLongitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.PickupAddressMobile)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressMobile1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressNotes).HasDefaultValue("");
            entity.Property(e => e.PickupAddressPhone)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressPhone1)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressPostalBarcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressPostalCode)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressRegion)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressSuite)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PickupAddressVicinity)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupArriveTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.PickupCompanyAccountId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.PickupCompanyId).HasDefaultValue(-1L);
            entity.Property(e => e.PickupCompanyName)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.PickupContact)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.PickupNotes).HasDefaultValue("");
            entity.Property(e => e.PickupTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.PickupWaitTimeStart).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.PickupZone)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Pieces).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.ReadyTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.Reference)
                .HasMaxLength(511)
                .HasDefaultValue("");
            entity.Property(e => e.Route)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Schedule)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.ServiceLevel)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.TotalAmount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalFixedAmount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalPayrollAmount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TotalTaxAmount).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.TripChargesAsJson).HasDefaultValue("");
            entity.Property(e => e.TripItemsAsJson).HasDefaultValue("");
            entity.Property(e => e.UNClass)
                .HasMaxLength(6)
                .HasDefaultValue("");
            entity.Property(e => e.UndeliverableNotes).HasDefaultValue("");
            entity.Property(e => e.VerifiedTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.Volume).HasColumnType("decimal(10, 4)");
            entity.Property(e => e.Weight).HasColumnType("decimal(10, 4)");
        });

        modelBuilder.Entity<TripStorageIndex>(entity =>
        {
            entity.HasKey(e => e.TripId);

            entity.ToTable("TripStorageIndex");

            entity.HasIndex(e => e.AccountId, "IX_TripStorageIndex_AccountId");

            entity.HasIndex(e => e.CallTime, "IX_TripStorageIndex_CallTime");

            entity.HasIndex(e => e.DeliveryCompanyName, "IX_TripStorageIndex_DeliveryCompany");

            entity.HasIndex(e => e.Lastupdated, "IX_TripStorageIndex_LastUpdated");

            entity.HasIndex(e => e.PickupCompanyName, "IX_TripStorageIndex_PickupCompany");

            entity.HasIndex(e => e.PickupedTime, "IX_TripStorageIndex_PickupedTime");

            entity.HasIndex(e => e.Reference, "IX_TripStorageIndex_Reference");

            entity.HasIndex(e => e.Status1, "IX_TripStorageIndex_Satus1");

            entity.HasIndex(e => e.Status2, "IX_TripStorageIndex_Satus2");

            entity.HasIndex(e => e.Status3, "IX_TripStorageIndex_Satus3");

            entity.HasIndex(e => e.VerifiedTime, "IX_TripStorageIndex_VerifiedTime");

            entity.Property(e => e.TripId)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.AccountId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.CallTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.DeliveryCompanyName)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Lastupdated).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.PickupCompanyName)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.PickupedTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
            entity.Property(e => e.Reference)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.VerifiedTime).HasDefaultValue(new DateTimeOffset(new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 11, 0, 0, 0)));
        });

        modelBuilder.Entity<XRef>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC074AA9762F");

            entity.ToTable("XRef");

            entity.HasIndex(e => new { e.Type, e.Id1, e.Id2 }, "IX_XRef_TrypeId1");

            entity.HasIndex(e => new { e.Type, e.Id2, e.Id1 }, "IX_XRef_TypeId2");

            entity.Property(e => e.DecimalData1).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData2).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData3).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData4).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData5).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData6).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData7).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.DecimalData8).HasColumnType("decimal(13, 5)");
            entity.Property(e => e.Id1)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Id2)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.StringData1).HasDefaultValue("");
            entity.Property(e => e.StringData2).HasDefaultValue("");
            entity.Property(e => e.StringData3).HasDefaultValue("");
            entity.Property(e => e.StringData4).HasDefaultValue("");
            entity.Property(e => e.StringData5).HasDefaultValue("");
            entity.Property(e => e.StringData6).HasDefaultValue("");
            entity.Property(e => e.StringData7).HasDefaultValue("");
            entity.Property(e => e.StringData8).HasDefaultValue("");
        });

        modelBuilder.Entity<Zone>(entity =>
        {
            entity.ToTable(tb => tb.HasTrigger("Trigger_OnDeleteZone"));

            entity.HasIndex(e => e.Abbreviation, "IX_Zones_ByAbbreviation");

            entity.HasIndex(e => e.ZoneName, "IX_Zones_ZoneName");

            entity.Property(e => e.Abbreviation)
                .HasMaxLength(20)
                .HasDefaultValue("");
            entity.Property(e => e.ZoneName)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
