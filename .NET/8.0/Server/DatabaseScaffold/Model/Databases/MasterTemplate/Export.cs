﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Export
{
    public long Id { get; set; }

    public DateTime Exported { get; set; }

    public string Code { get; set; } = null!;

    public short DataType { get; set; }

    public string String1 { get; set; } = null!;

    public string String2 { get; set; } = null!;

    public string String3 { get; set; } = null!;

    public string String4 { get; set; } = null!;

    public string String5 { get; set; } = null!;

    public bool Bool1 { get; set; }

    public bool Bool2 { get; set; }

    public bool Bool3 { get; set; }

    public bool Bool4 { get; set; }

    public bool Bool5 { get; set; }

    public long Int1 { get; set; }

    public long Int2 { get; set; }

    public long Int3 { get; set; }

    public long Int4 { get; set; }

    public long Int5 { get; set; }
}
