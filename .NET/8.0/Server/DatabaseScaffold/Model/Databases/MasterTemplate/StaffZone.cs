﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class StaffZone
{
    public byte Type { get; set; }

    public string StaffId { get; set; } = null!;

    public string ZoneName { get; set; } = null!;
}
