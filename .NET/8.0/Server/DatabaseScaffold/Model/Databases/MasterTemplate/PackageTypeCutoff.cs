﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class PackageTypeCutoff
{
    public int PackageTypeId { get; set; }

    public decimal CutoffAmount { get; set; }

    public int ToPackageType { get; set; }

    public byte CutoffType { get; set; }
}
