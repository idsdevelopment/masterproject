﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class ResellerCustomerCompany
{
    public string CustomerCode { get; set; } = null!;

    public long CompanyId { get; set; }
}
