﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class ServiceLevelPackageType
{
    public int ServiceLevelId { get; set; }

    public int PackageTypeId { get; set; }
}
