﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class ChargeDetail
{
    public string ChargeId { get; set; } = null!;

    public string Label { get; set; } = null!;

    public string AccountId { get; set; } = null!;

    public short SortIndex { get; set; }

    public bool IsText { get; set; }

    public decimal Value { get; set; }

    public string Text { get; set; } = null!;

    public bool AlwaysApplied { get; set; }

    public short ChargeType { get; set; }

    public short DisplayType { get; set; }

    public bool DisplayOnWaybill { get; set; }

    public bool DisplayOnInvoice { get; set; }

    public bool DisplayOnEntry { get; set; }

    public bool DisplayOnDriversScreen { get; set; }

    public bool IsWeightCharge { get; set; }

    public bool IsVolumeCharge { get; set; }

    public bool IsMultiplier { get; set; }

    public bool IsDiscountable { get; set; }

    public bool IsFormula { get; set; }

    public bool IsTax { get; set; }

    public decimal Min { get; set; }

    public decimal Max { get; set; }

    public string Formula { get; set; } = null!;

    public bool ExcludeFromTaxes { get; set; }

    public bool IsFuelSurcharge { get; set; }

    public bool ExcludeFromFuelSurcharge { get; set; }

    public bool IsOverride { get; set; }

    public string OverridenPackage { get; set; } = null!;

    public bool ExcludeFromPayroll { get; set; }
}
