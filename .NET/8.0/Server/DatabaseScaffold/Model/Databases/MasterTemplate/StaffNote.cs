﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class StaffNote
{
    public byte Type { get; set; }

    public string StaffId { get; set; } = null!;

    public string NoteName { get; set; } = null!;

    public string Note { get; set; } = null!;
}
