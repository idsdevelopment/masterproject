﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Staff
{
    public int Id { get; set; }

    public byte Type { get; set; }

    public string StaffId { get; set; } = null!;

    public long AddressId { get; set; }

    public bool Enabled { get; set; }

    public string LastName { get; set; } = null!;

    public string FirstName { get; set; } = null!;

    public string MiddleNames { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string TaxNumber { get; set; } = null!;

    public decimal TotalCommission { get; set; }

    public string CommissionFormula { get; set; } = null!;

    public bool HasMessageTopic { get; set; }

    public string EmergencyLastName { get; set; } = null!;

    public string EmergencyFirstName { get; set; } = null!;

    public string EmergencyPhone { get; set; } = null!;

    public string EmergencyMobile { get; set; } = null!;

    public string EmergencyEmail { get; set; } = null!;

    public decimal MainCommission1 { get; set; }

    public decimal MainCommission2 { get; set; }

    public decimal MainCommission3 { get; set; }

    public decimal OverrideCommission1 { get; set; }

    public decimal OverrideCommission2 { get; set; }

    public decimal OverrideCommission3 { get; set; }

    public decimal OverrideCommission4 { get; set; }

    public decimal OverrideCommission5 { get; set; }

    public decimal OverrideCommission6 { get; set; }

    public decimal OverrideCommission7 { get; set; }

    public decimal OverrideCommission8 { get; set; }

    public decimal OverrideCommission9 { get; set; }

    public decimal OverrideCommission10 { get; set; }

    public decimal OverrideCommission11 { get; set; }

    public decimal OverrideCommission12 { get; set; }
}
