﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class CompanyAddressGroupName
{
    public int GroupNumber { get; set; }

    public string Description { get; set; } = null!;
}
