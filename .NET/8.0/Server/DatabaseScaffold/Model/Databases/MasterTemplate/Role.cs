﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Role
{
    public int Id { get; set; }

    public string RoleName { get; set; } = null!;

    public bool Mandatory { get; set; }

    public bool IsAdministrator { get; set; }

    public bool IsDriver { get; set; }

    public bool IsWebService { get; set; }

    public string JsonObject { get; set; } = null!;
}
