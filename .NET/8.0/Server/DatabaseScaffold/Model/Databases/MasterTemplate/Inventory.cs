﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Inventory
{
    public string InventoryCode { get; set; } = null!;

    public string Barcode { get; set; } = null!;

    public bool Active { get; set; }

    public bool IsContainer { get; set; }

    public string ShortDescription { get; set; } = null!;

    public string LongDescription { get; set; } = null!;

    public decimal UnitVolume { get; set; }

    public decimal UnitWeight { get; set; }

    public bool DangerousGoods { get; set; }

    public string UNClass { get; set; } = null!;

    public decimal PackageQuantity { get; set; }

    public string PackageType { get; set; } = null!;

    public string BinX { get; set; } = null!;

    public string BinY { get; set; } = null!;

    public string BinZ { get; set; } = null!;

    public string StringData1 { get; set; } = null!;

    public string StringData2 { get; set; } = null!;

    public string StringData3 { get; set; } = null!;

    public string StringData4 { get; set; } = null!;

    public long IntData1 { get; set; }

    public long IntData2 { get; set; }

    public long IntData3 { get; set; }

    public long IntData4 { get; set; }

    public bool BitData1 { get; set; }

    public bool BitData2 { get; set; }

    public bool BitData3 { get; set; }

    public bool BitData4 { get; set; }

    public bool BitData5 { get; set; }

    public bool BitData6 { get; set; }

    public bool BitData7 { get; set; }

    public bool BitData8 { get; set; }

    public decimal DecimalData1 { get; set; }

    public decimal DecimalData2 { get; set; }

    public decimal DecimalData3 { get; set; }

    public decimal DecimalData4 { get; set; }
}
