﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Invoice
{
    public bool IsCreditNote { get; set; }

    public long InvoiceNumber { get; set; }

    public DateTimeOffset InvoiceDateTime { get; set; }

    public long BillingCompanyId { get; set; }

    public string BillingCompany { get; set; } = null!;

    public decimal TaxableValue { get; set; }

    public decimal NonTaxableValue { get; set; }

    public decimal TotalCharges { get; set; }

    public decimal TotalTaxA { get; set; }

    public decimal TotalTaxB { get; set; }

    public decimal TotalValue { get; set; }

    public decimal TotalDiscount { get; set; }

    public string TripIdsAsJson { get; set; } = null!;

    public string TripChargesAsJson { get; set; } = null!;

    public decimal Residual { get; set; }

    public short BillingPeriod { get; set; }

    public bool EmailInvoice { get; set; }

    public string EmailAddress { get; set; } = null!;

    public string EmailSubject { get; set; } = null!;

    public DateTimeOffset InvoiceFromDateTime { get; set; }

    public DateTimeOffset InvoiceToDateTime { get; set; }
}
