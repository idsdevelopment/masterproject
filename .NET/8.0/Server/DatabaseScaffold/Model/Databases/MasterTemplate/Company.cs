﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Company
{
    public long CompanyId { get; set; }

    public string CompanyName { get; set; } = null!;

    public string DisplayCompanyName { get; set; } = null!;

    public string CompanyNumber { get; set; } = null!;

    public string LocationBarcode { get; set; } = null!;

    public bool AllowToBeGlobal { get; set; }

    public bool Enabled { get; set; }

    public bool IsTransient { get; set; }

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public long PrimaryAddressId { get; set; }

    public DateTime LastModified { get; set; }

    public DateTimeOffset LastBilled { get; set; }

    public string BillToCompany { get; set; } = null!;

    public short BillingPeriod { get; set; }

    public bool MultipleTripsOnInvoice { get; set; }

    public bool EmailInvoice { get; set; }

    public string InvoiceOverride { get; set; } = null!;

    public decimal CreditLimit { get; set; }
}
