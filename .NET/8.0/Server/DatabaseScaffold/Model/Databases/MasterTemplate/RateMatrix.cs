﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class RateMatrix
{
    public int FromZoneId { get; set; }

    public int ToZoneId { get; set; }

    public int ServiceLevelId { get; set; }

    public int PackageTypeId { get; set; }

    public int VehicleTypeId { get; set; }

    public decimal Value { get; set; }

    public string Formula { get; set; } = null!;

    public bool IsOverride { get; set; }

    public long CompanyId { get; set; }

    public byte OverrideType { get; set; }
}
