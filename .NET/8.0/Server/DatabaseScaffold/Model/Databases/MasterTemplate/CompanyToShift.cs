﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class CompanyToShift
{
    public int Id { get; set; }

    public long CompanyId { get; set; }

    public long ShiftId { get; set; }
}
