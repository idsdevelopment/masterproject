﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class Zone
{
    public int Id { get; set; }

    public string ZoneName { get; set; } = null!;

    public string Abbreviation { get; set; } = null!;

    public int SortIndex { get; set; }
}
