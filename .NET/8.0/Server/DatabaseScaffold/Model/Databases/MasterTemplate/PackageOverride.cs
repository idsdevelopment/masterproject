﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class PackageOverride
{
    public int Id { get; set; }

    public long CompanyId { get; set; }

    public int FromPackageTypeId { get; set; }

    public int ToPackageTypeId { get; set; }

    public short SortIndex { get; set; }
}
