﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class StaffRole
{
    public int Id { get; set; }

    public byte Type { get; set; }

    public string StaffId { get; set; } = null!;

    public int RoleId { get; set; }

    public byte[] ConcurrencyToken { get; set; } = null!;
}
