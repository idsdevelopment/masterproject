﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class StaffShift
{
    public int Id { get; set; }

    public string Shift { get; set; } = null!;

    public short StartDay { get; set; }

    public TimeOnly Start { get; set; }

    public short EndDay { get; set; }

    public TimeOnly End { get; set; }

    public string Formula { get; set; } = null!;

    public int ServiceLevelId { get; set; }
}
