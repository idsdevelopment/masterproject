﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class ServiceLevel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public long Background_ARGB { get; set; }

    public long Foreground_ARGB { get; set; }

    public bool Monday { get; set; }

    public bool Tuesday { get; set; }

    public bool Wednesday { get; set; }

    public bool Thursday { get; set; }

    public bool Friday { get; set; }

    public bool Saturday { get; set; }

    public bool Sunday { get; set; }

    public TimeOnly StartTime { get; set; }

    public TimeOnly EndTime { get; set; }

    public TimeOnly DeliveryHours { get; set; }

    public TimeOnly WarningHours { get; set; }

    public TimeOnly CriticalHours { get; set; }

    public TimeOnly BeyondHours { get; set; }

    public short SortOrder { get; set; }
}
