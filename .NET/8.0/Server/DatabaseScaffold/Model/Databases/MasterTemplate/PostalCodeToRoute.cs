﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class PostalCodeToRoute
{
    public long RouteId { get; set; }

    public string PostalCode { get; set; } = null!;

    public string Zone1 { get; set; } = null!;

    public string Zone2 { get; set; } = null!;

    public string Zone3 { get; set; } = null!;

    public string Zone4 { get; set; } = null!;
}
