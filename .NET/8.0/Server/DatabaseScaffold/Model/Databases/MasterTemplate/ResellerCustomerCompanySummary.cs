﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class ResellerCustomerCompanySummary
{
    public long CompanyId { get; set; }

    public long AddressId { get; set; }

    public bool Enabled { get; set; }

    public string CustomerCode { get; set; } = null!;

    public string CompanyName { get; set; } = null!;

    public string LocationBarcode { get; set; } = null!;

    public string DisplayCompanyName { get; set; } = null!;

    public string Suite { get; set; } = null!;

    public string AddressLine1 { get; set; } = null!;

    public string AddressLine2 { get; set; } = null!;

    public string City { get; set; } = null!;

    public string Region { get; set; } = null!;

    public string PostalCode { get; set; } = null!;

    public string CountryCode { get; set; } = null!;
}
