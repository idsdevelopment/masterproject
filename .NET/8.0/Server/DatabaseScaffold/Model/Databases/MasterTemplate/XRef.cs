﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.MasterTemplate;

public partial class XRef
{
    public long Id { get; set; }

    public short Type { get; set; }

    public string Id1 { get; set; } = null!;

    public string Id2 { get; set; } = null!;

    public string StringData1 { get; set; } = null!;

    public string StringData2 { get; set; } = null!;

    public string StringData3 { get; set; } = null!;

    public string StringData4 { get; set; } = null!;

    public string StringData5 { get; set; } = null!;

    public string StringData6 { get; set; } = null!;

    public string StringData7 { get; set; } = null!;

    public string StringData8 { get; set; } = null!;

    public long IntData1 { get; set; }

    public long IntData2 { get; set; }

    public long IntData3 { get; set; }

    public long IntData4 { get; set; }

    public bool BitData1 { get; set; }

    public bool BitData2 { get; set; }

    public bool BitData3 { get; set; }

    public bool BitData4 { get; set; }

    public bool BitData5 { get; set; }

    public bool BitData6 { get; set; }

    public bool BitData7 { get; set; }

    public bool BitData8 { get; set; }

    public decimal DecimalData1 { get; set; }

    public decimal DecimalData2 { get; set; }

    public decimal DecimalData3 { get; set; }

    public decimal DecimalData4 { get; set; }

    public decimal DecimalData5 { get; set; }

    public decimal DecimalData6 { get; set; }

    public decimal DecimalData7 { get; set; }

    public decimal DecimalData8 { get; set; }
}
