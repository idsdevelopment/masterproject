﻿namespace Database.Model.Databases.MasterTemplate;

public partial class Address
{
	protected bool Equals( Address other ) => this == other;

	public override bool Equals( object? obj )
	{
		if( ReferenceEquals( null, obj ) )
			return false;

		if( ReferenceEquals( this, obj ) )
			return true;

		return ( obj.GetType() == GetType() ) && Equals( (Address)obj );
	}

	public override int GetHashCode()
	{
		var HashCode = new HashCode();
		HashCode.Add( Random.Shared.NextInt64() );
		return HashCode.ToHashCode();
	}

	public static bool operator ==( Address? a1, Address? a2 )
	{
		if( a1 is null && a2 is null ) // Cast to object to stop recursion and stack overflow
			return true;

		return a1 is not null && a2 is not null && ( a1.SecondaryId == a2.SecondaryId )
		       && ( a1.AddressLine1 == a2.AddressLine1 )
		       && ( a1.AddressLine2 == a2.AddressLine2 )
		       && ( a1.Barcode == a2.Barcode )
		       && ( a1.City == a2.City )
		       && ( a1.CountryCode == a2.CountryCode )
		       && ( a1.Contact == a2.Contact )
		       && ( a1.EmailAddress == a2.EmailAddress )
		       && ( a1.EmailAddress1 == a2.EmailAddress1 )
		       && ( a1.EmailAddress2 == a2.EmailAddress2 )
		       && ( a1.Fax == a2.Fax )
		       && ( a1.Latitude == a2.Latitude )
		       && ( a1.Longitude == a2.Longitude )
		       && ( a1.Mobile == a2.Mobile )
		       && ( a1.Mobile1 == a2.Mobile1 )
		       && ( a1.Notes == a2.Notes )
		       && ( a1.Phone == a2.Phone )
		       && ( a1.Phone1 == a2.Phone1 )
		       && ( a1.PostalBarcode == a2.PostalBarcode )
		       && ( a1.PostalCode == a2.PostalCode )
		       && ( a1.Region == a2.Region )
		       && ( a1.Suite == a2.Suite )
		       && ( a1.Vicinity == a2.Vicinity )
		       && ( a1.ZoneId == a2.ZoneId );
	}

	public static bool operator !=( Address a1, Address a2 ) => !( a1 == a2 );

	public static explicit operator Protocol.Data.Address( Address? a ) => a is not null ? new Protocol.Data.Address
	                                                                                       {
		                                                                                       AddressLine1  = a.AddressLine1,
		                                                                                       AddressLine2  = a.AddressLine2,
		                                                                                       Barcode       = a.Barcode,
		                                                                                       City          = a.City,
		                                                                                       Country       = a.Country,
		                                                                                       CountryCode   = a.CountryCode,
		                                                                                       ContactName   = a.Contact,
		                                                                                       EmailAddress  = a.EmailAddress,
		                                                                                       EmailAddress1 = a.EmailAddress1,
		                                                                                       EmailAddress2 = a.EmailAddress2,
		                                                                                       Fax           = a.Fax,
		                                                                                       Latitude      = a.Latitude,
		                                                                                       Longitude     = a.Longitude,
		                                                                                       Mobile        = a.Mobile,
		                                                                                       Mobile1       = a.Mobile1,
		                                                                                       Notes         = a.Notes,
		                                                                                       Phone         = a.Phone,
		                                                                                       Phone1        = a.Phone1,
		                                                                                       PostalBarcode = a.PostalBarcode,
		                                                                                       PostalCode    = a.PostalCode.TrimMax20(),
		                                                                                       Region        = a.Region,
		                                                                                       SecondaryId   = a.SecondaryId,
		                                                                                       Suite         = a.Suite,
		                                                                                       Vicinity      = a.Vicinity,
		                                                                                       ZoneName      = ""
	                                                                                       }
		                                                                       : new Protocol.Data.Address();
}