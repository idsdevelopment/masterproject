﻿namespace Database.Model.Databases.MasterTemplate;

public partial class XRef
{
	public enum TYPE : short
	{
		UNKNOWN                         = -1,
		TRIP                            = 0,
		PML_AURT                        = 1,
		PML_SALESFORCE                  = 2,
		ACCOUNT_ID_TO_BARCODE           = 3,
		PML_CARTON_TO_PACKETS           = 4,
		SECONDARY_ACCOUNT_ID_TO_BARCODE = 5,
	}

	public XRef AsTrip( string azureId, string ids1Id, string barcode, string barcode1, string itemCode, string description, string reference, decimal pieces, decimal weight )
	{
		Type = (short)TYPE.TRIP;
		Id1  = azureId;
		Id2  = ids1Id;

		StringData1 = barcode.Trim();
		StringData2 = itemCode.Trim();
		StringData3 = description.Trim();
		StringData4 = reference.Trim();
		StringData5 = barcode1.Trim();

		StringData6 = StringData7 = StringData8 = "";

		DecimalData1 = pieces;
		DecimalData2 = weight;

		return this;
	}


	public XRef( TYPE type )
	{
		Type         = (short)type;
		Id1          = Id2          = "";
		StringData1  = StringData2  = StringData3  = StringData4 = StringData4 = StringData4 = StringData4 = StringData4 = "";
		IntData1     = IntData2     = IntData3     = IntData4;
		DecimalData1 = DecimalData2 = DecimalData3 = DecimalData4 = DecimalData5 = DecimalData6 = DecimalData7 = DecimalData8 = 0;
		BitData1     = BitData2     = BitData3     = BitData4     = BitData5     = BitData6     = BitData7     = BitData8     = false;
	}

	public XRef() : this( TYPE.UNKNOWN )
	{
	}
}