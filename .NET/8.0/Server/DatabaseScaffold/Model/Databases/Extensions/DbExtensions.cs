﻿public static class DbExtensions
{
	public static string Max20( this string txt ) => txt.MaxLength( 20 );
	public static string Max50( this string txt ) => txt.MaxLength( 50 );
	public static string Max100( this string txt ) => txt.MaxLength( 100 );
	public static string Max255( this string txt ) => txt.MaxLength( 255 );

	public static string TrimToUpperMax( this string txt, int maxLen ) => txt.Trim().MaxLength( maxLen ).ToUpper();
	public static string TrimToUpperMax100( this string txt ) => txt.TrimToUpperMax( 100 );
	public static string TrimToUpperMax50( this string txt ) => txt.TrimToUpperMax( 50 );

	public static string Max900Bytes( this string txt ) // Max Db Index Size
	{
		const int MAX_SIZE = 900 - 4; // Allow some for string length in Db
		var       Bytes    = Encoding.Unicode.GetBytes( txt );

		if( Bytes.Length > MAX_SIZE )
		{
			Array.Resize( ref Bytes, MAX_SIZE );
			txt = Encoding.Unicode.GetString( Bytes );
		}
		return txt;
	}

	public static string TrimMax20( this string txt ) => txt.NullTrim().MaxLength( 20 );
	public static string TrimMax50( this string txt ) => txt.NullTrim().MaxLength( 50 );
	public static string TrimMax100( this string txt ) => txt.NullTrim().MaxLength( 100 );
	public static string TrimMax255( this string txt ) => txt.NullTrim().MaxLength( 255 );

	public static decimal Limit( this decimal val, uint max )
	{
		if( Math.Abs( val ) > max )
			return val < 0 ? -max : max;
		return val;
	}

	public static decimal Max180( this decimal val ) => val.Limit( 180 );
	public static decimal Max90( this decimal val ) => val.Limit( 90 );
}