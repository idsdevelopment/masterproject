﻿#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

using Microsoft.EntityFrameworkCore;

namespace Database.Model.Databases
{
	namespace MasterTemplate
	{
		public partial class MasterTemplateContext : DbContext
		{
			public MasterTemplateContext()
			{
			}

			public MasterTemplateContext( DbContextOptions options ) : base( options )
			{
			}
		}
	}

	namespace Users
	{
		public partial class UsersContext : DbContext
		{
			public UsersContext()
			{
			}

			public UsersContext( DbContextOptions options ) : base( options )
			{
			}
		}
	}
}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.