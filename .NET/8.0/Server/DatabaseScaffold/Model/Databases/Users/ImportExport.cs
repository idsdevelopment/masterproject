﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class ImportExport
{
    public string CarrierId { get; set; } = null!;

    public bool ImportEnabled { get; set; }

    public bool ExportEnabled { get; set; }

    public DateTime LastRun { get; set; }

    public DateTime NextRun { get; set; }

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;
}
