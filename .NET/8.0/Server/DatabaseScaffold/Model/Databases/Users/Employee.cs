﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Employee
{
    public string Code { get; set; } = null!;

    public string Password { get; set; } = null!;

    public bool AllowCreation { get; set; }
}
