﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Schedule
{
    public long Id { get; set; }

    public byte TriggerType { get; set; }

    public string CarrierId { get; set; } = null!;

    public string ClientTimeZoneId { get; set; } = null!;

    public DateTimeOffset NextRun { get; set; }

    public string TripId { get; set; } = null!;

    public string Driver { get; set; } = null!;

    public byte Hour { get; set; }

    public byte Minute { get; set; }

    public bool January { get; set; }

    public bool February { get; set; }

    public bool March { get; set; }

    public bool April { get; set; }

    public bool May { get; set; }

    public bool June { get; set; }

    public bool July { get; set; }

    public bool August { get; set; }

    public bool September { get; set; }

    public bool October { get; set; }

    public bool November { get; set; }

    public bool December { get; set; }

    public bool Monday1 { get; set; }

    public bool Monday2 { get; set; }

    public bool Monday3 { get; set; }

    public bool Monday4 { get; set; }

    public bool Monday5 { get; set; }

    public bool Tuesday1 { get; set; }

    public bool Tuesday2 { get; set; }

    public bool Tuesday3 { get; set; }

    public bool Tuesday4 { get; set; }

    public bool Tuesday5 { get; set; }

    public bool Wednesday1 { get; set; }

    public bool Wednesday2 { get; set; }

    public bool Wednesday3 { get; set; }

    public bool Wednesday4 { get; set; }

    public bool Wednesday5 { get; set; }

    public bool Thursday1 { get; set; }

    public bool Thursday2 { get; set; }

    public bool Thursday3 { get; set; }

    public bool Thursday4 { get; set; }

    public bool Thursday5 { get; set; }

    public bool Friday1 { get; set; }

    public bool Friday2 { get; set; }

    public bool Friday3 { get; set; }

    public bool Friday4 { get; set; }

    public bool Friday5 { get; set; }

    public bool Saturday1 { get; set; }

    public bool Saturday2 { get; set; }

    public bool Saturday3 { get; set; }

    public bool Saturday4 { get; set; }

    public bool Saturday5 { get; set; }

    public bool Sunday1 { get; set; }

    public bool Sunday2 { get; set; }

    public bool Sunday3 { get; set; }

    public bool Sunday4 { get; set; }

    public bool Sunday5 { get; set; }
}
