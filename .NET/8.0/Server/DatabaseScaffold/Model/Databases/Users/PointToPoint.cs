﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class PointToPoint
{
    public byte Mode { get; set; }

    public decimal FromLatitude { get; set; }

    public decimal FromLongitude { get; set; }

    public decimal ToLatitude { get; set; }

    public decimal ToLongitude { get; set; }

    public DateTime DateCreated { get; set; }

    public decimal DistanceInMeters { get; set; }

    public string LegsAsJson { get; set; } = null!;
}
