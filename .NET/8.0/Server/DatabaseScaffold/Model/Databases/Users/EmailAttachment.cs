﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class EmailAttachment
{
    public long Id { get; set; }

    public long MessageId { get; set; }

    public string FileName { get; set; } = null!;

    public string MediaType { get; set; } = null!;

    public byte[] Attachment { get; set; } = null!;
}
