﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Database.Model.Databases.Users;

public partial class UsersContext : DbContext
{
    public UsersContext(DbContextOptions<UsersContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Billing> Billings { get; set; }

    public virtual DbSet<CarrierSetting> CarrierSettings { get; set; }

    public virtual DbSet<CarrierToken> CarrierTokens { get; set; }

    public virtual DbSet<Configuration> Configurations { get; set; }

    public virtual DbSet<Email> Emails { get; set; }

    public virtual DbSet<EmailAttachment> EmailAttachments { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<EndPoint> EndPoints { get; set; }

    public virtual DbSet<ImportExport> ImportExports { get; set; }

    public virtual DbSet<InventoryBarcode> InventoryBarcodes { get; set; }

    public virtual DbSet<MapAddress> MapAddresses { get; set; }

    public virtual DbSet<PointToPoint> PointToPoints { get; set; }

    public virtual DbSet<Preference> Preferences { get; set; }

    public virtual DbSet<Process> Processes { get; set; }

    public virtual DbSet<ResellerCustomer> ResellerCustomers { get; set; }

    public virtual DbSet<Schedule> Schedules { get; set; }

    public virtual DbSet<SentEmail> SentEmails { get; set; }

    public virtual DbSet<__TransactionHistory> __TransactionHistories { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Billing>(entity =>
        {
            entity.HasKey(e => e.CarrierId).HasName("PK__Billing__CB820559D9B46A38");

            entity.ToTable("Billing", tb => tb.HasTrigger("Trigger_Billing"));

            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.DefaultTokenValue).HasColumnType("decimal(18, 6)");
        });

        modelBuilder.Entity<CarrierSetting>(entity =>
        {
            entity.HasKey(e => new { e.CarrierId, e.PrimaryId, e.SecondaryId }).HasName("PK_Table");

            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Decimal1).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Decimal2).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Decimal3).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Decimal4).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Decimal5).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.String1).HasDefaultValue("");
            entity.Property(e => e.String2).HasDefaultValue("");
            entity.Property(e => e.String3).HasDefaultValue("");
            entity.Property(e => e.String4).HasDefaultValue("");
            entity.Property(e => e.String5).HasDefaultValue("");
        });

        modelBuilder.Entity<CarrierToken>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC07836E6BC9");

            entity.HasIndex(e => new { e.CarrierId, e.Expires, e.Id }, "IX_Table_ByCarrierIdExpiresId");

            entity.Property(e => e.Id).ValueGeneratedNever();
            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Expires).HasDefaultValueSql("(getutcdate())");
            entity.Property(e => e.SingleTokenValue).HasColumnType("decimal(18, 6)");
        });

        modelBuilder.Entity<Configuration>(entity =>
        {
            entity.ToTable("Configuration");

            entity.Property(e => e.AddressCacheCleanUpIntervalInMinutes).HasDefaultValue(720);
            entity.Property(e => e.AzureMapsApiKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.BarcodeLookupUrl)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.BingMapsApiKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.BlobStorageAccount)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.BlobStoragePrimaryKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.BlobStorageSecondaryKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.DatabaseEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.DatabaseServerName)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.DefaultSingleTokenValue).HasColumnType("decimal(18, 6)");
            entity.Property(e => e.DynamsoftBarcodeKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.GoogleMapsApiKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.LastAddressCleanup)
                .HasDefaultValueSql("((0))")
                .HasColumnType("smalldatetime");
            entity.Property(e => e.PayPalClientId)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.PayPalClientSecret)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.SendGridKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.SendGridUsername)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.ServiceBusPrimaryConnectionString)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.ServiceBusSecondaryConnectionString)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.SessionExpiresTimeoutMinutes).HasDefaultValue((short)60);
        });

        modelBuilder.Entity<Email>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC07CE934F46");

            entity.ToTable(tb => tb.HasTrigger("Trigger_Emails"));

            entity.HasIndex(e => new { e.CarrierId, e.Id }, "IX_Emails_CarrierId");

            entity.HasIndex(e => e.SleepUntil, "IX_Emails_SleepUntil");

            entity.Property(e => e.BCC).HasDefaultValue("");
            entity.Property(e => e.Body).HasDefaultValue("");
            entity.Property(e => e.CC).HasDefaultValue("");
            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Created).HasDefaultValueSql("(getutcdate())");
            entity.Property(e => e.From)
                .HasMaxLength(1024)
                .HasDefaultValue("");
            entity.Property(e => e.SleepUntil).HasDefaultValueSql("(getutcdate())");
            entity.Property(e => e.Subject)
                .HasMaxLength(1024)
                .HasDefaultValue("");
            entity.Property(e => e.To)
                .HasMaxLength(1024)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<EmailAttachment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC077237B167");

            entity.HasIndex(e => e.MessageId, "IX_EmailAttachments_MessageId");

            entity.Property(e => e.FileName).HasMaxLength(255);
            entity.Property(e => e.MediaType).HasMaxLength(255);
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.Code).HasName("PK__Table__A25C5AA689023BF5");

            entity.ToTable("Employee");

            entity.Property(e => e.Code).HasMaxLength(50);
            entity.Property(e => e.Password)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<EndPoint>(entity =>
        {
            entity.HasKey(e => e.CarrierId).HasName("PK__tmp_ms_x__CB820559D1EACBB7");

            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageAccount)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageAccountKey)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageBlobEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageFileEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
            entity.Property(e => e.StorageTableEndpoint)
                .IsUnicode(false)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<ImportExport>(entity =>
        {
            entity.HasKey(e => e.CarrierId).HasName("PK__tmp_ms_x__CB8205596D6A6FE1");

            entity.ToTable("ImportExport");

            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.LastRun)
                .HasDefaultValue(new DateTime(1900, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                .HasColumnType("smalldatetime");
            entity.Property(e => e.NextRun)
                .HasDefaultValue(new DateTime(1900, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                .HasColumnType("smalldatetime");
            entity.Property(e => e.Password)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.UserName)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<InventoryBarcode>(entity =>
        {
            entity.HasKey(e => e.Barcode).HasName("PK__tmp_ms_x__177800D2A10E1F70");

            entity.Property(e => e.Barcode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Owner)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<MapAddress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC07747D8D8D");

            entity.HasIndex(e => new { e.AddressLine1, e.City, e.Region, e.Country }, "IX_MapAddresses_Adr1City");

            entity.HasIndex(e => e.Expires, "IX_MapAddresses_Expires");

            entity.HasIndex(e => new { e.Latitude, e.Longitude }, "IX_MapAddresses_LatLong");

            entity.Property(e => e.AddressLine1)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.AddressLine2)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.City)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Country)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Expires).HasDefaultValueSql("(getutcdate())");
            entity.Property(e => e.Latitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.Longitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.PostalCode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Region)
                .HasMaxLength(100)
                .HasDefaultValue("");
            entity.Property(e => e.RegionCode)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<PointToPoint>(entity =>
        {
            entity.HasKey(e => new { e.Mode, e.FromLatitude, e.FromLongitude, e.ToLatitude, e.ToLongitude });

            entity.ToTable("PointToPoint");

            entity.HasIndex(e => e.DateCreated, "IX_Distances_DateCreated");

            entity.HasIndex(e => new { e.FromLatitude, e.FromLongitude, e.ToLatitude, e.ToLongitude }, "nci_wi_PointToPoint_23F83CC7D601A9720975B545D9151F87");

            entity.Property(e => e.FromLatitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.FromLongitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.ToLatitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.ToLongitude).HasColumnType("decimal(12, 6)");
            entity.Property(e => e.DateCreated).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.DistanceInMeters).HasColumnType("decimal(12, 1)");
            entity.Property(e => e.LegsAsJson).HasDefaultValue("");
        });

        modelBuilder.Entity<Preference>(entity =>
        {
            entity.HasKey(e => new { e.IdsOnly, e.DisplayOrder, e.SubDisplayOrder, e.Description, e.Id });

            entity.HasIndex(e => e.DevicePreference, "IX_Preferences_DevicePreference");

            entity.HasIndex(e => e.Id, "IX_Preferences_Id");

            entity.Property(e => e.Description).HasMaxLength(100);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.DefaultStringValue).HasDefaultValue("");
            entity.Property(e => e.DevicePreference)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.ResellerId)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Process>(entity =>
        {
            entity.HasKey(e => e.ProcessId).HasName("PK__tmp_ms_x__1B39A9560B3CDB9F");

            entity.HasIndex(e => e.TimeStarted, "IX_Processes_ByTimeStarted");

            entity.Property(e => e.ResponseString).HasDefaultValue("");
        });

        modelBuilder.Entity<ResellerCustomer>(entity =>
        {
            entity.HasIndex(e => e.LoginCode, "IX_ResellerCustomers_LoginCode").IsUnique();

            entity.HasIndex(e => e.OriginalLoginCode, "IX_ResellerCustomers_OriginalLoginCode");

            entity.HasIndex(e => new { e.ResellerId, e.CustomerCode }, "IX_ResellerCustomers_ResellerId_CustomerCode").IsUnique();

            entity.HasIndex(e => new { e.ResellerId, e.LoginCode }, "IX_ResellerCustomers_ResellerId_LoginCode");

            entity.Property(e => e.CustomerCode)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.LoginCode).HasMaxLength(50);
            entity.Property(e => e.OriginalLoginCode)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasDefaultValue("");
            entity.Property(e => e.ResellerId).HasMaxLength(50);
            entity.Property(e => e.UserName)
                .HasMaxLength(255)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<Schedule>(entity =>
        {
            entity.HasIndex(e => new { e.CarrierId, e.TriggerType, e.NextRun, e.Id }, "IX_Schedules_NextRun").IsUnique();

            entity.HasIndex(e => new { e.CarrierId, e.TripId }, "IX_Schedules_TripId");

            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.ClientTimeZoneId)
                .HasMaxLength(50)
                .HasDefaultValue("Pacific Standard Time");
            entity.Property(e => e.Driver)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.NextRun).HasDefaultValueSql("(getdate())");
            entity.Property(e => e.TripId)
                .HasMaxLength(50)
                .HasDefaultValue("");
        });

        modelBuilder.Entity<SentEmail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__tmp_ms_x__3214EC07B61A1055");

            entity.HasIndex(e => new { e.CarrierId, e.Timestamp }, "IX_SentEmails_CarrierId");

            entity.HasIndex(e => new { e.Failed, e.EmailId }, "IX_SentEmails_Failed");

            entity.HasIndex(e => e.Timestamp, "IX_SentEmails_Timestamp");

            entity.Property(e => e.Bcc).HasDefaultValue("");
            entity.Property(e => e.CarrierId)
                .HasMaxLength(50)
                .HasDefaultValue("");
            entity.Property(e => e.Cc).HasDefaultValue("");
            entity.Property(e => e.From).HasDefaultValue("");
            entity.Property(e => e.Subject).HasDefaultValue("");
            entity.Property(e => e.Timestamp).HasDefaultValue(new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
            entity.Property(e => e.To).HasDefaultValue("");
        });

        modelBuilder.Entity<__TransactionHistory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.__TransactionHistory");

            entity.ToTable("__TransactionHistory");

            entity.Property(e => e.Id).ValueGeneratedNever();
            entity.Property(e => e.CreationTime).HasColumnType("datetime");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
