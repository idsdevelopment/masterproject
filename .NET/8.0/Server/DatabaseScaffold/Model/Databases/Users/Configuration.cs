﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Configuration
{
    public int Id { get; set; }

    public string SendGridKey { get; set; } = null!;

    public string SendGridUsername { get; set; } = null!;

    public string BlobStorageAccount { get; set; } = null!;

    public string BlobStoragePrimaryKey { get; set; } = null!;

    public string BlobStorageSecondaryKey { get; set; } = null!;

    public short SessionExpiresTimeoutMinutes { get; set; }

    public string DatabaseServerName { get; set; } = null!;

    public string DatabaseEndpoint { get; set; } = null!;

    public string GoogleMapsApiKey { get; set; } = null!;

    public string BingMapsApiKey { get; set; } = null!;

    public string AzureMapsApiKey { get; set; } = null!;

    public int AddressCacheCleanUpIntervalInMinutes { get; set; }

    public DateTime LastAddressCleanup { get; set; }

    public int AverageAccessInterval { get; set; }

    public string ServiceBusPrimaryConnectionString { get; set; } = null!;

    public string ServiceBusSecondaryConnectionString { get; set; } = null!;

    public string BarcodeLookupUrl { get; set; } = null!;

    public decimal DefaultSingleTokenValue { get; set; }

    public int DefaultTokenExpiresInDays { get; set; }

    public string PayPalClientId { get; set; } = null!;

    public string PayPalClientSecret { get; set; } = null!;

    public string DynamsoftBarcodeKey { get; set; } = null!;
}
