﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class MapAddress
{
    public long Id { get; set; }

    public string AddressLine1 { get; set; } = null!;

    public string AddressLine2 { get; set; } = null!;

    public string City { get; set; } = null!;

    public string Region { get; set; } = null!;

    public string RegionCode { get; set; } = null!;

    public string Country { get; set; } = null!;

    public string PostalCode { get; set; } = null!;

    public decimal Latitude { get; set; }

    public decimal Longitude { get; set; }

    public DateOnly Expires { get; set; }
}
