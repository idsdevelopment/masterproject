﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class SentEmail
{
    public long Id { get; set; }

    public bool Failed { get; set; }

    public long EmailId { get; set; }

    public DateTime Timestamp { get; set; }

    public string CarrierId { get; set; } = null!;

    public string Subject { get; set; } = null!;

    public string To { get; set; } = null!;

    public string From { get; set; } = null!;

    public string Cc { get; set; } = null!;

    public string Bcc { get; set; } = null!;

    public short RetryCount { get; set; }
}
