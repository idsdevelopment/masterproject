﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Preference
{
    public string Description { get; set; } = null!;

    public short DataType { get; set; }

    public byte DisplayOrder { get; set; }

    public byte SubDisplayOrder { get; set; }

    public int Id { get; set; }

    public bool IdsOnly { get; set; }

    public string DevicePreference { get; set; } = null!;

    public bool Default { get; set; }

    public short DefaultIntValue { get; set; }

    public string DefaultStringValue { get; set; } = null!;

    public double DefaultDoubleValue { get; set; }

    public string ResellerId { get; set; } = null!;
}
