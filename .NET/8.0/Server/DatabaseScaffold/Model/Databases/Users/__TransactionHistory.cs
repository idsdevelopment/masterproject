﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class __TransactionHistory
{
    public Guid Id { get; set; }

    public DateTime CreationTime { get; set; }
}
