﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Email
{
    public long Id { get; set; }

    public string CarrierId { get; set; } = null!;

    public bool Locked { get; set; }

    public string From { get; set; } = null!;

    public string To { get; set; } = null!;

    public string CC { get; set; } = null!;

    public string BCC { get; set; } = null!;

    public string Subject { get; set; } = null!;

    public string Body { get; set; } = null!;

    public bool IsBodyHtml { get; set; }

    public DateTime Created { get; set; }

    public DateTime SleepUntil { get; set; }
}
