﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class InventoryBarcode
{
    public string Barcode { get; set; } = null!;

    public string Owner { get; set; } = null!;

    public string Description { get; set; } = null!;
}
