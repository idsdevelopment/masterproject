﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class CarrierSetting
{
    public string CarrierId { get; set; } = null!;

    public short PrimaryId { get; set; }

    public short SecondaryId { get; set; }

    public DateTimeOffset Date1 { get; set; }

    public DateTimeOffset Date2 { get; set; }

    public DateTimeOffset Date3 { get; set; }

    public DateTimeOffset Date4 { get; set; }

    public DateTimeOffset Date5 { get; set; }

    public decimal Decimal1 { get; set; }

    public decimal Decimal2 { get; set; }

    public decimal Decimal3 { get; set; }

    public decimal Decimal4 { get; set; }

    public decimal Decimal5 { get; set; }

    public bool Bool1 { get; set; }

    public bool Bool2 { get; set; }

    public bool Bool3 { get; set; }

    public bool Bool4 { get; set; }

    public bool Bool5 { get; set; }

    public string String1 { get; set; } = null!;

    public string String2 { get; set; } = null!;

    public string String3 { get; set; } = null!;

    public string String4 { get; set; } = null!;

    public string String5 { get; set; } = null!;
}
