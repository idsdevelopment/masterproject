﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class EndPoint
{
    public string CarrierId { get; set; } = null!;

    public string StorageAccount { get; set; } = null!;

    public string StorageAccountKey { get; set; } = null!;

    public string StorageTableEndpoint { get; set; } = null!;

    public string StorageFileEndpoint { get; set; } = null!;

    public string StorageBlobEndpoint { get; set; } = null!;
}
