﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Process
{
    public long ProcessId { get; set; }

    public long TimeStarted { get; set; }

    public bool Processing { get; set; }

    public string ResponseString { get; set; } = null!;
}
