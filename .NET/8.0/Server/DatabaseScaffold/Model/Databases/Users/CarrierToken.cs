﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class CarrierToken
{
    public int Id { get; set; }

    public string CarrierId { get; set; } = null!;

    public int OriginalTokens { get; set; }

    public int AvailableTokens { get; set; }

    public decimal SingleTokenValue { get; set; }

    public DateTimeOffset Expires { get; set; }
}
