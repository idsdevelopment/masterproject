﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class Billing
{
    public string CarrierId { get; set; } = null!;

    public decimal DefaultTokenValue { get; set; }

    public int DefaultTokenExpireInDays { get; set; }
}
