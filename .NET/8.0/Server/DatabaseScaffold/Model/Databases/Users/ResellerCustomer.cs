﻿using System;
using System.Collections.Generic;

namespace Database.Model.Databases.Users;

public partial class ResellerCustomer
{
    public long Id { get; set; }

    public string ResellerId { get; set; } = null!;

    public string LoginCode { get; set; } = null!;

    public string CustomerCode { get; set; } = null!;

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string OriginalLoginCode { get; set; } = null!;

    public bool Enabled { get; set; }
}
