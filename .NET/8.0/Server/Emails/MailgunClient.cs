﻿namespace Emails;

public class MailgunClient : Client
{
	public MailgunClient( IRequestContext context, string fromAddress ) : base( context, new CarrierDb.EmailPreference(
	                                                                                                                   CarrierDb.MAILGUN_SERVER,
	                                                                                                                   true,
	                                                                                                                   CarrierDb.MAILGUN_USER_NAME,
	                                                                                                                   int.Parse( CarrierDb.MAILGUN_PORT ),
	                                                                                                                   CarrierDb.MAILGUN_PASSWORD,
	                                                                                                                   fromAddress,
	                                                                                                                   "",
	                                                                                                                   false
	                                                                                                                  )
	                                                                          )
	{
	}
}