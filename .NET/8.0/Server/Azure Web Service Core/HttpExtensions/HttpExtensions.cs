﻿namespace AzureWebService.HttpExtensions;

public static class HttpExtensions
{
	public static string GetClientIpAddress( this HttpContext context )
	{
		context.Request.Headers.TryGetValue( "X-Forwarded-For", out var IpAddress );
		return IpAddress.FirstOrDefault() ?? context.Connection.RemoteIpAddress?.ToString() ?? "Unknown";
	}
}