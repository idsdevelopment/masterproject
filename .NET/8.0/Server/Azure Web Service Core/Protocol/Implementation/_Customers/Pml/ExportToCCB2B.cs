﻿using Exports._Cutomers.Pml;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePML_ExportToCCB2B()
		{
			CBC2C.Export( Context );
		}
	}
}