﻿using MapsApiV2;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override DistancesAndAddresses ResponseDirections( RouteOptimisationAddresses requestObject ) => Routing.Directions( Context, requestObject ).Result;

		public override MapApiKeys ResponseGetMapsApiKeys()
		{
			var Config = Context.Configuration;

			return new MapApiKeys
			       {
				       BingMapsKey   = Config.BingMapsApiKey,
				       GoogleMapsKey = Config.GoogleMapsApiKey,
				       AzureMapKey   = Config.AzureMapsApiKey
			       };
		}

		public override AddressValidList ResponseValidateAddresses( AddressValidationList requestObject ) => Routing.ValidateAddresses( Context, requestObject ).Result;
	}
}