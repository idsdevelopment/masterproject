﻿using System.Diagnostics;
using Logs;

namespace Ids;

public static class HttpExtensions
{
	public static string OriginalString( this HttpContext context )
	{
		var Request = context.Request;
		return $"{Request.Scheme}://{Request.Host}{Request.Path}{Request.QueryString}";
	}

	public static string IpAddress( this HttpContext context ) => context.Request.Headers[ "X-Forwarded-For" ].FirstOrDefault() ?? context.Connection.RemoteIpAddress?.ToString() ?? "Unknown";
}

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		private const string INTERNAL_CONTEXT = "InternalContext";

		private static bool IsValidUri( string uri ) => Uri.TryCreate( uri, UriKind.Absolute, out var Temp )
		                                                && ( ( Temp.Scheme == Uri.UriSchemeHttp ) || ( Temp.Scheme == Uri.UriSchemeHttps ) );

		private static bool ContextOk( RequestContext.RequestContext? ctx ) => ctx is not null && !IsValidUri( ctx.CarrierId );

		public IRequestContext ExceptionContext
		{
			get
			{
				var C = Context;

				if( ContextOk( C ) )
					return C;

				var Ctx = new RequestContext.RequestContext
				          {
					          CarrierId = INTERNAL_CONTEXT,
					          UserName  = INTERNAL_CONTEXT,
					          IpAddress = HttpContext.IpAddress()
				          };

				return Ctx;
			}
		}

		protected override void InitialiseExceptions()
		{
			( bool Ok, string Url ) GetUrl()
			{
				var Uri = HttpContext.OriginalString();

				return ( !( Uri.EndsWith( "idsroute.com/", StringComparison.OrdinalIgnoreCase )
				            || Uri.EndsWith( ".azurewebsites.net/", StringComparison.OrdinalIgnoreCase ) ),
				         Uri );
			}

			OnAbortConnectionException = exception =>
			                             {
				                             var (Ok, Url) = GetUrl();

				                             if( Ok )
				                             {
				                             #if DEBUG
					                             Debug.WriteLine( $"AbortConnectionException: {Url}" );
				                             #endif
					                             BadRequestLog.Add( ExceptionContext, Url, DoNoKeyException( exception ) );
				                             }
			                             };

			OnException = exception =>
			              {
				              if( exception is not QuietException )
				              {
					              var (Ok, Url) = GetUrl();

					              if( Ok )
					              {
					              #if DEBUG
						              Debug.WriteLine( $"""
						                                Exception: {Url}
						                                Message: {exception}
						                                """ );
					              #endif
						              SystemLog.Add( ExceptionContext, Url, DoNoKeyException( exception ) );
					              }
				              }
			              };
		}

		public override void ResponseThrowSystemLogException( string requestObject )
		{
			Context.SystemLogExceptionProgram( "DEBUG", new Exception( requestObject ) );
		}

	#if DISABLE_TELEMETRY
		private static void DisableApplicationInsightsOnDebug()
		{
//				TelemetryConfiguration.Active.DisableTelemetry = true;
		}
	#endif

		private Exception DoNoKeyException( Exception exception )
		{
			if( exception is IdsServerExtensions.KeyNotPresentException )
			{
				var Temp = new StringBuilder( $"{exception.Message}\r\n" );

				var Form = HttpContext.Request.Form;
				var Keys = Form.Keys;

				foreach( var Key in Keys )
					Temp.Append( $"Key: {Key} Post Data: {Form[ Key ]}\r\n" );

				exception = new KeyNotFoundException( Temp.ToString().Trim(), exception );
			}

			return exception;
		}
	}
}