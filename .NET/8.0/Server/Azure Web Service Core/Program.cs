using System.Diagnostics;
using AzureWebService.Protocol.Implementation.Forwarders;
using Ids;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace Azure_Web_Service_Core;

public class Program
{
	private const string TEXT_HTML = "text/html";

	private const int MAX_REQUEST_LENGTH = 104_857_600; // 100MB

	private static readonly IdsServer Server = new();

	public static void Main( string[] args )
	{
		var Builder = WebApplication.CreateBuilder( args );

		var Services = Builder.Services;

//		Services.AddApplicationInsightsTelemetry();

		Services.AddMvc( options =>
		                 {
			                 options.CacheProfiles.Add( "NoCache",
			                                            new CacheProfile
			                                            {
				                                            NoStore  = true,
				                                            Duration = 0
			                                            } );
		                 } );

		Services.Configure<FormOptions>( options =>
		                                 {
			                                 options.ValueLengthLimit         = MAX_REQUEST_LENGTH;
			                                 options.MultipartBodyLengthLimit = MAX_REQUEST_LENGTH;
		                                 } );

		Services.Configure<KestrelServerOptions>( options =>
		                                          {
			                                          options.Limits.KeepAliveTimeout   = TimeSpan.FromMinutes( 30 ); // executionTimeout
			                                          options.Limits.MaxRequestBodySize = MAX_REQUEST_LENGTH;
		                                          } );

		async Task<bool> Handler( HttpContext context )
		{
			try
			{
				Debug.WriteLine( $"Request: {context.Request.Path}" );
				return await Server.ProcessRequestAsync( context );
			}
		#if DEBUG
			catch( Exception E )
		#else
			catch
		#endif
			{
			#if DEBUG
				Debug.WriteLine( $"""
				                  ------------------------------
				                  {E.Message}
				                  ------------------------------
				                  """ );
			#endif
				var Response = context.Response;
				Response.StatusCode = 666;
				var Body = Response.Body;
				Body.SetLength( 0 );
				Body.Close();
				await Body.FlushAsync();
			}
			return false;
		}

		var App = Builder.Build();

		var Environment = App.Services.GetRequiredService<IWebHostEnvironment>();

		Hosting.Environment.HostingEnvironment = Environment;
		var RootPath = Environment.ContentRootPath;
		FileExtensions.ContentRoot = RootPath;

		App.Use( async ( context, next ) =>
		         {
			         if( !await Handler( context ) )
				         await next.Invoke(); // Call the next middleware in the pipeline
		         } );

		App.UseHttpsRedirection();
		App.UseStaticFiles();

		App.MapFallback( async context =>
		                 {
			                 var Response = context.Response;
			                 Response.ContentType = TEXT_HTML;

			                 var Bytes = "/index.html".CacheRead();
			                 await Response.Body.WriteAsync( Bytes, 0, Bytes.Length );
		                 } );
		App.Run();
	}
}