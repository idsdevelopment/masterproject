﻿namespace DatabaseCore.Model.Carrier._Customers.Pml;

public class PmlTripExport : TripExport
{
	public bool IsFutile
	{
		get => Bool1;
		set => Bool1 = value;
	}

	public PmlTripExport()
	{
	}

	public PmlTripExport( TripExport export ) : base( export )
	{
	}

	public PmlTripExport( Export export ) : base( export )
	{
	}

	public static void AddTripStatusToExport( CarrierDb db, string code, STATUS status, bool isFutile )
	{
		var Rec = db.CreateExportTrip<PmlTripExport>( code, TRIP_DATA_SUB_TYPES.STATUS, status );

		if( Rec is not null )
		{
			Rec.IsFutile = isFutile;
			db.AddToExport( Rec );
		}
	}

	public static void AddFutileTripToExport( CarrierDb db, string code )
	{
		AddTripStatusToExport( db, code, STATUS.DELETED, true );
	}
}