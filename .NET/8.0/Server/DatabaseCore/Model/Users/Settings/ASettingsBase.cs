﻿using Database.Model.Database;

namespace DatabaseCore.Model.Users.Settings;

public abstract class ASettingsBase
{
	protected readonly IRequestContext Context;

	protected ASettingsBase( IRequestContext context )
	{
		Context = context;
	}

	protected CarrierSetting NewSetting( short primaryId, short secondaryId ) => new()
	                                                                             {
		                                                                             CarrierId   = Context.CarrierId,
		                                                                             PrimaryId   = primaryId,
		                                                                             SecondaryId = secondaryId,
		                                                                             String1     = "",
		                                                                             String2     = "",
		                                                                             String3     = "",
		                                                                             String4     = "",
		                                                                             String5     = "",
		                                                                             Date1       = DateTimeOffset.MinValue,
		                                                                             Date2       = DateTimeOffset.MinValue,
		                                                                             Date3       = DateTimeOffset.MinValue,
		                                                                             Date4       = DateTimeOffset.MinValue,
		                                                                             Date5       = DateTimeOffset.MinValue
	                                                                             };

	public CarrierSetting GetSetting( short primaryId, short secondaryId )
	{
		try
		{
			using var Db = new UsersEntities();

			var Rec = Db.Entity.CarrierSettings.Find( Context.CarrierId, primaryId, secondaryId );

			if( Rec is not null )
				return Rec;
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return NewSetting( primaryId, secondaryId );
	}

	public void UpdateSetting( CarrierSetting setting )
	{
		try
		{
			using var Db  = new UsersEntities();
			var       Rec = Db.Entity.CarrierSettings.Find( setting.CarrierId, setting.PrimaryId, setting.SecondaryId );

			if( Rec is null )
				Db.Entity.CarrierSettings.Add( setting );
			else
			{
				Rec.String1  = setting.String1;
				Rec.String2  = setting.String2;
				Rec.String3  = setting.String3;
				Rec.String4  = setting.String4;
				Rec.String5  = setting.String5;
				Rec.Date1    = setting.Date1;
				Rec.Date2    = setting.Date2;
				Rec.Date3    = setting.Date3;
				Rec.Date4    = setting.Date4;
				Rec.Date5    = setting.Date5;
				Rec.Bool1    = setting.Bool1;
				Rec.Bool2    = setting.Bool2;
				Rec.Bool3    = setting.Bool3;
				Rec.Bool4    = setting.Bool4;
				Rec.Bool5    = setting.Bool5;
				Rec.Decimal1 = setting.Decimal1;
				Rec.Decimal2 = setting.Decimal2;
				Rec.Decimal3 = setting.Decimal3;
				Rec.Decimal4 = setting.Decimal4;
				Rec.Decimal5 = setting.Decimal5;
			}
			Db.SaveChanges();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}
}