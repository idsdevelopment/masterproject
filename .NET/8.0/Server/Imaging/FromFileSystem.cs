﻿using System;
using System.IO;

namespace Imaging;

public static class FromFileSystem
{
	public static MemoryStream GetImage( string path )
	{
		var Result = new MemoryStream();
		var Path   = Hosting.Environment.MapPath( path );

		using var Stream = new FileStream( Path, FileMode.Open, FileAccess.Read );
		Stream.CopyTo( Result );

		return Result;
	}

	public static string GetImageBase64( string path )
	{
		using var Image = GetImage( path );

		return Convert.ToBase64String( Image.GetBuffer() );
	}
}