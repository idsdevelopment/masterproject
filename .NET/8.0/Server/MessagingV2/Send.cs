﻿namespace MessagingV2;

internal class Send<TMessage>
{
	internal const int DEFAULT_CLEANUP_INACTIVE_PERIOD_MINUTES = 60 * 24 * 7;

	internal static void Execute( IRequestContext context, TMessage message,
	                              string topic,
	                              string subTopic = "", string section = "",
	                              int cleanupInactivePeriodMinutes = DEFAULT_CLEANUP_INACTIVE_PERIOD_MINUTES )
	{
		var Json    = JsonConvert.SerializeObject( message );
		var Now     = DateTime.UtcNow;
		var Expires = Now.AddMinutes( cleanupInactivePeriodMinutes );

		var Ids = Database.GetTopics( ( context, topic, subTopic, section ) );

		using var Db     = new UsersEntities();
		var       E      = Db.Entity;
		var       Queues = E.MessageQueues;
		var       Topics = E.MessagingTopics;

		foreach( var Id in Ids )
		{
			var QRec = new MessageQueue
			           {
				           TopicId     = Id,
				           JsonMessage = Json,
				           Expires     = Expires,
				           WaitingAck  = false
			           };

			try
			{
				Queues.Add( QRec );
				Db.SaveChanges();

				while( true )
				{
					try
					{
						var Rec = ( from T in Topics
						            where T.Id == Id
						            select T ).FirstOrDefault();

						if( Rec is not null )
						{
							Rec.LastUpdated = Now;
							Db.SaveChanges();
						}
					}
					catch( DbUpdateConcurrencyException )
					{
						continue;
					}
					break;
				}
			}
			catch( Exception Ex )
			{
				context.SystemLogExceptionProgram( Database.PROGRAM, Ex );
			}
		}
	}
}