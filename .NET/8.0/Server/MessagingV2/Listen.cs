﻿namespace MessagingV2;

internal class Listen<TMessage>
{
	private const int LISTEN_SLEEP_MILLISECONDS = 10_000;

	internal static TMessage? Execute( IRequestContext context, TMessage message,
	                                   string topic,
	                                   string subTopic, string section = "" )
	{
		var (CarrierId, Topic, SubTopic, Section) = Database.FixTopic( ( context, topic, subTopic, section ) );

		try
		{
			bool DoSleep()
			{
				Thread.Sleep( LISTEN_SLEEP_MILLISECONDS );
				return true;
			}

			do
			{
				using var Db     = new UsersEntities();
				var       E      = Db.Entity;
				var       Topics = E.MessagingTopics;
				var       Queues = E.MessageQueues;

				var TopicRec = ( from T in Topics
				                 where ( T.CarrierId == CarrierId ) && ( T.Topic == Topic ) && ( T.SubTopic == SubTopic ) && ( T.Section == Section )
				                 select T ).FirstOrDefault();

				if( TopicRec is not null )
				{
					var Now = DateTime.UtcNow;
					TopicRec.LastUpdated = Now;

					var QueueRec = ( from Q in Queues
					                 where Q.TopicId == TopicRec.Id
					                 orderby Q.Id
					                 select Q ).FirstOrDefault();

					if( QueueRec is not null )
					{
						QueueRec.WaitingAck = true;

						try
						{
							Db.SaveChanges();
						}
						catch( DbUpdateConcurrencyException )
						{
							continue;
						}
						catch( Exception Ex )
						{
							context.SystemLogExceptionProgram( Database.PROGRAM, Ex );
							return default;
						}
						return JsonConvert.DeserializeObject<TMessage>( QueueRec.JsonMessage );
					}
				}
				else
					break;
			}
			while( DoSleep() );
		}
		catch
		{
		}
		return default;
	}
}