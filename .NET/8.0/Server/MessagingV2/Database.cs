﻿namespace MessagingV2;

internal class Database
{
	internal const string PROGRAM = "MessagingV2";

	internal static (string CarrierIf, string Topic, string SubTopic, string Section) FixTopic( (IRequestContext context, string Topic, string SubTopic, string Section) topic )
		=> ( topic.context.CarrierId.TrimToUpper(),
		     topic.Topic.TrimToUpper(),
		     topic.SubTopic.TrimToUpper(),
		     topic.Section.TrimToUpper() );

	public static long AddUpdateTopic( (IRequestContext context, string topic, string subTopic, string section) topic, int cleanupInactivePeriodMinutes )
	{
		var (CarrierId, Topic, SubTopic, Section) = FixTopic( topic );

		using var Db     = new UsersEntities();
		var       E      = Db.Entity;
		var       Topics = E.MessagingTopics;

		while( true )
		{
			MessagingTopic? Rec;

			try
			{
				Rec = ( from T in Topics
				        where ( T.CarrierId == CarrierId ) && ( T.Topic == Topic ) && ( T.SubTopic == SubTopic ) && ( T.Section == Section )
				        select T ).FirstOrDefault();

				var Now = DateTime.UtcNow;

				if( Rec is null )
				{
					Rec = new MessagingTopic
					      {
						      CarrierId                    = CarrierId,
						      Topic                        = Topic,
						      SubTopic                     = SubTopic,
						      Section                      = Section,
						      CleanupInactivePeriodMinutes = cleanupInactivePeriodMinutes,
						      LastUpdated                  = Now
					      };
				}
				else
				{
					Rec.CleanupInactivePeriodMinutes = cleanupInactivePeriodMinutes;
					Rec.LastUpdated                  = Now;
				}
			}
			catch( DbUpdateConcurrencyException )
			{
				continue;
			}
			catch( Exception Ex )
			{
				topic.context.SystemLogExceptionProgram( PROGRAM, Ex );
				return -1;
			}
			return Rec.Id;
		}
	}

	public static List<long> GetTopics( (IRequestContext context, string topic, string subTopic, string section) topic )
	{
		var (CarrierId, Topic, SubTopic, Section) = FixTopic( topic );

		using var Db = new UsersEntities();

		List<long> Recs;

		try
		{
			Recs = ( from T in Db.Entity.MessagingTopics
			         where ( T.CarrierId == CarrierId )
			               && ( ( Topic == "" ) || ( T.Topic == Topic ) )
			               && ( ( SubTopic == "" ) || ( T.SubTopic == SubTopic ) )
			               && ( ( Section == "" ) || ( T.Section == Section ) )
			         orderby T.Id
			         select T.Id ).ToList();
		}
		catch( Exception Ex )
		{
			topic.context.SystemLogExceptionProgram( PROGRAM, Ex );
			Recs = new List<long>();
		}
		return Recs;
	}
}