﻿namespace ResourceGroups;

internal static class Groups
{
	internal const string DATABASE_RESOURCE_GROUP = "IdsRoute",
	                      BACKUP_RESOURCE_GROUP   = "Backups";
}