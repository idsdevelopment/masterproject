﻿namespace ResourceGroups;

internal class AzureRegion
{
	// ReSharper disable once InconsistentNaming
	public static readonly AzureLocation DEFAULT_REGION = AzureLocation.NorthCentralUS;
}