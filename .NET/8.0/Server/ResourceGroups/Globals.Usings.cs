﻿global using System;
global using System.Threading.Tasks;
global using Azure;
global using Azure.Identity;
global using Azure.ResourceManager;
global using Azure.ResourceManager.Resources;
global using Interfaces.Interfaces;
global using Utils;
global using Azure.Core;
