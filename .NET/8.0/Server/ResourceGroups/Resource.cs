﻿namespace ResourceGroups;

public static class Resource
{
	public static ArmClient Client => _Client ??= new ArmClient( new DefaultAzureCredential() );

	private static ArmClient? _Client;

	public static async Task<(ResourceGroupResource? GroupResource, AzureLocation Region)> CreateResourceGroup( IRequestContext context, string groupName )
	{
		var Region = AzureRegion.DEFAULT_REGION;

		try
		{
			// Now we get a ResourceGroupResource collection for that subscription
			var Response = await Client.GetSubscriptions().GetAsync( AzureSubscription.SUBSCRIPTION_ID );

			if( Response is { HasValue: true, Value: { } Subscription } )
			{
				var ResourceGroups = Subscription.GetResourceGroups();

				// With the collection, we can create a new resource group with and specific name
				var ResourceGroupData = new ResourceGroupData( Region );

				ArmOperation<ResourceGroupResource> Operation = await ResourceGroups.CreateOrUpdateAsync( WaitUntil.Completed,
				                                                                                          groupName,
				                                                                                          ResourceGroupData );

				if( Operation is { Value: { } ResourceGroup } )
					return ( ResourceGroup, Region );
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}
		return ( null, Region );
	}

	public static async Task<(string GroupName, ResourceGroupResource? ResourceGroup, AzureLocation Region)> CreateResourceGroup( IRequestContext context )
	{
		var Cid  = context.CarrierId.ToIdent();
		var Temp = await CreateResourceGroup( context, Cid );
		return ( GroupName: Cid, Temp.GroupResource, Temp.Region );
	}
}