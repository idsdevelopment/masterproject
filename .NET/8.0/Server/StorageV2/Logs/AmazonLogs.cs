﻿namespace StorageV2.Logs;

public class AmazonLogs : ALogBase
{
	public static void AddCsv( string program, string carrierId, DateTimeOffset dateTime, string csvName, string csvData )
	{
		Tasks.RunVoid( () =>
		               {
			               Append( $"AmazonLogs/{program}", carrierId, csvName, dateTime, csvData );
		               } );
	}
}