﻿using System.ComponentModel;

namespace TripManagement;

public class TripDisplayEntry : INotifyPropertyChanged
{
	protected readonly Trip _Trip;


	public TripDisplayEntry( Trip trip )
	{
		_Trip = trip;

		var SColors = Globals.ServiceLevelColors.GetValueOrDefault( trip.ServiceLevel.TrimToUpper(),
		                                                            ( BackgroundColor: Colors.Black, ForegroundColor: Colors.White ) );
		ServiceLevelBackground = SColors.BackgroundColor;
		ServiceLevelForeground = SColors.ForegroundColor;
	}

	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	protected bool SetField<T>( ref T field, T value, [CallerMemberName] string? propertyName = null )
	{
		if( EqualityComparer<T>.Default.Equals( field, value ) )
			return false;
		field = value;
		OnPropertyChanged( propertyName );
		return true;
	}

	public event PropertyChangedEventHandler? PropertyChanged;

#region Properties
	public string TripId => _Trip.TripId;

#region Status
	public bool IsPickup   => _Trip.Status1 < STATUS.PICKED_UP;
	public bool IsDelivery => _Trip.Status1 >= STATUS.PICKED_UP;
#endregion

#region Colours
	private static Color? _PickupColor,
	                      _DeliveryColor,
	                      _ReadColor,
	                      _ReadTexColor,
	                      _UnReadColor,
	                      _UnReadTextColor;

	private static Color GetColor( string key, ref Color? color )
	{
		return color ??= Globals.ColorResource( key );
	}

	public Color PickupDeliveryColor => IsPickup   ? GetColor( "PickupColor", ref _PickupColor ) :
	                                    IsDelivery ? GetColor( "DeliveryColor", ref _DeliveryColor ) : Colors.Red;
#endregion

#region Reference
	public string Reference => _Trip.Reference;
#endregion

#region Notes
	public string PickupNotes   => _Trip.PickupNotes.Trim();
	public string DeliveryNotes => _Trip.DeliveryNotes.Trim();
	public string DisplayNotes  => IsPickup ? PickupNotes : DeliveryNotes;
#endregion

#region Service Level
	public string ServiceLevel           => _Trip.ServiceLevel;
	public Color  ServiceLevelBackground { get; private set; }
	public Color  ServiceLevelForeground { get; private set; }
#endregion

#region Package Type
	public string PackageType => _Trip.PackageType;
#endregion

#region Pieces / Weight
	public decimal Pieces        => _Trip.Pieces;
	public string  DisplayPieces => $"{_Trip.Pieces:N0}";
	public string  DisplayWeight => $"{_Trip.Weight:N0}";
#endregion

#region Date / Time
	public DateTimeOffset PickupDeliveryDateTime => ( IsPickup ? _Trip.PickupTime : _Trip.DeliveryTime ) ?? DateTimeOffset.Now;
	public string         DisplayTime            => $"{PickupDeliveryDateTime:ddd h:mm tt}";
#endregion

#region Read By Driver
	public bool  ReadByDriver  => _Trip.ReadByDriver;
	public Color ReadColor     => ReadByDriver ? GetColor( "ReadColor", ref _ReadColor ) : GetColor( "UnReadColor", ref _UnReadColor );
	public Color ReadTextColor => ReadByDriver ? GetColor( "ReadTextColor", ref _ReadTexColor ) : GetColor( "UnReadTextColor", ref _UnReadTextColor );
#endregion

#region Company
	public string PickupCompanyName   => _Trip.PickupCompanyName.Trim();
	public string DeliveryCompanyName => _Trip.DeliveryCompanyName.Trim();
	public string DisplayCompanyName  => IsPickup ? PickupCompanyName : DeliveryCompanyName;


	public string PickupLocation   => _Trip.PickupAddressBarcode.Trim();
	public string DeliveryLocation => _Trip.DeliveryAddressBarcode.Trim();
	public string DisplayLocation  => IsPickup ? PickupLocation : DeliveryLocation;

	public string PickupCity   => _Trip.PickupAddressCity.Trim();
	public string DeliveryCity => _Trip.DeliveryAddressCity.Trim();
	public string DisplayCity  => IsPickup ? PickupCity : DeliveryCity;

#region Address
	public string PickupAddress   => _Trip.PickupAddressAddressLine1.Trim();
	public string DeliveryAddress => _Trip.DeliveryAddressAddressLine1.Trim();

	private string? _DisplayAddress;

	public string DisplayAddress
	{
		get { return _DisplayAddress ??= $"{( IsPickup ? PickupAddress : DeliveryAddress )}  {DeliveryCity}"; }
	}
#endregion

	public bool IsSameCompany( TripDisplayEntry other )
	{
		int Compare;

		if( IsPickup )
		{
			if( PickupLocation.IsNotNullOrWhiteSpace() && other.PickupLocation.IsNotNullOrWhiteSpace() )
				Compare = string.Compare( PickupLocation, other.PickupLocation, StringComparison.OrdinalIgnoreCase );
			else
				Compare = string.Compare( PickupCompanyName, other.PickupCompanyName, StringComparison.OrdinalIgnoreCase );
		}
		else
		{
			if( DeliveryLocation.IsNotNullOrWhiteSpace() && other.DeliveryLocation.IsNotNullOrWhiteSpace() )
				Compare = string.Compare( DeliveryLocation, other.DeliveryLocation, StringComparison.OrdinalIgnoreCase );
			else
				Compare = string.Compare( DeliveryCompanyName, other.DeliveryCompanyName, StringComparison.OrdinalIgnoreCase );
		}
		return Compare == 0;
	}
#endregion


#region Convertors
	public static implicit operator TripDisplayEntry( Trip trip ) => new( trip );
	public static implicit operator Trip( TripDisplayEntry entry ) => entry._Trip;
#endregion
#endregion
}