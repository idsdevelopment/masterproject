﻿using System.Collections.Specialized;
using AzureRemoteService;
using Events;

namespace TripManagement;

using SERVICE_LEVEL_COLORS = (Color BackgroundColor, Color ForegroundColor);

// All the code in this file is included in all platforms.
public partial class Globals
{
	public static readonly OBSERVABLE_TRIP_DICTIONARY TripDisplayEntries = InitCollection();

	public static Action<bool>? OnWaitingForTrips;

	public static Func<string, string>? GetStringResource;
	public static Func<string, Color>?  GetColorResource;

	public static void GetTrips( string driverCode, Action onHaveTrips )
	{
		Tasks.RunVoid( async () =>
		               {
			               void WaitingForTrips( bool waiting )
			               {
				               OnWaitingForTrips?.Invoke( waiting );
			               }

			               await Azure.Retry( async () =>
			                                  {
				                                  WaitingForTrips( true );

				                                  var TokenSource = new CancellationTokenSource();
				                                  var Token       = TokenSource.Token;

				                                  void CheckForCancellation()
				                                  {
					                                  if( Token.IsCancellationRequested )
					                                  {
						                                  TripDisplayEntries.Clear();
						                                  Token.ThrowIfCancellationRequested();
					                                  }
				                                  }

				                                  try
				                                  {
					                                  Task[] Tsks;

					                                  if( !HaveServiceLevelColors )
					                                  {
						                                  Tsks = new Task[ 2 ];

						                                  Tsks[ 1 ] = Task.Run( async () =>
						                                                        {
							                                                        try
							                                                        {
								                                                        Preferences._Preferences = await Azure.Client.RequestDevicePreferences();

								                                                        CheckForCancellation();

								                                                        var Colors = await Azure.Client.RequestGetServiceLevelColours();

								                                                        foreach( var C in Colors )
								                                                        {
									                                                        CheckForCancellation();

									                                                        ServiceLevelColors[ C.ServiceLevel.TrimToUpper() ] = ( Color.FromUint( C.Background_ARGB ),
									                                                                                                               Color.FromUint( C.Foreground_ARGB ) );
								                                                        }
								                                                        HaveServiceLevelColors = true;
							                                                        }
							                                                        catch( Exception E ) when( E is not OperationCanceledException )
							                                                        {
								                                                        await TokenSource.CancelAsync();
							                                                        }
						                                                        }, Token );
					                                  }
					                                  else
						                                  Tsks = new Task[ 1 ];

					                                  Tsks[ 0 ] = Task.Run( async () =>
					                                                        {
						                                                        try
						                                                        {
							                                                        var Trips = await Azure.Client.RequestGetTripsForDriver( driverCode );
							                                                        TripDisplayEntries.Clear();

							                                                        foreach( var Trip in Trips )
							                                                        {
								                                                        CheckForCancellation();

								                                                        TripDisplayEntries.Add( Trip.TripId, new TripDisplayEntry( Trip ) );
							                                                        }
						                                                        }
						                                                        catch( Exception E ) when( E is not OperationCanceledException )
						                                                        {
							                                                        await TokenSource.CancelAsync();
						                                                        }
					                                                        }, Token );

					                                  await Task.WhenAll( Tsks );
					                                  return TripDisplayEntries.Count > 0;
				                                  }
				                                  finally
				                                  {
					                                  WaitingForTrips( false );
					                                  onHaveTrips();
				                                  }
			                                  }, int.MaxValue ); // Retry forever
		               } );
	}

	public static string StringResource( string key ) => GetStringResource?.Invoke( key ) ?? "";

	public static Color ColorResource( string key ) => GetColorResource?.Invoke( key ) ?? Colors.Transparent;

	private static OBSERVABLE_TRIP_DICTIONARY InitCollection()
	{
		var Collection = new OBSERVABLE_TRIP_DICTIONARY();
		Collection.CollectionChanged += CollectionOnCollectionChanged;
		return Collection;
	}

	private static void CollectionOnCollectionChanged( object? sender, NotifyCollectionChangedEventArgs e )
	{
		TripDisplayEntriesChanged.Value = e;
	}

#region Service Levels
	private static           bool                                     HaveServiceLevelColors;
	internal static readonly Dictionary<string, SERVICE_LEVEL_COLORS> ServiceLevelColors = new();
#endregion

#region Events
	public static readonly WeakEvent<NotifyCollectionChangedEventArgs> TripDisplayEntriesChanged = new();

	public static readonly WeakEvent<bool> OnPing = new();
#endregion
}