﻿namespace Records;

public record GpsPoint( DateTimeOffset DateTime, double Latitude, double Longitude );

public class GpsPoints : List<GpsPoint>;