﻿using Protocol.Data;

namespace Records;

public record ReceivedByDevice( string Program, Trip Trip );

public record ReadByDriver( string Program, Trip Trip );

public record DeviceTripUpdate
{
	public string Program { get; init; } = "";

	public STATUS Status { get; init; } = STATUS.NEW;

	public STATUS1 Status1 { get; init; } = STATUS1.UNSET;

	public string TripId { get; init; } = "";

	public DateTimeOffset UpdateTime { get; init; } = DateTimeOffset.MinValue;

	public double UpdateLatitude { get; init; } = 0.0;

	public double UpdateLongitude { get; init; } = 0.0;

	public decimal Pieces { get; init; } = 0;

	public decimal Weight { get; init; } = 0;

	public string UndeliverableNotes { get; init; } = "";

	public Signature Signature { get; init; } = new();

	public string Reference { get; init; } = "";

	public string PopPod { get; init; } = "";

	public string DriverNotes { get; init; } = "";

	public DateTimeOffset WaitTimeStarTime { get; init; } = DateTimeOffset.MinValue;

	public int WaitTimeInSeconds { get; init; } = 0;

	public DateTimeOffset ArriveTime { get; init; } = DateTimeOffset.MinValue;

	public static implicit operator Protocol.Data.DeviceTripUpdate( DeviceTripUpdate update ) => new( update.Program )
	                                                                                             {
		                                                                                             Status             = update.Status,
		                                                                                             Status1            = update.Status1,
		                                                                                             TripId             = update.TripId,
		                                                                                             UpdateTime         = update.UpdateTime,
		                                                                                             UpdateLatitude     = update.UpdateLatitude,
		                                                                                             UpdateLongitude    = update.UpdateLongitude,
		                                                                                             Pieces             = update.Pieces,
		                                                                                             Weight             = update.Weight,
		                                                                                             UndeliverableNotes = update.UndeliverableNotes,
		                                                                                             Signature          = update.Signature,
		                                                                                             Reference          = update.Reference,
		                                                                                             PopPod             = update.PopPod,
		                                                                                             DriverNotes        = update.DriverNotes,
		                                                                                             WaitTimeStarTime   = update.WaitTimeStarTime,
		                                                                                             WaitTimeInSeconds  = update.WaitTimeInSeconds,
		                                                                                             ArriveTime         = update.ArriveTime
	                                                                                             };
}