﻿using Extensions;
using Ids1RemoteService;
using Protocol.Data;

// ReSharper disable InconsistentNaming

namespace Records;

public record Trip
{
	public string TripId { get; set; } = "";

	public STATUS  Status  { get; set; } = STATUS.UNSET;
	public STATUS1 Status1 { get; set; } = STATUS1.UNSET;

	public string Driver        { get; set; } = "";
	public string Reference     { get; set; } = "";
	public string PickupNotes   { get; set; } = "";
	public string DeliveryNotes { get; set; } = "";
	public string ServiceLevel  { get; set; } = "";
	public string PackageType   { get; set; } = "";

	public decimal Pieces { get; set; }
	public decimal Weight { get; set; }

	public DateTimeOffset PickupTime   { get; } = DateTimeOffset.MinValue;
	public DateTimeOffset DeliveryTime { get; } = DateTimeOffset.MinValue;
	public DateTimeOffset DueTime      { get; } = DateTimeOffset.MinValue;

	public bool ReadByDriver     { get; set; }
	public bool ReceivedByDevice { get; set; }

	public string PickupCompanyName    { get; set; } = "";
	public string PickupAddressBarcode { get; set; } = "";
	public string PickupSuite          { get; set; } = "";
	public string PickupAddressLine1   { get; set; } = "";
	public string PickupAddressCity    { get; set; } = "";

	public string DeliveryCompanyName    { get; set; } = "";
	public string DeliveryAddressBarcode { get; set; } = "";
	public string DeliverySuite          { get; set; } = "";
	public string DeliveryAddressLine1   { get; set; } = "";

	public string DeliveryAddressCity { get; set; } = "";

	public string AccountId { get; set; } = "";

	public string POP { get; set; } = "";
	public string POD { get; set; } = "";

	public Trip( Protocol.Data.Trip trip )
	{
		OriginalTrip     = trip;
		OriginalTripType = ORIGINAL_TRIP_TYPE.PROTOCOL_DATA;

		TripId    = trip.TripId;
		AccountId = trip.AccountId;

		Status           = trip.Status1;
		Status1          = trip.Status2;
		Driver           = trip.Driver;
		Reference        = trip.Reference;
		ServiceLevel     = trip.ServiceLevel;
		PackageType      = trip.PackageType;
		Pieces           = trip.Pieces;
		Weight           = trip.Weight;
		PickupTime       = trip.PickupTime ?? DateTimeOffset.MinValue;
		DeliveryTime     = trip.DeliveryTime ?? DateTimeOffset.MinValue;
		DueTime          = trip.DueTime ?? DateTimeOffset.MinValue;
		ReadByDriver     = trip.ReadByDriver;
		ReceivedByDevice = trip.ReceivedByDevice;

		PickupCompanyName    = trip.PickupCompanyName;
		PickupAddressBarcode = trip.PickupAddressBarcode;
		PickupSuite          = trip.PickupAddressSuite;
		PickupAddressLine1   = trip.PickupAddressAddressLine1;
		PickupAddressCity    = trip.PickupAddressCity;
		PickupNotes          = trip.PickupNotes.Trim();

		DeliveryCompanyName    = trip.DeliveryCompanyName;
		DeliveryAddressBarcode = trip.DeliveryAddressBarcode;
		DeliverySuite          = trip.DeliveryAddressSuite;
		DeliveryAddressLine1   = trip.DeliveryAddressAddressLine1;
		DeliveryAddressCity    = trip.DeliveryAddressCity;
		DeliveryNotes          = trip.DeliveryNotes.Trim();

		POP = trip.POP;
		POD = trip.POD;
	}

	public Trip( remoteTrip trip )
	{
		OriginalTrip     = trip;
		OriginalTripType = ORIGINAL_TRIP_TYPE.REMOTE_TRIP;

		TripId    = trip.tripId;
		AccountId = trip.accountId;

		Status                 = trip.status.ToStatus();
		Status1                = STATUS1.UNSET;
		Reference              = trip.clientReference;
		PickupNotes            = trip.pickupNotes.Trim();
		DeliveryNotes          = trip.deliveryNotes.Trim();
		ServiceLevel           = trip.serviceLevel;
		PackageType            = trip.packageType;
		Pieces                 = trip.pieces;
		Weight                 = (decimal)trip.weight;
		PickupTime             = trip.pickupTime;
		DeliveryTime           = trip.deliveredTime;
		DueTime                = trip.deadlineTime;
		ReadByDriver           = trip.carCharge;
		ReceivedByDevice       = trip.redirect;
		PickupCompanyName      = trip.pickupCompany;
		PickupAddressBarcode   = trip.pickupCompany.ToAddressBarcode();
		DeliveryCompanyName    = trip.deliveryCompany;
		DeliveryAddressBarcode = trip.deliveryCompany.ToAddressBarcode();
		POD                    = trip.podName;
		POP                    = trip.podName;
	}


	public Trip( remoteTripDetailed rd )
	{
		OriginalTrip = rd;

		OriginalTripType = ORIGINAL_TRIP_TYPE.REMOTE_TRIP_DETAILED;

		TripId    = rd.tripId;
		AccountId = rd.accountId;

		Status    = rd.status.ToStatus();
		Status1   = STATUS1.UNSET;
		Driver    = rd.driver;
		Reference = rd.clientReference;

		ServiceLevel = rd.serviceLevel;
		PackageType  = rd.packageType;

		Pieces = rd.pieces;
		Weight = (decimal)rd.weight;

		DueTime          = rd.deadlineTime;
		ReadByDriver     = rd.carCharge;
		ReceivedByDevice = rd.redirect;

		PickupTime           = rd.pickupTime;
		PickupCompanyName    = rd.pickupCompany;
		PickupAddressBarcode = rd.pickupCompany.ToAddressBarcode();
		PickupSuite          = rd.pickupSuite;
		PickupAddressLine1   = rd.pickupStreet;
		PickupAddressCity    = rd.pickupCity;
		PickupNotes          = rd.pickupNotes.Trim();

		DeliveryTime           = rd.deliveredTime;
		DeliveryCompanyName    = rd.deliveryCompany;
		DeliveryAddressBarcode = rd.deliveryCompany.ToAddressBarcode();
		DeliverySuite          = rd.deliverySuite;
		DeliveryAddressLine1   = rd.deliveryStreet;
		DeliveryAddressCity    = rd.deliveryCity;
		DeliveryNotes          = rd.deliveryNotes.Trim();
		POD                    = rd.podName;
		POP                    = rd.podName;
	}

	public Trip()
	{
	}

#region Original Trip
	// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
	public object? OriginalTrip { get; set; }

	public enum ORIGINAL_TRIP_TYPE
	{
		NONE,
		PROTOCOL_DATA,
		REMOTE_TRIP,
		REMOTE_TRIP_DETAILED
	}

	public ORIGINAL_TRIP_TYPE OriginalTripType { get; set; } = ORIGINAL_TRIP_TYPE.NONE;
#endregion

#region Convertors
	public static explicit operator Protocol.Data.Trip?( Trip trip ) => trip.OriginalTrip as Protocol.Data.Trip;
	public static explicit operator remoteTrip?( Trip trip ) => trip.OriginalTrip as remoteTrip;
	public static explicit operator remoteTripDetailed?( Trip trip ) => trip.OriginalTrip as remoteTripDetailed;
#endregion
}