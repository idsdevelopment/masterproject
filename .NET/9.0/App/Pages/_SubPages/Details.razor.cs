﻿using Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using TripManagement;

namespace SubPages.Details;

public partial class Details
{
	public static TripDisplayEntry? Trip;

	[Inject]
	private IJSRuntime JsRuntime { get; set; } = null!;

	private void OnBack()
	{
		JsRuntime.GoBack();
	}
}