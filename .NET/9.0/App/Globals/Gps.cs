﻿using System.Timers;
using Client;
using Microsoft.JSInterop;
using Records;
using Utils;
using Timer = System.Timers.Timer;

namespace App;

public partial class Globals
{
	public static class Gps
	{
		private const decimal MIN_DISTANCE_IN_METRES = 20; // 20 Metres

		private const int MAX_STATIONARY_MINUTES           = 5,
		                  POLLING_INTERVAL_MS              = 1_000,
		                  GPS_UPDATE_TIME_LIMIT_IN_MINUTES = 1;

		private static readonly Lock PointsLock = new(),
		                             ServerLock = new();


		private static DateTimeOffset LastGpsUpdate = DateTimeOffset.MinValue;

		private static readonly GpsPoints GpsPoints = [];
		private static          bool      CollectingGpsPoints;

		private static readonly Timer PollingTimer = new( POLLING_INTERVAL_MS ) { AutoReset = true };

		private static IJSRuntime JsRuntime { get; set; } = null!;


		private static bool IsDistanceOk( DateTimeOffset now, double lat, double lon )
		{
			var Count = GpsPoints.Count;

			if( Count > 0 )
			{
				var LastPoint = GpsPoints[ Count - 1 ];
				return !IsStationary( lat, lon, LastPoint ) || StationaryTooLong( now, LastPoint );
			}
			return true;
		}

		private static bool StationaryTooLong( DateTimeOffset now, GpsPoint lastPoint )
		{
			var Minutes = ( now - lastPoint.DateTime ).TotalMinutes;
			return Minutes >= MAX_STATIONARY_MINUTES;
		}

		private static bool IsStationary( double lat, double lon, GpsPoint lastPoint )
		{
			var Distance         = Measurement.SimpleGpsDistance( lastPoint.Latitude, lastPoint.Longitude, lat, lon );
			var DistanceInMetres = Math.Abs( Distance.Metres );
			return DistanceInMetres < MIN_DISTANCE_IN_METRES;
		}

		private static async void PollingTimerOnElapsed( object? sender, ElapsedEventArgs? e )
		{
			try
			{
				if( CollectingGpsPoints )
				{
					var (Latitude, Longitude) = await GetLocation();
					var Now = DateTimeOffset.Now;

					if( IsDistanceOk( Now, Latitude, Longitude ) )
					{
						lock( PointsLock )
							GpsPoints.Add( new GpsPoint( Now, Latitude, Longitude ) );

						var UpdatePoints = new List<GpsPoint>();

						ServerLock.Enter();

						try
						{
							if( ( Now - LastGpsUpdate ).TotalMinutes >= GPS_UPDATE_TIME_LIMIT_IN_MINUTES )
							{
								lock( PointsLock )
								{
									UpdatePoints.AddRange( GpsPoints );
									GpsPoints.Clear();
								}
							}

							if( UpdatePoints.Count > 0 )
							{
								await IClient.Instance.UpdateGpsPointsForDriver( Login.Driver, UpdatePoints );
								LastGpsUpdate = Now;
							}
						}
						catch
						{
							lock( PointsLock )
							{
								var SavePoints = new GpsPoints();
								SavePoints.AddRange( GpsPoints );
								GpsPoints.Clear();
								GpsPoints.AddRange( UpdatePoints );
								GpsPoints.AddRange( SavePoints );
							}
						}
						finally
						{
							ServerLock.Exit();
						}
					}
				}
			}
			catch
			{
			}
		}


		public static async Task<GpsPoint> CurrentGpsPoint()
		{
			var (Latitude, Longitude) = await GetLocation();

			if( ( Latitude == 0 ) && ( Longitude == 0 ) ) // return last known location
			{
				lock( PointsLock )
				{
					var Count = GpsPoints.Count;

					if( Count > 0 )
						return GpsPoints[ Count - 1 ];
				}
			}

			return new GpsPoint( DateTimeOffset.Now, Latitude, Longitude );
		}

		public record Location( double Latitude, double Longitude );

		public static void Initialise( IJSRuntime jsRuntime )
		{
			JsRuntime            =  jsRuntime;
			PollingTimer.Elapsed += PollingTimerOnElapsed;
		}


		public static async Task<Location> GetLocation()
		{
			try
			{
				return await JsRuntime.InvokeAsync<Location>( "getGeolocation" );
			}
		#if DEBUG
			catch( Exception Ex )
			{
				Console.WriteLine( Ex );
			}
		#else
			catch
			{
			}
		#endif
			return new Location( 0, 0 );
		}

		public static void StartCollecting()
		{
			lock( PointsLock )
			{
				GpsPoints.Clear();
				CollectingGpsPoints = true;
				PollingTimerOnElapsed( null, null );
				PollingTimer.Start();
			}
		}

		public static void StopCollecting()
		{
			CollectingGpsPoints = false;
		}
	}
}