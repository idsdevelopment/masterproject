﻿namespace App;

public partial class Globals
{
	public class MainLayout
	{
		public static Layout.MainLayout? Instance { get; set; } = null!;

		public static bool MenuVisible
		{
			get => Instance?.MenuVisible ?? false;
			set
			{
				if( Instance is not null )
					Instance.MenuVisible = value;
			}
		}

		public static string Title
		{
			get => Instance?.Title ?? "";
			set
			{
				if( Instance is not null )
					Instance.Title = value;
			}
		}

		// ReSharper disable once MemberHidesStaticFromOuterClass
		public static bool IsClient => Instance?.IsClient ?? false;
	}

	public static bool IsClient => MainLayout.IsClient;
}