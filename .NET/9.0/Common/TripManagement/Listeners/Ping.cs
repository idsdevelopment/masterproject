﻿using AzureRemoteService;
using Timer = System.Timers.Timer;

namespace TripManagement.Listeners;

public static class Ping
{
	private const int PING_INTERVAL_IN_SECONDS = 5;

	private static readonly Timer PingTimer = new( PING_INTERVAL_IN_SECONDS * 1_000 );

	private static bool InPing;

	static Ping()
	{
		PingTimer.Elapsed += async ( _, _ ) =>
		                     {
			                     if( !InPing )
			                     {
				                     InPing = true;

				                     try
				                     {
					                     void SetOnline( bool online )
					                     {
						                     Globals.OnPing.Value = online;
					                     }

					                     try
					                     {
						                     var OnLine = ( await Azure.Client.RequestPing() ).Compare( "OK", StringComparison.OrdinalIgnoreCase ) == 0;
						                     SetOnline( OnLine );
					                     }
					                     catch
					                     {
						                     SetOnline( false );
					                     }
				                     }
				                     finally
				                     {
					                     InPing = false;
				                     }
			                     }
		                     };

		PingTimer.Start();
	}

	public static void Initialise()
	{
	}
}