﻿using System;
using System.Windows.Forms;
using TrwBarcode.Qr;

namespace BarcodeTesterWindowsForms;

public partial class Form1 : Form
{
	public Form1()
	{
		InitializeComponent();
		ErrorCorrectionCombo.SelectedIndex = 0;
		MaskCombo.SelectedIndex            = 0;
		VersionCombo.SelectedIndex         = 0;
	}

	private bool                          Debug;
	private QrProperties.ERROR_CORRECTION ErrorCorrection = QrProperties.ERROR_CORRECTION.AUTO;
	private QrProperties.MASK_PATTERN     Mask            = QrProperties.MASK_PATTERN.AUTO;
	private byte                          Version;

	private void button1_Click( object sender, EventArgs e )
	{
		var BCode = new TBarcode.TrwBarcode
					{
						BarcodeType = TBarcode.TrwBarcode.BARCODE_TYPE.QR,
						Height      = 600,
						Width       = 600,
						QrProperties =
						{
							Debug           = Debug,
							Version         = Version,
							ErrorCorrection = ErrorCorrection,
							Data            = ( Version == 0 ? QrProperties.ENCODING.AUTO : QrProperties.ENCODING.ALPHANUMERIC, Entry.Text ),
							MaskPattern     = Mask
						}
					};

		Image.Image = System.Drawing.Image.FromStream( BCode.AsMemoryStream( TBarcode.TrwBarcode.IMAGE_FORMAT.BMP ) );

		var Props = BCode.QrProperties;
		VersionText.Text         = Props.Version.ToString();
		ErrorCorrectionText.Text = Props.ErrorCorrection.ToString();
		MaskText.Text            = Props.MaskPatternUsed.ToString();
	}

	private void ErrorCorrectionCombo_SelectedIndexChanged( object sender, EventArgs e )
	{
		ErrorCorrection = (QrProperties.ERROR_CORRECTION)ErrorCorrectionCombo.SelectedIndex;
	}

	private void MaskCombo_SelectedIndexChanged( object sender, EventArgs e )
	{
		Mask = (QrProperties.MASK_PATTERN)MaskCombo.SelectedIndex - 1;
	}

	private void VersionCombo_SelectedIndexChanged( object sender, EventArgs e )
	{
		Version = (byte)VersionCombo.SelectedIndex;
	}

	private void DebugCheckBox_CheckedChanged( object sender, EventArgs e )
	{
		Debug = DebugCheckBox.Checked;
		button1_Click( sender, e );
	}
}