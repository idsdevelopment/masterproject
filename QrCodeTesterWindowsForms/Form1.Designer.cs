﻿namespace BarcodeTesterWindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.Image = new System.Windows.Forms.PictureBox();
            this.Entry = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ErrorCorrectionCombo = new System.Windows.Forms.ComboBox();
            this.VersionCombo = new System.Windows.Forms.ComboBox();
            this.MaskCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.VersionText = new System.Windows.Forms.Label();
            this.ErrorCorrectionText = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.MaskText = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DebugCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Image)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 248);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Encode";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Image
            // 
            this.Image.Location = new System.Drawing.Point(520, 248);
            this.Image.Name = "Image";
            this.Image.Size = new System.Drawing.Size(844, 532);
            this.Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Image.TabIndex = 1;
            this.Image.TabStop = false;
            // 
            // Entry
            // 
            this.Entry.Location = new System.Drawing.Point(510, 31);
            this.Entry.Multiline = true;
            this.Entry.Name = "Entry";
            this.Entry.Size = new System.Drawing.Size(844, 149);
            this.Entry.TabIndex = 2;
            this.Entry.Text = "ZXS5413440604";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Error Correction";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Version";
            // 
            // ErrorCorrectionCombo
            // 
            this.ErrorCorrectionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ErrorCorrectionCombo.FormattingEnabled = true;
            this.ErrorCorrectionCombo.Items.AddRange(new object[] {
            "  7% - L",
            "15% - M",
            "25% - Q",
            "30% - H"});
            this.ErrorCorrectionCombo.Location = new System.Drawing.Point(34, 134);
            this.ErrorCorrectionCombo.Name = "ErrorCorrectionCombo";
            this.ErrorCorrectionCombo.Size = new System.Drawing.Size(121, 21);
            this.ErrorCorrectionCombo.TabIndex = 5;
            this.ErrorCorrectionCombo.SelectedIndexChanged += new System.EventHandler(this.ErrorCorrectionCombo_SelectedIndexChanged);
            // 
            // VersionCombo
            // 
            this.VersionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VersionCombo.FormattingEnabled = true;
            this.VersionCombo.Items.AddRange(new object[] {
            "AUTO ",
            "1",
            " 2",
            " 3",
            " 4",
            " 5",
            " 6",
            " 7",
            " 8",
            " 9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40"});
            this.VersionCombo.Location = new System.Drawing.Point(34, 47);
            this.VersionCombo.Name = "VersionCombo";
            this.VersionCombo.Size = new System.Drawing.Size(121, 21);
            this.VersionCombo.TabIndex = 6;
            this.VersionCombo.SelectedIndexChanged += new System.EventHandler(this.VersionCombo_SelectedIndexChanged);
            // 
            // MaskCombo
            // 
            this.MaskCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MaskCombo.FormattingEnabled = true;
            this.MaskCombo.Items.AddRange(new object[] {
            "AUTO",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.MaskCombo.Location = new System.Drawing.Point(34, 199);
            this.MaskCombo.Name = "MaskCombo";
            this.MaskCombo.Size = new System.Drawing.Size(121, 21);
            this.MaskCombo.TabIndex = 8;
            this.MaskCombo.SelectedIndexChanged += new System.EventHandler(this.MaskCombo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mask";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(507, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "V ersion:";
            // 
            // VersionText
            // 
            this.VersionText.AutoSize = true;
            this.VersionText.ForeColor = System.Drawing.Color.Blue;
            this.VersionText.Location = new System.Drawing.Point(561, 207);
            this.VersionText.Name = "VersionText";
            this.VersionText.Size = new System.Drawing.Size(0, 13);
            this.VersionText.TabIndex = 10;
            // 
            // ErrorCorrectionText
            // 
            this.ErrorCorrectionText.AutoSize = true;
            this.ErrorCorrectionText.ForeColor = System.Drawing.Color.Blue;
            this.ErrorCorrectionText.Location = new System.Drawing.Point(823, 207);
            this.ErrorCorrectionText.Name = "ErrorCorrectionText";
            this.ErrorCorrectionText.Size = new System.Drawing.Size(0, 13);
            this.ErrorCorrectionText.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(737, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Error Correction:";
            // 
            // MaskText
            // 
            this.MaskText.AutoSize = true;
            this.MaskText.ForeColor = System.Drawing.Color.Blue;
            this.MaskText.Location = new System.Drawing.Point(1083, 207);
            this.MaskText.Name = "MaskText";
            this.MaskText.Size = new System.Drawing.Size(0, 13);
            this.MaskText.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1051, 207);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Mask: ";
            // 
            // DebugCheckBox
            // 
            this.DebugCheckBox.AutoSize = true;
            this.DebugCheckBox.Location = new System.Drawing.Point(219, 250);
            this.DebugCheckBox.Name = "DebugCheckBox";
            this.DebugCheckBox.Size = new System.Drawing.Size(65, 21);
            this.DebugCheckBox.TabIndex = 15;
            this.DebugCheckBox.Text = "Debug";
            this.DebugCheckBox.UseVisualStyleBackColor = true;
            this.DebugCheckBox.CheckedChanged += new System.EventHandler(this.DebugCheckBox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1414, 923);
            this.Controls.Add(this.DebugCheckBox);
            this.Controls.Add(this.MaskText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ErrorCorrectionText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.VersionText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.MaskCombo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.VersionCombo);
            this.Controls.Add(this.ErrorCorrectionCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Entry);
            this.Controls.Add(this.Image);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox Image;
        private System.Windows.Forms.TextBox Entry;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ErrorCorrectionCombo;
        private System.Windows.Forms.ComboBox VersionCombo;
        private System.Windows.Forms.ComboBox MaskCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label VersionText;
        private System.Windows.Forms.Label ErrorCorrectionText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label MaskText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox DebugCheckBox;
    }
}

