﻿var map = null,
    pin = null,

    lineSource = null,
    pinSource = null,
    popupSource = null;

var animation;
var animationTime = 15000;

var RoutePoints;
var AnimationType;

var Loop = false;
var Reverse = false;
var RotationOffset = 0; // 90
var pipeline;
var routeURL;
var Markers = null;


//Define an HTML template for a custom popup content layout.
const popupTemplate =
    '<div class="azureMapPopup"><div class="name">{name}</div><pre class="desc">{description}</pre></div>';


function InitialiseAzureMap(mapId, centreLatitude, centreLongitude, key) {
    //Initialize a map instance.
    map = new atlas.Map(mapId,
        {
            center: [centreLongitude, centreLatitude],
            zoom: 12,
            language: "en-US",
            authOptions: {
                authType: "subscriptionKey",
                subscriptionKey: key
            }
        });

    pipeline = atlas.service.MapsURL.newPipeline(new atlas.service.MapControlCredential(map));
// ReSharper disable once InconsistentNaming
    routeURL = new atlas.service.RouteURL(pipeline);
}

function BeginAzureRoute() {
    RoutePoints = [];
    if (map != null) {
        Markers = [];
    }
}

function AddAzurePoint(latitude, longitude, dateTimeAsString) {
    // console.log(latitude + ", " + longitude + ", " + dateTimeAsString);

    RoutePoints.push(new atlas.data.Feature(new atlas.data.Point([longitude, latitude]),
        { _timestamp: new Date(dateTimeAsString).getTime() }));
}

function AddAzurePointAAndMarker(latitude,
    longitude,
    dateTimeAsString,
    pinText,
    pinType,
    popupName = "",
    popupText = "") {
    AddAzurePoint(latitude, longitude, dateTimeAsString);
    Markers.push([latitude, longitude, pinText, pinType, popupName, popupText]);
}

function SetAzureAnimation(animationType) {
    AnimationType = animationType.toLowerCase;
    switch (AnimationType) {
    case "none":
        Loop = false;
        Reverse = false;
        RotationOffset = 0;
        break;

    default:
        break;
    }
}


function ShowAzureRoute(centreLatitude, centreLongitude) {
    //Load a custom image icon into the map resources.
    map.imageSprite.createFromTemplate("arrow-icon", "marker-arrow", "teal", "#fff").then(function() {

        if (lineSource != null) {
            lineSource.clear();
            pinSource.clear();
            popupSource.clear();
        } else {
            lineSource = new atlas.source.DataSource();
            pinSource = new atlas.source.DataSource();
            popupSource = new atlas.source.DataSource();
            map.sources.add([lineSource, pinSource, popupSource]);
        }


        //Create a layer to render the path.
        map.layers.add(new atlas.layer.LineLayer(lineSource,
            null,
            {
                strokeColor: "#bf00ff",
                strokeWidth: 3
            }));

        //Create a layer to render a symbol which we will animate.
        const symbolLayer = new atlas.layer.SymbolLayer(pinSource);
        map.layers.add(symbolLayer);

        const popupLayer = new atlas.layer.SymbolLayer(popupSource);
        map.layers.add(popupLayer);

        //Create a pin and wrap with the shape class and add to data source.
        pin = new atlas.Shape(RoutePoints[0]);
        pinSource.add(pin);

        //Create the animation.
        animation = atlas.animations.moveAlongRoute(RoutePoints,
            pin,
            {
                //Specify the property that contains the timestamp.
                timestampProperty: "timestamp",

                //Capture metadata so that data driven styling can be done.
                captureMetadata: true,

                loop: Loop,
                reverse: Reverse,
                rotationOffset: RotationOffset,

                //Animate such that 1 second of animation time = 1 minute of data time.
                speedMultiplier: 60,

                //If following enabled, add a map to the animation.
                map: map,

                //Camera options to use when following.
                zoom: 5,
                pitch: 45,
                rotate: true
            });

        snapPointsToRoute();

        //Create a popup but leave it closed so we can update it and display it later.
        var popup = new atlas.Popup({
            pixelOffset: [0, -18],
            closeButton: false,
            showPointer: false

        });
        popup.setOptions();


        //Add a hover event to the symbol layer.
        map.events.add("mouseover",
            popupLayer,
            function(e) {
                const shapes = e.shapes;
                //Make sure that the point exists.
                if (shapes && shapes.length > 0) {
                    const properties = shapes[0].getProperties();
                    const content = popupTemplate.replace(/{name}/g, properties.name)
                        .replace(/{description}/g, properties.description);
                    const coordinate = e.shapes[0].getCoordinates();

                    popup.setOptions({
                        //Update the content of the popup.
                        content: content,

                        //Update the popup's position with the symbol's coordinate.
                        position: coordinate

                    });
                    //Open the popup.
                    popup.open(map);
                }
            });

        map.events.add("mouseleave",
            popupLayer,
            function() {
                popup.close();
            });
    });

    function snapPointsToRoute() {
        //Get all the GPS trace data from the datasource as GeoJSON and create an array.
        //  const points = RoutePoints.toJson().features;

        //Extract the Point geometries from the array of features and use them as supporting points in the route request.
        const supportingPoints = RoutePoints.map((val) => {
            return val.geometry;
        });

        //When reconstructing a route, the start and end coordinates must be specified in the query.
        const coordinates = [
            RoutePoints[0].geometry.coordinates, RoutePoints[RoutePoints.length - 1].geometry.coordinates
        ];

        //Pass all coordinates to reconstruct the route and create a logical path as supporting points in the body of the request.
        const options = {
            postBody: {
                "supportingPoints": {
                    "type": "GeometryCollection",
                    "geometries": supportingPoints
                }
            }
        };

        //Calculate a route.
        routeURL.calculateRouteDirections(atlas.service.Aborter.timeout(10000), coordinates, options).then(
            (directions) => {
                //Get the logical route path as GeoJSON and add it to the data source.
                var data = directions.geojson.getFeatures();
                lineSource.clear();
                lineSource.add(data);

                map.markers.clear();

                Markers.forEach(marker => {
                    var [lat, lng, pText, pinType, popupName, popupText] = marker;

                    //Create a HTML marker and add it to the map.
                    map.markers.add(last = new atlas.HtmlMarker({
                        color: pinType === 0 ? "DodgerBlue" : pinType === 1 ? "Orange" : "#F4F10D",
                        text: pText,
                        position: [lng, lat]

                    }));


                    if (popupName !== "" || popupText !== "") {
                        popupSource.add(new atlas.data.Feature(new atlas.data.Point([lng, lat]),
                            {
                                name: popupName,
                                description: popupText
                            }));
                    }

                });

                //Update the map view to center over the route.
                map.setCamera({
                    bounds: data.bbox,
                    padding: 40 //Add a padding to account for the pixel size of symbols.
                });
            });
    }
}