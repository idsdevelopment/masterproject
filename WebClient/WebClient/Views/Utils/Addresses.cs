﻿using System.Text;

namespace WebClient.Views.Utils;

public class Addresses
{
	public static string ToString( string companyName, string suite, string addressLine1, string addressLine2, string city, string region, string country, string postalCode )
	{
		var Sb = new StringBuilder( companyName.Trim() ).Append( '\n' )
		                                                .Append( $"{suite.Trim()} {addressLine1.Trim()}\n" )
		                                                .Append( $"{addressLine2.Trim()}\n" )
		                                                .Append( $"{city.Trim()} {region.Trim()}\n" )
		                                                .Append( $"{country.Trim()} {postalCode.Trim()}".Trim() );

		return Sb.Replace( "\n\n", "\n" ).Replace(" \n", "\n").ToString().Trim();
	}
}