using Components.AzureMaps;
using Utils.Csv;
using Addresses = WebClient.Views.Utils.Addresses;

// ReSharper disable InconsistentNaming

// ReSharper disable LocalVariableHidesMember

namespace Client.Views.Routing;

public class DisplayEntry
{
	public RouteEntry.PIN_TYPE PinType { get; init; } = RouteEntry.PIN_TYPE.NONE;
	public string              PinText { get; init; } = "";


#region Address
	public string DisplayAddress => _DisplayAddress ??= Addresses.ToString( "", Suite, AddressLine1, AddressLine2, City, Region, Country, PostalCode );

	private string? _DisplayAddress;

	public string CompanyName  { get; init; } = "";
	public string Suite        { get; init; } = "";
	public string AddressLine1 { get; init; } = "";
	public string AddressLine2 { get; init; } = "";
	public string City         { get; init; } = "";
	public string Region       { get; init; } = "";
	public string Country      { get; init; } = "";
	public string PostalCode   { get; init; } = "";
#endregion

#region Duration
	public decimal DurationInMinutes { get; init; }

	private string? _DisplayDuration;
	public  string  DisplayDuration => _DisplayDuration ??= $"{new TimeSpan( 0, (int)DurationInMinutes, 0 ):g}";
#endregion

#region Distance
	public bool UseMiles;

	public decimal DistanceInMetres { get; init; }

	private string? _DisplayDistance;

	public string DisplayDistance
	{
		get
		{
			if( _DisplayDistance is null )
			{
				var M = new Measurement
				        {
					        Metres = DistanceInMetres
				        };

				_DisplayDistance = $"{( UseMiles ? M.Miles : M.KiloMetres ):N1}";
			}
			return _DisplayDistance;
		}
	}
#endregion
}

public class ManifestEntry
{
	public string Account     = "",
	              Reference   = "",
	              CallerName  = "",
	              CallerPhone = "",
	              CallerEmail = "",
	              //
	              PickupCompany    = "",
	              PickupContact    = "",
	              PickupPhone      = "",
	              PickupLocation   = "",
	              PickupSuite      = "",
	              PickupStreet     = "",
	              PickupCity       = "",
	              PickupCountry    = "",
	              PickupNotes      = "",
	              PickupPostalCode = "",
	              PickupRegion     = "",
	              //
	              DeliveryCompany    = "",
	              DeliveryContact    = "",
	              DeliveryPhone      = "",
	              DeliveryLocation   = "",
	              DeliverySuite      = "",
	              DeliveryStreet     = "",
	              DeliveryCity       = "",
	              DeliveryCountry    = "",
	              DeliveryNotes      = "",
	              DeliveryPostalCode = "",
	              DeliveryRegion     = "",
	              //
	              ServiceLevel = "",
	              ReadyBy      = "",
	              DueDate      = "",
	              PackageType  = "",
	              Pieces       = "",
	              Weight       = "",
	              Length       = "",
	              Width        = "",
	              Height       = "",
	              Driver       = "",
	              Route        = "",
	              TripId       = "";
}

public class ManifestViewModel : ViewModelBase
{
	private const string ACCOUNT      = "account",
	                     REFERENCE    = "reference",
	                     CALLER_NAME  = "caller name",
	                     CALLER_PHONE = "caller phone",
	                     CALLER_EMAIL = "caller email",
//
	                     PICKUP_COMPANY     = "pickup company name",
	                     PICKUP_CONTACT     = "pickup contact",
	                     PICKUP_PHONE       = "pickup phone",
	                     PICKUP_LOCATION    = "pickup location barcode",
	                     PICKUP_SUITE       = "pickup suite",
	                     PICKUP_STREET      = "pickup street",
	                     PICKUP_CITY        = "pickup city",
	                     PICKUP_COUNTRY     = "pickup country",
	                     PICKUP_NOTES       = "pickup notes",
	                     PICKUP_POSTAL_CODE = "pickup postal/zip",
	                     PICKUP_REGION      = "pickup state/province",
//
	                     DELIVERY_COMPANY     = "delivery company name",
	                     DELIVERY_CONTACT     = "delivery contact",
	                     DELIVERY_PHONE       = "delivery phone",
	                     DELIVERY_LOCATION    = "delivery location barcode",
	                     DELIVERY_SUITE       = "delivery suite",
	                     DELIVERY_STREET      = "delivery street",
	                     DELIVERY_CITY        = "delivery city",
	                     DELIVERY_COUNTRY     = "delivery country",
	                     DELIVERY_NOTES       = "delivery notes",
	                     DELIVERY_POSTAL_CODE = "delivery postal/zip",
	                     DELIVERY_REGION      = "delivery state/province",
//
	                     SERVICE_LEVEL = "service level",
	                     READY_BY      = "ready by",
	                     DUE_BY        = "due by",
	                     PACKAGE_TYPE  = "package type",
	                     PIECES        = "pieces",
	                     WEIGHT        = "weight",
	                     LENGTH        = "length",
	                     WIDTH         = "width",
	                     HEIGHT        = "height",
	                     DRIVER        = "driver",
	                     ROUTE         = "route",
	                     TRIP_ID       = "shipment id";


	public ManifestViewModel( Action? stateChanged ) : base( stateChanged )
	{
	}


#region Route
	public List<DisplayEntry> DisplayAddresses
	{
		get { return Get( () => DisplayAddresses, new List<DisplayEntry>() ); }
		set { Set( () => DisplayAddresses, value ); }
	}

	public IEnumerable<RouteEntry>? Route
	{
		get { return Get( () => Route, (IEnumerable<RouteEntry>?)null ); }
		set { Set( () => Route, value ); }
	}
#endregion

#region File Map
	private enum REQUIRED
	{
		MANDATORY,
		OPTIONAL
	}

	[Flags]
	public enum ERRORS : byte
	{
		NONE,
		IMPORT_FILE = 1
	}

	private static Dictionary<string, (REQUIRED Required, int Index)> NewFieldMap() => new()
	                                                                                   {
		                                                                                   {ACCOUNT, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {REFERENCE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {CALLER_NAME, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {CALLER_PHONE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {CALLER_EMAIL, ( REQUIRED.OPTIONAL, -1 )},

		                                                                                   {PICKUP_COMPANY, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PICKUP_CONTACT, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PICKUP_PHONE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PICKUP_LOCATION, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PICKUP_SUITE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PICKUP_STREET, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {PICKUP_CITY, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {PICKUP_REGION, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {PICKUP_COUNTRY, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PICKUP_POSTAL_CODE, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {PICKUP_NOTES, ( REQUIRED.OPTIONAL, -1 )},

		                                                                                   {DELIVERY_COMPANY, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DELIVERY_CONTACT, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DELIVERY_PHONE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DELIVERY_LOCATION, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DELIVERY_SUITE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DELIVERY_STREET, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {DELIVERY_CITY, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {DELIVERY_REGION, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {DELIVERY_COUNTRY, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DELIVERY_POSTAL_CODE, ( REQUIRED.MANDATORY, -1 )},
		                                                                                   {DELIVERY_NOTES, ( REQUIRED.OPTIONAL, -1 )},

		                                                                                   {SERVICE_LEVEL, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {READY_BY, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DUE_BY, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PACKAGE_TYPE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {PIECES, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {WEIGHT, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {LENGTH, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {WIDTH, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {HEIGHT, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {DRIVER, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {ROUTE, ( REQUIRED.OPTIONAL, -1 )},
		                                                                                   {TRIP_ID, ( REQUIRED.OPTIONAL, -1 )}
	                                                                                   };
#endregion

#region Errors
	public bool HasErrors
	{
		get { return Get( () => HasErrors, false ); }
		set { Set( () => HasErrors, value ); }
	}

	public List<string> ErrorList
	{
		get { return Get( () => ErrorList, new List<string>() ); }
		set { Set( () => ErrorList, value ); }
	}

#region Invalid Addresses
	public bool HasInvalidAddresses
	{
		get { return Get( () => HasInvalidAddresses, false ); }
		set { Set( () => HasInvalidAddresses, value ); }
	}


	public AddressValidList InvalidAddresses
	{
		get { return Get( () => InvalidAddresses, new AddressValidList() ); }
		set { Set( () => InvalidAddresses, value ); }
	}
#endregion
#endregion


#region Busy
	public enum BUSY_STATE
	{
		NOT_BUSY,
		VALIDATING,
		CALCULATING
	}

	public Action<BUSY_STATE>? OnBusy;

	public BUSY_STATE Busy
	{
		get { return Get( () => Busy, BUSY_STATE.NOT_BUSY ); }
		set { Set( () => Busy, value ); }
	}

	[DependsUpon( nameof( Busy ), Delay = 500 )]
	public void WhenBusyChanges()
	{
		OnBusy?.Invoke( Busy );
	}
#endregion

#region Csv
	public Action<bool>? ManifestUploaded = null;

	public byte[] CsvBytes

	{
		get { return Get( () => CsvBytes, Array.Empty<byte>() ); }
		set { Set( () => CsvBytes, value ); }
	}

	[DependsUpon( nameof( CsvBytes ) )]
	public async void WhenCsvBytesChange()
	{
		var Bytes = CsvBytes;

		if( Bytes.Length > 0 )
		{
			Busy = BUSY_STATE.VALIDATING;

			var Addresses = new RouteOptimisationCompanyAddresses
			                {
				                TravelMode = RouteBase.TRAVEL_MODE.SHORTEST | RouteBase.TRAVEL_MODE.PICKUP_AND_DELIVERIES | RouteBase.TRAVEL_MODE.AVOID_UNPAVED_ROADS
			                };

			try
			{
				var Errors   = new List<string>();
				var FieldMap = NewFieldMap();
				var Manifest = new List<ManifestEntry>();

				var Csv = new Csv( Bytes ).TrimEnd();

				var Count = Csv.RowCount;

				bool Error;

				try
				{
					for( var Ndx = 0; Ndx < Count; Ndx++ )
					{
						var Row = Csv[ Ndx ];

						if( Ndx == 0 ) // Header
						{
							var ColNum = 0;

							foreach( var Col in Row )
							{
								var CsvText = Col.AsString.Trim();
								var Text    = CsvText.Pack( false ).ToLower().Trim( '*', ' ' );

								if( FieldMap.TryGetValue( Text, out var Required ) )
									FieldMap[ Text ] = ( Required.Required, ColNum );
								else
									Errors.Add( $"Unknown Column: {CsvText}" );

								ColNum++;
							}

							Errors.AddRange( from F in FieldMap
							                 let R = F.Value
							                 where ( R.Required == REQUIRED.MANDATORY ) && ( R.Index < 0 )
							                 select $"Missing required column: {F.Key.Capitalise()}" );

							if( Errors.Count > 0 )
							{
								ErrorList = Errors;
								HasErrors = true;
								return;
							}
						}
						else
						{
							var Entry = new ManifestEntry();
							Manifest.Add( Entry );

							foreach( var (Key, Value) in FieldMap )
							{
								var Index = Value.Index;

								if( Index >= 0 )
								{
									var CellValue = Row[ Index ].AsString.Pack();

									if( CellValue == "" )
									{
										if( !FieldMap.TryGetValue( Key, out var Required ) )
											Errors.Add( $"Lost Column: {Key}" );

										else if( Required.Required == REQUIRED.MANDATORY )
											Errors.Add( $"Line: {Index + 1}, Required Field: \"{Key}\", Is Empty" );
									}

									switch( Key )
									{
									case ACCOUNT:
										Entry.Account = CellValue;
										break;

									case REFERENCE:
										Entry.Reference = CellValue;
										break;

									case CALLER_NAME:
										Entry.CallerName = CellValue;
										break;

									case CALLER_PHONE:
										Entry.CallerPhone = CellValue;
										break;

									case CALLER_EMAIL:
										Entry.CallerEmail = CellValue;
										break;

									case PICKUP_COMPANY:
										Entry.PickupCompany = CellValue;
										break;

									case PICKUP_CONTACT:
										Entry.PickupContact = CellValue;
										break;

									case PICKUP_PHONE:
										Entry.PickupPhone = CellValue;
										break;

									case PICKUP_LOCATION:
										Entry.PickupLocation = CellValue;
										break;

									case PICKUP_SUITE:
										Entry.PickupSuite = CellValue;
										break;

									case PICKUP_STREET:
										Entry.PickupStreet = Global.AddressesUtils.FixAddressLine( CellValue );
										break;

									case PICKUP_CITY:
										Entry.PickupCity = CellValue;
										break;

									case PICKUP_COUNTRY:
										Entry.PickupCountry = CellValue;
										break;

									case PICKUP_NOTES:
										Entry.PickupNotes = CellValue;
										break;

									case PICKUP_POSTAL_CODE:
										Entry.PickupPostalCode = CellValue;
										break;

									case PICKUP_REGION:
										Entry.PickupRegion = CellValue;
										break;

//
									case DELIVERY_COMPANY:
										Entry.DeliveryCompany = CellValue;
										break;

									case DELIVERY_CONTACT:
										Entry.DeliveryContact = CellValue;
										break;

									case DELIVERY_PHONE:
										Entry.DeliveryPhone = CellValue;
										break;

									case DELIVERY_LOCATION:
										Entry.DeliveryLocation = CellValue;
										break;

									case DELIVERY_SUITE:
										Entry.DeliverySuite = CellValue;
										break;

									case DELIVERY_STREET:
										Entry.DeliveryStreet = Global.AddressesUtils.FixAddressLine( CellValue );
										break;

									case DELIVERY_CITY:
										Entry.DeliveryCity = CellValue;
										break;

									case DELIVERY_COUNTRY:
										Entry.DeliveryCountry = CellValue;
										break;

									case DELIVERY_NOTES:
										Entry.DeliveryNotes = CellValue;
										break;

									case DELIVERY_POSTAL_CODE:
										Entry.DeliveryPostalCode = CellValue;
										break;

									case DELIVERY_REGION:
										Entry.DeliveryRegion = CellValue;
										break;

//
									case SERVICE_LEVEL:
										Entry.ServiceLevel = CellValue;
										break;

									case READY_BY:
										Entry.ReadyBy = CellValue;
										break;

									case DUE_BY:
										Entry.DueDate = CellValue;
										break;

									case PACKAGE_TYPE:
										Entry.PackageType = CellValue;
										break;

									case PIECES:
										Entry.Pieces = CellValue;
										break;

									case WEIGHT:
										Entry.Weight = CellValue;
										break;

									case LENGTH:
										Entry.Length = CellValue;
										break;

									case WIDTH:
										Entry.Width = CellValue;
										break;

									case HEIGHT:
										Entry.Height = CellValue;
										break;

									case DRIVER:
										Entry.Driver = CellValue;
										break;

									case ROUTE:
										Entry.Route = CellValue;
										break;

									case TRIP_ID:
										Entry.TripId = CellValue;
										break;
									}
								}
							}

							if( Errors.Count > 0 )
							{
								ErrorList = Errors;
								HasErrors = true;
								return;
							}
						}
					}

					var Comp = Addresses.Companies;

					foreach( var E in Manifest )
					{
						Comp.Add( new Company
						          {
							          IsPickupCompany = true,

							          CompanyName = E.PickupCompany,
							          Barcode     = E.PickupLocation,

							          Suite        = E.PickupSuite,
							          AddressLine1 = E.PickupStreet,
							          City         = E.PickupCity,
							          Region       = E.PickupRegion,
							          Country      = E.PickupCountry,
							          PostalCode   = E.PickupPostalCode,

							          ContactName = E.PickupContact,
							          Phone       = E.PickupPhone,
							          Notes       = E.PickupNotes
						          } );

						Comp.Add( new Company
						          {
							          CompanyName = E.DeliveryCompany,
							          Barcode     = E.DeliveryLocation,

							          Suite        = E.DeliverySuite,
							          AddressLine1 = E.DeliveryStreet,
							          City         = E.DeliveryCity,
							          Region       = E.DeliveryRegion,
							          Country      = E.DeliveryCountry,
							          PostalCode   = E.DeliveryPostalCode,

							          ContactName = E.DeliveryContact,
							          Phone       = E.DeliveryPhone,
							          Notes       = E.DeliveryNotes
						          } );
					}

					var ValidationList = new AddressValidationList();

					ValidationList.AddRange( from C in Comp
					                         select new AddressValidation
					                                {
						                                CompanyName  = C.CompanyName,
						                                Suite        = C.Suite,
						                                AddressLine1 = C.AddressLine1,
						                                AddressLine2 = C.AddressLine2,
						                                City         = C.City,
						                                Region       = C.Region,
						                                Country      = C.Country,
						                                CountryCode  = C.CountryCode,
						                                PostalCode   = C.PostalCode
					                                } );

					var ValidAddresses = await Azure.Client.RequestValidateAddresses( ValidationList );

					var Invalid = new AddressValidList();

					foreach( var Address in ValidAddresses )
					{
						if( !Address.IsValid )
							Invalid.Add( Address );
					}

					if( Invalid.Count > 0 )
					{
						InvalidAddresses    = Invalid;
						HasInvalidAddresses = true;
					}
				}
				finally
				{
					Busy  = BUSY_STATE.CALCULATING;
					Error = HasErrors || HasInvalidAddresses;
					ManifestUploaded?.Invoke( !Error );
				}

				if( !Error )
				{
					var Rte = await Azure.Client.RequestDistancesWithAddresses( Addresses );
					Route = Rte.ToRoute( DateTimeOffset.Now ).Route;

					var De = ( from RouteEntry in Route
					           let Co = RouteEntry.Company
					           select new DisplayEntry
					                  {
						                  PinType           = RouteEntry.PinType,
						                  PinText           = RouteEntry.PinText,
						                  CompanyName       = Co.CompanyName,
						                  Suite             = Co.Suite,
						                  AddressLine1      = Co.AddressLine1,
						                  AddressLine2      = Co.AddressLine2,
						                  City              = Co.City,
						                  Country           = Co.Country,
						                  Region            = Co.Region,
						                  PostalCode        = Co.PostalCode,
						                  DistanceInMetres  = RouteEntry.Distance,
						                  DurationInMinutes = RouteEntry.Duration
					                  } ).ToList();

					DisplayAddresses = De;
				}
			}
			finally
			{
				CsvBytes = Array.Empty<byte>();
				Busy     = BUSY_STATE.NOT_BUSY;
			}
		}
	}
#endregion
}