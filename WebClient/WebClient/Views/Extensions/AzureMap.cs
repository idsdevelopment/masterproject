﻿using Components.AzureMaps;
using Microsoft.AspNetCore.Components;
using Protocol.Data.Maps.Route;
using Addresses = WebClient.Views.Utils.Addresses;

namespace Client.Views;

public static partial class Extensions
{
	public static (List<RouteEntry> Route, List<MarkupString> Directions) ToRoute( this DistancesAndAddresses? route, DateTimeOffset startTime, bool detailed = false )
	{
		var Route      = new List<RouteEntry>();
		var Directions = new List<MarkupString>();

		if( route is { } Rte )
		{
			var                 Time     = startTime;
			var                 PinNo    = 0;
			Leg?                LastLeg  = null;
			Step?               LastStep = null;
			DistanceAndAddress? LastR    = null;

			foreach( var R in Rte )
			{
				LastR = R;

				foreach( var Leg in R.Route )
				{
					LastLeg = Leg;

					if( Leg.Distance != 0 )
					{
						var NewLeg = true;

						foreach( var Step in Leg.Steps )
						{
							LastStep = Step;

							Directions.Add( new MarkupString( Step.HtmlInstructions ) );

							if( NewLeg )
							{
								NewLeg = false;

								RouteEntry.PIN_TYPE PinType()
								{
									var Points = Leg.StartPickupDeliveryPoints;
									var Pu     = Points.PickupPoints.Count > 0;
									var Del    = Points.DeliveryPoints.Count > 0;

									return Del switch
									       {
										       true when Pu   => RouteEntry.PIN_TYPE.BOTH,
										       true           => RouteEntry.PIN_TYPE.DELIVERY,
										       false when !Pu => RouteEntry.PIN_TYPE.NONE,
										       _              => RouteEntry.PIN_TYPE.PICKUP
									       };
								}

								var C = R.FromCompany;

								Route.Add( new RouteEntry
								           {
									           DateTime  = Time,
									           Latitude  = Maths.Round6( Step.StartLocation.Latitude ),
									           Longitude = Maths.Round6( Step.StartLocation.Longitude ),
									           Distance  = Leg.Distance,
									           Duration  = Leg.Duration,
									           AddPin    = true,
									           PinText   = PinNo == 0 ? "S" : PinNo.ToString(),
									           PinType   = PinType(),
									           Company   = C,
									           PopupName = C.CompanyName.Trim(),
									           PopupText = Addresses.ToString( "", C.Suite, C.AddressLine1, C.AddressLine2, C.City, C.Region, C.Country, C.PostalCode )
								           } );
								++PinNo;
							}
							Time = Time.AddMinutes( (int)Step.Duration );

							if( detailed )
							{
								Route.Add( new RouteEntry
								           {
									           DateTime  = Time,
									           Latitude  = Maths.Round6( Step.EndLocation.Latitude ),
									           Longitude = Maths.Round6( Step.EndLocation.Longitude ),
									           Distance  = Step.Distance
								           } );
							}
						}
					}
				}
			}

			var Cnt = Route.Count;

			if( ( Cnt > 0 ) && LastLeg is { } L && LastStep is { } S )
			{
				var C = LastR?.ToCompany ?? new Company();

				Route.Add( new RouteEntry
				           {
					           DateTime  = Time,
					           Latitude  = Maths.Round6( S.EndLocation.Latitude ),
					           Longitude = Maths.Round6( S.EndLocation.Longitude ),
					           Distance  = L.Distance,
					           Duration  = L.Duration,
					           AddPin    = true,
					           PinText   = "E",
					           PinType   = RouteEntry.PIN_TYPE.DELIVERY,
					           Company   = C,
					           PopupName = C.CompanyName.Trim(),
					           PopupText = Addresses.ToString( "", C.Suite, C.AddressLine1, C.AddressLine2, C.City, C.Region, C.Country, C.PostalCode )
				           } );
			}
		}

		return ( Route, Directions );
	}
}