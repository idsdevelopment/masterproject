﻿namespace Client.Views;

public static partial class Extensions
{
	private const string ERROR = "data-error";

	public static Dictionary<string, object> InError( this Dictionary<string, object> dict, bool inError )
	{
		if( inError )
			dict[ ERROR ] = "";
		else
			dict.Remove( ERROR );

		return dict;
	}

	public static bool IsInError( this Dictionary<string, object> dict ) => dict.ContainsKey( ERROR );
}