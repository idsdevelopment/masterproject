﻿namespace Client.Views;

public static partial class Extensions
{
	private const string SELECTED = "data-selected";

	public static string ToSelected( this bool sel ) => sel ? SELECTED : "";

	public static bool IsSelected( this string sel ) => sel.Trim().Compare( SELECTED, StringComparison.OrdinalIgnoreCase ) == 0;

	public static Dictionary<string, object> Selected( this Dictionary<string, object> dict, bool sel )
	{
		if( sel )
			dict[ SELECTED ] = "";
		else
			dict.Remove( SELECTED );

		return dict;
	}

	public static bool IsSelected( this Dictionary<string, object> dict ) => dict.ContainsKey( SELECTED );
}