﻿namespace Client.Views;

public static partial class Extensions
{
	private const string VISIBLE     = "visible",
	                     COLLAPSE    = "collapse",
	                     DATA_HIDDEN = "data-hidden";

	public static string Visible( this bool visible ) => visible ? VISIBLE : COLLAPSE;

	public static bool IsVisible( this string visible ) => visible.Trim().Compare( VISIBLE, StringComparison.OrdinalIgnoreCase ) == 0;


	public static Dictionary<string, object> Visible( this Dictionary<string, object> dict, bool visible )
	{
		if( !visible )
			dict[ DATA_HIDDEN ] = "";
		else
			dict.Remove( DATA_HIDDEN );

		return dict;
	}

	public static Dictionary<string, object> Hidden( this Dictionary<string, object> dict, bool hidden ) => dict.Visible( !hidden );

	public static bool IsVisible( this Dictionary<string, object> dict ) => !dict.ContainsKey( DATA_HIDDEN );
	public static bool IsHidden( this Dictionary<string, object> dict ) => !dict.IsVisible();
}