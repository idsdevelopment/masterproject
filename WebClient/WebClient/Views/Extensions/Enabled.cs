﻿namespace Client.Views;

public static partial class Extensions
{
	private const string DISABLED        = "data-disabled",
	                     DISABLED_BUTTON = "disabled";

	public static Dictionary<string, object> Disable( this Dictionary<string, object> dict, bool disable )
	{
		return dict.Enable( !disable );
	}

	public static Dictionary<string, object> Enable( this Dictionary<string, object> dict, bool enable )
	{
		if( !enable )
		{
			dict[ DISABLED ]        = "";
			dict[ DISABLED_BUTTON ] = "";
		}
		else
		{
			dict.Remove( DISABLED );
			dict.Remove( DISABLED_BUTTON );
		}

		return dict;
	}

	public static bool IsEnabled( this Dictionary<string, object> dict ) => !dict.ContainsKey( DISABLED ) && !dict.ContainsKey( DISABLED_BUTTON );
}