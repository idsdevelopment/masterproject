﻿namespace Client.Views.SignIn;

public class SignInViewModel : ViewModelBase
{
	[Flags]
	public enum ERRORS : byte
	{
		NONE,
		USER_NAME   = 1,
		PASSWORD    = 1 << 1,
		CREDENTIALS = 1 << 2,
		CARRIER     = 1 << 3,
		ALL         = USER_NAME | PASSWORD | CARRIER
	}


	[Setting]
	public string CarrierId
	{
		get { return Get( () => CarrierId, "" ); }
		set { Set( () => CarrierId, value ); }
	}

	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}


	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}

	private readonly Globals.SignIn.SIGN_IN_TYPE SignInType;

	public Action<ERRORS>? OnError;
	public Action<bool>?   OnSignIn;

	public async void ExecuteSignIn()
	{
		var Ok    = false;
		var PWord = Password;
		var UName = UserName.Trim();

		Azure.Slot = Azure.SLOT.ALPHA_TRW;

		switch( SignInType )
		{
		case Globals.SignIn.SIGN_IN_TYPE.SHIPPING_PORTAL:
			var Codes = await Azure.Client.RequestGetResellerCustomerLoginCredentials( Encryption.ToTimeLimitedToken( UName ),
			                                                                           Encryption.ToTimeLimitedToken( PWord ) );

			if( Codes is not null && Codes.Ok )
			{
				var (AccountId, AccountOk) = Encryption.FromTimeLimitedToken( Codes.AccountId );

				if( AccountOk )
				{
					var (Cid, CarrierOk) = Encryption.FromTimeLimitedToken( Codes.CarrierId );

					if( CarrierOk )
					{
						var (Name, NameOk) = Encryption.FromTimeLimitedToken( Codes.UserName );

						if( NameOk )
						{
							if( ( await Azure.LogIn( Globals.PROGRAM, Cid, Name, PWord ) ).Status == Azure.AZURE_CONNECTION_STATUS.OK )
							{
								Globals.ShippingPortal.AccountId = AccountId;
								Globals.User.CarrierId           = Cid;
								Globals.User.UserName            = Name;
								SaveSettings();
								Globals.SignIn.SignInType = SignInType;
								Ok                        = true;
							}
						}
					}
				}
			}
			break;

		case Globals.SignIn.SIGN_IN_TYPE.CARRIER:
			var Cid1 = CarrierId.Trim();
			var U2   = UserName.Trim();

			if( ( await Azure.LogIn( Globals.PROGRAM, Cid1, U2, Password ) ).Status == Azure.AZURE_CONNECTION_STATUS.OK )
			{
				Globals.ShippingPortal.AccountId = "";
				Globals.User.CarrierId           = Cid1;
				Globals.User.UserName            = U2;
				SaveSettings();
				Globals.SignIn.SignInType = SignInType;
				Ok                        = true;
			}
			break;
		}
		OnSignIn?.Invoke( Ok );
	}

	[DependsUpon250( nameof( CarrierId ) )]
	[DependsUpon250( nameof( UserName ) )]
	[DependsUpon250( nameof( Password ) )]
	public void WhenInputChanges()
	{
		var Errors = ERRORS.NONE;

		if( ( SignInType == Globals.SignIn.SIGN_IN_TYPE.CARRIER ) && ( CarrierId.Length < 6 ) )
			Errors |= ERRORS.CARRIER;

		if( UserName.Trim().Length < 2 )
			Errors |= ERRORS.USER_NAME;

		if( Password.Length < 4 )
			Errors |= ERRORS.PASSWORD;

		OnError?.Invoke( Errors );
	}

	public SignInViewModel( Globals.SignIn.SIGN_IN_TYPE signInType, Func<Task<string>>? onLoadData, Func<string, Task>? onSaveData, Action? osStateChanged ) : base( onLoadData, onSaveData, osStateChanged )
	{
		SignInType = signInType;
	}
}