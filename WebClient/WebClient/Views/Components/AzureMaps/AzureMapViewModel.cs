﻿#nullable enable

namespace Components.AzureMaps;

public static class RouteEntryExtensions
{
	public static string ToString( this RouteEntry.PIN_TYPE p )
	{
		return p switch
		       {
			       RouteEntry.PIN_TYPE.DELIVERY => "Delivery",
			       RouteEntry.PIN_TYPE.PICKUP   => "Pickup",
			       RouteEntry.PIN_TYPE.BOTH     => "Pu & Del",
			       _                            => ""
		       };
	}
}

public class RouteEntry
{
	public enum PIN_TYPE // Must match JavaScript
	{
		NONE = -1,
		PICKUP,
		DELIVERY,
		BOTH
	}

	public bool AddPin;

	public DateTimeOffset DateTime;

	public decimal Latitude,
	               Longitude,
	               Distance,
	               Duration;

	public string PinText   = "",
	              PopupName = "",
	              PopupText = "";

	public PIN_TYPE PinType;

	public Company Company = null!;
}

public class AzureMapViewModel : ViewModelBase
{
	public decimal Latitude
	{
		get { return Get( () => Latitude, 0 ); }
		set { Set( () => Latitude, value ); }
	}


	public decimal Longitude
	{
		get { return Get( () => Longitude, 0 ); }
		set { Set( () => Longitude, value ); }
	}

	private static string? ApiKey;

	public AzureMapViewModel( Action refresh ) : base( refresh )
	{
	}

#region Route
	public IEnumerable<RouteEntry>? Route
	{
		get { return Get( () => Route, (IEnumerable<RouteEntry>?)null ); }
		set { Set( () => Route, value ); }
	}


	[DependsUpon( nameof( Route ) )]
	public async void WhenRouteChanges()
	{
		if( Route is not null )
		{
			await JsBinder.Invoke( "BeginAzureRoute" );

			var         First = true;
			RouteEntry? R     = null;

			foreach( var RouteEntry in Route )
			{
				if( First )
				{
					First = false;
					R     = RouteEntry;
				}

				var Lat  = Maths.Round6( RouteEntry.Latitude );
				var Lng  = Maths.Round6( RouteEntry.Longitude );
				var Date = $"{RouteEntry.DateTime:G}";

				Console.WriteLine( $"--> {Lat}, {Lng}, {Date}, {RouteEntry.PinText}" );

				if( !RouteEntry.AddPin )
					await JsBinder.Invoke( "AddAzurePoint", new object[] {(double)Lat, (double)Lng, Date} );
				else
					await JsBinder.Invoke( "AddAzurePointAAndMarker", new object[]
					                                                  {
						                                                  (double)Lat, (double)Lng, Date,
						                                                  RouteEntry.PinText, RouteEntry.PinType,
						                                                  RouteEntry.PopupName, RouteEntry.PopupText
					                                                  } );
			}
			await JsBinder.Invoke( "SetAzureAnimation", new object[] {Animation.ToLower()} );

			if( R is not null )
				await JsBinder.Invoke( "ShowAzureRoute", new object[] {Maths.Round6AsDouble( R.Latitude ), Maths.Round6AsDouble( R.Longitude )} );
		}
	}
#endregion

#region Options
	public string Header
	{
		get { return Get( () => Header, "" ); }
		set { Set( () => Header, value ); }
	}


	public string HeaderClass
	{
		get { return Get( () => HeaderClass, "" ); }
		set { Set( () => HeaderClass, value ); }
	}


	public string Class
	{
		get { return Get( () => Class, "" ); }
		set { Set( () => Class, value ); }
	}


	public string Animation
	{
		get { return Get( () => Animation, "" ); }
		set { Set( () => Animation, value ); }
	}
#endregion

#region MapId
	public string MapId
	{
		get { return Get( () => MapId, "" ); }
		set { Set( () => MapId, value ); }
	}

	[DependsUpon( nameof( MapId ) )]
	public async void WhenMapIdChanges()
	{
		ApiKey ??= ( await Azure.Client.RequestGetMapsApiKeys() ).AzureMapKey;

		var Service = new GeolocationService( JsRuntime );

		GeolocationResult? Pos;

		try
		{
			Pos = await Service.GetCurrentPosition();
		}
		catch( Exception E )
		{
			Console.WriteLine( E );
			Pos = null;
		}

		if( Pos is not null )
		{
			var Position = Pos.Position;

			var Coords = Position?.Coords;

			if( Coords is not null )
			{
				Latitude  = (decimal)Coords.Latitude;
				Longitude = (decimal)Coords.Longitude;
			}
		}
		await JsBinder.Invoke( "InitialiseAzureMap", new object[] {MapId, (double)Latitude, (double)Longitude, ApiKey} );
	}
#endregion

#region JavaScript
	private JsBinder JsBinder = null!;

	public JSRuntime JsRuntime
	{
		get { return Get( () => JsRuntime, (JSRuntime)null! ); }
		set { Set( () => JsRuntime, value ); }
	}

	[DependsUpon( nameof( JsRuntime ) )]
	public void WhenJsRuntimeChanges()
	{
		JsBinder = new JsBinder( JsRuntime );
	}
#endregion
}