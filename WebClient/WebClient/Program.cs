using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using WebClient;

var Builder        = WebAssemblyHostBuilder.CreateDefault( args );
var RootComponents = Builder.RootComponents;
RootComponents.Add<App>( "#app" );
RootComponents.Add<HeadOutlet>( "head::after" );

var Services = Builder.Services;
Services.AddScoped( _ => new HttpClient {BaseAddress = new Uri( Builder.HostEnvironment.BaseAddress )} );
Services.AddBlazoredLocalStorage();
Services.AddScoped<ClipboardService>();

await Builder.Build().RunAsync();