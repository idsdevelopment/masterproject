﻿using System.Text;

namespace Client;

internal class Global
{
	public class AddressesUtils
	{
		public static string FormAddress( string companyName, string suite, string addr1, string addr2, string vicinity, string city, string state, string postCode )
		{
			var Address = new StringBuilder();

			void AddLine( string text )
			{
				text = text.Trim();

				if( text.IsNotNullOrWhiteSpace() )
					Address.Append( text ).Append( "\r\n" );
			}

			void Add( string text, string extra = "" )
			{
				if( text.IsNotNullOrWhiteSpace() )
					Address.Append( text.Trim() ).Append( extra );
			}

			AddLine( companyName );

			Add( suite, "/" );
			AddLine( addr1 );
			AddLine( addr2 );
			AddLine( vicinity );
			Add( city, " " );
			Add( state, " " );
			Add( postCode, " " );

			return Address.ToString().Trim();
		}

		public static string FormAddress( string companyName, string suite, string addr1, string addr2, string city, string state, string postCode ) => FormAddress( companyName, suite, addr1, addr2, "", city, state, postCode );

		public static string FixAddressLine( string addr )
		{
			addr = addr.TrimToUpper().AlphaNumericAndSpaces( true, true );

			if( addr.LastIndexOf( " RD ", StringComparison.Ordinal ) >= 0 )
				return addr.ReplaceLastOccurrence( " RD ", " ROAD " );

			if( addr.LastIndexOf( " ST ", StringComparison.Ordinal ) >= 0 )
				return addr.ReplaceLastOccurrence( " ST ", " STREET " );

			if( addr.LastIndexOf( " AVE ", StringComparison.Ordinal ) >= 0 )
				return addr.ReplaceLastOccurrence( " AVE ", " AVENUE " );

			if( addr.LastIndexOf( " DR ", StringComparison.Ordinal ) >= 0 )
				return addr.ReplaceLastOccurrence( " DR ", " DRIVE " );

			if( addr.LastIndexOf( " RT ", StringComparison.Ordinal ) >= 0 )
				return addr.ReplaceLastOccurrence( " RT ", " ROUTE " );

			return addr.LastIndexOf( " LN ", StringComparison.Ordinal ) >= 0 ? addr.ReplaceLastOccurrence( " LN ", " LANE " ) : addr;
		}
	}
}