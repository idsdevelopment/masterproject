﻿namespace Client;

public partial class Globals
{
	public const string VERSION_NUMBER = "1.0.0";
	public const string PROGRAM        = $"Web Client. Version: {VERSION_NUMBER}";
}