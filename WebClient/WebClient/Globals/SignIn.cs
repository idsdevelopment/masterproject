﻿namespace Client;

public partial class Globals
{
	public class SignIn
	{
		public enum SIGN_IN_TYPE : byte
		{
			NONE,
			SHIPPING_PORTAL,
			CARRIER
		}

		public static SIGN_IN_TYPE SignInType = SIGN_IN_TYPE.NONE;
	}
}