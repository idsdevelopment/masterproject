﻿using System.Net;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using IRequestContext = Interfaces.Interfaces.IRequestContext;

namespace Amazon;

public class Client
{
	private readonly AmazonS3Client? _Client;
	private readonly IRequestContext Context;

	public Client( IRequestContext context, string accessKey, string secretKey, RegionEndpoint endpoint )
	{
		Context = context;

		try
		{
			AWSCredentials Credentials = new BasicAWSCredentials( accessKey, secretKey );
			_Client = new AmazonS3Client( Credentials, endpoint );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public async Task CreateBucketAsync( string bucketName )
	{
		if( _Client is not null )
		{
			try
			{
				var ListBucketResponse = await _Client.ListBucketsAsync();
				var BucketExists       = ListBucketResponse.Buckets.Any( b => b.BucketName == bucketName );

				if( !BucketExists )
				{
					var PutBucketRequest = new PutBucketRequest
					                       {
						                       BucketName      = bucketName,
						                       UseClientRegion = true
					                       };

					var Response = await _Client.PutBucketAsync( PutBucketRequest );

					if( Response.HttpStatusCode is not (HttpStatusCode.OK or HttpStatusCode.Accepted) )
						Context.SystemLogException( $"Failed to create S3 bucket: {Response.HttpStatusCode}" );
				}
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}
		}
	}

	public async Task SaveStreamAsync( Stream stream, string container, string fileName )
	{
		if( _Client is not null )
		{
			try
			{
				var PutRequest = new PutObjectRequest
				                 {
					                 BucketName  = container,
					                 Key         = fileName,
					                 InputStream = stream
				                 };

				var Response = await _Client.PutObjectAsync( PutRequest );

				if( Response.HttpStatusCode is not (HttpStatusCode.OK or HttpStatusCode.Accepted) )
					Context.SystemLogException( $"Failed to save stream to S3: {Response.HttpStatusCode}" );
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}
		}
	}
}