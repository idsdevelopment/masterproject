﻿#nullable enable

using AzureRemoteService;

namespace DataSources;

public class CompanyDataSource : VList<Company>
{
	public CompanyDataSource( int preFetchCount = 20, CancellationToken cancellationToken = new() ) : base( preFetchCount, cancellationToken )
	{
	}

	protected override async Task<int> TotalCount() => ( await Azure.Client.RequestGetLookupRecords( new RecordLookup
	                                                                                                 {
		                                                                                                 RecordType = RecordLookupType.RECORD_TYPE.COMPANY
	                                                                                                 } ) ).Count;

	protected override void OnUpdated( (DateTime LastUpdatedUTC, string? Item) updated )
	{
	}

	protected override async Task<Company[]> GetNext( int currentIndex, int preFetchCount, CancellationToken cancellationToken )
	{
		RecordLookupResponse? Temp = null;

		try
		{
			Temp = await Azure.Client.RequestGetLookupRecords( new RecordLookup
			                                                   {
				                                                   RecordType = RecordLookupType.RECORD_TYPE.COMPANY,
				                                                   Count      = preFetchCount,
				                                                   Offset     = currentIndex
			                                                   } );
		}
		catch
		{
		}
		return Temp is not null ? Temp.CompanyList.ToArray() : Array.Empty<Company>();
	}

	protected override string ToString( Company item ) => item.CompanyName;
}