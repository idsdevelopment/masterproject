﻿using AzureRemoteService;
using Utils;

namespace Login;

public class LoginViewModel : ViewModelBase
{
	private const string PGM = "User Login Demo Vsn 1.0";

	protected override void OnInitialised()
	{
		base.OnInitialised();
		Azure.Slot    = Azure.SLOT.ALPHA_TRW;
		SelectedIndex = (int)TAB_PAGE.BLANK;
	}

#region Logged In
	public string CarrierId
	{
		get { return Get( () => CarrierId, "" ); }
		set { Set( () => CarrierId, value ); }
	}

	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}

	public string AccountId
	{
		get { return Get( () => AccountId, "" ); }
		set { Set( () => AccountId, value ); }
	}
#endregion

#region Message Tab Control
	public enum TAB_PAGE
	{
		BLANK,
		SIGN_IN_ERROR,
		OK
	}

	public int SelectedIndex
	{
		get { return Get( () => SelectedIndex, (int)TAB_PAGE.OK ); }
		set { Set( () => SelectedIndex, value ); }
	}
#endregion

#region Two code login
	public string TwoCodeUserName
	{
		get { return Get( () => TwoCodeUserName, "3pllinks"); }
		set { Set( () => TwoCodeUserName, value ); }
	}

	public string TwoCodePassword
	{
		get { return Get( () => TwoCodePassword, "3PL Links"); }
		set { Set( () => TwoCodePassword, value ); }
	}

	public bool TwoCodeCanLogin
	{
		get { return Get( () => TwoCodeCanLogin, true ); }
		set { Set( () => TwoCodeCanLogin, value ); }
	}

	public string TwoCodeLoginEnabled
	{
		get { return Get( () => TwoCodeLoginEnabled, "" ); }
		set { Set( () => TwoCodeLoginEnabled, value ); }
	}

	[DependsUpon350( nameof( TwoCodeUserName ) )]
	[DependsUpon350( nameof( TwoCodePassword ) )]
	public void EnableTwoCodeLogin()
	{
		TwoCodeCanLogin = TwoCodeUserName.Trim().IsNotNullOrWhiteSpace()
						  && TwoCodePassword.Trim().IsNotNullOrWhiteSpace();
	}

	public ICommand TwoCodeLogin => Commands[ nameof( TwoCodeLogin ) ];

	public async void Execute_TwoCodeLogin()
	{
		SelectedIndex = (int)TAB_PAGE.BLANK;

		var (Status, Carrier, UName, AccId, Enabled) = await Azure.LogIn( PGM, TwoCodeUserName, TwoCodePassword );

		if( Status == Azure.AZURE_CONNECTION_STATUS.OK )
		{
			CarrierId           = Carrier;
			UserName            = UName;
			AccountId           = AccId;
			TwoCodeLoginEnabled = Enabled.ToString();
			SelectedIndex       = (int)TAB_PAGE.OK;
		}
		else
			SelectedIndex = (int)TAB_PAGE.SIGN_IN_ERROR;
	}
#endregion
}