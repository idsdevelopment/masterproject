﻿#nullable enable

// ReSharper disable MemberHidesStaticFromOuterClass

using System;
using System.ServiceModel;
using System.Threading.Tasks;
using IdsServiceReference;
using Protocol.Data;
using Utils;

// ReSharper disable ExplicitCallerInfoArgument

namespace Ids1WebService;

public class EndPoints
{
	public const string JBTEST2 = "http://jbtest2.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    OZ      = "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    REPORTS = "http://reports.internetdispatcher.org:8080/ids-beans/IDSWS",
	                    CUSTOM  = "http://custom.internetdispatcher.org:8080/ids-beans/IDSWS";
}

// ReSharper disable once InconsistentNaming
public class IDSClient : DSWSClient
{
	public class Ids1Service
	{
		public static IDSClient Client( bool useJbTest, string carrierId, bool stripTest = false )
		{
			var Cid = carrierId.Trim();

			if( stripTest )
				Cid = Cid.ReplaceLastOccurrence( "test", "", StringComparison.OrdinalIgnoreCase );

			//Ids circuit breaker login
			var AToken = $"{Cid}-{SOAP_LOGIN.UserName}-{SOAP_LOGIN.Password}";

			var EndPoint = useJbTest ? EndPoints.JBTEST2 : EndPoints.CUSTOM;

			return new IDSClient( AToken, EndPoint );
		}
	}


	internal static readonly (string UserName, string Password) SOAP_LOGIN = ( "AzureWebService", "8y7b25b798538793b98" );

	private readonly string AuthToken;

	internal static DateTime ServerTime( DateTimeOffset? time ) => time?.AsPacificStandardTime().DateTime ?? DateTimeExtensions.Epoch;

	public static async Task<remoteTrip?> GetTrip( bool useJbTest, string carrierId, string tripId )
	{
		using var Client = Ids1Service.Client( useJbTest, carrierId );

		var Response = await Client.findTripAsync( new findTrip
		                                           {
			                                           authToken = Client.AuthToken,
			                                           tripId    = tripId
		                                           } );
		return Response?.findTripResponse?.@return;
	}

#region Signature
	public static async Task<bool> SaveSignatures( bool useJbTest, string carrierId, string tripId, Signature pickup, Signature delivery, string pop, string pod )
	{
		try
		{
			using var Client = Ids1Service.Client( useJbTest, carrierId );

			await Client.savePickupAndDeliverySignaturesAsync( new savePickupAndDeliverySignatures
			                                                   {
				                                                   authToken = Client.AuthToken,
				                                                   tripId    = tripId,
				                                                   //
				                                                   delSigHeight = delivery.Height,
				                                                   delSigWidth  = delivery.Width,
				                                                   delSignature = delivery.Points,
				                                                   pod          = pod,
				                                                   //
				                                                   puSigHeight = pickup.Height,
				                                                   puSigWidth  = pickup.Width,
				                                                   puSignature = pickup.Points,
				                                                   pop         = pop
			                                                   } );
			return true;
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
		}
		return false;
	}
#endregion

	public static async Task<bool> UpdateTrip( bool useJbTest, string carrierId, remoteTrip trip )
	{
		try
		{
			var Client = Ids1Service.Client( useJbTest, carrierId );

			await Client.updateTripQuickBroadcastAsync( new updateTripQuickBroadcast
			                                            {
				                                            authToken        = Client.AuthToken,
				                                            _tripVal         = trip,
				                                            updateDriverZone = false
			                                            } );
		}
		catch
		{
			return false;
		}
		return true;
	}

	public IDSClient( string authToken, string endPoint ) : base( new BasicHttpBinding( BasicHttpSecurityMode.None ), new EndpointAddress( endPoint ) )
	{
		AuthToken = authToken;
	}
}