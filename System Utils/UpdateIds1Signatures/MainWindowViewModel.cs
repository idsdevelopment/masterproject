﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using AzureRemoteService;
using Ids1WebService;
using Protocol.Data;
using Utils;
using XamlViewModel.Wpf;

namespace UpdateIds1Signatures;

internal class MainWindowViewModel : ViewModelBase
{
	private const string PROGRAM = "Update Ids1 Signatures";

	[Setting]
	public string CarrierId
	{
		get { return Get( () => CarrierId, "" ); }
		set { Set( () => CarrierId, value ); }
	}


	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}


	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}


	public bool SignInEnabled
	{
		get { return Get( () => SignInEnabled, false ); }
		set { Set( () => SignInEnabled, value ); }
	}


	public bool InvalidCred
	{
		get { return Get( () => InvalidCred, false ); }
		set { Set( () => InvalidCred, value ); }
	}


	public ICommand SignIn => Commands[ nameof( SignIn ) ];


	public bool SignInOk
	{
		get { return Get( () => SignInOk, true ); }
		set { Set( () => SignInOk, value ); }
	}


	public bool ShowSignIn
	{
		get { return Get( () => ShowSignIn, false ); }
		set { Set( () => ShowSignIn, value ); }
	}

#region Signature
	public List<Signature> Signatures
	{
		get { return Get( () => Signatures, new List<Signature>() ); }
		set { Set( () => Signatures, value ); }
	}
#endregion


	public string ErrorText
	{
		get { return Get( () => ErrorText, "" ); }
		set { Set( () => ErrorText, value ); }
	}

	public bool UseJbTest
	{
		get { return Get( () => UseJbTest, true ); }
		set { Set( () => UseJbTest, value ); }
	}


	public bool EnableIds1
	{
		get { return Get( () => EnableIds1, false ); }
		set { Set( () => EnableIds1, value ); }
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		if( IsInDesignMode )
		{
			ShowSignIn = false;
			SignInOk   = true;
		}
		else
		{
			ShowSignIn = true;
			SignInOk   = false;
		}
	}

	~MainWindowViewModel()
	{
		SaveSettings();
	}

	[DependsUpon( nameof( InvalidCred ), Delay = 4000 )]
	public void WhenInvalidCredChanged()
	{
		InvalidCred = false;
	}


	[DependsUpon350( nameof( CarrierId ) )]
	[DependsUpon350( nameof( UserName ) )]
	[DependsUpon350( nameof( Password ) )]
	public void EnableSignIn()
	{
		SignInEnabled = CarrierId.IsNotNullOrWhiteSpace() && UserName.IsNotNullOrWhiteSpace() && Password.IsNotNullOrWhiteSpace();
	}


	public async void Execute_SignIn()
	{
		if( ( await Azure.LogIn( PROGRAM, CarrierId, UserName, Password ) ).Status == Azure.AZURE_CONNECTION_STATUS.OK )
		{
			SaveSettings();
			SignInOk   = true;
			ShowSignIn = false;
		}
		else
			InvalidCred = true;
	}

#region Ids1
	public ICommand UpdateIds1 => Commands[ nameof( UpdateIds1 ) ];

	public async void Execute_UpdateIds1()
	{
		EnableIds1 = false;

		ErrorText = "";

		if( await IDSClient.GetTrip( UseJbTest, CarrierId, TripId ) is { } RemoteTrip )
		{
			Signature? Pickup   = null,
			           Delivery = null;

			for( var Ndx = Signatures.Count; --Ndx >= 0; )
			{
				var Sig = Signatures[ Ndx ];

				switch( Sig.Status )
				{
				case STATUS.DELIVERED:
				case STATUS.VERIFIED:
					Delivery ??= Sig;
					break;

				case STATUS.PICKED_UP:
					Pickup ??= Sig;
					break;
				}

				if( Pickup is not null && Delivery is not null )
					break;
			}

			var Ok = await IDSClient.SaveSignatures( UseJbTest, CarrierId,
			                                         TripId,
			                                         Pickup ?? new Signature(),
			                                         Delivery ?? new Signature(),
			                                         Trip.POP,
			                                         Trip.POD
			                                       );

			if( !Ok )
				ErrorText = "Cannot update Ids1";
			else
				TripId = "";
		}
		else
			ErrorText = "Cannot find trip in Ids1";
	}
#endregion

#region Window
	[Setting]
	public int Top
	{
		get { return Get( () => Top, 100 ); }
		set { Set( () => Top, value ); }
	}


	[Setting]
	public int Left
	{
		get { return Get( () => Left, 100 ); }
		set { Set( () => Left, value ); }
	}
#endregion

#region Trip Id
	public bool TripIdNotFound
	{
		get { return Get( () => TripIdNotFound, false ); }
		set { Set( () => TripIdNotFound, value ); }
	}

	[DependsUpon( nameof( TripIdNotFound ), Delay = 4000 )]
	public void WhenTripIdNotFoundChanges()
	{
		TripIdNotFound = false;
	}

	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	public ICommand GetTrip => Commands[ nameof( GetTrip ) ];


	private Trip Trip = new();

	public async void Execute_GetTrip()
	{
		TripIdNotFound = false;
		EnableIds1     = false;

		Trip = await Azure.Client.RequestGetTrip( new GetTrip
		                                          {
			                                          Signatures = true,
			                                          TripId     = TripId
		                                          } );

		if( Trip is {Ok: true} )
		{
			var Sigs = Signatures = ( from S in Trip.Signatures
			                          orderby S.Date
			                          select S ).ToList();

			EnableIds1 = Sigs.Count > 0;
		}
		else
			TripIdNotFound = true;
	}
#endregion
}