﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace UpdateIds1Signatures;

public class InputValidConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is not string S || ( S.Length == 0 ) ? Brushes.LightCoral : Brushes.Transparent;

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class BoolToVisibilityConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) => value is true ? Visibility.Visible : Visibility.Collapsed;

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
	public MainWindow()
	{
		InitializeComponent();
	}
}