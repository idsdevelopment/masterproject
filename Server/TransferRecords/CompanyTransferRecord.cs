﻿namespace TransferRecords;

public static partial class Extensions
{
	public static CompanyTransferRecord ToTransferRecord( this Company c ) => new( c );
}

public record CompanyTransferRecord( string                CompanyName,
									 string                CompanyNumber,
									 string                LocationBarcode,
									 bool                  Enabled,
									 string                UserName,
									 string                Password,
									 AddressTransferRecord Address,
									 bool                  EmailInvoice,
									 int                   BillingPeriod )
{
	public CompanyTransferRecord( Company c ) : this( c.CompanyName,
													  c.CompanyNumber,
													  c.LocationBarcode,
													  c.Enabled,
													  c.UserName,
													  c.Password,
													  ( (Address)c ).ToTransferRecord(),
													  c.EmailInvoice,
													  c.BillingPeriod
													)
	{
	}
}