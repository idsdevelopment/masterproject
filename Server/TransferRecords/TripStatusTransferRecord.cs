﻿#nullable enable

namespace TransferRecords;

public static partial class Extensions
{
	public static TripStatusTransferRecord ToTripStatusTransferRecord( this Trip t ) => new( t );
}

public record TripStatusTransferRecord( string Program, string AccountId, string TripId, STATUS Status, STATUS1 Status1, STATUS2 Status2 )
{
	public TripStatusTransferRecord( TripUpdate trip ) : this( trip.Program, trip.AccountId, trip.TripId, trip.Status1, trip.Status2, trip.Status3 )
	{
	}
}