﻿#nullable enable

namespace TransferRecords;

public static partial class Extensions
{
	public static PrimaryCompanyTransferRecord ToTransferRecord( this AddUpdatePrimaryCompany a, string carrierId ) => new( carrierId, a );
}

public record PrimaryCompanyTransferRecord( string Program,
                                            string CarrierId,
                                            string CompanyCode,
                                            string LoginCode,
                                            string UserName,
                                            string Password,
                                            CompanyTransferRecord Company,
                                            CompanyTransferRecord BillingCompany,
                                            CompanyTransferRecord ShippingCompany ) : AccountTransferRecord( Program,
                                                                                                             CarrierId,
                                                                                                             CompanyCode,
                                                                                                             LoginCode,
                                                                                                             UserName,
                                                                                                             Password,
                                                                                                             Company,
                                                                                                             BillingCompany,
                                                                                                             ShippingCompany )
{
	public PrimaryCompanyTransferRecord( string carrierId, AddUpdatePrimaryCompany a ) : this( a.ProgramName,
	                                                                                           carrierId,
	                                                                                           a.CustomerCode,
	                                                                                           a.UserName,
	                                                                                           a.UserName,
	                                                                                           Encryption.FromTimeLimitedToken( a.Password ).Value,
	                                                                                           new CompanyTransferRecord( a.Company ),
	                                                                                           new CompanyTransferRecord( a.BillingCompany ),
	                                                                                           new CompanyTransferRecord( a.ShippingCompany )
	                                                                                         )
	{
	}
}