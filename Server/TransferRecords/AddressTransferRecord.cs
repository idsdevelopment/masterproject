﻿namespace TransferRecords;

public static partial class Extensions
{
	public static AddressTransferRecord ToTransferRecord( this Address a ) => new( a );
}

public record AddressTransferRecord(
	string Barcode,
	string PostalBarcode,
	string Suite,
	string AddressLine1,
	string AddressLine2,
	string City,
	string Region,
	string PostalCode,
	string Country,
	string CountryCode,
	string Phone,
	string Mobile,
	string Fax,
	string Contact,
	string EmailAddress,
	string Notes,
	string Zone
)
{
	public AddressTransferRecord( Address a ) : this( a.Barcode,
	                                                  a.PostalBarcode,
	                                                  a.Suite,
	                                                  a.AddressLine1,
	                                                  a.AddressLine2,
	                                                  a.City,
	                                                  a.Region,
	                                                  a.PostalCode,
	                                                  a.Country,
	                                                  a.CountryCode,
	                                                  a.Phone,
	                                                  a.Mobile,
	                                                  a.Fax,
	                                                  a.ContactName,
	                                                  a.EmailAddress,
	                                                  a.Notes,
	                                                  a.ZoneName
	                                                )
	{
	}
}