﻿namespace ReportsModule;

public static class StringExtensions
{
	private const string
		HEIGHT  = "~HEIGHT~",
		CONTENT = "~CONTENT~";

	private static string DoReplace( string str, string token, string value ) => str.Replace( token, value );

	public static string ToCm( decimal sizeCm ) => sizeCm.ToString( "G" ) + "cm";

	public static string ReplaceHeight( this string str, string strHeight ) => DoReplace( str, HEIGHT, strHeight );

	public static string ReplaceHeight( this string str, decimal height ) => str.ReplaceHeight( ToCm( height ) );

	public static string ReplaceContent( this string str, string content ) => DoReplace( str, CONTENT, content );
}