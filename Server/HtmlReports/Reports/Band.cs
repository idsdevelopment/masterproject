﻿using Utils;

namespace ReportsModule;

public partial class Reports
{
	private const string HEIGHT          = "HEIGHT",
	                     PRINT_ON_CHANGE = "PRINTONCHANGE";

	private const string SECTION = """
									<section data-band style="height:~HEIGHT~; max-height:~HEIGHT~;">
										~CONTENT~ 
									</section>
									""";

	private const string PAGE_BREAK = """
										<aside data-pageBreak>
										</aside>
										""";

	private const string PAGE_NUMBER_TOKEN = "~^P#^~";

	private static string PrintOnChangeGetVariable( Pulsar.FunctionArgs args )
	{
		var Variable = "";

		foreach( var Arg in args )
		{
			Variable = Arg.Key.TrimToUpper() switch
			           {
				           PRINT_ON_CHANGE => Arg.Value.Trim(),
				           _               => ""
			           };

			if( Variable != "" )
			{
				Variable = Variable.TrimStart( '$' );
				break;
			}
		}

		return Variable;
	}


	private bool PrintOnChange( bool isHeader )
	{
		bool Result;

		var Obj = Pulsar.GetAssignAsObject( isHeader ? Data.HeaderBreakVariable : Data.FooterBreakVariable );

		if( Obj is not null )
		{
			var PrevObject = isHeader ? Data.HeaderBreakObject : Data.FooterBreakObject;

			Result = PrevObject is null || ( PrevObject != Obj );

			if( isHeader )
				Data.HeaderBreakObject = Obj;
			else
				Data.FooterBreakObject = Obj;
		}
		else
			Result = true;

		return Result;
	}

	private static decimal GetHeightName( Pulsar.FunctionArgs args )
	{
		string? Height = null;

		foreach( var Arg in args )
		{
			Height = Arg.Key.TrimToUpper() switch
			         {
				         HEIGHT => Arg.Value.Trim(),
				         _      => Height
			         };

			if( Height is not null )
				break;
		}

		if( string.IsNullOrEmpty( Height ) )
			Height = "0";

		if( !decimal.TryParse( Height, out var H ) )
			H = 0;

		return H;
	}

	private string GetFooter( Pulsar pulsar )
	{
		Data.PrintedHeaderOrBand = false;
		Data.RemainingHeightCm   = Data.PaperHeightLessHeaderAndFooterCm;

		var PageNumber = Data.PageNumber++;
		pulsar.Assign(PAGE_NUMBER, PageNumber);

        //string Footer;
        //var PrintFooter = PrintOnChange(false);

        //      if (PrintFooter)
        //      {
        //	Footer = !string.IsNullOrEmpty(Data.Footer) ? new Pulsar(pulsar).Fetch(Data.Footer)
        //		: "";
        //}
        //      else
        //      {
        //	Data.RemainingHeightCm += Data.FooterSizeCm;
        //	Footer = "";
        //      }



        return ((!string.IsNullOrEmpty(Data.Footer) ? new Pulsar(pulsar).Fetch(Data.Footer) : "")
                 + "\r\n</article >\r\n").Replace(PAGE_NUMBER_TOKEN, PageNumber.ToString());
        //return (Footer + "\r\n</article >\r\n").Replace(PAGE_NUMBER_TOKEN, PageNumber.ToString());
    }


	private string GetHeader( Pulsar pulsar )
	{
		Data.RemainingHeightCm = Data.PaperHeightLessHeaderAndFooterCm;

		string Header;
		var    PrintHeader = PrintOnChange( true );

		if( PrintHeader )
		{
			Header = !string.IsNullOrEmpty( Data.Header ) ? new Pulsar( pulsar ).Fetch( Data.Header )
			                                                                    //.Replace( PAGE_NUMBER_TOKEN, Data.PageNumber.ToString() )
				         : "";
		}
		else
		{
			Data.RemainingHeightCm += Data.HeaderSizeCm;
			Header                 =  "";
		}

		return $"\r\n<article data-page>\r\n{Header}".Replace(PAGE_NUMBER_TOKEN, Data.PageNumber.ToString());
	}

	private string DoBand( Pulsar pulsar, string? content, Pulsar.FunctionArgs args )
	{
		if( content is not null )
		{
			var PrintHeader = Data.NeedHeader;

			var Header = PrintHeader ? GetHeader( pulsar ) : "";

			var BandHeight = GetHeightName( args );

			var Band = Header + SECTION.ReplaceHeight( BandHeight ).ReplaceContent( content ).Replace( PAGE_NUMBER_TOKEN, Data.PageNumber.ToString() );

			// Does then band fit?
			if( Data.NeedFooter( BandHeight, PrintHeader ) )
				Band = GetFooter( pulsar ) + GetHeader( pulsar ) + Band;

			Data.RemainingHeightCm -= BandHeight;

			Data.PrintedHeaderOrBand = true;

			return Band;
		}

		return null!;
	}

	private string DoPageBreak( Pulsar pulsar, Pulsar.FunctionArgs args ) => PAGE_BREAK + GetFooter( pulsar );

	private static string DoPageNumber( Pulsar pulsar, Pulsar.FunctionArgs args ) => PAGE_NUMBER_TOKEN;
	private static string DoReportDate( Pulsar pulsar, Pulsar.FunctionArgs args ) => pulsar.GetAssign( REPORT_DATE );
	private static string DoReportTime( Pulsar pulsar, Pulsar.FunctionArgs args ) => pulsar.GetAssign( REPORT_TIME );
}