﻿using System;
using System.Collections.Generic;
using Interfaces.Interfaces;
using Messaging;
using Protocol.Data;
using Utils;

namespace Broadcast;

public class BroadcastUpdates( IRequestContext context ) : IBroadcast
{
	public static void Broadcast( IRequestContext context, TripUpdateMessage msg )
	{
		if( ( msg.Broadcast & ( TripUpdateStatus.BROADCAST.DISPATCH_BOARD | TripUpdateStatus.BROADCAST.DRIVERS_BOARD | TripUpdateStatus.BROADCAST.DRIVER ) ) != 0 )
		{
			if( msg.BroadcastToDriver && ( ( msg.Status2 & STATUS2.DONT_SEND_TO_DRIVER ) == 0 ) )
			{
				var Driver = msg.Driver.TrimToLower();

				if( !Driver.IsNullOrWhiteSpace() )
					Messaging<TripUpdateMessage>.Send( context, Protocol.Data.Messaging.DRIVERS, Driver, [msg] );
			}

			var Board = msg.Board.TrimToLower();

			if( msg.BroadcastToDispatchBoard )
				Messaging<TripUpdateMessage>.Send( context, Protocol.Data.Messaging.DISPATCH_BOARD, "", [msg], Board );

			if( msg.BroadcastToDriversBoard )
				Messaging<TripUpdateMessage>.Send( context, Protocol.Data.Messaging.DRIVERS_BOARD, "", [msg], Board );

			Messaging<TripUpdateMessage>.Send( context, Protocol.Data.Messaging.SEARCH_TRIPS, "", [msg], Board );
		}
	}

	public static bool Broadcast( IRequestContext context, IEnumerable<TripUpdateMessage> msg )
	{
		try
		{
			foreach( var TripUpdateMessage in msg )
				Broadcast( context, TripUpdateMessage );

			return true;
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
		return false;
	}

	public static TripUpdate Broadcast( IRequestContext context, TripUpdate t )
	{
		Broadcast( context, new TripUpdateMessage
		                    {
			                    Program                  = t.Program,
			                    Driver                   = t.Driver,
			                    Action                   = TripUpdateMessage.ACTION.UPDATE_TRIP,
			                    Board                    = t.Board,
			                    BroadcastToDispatchBoard = t.BroadcastToDispatchBoard,
			                    BroadcastToDriver        = t.BroadcastToDriver,
			                    BroadcastToDriversBoard  = t.BroadcastToDriverBoard,
			                    ReadByDriver             = t.ReadByDriver,
			                    ReceivedByDevice         = t.ReceivedByDevice,
			                    Status                   = t.Status1,
			                    Status1                  = t.Status2,
			                    Status2                  = t.Status3,
			                    TripIdList               = [t.TripId]
		                    } );

		return t;
	}

#region IBroadcast Interface
	public bool Broadcast( IEnumerable<TripUpdateMessage> msg ) => Broadcast( context, msg );

	public TripUpdate Broadcast( TripUpdate t ) => Broadcast( context, t );

	public void Broadcast( TripUpdateMessage msg )
	{
		Broadcast( context, msg );
	}
#endregion
}