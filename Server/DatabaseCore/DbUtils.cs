﻿using System.Data.Common;
using Database.Model.Database;
using Microsoft.Data.SqlClient;
using static Database.Utils.DbUtils;
using Configuration = Database.Model.Databases.MasterTemplate.Configuration;
using Role = Protocol.Data.Role;
using Staff = Protocol.Data.Staff;

namespace Database.Utils;

public class Company
{
	public static async Task CreateCompanyStorage( IRequestContext context, CarrierDb db )
	{
		var E = await Users.AddStorageEndPoint( context );

		if( E is not null )
		{
			var Cfg = new Configuration
			          {
				          CurrentVersion = db.CurrentVersion,

				          Id               = Guid.Empty,
				          CompanyAddressId = Guid.Empty,

				          StorageAccount       = E.StorageAccount,
				          StorageAccountKey    = E.StorageAccountKey,
				          StorageBlobEndpoint  = E.StorageBlobEndpoint,
				          StorageFileEndpoint  = E.StorageFileEndpoint,
				          StorageTableEndpoint = E.StorageTableEndpoint
			          };

			db.UpdateConfiguration( Cfg );
		}
	}

	// Disposes returned Db
	public static async Task<bool> CreateCompany( IRequestContext context, Func<CarrierDb> dbCreateFunc )
	{
		try
		{
			var Db = dbCreateFunc();
			await CreateCompanyStorage( context, Db );
			Db.AddDefaultRoles();
			await Db.DisposeAsync();

			return true;
		}
		catch( Exception E )
		{
			context.SystemLogException( nameof( CreateCompany ), E );
		}

		return false;
	}

	public static long CreateCompany( IRequestContext context, string carrierId, string adminId, string adminPassword )
	{
		var Pid = Users.StartProcess( context );

		var SaveId = context.CarrierId;
		context.CarrierId = carrierId.Trim().Capitalise();

		var DbContext = context.Clone();

		Tasks.RunVoid( async () =>
		               {
			               try
			               {
				               var Ok = await CreateCompany( context, () =>
				                                                      {
					                                                      var Db = new CarrierDb( DbContext );

					                                                      if( !Db.Exists )
					                                                      {
						                                                      CreateIfNew( DbContext, Db.Entity );
						                                                      Db.Dispose();
						                                                      Db = new CarrierDb( DbContext );
					                                                      }
					                                                      return Db;
				                                                      } );

				               if( Ok )
				               {
					               await using var Db = new CarrierDb( DbContext );

					               // Make sure there is a default Config record
					               Db.UpdateConfiguration( new Configuration
					                                       {
						                                       CompanyAddressId     = Guid.Empty,
						                                       Id                   = Guid.Empty,
						                                       CurrentVersion       = 0,
						                                       StorageAccount       = "",
						                                       StorageAccountKey    = "",
						                                       StorageBlobEndpoint  = "",
						                                       StorageFileEndpoint  = "",
						                                       StorageTableEndpoint = ""
					                                       } );

					               Db.AddRoles( [
						               new Role
						               {
							               Name            = CarrierDb.ADMIN,
							               IsAdministrator = true,
							               JsonObject      = "",
							               IsDriver        = false,
							               Mandatory       = false
						               }
					               ] );

					               var UpdRec = new UpdateStaffMemberWithRolesAndZones( "Carrier create wizard.", new Staff
					                                                                                              {
						                                                                                              StaffId  = adminId,
						                                                                                              Password = Encryption.ToTimeLimitedToken( adminPassword ),
						                                                                                              Enabled  = true
					                                                                                              } );

					               UpdRec.Roles.Add( new Role
					                                 {
						                                 IsAdministrator = true,
						                                 Name            = CarrierDb.ADMIN
					                                 } );

					               Db.AddUpdateStaffMember( UpdRec );
				               }
			               }
			               catch( Exception Exception )
			               {
				               DbContext.SystemLogException( Exception );
			               }
			               finally
			               {
				               DbContext.CarrierId = SaveId;
				               Users.EndProcess( DbContext, Pid );
			               }
		               } );

		return Pid;
	}
}

public class DbUtils
{
	public const string MASTER_DATABASE_NAME = "MasterTemplate";

	private const string BASIC    = "Basic",
	                     STANDARD = "Standard";

	public enum EDITION
	{
		BASIC,
		S0,
		S1,
		S2
	}

	public enum INITIALISE_MODULE
	{
		NONE,
		CREATE,
		DELETE
	}

	public class AzureEdition
	{
		public string Version { get; set; } = "";
		public string Edition { get; set; } = "";
		public string Tier    { get; set; } = "";
	}

	public static string GetDatabaseName( DbContext db ) => new SqlConnectionStringBuilder( db.Database.GetConnectionString() ).InitialCatalog;

	public static void GetDatabaseSettings( DbContext db, out string dbName, out string userName, out string password )
	{
		var Conn = new SqlConnectionStringBuilder( db.Database.GetConnectionString() );
		dbName   = Conn.InitialCatalog;
		userName = Conn.UserID;
		password = Conn.Password;
	}

	public static void GetDatabaseSettings( string connectionString, out string dbName, out string userName,
	                                        out string password )
	{
		var Conn = new SqlConnectionStringBuilder( connectionString );
		dbName   = Conn.InitialCatalog;
		userName = Conn.UserID;
		password = Conn.Password;
	}

	public static bool Exists( DbContext entity ) => entity.Database.CanConnect();

	public static void Clone( IRequestContext context, DbContext entity, string dbToCopy, string newDatabaseName )
	{
		try
		{
#pragma warning disable EF1002 // Risk of vulnerability to SQL injection.
			entity.Database.ExecuteSqlRaw( $"CREATE DATABASE {newDatabaseName} AS COPY OF {dbToCopy}" );
#pragma warning restore EF1002 // Risk of vulnerability to SQL injection.
		}
		catch( Exception Exception )
		{
			context.SystemLogException( nameof( Clone ), Exception );
		}
	}

	public static AzureEdition GetEdition( DbContext entity )
	{
		var DbName = GetDatabaseName( entity );

		FormattableString Sql = $"SELECT @@VERSION AS Version, DATABASEPROPERTYEX( '{DbName}', 'Edition' ) AS Edition, COALESCE(DATABASEPROPERTYEX( '{DbName}', 'ServiceObjective' ), 'N/A in v11') AS Tier";

		return entity.Database.SqlQuery<AzureEdition>( Sql ).FirstOrDefault() ?? new AzureEdition();
	}

	public static AzureEdition SetEdition( DbContext entity, string edition, string tier, string maxSizeInGb )
	{
		var OldEdition = GetEdition( entity );
		var DbName     = GetDatabaseName( entity );

		using var Transaction = entity.Database.BeginTransaction();
		entity.Database.ExecuteSql( $"ALTER DATABASE {DbName} MODIFY (EDITION = '{edition}',  SERVICE_OBJECTIVE = '{tier}',  MAXSIZE = {maxSizeInGb} GB)" );

		return OldEdition;
	}


	public static AzureEdition SetEdition( DbContext entity, AzureEdition edition )
	{
		var MaxSize = edition.Edition.Contains( BASIC, StringComparison.OrdinalIgnoreCase ) ? "2" : "250";

		return SetEdition( entity, edition.Edition, edition.Tier, MaxSize );
	}


	public static AzureEdition SetEdition( IRequestContext context, DbContext entity, EDITION edition )
	{
		try
		{
			string MaxSize = "250",
			       Edition = STANDARD,
			       Tier;

			switch( edition )
			{
			case EDITION.S0:
				Tier = "S0";
				break;

			case EDITION.S1:
				Tier = "S1";
				break;

			case EDITION.S2:
				Tier = "S2";
				break;

			default:
				MaxSize = "2";
				Edition = BASIC;
				Tier    = BASIC;
				break;
			}

			return SetEdition( entity, Edition, Tier, MaxSize );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return new AzureEdition
		       {
			       Edition = BASIC,
			       Tier    = BASIC,
			       Version = ""
		       };
	}

	public static void Clone( IRequestContext context, DbContext entity, string newDatabaseName )
	{
		Clone( context, entity, GetDatabaseName( entity ), newDatabaseName );
	}

	public static bool CreateIfNew( IRequestContext context, DbContext entity )
	{
		if( context is null )
			throw new ArgumentNullException( nameof( context ) );

		var DBase = entity.Database;

		if( !DBase.CanConnect() )
		{
			var SaveTimeOut = DBase.GetCommandTimeout();
			var Error       = false;

			try
			{
				var MasterContext = context.Clone( MASTER_DATABASE_NAME );
				var MasterDb      = new CarrierDb( MasterContext );
				MasterDb.Entity.Database.SetCommandTimeout( 10 * 60 );

				Clone( context, MasterDb.Entity, MASTER_DATABASE_NAME, GetDatabaseName( entity ) );

				while( !DBase.CanConnect() )
					Thread.Sleep( 2_000 );

				return true;
			}
			catch( Exception Ex )
			{
				Error = true;
				context.SystemLogException( nameof( CreateIfNew ), Ex );
			}
			finally
			{
				if( !Error )
				{
					var DbName = GetDatabaseName( entity );

					try
					{
						using var Transaction = DBase.BeginTransaction();
						var       SqlCommand  = $"ALTER DATABASE {DbName} MODIFY (EDITION = 'basic')";
						DBase.ExecuteSqlRaw( SqlCommand );
						Transaction.Commit();
					}
					catch // Throw away probably local Db
					{
					}
				}

				DBase.SetCommandTimeout( SaveTimeOut );
			}
		}

		return false;
	}

	public static bool DeleteIfExists( DbContext entity )
	{
		using( entity )
		{
			var DBase       = entity.Database;
			var SaveTimeOut = DBase.GetCommandTimeout();

			try
			{
				DBase.SetCommandTimeout( 5 * 60 );
				DBase.EnsureDeleted();
				return true;
			}
			catch
			{
			}
			finally
			{
				DBase.SetCommandTimeout( SaveTimeOut );
			}
		}
		return false;
	}

	public static string RenameDatabase( IRequestContext context, string connectionString, string oldName, string newName )
	{
		using var Connection   = new SqlConnection( connectionString );
		var       ConnectionDb = Connection.Database;

		var       NewConnection = new StringBuilder( connectionString ).Replace( $"catalog={ConnectionDb}", $"catalog={oldName}" ).ToString();
		using var Connection1   = new SqlConnection( NewConnection );

		try
		{
			Connection1.Open();

			try
			{
				var Command = new SqlCommand( $"ALTER DATABASE {oldName} MODIFY NAME = {newName};", Connection1 );
				Command.ExecuteNonQuery();
			}
			finally
			{
				Connection1.Close();
			}
		}
		catch // Rename appears to close the connection
		{
		}

		NewConnection = new StringBuilder( NewConnection ).Replace( $"catalog={oldName}", $"catalog={newName}" ).ToString();

		try
		{
			//Thread.Sleep( 30_000 );
			using var Connection2 = new SqlConnection( NewConnection );

			Connection2.Open();
			Connection2.Close();
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
			return "";
		}

		return NewConnection;
	}


	public static string RenameDatabase( IRequestContext context, SqlConnection connection, string oldName, string newName ) => RenameDatabase( context, connection.ConnectionString, oldName, newName );

	public static string RenameDatabase( IRequestContext context, DbConnection connection, string oldName, string newName ) => RenameDatabase( context, connection.ConnectionString, oldName, newName );
}