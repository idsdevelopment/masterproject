﻿using System.Net.Mail;
using Database.Model.Database;
using Logs;

namespace Database.Model.Databases.Users;

public class Emails
{
	public class SentEmails
	{
		public static void Log( IRequestContext context, long messageId, string from, string to, string cc, string bcc, string subject, bool failed )
		{
			try
			{
				using var Db         = new UsersEntities();
				var       SentEmails = Db.Entity.SentEmails;

				void NewEmail()
				{
					var Email = new SentEmail
					            {
						            Failed     = failed,
						            EmailId    = messageId,
						            RetryCount = 0,

						            CarrierId = context.CarrierId,
						            Timestamp = DateTime.UtcNow,
						            From      = from,
						            To        = to,
						            Cc        = cc,
						            Bcc       = bcc,
						            Subject   = subject
					            };

					SentEmails.Add( Email );
				}

				var Email = ( from E in SentEmails
				              where E.EmailId == messageId
				              select E ).FirstOrDefault();

				if( failed )
				{
					if( Email is not null )
						Email.RetryCount++;
					else
						NewEmail();
				}
				else
				{
					if( Email is not null )
						Email.Failed = false;
					else
						NewEmail();
				}

				Db.SaveChanges();
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}

		public static void Cleanup()
		{
			try
			{
				using var Db     = new UsersEntities();
				var       Entity = Db.Entity;

				var Date30DaysAgo = DateTime.UtcNow.AddDays( -30 ); // EF has problems with Timestamps maths in the query.

				while( true )
				{
					var Recs = ( from E in Entity.SentEmails
					             where E.Timestamp <= Date30DaysAgo
					             select E ).Take( 100 ).ToList();

					if( Recs.Count == 0 )
						break;

					Entity.SentEmails.RemoveRange( Recs );

					Db.SaveChanges();
				}
			}
			catch( Exception Exception )
			{
				SystemLog.Add( Exception );
			}
		}
	}

	private const int
	#if DEBUG
		ERROR_WAIT_IN_MINUTES = 1,
	#else
		ERROR_WAIT_IN_MINUTES = 30,
	#endif
		ERROR_EXPIRED_IN_DAYS = 2;

	public static void Send( IRequestContext context, MailMessage message )
	{
		try
		{
			string BuildAddress( MailAddressCollection addresses )
			{
				return string.Join( ";", addresses );
			}

			using var Db     = new UsersEntities();
			var       Entity = Db.Entity;
			var       Emails = Entity.Emails;

			var Now = DateTime.UtcNow;

			if( message.From is not null )
			{
				var MailRec = new Email
				              {
					              Locked     = true,
					              CarrierId  = context.CarrierId,
					              From       = message.From.Address,
					              To         = BuildAddress( message.To ),
					              CC         = BuildAddress( message.CC ),
					              BCC        = BuildAddress( message.Bcc ),
					              Subject    = message.Subject,
					              Body       = message.Body,
					              IsBodyHtml = message.IsBodyHtml,
					              Created    = Now,
					              SleepUntil = Now
				              };

				Emails.Add( MailRec );
				Db.SaveChanges();

				try
				{
					foreach( var Attachment in message.Attachments )
					{
						if( Attachment.Name is not null )
						{
							var ContentStream = Attachment.ContentStream;

							byte[] BData;

							using( var MemoryStream = new MemoryStream() )
							{
								ContentStream.Position = 0;
								ContentStream.CopyTo( MemoryStream );
								BData = MemoryStream.ToArray();
							}

							Db.Entity.EmailAttachments.Add( new EmailAttachment
							                                {
								                                Attachment = BData,
								                                FileName   = Attachment.Name,
								                                MediaType  = Attachment.ContentType.MediaType,
								                                MessageId  = MailRec.Id
							                                } );
							Db.SaveChanges();
						}
					}
				}
				catch( Exception Exception )
				{
					context.SystemLogException( Exception );
				}
				finally
				{
					MailRec = ( from E in Emails
					            where E.Id == MailRec.Id
					            select E ).FirstOrDefault();

					if( MailRec is not null )
					{
						MailRec.Locked = false;
						Db.SaveChanges();
					}
				}
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	public static (long MessageId, string CarrierId, bool Expired, MailMessage? Message) GetNexMessage( string? carrierId = null )
	{
		try
		{
			using var Db     = new UsersEntities();
			var       Entity = Db.Entity;

			var Now = DateTime.UtcNow;

			var Rec = ( from E in Entity.Emails
			            where ( E.SleepUntil <= Now ) && ( ( carrierId == null ) || ( E.CarrierId == carrierId ) ) && !E.Locked
			            select E ).FirstOrDefault();

			if( Rec is not null )
			{
				Rec.SleepUntil = Now.AddMinutes( ERROR_WAIT_IN_MINUTES );
				Db.SaveChanges();

				static MailAddress ToAddress( string address )
				{
					return new MailAddress( address );
				}

				static void Addresses( MailAddressCollection collection, string addresses )
				{
					foreach( var A in addresses.Split( [';'], StringSplitOptions.RemoveEmptyEntries ) )
						collection.Add( ToAddress( A ) );
				}

				var Result = new MailMessage();
				Result.From = ToAddress( Rec.From );
				Addresses( Result.To, Rec.To );
				Addresses( Result.CC, Rec.CC );
				Addresses( Result.Bcc, Rec.BCC );
				Result.Subject    = Rec.Subject;
				Result.Body       = Rec.Body;
				Result.IsBodyHtml = Rec.IsBodyHtml;

				foreach( var A in ( from A in Entity.EmailAttachments
				                    where A.MessageId == Rec.Id
				                    select A ).ToList() )
				{
					// Don't dispose of the stream, the attachment will do that.
					var Stream     = new MemoryStream( A.Attachment );
					var Attachment = new Attachment( Stream, A.FileName, A.MediaType );
					Result.Attachments.Add( Attachment );
				}

				return ( Rec.Id, Rec.CarrierId, ( Now - Rec.Created ).Days >= ERROR_EXPIRED_IN_DAYS, Result );
			}
		}
		catch( Exception Exception )
		{
			SystemLog.Add( Exception );
		}
		return ( 0, "", true, null );
	}

	public static void DeleteEmail( long id )
	{
		using var Db     = new UsersEntities();
		var       Entity = Db.Entity;

		var Rec = ( from E in Entity.Emails
		            where E.Id == id
		            select E ).FirstOrDefault();

		if( Rec is not null )
		{
			Entity.Emails.Remove( Rec );
			Db.SaveChanges();
		}
	}
}