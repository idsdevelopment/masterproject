﻿using Preference = Database.Model.Databases.Users.Preference;

namespace Database.Model.Database;

public static partial class Users
{
	public static List<Preference> GetPreference( IRequestContext context )
	{
		try
		{
			using var Db = new UsersEntities();

			return ( from P in Db.Entity.Preferences
			         select P ).ToList();
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return [];
	}
}