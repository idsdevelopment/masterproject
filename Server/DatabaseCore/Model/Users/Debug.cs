﻿using ResellerCustomer = Database.Model.Databases.Users.ResellerCustomer;

namespace Database.Model.Database;

public static partial class Users
{
	public static void CloneResellerCustomer( IRequestContext context, string originalReseller, string prefix )
	{
		using var Db  = new UsersEntities();
		var       Rc  = Db.Entity.ResellerCustomers;
		var       Cid = context.CarrierId;

		//Remove old records
		Rc.RemoveRange( from C in Rc
		                where C.ResellerId == Cid
		                select C );

		Db.SaveChanges();

		foreach( var Orig in from C in Rc
		                     where C.ResellerId == originalReseller
		                     select C )
		{
			try
			{
				Rc.Add( new ResellerCustomer
				        {
					        ResellerId        = Cid,
					        CustomerCode      = Orig.CustomerCode,
					        LoginCode         = prefix + Orig.LoginCode,
					        OriginalLoginCode = Orig.LoginCode,
					        Password          = Orig.Password,
					        UserName          = Orig.UserName
				        } );
			}
			catch
			{
			}
		}

		Db.SaveChanges();
	}
}