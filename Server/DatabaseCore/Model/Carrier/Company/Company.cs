﻿using Address = Database.Model.Databases.MasterTemplate.Address;
using Company = Database.Model.Databases.MasterTemplate.Company;
using CompanyAddress = Protocol.Data.CompanyAddress;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private const int MAX_UPDATE_DISTANCE_IM_METRES = 500;

	public class CompanyLogger
	{
		public AddressLogger? Address { get; set; }

		public string CompanyName        { get; set; }
		public string LocationBarcode    { get; set; }
		public string DisplayCompanyName { get; set; }
		public string CompanyNumber      { get; set; }
		public string UserName           { get; set; }
		public string Password           { get; set; }

		public bool    Enabled                { get; set; }
		public string  BillToCompany          { get; set; }
		public decimal CreditLimit            { get; set; }
		public bool    EmailInvoice           { get; set; }
		public string  InvoiceOverride        { get; set; }
		public bool    MultipleTripsOnInvoice { get; set; }
		public short   BillingPeriod          { get; set; }

		public CompanyLogger( Company company, AddressLogger? address )
		{
			CompanyName        = company.CompanyName;
			LocationBarcode    = company.LocationBarcode;
			DisplayCompanyName = company.DisplayCompanyName;
			CompanyNumber      = company.CompanyNumber;
			Password           = company.Password;
			UserName           = company.UserName;
			Address            = address;
			Enabled            = company.Enabled;

			BillToCompany   = company.BillToCompany;
			CreditLimit     = company.CreditLimit;
			EmailInvoice    = company.EmailInvoice;
			InvoiceOverride = company.InvoiceOverride;

			MultipleTripsOnInvoice = company.MultipleTripsOnInvoice;
			BillingPeriod          = company.BillingPeriod;
		}
	}

	private static string GetZoneName( IRequestContext context, Address addr )
	{
		using var Db = new CarrierDb( context );
		return Db.GetZoneNameById( addr.ZoneId );
	}

	private static Protocol.Data.Company ConvertCompany( IRequestContext context, Company co, Address addr )
	{
		var RetVal = new Protocol.Data.Company
					 {
						 CompanyId       = co.CompanyId,
						 CompanyName     = co.CompanyName ?? "",
						 CompanyNumber   = co.CompanyNumber ?? "",
						 LocationBarcode = co.LocationBarcode ?? "",

						 Enabled = co.Enabled,

						 Password = co.Password ?? "",
						 UserName = co.UserName ?? "",

						 BillToCompany   = co.BillToCompany ?? "",
						 CreditLimit     = co.CreditLimit,
						 EmailInvoice    = co.EmailInvoice,
						 InvoiceOverride = co.InvoiceOverride ?? "",

						 MultipleTripsOnInvoice = co.MultipleTripsOnInvoice,
						 BillingPeriod          = co.BillingPeriod,

						 Country       = addr.Country ?? "",
						 CountryCode   = addr.CountryCode ?? "",
						 Suite         = addr.Suite ?? "",
						 AddressLine1  = addr.AddressLine1 ?? "",
						 AddressLine2  = addr.AddressLine2 ?? "",
						 Barcode       = addr.Barcode ?? "",
						 City          = addr.City ?? "",
						 ContactName   = addr.Contact ?? "",
						 EmailAddress  = addr.EmailAddress ?? "",
						 EmailAddress1 = addr.EmailAddress1 ?? "",
						 EmailAddress2 = addr.EmailAddress2 ?? "",
						 Fax           = addr.Fax ?? "",
						 Latitude      = addr.Latitude,
						 Longitude     = addr.Longitude,
						 Mobile        = addr.Mobile ?? "",
						 Mobile1       = addr.Mobile1 ?? "",
						 Notes         = addr.Notes ?? "",
						 Phone         = addr.Phone ?? "",
						 Phone1        = addr.Phone1 ?? "",
						 PostalBarcode = addr.PostalBarcode ?? "",
						 PostalCode    = (addr.PostalCode ?? "").TrimMax20(),
						 Region        = addr.Region ?? "",
						 Vicinity      = addr.Vicinity ?? "",
						 ZoneName      = GetZoneName( context, addr )
					 };
		return RetVal;
	}

	private static CompanyAddress ConvertCompanyAddress( IRequestContext context, Company co, Address addr )
	{
		var RetVal = new CompanyAddress
					 {
						 CompanyId       = co.CompanyId,
						 CompanyName     = co.CompanyName ?? "",
						 CompanyNumber   = co.CompanyNumber ?? "",
						 LocationBarcode = co.LocationBarcode ?? "",

						 Enabled = co.Enabled,

						 BillToCompany   = co.BillToCompany,
						 CreditLimit     = co.CreditLimit,
						 EmailInvoice    = co.EmailInvoice,
						 InvoiceOverride = co.InvoiceOverride,

						 MultipleTripsOnInvoice = co.MultipleTripsOnInvoice,
						 BillingPeriod          = co.BillingPeriod,

						 Country       = addr.Country ?? "",
						 CountryCode   = addr.CountryCode ?? "",
						 Suite         = addr.Suite ?? "",
						 AddressLine1  = addr.AddressLine1 ?? "",
						 AddressLine2  = addr.AddressLine2 ?? "",
						 Barcode       = addr.Barcode ?? "",
						 City          = addr.City ?? "",
						 ContactName   = addr.Contact ?? "",
						 EmailAddress  = addr.EmailAddress ?? "",
						 EmailAddress1 = addr.EmailAddress1 ?? "",
						 EmailAddress2 = addr.EmailAddress2 ?? "",
						 Fax           = addr.Fax ?? "",
						 Latitude      = addr.Latitude,
						 Longitude     = addr.Longitude,
						 Mobile        = addr.Mobile ?? "",
						 Mobile1       = addr.Mobile1 ?? "",
						 Notes         = addr.Notes ?? "",
						 Phone         = addr.Phone ?? "",
						 Phone1        = addr.Phone1 ?? "",
						 PostalBarcode = addr.PostalBarcode ?? "",
						 PostalCode    = addr.PostalCode ?? "",
						 Region        = addr.Region ?? "",
						 Vicinity      = addr.Vicinity ?? "",
						 ZoneName      = GetZoneName( context, addr )
					 };

		return RetVal;
	}

	public ( bool Ok, Company Company, Address Address ) GetCompanyByLocationBarcode( string locationBarcode )
	{
		var E = Entity;

		var Ca = ( from Company in E.Companies
				   where Company.LocationBarcode == locationBarcode
				   join A in E.Addresses on Company.PrimaryAddressId equals A.Id
				   select new {Company, Address = A} ).FirstOrDefault();

		return Ca is not null ? ( true, Ca.Company, Ca.Address ) : ( false, null!, null! );
	}

	public (CompanyLogger Before, CompanyLogger After, bool Ok) UpdateCompany( long companyId, Protocol.Data.Company co, string programName )
	{
		CompanyLogger Before,
					  After;
		var Ok = false;

		try
		{
			var (PValue, POk) = Encryption.FromTimeLimitedToken( co.Password );

			var E = Entity;

			var Rec = ( from C in E.Companies
						where C.CompanyId == companyId
						select C ).FirstOrDefault();

			if( Rec is not null )
			{
				var (ABefore, AAfter, Id, _) = UpdateAddress( Rec.PrimaryAddressId, co, programName );

				Before = new CompanyLogger( Rec, ABefore );

				Rec.LastModified = DateTime.UtcNow;

				Rec.PrimaryAddressId   = Id;
				Rec.DisplayCompanyName = co.CompanyName;

				//Rec.CompanyName = co.CompanyName.ToUpper();
				Rec.CompanyName     = co.CompanyName; // cjt 20190808 
				Rec.CompanyNumber   = co.CompanyNumber;
				Rec.LocationBarcode = co.LocationBarcode;

				Rec.Enabled = co.Enabled;

				Rec.BillToCompany          = co.BillToCompany;
				Rec.CreditLimit            = co.CreditLimit;
				Rec.EmailInvoice           = co.EmailInvoice;
				Rec.InvoiceOverride        = co.InvoiceOverride;
				Rec.MultipleTripsOnInvoice = co.MultipleTripsOnInvoice;
				Rec.BillingPeriod          = co.BillingPeriod;

				if( POk )
					Rec.Password = PValue;

				Rec.UserName = co.UserName;

				E.SaveChanges();

				After = new CompanyLogger( Rec, AAfter );
				Ok    = true;
			}
			else
				Before = After = new CompanyLogger( new Company(), new AddressLogger( new Address() ) );
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
			Before = After = new CompanyLogger( new Company(), new AddressLogger( new Address() ) );
		}

		return ( Before, After, Ok );
	}

	public ( Company? Company, CompanyLogger? Log, bool Ok ) AddCompany( Protocol.Data.Company company, string programName )
	{
		( Company? Company, CompanyLogger? Log, bool Ok ) Retval = ( Company: null, Log: null, Ok: false );
		var (Value, POk) = Encryption.FromTimeLimitedToken( company.Password );

		var E = Entity;

		try
		{
			var (AddressId, Address, Ok) = AddAddress( company, programName );

			if( Ok )
			{
				var Company = new Company
							  {
								  CompanyName     = company.CompanyName,
								  LocationBarcode = company.LocationBarcode,

								  BillToCompany   = company.BillToCompany,
								  CreditLimit     = company.CreditLimit,
								  EmailInvoice    = company.EmailInvoice,
								  InvoiceOverride = company.InvoiceOverride,

								  MultipleTripsOnInvoice = company.MultipleTripsOnInvoice,
								  BillingPeriod          = company.BillingPeriod,

								  Enabled = company.Enabled,

								  DisplayCompanyName = company.CompanyName,
								  CompanyNumber      = company.CompanyNumber,
								  UserName           = company.UserName,
								  Password           = POk ? Value : "",
								  PrimaryAddressId   = AddressId,
								  LastModified       = DateTime.Now
							  };

				E.Companies.Add( Company );
				E.SaveChanges();

				E.CompanyAddresses.Add( new MasterTemplate.CompanyAddress
										{
											CompanyId = Company.CompanyId,
											AddressId = AddressId
										} );
				E.SaveChanges();
				Retval.Company = Company;
				Retval.Log     = new CompanyLogger( Company, Address );
				Retval.Ok      = true;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( AddCompany ), Exception );
		}

		return Retval;
	}

	public (Company? Company, bool Ok) ImportCompany( Protocol.Data.Company company, string programName )
	{
		(Company? Company, bool Ok) Retval = ( Company: null, Ok: false );
		var                         E      = Entity;

		try
		{
			var (AddressId, Address, Ok) = AddAddress( company, programName );

			if( Ok )
			{
				var Company = new Company
							  {
								  CompanyName        = company.CompanyName,
								  LocationBarcode    = company.LocationBarcode,
								  DisplayCompanyName = company.CompanyName,
								  CompanyNumber      = company.CompanyNumber,
								  BillToCompany      = company.BillToCompany,

								  Enabled = company.Enabled,

								  CreditLimit     = company.CreditLimit,
								  EmailInvoice    = company.EmailInvoice,
								  InvoiceOverride = company.InvoiceOverride,

								  MultipleTripsOnInvoice = company.MultipleTripsOnInvoice,
								  BillingPeriod          = company.BillingPeriod,

								  UserName         = company.UserName,
								  Password         = company.Password,
								  PrimaryAddressId = AddressId,
								  LastModified     = DateTime.Now
							  };

				E.Companies.Add( Company );
				E.SaveChanges();

				E.CompanyAddresses.Add( new MasterTemplate.CompanyAddress
										{
											CompanyId = Company.CompanyId,
											AddressId = AddressId
										} );
				E.SaveChanges();
				Context.LogDifferences( "CompanyImport", programName, "Import company", new CompanyLogger( Company, Address ) );
				Retval.Company = Company;
				Retval.Ok      = true;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( ImportCompany ), Exception );
		}

		return Retval;
	}

	public (Company Company, bool Ok ) GetCompany( long companyId )
	{
		var Comp = ( from Co in Entity.Companies
					 where Co.CompanyId == companyId
					 select Co ).FirstOrDefault();

		return ( Company: Comp ?? new Company(), Ok: Comp is not null );
	}

	public (Address Address, bool Ok ) GetCompanyAddress( long companyId )
	{
		var E = Entity;

		var Addr = ( from Co in E.Companies
					 where Co.CompanyId == companyId
					 join A in E.Addresses on Co.PrimaryAddressId equals A.Id
					 select A ).FirstOrDefault();

		return ( Address: Addr ?? new Address(), Ok: Addr is not null );
	}

	public (Address? Address, bool Ok) GetCompanyPrimaryAddress( Company company ) => GetAddress( company.PrimaryAddressId );

	public (Company Company, bool Ok) GetCompanyByName( string companyName )
	{
		try
		{
			companyName = companyName.ToUpper();

			var Comp = ( from C in Entity.Companies
						 where C.CompanyName == companyName
						 select C ).FirstOrDefault();

			return ( Company: Comp ?? new Company(), Ok: Comp is not null );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return ( null!, false );
	}

	public (Company Company, bool Ok) GetCompanyByCompanyNumber( string companyCode )
	{
		try
		{
			companyCode = companyCode.ToUpper();

			var Comp = ( from C in Entity.Companies
						 where C.CompanyNumber == companyCode
						 select C ).FirstOrDefault();

			return ( Company: Comp ?? new Company(), Ok: Comp is not null );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return ( null!, false );
	}

	public void UpdateCompanyGps( long coId, decimal lat, decimal lng )
	{
		if( ( coId != 0 ) && ( ( lat != 0 ) || ( lng != 0 ) ) )
		{
			var (Address, Ok) = GetCompanyAddress( coId );

			if( Ok )
			{
				var RLat = Address.Latitude;
				var RLng = Address.Longitude;

				if( ( ( RLat == 0 ) && ( RLng == 0 ) ) || ( Math.Abs( Measurement.SimpleGpsDistance( RLat, RLng, lat, lng ).Metres ) >= MAX_UPDATE_DISTANCE_IM_METRES ) )
				{
					RLat = lat;
					RLng = lng;
				}
				else
				{
					RLat = ( RLat + lat ) / 2;
					RLng = ( RLng + lng ) / 2;
				}

				Address.Latitude  = RLat;
				Address.Longitude = RLng;
				Entity.SaveChanges();
			}
		}
	}

	public List<Company> GetCompaniesByName( string companyName )
	{
		companyName = companyName.ToUpper();

		return ( from C in Entity.Companies
				 where C.CompanyName == companyName
				 select C ).ToList();
	}

	public List<Address> GetCompanyAddresses( long companyId )
	{
		var E = Entity;

		return ( from Ca in E.CompanyAddresses
				 where Ca.CompanyId == companyId
				 from A in E.Addresses
				 where A.Id == Ca.AddressId
				 select A ).ToList();
	}

	public List<Company> GetCompaniesByCompanyAndAddressLine1( string companyName, string suite, string addressLine1 )
	{
		var RetVal = new List<Company>();

		try
		{
			var Addresses = CheckSuiteAndAddressLine1( suite, addressLine1 );

			if( Addresses.Count > 0 )
			{
				var E = Entity;

				companyName = companyName.ToUpper();

				var Companies = from C in E.Companies
								where C.CompanyName == companyName
								select C;

				if( Companies.Any() )
				{
					foreach( var Company in Companies )
					{
						foreach( var Address in Addresses )
						{
							var Rec = ( from CompanyAddress in E.CompanyAddresses
										where ( CompanyAddress.AddressId == Address.Id ) && ( CompanyAddress.CompanyId == Company.CompanyId )
										select CompanyAddress ).FirstOrDefault();

							if( Rec is not null )
								RetVal.Add( Company );
						}
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return RetVal;
	}

	public (Company? Company, Address? Address, bool Ok) GetCompanyBySecondaryId( string id )
	{
		var (Address, Ok) = GetAddressBySecondaryId( id );

		if( Ok && Address is not null )
		{
			var Company = ( from A in Entity.CompanyAddresses
							where A.AddressId == Address.Id
							join C in Entity.Companies on A.CompanyId equals C.CompanyId
							select C ).FirstOrDefault();

			return ( Company, Address, Company is not null );
		}

		return ( null, null, false );
	}

	public CompanySummaryList GetCustomerCompaniesSummary( string customerCode )
	{
		var RetVal = new CompanySummaryList();

		try
		{
			var Recs = from Rc in Entity.ResellerCustomerCompanySummaries
					   where Rc.CustomerCode == customerCode
					   orderby Rc.CompanyName
					   select Rc;

			RetVal.AddRange( Recs.Select( rec => new CompanyByAccountSummary
												 {
													 CompanyId       = rec.CompanyId,
													 CustomerCode    = customerCode,
													 CompanyName     = rec.CompanyName ?? "",
													 LocationBarcode = rec.LocationBarcode ?? "",

													 Enabled = rec.Enabled,

													 Suite        = rec.Suite ?? "",
													 AddressLine1 = rec.AddressLine1 ?? "",
													 AddressLine2 = rec.AddressLine2 ?? "",
													 City         = rec.City ?? "",
													 Region       = rec.Region ?? "",
													 PostalCode   = rec.PostalCode ?? "",
													 CountryCode  = rec.CountryCode ?? ""
												 } ) );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( nameof( GetCustomerCompaniesSummary ), Exception );
		}

		return RetVal;
	}

	public CompanyDetailList GetCustomerCompaniesDetailed( string customerCode )
	{
		var Result = new CompanyDetailList();

		try
		{
			var E = Entity;

			var CustomerCompanies = ( from C in E.ResellerCustomerCompanies
									  where C.CustomerCode == customerCode
									  join Company in E.Companies on C.CompanyId equals Company.CompanyId
									  join Address in E.Addresses on Company.PrimaryAddressId equals Address.Id into Addresses
									  select new {Company, Addresses} ).ToList();

			foreach( var Co in CustomerCompanies )
			{
				if( Co is not null )
				{
					Result.AddRange( from Address in Co.Addresses
									 select new CompanyDetail
											{
												Company = ConvertCompany( Context, Co.Company, Address ),
												Address = ConvertCompanyAddress( Context, Co.Company, Address )
											}
								   );
				}
			}
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}

		return Result;
	}
}