﻿using StorageV2.AuditTrails;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

// ReSharper disable InconsistentNaming

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class TripLoggerItem
	{
		public string ItemCode { get; set; }

		public string ItemCode1 { get; set; }

		public string Barcode { get; set; }

		public string Barcode1 { get; set; }

		public string Description { get; set; }

		public string Reference { get; set; }

		public decimal Height { get; set; }

		public decimal Width { get; set; }

		public decimal Length { get; set; }

		public decimal Volume { get; set; }

		public decimal Pieces { get; set; }

		public decimal Weight { get; set; }

		public decimal Value { get; set; }

		public decimal Tax1 { get; set; }

		public decimal Tax2 { get; set; }

		public bool HasDocuments { get; set; }

		public decimal Original { get; set; }

		public decimal Qh { get; set; }

		public TripLoggerItem( Protocol.Data.TripItem i )
		{
			ItemCode     = i.ItemCode;
			ItemCode1    = i.ItemCode1;
			Barcode      = i.Barcode;
			Barcode1     = i.Barcode1;
			Description  = i.Description;
			Reference    = i.Reference;
			Height       = i.Height;
			Width        = i.Width;
			Length       = i.Length;
			Volume       = i.Volume;
			Pieces       = i.Pieces;
			Weight       = i.Weight;
			Value        = i.Value;
			Tax1         = i.Tax1;
			Tax2         = i.Tax2;
			HasDocuments = i.HasDocuments;
			Original     = i.Original;
			Qh           = i.Qh;
		}
	}

	public class TripLoggerPackage
	{
		public string PackageType { get; set; }

		public decimal Height { get; set; }

		public decimal Width { get; set; }

		public decimal Length { get; set; }

		public decimal Volume { get; set; }

		public decimal Pieces { get; set; }

		public decimal Weight { get; set; }

		public decimal Value { get; set; }

		public decimal Tax1 { get; set; }

		public decimal Tax2 { get; set; }

		public bool HasDocuments { get; set; }

		public decimal Original { get; set; }

		public decimal Qh { get; set; }

		public List<TripLoggerItem> Items { get; set; }

		public TripLoggerPackage( TripPackage p )
		{
			PackageType  = p.PackageType;
			Height       = p.Height;
			Width        = p.Width;
			Length       = p.Length;
			Volume       = p.Volume;
			Pieces       = p.Pieces;
			Weight       = p.Weight;
			Value        = p.Value;
			Tax1         = p.Tax1;
			Tax2         = p.Tax2;
			HasDocuments = p.HasDocuments;
			Original     = p.Original;
			Qh           = p.Qh;

			Items = ( from I in p.Items
			          select new TripLoggerItem( I ) ).ToList();
		}
	}

	public class TripLoggerCharge
	{
		public string  ChargeId { get; set; }
		public decimal Quantity { get; set; }
		public decimal Value    { get; set; }
		public string  Text     { get; set; }

		public TripLoggerCharge( TripCharge c )
		{
			ChargeId = c.ChargeId;
			Quantity = c.Quantity;
			Value    = c.Value;
			Text     = c.Text;
		}
	}

	public class TripLogger
	{
		public string TripId { get; set; } = "";

		public string Location { get; set; } = "";

		public DateTime LastModified { get; set; }

		public int    Status1       { get; set; }
		public string Status1AsText => ( (STATUS)Status1 ).AsString();
		public int    Status2       { get; set; }
		public string Status2AsText => ( (STATUS1)Status2 ).AsString();
		public int    Status3       { get; set; }
		public string Status3AsText => "";
		public int    Status4       { get; set; }
		public string Status4AsText => ( (STATUS3)Status4 ).AsString();

		public bool ReceivedByDevice { get; set; }
		public bool ReadByDriver     { get; set; }

		public string Barcode   { get; set; } = "";
		public string AccountId { get; set; } = "";

		public string Reference { get; set; } = "";

		public DateTimeOffset CallTime    { get; set; }
		public string         CallerPhone { get; set; } = "";
		public string         CallerEmail { get; set; } = "";

		public DateTimeOffset ReadyTime { get; set; }
		public DateTimeOffset DueTime   { get; set; }

		public string DriverCode   { get; set; } = "";
		public string Board        { get; set; } = "";
		public string PackageType  { get; set; } = "";
		public string ServiceLevel { get; set; } = "";

		public string  PickupAccountId            { get; set; } = "";
		public long    PickupCompanyId            { get; set; }
		public string  PickupCompanyName          { get; set; } = "";
		public string  PickupAddressBarcode       { get; set; } = "";
		public string  PickupAddressPostalBarcode { get; set; } = "";
		public string  PickupAddressSuite         { get; set; } = "";
		public string  PickupAddressAddressLine1  { get; set; } = "";
		public string  PickupAddressAddressLine2  { get; set; } = "";
		public string  PickupAddressVicinity      { get; set; } = "";
		public string  PickupAddressCity          { get; set; } = "";
		public string  PickupAddressRegion        { get; set; } = "";
		public string  PickupAddressPostalCode    { get; set; } = "";
		public string  PickupAddressCountry       { get; set; } = "";
		public string  PickupAddressCountryCode   { get; set; } = "";
		public string  PickupAddressPhone         { get; set; } = "";
		public string  PickupAddressPhone1        { get; set; } = "";
		public string  PickupAddressMobile        { get; set; } = "";
		public string  PickupAddressMobile1       { get; set; } = "";
		public string  PickupAddressFax           { get; set; } = "";
		public string  PickupAddressEmailAddress  { get; set; } = "";
		public string  PickupAddressEmailAddress1 { get; set; } = "";
		public string  PickupAddressEmailAddress2 { get; set; } = "";
		public string  PickupContact              { get; set; } = "";
		public decimal PickupAddressLatitude      { get; set; }
		public decimal PickupAddressLongitude     { get; set; }
		public string  PickupAddressNotes         { get; set; } = "";
		public string  PickupNotes                { get; set; } = "";

		public string  DeliveryAccountId            { get; set; } = "";
		public long    DeliveryCompanyId            { get; set; }
		public string  DeliveryCompanyName          { get; set; } = "";
		public string  DeliveryAddressBarcode       { get; set; } = "";
		public string  DeliveryAddressPostalBarcode { get; set; } = "";
		public string  DeliveryAddressSuite         { get; set; } = "";
		public string  DeliveryAddressAddressLine1  { get; set; } = "";
		public string  DeliveryAddressAddressLine2  { get; set; } = "";
		public string  DeliveryAddressVicinity      { get; set; } = "";
		public string  DeliveryAddressCity          { get; set; } = "";
		public string  DeliveryAddressRegion        { get; set; } = "";
		public string  DeliveryAddressPostalCode    { get; set; } = "";
		public string  DeliveryAddressCountry       { get; set; } = "";
		public string  DeliveryAddressCountryCode   { get; set; } = "";
		public string  DeliveryAddressPhone         { get; set; } = "";
		public string  DeliveryAddressPhone1        { get; set; } = "";
		public string  DeliveryAddressMobile        { get; set; } = "";
		public string  DeliveryAddressMobile1       { get; set; } = "";
		public string  DeliveryAddressFax           { get; set; } = "";
		public string  DeliveryAddressEmailAddress  { get; set; } = "";
		public string  DeliveryAddressEmailAddress1 { get; set; } = "";
		public string  DeliveryAddressEmailAddress2 { get; set; } = "";
		public string  DeliveryContact              { get; set; } = "";
		public decimal DeliveryAddressLatitude      { get; set; }
		public decimal DeliveryAddressLongitude     { get; set; }
		public string  DeliveryAddressNotes         { get; set; } = "";
		public string  DeliveryNotes                { get; set; } = "";

		public string  BillingAccountId            { get; set; } = "";
		public long    BillingCompanyId            { get; set; }
		public string  BillingCompanyName          { get; set; } = "";
		public string  BillingAddressBarcode       { get; set; } = "";
		public string  BillingAddressPostalBarcode { get; set; } = "";
		public string  BillingAddressSuite         { get; set; } = "";
		public string  BillingAddressAddressLine1  { get; set; } = "";
		public string  BillingAddressAddressLine2  { get; set; } = "";
		public string  BillingAddressVicinity      { get; set; } = "";
		public string  BillingAddressCity          { get; set; } = "";
		public string  BillingAddressRegion        { get; set; } = "";
		public string  BillingAddressPostalCode    { get; set; } = "";
		public string  BillingAddressCountry       { get; set; } = "";
		public string  BillingAddressCountryCode   { get; set; } = "";
		public string  BillingAddressPhone         { get; set; } = "";
		public string  BillingAddressPhone1        { get; set; } = "";
		public string  BillingAddressMobile        { get; set; } = "";
		public string  BillingAddressMobile1       { get; set; } = "";
		public string  BillingAddressFax           { get; set; } = "";
		public string  BillingAddressEmailAddress  { get; set; } = "";
		public string  BillingAddressEmailAddress1 { get; set; } = "";
		public string  BillingAddressEmailAddress2 { get; set; } = "";
		public string  BillingContact              { get; set; } = "";
		public decimal BillingAddressLatitude      { get; set; }
		public decimal BillingAddressLongitude     { get; set; }
		public string  BillingAddressNotes         { get; set; } = "";
		public string  BillingNotes                { get; set; } = "";

		public string  CurrentZone        { get; set; } = "";
		public string  PickupZone         { get; set; } = "";
		public string  DeliveryZone       { get; set; } = "";
		public decimal Weight             { get; set; }
		public decimal Pieces             { get; set; }
		public decimal Volume             { get; set; }
		public string  Measurement        { get; set; } = null!;
		public decimal OriginalPieceCount { get; set; }

		public byte[] MissingPieces { get; set; } = null!;

		public bool                    DangerousGoods             { get; set; }
		public bool                    HasItems                   { get; set; }
		public string                  UNClass                    { get; set; } = "";
		public bool                    HasDangerousGoodsDocuments { get; set; }
		public string                  POP                        { get; set; } = "";
		public string                  POD                        { get; set; } = "";
		public DateTimeOffset          PickupTime                 { get; set; }
		public double                  PickupLatitude             { get; set; }
		public double                  PickupLongitude            { get; set; }
		public DateTimeOffset          DeliveredTime              { get; set; }
		public double                  DeliveredLatitude          { get; set; }
		public double                  DeliveredLongitude         { get; set; }
		public DateTimeOffset          VerifiedTime               { get; set; }
		public double                  VerifiedLatitude           { get; set; }
		public double                  VerifiedLongitude          { get; set; }
		public DateTimeOffset          ClaimedTime                { get; set; }
		public double                  ClaimedLatitude            { get; set; }
		public double                  ClaimedLongitude           { get; set; }
		public List<TripLoggerPackage> TripPackages               { get; set; } = [];
		public List<TripLoggerCharge>  TripCharges                { get; set; } = [];
		public string                  CallerName                 { get; set; } = null!;
		public bool                    IsQuote                    { get; set; }
		public string                  UndeliverableNotes         { get; set; } = null!;

		public string Group0 { get; set; } = "";
		public string Group1 { get; set; } = "";
		public string Group2 { get; set; } = "";
		public string Group3 { get; set; } = "";
		public string Group4 { get; set; } = "";

		public string Route       { get; set; } = "";
		public string CallTakerId { get; set; } = "";

		public string DriverNotes { get; set; } = "";

		public DateTimeOffset PickupWaitTimeStart             { get; set; }
		public int            PickupWaitTimeDurationInSeconds { get; set; }

		public DateTimeOffset DeliveryWaitTimeStart             { get; set; }
		public int            DeliveryWaitTimeDurationInSeconds { get; set; }

		public DateTimeOffset PickupArriveTime   { get; set; }
		public DateTimeOffset DeliveryArriveTime { get; set; }

		public long    InvoiceNumber      { get; set; }
		public decimal TotalAmount        { get; set; }
		public decimal TotalTaxAmount     { get; set; }
		public decimal DiscountAmount     { get; set; }
		public decimal TotalFixedAmount   { get; set; }
		public decimal TotalPayrollAmount { get; set; }
		public decimal DeliveryAmount     { get; set; }
		public int     Pallets            { get; set; }
		public string  Schedule           { get; set; } = "";
		public string  ImportReference    { get; set; } = "";

		public TripLogger( Trip t )
		{
			TripId       = t.TripId;
			Location     = t.Location;
			Status1      = t.Status1;
			LastModified = t.LastModified;
			Status2      = t.Status2;
			Status3      = t.Status3;
			Status4      = t.Status4;

			ReceivedByDevice = t.ReceivedByDevice;
			ReadByDriver     = t.ReadByDriver;

			Barcode     = t.Barcode;
			AccountId   = t.AccountId;
			Reference   = t.Reference;
			CallTime    = t.CallTime;
			CallerPhone = t.CallerPhone;
			CallerEmail = t.CallerEmail;
			ReadyTime   = t.ReadyTime;
			DueTime     = t.DueTime;
			DriverCode  = t.DriverCode;

			Board        = t.Board;
			PackageType  = t.PackageType;
			ServiceLevel = t.ServiceLevel;

			PickupCompanyId            = t.PickupCompanyId;
			PickupCompanyName          = t.PickupCompanyName;
			PickupAddressBarcode       = t.PickupAddressBarcode;
			PickupAddressPostalBarcode = t.PickupAddressPostalBarcode;
			PickupAddressSuite         = t.PickupAddressSuite;
			PickupAddressAddressLine1  = t.PickupAddressAddressLine1;
			PickupAddressAddressLine2  = t.PickupAddressAddressLine2;
			PickupAddressVicinity      = t.PickupAddressVicinity;
			PickupAddressCity          = t.PickupAddressCity;
			PickupAddressRegion        = t.PickupAddressRegion;
			PickupAddressPostalCode    = t.PickupAddressPostalCode;
			PickupAddressCountry       = t.PickupAddressCountry;
			PickupAddressCountryCode   = t.PickupAddressCountryCode;
			PickupAddressPhone         = t.PickupAddressPhone;
			PickupAddressPhone1        = t.PickupAddressPhone1;
			PickupAddressMobile        = t.PickupAddressMobile;
			PickupAddressMobile1       = t.PickupAddressMobile1;
			PickupAddressFax           = t.PickupAddressFax;
			PickupAddressEmailAddress  = t.PickupAddressEmailAddress;
			PickupAddressEmailAddress1 = t.PickupAddressEmailAddress1;
			PickupAddressEmailAddress2 = t.PickupAddressEmailAddress2;
			PickupContact              = t.PickupContact;
			PickupAddressLatitude      = t.PickupAddressLatitude;
			PickupAddressLongitude     = t.PickupAddressLongitude;
			PickupAddressNotes         = t.PickupAddressNotes;
			PickupNotes                = t.PickupNotes;

			DeliveryCompanyId            = t.DeliveryCompanyId;
			DeliveryCompanyName          = t.DeliveryCompanyName;
			DeliveryAddressBarcode       = t.DeliveryAddressBarcode;
			DeliveryAddressPostalBarcode = t.DeliveryAddressPostalBarcode;
			DeliveryAddressSuite         = t.DeliveryAddressSuite;
			DeliveryAddressAddressLine1  = t.DeliveryAddressAddressLine1;
			DeliveryAddressAddressLine2  = t.DeliveryAddressAddressLine2;
			DeliveryAddressVicinity      = t.DeliveryAddressVicinity;
			DeliveryAddressCity          = t.DeliveryAddressCity;
			DeliveryAddressRegion        = t.DeliveryAddressRegion;
			DeliveryAddressPostalCode    = t.DeliveryAddressPostalCode;
			DeliveryAddressCountry       = t.DeliveryAddressCountry;
			DeliveryAddressCountryCode   = t.DeliveryAddressCountryCode;
			DeliveryAddressPhone         = t.DeliveryAddressPhone;
			DeliveryAddressPhone1        = t.DeliveryAddressPhone1;
			DeliveryAddressMobile        = t.DeliveryAddressMobile;
			DeliveryAddressMobile1       = t.DeliveryAddressMobile1;
			DeliveryAddressFax           = t.DeliveryAddressFax;
			DeliveryAddressEmailAddress  = t.DeliveryAddressEmailAddress;
			DeliveryAddressEmailAddress1 = t.DeliveryAddressEmailAddress1;
			DeliveryAddressEmailAddress2 = t.DeliveryAddressEmailAddress2;
			DeliveryContact              = t.DeliveryContact;
			DeliveryAddressLatitude      = t.DeliveryAddressLatitude;
			DeliveryAddressLongitude     = t.DeliveryAddressLongitude;
			DeliveryAddressNotes         = t.DeliveryAddressNotes;
			DeliveryNotes                = t.DeliveryNotes;

			BillingCompanyId            = t.BillingCompanyId;
			BillingCompanyName          = t.BillingCompanyName;
			BillingAddressBarcode       = t.BillingAddressBarcode;
			BillingAddressPostalBarcode = t.BillingAddressPostalBarcode;
			BillingAddressSuite         = t.BillingAddressSuite;
			BillingAddressAddressLine1  = t.BillingAddressAddressLine1;
			BillingAddressAddressLine2  = t.BillingAddressAddressLine2;
			BillingAddressVicinity      = t.BillingAddressVicinity;
			BillingAddressCity          = t.BillingAddressCity;
			BillingAddressRegion        = t.BillingAddressRegion;
			BillingAddressPostalCode    = t.BillingAddressPostalCode;
			BillingAddressCountry       = t.BillingAddressCountry;
			BillingAddressCountryCode   = t.BillingAddressCountryCode;
			BillingAddressPhone         = t.BillingAddressPhone;
			BillingAddressPhone1        = t.BillingAddressPhone1;
			BillingAddressMobile        = t.BillingAddressMobile;
			BillingAddressMobile1       = t.BillingAddressMobile1;
			BillingAddressFax           = t.BillingAddressFax;
			BillingAddressEmailAddress  = t.BillingAddressEmailAddress;
			BillingAddressEmailAddress1 = t.BillingAddressEmailAddress1;
			BillingAddressEmailAddress2 = t.BillingAddressEmailAddress2;
			BillingContact              = t.BillingContact;
			BillingAddressLatitude      = t.BillingAddressLatitude;
			BillingAddressLongitude     = t.BillingAddressLongitude;
			BillingAddressNotes         = t.BillingAddressNotes;
			BillingNotes                = t.BillingNotes;

			CurrentZone  = t.CurrentZone;
			PickupZone   = t.PickupZone;
			DeliveryZone = t.DeliveryZone;

			Weight             = t.Weight;
			Pieces             = t.Pieces;
			OriginalPieceCount = t.OriginalPieceCount;
			Volume             = t.Volume;
			Measurement        = t.Measurement;
			MissingPieces      = t.MissingPieces;

			DangerousGoods             = t.DangerousGoods;
			HasItems                   = t.HasItems;
			UNClass                    = t.UNClass;
			HasDangerousGoodsDocuments = t.HasDangerousGoodsDocuments;
			POP                        = t.POP;
			POD                        = t.POD;
			PickupTime                 = t.PickupTime;
			PickupLatitude             = t.PickupLatitude;
			PickupLongitude            = t.PickupLongitude;
			DeliveredTime              = t.DeliveredTime;
			DeliveredLatitude          = t.DeliveredLatitude;
			DeliveredLongitude         = t.DeliveredLongitude;
			VerifiedTime               = t.VerifiedTime;
			VerifiedLatitude           = t.VerifiedLatitude;
			VerifiedLongitude          = t.VerifiedLongitude;
			ClaimedTime                = t.ClaimedTime;
			ClaimedLatitude            = t.ClaimedLatitude;
			ClaimedLongitude           = t.ClaimedLongitude;

			try
			{
				TripCharges = ( from C in JsonConvert.DeserializeObject<List<TripCharge>>( t.TripChargesAsJson )
				                where C != null
				                select new TripLoggerCharge( C ) ).ToList();
			}
			catch
			{
				TripCharges = [];
			}

			if( (TripUpdate.PACKAGE_TYPES)t.TripItemsType == TripUpdate.PACKAGE_TYPES.PACKAGE )
			{
				try
				{
					TripPackages = ( from P in JsonConvert.DeserializeObject<List<TripPackage>>( t.TripItemsAsJson )
					                 where P != null
					                 select new TripLoggerPackage( P ) ).ToList();
				}
				catch
				{
					TripPackages = [];
				}
			}
			else
				TripPackages = [];

			CallerName         = t.CallerName;
			IsQuote            = t.IsQuote;
			UndeliverableNotes = t.UndeliverableNotes;

			PickupAccountId   = t.PickupCompanyAccountId;
			DeliveryAccountId = t.DeliveryCompanyAccountId;
			BillingAccountId  = t.BillingCompanyAccountId;

			Group0 = t.Group0;
			Group1 = t.Group1;
			Group2 = t.Group2;
			Group3 = t.Group3;
			Group4 = t.Group4;

			Route       = t.Route;
			CallTakerId = t.CallTakerId;

			DriverNotes = t.DriverNotes;

			PickupWaitTimeStart               = t.PickupWaitTimeStart;
			PickupWaitTimeDurationInSeconds   = t.PickupWaitTimeDurationInSeconds;
			DeliveryWaitTimeStart             = t.DeliveryWaitTimeStart;
			DeliveryWaitTimeDurationInSeconds = t.DeliveryWaitTimeDurationInSeconds;

			PickupArriveTime   = t.PickupArriveTime;
			DeliveryArriveTime = t.DeliveryArriveTime;

			InvoiceNumber      = t.InvoiceNumber;
			TotalAmount        = t.TotalAmount;
			TotalTaxAmount     = t.TotalTaxAmount;
			DiscountAmount     = t.DiscountAmount;
			TotalFixedAmount   = t.TotalFixedAmount;
			TotalPayrollAmount = t.TotalPayrollAmount;
			DeliveryAmount     = t.DeliveryAmount;
			Pallets            = t.Pallets;
			Schedule           = t.Schedule;
			ImportReference    = t.ImportReference;
		}

		public TripLogger()
		{
		}
	}

	public void LogTrip( string prog, TripLogger logger )
	{
		TripAuditTrail.LogNew( Context, prog, logger.TripId, logger );
	}

	public void LogTrip( string prog, TripLogger before, TripLogger after )
	{
		TripAuditTrail.LogDifferences( Context, prog, before.TripId, before, after );
	}

	public void LogTrip( string prog, Trip t )
	{
		LogTrip( prog, new TripLogger( t ) );
	}

	private void LogDeletedTrip( string prog, TripLogger after )
	{
		TripAuditTrail.LogDeleted( Context, prog, after.TripId, after );
	}

	private void LogDeletedTrip( string prog, Trip after )
	{
		LogDeletedTrip( prog, new TripLogger( after ) );
	}

	private void LogFinalisedTrip( string prog, TripLogger after )
	{
		TripAuditTrail.LogFinalised( Context, prog, after.TripId, after );
	}

	private void LogFinalisedTrip( string prog, Trip after )
	{
		LogFinalisedTrip( prog, new TripLogger( after ) );
	}
}