﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddToExport( Export? export )
	{
		if( export is not null )
		{
			try
			{
				var Db = new CarrierDb( Context );
				var E  = Db.Entity;

				export.Exported = DateTime.UtcNow;

				// Just add it, should already be unique
				E.Exports.Add( export );
				E.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}
}