﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public bool PushTripToExport( string tripId )
	{
		try
		{
			var E = Entity;

			var Rec = ( from T in E.Trips
			            where T.TripId == tripId
			            select T ).FirstOrDefault();

			if( Rec is not null )
			{
				var Before = new TripLogger( Rec );

				Rec.Status1 = (int)STATUS.LIMBO;
				Rec.Status4 = (int)STATUS3.IMPORTED;
				E.SaveChanges();
				AddTripToExport( tripId, (STATUS)Rec.Status1 );

				var After = new TripLogger( Rec );
				LogTrip( "Manual Push Trip To Export", Before, After );

				return true;
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return false;
	}
}