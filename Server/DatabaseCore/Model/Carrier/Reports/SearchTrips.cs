﻿using StorageV2.Carrier;
using Trip = Protocol.Data.Trip;

// ReSharper disable InvalidXmlDocComment

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public List<Trip> SearchPickupedAndVerifiedTripsByStatus( DateTimeOffset pickupedFrom, DateTimeOffset pickupedTo, DateTimeOffset verifiedFrom, DateTimeOffset verifiedTo,
	                                                          STATUS statusFrom, STATUS statsTo )
	{
		var Result = new List<Trip>();

		void SearchDb()
		{
			using var Db = new CarrierDb( Context );

			var Recs = ( from T in Db.Entity.Trips
			             where ( T.Status1 >= (int)statusFrom ) && ( T.Status1 <= (int)statsTo )
			                                                    && ( T.PickupTime >= pickupedFrom ) && ( T.PickupTime <= pickupedTo )
			                                                    && ( T.VerifiedTime >= verifiedFrom ) && ( T.VerifiedTime <= verifiedTo )
			             select T ).ToList();

			lock( Result )
			{
				Result.AddRange( from T in Recs
				                 select T.ToTrip() );
			}
		}

		void SearchStorage()
		{
			if( statsTo >= STATUS.FINALISED )
			{
				using var Db = new CarrierDb( Context );

				var Indices = ( from T in Db.Entity.TripStorageIndices
				                where ( T.Status1 >= (byte)statusFrom ) && ( T.Status1 <= (byte)statsTo )
				                                                        && ( T.PickupedTime >= pickupedFrom ) && ( T.PickupedTime <= pickupedTo )
				                                                        && ( T.VerifiedTime >= verifiedFrom ) && ( T.VerifiedTime <= verifiedTo )
				                select T ).ToList();

				foreach( var StorageIndex in Indices )
				{
					var Trips = TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId );

					lock( Result )
						Result.AddRange( Trips );
				}
			}
		}

		Task.WaitAll( Task.Run( SearchDb ),
		              Task.Run( SearchStorage ) );
		return Result;
	}

	public List<Trip> SearchTripsByDelByStatus( DateTimeOffset verifiedFrom, DateTimeOffset verifiedTo,
	                                            STATUS statusFrom, STATUS statsTo )
	{
		var Result = new List<Trip>();

		void SearchDb()
		{
			using var Db = new CarrierDb( Context );

			var Recs = ( from T in Db.Entity.Trips
			             where ( T.Status1 >= (int)statusFrom ) && ( T.Status1 <= (int)statsTo )
			                                                    && ( T.VerifiedTime >= verifiedFrom ) && ( T.VerifiedTime <= verifiedTo )
			             select T ).ToList();

			lock( Result )
			{
				Result.AddRange( from T in Recs
				                 select T.ToTrip() );
			}
		}

		void SearchStorage()
		{
			if( statsTo >= STATUS.FINALISED )
			{
				using var Db = new CarrierDb( Context );

				var Indices = ( from T in Db.Entity.TripStorageIndices
				                where ( T.Status1 >= (byte)statusFrom ) && ( T.Status1 <= (byte)statsTo )
				                                                        && ( T.VerifiedTime >= verifiedFrom ) && ( T.VerifiedTime <= verifiedTo )
				                select T ).ToList();

				foreach( var StorageIndex in Indices )
				{
					var Trips = TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId );

					lock( Result )
						Result.AddRange( Trips );
				}
			}
		}

		Task.WaitAll( Task.Run( SearchDb ),
		              Task.Run( SearchStorage ) );
		return Result;
	}

	/// <summary>
	///     Return a list of trips between dates and statuses
	///     Allow to select which dates to use
	///     CALL for call time
	///     VER for verified time
	///     PUVER for pickup or verified time
	/// </summary>
	/// <param
	///     name="searchByTime">
	/// </param>
	/// <param
	///     name="SearchByStatus">
	/// </param>
	/// <param
	///     name="pickupedFrom">
	/// </param>
	/// <param
	///     name="pickupedTo">
	/// </param>
	/// <param
	///     name="statusFrom">
	/// </param>
	/// <param
	///     name="statsTo">
	/// </param>
	/// <returns></returns>
	public List<Trip> SearchTripsBy( string searchByTime, DateTimeOffset from, DateTimeOffset to,
	                                 STATUS statusFrom = STATUS.UNSET, STATUS statsTo = STATUS.UNKNOWN )
	{
		var Result = new List<Trip>();

		void SearchDb()
		{
			using var Db       = new CarrierDb( Context );
			var       DbEntity = Db.Entity;
			var       Recs     = new List<MasterTemplate.Trip>();

			switch( searchByTime )
			{
			case "CALL":
				//
				Recs = ( from T in DbEntity.Trips
				         where ( T.Status1 >= (int)statusFrom ) && ( T.Status1 <= (int)statsTo )
				                                                && ( T.CallTime >= @from ) && ( T.CallTime <= to )
				         select T ).ToList();
				break;

			case "VER":
				Recs = ( from T in DbEntity.Trips
				         where ( T.Status1 >= (int)statusFrom ) && ( T.Status1 <= (int)statsTo )
				                                                && ( T.VerifiedTime >= @from ) && ( T.VerifiedTime <= to )
				         select T ).ToList();
				break;

			case "PUVER":
				Recs = ( from T in DbEntity.Trips
				         where ( T.Status1 >= (int)statusFrom ) && ( T.Status1 <= (int)statsTo )
				                                                && ( T.PickupTime >= @from ) && ( T.PickupTime <= to )
				                                                && ( T.VerifiedTime >= @from ) && ( T.VerifiedTime <= to )
				         select T ).ToList();
				break;
			}

			lock( Result )
			{
				Result.AddRange( from T in Recs
				                 select T.ToTrip() );
			}
		}

		void SearchStorage()
		{
			if( statsTo >= STATUS.FINALISED )
			{
				using var Db      = new CarrierDb( Context );
				var       Indices = new List<TripStorageIndex>();

				switch( searchByTime )
				{
				case "CALL":
					Indices = ( from T in Db.Entity.TripStorageIndices
					            where ( T.Status1 >= (byte)statusFrom ) && ( T.Status1 <= (byte)statsTo )
					                                                    && ( T.CallTime >= @from ) && ( T.CallTime <= to )
					            select T ).ToList();
					break;

				case "VER":
					Indices = ( from T in Db.Entity.TripStorageIndices
					            where ( T.Status1 >= (byte)statusFrom ) && ( T.Status1 <= (byte)statsTo )
					                                                    && ( T.VerifiedTime >= @from ) && ( T.VerifiedTime <= to )
					            select T ).ToList();
					break;

				case "PUVER":
					Indices = ( from T in Db.Entity.TripStorageIndices
					            where ( T.Status1 >= (byte)statusFrom ) && ( T.Status1 <= (byte)statsTo )
					                                                    && ( T.PickupedTime >= @from ) && ( T.PickupedTime <= to )
					                                                    && ( T.VerifiedTime >= @from ) && ( T.VerifiedTime <= to )
					            select T ).ToList();
					break;
				}

				foreach( var StorageIndex in Indices )
				{
					var Trips = TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId );

					lock( Result )
						Result.AddRange( Trips );
				}
			}
		}

		Task.WaitAll( Task.Run( SearchDb ),
		              Task.Run( SearchStorage ) );
		return Result;
	}

	public List<Trip> SearchTripsByLastUpdatedAndStatus( DateTimeOffset fromDate, DateTimeOffset toDate, STATUS fromStatus, STATUS toStatus )
	{
		var Result = new List<Trip>();

		void SearchDb()
		{
			var FromStat = fromStatus;
			var ToStat   = toStatus;

			if( FromStat is STATUS.LIMBO or < STATUS.FINALISED )
			{
				if( ToStat is >= STATUS.FINALISED and not STATUS.LIMBO )
					ToStat = STATUS.FINALISED - 1;

				using var Db = new CarrierDb( Context );

				var Recs = ( from T in Db.Entity.Trips
				             where ( T.LastModified >= fromDate ) && ( T.LastModified <= toDate ) && ( T.Status1 >= (int)FromStat ) && ( T.Status1 <= (int)ToStat )
				             select T ).ToList();

				lock( Result )
				{
					Result.AddRange( from T in Recs
					                 select T.ToTrip() );
				}
			}
		}

		void SearchStorage()
		{
			var FromStat = fromStatus;

			if( FromStat < STATUS.FINALISED )
				FromStat = STATUS.FINALISED;

			var ToStat = toStatus;

			if( ToStat == STATUS.LIMBO )
				ToStat = STATUS.DELETED;

			using var Db = new CarrierDb( Context );

			var Indices = ( from T in Db.Entity.TripStorageIndices
			                where ( T.Lastupdated >= fromDate ) && ( T.Lastupdated <= toDate )
			                                                    && ( T.Status1 >= (byte)FromStat ) && ( T.Status1 <= (byte)ToStat )
			                select T ).ToList();

			foreach( var StorageIndex in Indices )
			{
				var Trips = TripsByDate.GetTrips( Context, StorageIndex.CallTime, StorageIndex.TripId );

				lock( Result )
					Result.AddRange( Trips );
			}
		}

		Task.WaitAll( Task.Run( SearchDb ),
		              Task.Run( SearchStorage ) );
		return Result;
	}
}