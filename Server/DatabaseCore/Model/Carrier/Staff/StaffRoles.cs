﻿using Role = Protocol.Data.Role;

namespace Database.Model.Databases.Carrier;

using STAFF_ROLE_UPDATE = (IList<StaffRole> Before, IList<StaffRole> After, bool Ok);

public static class StaffRoleExtensions
{
	public static Role ToProtocolDataRole( this MasterTemplate.Role r ) => new()
	                                                                       {
		                                                                       Name            = r.RoleName,
		                                                                       Mandatory       = r.Mandatory,
		                                                                       IsAdministrator = r.IsAdministrator,
		                                                                       IsDriver        = r.IsDriver,
		                                                                       JsonObject      = r.JsonObject
	                                                                       };
}

public partial class CarrierDb
{
	public IDictionary<string, MasterTemplate.Role> GetRolesAsDictionary()
	{
		try
		{
			return ( from R in ( from R in Entity.Roles
			                     select R ).AsEnumerable()
			         group R by R.Id
			         into G
			         let Role = G.FirstOrDefault()
			         where Role != null
			         select Role ).ToDictionary( role => role.RoleName, role => role );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return new Dictionary<string, MasterTemplate.Role>();
	}

	public List<MasterTemplate.Role> GetStaffMemberDbRoles( string staffId, STAFF_TYPE type )
	{
		try
		{
			var E = Entity;

			staffId = staffId.Trim();

			return ( from S in E.Staff
			         where ( S.Type == (int)type ) && ( S.StaffId == staffId )
			         join Sr in E.StaffRoles on S.StaffId equals Sr.StaffId
			         join R in E.Roles on Sr.RoleId equals R.Id
			         select R ).ToList();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return [];
	}

	public Roles GetStaffMemberRoles( string staffId, STAFF_TYPE type )
	{
		var Result = new Roles();

		try
		{
			Result.AddRange( from R in GetStaffMemberDbRoles( staffId, type )
			                 select R.ToProtocolDataRole() );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public STAFF_ROLE_UPDATE AddUpdateStaffRoles( string staffId, List<Role> roles )
	{
		var E          = Entity;
		var StaffRoles = Entity.StaffRoles;

		while( true )
		{
			try
			{
				var Before = ( from R in StaffRoles
				               where R.StaffId == staffId
				               select R ).ToList();

				StaffRoles.RemoveRange( Before );
				E.SaveChanges();

				var DictRoles = GetRolesAsDictionary();

				var ValidRoles = new List<MasterTemplate.Role>();

				foreach( var Role in roles )
				{
					if( DictRoles.TryGetValue( Role.Name, out var DbRole ) )
						ValidRoles.Add( DbRole );
				}

				foreach( var ValidRole in ValidRoles )
					StaffRoles.Add( new StaffRole { StaffId = staffId, RoleId = ValidRole.Id } );

				E.SaveChanges();

				var After = ( from R in StaffRoles
				              where R.StaffId == staffId
				              select R ).ToList();

				return ( Before, After, Ok: true );
			}
			catch( Exception Ex ) when( Ex is DbUpdateConcurrencyException or DbUpdateException )
			{
				RandomDelay();
				continue;
			}
			catch( Exception Ex )
			{
				Context.SystemLogException( Ex );
			}
			break;
		}

		return ( Before: [], After: [], Ok: false );
	}

	public bool IsStaffMemberADriverOrAdmin( string staffId )
	{
		foreach( var Role in GetStaffMemberDbRoles( staffId, STAFF_TYPE.CARRIER ) )
		{
			if( Role.IsDriver || Role.IsAdministrator )
				return true;
		}
		return false;
	}
}