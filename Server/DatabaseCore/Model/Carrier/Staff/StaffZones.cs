﻿using Zone = Protocol.Data.Zone;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddUpdateStaffZones( string staffId, List<Zone> zones, string programName )
	{
		AddZones( zones, programName );

		var E = Entity;

		try
		{
			var StaffZones = E.StaffZones;

			StaffZones.RemoveRange( from Zne in StaffZones
			                        where Zne.StaffId == staffId
			                        select Zne );

			foreach( var Zone in zones )
				StaffZones.Add( new StaffZone { StaffId = staffId, ZoneName = Zone.Name } );

			E.SaveChanges();
		}
		catch( Exception Ex )
		{
			Context.SystemLogException( nameof( AddUpdateStaffRoles ), Ex );
		}
	}
}