﻿using Address = Protocol.Data.Address;
using Staff = Database.Model.Databases.MasterTemplate.Staff;
using Zone = Database.Model.Databases.MasterTemplate.Zone;

// ReSharper disable StringCompareToIsCultureSpecific

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class StaffLogger
	{
		public string StaffId { get; set; }

		public string Type              { get; set; }
		public bool   Enabled           { get; set; }
		public string Password          { get; set; }
		public string TaxNumber         { get; set; }
		public string FirstName         { get; set; }
		public string LastName          { get; set; }
		public string MiddleNames       { get; set; }
		public string CommissionFormula { get; set; }

		public string EmergencyFirstName { get; set; }
		public string EmergencyLastName  { get; set; }
		public string EmergencyPhone     { get; set; }
		public string EmergencyMobile    { get; set; }
		public string EmergencyEmail     { get; set; }

		public decimal MainCommission1 { get; set; }
		public decimal MainCommission2 { get; set; }
		public decimal MainCommission3 { get; set; }

		public decimal OverrideCommission1  { get; set; }
		public decimal OverrideCommission2  { get; set; }
		public decimal OverrideCommission3  { get; set; }
		public decimal OverrideCommission4  { get; set; }
		public decimal OverrideCommission5  { get; set; }
		public decimal OverrideCommission6  { get; set; }
		public decimal OverrideCommission7  { get; set; }
		public decimal OverrideCommission8  { get; set; }
		public decimal OverrideCommission9  { get; set; }
		public decimal OverrideCommission10 { get; set; }
		public decimal OverrideCommission11 { get; set; }
		public decimal OverrideCommission12 { get; set; }

		public string Roles { get; set; } = "";
		public string Zones { get; set; } = "";

		public AddressLogger? Before { get; set; }
		public AddressLogger? After  { get; set; }

		private readonly CarrierDb Db;

		public StaffLogger( CarrierDb db, Staff s )
		{
			Db = db;

			StaffId = s.StaffId;

			Type = (STAFF_TYPE)s.Type switch
			       {
				       STAFF_TYPE.CUSTOMER => "CUSTOMER",
				       _                   => "CARRIER"
			       };

			Enabled           = s.Enabled;
			Password          = s.Password;
			TaxNumber         = s.TaxNumber;
			FirstName         = s.FirstName;
			LastName          = s.LastName;
			MiddleNames       = s.MiddleNames;
			CommissionFormula = s.CommissionFormula;

			EmergencyFirstName = s.EmergencyFirstName;
			EmergencyLastName  = s.EmergencyLastName;
			EmergencyPhone     = s.EmergencyPhone;
			EmergencyMobile    = s.EmergencyMobile;
			EmergencyEmail     = s.EmergencyEmail;

			MainCommission1 = s.MainCommission1;
			MainCommission2 = s.MainCommission2;
			MainCommission3 = s.MainCommission3;

			OverrideCommission1  = s.OverrideCommission1;
			OverrideCommission2  = s.OverrideCommission2;
			OverrideCommission3  = s.OverrideCommission3;
			OverrideCommission4  = s.OverrideCommission4;
			OverrideCommission5  = s.OverrideCommission5;
			OverrideCommission6  = s.OverrideCommission6;
			OverrideCommission7  = s.OverrideCommission7;
			OverrideCommission8  = s.OverrideCommission8;
			OverrideCommission9  = s.OverrideCommission9;
			OverrideCommission10 = s.OverrideCommission10;
			OverrideCommission11 = s.OverrideCommission11;
			OverrideCommission12 = s.OverrideCommission12;

			var Addr = new AddressLogger( new MasterTemplate.Address() );
			Before = Addr;
			After  = Addr;
		}

		public StaffLogger( CarrierDb db, Staff s, AddressLogger? before ) : this( db, s )
		{
			Before = before;
		}

		public StaffLogger( CarrierDb db, Staff s, AddressLogger? before, AddressLogger? after ) : this( db, s, before )
		{
			Before = before;
			After  = after;
		}

		public StaffLogger( CarrierDb db, Staff staff, IEnumerable<StaffRole> roles ) : this( db, staff )
		{
			UpdateRoles( roles );
		}

		public StaffLogger( CarrierDb db, Staff staff, IEnumerable<StaffRole> roles, IEnumerable<Zone> zones ) : this( db, staff, roles )
		{
			UpdateZones( zones );
		}

		public void UpdateRoles( IEnumerable<StaffRole> roles )
		{
			var Rls = new StringBuilder();

			var First = true;

			var DictRoles = Db.GetRolesAsDictionaryById();

			foreach( var Role in roles )
			{
				if( First )
					First = false;
				else
					Rls.Append( ", " );

				Rls.Append( DictRoles.TryGetValue( Role.RoleId, out var Rle ) ? Rle.RoleName : "????" );
			}

			Roles = Rls.ToString();
		}

		public void UpdateZones( IEnumerable<Zone> zones )
		{
			var Zns = new StringBuilder();

			var First = true;

			foreach( var Zone in zones )
			{
				if( First )
					First = false;
				else
					Zns.Append( ", " );

				Zns.Append( Zone.ZoneName );
			}

			Zones = Zns.ToString();
		}
	}

	public StaffLookupSummaryList StaffLookup( StaffLookup lookup )
	{
		var RetVal = new StaffLookupSummaryList();

		try
		{
			var Key   = lookup.Key;
			var Count = lookup.PreFetchCount;

			IEnumerable<Staff> Result;

			var LookupType = (int)lookup.Type;

			switch( lookup.Fetch )
			{
			case PreFetch.PREFETCH.FIRST:
				Result = ( from S in Entity.Staff
				           where S.Type == LookupType
				           orderby S.StaffId
				           select S ).Take( Count );
				break;

			case PreFetch.PREFETCH.LAST:
				Result = ( from S in Entity.Staff
				           where S.Type == LookupType
				           orderby S.StaffId descending
				           select S ).Take( Count );
				break;

			case PreFetch.PREFETCH.FIND_MATCH:
				Result = ( from S in Entity.Staff
				           where ( S.Type == LookupType ) && S.StaffId.StartsWith( Key )
				           orderby S.StaffId
				           select S ).Take( Count );
				break;

			case PreFetch.PREFETCH.FIND_RANGE:
				if( Count >= 0 ) // Next
				{
					Result = ( from S in Entity.Staff
					           where ( S.Type == LookupType ) && ( S.StaffId.CompareTo( Key ) >= 0 )
					           orderby S.StaffId
					           select S ).Take( Count );
				}
				else // Prior
				{
					Result = ( from S in Entity.Staff
					           where ( S.Type == LookupType ) && ( S.StaffId.CompareTo( Key ) <= 0 )
					           orderby S.StaffId
					           select S ).Take( -Count );
				}
				break;

			case PreFetch.PREFETCH.OFFSET:
				Result = ( from S in Entity.Staff
				           where S.Type == LookupType
				           orderby S.StaffId
				           select S ).Skip( lookup.Offset ).Take( -Count );
				break;

			default:
				Context.SystemLogException( new Exception( $"Unknown prefetch type: {lookup.Fetch}" ) );

				return RetVal;
			}

			RetVal.AddRange( from S in Result
			                 let Addr = GetAddress( S.AddressId )
			                 let A = Addr.Ok ? Addr.Address : new MasterTemplate.Address()
			                 select new StaffLookupSummary
			                        {
				                        AddressLine1 = A.AddressLine1 ?? "",
				                        StaffId      = S.StaffId,
				                        FirstName    = S.FirstName,
				                        LastName     = S.LastName
			                        } );
		}
		catch( Exception E )
		{
			Context.SystemLogException( nameof( CustomersLookup ), E );
		}

		return RetVal;
	}

	public List<Staff> GetStaff( STAFF_TYPE type ) => ( from S in Entity.Staff
	                                                    where S.Type == (int)type
	                                                    orderby S.StaffId
	                                                    select S ).ToList();

	public StaffList GetStaffAsProtocolStaff( STAFF_TYPE type )
	{
		var Result = new StaffList();
		Result.AddRange( GetStaff( type ).Select( Convert ) );

		return Result;
	}

	public Dictionary<string, List<int>> GetStaffRolesAsDictionary( STAFF_TYPE type )
	{
		var SRoles = ( from S in Entity.StaffRoles
		               where S.Type == (int)type
		               select S ).AsEnumerable();

		return ( from S in SRoles
		         group S.RoleId by S.StaffId
		         into StaffRoles
		         select StaffRoles ).ToDictionary( s => s.Key, s => s.ToList() );
	}


	public StaffAndRolesList GetStaffAndRoles( STAFF_TYPE type )
	{
		var Result = new StaffAndRolesList();

		try
		{
			// Do in memory. Too slow using database
			var StaffList = GetStaff( type );
			var SRoles    = GetStaffRolesAsDictionary( type );
			var Roles     = GetRolesAsDictionaryById();

			Dictionary<string, Roles> StaffRoles = [];

			foreach( var S in StaffList )
			{
				var Rls = new Roles();

				if( SRoles.TryGetValue( S.StaffId, out var RoleIds ) )
				{
					foreach( var RoleId in RoleIds )
					{
						if( Roles.TryGetValue( RoleId, out var Role ) )
							Rls.Add( Role.ToProtocolDataRole() );
					}
				}
				StaffRoles.TryAdd( S.StaffId, Rls ); // Seem to be able to have duplicates
			}

			var AddressIds = ( from A in StaffList
			                   select A.AddressId ).ToList();

			var Addresses = ( from A in Entity.Addresses
			                  where AddressIds.Contains( A.Id )
			                  select A ).ToDictionary( a => a.Id, a => a );

			foreach( var Staff in StaffList )
			{
				var PStaff = Convert( Staff );
				PStaff.Address = GetProtocolDataAddress( Addresses[ Staff.AddressId ] );

				Result.Add( new StaffAndRoles( PStaff, StaffRoles[ Staff.StaffId ] ) );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	public StaffLookupSummaryList GetAdministrators()
	{
		var Result = new StaffLookupSummaryList();

		try
		{
			var E = Entity;

			var Admins = ( from R in E.Roles
			               where R.IsAdministrator
			               join Sr in E.StaffRoles on R.Id equals Sr.RoleId
			               join S in E.Staff on Sr.StaffId equals S.StaffId
			               where ( S.Type == (int)STAFF_TYPE.CARRIER ) && S.Enabled
			               orderby S.StaffId
			               join Address in E.Addresses on S.AddressId equals Address.Id
			               select new { Driver = S, Address = Address.AddressLine1 } ).Distinct().ToList();

			Result.AddRange( from D in Admins
			                 select new StaffLookupSummary
			                        {
				                        FirstName = D.Driver.FirstName,
				                        LastName  = D.Driver.LastName,
				                        StaffId   = D.Driver.StaffId,

				                        AddressLine1 = D.Address
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}

	public (bool UserNameOk, bool PasswordOk, int UserId, bool Ok) LoginOk( string staffId, string password, STAFF_TYPE type )
	{
		var Result = ( UserNameOk: false, PasswordOk: false, UserId: -1, Ok: false );

		staffId = staffId.ToLower();

		var Rec = GetStaff( staffId, type );

		if( Rec is { Enabled: true } )
		{
			Result.UserId     = Rec.Id;
			Result.UserNameOk = true;

			if( Rec.Password == password )
			{
				Result.PasswordOk = true;
				Result.Ok         = true;
			}
		}

		return Result;
	}

	public void DeleteStaffMember( string programName, string staffId, STAFF_TYPE type )
	{
		staffId = staffId.TrimToLower();

		try
		{
			var Rec = GetStaff( staffId, type );

			if( Rec is not null )
			{
				Entity.Staff.Remove( Rec );
				Entity.SaveChanges();

				if( type == STAFF_TYPE.CARRIER )
				{
					var Before = new StaffLogger( this, Rec );
					Context.LogDeleted( Context.GetStorageId( LOG.STAFF ), programName, "Remove staff member " + staffId, Before );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void AddUpdateStaffMember( UpdateStaffMemberWithRolesAndZones staffMember, bool encryptedPassword = true )
	{
		try
		{
			var Prog = staffMember.ProgramName;
			var S    = staffMember.Staff;
			var A    = S.Address;

			var UpdateLog = S.Type == STAFF_TYPE.CARRIER;

			if( encryptedPassword )
			{
				var (Value, Ok) = Encryption.FromTimeLimitedToken( S.Password );

				if( !Ok )
					throw new Exception( "Invalid password" );

				S.Password = Value;
			}

			var Rec = GetStaff( S.StaffId, S.Type );

			if( Rec is null )
			{
				var (AddressId, Address, AddressOk) = AddAddress( A, staffMember.ProgramName );

				if( AddressOk )
				{
					var Staff = new Staff
					            {
						            AddressId = AddressId,
						            StaffId   = S.StaffId,
						            Enabled   = S.Enabled,

						            FirstName   = S.FirstName.Max50(),
						            LastName    = S.LastName.Max50(),
						            MiddleNames = S.MiddleNames,
						            Password    = S.Password,

						            EmergencyFirstName = S.EmergencyFirstName.Max50(),
						            EmergencyLastName  = S.EmergencyLastName.Max50(),
						            EmergencyMobile    = S.EmergencyMobile,
						            EmergencyPhone     = S.EmergencyPhone,
						            EmergencyEmail     = S.EmergencyEmail,

						            TaxNumber         = S.TaxNumber,
						            CommissionFormula = "",

						            MainCommission1 = S.MainCommission1,
						            MainCommission2 = S.MainCommission2,
						            MainCommission3 = S.MainCommission3,

						            OverrideCommission1  = S.OverrideCommission1,
						            OverrideCommission2  = S.OverrideCommission2,
						            OverrideCommission3  = S.OverrideCommission3,
						            OverrideCommission4  = S.OverrideCommission4,
						            OverrideCommission5  = S.OverrideCommission5,
						            OverrideCommission6  = S.OverrideCommission6,
						            OverrideCommission7  = S.OverrideCommission7,
						            OverrideCommission8  = S.OverrideCommission8,
						            OverrideCommission9  = S.OverrideCommission9,
						            OverrideCommission10 = S.OverrideCommission10,
						            OverrideCommission11 = S.OverrideCommission11,
						            OverrideCommission12 = S.OverrideCommission12
					            };
					Entity.Staff.Add( Staff );
					Entity.SaveChanges();

					if( UpdateLog )
						Context.LogNew( Context.GetStorageId( LOG.STAFF ), Prog, "Add staff member", new StaffLogger( this, Staff, Address ) );

					AddUpdateStaffRoles( S.StaffId, staffMember.Roles );
				}
			}
			else
			{
				var (AddressBefore, AddressAfter, AddressId, AddressOk) = UpdateAddress( Rec.AddressId, A, Prog );

				if( AddressOk )
				{
					var Before = UpdateLog ? new StaffLogger( this, Rec ) : null;

					Rec.AddressId            = AddressId;
					Rec.Enabled              = S.Enabled;
					Rec.FirstName            = S.FirstName.Max50();
					Rec.LastName             = S.LastName.Max50();
					Rec.MiddleNames          = S.MiddleNames;
					Rec.Password             = S.Password;
					Rec.EmergencyFirstName   = S.EmergencyFirstName.Max50();
					Rec.EmergencyLastName    = S.EmergencyLastName.Max50();
					Rec.EmergencyMobile      = S.EmergencyMobile;
					Rec.EmergencyPhone       = S.EmergencyPhone;
					Rec.EmergencyEmail       = S.EmergencyEmail;
					Rec.TaxNumber            = S.TaxNumber;
					Rec.CommissionFormula    = "";
					Rec.MainCommission1      = S.MainCommission1;
					Rec.MainCommission2      = S.MainCommission2;
					Rec.MainCommission3      = S.MainCommission3;
					Rec.OverrideCommission1  = S.OverrideCommission1;
					Rec.OverrideCommission2  = S.OverrideCommission2;
					Rec.OverrideCommission3  = S.OverrideCommission3;
					Rec.OverrideCommission4  = S.OverrideCommission4;
					Rec.OverrideCommission5  = S.OverrideCommission5;
					Rec.OverrideCommission6  = S.OverrideCommission6;
					Rec.OverrideCommission7  = S.OverrideCommission7;
					Rec.OverrideCommission8  = S.OverrideCommission8;
					Rec.OverrideCommission9  = S.OverrideCommission9;
					Rec.OverrideCommission10 = S.OverrideCommission10;
					Rec.OverrideCommission11 = S.OverrideCommission11;
					Rec.OverrideCommission12 = S.OverrideCommission12;

					Entity.SaveChanges();

					var After = UpdateLog ? new StaffLogger( this, Rec, AddressBefore, AddressAfter ) : null;

					var (BFore, Afr, IsOk) = AddUpdateStaffRoles( S.StaffId, staffMember.Roles );

					if( IsOk && Before is not null && After is not null )
					{
						Before.UpdateRoles( BFore );
						After.UpdateRoles( Afr );

						Context.LogDifferences( Context.GetStorageId( LOG.STAFF ), Prog, "Update staff member", Before, After );
					}
				}
			}
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
	}

	public void EnableDisableStaffMember( string program, string staffId, STAFF_TYPE type, bool enable )
	{
		try
		{
			var Rec = GetStaff( staffId, type );

			if( Rec is not null )
			{
				var Before = type == STAFF_TYPE.CARRIER ? new StaffLogger( this, Rec ) : null;

				Rec.Enabled = enable;

				Entity.SaveChanges();

				if( Before is not null )
				{
					var After = new StaffLogger( this, Rec );
					Context.LogDifferences( Context.GetStorageId( LOG.STAFF ), program, "Enable staff member", Before, After );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public bool StaffMemberExists( string staffId, STAFF_TYPE type ) => GetStaff( staffId, type ) is not null;

	public int GetStaffMemberInternalId( string staffId, STAFF_TYPE type )
	{
		var Rec = GetStaff( staffId, type );
		return Rec?.Id ?? 0;
	}

	public Protocol.Data.Staff GetStaffMember( string staffId, STAFF_TYPE type )
	{
		Protocol.Data.Staff Staff;

		var Rec = GetStaff( staffId, type );

		if( Rec is not null )
		{
			Staff         = Convert( Rec );
			Staff.Address = GetProtocolDataAddress( Rec.AddressId );
		}
		else
			Staff = new Protocol.Data.Staff();

		return Staff;
	}

	private static Address GetProtocolDataAddress( MasterTemplate.Address a ) => new()
	                                                                             {
		                                                                             AddressLine1  = a.AddressLine1,
		                                                                             AddressLine2  = a.AddressLine2,
		                                                                             Barcode       = a.Barcode,
		                                                                             City          = a.City,
		                                                                             Country       = a.Country,
		                                                                             CountryCode   = a.CountryCode,
		                                                                             EmailAddress  = a.EmailAddress,
		                                                                             EmailAddress1 = a.EmailAddress1,
		                                                                             EmailAddress2 = a.EmailAddress2,
		                                                                             Fax           = a.Fax,
		                                                                             Latitude      = a.Latitude,
		                                                                             Longitude     = a.Longitude,
		                                                                             Mobile        = a.Mobile,
		                                                                             Mobile1       = a.Mobile1,
		                                                                             Notes         = a.Notes,
		                                                                             Phone         = a.Phone,
		                                                                             Phone1        = a.Phone1,
		                                                                             PostalBarcode = a.PostalBarcode,
		                                                                             PostalCode    = a.PostalCode.TrimMax20(),
		                                                                             Region        = a.Region,
		                                                                             SecondaryId   = a.SecondaryId,
		                                                                             Suite         = a.Suite,
		                                                                             Vicinity      = a.Vicinity
	                                                                             };

	private Address GetProtocolDataAddress( long id )
	{
		try
		{
			var (Addr, Ok) = GetAddress( id );

			if( Ok && Addr is not null )
				return GetProtocolDataAddress( Addr );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return new Address();
	}

	private static Protocol.Data.Staff Convert( Staff s ) => new()
	                                                         {
		                                                         Ok                   = true,
		                                                         StaffId              = s.StaffId,
		                                                         MainCommission1      = s.MainCommission1,
		                                                         FirstName            = s.FirstName,
		                                                         LastName             = s.LastName,
		                                                         EmergencyLastName    = s.EmergencyLastName,
		                                                         EmergencyEmail       = s.EmergencyEmail,
		                                                         EmergencyMobile      = s.EmergencyMobile,
		                                                         EmergencyPhone       = s.EmergencyPhone,
		                                                         OverrideCommission1  = s.OverrideCommission1,
		                                                         EmergencyFirstName   = s.EmergencyFirstName,
		                                                         MainCommission2      = s.MainCommission2,
		                                                         Enabled              = s.Enabled,
		                                                         OverrideCommission2  = s.OverrideCommission2,
		                                                         OverrideCommission3  = s.OverrideCommission3,
		                                                         TaxNumber            = s.TaxNumber,
		                                                         MiddleNames          = s.MiddleNames,
		                                                         Password             = Encryption.ToTimeLimitedToken( s.Password ),
		                                                         MainCommission3      = s.MainCommission3,
		                                                         OverrideCommission10 = s.OverrideCommission10,
		                                                         OverrideCommission11 = s.OverrideCommission11,
		                                                         OverrideCommission12 = s.OverrideCommission12,
		                                                         OverrideCommission4  = s.OverrideCommission4,
		                                                         OverrideCommission5  = s.OverrideCommission5,
		                                                         OverrideCommission6  = s.OverrideCommission6,
		                                                         OverrideCommission7  = s.OverrideCommission7,
		                                                         OverrideCommission8  = s.OverrideCommission8,
		                                                         OverrideCommission9  = s.OverrideCommission9
	                                                         };

	private Staff? GetStaff( string staffId, STAFF_TYPE type ) => ( from S in Entity.Staff
	                                                                where ( S.Type == (int)type ) && ( S.StaffId == staffId )
	                                                                select S ).FirstOrDefault();
}