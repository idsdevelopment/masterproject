﻿using Role = Protocol.Data.Role;
using Staff = Protocol.Data.Staff;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void AddDriver( string program, string driverCode, string password = "" )
	{
		if( password.IsNullOrWhiteSpace() )
			password = driverCode;

		var E     = Entity;
		var Staff = E.Staff;

		void UpdateDriver()
		{
			var DriverRole = GetDriverRole();

			var StaffUpdate = new UpdateStaffMemberWithRolesAndZones( program, new Staff
			                                                                   {
				                                                                   StaffId  = driverCode,
				                                                                   Password = password
			                                                                   } );

			StaffUpdate.Roles.Add( new Role
			                       {
				                       Name            = DriverRole.RoleName,
				                       Mandatory       = DriverRole.Mandatory,
				                       IsDriver        = true,
				                       IsAdministrator = DriverRole.IsAdministrator
			                       } );

			AddUpdateStaffMember( StaffUpdate, false );
		}

		var Driver = ( from S in Staff
		               where S.Type == (int)STAFF_TYPE.CARRIER && S.StaffId == driverCode
		               select S ).FirstOrDefault();

		if( Driver is null )
			UpdateDriver();

		else if( !( from R in GetStaffMemberRoles( driverCode, STAFF_TYPE.CARRIER )
		            where R.IsDriver
		            select true ).FirstOrDefault() )
		{
			password = Driver.Password;
			UpdateDriver();
		}
	}


	public StaffLookupSummaryList GetDrivers()
	{
		var Result = new StaffLookupSummaryList();

		try
		{
			var E = Entity;

			var Drivers = ( from R in E.Roles
			                where R.IsDriver
			                join Sr in E.StaffRoles on R.Id equals Sr.RoleId
			                join S in E.Staff on Sr.StaffId equals S.StaffId
			                where S.Type == (int)STAFF_TYPE.CARRIER && S.Enabled
			                orderby S.StaffId
			                join Address in E.Addresses on S.AddressId equals Address.Id
			                select new { Driver = S, Address = Address.AddressLine1 } ).Distinct().ToList();

			Result.AddRange( from D in Drivers
			                 select new StaffLookupSummary
			                        {
				                        FirstName = D.Driver.FirstName,
				                        LastName  = D.Driver.LastName,
				                        StaffId   = D.Driver.StaffId,

				                        AddressLine1 = D.Address
			                        } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}


	public StaffLookupSummary? GetDriver( string driverCode ) => ( from D in GetDrivers()
	                                                               where D.StaffId == driverCode
	                                                               select D ).FirstOrDefault();


	public void CreateDriver( string program, string driverCode )
	{
		var DbContext = Context.Clone();

		Tasks.RunVoid( () =>
		               {
			               var Driver = driverCode.Trim();

			               using var Db = new CarrierDb( DbContext );

			               if( Db.GetDriver( Driver ) is null )
				               Db.AddDriver( program, Driver, Driver );
		               } );
	}

	public bool IsDriver( string driverCode )
	{
		driverCode = driverCode.Trim();

		var E = Entity;

		return ( from S in E.Staff
		         where S.Type == (int)STAFF_TYPE.CARRIER && S.Enabled && ( S.StaffId == driverCode )
		         join Sr in E.StaffRoles on S.StaffId equals Sr.StaffId
		         join R in E.Roles on Sr.RoleId equals R.Id
		         where R.IsDriver
		         select true ).FirstOrDefault();
	}
}