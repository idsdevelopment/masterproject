﻿using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public class PmlSalesForce : XRef
	{
		public PmlSalesForce( string tripId, string sellerId, string storefront, string customerSellerId, List<Protocol.Data.TripItem> items ) : this()
		{
			Id1         = tripId;
			StringData1 = sellerId;
			StringData2 = storefront;
			StringData3 = customerSellerId;
			StringData4 = JsonConvert.SerializeObject( items );
		}

		private PmlSalesForce() : base( TYPE.PML_SALESFORCE )
		{
		}

		public void Deconstruct( out string tripId, out string sellerId, out string storefront, out string customerSellerId, out List<Protocol.Data.TripItem> items )
		{
			tripId           = Id1;
			sellerId         = StringData1;
			storefront       = StringData2;
			customerSellerId = StringData3;
			items            = JsonConvert.DeserializeObject<List<Protocol.Data.TripItem>>( StringData4 ) ?? [];
		}

		public static void Delete( IRequestContext context, string tripId )
		{
			using var Db     = new CarrierDb( context );
			var       Entity = Db.Entity;

			var Xref = ( from X in Entity.XRefs
			             where X.Type == (short)TYPE.PML_SALESFORCE
			             where X.Id1 == tripId
			             select X ).FirstOrDefault();

			if( Xref is not null )
			{
				Entity.XRefs.Remove( Xref );
				Entity.SaveChanges();
			}
		}

		public static void Delete( IRequestContext context, Trip trip )
		{
			Delete( context, trip.TripId );
		}

		public static void Delete( IRequestContext context, TripUpdate trip )
		{
			Delete( context, trip.TripId );
		}


		public static PmlSalesForce? Get( IRequestContext context, string tripId )
		{
			XRef? Xref;

			using( var Db = new CarrierDb( context ) )
			{
				Xref = ( from X in Db.Entity.XRefs
				         where X.Type == (short)TYPE.PML_SALESFORCE
				         where X.Id1 == tripId
				         select X ).FirstOrDefault();
			}

			return Xref is not null ? new PmlSalesForce
			                          {
				                          Id1         = Xref.Id1,
				                          StringData1 = Xref.StringData1,
				                          StringData2 = Xref.StringData2,
				                          StringData3 = Xref.StringData3,
				                          StringData4 = Xref.StringData4
			                          } : null;
		}

		public void Save( IRequestContext? context )
		{
			if( context is not null )
			{
				using var Db     = new CarrierDb( context );
				var       Entity = Db.Entity;

				var Xref = ( from X in Entity.XRefs
				             where X.Type == (short)TYPE.PML_SALESFORCE
				             where X.Id1 == Id1
				             select X ).FirstOrDefault();

				if( Xref is null )
				{
					Xref = new XRef
					       {
						       Type = (short)TYPE.PML_SALESFORCE
					       };

					Entity.XRefs.Add( Xref );
				}

				Xref.Id1         = Id1;
				Xref.StringData1 = StringData1;
				Xref.StringData2 = StringData2;
				Xref.StringData3 = StringData3;
				Xref.StringData4 = StringData4;

				Entity.SaveChanges();
			}
		}
	}
}