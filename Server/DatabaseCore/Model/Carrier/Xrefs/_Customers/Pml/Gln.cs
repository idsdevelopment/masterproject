﻿namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public string GetPmlGln( string pickupLocation )
	{
		var Xref = GetSecondaryAccountIdFromBarcodeXref( pickupLocation );
		return Xref is not null ? Xref.Id1 : "UNKNOWN";
	}
}