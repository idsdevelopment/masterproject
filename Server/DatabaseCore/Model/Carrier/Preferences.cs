﻿using StorageV2.Logs;
using Preference = Protocol.Data.Preference;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	//Droid
	public const string EDIT_PIECES_WEIGHT = "EDIT_PIECES_WEIGHT",
	                    EDIT_REFERENCE     = "EDIT_REFERENCE",
	                    //
	                    BY_PIECES   = "BY_PIECES",
	                    BY_SHIPMENT = "BY_SHIPMENT",
	                    //
	                    BY_LOCATION                          = "BY_LOCATION",
	                    ALLOW_MANUAL_BARCODE                 = "ALLOW_MANUAL_BARCODE",
	                    ALLOW_UNDELIVERABLE                  = "ALLOW_UNDELIVERABLE",
	                    UNDELIVERABLE_BACK_TO_DISPATCH       = "UNDELIVERABLE_BACK_TO_DISPATCH",
	                    UNDELIVERABLE_DELETE_NO_PICKUP       = "UNDELIVERABLE_DELETE_NO_PICKUP",
	                    ALLOW_OVERSCANS                      = "ALLOW_OVERSCANS",
	                    ALLOW_UNDERSCANS                     = "ALLOW_UNDERSCANS",
	                    UNDELIVERABLE_PIECES_CREATE_NEW_TRIP = "UNDELIVERABLE_PIECES_CREATE_NEW_TRIP",
	                    //	                    
	                    REQUIRE_POP           = "REQUIRE_POP",
	                    REQUIRE_POP_SIGNATURE = "REQUIRE_POP_SIGNATURE",
	                    REQUIRE_POP_PICTURE   = "REQUIRE_POP_PICTURE",
	                    REQUIRE_POD           = "REQUIRE_POD",
	                    REQUIRE_POD_SIGNATURE = "REQUIRE_POD_SIGNATURE",
	                    REQUIRE_POD_PICTURE   = "REQUIRE_POD_PICTURE",

	                    //	                    
	                    POP_POD_MUST_BE_ALPHA           = "POP_POD_MUST_BE_ALPHA",
	                    POP_POD_MIN_LENGTH              = "POP_POD_MIN_LENGTH",
	                    AUTO_OPEN_SHIPMENT_TAB          = "AUTO_OPEN_SHIPMENT_TAB",
	                    BATCH_LOCATION                  = "BATCH_LOCATION",
	                    BATCH_UNDELIVERABLE_AT_LOCATION = "BATCH_UNDELIVERABLE_AT_LOCATION",
	                    DYNAMSOFT_SCANNER               = "DYNAMSOFT_SCANNER",
	                    //
	                    ENABLE_PICKUP_GEO_FENCE   = "ENABLE_PICKUP_GEO_FENCE",
	                    ENABLE_DELIVERY_GEO_FENCE = "ENABLE_DELIVERY_GEO_FENCE",
	                    DELIVERY_GEO_FENCE_RADIUS = "DELIVERY_GEO_FENCE_RADIUS",
	                    //
	                    DEVICE_DISCLAIMER = "DEVICE_DISCLAIMER",
	                    //
	                    COMPULSORY_ARRIVE_TIME = "COMPULSORY_ARRIVE_TIME",
	                    PICKUP_ARRIVE_TIME     = "PICKUP_ARRIVE_TIME",
	                    DELIVERY_ARRIVE_TIME   = "DELIVERY_ARRIVE_TIME",
	                    PICKUP_WAIT_TIME       = "PICKUP_WAIT_TIME",
	                    DELIVERY_WAIT_TIME     = "DELIVERY_WAIT_TIME",
	                    EXTRA_SOUNDS           = "EXTRA_SOUNDS",
	                    CONFIRM_REMOVAL        = "CONFIRM_REMOVAL",
	                    ENHANCED_VISUALS       = "ENHANCED_VISUALS",

	                    // IMPORT / EXPORT
	                    IMPORT = "IMPORT",
	                    EXPORT = "EXPORT",

	                    // Ids1
	                    IMPORT_USER_NAME      = "IMPORT_USER_NAME",
	                    IMPORT_PASSWORD       = "IMPORT_PASSWORD",
	                    USE_JBTEST            = "USE_JBTEST",
	                    SEND_TO_IDS1          = "SEND_TO_IDS1",
	                    SEND_ACCOUNTS_TO_IDS1 = "SEND_ACCOUNTS_TO_IDS1",

	                    // Customers
	                    PML      = "PML",
	                    PML_TEST = "PMLTEST",
	                    PRIORITY = "PRIORITY",
	                    CORAL    = "CORAL",

	                    // Modules
	                    INVENTORY = "INVENTORY",

	                    //Email
	                    SMTP_SERVER        = "SMTP_SERVER",
	                    SMTP_USE_SSL       = "SMTP_USE_SSL",
	                    SMTP_USER_NAME     = "SMTP_USER_NAME",
	                    SMTP_PASSWORD      = "SMTP_PASSWORD",
	                    SMTP_PORT          = "SMTP_PORT",
	                    SMTP_FROM_ADDRESS  = "SMTP_FROM_ADDRESS",
	                    SMTP_ERROR_ADDRESS = "SMTP_ERROR_ADDRESS",
	                    EMAIL_DISPATCHED   = "EMAIL_DISPATCHED",

	                    // Mailgun
	                    USE_MAILGUN       = "USE_MAILGUN",
	                    MAILGUN_SERVER    = "smtp.mailgun.org",
	                    MAILGUN_USER_NAME = "postmaster@mg.idsroutes.com",
	                    MAILGUN_PASSWORD  = "928f28dee6725d55ac8d08e81dcc3001-86220e6a-433cca77",
	                    MAILGUN_PORT      = "587",

	                    // Invoicing
	                    NEXT_INVOICE_NUMBER = "NEXT_INVOICE_NUMBER",

	                    // System
	                    DISABLE_REUSED_TRIP_ID = "DISABLE_REUSED_TRIP_ID";

	private static readonly MemoryCache<string, Preferences> PreferenceCache = new();

	public class CustomerPreference
	{
		public bool IsPml,
		            IsPriority,
		            IsCoral;
	}

	public record EmailPreference(
		string SmtpServer,
		bool SmtpUseSsl,
		string SmtpUserName,
		int SmtpPort,
		string SmtpPassword,
		string SmtpFromAddress,
		string SmtpErrorAddress,
		bool EmailDispatched );

	public bool SendToIds1Preference()
	{
		var Prefs = GetPreferences( true );

		foreach( var P in Prefs )
		{
			if( P.DevicePreference.Trim() == SEND_TO_IDS1 )
				return P.Enabled;
		}
		return false;
	}

	public bool SendAccountsToIds1Preference()
	{
		var Prefs = GetPreferences( true );

		foreach( var P in Prefs )
		{
			if( P.DevicePreference.Trim() == SEND_ACCOUNTS_TO_IDS1 )
				return P.Enabled;
		}
		return false;
	}

	public EmailPreference GetEmailPreferences()
	{
		var SmtpServer       = "";
		var SmtpUserName     = "";
		var SmtpPort         = 0;
		var SmtpPassword     = "";
		var SmtpFromAddress  = "";
		var SmtpErrorAddress = "";
		var EmailDispatched  = false;
		var SmtpUseSsl       = true;
		var UseMailgun       = false;

		var Prefs = GetPreferences( true );

		foreach( var P in Prefs )
		{
			switch( P.DevicePreference.Trim() )
			{
			case USE_MAILGUN:
				UseMailgun = P.Enabled;
				break;

			case SMTP_SERVER:
				SmtpServer = P.StringValue;
				break;

			case SMTP_USE_SSL:
				SmtpUseSsl = P.Enabled;
				break;

			case SMTP_USER_NAME:
				SmtpUserName = P.StringValue;
				break;

			case SMTP_PORT:
				SmtpPort = P.IntValue;
				break;

			case SMTP_PASSWORD:
				SmtpPassword = P.StringValue;
				break;

			case SMTP_FROM_ADDRESS:
				SmtpFromAddress = P.StringValue;
				break;

			case SMTP_ERROR_ADDRESS:
				SmtpErrorAddress = P.StringValue;
				break;

			case EMAIL_DISPATCHED:
				EmailDispatched = P.Enabled;
				break;
			}
		}

		if( UseMailgun )
		{
			SmtpServer   = MAILGUN_SERVER;
			SmtpUserName = MAILGUN_USER_NAME;
			SmtpPassword = MAILGUN_PASSWORD;
			SmtpPort     = MAILGUN_PORT.ToInt();
			SmtpUseSsl   = false;
		}

		return new EmailPreference( SmtpServer,
		                            SmtpUseSsl,
		                            SmtpUserName,
		                            SmtpPort,
		                            SmtpPassword,
		                            SmtpFromAddress,
		                            SmtpErrorAddress,
		                            EmailDispatched );
	}

	public CustomerPreference CustomerPreferences()
	{
		var Result = new CustomerPreference();
		var Prefs  = GetPreferences( true );

		foreach( var P in Prefs )
		{
			switch( P.DevicePreference.Trim() )
			{
			case PML:
				Result.IsPml = P.Enabled;
				break;

			case PRIORITY:
				Result.IsPriority = P.Enabled;
				break;

			case CORAL:
				Result.IsCoral = P.Enabled;
				break;
			}
		}
		return Result;
	}

	public MenuPreference MenuPreferences()
	{
		var Result = new MenuPreference();
		var Prefs  = GetPreferences( true );

		foreach( var P in Prefs )
		{
			switch( P.DevicePreference.Trim() )
			{
			case AUTO_OPEN_SHIPMENT_TAB:
				Result.AutoOpenMenuTab = P.Enabled;
				break;

			case INVENTORY:
				Result.HasInventory = P.Enabled;
				break;

			case PML:
				Result.IsPml = P.Enabled;
				break;

			case PRIORITY:
				Result.IsPriority = P.Enabled;
				break;
			}
		}
		return Result;
	}

	public DevicePreferences DevicePreferences()
	{
		var Result = new DevicePreferences();
		var Prefs  = GetPreferences( true );

		foreach( var P in Prefs )
		{
			switch( P.DevicePreference.Trim() )
			{
		#region Customers
			case PML:
			case PML_TEST:
				Result.IsPml = P.Enabled;
				break;

			case PRIORITY:
				Result.IsPriority = P.Enabled;
				break;
		#endregion

		#region Trip Confirmation
			case EXTRA_SOUNDS:
				Result.ExtraSounds = P.Enabled;
				break;

			case CONFIRM_REMOVAL:
				Result.ConfirmRemoval = P.Enabled;
				break;

			case ENHANCED_VISUALS:
				Result.EnhancedVisuals = P.Enabled;
				break;
		#endregion

		#region Arrive / Wait Times
			case COMPULSORY_ARRIVE_TIME:
				Result.CompulsoryArriveTime = P.Enabled;
				break;

			case PICKUP_ARRIVE_TIME:
				Result.PickupArriveTime = P.Enabled;
				break;

			case DELIVERY_ARRIVE_TIME:
				Result.DeliveryArriveTime = P.Enabled;
				break;

			case PICKUP_WAIT_TIME:
				Result.PickupWaitTime = P.Enabled;
				break;

			case DELIVERY_WAIT_TIME:
				Result.DeliveryWaitTime = P.Enabled;
				break;
		#endregion

			case BATCH_UNDELIVERABLE_AT_LOCATION:
				Result.BatchUndeliverableAtLocation = P.Enabled;
				break;

			case BATCH_LOCATION:
				Result.BatchLocation = P.Enabled;
				break;

			case POP_POD_MUST_BE_ALPHA:
				Result.PopPodMustBeAlpha = P.Enabled;
				break;

			case POP_POD_MIN_LENGTH:
				Result.PopPodMinLength = P.IntValue;
				break;

			case REQUIRE_POP:
				Result.RequirePop = P.Enabled;
				break;

			case REQUIRE_POP_SIGNATURE:
				Result.RequirePopSignature = P.Enabled;
				break;

			case REQUIRE_POP_PICTURE:
				Result.RequirePopPicture = P.Enabled;
				break;

			case REQUIRE_POD:
				Result.RequirePod = P.Enabled;
				break;

			case REQUIRE_POD_SIGNATURE:
				Result.RequirePodSignature = P.Enabled;
				break;

			case REQUIRE_POD_PICTURE:
				Result.RequirePodPicture = P.Enabled;
				break;

			case UNDELIVERABLE_PIECES_CREATE_NEW_TRIP:
				Result.CreateNewTripForPieces = P.Enabled;
				break;

			case EDIT_PIECES_WEIGHT:
				Result.EditPiecesWeight = P.Enabled;
				break;

			case EDIT_REFERENCE:
				Result.EditReference = P.Enabled;
				break;

			case BY_PIECES:
				Result.ByPiece = P.Enabled;
				break;

			case BY_LOCATION:
				Result.ByLocation = P.Enabled;
				break;

			case BY_SHIPMENT:
				Result.ScanShipments = P.Enabled;
				break;

			case ALLOW_MANUAL_BARCODE:
				Result.AllowManualBarcodeInput = P.Enabled;
				break;

			case ALLOW_UNDELIVERABLE:
				Result.AllowUndeliverable = P.Enabled;
				break;

			case UNDELIVERABLE_BACK_TO_DISPATCH:
				Result.UndeliverableBackToDispatch = P.Enabled;
				break;

			case UNDELIVERABLE_DELETE_NO_PICKUP:
				Result.DeleteCannotPickup = P.Enabled;
				break;

			case ALLOW_OVERSCANS:
				Result.AllowOverScans = P.Enabled;
				break;

			case ALLOW_UNDERSCANS:
				Result.AllowUnderScans = P.Enabled;
				break;

			case DYNAMSOFT_SCANNER:
				Result.DynamsoftBarcodeReader = P.Enabled;
				break;

		#region Geo Fence
			case ENABLE_DELIVERY_GEO_FENCE:
				Result.EnableDeliveryGeoFence = P.Enabled;
				break;

			case ENABLE_PICKUP_GEO_FENCE:
				Result.EnablePickupGeoFence = P.Enabled;
				break;

			case DELIVERY_GEO_FENCE_RADIUS:
				Result.DeliveryGeoFenceRadius = P.IntValue;
				break;
		#endregion

			case DEVICE_DISCLAIMER:
				Result.DeviceDisclaimer = P.StringValue;
				break;
			}
		}
		return Result;
	}

	public ClientPreferences ClientPreferences()
	{
		var Pref = CustomerPreferences();

		return new ClientPreferences
		       {
			       Pml   = Pref.IsPml,
			       Coral = Pref.IsCoral
		       };
	}

	public Preferences GetPreferences( bool includeIds = false )
	{
		var IsIds = Context.IsIds || includeIds;

		var Key = CacheKey( Context );

		if( IsIds )
			PreferenceCache.Remove( Key );

		else
		{
			var Pref = PreferenceCache.TryGetValue( Key );

			if( Pref.Found )
				return Pref.Value;
		}
		var MasterPreferences = Database.Users.GetPreference( Context );

		var CarrierPreferences = MasterPreferences.Select( carrierPreference => ( from Up in Entity.Preferences
		                                                                          where Up.UsersPreferenceId == carrierPreference.Id
		                                                                          select Up ).FirstOrDefault() ).Where( rec => rec is not null ).ToList();

		var Toffset = Context.TimeZoneOffset;

		var Result = ( from MasterPreference in MasterPreferences
		               where !MasterPreference.IdsOnly || ( IsIds && MasterPreference.IdsOnly )
		               where ( MasterPreference.ResellerId == "" ) || ( string.Compare( MasterPreference.ResellerId, Context.CarrierId, StringComparison.OrdinalIgnoreCase ) == 0 )
		               let CarrierRec = ( from Cp in CarrierPreferences
		                                  where Cp.UsersPreferenceId == MasterPreference.Id
		                                  select Cp ).FirstOrDefault()
		               let Enabled = CarrierRec?.Enabled ?? MasterPreference.Default
		               let StringValue = CarrierRec?.StringValue ?? MasterPreference.DefaultStringValue
		               let InvValue = CarrierRec?.IntValue ?? MasterPreference.DefaultIntValue
		               let DoubleValue = (decimal)( CarrierRec?.DoubleValue ?? MasterPreference.DefaultDoubleValue )
		               let DateTimeValue = ( CarrierRec?.DateTimeValue ?? DateTime.UtcNow ).AddHours( Toffset )
		               orderby MasterPreference.IdsOnly descending, MasterPreference.DisplayOrder descending, MasterPreference.SubDisplayOrder, MasterPreference.Description
		               select new Preference
		                      {
			                      DataType         = (byte)MasterPreference.DataType,
			                      Description      = MasterPreference.Description,
			                      Enabled          = Enabled,
			                      DevicePreference = MasterPreference.DevicePreference,
			                      IdsOnly          = MasterPreference.IdsOnly,
			                      Id               = MasterPreference.Id,
			                      DisplayOrder     = MasterPreference.DisplayOrder,
			                      DecimalValue     = DoubleValue,
			                      StringValue      = StringValue,
			                      IntValue         = InvValue,
			                      DateTimeValue    = DateTimeValue
		                      } ).ToList();

		var Reslt = new Preferences();

		foreach( var P in Result )
		{
			P.IntValue = P.DevicePreference.Trim() switch
			             {
				             NEXT_INVOICE_NUMBER => (int)Context.NextInvoiceNumber,
				             _                   => P.IntValue
			             };
		}
		Reslt.AddRange( Result );

		if( !IsIds )
			PreferenceCache.AddAbsolute( Key, Reslt, DateTimeOffset.UtcNow.AddHours( 1 ) );
		return Reslt;
	}

	public void UpdatePreference( Preference preference )
	{
		var Enabled = preference.Enabled;
		var Cid     = Context.CarrierId;
		var Sv      = preference.StringValue;

		switch( preference.DevicePreference )
		{
		case IMPORT:
			Tasks.RunVoid( () =>
			               {
				               Database.Users.ImportExport.EnableCarrierImport( Cid, Enabled );
			               } );
			break;

		case EXPORT:
			Tasks.RunVoid( () =>
			               {
				               Database.Users.ImportExport.EnableCarrierExport( Cid, Enabled );
			               } );
			break;

		case IMPORT_USER_NAME:
			Tasks.RunVoid( () =>
			               {
				               Database.Users.ImportExport.UpdateUsername( Cid, Sv );
			               } );
			break;

		case IMPORT_PASSWORD:
			Tasks.RunVoid( () =>
			               {
				               Database.Users.ImportExport.UpdatePassword( Cid, Sv );
			               } );
			break;

		case NEXT_INVOICE_NUMBER:
			{
				long InvNum  = preference.IntValue;
				var  LastInv = LastInvoiceNumber;

				if( InvNum <= LastInv )
					InvNum = LastInv + 1;

				NextInvoiceNumber = InvNum;
				break;
			}
		}

		var Rec = ( from P in Entity.Preferences
		            where P.UsersPreferenceId == preference.Id
		            select P ).FirstOrDefault();

		if( Rec is null )
		{
			Rec = new MasterTemplate.Preference { UsersPreferenceId = preference.Id };
			Entity.Preferences.Add( Rec );
		}

		Rec.Enabled       = Enabled;
		Rec.StringValue   = Sv;
		Rec.DateTimeValue = preference.DateTimeValue.AddHours( -Context.TimeZoneOffset );
		Rec.DoubleValue   = (float)preference.DecimalValue;
		Rec.IntValue      = preference.IntValue;

		Entity.SaveChanges();

		PreferenceCache.Remove( CacheKey( Context ) );
	}

	public bool IsPasswordValid( string password )
	{
		try
		{
			var (Password, Ok) = Encryption.FromTimeLimitedToken( password );

			if( !Ok )
				throw new Exception( "Invalid password" );

			var Pl = Password.Length;

			var Preferences = ( from P in GetPreferences()
			                    where P.DevicePreference.IsNotNullOrWhiteSpace()
			                    select P ).ToDictionary( preference => preference.DevicePreference.Trim(), preference => preference.IntValue );

			if( Preferences.TryGetValue( "PASSWORD_MIN_LENGTH", out var PLength ) && ( Pl >= PLength ) )
			{
				var Upper   = 0;
				var Lower   = 0;
				var Numeric = 0;
				var Symbols = 0;

				foreach( var C in Password )
				{
					switch( C )
					{
					case >= '0' and <= '9':
						Numeric++;
						break;

					case >= 'A' and <= 'Z':
						Upper++;
						break;

					case >= 'a' and <= 'z':
						Lower++;
						break;

					default:
						Symbols++;
						break;
					}
				}

				if( Preferences.TryGetValue( "PASSWORD_MIN_NUMERIC", out var PNum ) && ( Numeric >= PNum ) )
				{
					if( Preferences.TryGetValue( "PASSWORD_MIN_UPPER", out var PUpper ) && ( Upper >= PUpper ) )
					{
						if( Preferences.TryGetValue( "PASSWORD_MIN_LOWER", out var PLower ) && ( Lower >= PLower ) )
							return Preferences.TryGetValue( "PASSWORD_MIN_SYMBOL", out var PSymbol ) && ( Symbols >= PSymbol );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return false;
	}

	private static string CacheKey( IRequestContext context ) => context.CarrierId.TrimToUpper();

#region System Preferences
	public class SystemPreference
	{
		public bool DisableReusedTripId;
	}

	public SystemPreference GetSystemPreferences()
	{
		var Prefs = GetPreferences( true );

		var Result = new SystemPreference();

		foreach( var P in Prefs )
		{
			// ReSharper disable once ConvertSwitchStatementToSwitchExpression
			switch( P.DevicePreference.Trim() )
			{
			case DISABLE_REUSED_TRIP_ID:
				Result.DisableReusedTripId = P.Enabled;
				break;
			}
		}

		return Result;
	}
#endregion
}