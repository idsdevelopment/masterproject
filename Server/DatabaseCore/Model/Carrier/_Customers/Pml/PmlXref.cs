﻿using Protocol.Data._Customers.Pml;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public void Pml_AddUpdateXref( PmlXref requestObject )
	{
		AddUpdateLocationBarcodeToAccountId( XRef.TYPE.ACCOUNT_ID_TO_BARCODE, requestObject.AccountNumber, requestObject.Location );
		AddUpdateLocationBarcodeToAccountId( XRef.TYPE.SECONDARY_ACCOUNT_ID_TO_BARCODE, requestObject.GlnNumber, requestObject.Location );
	}

	public PmlXref Pml_GetXref( string locationCode )
	{
		var Xref = GetAccountIdFromBarcodeXref( locationCode );
		var Gln  = GetPmlGln( locationCode );

		return new PmlXref
		       {
			       Location      = locationCode,
			       AccountNumber = Xref?.Id1 ?? "",
			       GlnNumber     = Gln
		       };
	}
}