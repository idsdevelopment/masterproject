﻿using Protocol.Data._Customers.Pml;
using StorageV2.Carrier;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

namespace Database.Model.Databases.Carrier;

public static class PmlTripExtensions
{
	public static bool IsUplift( this string str ) => str.TrimToUpper() is "UP" or "UPLIFT" or "UPPACK" or "UPPC" or "UPTOTAL";
	public static bool IsUpTotal( this string str ) => str.TrimToUpper() is "UPTOTAL";
	public static bool IsUpTotal( this TripItem item ) => item.ItemCode.IsUpTotal();
	public static bool IsUplift( this TripItem item ) => item.ItemCode.IsUplift();
	public static bool IsUplift( this TripPackage package ) => package.PackageType.IsUplift();
}

public partial class CarrierDb
{
	public List<(Trip Trip, string OriginalDriver)> PML_ClaimSatchels( ClaimSatchels satchels )
	{
		var Result = new List<(Trip Trip, string OriginalDriver)>();

		try
		{
			var E          = Entity;
			var SatchelIds = satchels.SatchelIds;

			do
			{
				try
				{
					var Trips = ( from T in E.Trips
					              where SatchelIds.Contains( T.TripId )
					              select T ).ToList();

					// Only process the ones that are found
					SatchelIds = ( from T in Trips
					               select T.TripId ).ToList();

					foreach( var T in Trips )
					{
						var OriginalDriver = T.DriverCode;

						var Before = new TripLogger( T );

						new Signatures( Context ).AddUpdate( T.TripId, satchels.Signature );

						T.DriverCode       =  satchels.UserName;
						T.DeliveryNotes    =  $"Cage: {satchels.Cage}\r\n{T.DeliveryNotes}";
						T.ClaimedTime      =  satchels.ClaimTime;
						T.ClaimedLatitude  =  satchels.Latitude;
						T.ClaimedLongitude =  satchels.Longitude;
						T.Status2          |= (int)STATUS1.CLAIMED;
						E.SaveChanges();

						var After = new TripLogger( T );

						LogTrip( satchels.ProgramName, Before, After );

						Result.Add( ( T, OriginalDriver ) );
						SatchelIds.Remove( T.TripId );
					}
				}
				catch( DbUpdateConcurrencyException )
				{
					RandomDelay();
				}
				catch( Exception Exception )
				{
					Context.SystemLogException( Exception );
					break;
				}
			}
			while( SatchelIds.Count > 0 );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}


	public (Trip? Satchel, string OriginalDriver) PML_VerifySatchel( VerifySatchel verify )
	{
		(Trip? Satchel, string OriginalDriver) Result = ( null, "" );

		while( true )
		{
			try
			{
				var E      = Entity;
				var TripId = verify.TripId.Trim();

				var Satchel = ( from S in E.Trips
				                where S.TripId == TripId
				                select S ).FirstOrDefault();

				if( Satchel is not null )
				{
					Result.OriginalDriver = Satchel.DriverCode;

					var Before = new TripLogger( Satchel );

					Satchel.DriverCode = verify.UserName;
					Satchel.POD        = verify.UserName;
					Satchel.Status1    = (int)STATUS.VERIFIED;

					var VerifyTime = verify.DateTime;

					Satchel.VerifiedTime      = VerifyTime;
					Satchel.VerifiedLatitude  = verify.Latitude;
					Satchel.VerifiedLongitude = verify.Longitude;

					E.SaveChanges();

					var After = new TripLogger( Satchel );

					LogTrip( verify.ProgramName, Before, After );

					FinaliseTrip( verify.ProgramName, Satchel, DateTime.UtcNow, VerifyTime, verify.Latitude, verify.Longitude );

					Satchel.Status1 = (int)STATUS.FINALISED;

					Result.Satchel = Satchel;
				}
			}
			catch( DbUpdateConcurrencyException )
			{
				RandomDelay();
				continue;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}
	}


	public (List<string> PickedUpTripIds, Trip? Satchel) PML_UpdateSatchel( Satchel satchel )
	{
		(List<string> PickedUpTripIds, Trip? Satchel) Result = ( PickedUpTripIds: [], Satchel: null );

		try
		{
			var E     = Entity;
			var Trips = E.Trips;

			var HasUplifts = false;

			var TotalPacketQuantityForBarcodeInSatchel = new Dictionary<string, decimal>();

			foreach( var Package in satchel.Packages )
			{
				var PackageItems = new List<Protocol.Data.TripItem>();

				foreach( var Item in Package.Items )
				{
					if( !Item.IsUpTotal() ) // Only add items that are not UPTOTAL
					{
						var IsUplift = Item.IsUplift();
						HasUplifts |= IsUplift;
						var Pieces = Item.Pieces;

						if( !IsUplift || ( IsUplift && ( Pieces > 0 ) ) )
						{
							var Barcode = Item.Barcode;
							PackageItems.Add( Item );

							var (Ok, _, Packet, _, CartonQuantity) = GetPacketFromCarton( Barcode );

							if( Ok ) // Is Carton
							{
								Barcode =  Packet;
								Pieces  *= CartonQuantity;
							}

							var Qty = TotalPacketQuantityForBarcodeInSatchel.GetValueOrDefault( Barcode, 0 );
							TotalPacketQuantityForBarcodeInSatchel[ Barcode ] = Qty + Pieces;
						}
					}
					else
						HasUplifts = true;
				}

				Package.Items = PackageItems;
			}

			var Now        = DateTime.UtcNow;
			var PickupTime = satchel.PickupTime;

			Trip? PickupTrip = null;

			// Get the related trips
			foreach( var Rec in ( from T in Trips
			                      where satchel.TripIds.Contains( T.TripId )
			                      orderby T.TripId
			                      select T ).ToList() )
			{
				PickupTrip ??= Rec;

				var TripId = Rec.TripId;

				new Signatures( Context ).AddUpdate( TripId, satchel.Signature );

				var Before = new TripLogger( Rec );

				Rec.PickupTime   = PickupTime;
				Rec.LastModified = Now;
				Rec.Reference    = satchel.SatchelId;
				Rec.POP          = satchel.POP;

				var Packages = JsonConvert.DeserializeObject<List<TripPackage>>( Rec.TripItemsAsJson );

				if( Packages is not null )
				{
					foreach( var Package in Packages )
					{
						var Items = Package.Items;

						for( var Index = Items.Count - 1; Index >= 0; Index-- )
						{
							var Item    = Items[ Index ];
							var Barcode = Item.Barcode;

							if( TotalPacketQuantityForBarcodeInSatchel.ContainsKey( Barcode ) )
							{
								if( Item.Original <= 0 )
									Item.Original = Item.Pieces; // The create program isn't setting the original quantity

								Item.Pieces = 0; // May need to change

								if( TotalPacketQuantityForBarcodeInSatchel.TryGetValue( Item.Barcode, out var Residual ) && ( Residual > 0 ) )
								{
									var Allowed = Item.Original - Item.Pieces;

									if( Allowed > 0 )
									{
										var Adj = Math.Min( Residual, Allowed );
										Item.Pieces                                            += Adj;
										Residual                                               -= Adj;
										TotalPacketQuantityForBarcodeInSatchel[ Item.Barcode ] =  Residual;
									}
								}
							}
							else
								Items.RemoveAt( Index );
						}
					}
				}
				else
					Packages = [];

				Rec.TripItemsAsJson = JsonConvert.SerializeObject( Packages );

				Rec.Status1 = (int)STATUS.LIMBO;

				if( HasUplifts )
					Rec.Status4 = (int)STATUS3.IMPORTED; // Uplifts are created manually and are not imported

				E.SaveChanges();

				var After = new TripLogger( Rec );

				LogTrip( satchel.ProgramName, Before, After );
				Result.PickedUpTripIds.Add( TripId );

				AddTripToExport( TripId, (STATUS)Rec.Status1 );
			}

			if( PickupTrip is not null )
			{
				var Sid = satchel.SatchelId.Trim();

				static string NextId( string sid )
				{
					var P = sid.LastIndexOf( '-' );

					if( P > 0 )
					{
						var Temp = sid.Split( ["-"], StringSplitOptions.RemoveEmptyEntries );
						var Ndx  = Temp.Length - 1;
						var Num  = int.Parse( Temp[ Ndx ] );
						Array.Resize( ref Temp, Ndx );

						var Tmp = string.Join( "-", Temp );

						if( ( Num % 10 ) != 9 )
							return $"{Tmp}-{Num + 1}";
					}
					return $"{sid}-1";
				}

				var SatchelId = ( from T in Trips
				                  where T.TripId == Sid
				                  select T.TripId ).FirstOrDefault();

				if( SatchelId is not null )
					Sid = NextId( SatchelId );
				else
				{
					SatchelId = ( from T in E.TripStorageIndices
					              where T.TripId == Sid
					              select T.TripId ).FirstOrDefault();

					if( SatchelId is not null )
						Sid = NextId( SatchelId );
				}

				var STrip = new Trip( PickupTrip )
				            {
					            TripId          = Sid,
					            DriverCode      = satchel.Driver,
					            LastModified    = Now,
					            Reference       = string.Join( ",", satchel.TripIds ).Max900Bytes(), // Db Index Limit
					            Status1         = (int)STATUS.PICKED_UP,
					            ServiceLevel    = "Satchel",
					            PackageType     = "Satchel",
					            Pieces          = 1,
					            TripItemsType   = (int)TripUpdate.PACKAGE_TYPES.PACKAGE,
					            TripItemsAsJson = JsonConvert.SerializeObject( satchel.Packages ),
					            PickupTime      = satchel.PickupTime,
					            PickupLatitude  = satchel.PickupLatitude,
					            PickupLongitude = satchel.PickupLongitude,
					            CurrentZone     = PickupTrip.PickupZone,
					            POP             = satchel.POP
				            };

				Trips.Add( STrip );
				E.SaveChanges();

				new Signatures( Context ).AddUpdate( Sid, satchel.Signature );
				LogTrip( satchel.ProgramName, new TripLogger( STrip ) );

				Result.Satchel = STrip;
			}
			else
				throw new Exception( "No trip ids" );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}