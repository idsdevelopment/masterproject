﻿using Protocol.Data._Customers.Pml;
using Inventory = Database.Model.Databases.MasterTemplate.Inventory;
using Trip = Database.Model.Databases.MasterTemplate.Trip;

// ReSharper disable UseDeconstruction

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private static readonly object PmlLockObject = new();

	// Returns true if no more to do
	public bool PmlPreUpdateTripStatus( TripUpdateStatus t )
	{
		switch( t.Status )
		{
		case STATUS.DELETED:
			foreach( var TripId in t.TripIdList )
				PmlDeleteTrip( TripId, t.Program );
			break;
		}

		return false;
	}

	public TripList StorageSearchTrips( DateTime utcLastModifiedFrom, DateTime utcLastModifiedTo, STATUS fromStatus, STATUS toStatus, string[] serviceLevels, string[]? zones )
	{
		var Trips = from T in StorageSearchTrips( utcLastModifiedFrom, utcLastModifiedTo, fromStatus, toStatus )
		            where serviceLevels.Contains( T.ServiceLevel )
		            select T;

		return new TripList( zones switch
		                     {
			                     null => Trips,
			                     _ => from T in Trips
			                          where zones.Contains( T.CurrentZone.Trim() )
			                          select T
		                     } );
	}


	public TripList PML_FindTripsDetailedUpdatedSince( PmlFindSince requestObject )
	{
		var RetVal = new TripList();

		try
		{
			var ServiceLevels = requestObject.ServiceLevels.IsNullOrWhiteSpace()
				                    ? new[] { "" }
				                    : ( from S in requestObject.ServiceLevels.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries )
				                        select S.Trim() ).ToArray();

			var Now        = DateTime.UtcNow;
			var StartTime  = Now.AddSeconds( -Math.Abs( requestObject.SinceSeconds ) );
			var FromStatus = (int)requestObject.FromStatus;
			var ToStatus   = (int)requestObject.ToStatus;

			List<Trip> DbTrips      = [];
			TripList   StorageTrips = [];
			var        Tasks        = new Task[ 2 ];

			if( requestObject.AllZones )
			{
				Tasks[ 0 ] = Task.Run( () =>
				                       {
					                       using var Db = new CarrierDb( Context );

					                       DbTrips = ( from T in Db.Entity.Trips
					                                   where ( T.LastModified >= StartTime )
					                                         && ServiceLevels.Contains( T.ServiceLevel )
					                                         && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
					                                   select T ).ToList();
				                       } );
			}
			else
			{
				var Zones = requestObject.Zones.IsNullOrWhiteSpace()
					            ? new[] { "" }
					            : ( from Z in requestObject.Zones.Split( new[] { ',' }, StringSplitOptions.RemoveEmptyEntries )
					                select Z.Trim() ).ToArray();

				Tasks[ 0 ] = Task.Run( () =>
				                       {
					                       using var Db = new CarrierDb( Context );

					                       DbTrips = ( from T in Entity.Trips
					                                   where ( T.LastModified >= StartTime )
					                                         && Zones.Contains( T.CurrentZone )
					                                         && ServiceLevels.Contains( T.ServiceLevel )
					                                         && ( T.Status1 >= FromStatus ) && ( T.Status1 <= ToStatus )
					                                   select T ).ToList();
				                       } );
			}

			Tasks[ 1 ] = Task.Run( () =>
			                       {
				                       StorageTrips = StorageSearchTrips( StartTime, Now, (STATUS)FromStatus, (STATUS)ToStatus, ServiceLevels, null );
			                       } );

			Task.WaitAll( Tasks );

			RetVal.AddRange( StorageTrips );

			foreach( var Trip in DbTrips )
				RetVal.Add( Trip.ToTrip() );

			return new TripList( from T in RetVal
			                     orderby T.LastModified
			                     select T );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return RetVal;
	}

	private void PmlDeleteTrip( string masterTripId, string program )
	{
		var Xref = PmlSalesForce.Get( Context, masterTripId );

		if( Xref is not null )
		{
			var (_, SellerId, Storefront, _, _) = Xref;
			AddTripStatusToExport( masterTripId, STATUS.DELETED, SellerId, Storefront, "", "" );
		}
		DeleteTrip( program, masterTripId, true );
	}

#region Trip
	// Returns true if no more to do
	public bool PmlPreAddUpdateTrip( TripUpdate t )
	{
		switch( t.Status1 )
		{
		case STATUS.DELETED:
			PmlDeleteTrip( t.TripId, t.Program );
			break;

		default:
			var Packages = t.Packages; // Fix Pml Cartons

			foreach( var Package in Packages )
			{
				foreach( var Item in Package.Items )
				{
					var Barcode = Item.Barcode;
					var Qty     = GetCartonQuantity( Barcode );

					decimal CartonQty;
					var     IsCarton = Qty is not null;

					if( IsCarton )
						CartonQty = Qty!.Value;
					else
					{
						var (Ok, _, _, CartonQuantity) = GetFromCartonPacket( Barcode );

						CartonQty = Ok ? CartonQuantity : 0;
					}

					Item.IsCarton       = IsCarton;
					Item.CartonQuantity = CartonQty;
				}
			}

			t.Packages = Packages;
			break;
		}
		return false;
	}


	public TripUpdate PML_updateTripDetailedQuick( TripUpdate update )
	{
		lock( PmlLockObject ) // Queue request coming in
		{
			try
			{
				var E      = Entity;
				var TripId = update.TripId.Trim();

				if( update.Status1 == STATUS.DELETED )
				{
					if( TripId.IsNullOrWhiteSpace() )
						return update;

					while( true )
					{
						var (MasterTripId, Ok, BCode, _, _, _, Pieces, Weight) = AzureIdFromXref( TripId );

						// Is an Item
						if( Ok )
						{
							var Trip = ( from T in E.Trips
							             where T.TripId == MasterTripId
							             select T ).FirstOrDefault();

							if( Trip is not null )
							{
								var Before   = new TripLogger( Trip );
								var Packages = JsonConvert.DeserializeObject<List<TripPackage>?>( Trip.TripItemsAsJson ) ?? [];

								// Update the items
								foreach( var Package in Packages )
								{
									var Items = Package.Items;
									var Count = Items.Count;

									for( var Index = Count; --Index <= 0; )
									{
										var Item = Items[ Index ];

										if( Item.Barcode == BCode )
										{
											Trip.Pieces -= Pieces;
											Trip.Weight -= Weight;

											Item.Pieces -= Pieces;

											if( Item.Pieces == 0 )
												Items.RemoveAt( Index );
											else
												Item.Weight -= Weight;

											goto Found;
										}
									}
								}
							Found:
								Trip.TripItemsAsJson = JsonConvert.SerializeObject( Packages );
								Trip.LastModified    = DateTime.UtcNow;

								try
								{
									E.SaveChanges();

									DeleteTrip( update.Program, TripId );
									DeleteTripFromXref( MasterTripId!, TripId );

									var After = new TripLogger( Trip );
									LogTrip( update.Program, Before, After );
									update = Trip.ToTripUpdate( update.Program, true, true, true );
								}
								catch( DbUpdateConcurrencyException )
								{
									RandomDelay();
									continue;
								}
								catch( Exception Ex )
								{
									Context.SystemLogException( Ex );
								}
							}
						}
						else // Maybe a Master Trip
						{
							PmlDeleteTrip( update.TripId, update.Program );
							DeleteTrip( update.Program, TripId );

							update.BroadcastToDriver        = true;
							update.BroadcastToDispatchBoard = true;
							update.BroadcastToDriverBoard   = true;
						}
						return update;
					}
				}

				if( update.TripId.IsNullOrEmpty() )
					update.TripId = Trip.NextTripId( Context );

				switch( TripId[ 0 ] )
				{
				// Update the Satchel with the AURT number
				case 'Z':
					{
						if( update.DeliveryNotes.Contains( "AUDR", StringComparison.OrdinalIgnoreCase )
						    && update.BillingNotes.Contains( "AURT", StringComparison.OrdinalIgnoreCase ) )
						{
							static string GetValue( string key, string field )
							{
								return ( from S in field.Split( new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries )
								         where S.StartsWith( key, StringComparison.OrdinalIgnoreCase )
								         select S ).DefaultIfEmpty( "" ).First().Trim();
							}

							var Audr = GetValue( "AUDR", update.DeliveryNotes );
							var Aurt = GetValue( "AURT", update.BillingNotes );

							if( ( Audr != "" ) && ( Aurt != "" ) )
							{
								while( true )
								{
									try
									{
										var Trip = ( from T in E.Trips
										             where T.TripId == Audr
										             select T ).FirstOrDefault();

										if( Trip is not null )
										{
											var BNotes = Trip.BillingNotes;

											if( !BNotes.Contains( Aurt, StringComparison.OrdinalIgnoreCase ) )
											{
												var Before = new TripLogger( Trip );

												Trip.BillingNotes = $"{Aurt}\r\n{BNotes}";
												E.SaveChanges();

												var After = new TripLogger( Trip );
												LogTrip( update.Program, Before, After );
											}
										}
									}
									catch( DbUpdateConcurrencyException )
									{
										RandomDelay();
										continue;
									}
									break;
								}
							}
						}
						break;
					}

				case 'O': // Should Only Be Oz server trips not AUDR
					{
						if( !IsTripIdValid( update.TripId ) ) // Throw away modifications form Ids1
							return update;

						update.Status1             = STATUS.ACTIVE;
						update.BillingAddressNotes = "";

						var MercuryNumberArray = update.BillingNotes.Split( new[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries );
						var MercuryNumber      = MercuryNumberArray.Length > 0 ? MercuryNumberArray[ 0 ].Trim() : "";

						var Barcode = update.Reference.Trim();
						update.Group4 = Barcode;

						update.Reference = MercuryNumber;

						var Inv = E.Inventories;

						var InvRec = ( from I in Inv
						               where I.Barcode == Barcode
						               select I ).FirstOrDefault();

						// Create the Inventory Item If It Doesn't Exist
						if( InvRec is null )
						{
							Inv.Add( InvRec = new Inventory
							                  {
								                  Barcode          = Barcode,
								                  InventoryCode    = Barcode,
								                  ShortDescription = update.PackageType,
								                  LongDescription  = update.PackageType,
								                  Active           = true,
								                  PackageQuantity  = update.Pieces,
								                  UnitWeight       = update.Weight,
								                  PackageType      = update.PackageType,

								                  UNClass        = "",
								                  BinX           = "",
								                  BinY           = "",
								                  BinZ           = "",
								                  DangerousGoods = false,
								                  IsContainer    = false,
								                  UnitVolume     = 0,

								                  // Customisable Fields
								                  BitData1 = false,
								                  BitData2 = false,
								                  BitData3 = false,
								                  BitData4 = false,
								                  BitData5 = false,
								                  BitData6 = false,
								                  BitData7 = false,
								                  BitData8 = false,

								                  DecimalData1 = 0,
								                  DecimalData2 = 0,
								                  DecimalData3 = 0,
								                  DecimalData4 = 0,

								                  IntData1 = 0,
								                  IntData2 = 0,
								                  IntData3 = 0,
								                  IntData4 = 0,

								                  StringData1 = "",
								                  StringData2 = "",
								                  StringData3 = "",
								                  StringData4 = ""
							                  } );

							E.SaveChanges();
						}
						else
							update.PackageType = InvRec.ShortDescription;

						update.Packages =
						[
							new TripPackage
							{
								PackageType = $"{update.ServiceLevel} ({MercuryNumber})",
								Weight      = update.Weight,
								Pieces      = update.Pieces,
								Original    = update.Pieces,
								Items =
								[
									new Protocol.Data.TripItem
									{
										Barcode     = Barcode,
										ItemCode    = update.ServiceLevel,
										Description = InvRec.ShortDescription,
										Reference   = MercuryNumber,
										Pieces      = update.Pieces,
										Original    = update.Pieces,
										Weight      = update.Weight,
										BinaryData1 = InvRec.BitData1 // Pml Kiosk Alert Flag
									}
								]
							}
						];

						update.BroadcastToDispatchBoard = true;
						update.BroadcastToDriver        = false;
						update.BroadcastToDriverBoard   = false;

						var Dispatched = false;

						// Auto dispatch
						var PostalRec = ( from P in E.PostalCodeToRoutes
						                  where P.PostalCode == update.PickupAddressPostalCode
						                  select P ).FirstOrDefault();

						var AutoDispatch = PostalRec is not null;

						if( AutoDispatch )
						{
							var Route = ( from R in E.Routes
							              where R.RouteId == PostalRec!.RouteId
							              select R.Name ).FirstOrDefault();

							if( Route is not null )
							{
								Dispatched = true;

								update.Status1                  = STATUS.DISPATCHED;
								update.Driver                   = Route;
								update.PickupZone               = PostalRec!.Zone4;
								update.BroadcastToDispatchBoard = false;
								update.BroadcastToDriver        = true;
								update.BroadcastToDriverBoard   = true;
							}
						}

						update.Location           = update.PickupAddressBarcode;
						update.OriginalPieceCount = update.Pieces;

						AddUpdateTrip( update );

						if( Dispatched )
						{
							var Driver = update.Driver;

							if( Driver.IsNotNullOrWhiteSpace() )
								AddDriver( update.Program, Driver, Driver );
						}

						static string FixRegion( string region )
						{
							if( region.Contains( "New", StringComparison.OrdinalIgnoreCase ) || region.Contains( "NSW", StringComparison.OrdinalIgnoreCase ) )
								return "NSW";

							if( region.Contains( "Victoria", StringComparison.OrdinalIgnoreCase ) || region.Contains( "VIC", StringComparison.OrdinalIgnoreCase ) )
								return "VIC";

							if( region.Contains( "Capital", StringComparison.OrdinalIgnoreCase ) || region.Contains( "ACT", StringComparison.OrdinalIgnoreCase ) )
								return "ACT";

							if( region.Contains( "Northern", StringComparison.OrdinalIgnoreCase ) || region.Contains( "NT", StringComparison.OrdinalIgnoreCase ) )
								return "NT";

							if( region.Contains( "Queensland", StringComparison.OrdinalIgnoreCase ) || region.Contains( "QLD", StringComparison.OrdinalIgnoreCase ) )
								return "QLD";

							if( region.Contains( "South", StringComparison.OrdinalIgnoreCase ) || region.Contains( "SA", StringComparison.OrdinalIgnoreCase ) )
								return "SA";

							if( region.Contains( "Tasmania", StringComparison.OrdinalIgnoreCase ) || region.Contains( "TAS", StringComparison.OrdinalIgnoreCase ) )
								return "TAS";

							if( region.Contains( "Western", StringComparison.OrdinalIgnoreCase ) || region.Contains( "WA", StringComparison.OrdinalIgnoreCase ) )
								return "WA";

							return "";
						}

						update.PickupAddressRegion   = FixRegion( update.PickupAddressRegion );
						update.DeliveryAddressRegion = FixRegion( update.DeliveryAddressRegion );
						update.BillingAddressRegion  = FixRegion( update.BillingAddressRegion );

						AddCompanyAddress( update );

						var StatusList = new List<int> { (int)STATUS.ACTIVE };

						if( AutoDispatch )
							StatusList.Add( (int)STATUS.DISPATCHED );

						// Consolidate Into Trip
						var ConsolidationTrip = ( from T in E.Trips
						                          orderby T.Status1 descending, T.TripId // Auto Dispatch first
						                          where StatusList.Contains( T.Status1 ) && ( T.Location == update.Location )
						                                                                 && ( T.TripId != update.TripId ) && ( T.Reference == MercuryNumber )
						                                                                 && !T.ServiceLevel.ToUpper().StartsWith( "UP" )
						                          select T ).FirstOrDefault();

						// Previously consolidated?
						if( ConsolidationTrip is not null )
						{
							//Newly added rec
							var NewRec = ( from T in E.Trips
							               where T.TripId == update.TripId
							               select T ).FirstOrDefault();

							if( NewRec is not null )
							{
								var Before = new TripLogger( NewRec );

								NewRec.BillingAddressNotes = $"Consolidated into shipment: {ConsolidationTrip.TripId}\r\n{NewRec.BillingAddressNotes}";
								NewRec.Status1             = (int)STATUS.LIMBO;
								E.SaveChanges();

								var After = new TripLogger( NewRec );
								LogTrip( update.Program, Before, After );

								var NewRecBarcode = NewRec.Reference;

								NewRec.Reference = $"{NewRec.TripId} -> {ConsolidationTrip.TripId}";

								UpdateTrip( NewRec );

								SaveTripToXref( ConsolidationTrip.TripId, NewRec.TripId,
								                NewRecBarcode, NewRec.Group4,
								                NewRec.ServiceLevel, NewRec.PackageType,
								                NewRec.Reference,
								                NewRec.Pieces, NewRec.Weight );

								var Packages = JsonConvert.DeserializeObject<List<TripPackage>?>( ConsolidationTrip.TripItemsAsJson ) ?? [];

								while( true )
								{
									try
									{
										Before = new TripLogger( ConsolidationTrip );

										Packages.AddRange( update.Packages );
										Packages = Packages.Consolidate();

										var TotalPieces = 0M;
										var TotalWeight = 0M;

										foreach( var TripPackage in Packages )
										{
											TotalPieces += TripPackage.Pieces;
											TotalWeight += TripPackage.Weight;
										}

										ConsolidationTrip.Pieces = TotalPieces;
										ConsolidationTrip.Weight = TotalWeight;

										ConsolidationTrip.TripItemsAsJson = JsonConvert.SerializeObject( Packages );

										ConsolidationTrip.LastModified = DateTime.UtcNow;

										ConsolidationTrip.BillingAddressNotes = $"Consolidated from shipment: {update.TripId}\r\n{ConsolidationTrip.BillingAddressNotes}";

										if( MercuryNumber.IsNotNullOrWhiteSpace() )
										{
											var CRef = ConsolidationTrip.Reference.Trim();

											if( CRef.IsNotNullOrWhiteSpace() )
											{
												if( !CRef.EndsWith( MercuryNumber ) && !CRef.Contains( $"{MercuryNumber}," ) )
													ConsolidationTrip.Reference = $"{CRef},{MercuryNumber}";
											}
											else
												ConsolidationTrip.Reference = MercuryNumber;
										}

										After = new TripLogger( ConsolidationTrip );
										LogTrip( update.Program, Before, After );

										E.SaveChanges();

										update = ConsolidationTrip.ToTripUpdate( update.Program, update.BroadcastToDispatchBoard, update.BroadcastToDriverBoard, update.BroadcastToDriver );
										break;
									}
									catch( DbUpdateConcurrencyException Exception )
									{
										RandomDelay();
										// Get the current entity values and the values in the database
										var Entry          = Exception.Entries.Single();
										var DatabaseValues = Entry.GetDatabaseValues();

										//Record deleted ??
										if( DatabaseValues is not null )
											ConsolidationTrip = (Trip)DatabaseValues.ToObject();
										else
											break;
									}
								}
							}
						}
						break;
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogExceptionProgram( update.Program, Exception );
			}

			return update;
		}
	}
#endregion
}