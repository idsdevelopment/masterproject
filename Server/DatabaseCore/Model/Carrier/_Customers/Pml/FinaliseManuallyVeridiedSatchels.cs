﻿// ReSharper disable LocalVariableHidesMember

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public TripIdListAndProgram FinaliseManuallyVerifiedSatchels( DateTimeOffset endDateTime )
	{
		const string PROGRAM = "Finalise Manually Verified Satchels Version 1.01";
		var          Result  = new TripIdListAndProgram { Program = PROGRAM };

		try
		{
			var UtcDate = endDateTime.UtcDateTime.Date.ToEndOfDay();

			var TripIds = ( from T in Entity.Trips
			                where ( T.Status1 == (int)STATUS.VERIFIED ) && ( T.LastModified <= UtcDate ) && T.TripId.StartsWith( "AUDR" )
			                select T.TripId ).ToList();

			Result.TripIds.AddRange( TripIds ); // Copy, because it's used in another thread

			var Ctx = Context.Clone();

			Tasks.RunVoid( () =>
			               {
				               using var Db = new CarrierDb( Ctx );
				               Db.FinaliseTrips( PROGRAM, TripIds );
			               } );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Result;
	}
}