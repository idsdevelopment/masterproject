﻿using Protocol.Data._Customers.Pml;
using Utils.Csv;
using Staff = Protocol.Data.Staff;
using Zone = Database.Model.Databases.MasterTemplate.Zone;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	private const int POST_CODE = 0,
	                  OPERATION = 1,
	                  REGION    = 2,
	                  IDS_ROUTE = 3;

	private const string PML_POSTAL_CODE_IMPORT = "PML Post Codes";

	public PmlRoutes PML_GetRoutes()
	{
		var Result = new PmlRoutes();

		try
		{
			var E = Entity;

			var Recs = ( from R in E.Routes.AsEnumerable()
			             orderby R.Name
			             select new
			                    {
				                    IdsRoute = R.Name,
				                    PostCodes = from P in E.PostalCodeToRoutes
				                                orderby P.Zone1, P.Zone2, P.PostalCode
				                                where P.RouteId == R.RouteId
				                                select P
			                    } ).ToList();

			foreach( var R in Recs )
			{
				foreach( var PostalCodeToRoute in R.PostCodes )
				{
					Result.Add( new PmlRoute
					            {
						            IdsRoute   = R.IdsRoute,
						            Operation  = PostalCodeToRoute.Zone1,
						            Region     = PostalCodeToRoute.Zone2,
						            PostalCode = PostalCodeToRoute.PostalCode
					            } );
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return Result;
	}

	public void PML_DeleteRoutes( PmlDeleteRoutes requestObject )
	{
		try
		{
			var E = Entity;

			var Routes = ( from R in E.Routes
			               where requestObject.Contains( R.Name )
			               select R ).ToList();

			E.Routes.RemoveRange( Routes ); // Post codes will  delete via database trigger
			E.SaveChanges();

			foreach( var Route in Routes )
				EnableDisableStaffMember( requestObject.Program, Route.Name, STAFF_TYPE.CARRIER, false );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void PML_DeletePostcodes( PmlDeletePostCodes requestObject )
	{
		try
		{
			var E = Entity;

			var Recs = ( from P in E.PostalCodeToRoutes
			             where requestObject.Contains( P.PostalCode )
			             select P ).ToList();

			if( Recs.Count > 0 )
			{
				E.PostalCodeToRoutes.RemoveRange( Recs );
				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void PML_PostCodeImport( string csv )
	{
		try
		{
			var DriverRole = ( from R in GetRoles()
			                   where R.IsDriver
			                   select R ).FirstOrDefault();

			if( DriverRole is not null )
			{
				var Csv = new Csv( csv );

				var E      = Entity;
				var PCodes = E.PostalCodeToRoutes;
				var Routes = E.Routes;
				var Zones  = E.Zones;

				var RouteNames = new HashSet<string>();

				var First = true; // Check for header

				foreach( Row Row in Csv )
				{
					if( First )
					{
						First = false;
						var Op = Row[ OPERATION ].AsString.Trim();

						if( string.Compare( Op, "OPERATION", StringComparison.OrdinalIgnoreCase ) == 0 )
							continue;
					}
					var PostCode = Row[ POST_CODE ].AsString.Trim();

					if( PostCode.IsNotNullOrWhiteSpace() )
					{
						var Operation = Row[ OPERATION ].AsString.Trim();

						if( Operation.IsNotNullOrWhiteSpace() )
						{
							var Region = Row[ REGION ].AsString.Trim();

							if( Region.IsNotNullOrWhiteSpace() )
							{
								var IdsRoute = Row[ IDS_ROUTE ].AsString.Trim();

								RouteNames.Add( IdsRoute );

								if( IdsRoute.IsNotNullOrWhiteSpace() )
								{
									var Zone = $"{Operation}+{Region}";

									var ZRec = ( from Z in Zones
									             where Z.ZoneName == Zone
									             select Z ).FirstOrDefault();

									if( ZRec is null )
									{
										ZRec = new Zone
										       {
											       ZoneName     = Zone,
											       Abbreviation = $"{Operation.SubStr( 0, 9 )}+{Region.SubStr( 0, 9 )}"
										       };
										Zones.Add( ZRec );
									}

									E.SaveChanges();

									var RouteId = ( from R in Routes
									                where R.Name == IdsRoute
									                select R ).FirstOrDefault()?.RouteId ?? -1;

									if( RouteId == -1 )
									{
										var (Id, Ok) = AddUpdateRoute( PML_POSTAL_CODE_IMPORT, Context.CarrierId, IdsRoute );

										if( !Ok )
											continue;

										RouteId = Id;
									}

									var Rec = ( from P in PCodes
									            where PostCode == P.PostalCode
									            select P ).FirstOrDefault();

									if( Rec is null )
									{
										Rec = new PostalCodeToRoute
										      {
											      PostalCode = PostCode,
											      RouteId    = RouteId,
											      Zone1      = Operation,
											      Zone2      = Region,
											      Zone3      = "",
											      Zone4      = Zone // So Zone delete follows through
										      };
									}
									else
									{
										// Can't update primary key. Have to delete first
										PCodes.Remove( Rec );
										E.SaveChanges();

										Rec.RouteId = RouteId;
										Rec.Zone1   = Operation;
										Rec.Zone2   = Region;
									}

									PCodes.Add( Rec );
									E.SaveChanges();
								}
							}
						}
					}
				}

				// Create the staff records
				var StaffIds = ( from S in E.Staff
				                 where S.Type == (int)STAFF_TYPE.CARRIER
				                 select S.StaffId ).ToList();

				var NewIds = ( from R in RouteNames
				               where !StaffIds.Contains( R )
				               select R ).ToList();

				foreach( var Id in NewIds )
				{
					var Staff = new UpdateStaffMemberWithRolesAndZones( PML_POSTAL_CODE_IMPORT, new Staff
					                                                                            {
						                                                                            StaffId   = Id,
						                                                                            Enabled   = true,
						                                                                            Password  = Id,
						                                                                            FirstName = Id
					                                                                            } );
					Staff.Roles.Add( DriverRole );
					AddUpdateStaffMember( Staff, false );
				}
			}
			else
				throw new Exception( "No Driver Roles defined" );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}
}