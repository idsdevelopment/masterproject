﻿using Role = Database.Model.Databases.MasterTemplate.Role;

namespace Database.Model.Databases.Carrier;

public partial class CarrierDb
{
	public const string ADMIN = "Administrator";


	// Seem to be able to get 2 requests almost at the same time
	private static readonly object AddRoleLock = new();

	private readonly string[] RolesArray =
	[
//			ADMIN
	];

	public IDictionary<int, Role> GetRolesAsDictionaryById()
	{
		try
		{
			var Roles = ( from R in Entity.Roles
			              select R ).AsEnumerable();

			return ( from R in Roles
			         group R by R.Id
			         into Ids
			         let R1 = Ids.FirstOrDefault()
			         where R1 != null
			         select Ids ).ToDictionary( r => r.Key, r => r.First() );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return new Dictionary<int, Role>();
	}


	public Roles GetRoles()
	{
		var Result = new Roles();

		try
		{
			var DbRoles = Entity.Roles;

			var Roles = ( from Role in DbRoles
			              select Role ).ToList();

			// Make sure mandatory roles exist
			var Changed = false;

			foreach( var Role in RolesArray.Where( role => Roles.All( r => r.RoleName != role ) ) )
			{
				DbRoles.Add( new Role
				             {
					             RoleName        = Role,
					             Mandatory       = true,
					             JsonObject      = "",
					             IsAdministrator = true
				             } );

				Changed = true;
			}

			if( Changed )
			{
				Entity.SaveChanges();

				Roles = ( from Role in DbRoles
				          select Role ).ToList();
			}

			Result.AddRange( from Role in Roles
			                 select new Protocol.Data.Role
			                        {
				                        Mandatory = Role.Mandatory,
				                        Name      = Role.RoleName,

				                        IsAdministrator = Role.IsAdministrator,
				                        IsDriver        = Role.IsDriver,
				                        IsWebService    = Role.IsWebService,

				                        JsonObject = Role.JsonObject ?? ""
			                        } );
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}

		return Result;
	}

	public Role GetDriverRole()
	{
		var Roles = Entity.Roles;

		var Role = ( from Rle in Roles
		             where Rle.IsDriver
		             select Rle ).FirstOrDefault();

		if( Role is null )
		{
			Role = new Role
			       {
				       RoleName   = "Drivers",
				       IsDriver   = true,
				       JsonObject = ""
			       };

			try
			{
				Roles.Add( Role );
				Entity.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
		return Role;
	}

	public void AddRoles( List<Protocol.Data.Role> roles )
	{
		try
		{
			var E = Entity;

			var DbRoles = E.Roles;

			foreach( var Rle in roles )
			{
				var Role = ( from R in DbRoles
				             where R.RoleName == Rle.Name
				             select R ).FirstOrDefault();

				if( Role is null )
				{
					Role = new Role
					       {
						       RoleName   = Rle.Name,
						       Mandatory  = Rle.Mandatory,
						       JsonObject = Rle.JsonObject,

						       IsAdministrator = Rle.IsAdministrator,
						       IsDriver        = Rle.IsDriver,
						       IsWebService    = Rle.IsWebService
					       };
					DbRoles.Add( Role );
				}
				else
				{
					Role.Mandatory  = Rle.Mandatory;
					Role.JsonObject = Rle.JsonObject;
				}
				E.SaveChanges();
			}
		}
		catch( Exception Ex )
		{
			Context.SystemLogException( Ex );
		}
	}

	public void AddDefaultRoles()
	{
		var E = Entity;

		try
		{
			var DbRoles = E.Roles;

			DbRoles.RemoveRange( from R in DbRoles
			                     where R.Mandatory
			                     select R );

			foreach( var R in RolesArray )
			{
				DbRoles.Add( new Role
				             {
					             Mandatory  = true,
					             RoleName   = R,
					             JsonObject = ""
				             } );
			}

			E.SaveChanges();
		}
		catch( Exception Ex )
		{
			Context.SystemLogException( Ex );
		}
	}

	public void AddUpdateRole( Protocol.Data.Role role )
	{
		lock( AddRoleLock )
		{
			try
			{
				var E        = Entity;
				var Roles    = E.Roles;
				var RoleName = role.Name.Trim();

				var LowerRoleName = RoleName.ToLower();

				var Rec = ( from R in Roles
				            where R.RoleName.ToLower() == LowerRoleName
				            select R ).FirstOrDefault();

				if( Rec is null )
				{
					Rec = new Role
					      {
						      RoleName = RoleName,

						      IsAdministrator = role.IsAdministrator,
						      IsDriver        = role.IsDriver,
						      IsWebService    = role.IsWebService,

						      JsonObject = role.JsonObject
					      };
					Roles.Add( Rec );
				}
				else
				{
					Rec.IsAdministrator = role.IsAdministrator;
					Rec.IsDriver        = role.IsDriver;
					Rec.IsWebService    = role.IsWebService;

					Rec.JsonObject = role.JsonObject;
				}

				E.SaveChanges();
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
		}
	}

	public void RenameRole( RoleRename role )
	{
		try
		{
			if( role.Name != role.NewName )
			{
				var E     = Entity;
				var Roles = E.Roles;

				// Make sure new name doesn't exist already
				if( !( from R in Roles
				       where R.RoleName == role.NewName
				       select R ).Any() )
				{
					var Rec = ( from R in Roles
					            where R.RoleName == role.Name
					            select R ).FirstOrDefault();

					if( Rec is not null )
					{
						Rec.RoleName = role.NewName;

						Rec.IsAdministrator = role.IsAdministrator;
						Rec.IsDriver        = role.IsDriver;
						Rec.IsWebService    = role.IsWebService;

						E.SaveChanges();
					}
				}
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}

	public void DeleteRoles( DeleteRoles roles )
	{
		try
		{
			var E     = Entity;
			var Roles = E.Roles;

			var Recs = from R in Roles
			           where !R.Mandatory && roles.Contains( R.RoleName )
			           select R;

			if( Recs.Any() )
			{
				Roles.RemoveRange( Recs );
				E.SaveChanges();
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
	}
}