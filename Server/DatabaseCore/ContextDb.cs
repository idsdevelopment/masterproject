﻿public class ContextDb<TContext> : IDisposable, IAsyncDisposable where TContext : DbContext, new()
{
	public const  int    DATABASE_TIMEOUT_IN_MINUTES = 3;
	private const string CONNECTION_STRING           = "Server=tcp:idsroute.database.windows.net;Initial Catalog={CARRIER_ID};User ID=Terry;Password=Zaphod2015;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;MultipleActiveResultSets=True;Max Pool Size=300;";

	public IRequestContext Context { get; private set; } = null!;
	public TContext        Entity  { get; private set; }

	public bool Exists => Entity.Database.CanConnect();

	public ContextDb( string databaseName, Func<DbContextOptionsBuilder<TContext>, TContext> instance )
	{
		var Builder = new DbContextOptionsBuilder<TContext>();

		Builder.UseSqlServer( CONNECTION_STRING.Replace( "{CARRIER_ID}", databaseName.ToUpper() ),
		                      optionsBuilder => optionsBuilder.EnableRetryOnFailure() );

		Entity = instance( Builder );
		Entity.Database.SetCommandTimeout( DATABASE_TIMEOUT_IN_MINUTES * 60 );
	}

	public ContextDb( IRequestContext context, Func<DbContextOptionsBuilder, TContext> instance ) : this( context.CarrierId, instance )
	{
		Context = context;
	}

	public void RandomDelay()
	{
		Utils.RandomDelay.Delay( 1, 10 );
	}

	public void SaveChanges()
	{
		Entity.SaveChanges();
	}

	public async ValueTask DisposeAsync()
	{
		await Task.CompletedTask;
		Dispose();
	}

	public void Dispose()
	{
		Context = null!;
		Entity.Dispose();
		Entity = null!;
	}
}