﻿// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable ConvertToConstant.Local

namespace RequestContext;

public class RequestContext : AIRequestInterface
{
	/// <summary>
	///     Auth Token
	/// </summary>
	[JsonProperty( Order = 0 )]
	public string R { get; } = new Random().Next( 100_000_000 ).ToString(); // Make sure random is first (Greatest change in key)

	[JsonProperty( Order = 1 )]
	public string C // Carrier Id
	{
		get => CarrierId;
		set => CarrierId = value;
	}

	[JsonProperty( Order = 2 )]
	public string U // User Name
	{
		get => UserName;
		set => UserName = value;
	}

	[JsonProperty( Order = 3 )]
	public string P
	{
		get => Password;
		set => Password = value;
	} // Password

	[JsonProperty( Order = 4 )]
	public sbyte T // Timezone offset
	{
		get => TimeZoneOffset;
		set => TimeZoneOffset = value;
	}

	[JsonProperty( Order = 5 )]
	public bool D // Debug server
	{
		get => Debug;
		set => Debug = value;
	}

	[JsonProperty( Order = 6 )]
	public bool I // Is Ids
	{
		get => IsIds;
		set => IsIds = value;
	}

	[JsonProperty( Order = 7 )]
	public bool N // New Carrier
	{
		get => IsNewCarrier;
		set => IsNewCarrier = value;
	}

	[JsonProperty( Order = 8 )]
	public override string IpAddress
	{
		get => string.IsNullOrWhiteSpace( _IpAddress ) ? "Unknown" : _IpAddress;
		set => _IpAddress = value;
	}

	[JsonProperty( Order = 9 )]
	public new int UserId
	{
		get => base.UserId;
		set => base.UserId = value;
	}


	[JsonProperty( Order = 10 )]
	public new bool IsCustomerUser
	{
		get => base.IsCustomerUser;
		set => base.IsCustomerUser = value;
	}

	/// <summary>
	///     Not Part Of Auth Token
	/// </summary>
	[JsonIgnore]
	public Configuration Configuration
	{
		get => Users.Configuration;
		set => Users.Configuration = value;
	}

	public override (string Account, string Key) BlobStorageAccount
	{
		get
		{
			var Cfg = Configuration;

			return ( Account: Cfg.BlobStorageAccount, Key: Cfg.BlobStoragePrimaryKey );
		}
	}

	[JsonIgnore]
	public override string StorageAccount => GetEndPoint().StorageAccount;

	[JsonIgnore]
	public override string StorageAccountKey => GetEndPoint().StorageAccountKey;

	[JsonIgnore]
	public override string StorageTableEndpoint => GetEndPoint().StorageTableEndpoint;

	[JsonIgnore]
	public override string StorageFileEndpoint => GetEndPoint().StorageFileEndpoint;

	[JsonIgnore]
	public override string StorageBlobEndpoint => GetEndPoint().StorageBlobEndpoint;

	[JsonIgnore]
	public override string ServiceBusPrimaryConnectionString => Configuration.ServiceBusPrimaryConnectionString;

	[JsonIgnore]
	public override string ServiceBusSecondaryConnectionString => Configuration.ServiceBusSecondaryConnectionString;

	[JsonIgnore]
	public override string SendGridApiKey => Configuration.SendGridKey;

	[JsonIgnore]
	public override DateTime LastMessagingCleanup
	{
		get
		{
			using var Db = new CarrierDb( this );
			return Db.LastMessagingCleanup;
		}

		set
		{
			using var Db = new CarrierDb( this );
			Db.LastMessagingCleanup = value;
		}
	}

	[JsonIgnore]
	public override long NextInvoiceNumber
	{
		get
		{
			using var Db = new CarrierDb( this );
			return Db.NextInvoiceNumber;
		}

		set
		{
			using var Db = new CarrierDb( this );
			Db.NextInvoiceNumber = value;
		}
	}

	public override DateTime LastAddressCleanup
	{
		get
		{
			using var Db = new CarrierDb( this );
			return Db.LastAddressCleanup;
		}

		set
		{
			using var Db = new CarrierDb( this );
			Db.LastAddressCleanup = value;
		}
	}

#region System
	[JsonIgnore]
	public override bool DisableReusedTripIds
	{
		get
		{
			using var Db = new CarrierDb( this );
			return Db.GetSystemPreferences().DisableReusedTripId;
		}
	}
#endregion

	public static IRequestContext InternalContext => GetContext( IRequestContext.INTERNAL_ID, IRequestContext.INTERNAL_ID );

	private string _IpAddress = null!;

	[JsonIgnore]
	private EPoints? EndPoints;

	public RequestContext()
	{
		NewContext ??= GetContext;
	}

	static RequestContext()
	{
		NewRequestContextImplementation = ( carrierId, userName ) =>
		                                  {
			                                  var Ctx = GetContext( carrierId, userName );

			                                  if( Ctx is RequestContext Context )
				                                  Context.GetEndPoint();

			                                  return Ctx;
		                                  };
	}

	public static IRequestContext GetContext( string carrierId, string userName ) => new RequestContext
	                                                                                 {
		                                                                                 CarrierId = carrierId,
		                                                                                 UserName  = userName
	                                                                                 };

	public static string ToIdsAuthToken( string carrierId, string userName, string password ) => $"{carrierId}-{userName}-{password}";

	public static string ToAuthToken( string carrierId, string userName, string password, int timeZoneOffset, bool debug, int userId, string ipAddress, int expiresInSeconds ) =>
		ToAuthToken( new RequestContext
		             {
			             CarrierId = carrierId,
			             UserName  = userName,
			             P         = password,
			             T         = (sbyte)timeZoneOffset,
			             D         = debug,
			             UserId    = userId,
			             IpAddress = ipAddress
		             },
		             expiresInSeconds );

	public string ToAuthToken() => Encryption.Encrypt( JsonConvert.SerializeObject( this ) );

	public static RequestContext? ToRequestContext( string encryptedJsonToken ) => FromAuthToken( encryptedJsonToken );

	public static RequestContext? ToRequestContextNotTimeLimited( string encryptedJsonToken )
	{
		try
		{
			return JsonConvert.DeserializeObject<RequestContext>( Encryption.Decrypt( encryptedJsonToken ) );
		}
		catch
		{
		}

		return null;
	}

	public static RequestContext ToRequestContext( string carrierId, string userName, string password, int timeZoneOffset, bool debug = false, string ipAddress = "Internal" ) =>
		new()
		{
			C         = carrierId,
			U         = userName,
			P         = password,
			T         = (sbyte)timeZoneOffset,
			D         = debug,
			IpAddress = ipAddress
		};

	public static string ToIdsAuthToken( string jsonToken )
	{
		var Token = ToRequestContext( jsonToken );

		return Token is null ? "" : ToIdsAuthToken( Token.C, Token.U, Token.P );
	}

	public override IRequestContext Clone( string carrierId = "" )
	{
		var NewCarrier = carrierId.IsNotNullOrWhiteSpace();

		var Result = new RequestContext
		             {
			             CarrierId      = NewCarrier ? carrierId : CarrierId,
			             EndPoints      = NewCarrier ? null : EndPoints,
			             Configuration  = Configuration,
			             IpAddress      = IpAddress,
			             UserId         = UserId,
			             Password       = Password,
			             UserName       = UserName,
			             Debug          = Debug,
			             IsIds          = IsIds,
			             IsNewCarrier   = NewCarrier,
			             TimeZoneOffset = TimeZoneOffset,
			             Variables      = new ConcurrentDictionary<string, dynamic>( Variables )
		             };
		Result.GetEndPoint();

		return Result;
	}


	public override void SystemLogException( string message )
	{
		new SystemLogException( this, message );
	}

	public override void SystemLogException( Exception ex, [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		if( !caller.IsNullOrWhiteSpace() )
		{
			SystemLogException( $"""
			                     Method: {caller}
			                     File name: {fileName}
			                     Line number: {lineNumber:D}
			                     """, ex );
		}
		else
			new SystemLogException( this, ex );
	}

	public override void SystemLogExceptionProgram( string programName, Exception ex, [CallerMemberName] string caller = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0 )
	{
		if( !caller.IsNullOrWhiteSpace() )
		{
			SystemLogException( $"""
			                     Program: {programName} 
			                     Method: {caller} 
			                     File name: {fileName} 
			                     Line number: {lineNumber:D}
			                     """, ex );
		}
		else
			new SystemLogException( this, ex );
	}

	public override void SystemLogException( string message, Exception ex )
	{
		new SystemLogException( this, message, ex );
	}

	public override void LogDifferences( string storageArea, string program, string description, object before, object after )
	{
		UserAuditTrail.LogDifferences( this, storageArea, program, description, before, after );
	}

	public override void LogDifferences( string storageArea, string program, string description, object before )
	{
		UserAuditTrail.LogDifferences( this, storageArea, program, description, before );
	}

	public override void LogDeleted( string storageArea, string program, string description, object value1 )
	{
		UserAuditTrail.LogDeleted( this, storageArea, program, description, value1 );
	}

	public override void LogNew( string storageArea, string program, string description, object value1 )
	{
		UserAuditTrail.LogNew( this, storageArea, program, description, value1 );
	}

	public override string GetUserStorageId( string userName )
	{
		using var Db = new CarrierDb( this );

		var Id = Db.GetStaffMemberInternalId( userName, STAFF_TYPE.CARRIER );

		return ToStorageId( userName, Id );
	}

	public override void LogProgramStarted( string program, string description = "" )
	{
		UserAuditTrail.LogProgram( this, "Program", program, AuditTrailEntryBase.OPERATION.STARTED, description );
	}

	public override void LogProgramEnded( string program, string description = "" )
	{
		UserAuditTrail.LogProgram( this, "Program", program, AuditTrailEntryBase.OPERATION.ENDED, description );
	}

	public override void SendEmail( IRequestContext context, string from, string to, string subject, string body )
	{
		Client.Send( context, to, subject, body );
	}

	public static string ToAuthToken( RequestContext context, int expiresInSeconds ) => Encryption.ToTimeLimitedToken( Encryption.Encrypt( JsonConvert.SerializeObject( context ) ), expiresInSeconds );

	private class EPoints
	{
		[JsonIgnore]
		public string StorageAccount = "";

		[JsonIgnore]
		public string StorageAccountKey = "";

		[JsonIgnore]
		public string StorageBlobEndpoint = "";

		[JsonIgnore]
		public string StorageFileEndpoint = "";

		[JsonIgnore]
		public string StorageTableEndpoint = "";
	}

	private EPoints GetEndPoint()
	{
		if( EndPoints is null )
		{
			var Ep = Users.GetEndPoints( this );

			if( Ep is not null )
			{
				return EndPoints = new EPoints
				                   {
					                   StorageAccount       = Ep.StorageAccount,
					                   StorageAccountKey    = Ep.StorageAccountKey,
					                   StorageTableEndpoint = Ep.StorageTableEndpoint,
					                   StorageBlobEndpoint  = Ep.StorageBlobEndpoint,
					                   StorageFileEndpoint  = Ep.StorageFileEndpoint
				                   };
			}

			return new EPoints();
		}

		return EndPoints;
	}

	private static RequestContext? FromAuthToken( string encryptedJsonToken )
	{
		try
		{
			var (Value, Ok, TimeError, Base64Error, Diff, Expires) = Encryption.FromTimeLimitedTokenDetailed( encryptedJsonToken );

			if( Ok )
				return JsonConvert.DeserializeObject<RequestContext?>( Encryption.Decrypt( Value ) );

			string Error;

			if( TimeError )
			{
				var Neg = Diff < 0 ? "-" : "";
				var T   = new TimeSpan( Math.Abs( Diff ) );
				Error = $"Time Error. Diff={Neg}{T.Days}.{T.Hours}/{T.Minutes}/{T.Seconds}.{T.Milliseconds} ({Diff})\r\nNow: {DateTimeOffset.UtcNow:G}\r\nExpires: {new DateTime( Expires ).ToUniversalTime():G}";
			}
			else if( Base64Error )
				Error = "Base 64 Error";
			else
				Error = "Could Not Decrypt";

			throw new SystemException( $"{Error} Auth Token: {encryptedJsonToken}" );
		}
		catch( Exception Exception )
		{
			throw new SystemException( $"Auth Token Exception: {Exception.Message}" );
		}
	}

#region Ids1
	public const string AZURE_USER           = "AzureWebService",
	                    IDS_USER             = "Ids1WebService",
	                    IDS_CORE_USER        = "IdsCoreWebService",
	                    WEB_SERVICE_PASSWORD = "8y7b25b798538793b98";

#region Send To Ids1
	[JsonIgnore]
	private bool HaveSendToIds1;

	[JsonIgnore]
	private bool _SendToIds1;

	private readonly object SendToIds1Lock = new();

	[JsonIgnore]
	public override bool SendToIds1
	{
		get
		{
			if( !HaveSendToIds1 )
			{
				lock( SendToIds1Lock )
				{
					HaveSendToIds1 = true;

					// Break SOAP loop
					if( !IsSpecialLogin )
					{
						using var Db = new CarrierDb( this );
						_SendToIds1 = Db.SendToIds1Preference();
					}
				}
			}
			return _SendToIds1;
		}
	}
#endregion

#region Send Accounts To Ids1
	[JsonIgnore]
	private bool HaveSendAccountsToIds1;

	[JsonIgnore]
	private bool _SendAccountsToIds1;

	[JsonIgnore]
	public override bool SendAccountsToIds1
	{
		get
		{
			if( !HaveSendAccountsToIds1 )
			{
				HaveSendAccountsToIds1 = true;

				// Break SOAP loop
				if( !IsSpecialLogin )
				{
					using var Db = new CarrierDb( this );
					_SendAccountsToIds1 = Db.SendAccountsToIds1Preference();
				}
			}
			return _SendAccountsToIds1;
		}
	}
#endregion

	[JsonIgnore]
	public override bool IsSpecialLogin => ( UserName.Compare( AZURE_USER, StringComparison.OrdinalIgnoreCase ) == 0 ) || ( UserName.Compare( IDS_USER, StringComparison.OrdinalIgnoreCase ) == 0 );
#endregion

#region PayPal
	[JsonIgnore]
	public override string PayPalClientId => Configuration.PayPalClientId;

	[JsonIgnore]
	public override string PayPalClientSecret => Configuration.PayPalClientSecret;
#endregion

#region Preferences
	private class CarrierPreferences : ICarrierServicePreferences
	{
		public bool   Import    { get; }
		public bool   Export    { get; }
		public string AuthToken { get; }
		public string CarrierId { get; }
		public bool   UseJbTest { get; }
		public bool   IsPml     { get; }

		public CarrierPreferences( IRequestContext context )
		{
			CarrierId = context.CarrierId;

			using var Db = new CarrierDb( context );

			var Prefs = Db.GetPreferences( true );

			foreach( var P in Prefs )
			{
				switch( P.DevicePreference.Trim() )
				{
				case CarrierDb.IMPORT:
					Import = P.Enabled;
					break;

				case CarrierDb.EXPORT:
					Export = P.Enabled;
					break;

				case CarrierDb.USE_JBTEST:
					UseJbTest = P.Enabled;
					break;

				case CarrierDb.PML:
				case CarrierDb.PML_TEST:
					IsPml = P.Enabled;
					break;
				}
			}

			AuthToken = $"{CarrierId}-{AZURE_USER}-{WEB_SERVICE_PASSWORD}";
		}
	}

	[JsonIgnore]
	private ICarrierServicePreferences? _CarrierServicePreferences;

	[JsonIgnore]
	public override ICarrierServicePreferences CarrierServicePreferences
	{
		get { return _CarrierServicePreferences ??= new CarrierPreferences( this ); }
	}
#endregion

#region Messaging
	[JsonIgnore]
	private BroadcastUpdates? _BroadcastUpdates;

	private BroadcastUpdates BroadcastUpdates
	{
		get { return _BroadcastUpdates ??= new BroadcastUpdates( this ); }
	}

	public override void Broadcast( TripUpdateMessage msg )
	{
		BroadcastUpdates.Broadcast( msg );
	}

	public override bool Broadcast( IEnumerable<TripUpdateMessage> msg ) => BroadcastUpdates.Broadcast( msg );

	public override TripUpdate Broadcast( TripUpdate t ) => BroadcastUpdates.Broadcast( t );
#endregion
}