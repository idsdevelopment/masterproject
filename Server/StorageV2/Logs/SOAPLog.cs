﻿// ReSharper disable InconsistentNaming

namespace StorageV2.Logs;

public static class Extension
{
	public static int ToInt( this string val )
	{
		if( !int.TryParse( val, out var Value ) )
			Value = 0;

		return Value;
	}
}

public class SOAPLog : ALogBase
{
	private const string SOAP_LOG = "SOAPLogs";

	public static string GetSOAPLogContents( string carrierId, string logType, string logName ) => GetContents( LOGS, carrierId, logType, logName );


	public static void Append( string carrierId, WebLog log )
	{
		carrierId = carrierId.TrimToLower();

		if( log.LogDateTime == DateTimeOffset.MinValue )
			log.LogDateTime = DateTimeOffset.UtcNow;

		log.Message = $"------- {log.LogDateTime:yyyy-MM-dd hh:mm:ss.fff zz} -------\r\n{log.Message.Trim()}\r\n";
		var Prog = log.Program.Pack();

		Append( log, dateTime => $"{carrierId}/{SOAP_LOG}/{Prog}/{StorageBase.ToDateTimePath( dateTime )}", false );
	}

	public static List<string> GetSOAPCarrierList() => GetCarrierList( LOGS );

	public static List<string> GetSOAPLogsList( string carrierId ) => GetLogList( LOGS, carrierId );

	public static List<string> SearchLogs( string carrierId, string logType, DateTimeOffset fromDate, DateTimeOffset toDate, string searchString )
	{
		return SearchLogs( LOGS, carrierId, logType, fromDate, toDate, searchString, logName =>
		                                                                             {
			                                                                             var Parts = logName.Replace( '_', ' ' ).Replace( '-', ' ' ).Replace( "tz", "" ).Split( new[] {' '}, StringSplitOptions.RemoveEmptyEntries );

			                                                                             var Year  = Parts[ 0 ].ToInt();
			                                                                             var Month = Parts[ 1 ].ToInt();
			                                                                             var Day   = Parts[ 2 ].ToInt();
			                                                                             var Hour  = Parts[ 4 ].ToInt();
			                                                                             var Zone  = Parts[ 3 ].ToInt();

			                                                                             return new DateTimeOffset( Year, Month, Day, Hour, 0, 0, TimeSpan.FromHours( Zone ) );
		                                                                             } );
	}
}