﻿namespace StorageV2.Logs;

public class ImportExportLog : ALogBase
{
	protected new static void Append( string program, string carrierId, string logBase, DateTimeOffset importDateTime, string importedData )
	{
		Tasks.RunVoid( () =>
		               {
			               carrierId = carrierId.TrimToLower();

			               var Message = $"""
			                              ------- {importDateTime:yyyy-MM-dd hh:mm:ss.fff zz} -------
			                              {importedData}

			                              """;

			               var Log = new WebLog
			                         {
				                         CarrierId   = carrierId,
				                         Program     = program,
				                         Message     = Message,
				                         BasePath    = "",
				                         LogDateTime = importDateTime
			                         };

			               Append( Log, dateTime => $"{carrierId}/{logBase}/{StorageBase.ToDateTimePath( dateTime )}", false );
		               } );
	}
}

public class ImportLog : ImportExportLog
{
	public static void Append( string program, string carrierId, DateTimeOffset importDateTime, string importedData )
	{
		Append( program, carrierId, "Import Logs", importDateTime, importedData );
	}
}

public class ExportLog : ImportExportLog
{
	public static void Append( string program, string carrierId, DateTimeOffset exportDateTime, string exportData )
	{
		Append( program, carrierId, "Export Logs", exportDateTime, exportData );
	}
}

public class ExportErrorLog : ImportExportLog
{
	public static void Append( string program, string carrierId, DateTimeOffset errorDateTime, string exportData, string errorData )
	{
		var Error = $"""
		             ------ Data Exported ------ 
		             {exportData}

		             ---------- Error ----------
		             {errorData}
		             """;

		Append( program, carrierId, "Export Error Logs", errorDateTime, Error );
	}
}