﻿using System.IO;

namespace StorageV2;

public class AppendBlobStorage : BlobStorageBase<AppendBlobClient>
{
	private bool InCreate;
	protected override AppendBlobClient GetBlobClient() => ContainerClient.GetAppendBlobClient( BlobName );

	protected override void ObjectNotFound( string objectName )
	{
		if( !InCreate )
		{
			InCreate = true;

			try
			{
				BlobClient.Create();
			}
			finally
			{
				InCreate = false;
			}
		}
	}

	public void Append( string message )
	{
		Append( new BinaryData( message ) );
	}

	public void Append( BinaryData content )
	{
		Append( content.ToStream() );
	}

	public void Append( Stream content )
	{
		Retry( () =>
		       {
			       content.Position = 0;
			       BlobClient.AppendBlock( content );
		       } );
	}

	public AppendBlobStorage( IRequestContext context, string containerName, string blobName, bool toIdsStorage = false )
		: base( context, containerName, blobName, toIdsStorage )
	{
	}

	protected AppendBlobStorage( IRequestContext context, string containerName, string path, string blobName, bool toIdsStorage = false )
		: this( context, containerName, StorageAccount.CombinePathAndName( path, blobName ), toIdsStorage )
	{
	}
}