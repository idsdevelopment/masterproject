﻿namespace StorageV2;

internal class AppendBlobTest
{
	private readonly IRequestContext Context;

	public void Execute( StorageTest test )
	{
		var Blob = new AppendBlobStorage( Context, Testing.TEST_CONTAINER, test.Name );

		try
		{
			switch( test.Action )
			{
			case StorageTest.ACTION.PUT:
				var Data = test.Data;

				if( Data.Count > 0 )
					Blob.Append( Data[ 0 ] );
				break;

			case StorageTest.ACTION.GET:
				var StringData = Blob.DownloadBlobAsString();
				test.Data = new List<string> {StringData};
				break;

			case StorageTest.ACTION.DELETE_BLOB:
				Blob.DeleteBlob();
				break;

			case StorageTest.ACTION.DELETE_CONTAINER:
				Blob.DeleteContainer( Testing.TEST_CONTAINER );
				break;

			case StorageTest.ACTION.PUT_METADATA:
				Blob.MetaData = test.MetaData;
				break;

			case StorageTest.ACTION.GET_METADATA:
				test.MetaData = new Dictionary<string, string>( Blob.MetaData );
				break;
			}
		}
		finally
		{
			test.Ok           = !Blob.Err;
			test.ErrorMessage = Blob.ErrorMessage;
		}
	}

	public AppendBlobTest( IRequestContext context )
	{
		Context = context;
	}
}