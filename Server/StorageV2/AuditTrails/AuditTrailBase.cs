﻿using System.Runtime.Serialization;
using System.Text;

namespace StorageV2.AuditTrails;

public class AuditTrailEntryBase : TableEntry
{
	public const string UNKNOWN = "Unknown";

	// ReSharper disable once InconsistentNaming

	public enum OPERATION
	{
		UNKNOWN,
		NEW,
		MODIFY,
		DELETE,
		FAILED,
		INFORM,
		SIGN_IN,
		SIGN_OUT,
		STARTED,
		ENDED,
		FINALISED
	}

	public string Operation { get; set; } = "";

	public string Description { get; set; } = "";

	public string Program { get; set; } = "";

	[IgnoreDataMember]
	public string CombinedData
	{
		get => Data + Data1 + Data2 + Data3 + Data4 + Data5 + Data6 + Data7 + Data8 + Data9
		       + Data10 + Data11 + Data12 + Data13 + Data14;
		set
		{
			var Ndx = 0;

			foreach( var Chunk in value.Chunks( 31_000, 15 ) ) // 2 bytes / char
			{
				switch( Ndx++ )
				{
				case 0:
					Data = Chunk;
					break;

				case 1:
					Data1 = Chunk;
					break;

				case 2:
					Data2 = Chunk;
					break;

				case 3:
					Data3 = Chunk;
					break;

				case 4:
					Data4 = Chunk;
					break;

				case 5:
					Data5 = Chunk;
					break;

				case 6:
					Data6 = Chunk;
					break;

				case 7:
					Data7 = Chunk;
					break;

				case 8:
					Data8 = Chunk;
					break;

				case 9:
					Data9 = Chunk;
					break;

				case 10:
					Data10 = Chunk;
					break;

				case 11:
					Data11 = Chunk;
					break;

				case 12:
					Data12 = Chunk;
					break;

				case 13:
					Data13 = Chunk;
					break;

				case 14:
					Data14 = Chunk;
					break;
				}
			}
		}
	}

	public string? Data
	{
		get => _Data;
		set => _Data = value ?? "";
	}

	public string? Data1
	{
		get => _Data1;
		set => _Data1 = value ?? "";
	}

	public string? Data2
	{
		get => _Data2;
		set => _Data2 = value ?? "";
	}

	public string? Data3
	{
		get => _Data3;
		set => _Data3 = value ?? "";
	}

	public string? Data4
	{
		get => _Data4;
		set => _Data4 = value ?? "";
	}

	public string? Data5
	{
		get => _Data5;
		set => _Data5 = value ?? "";
	}

	public string? Data6
	{
		get => _Data6;
		set => _Data6 = value ?? "";
	}

	public string? Data7
	{
		get => _Data7;
		set => _Data7 = value ?? "";
	}

	public string? Data8
	{
		get => _Data8;
		set => _Data8 = value ?? "";
	}

	public string? Data9
	{
		get => _Data9;
		set => _Data9 = value ?? "";
	}

	public string? Data10
	{
		get => _Data10;
		set => _Data10 = value ?? "";
	}

	public string? Data11
	{
		get => _Data11;
		set => _Data11 = value ?? "";
	}

	public string? Data12
	{
		get => _Data12;
		set => _Data12 = value ?? "";
	}

	public string? Data13
	{
		get => _Data13;
		set => _Data13 = value ?? "";
	}

	public string? Data14
	{
		get => _Data14;
		set => _Data14 = value ?? "";
	}

	public int TimeZoneOffset { get; set; }

	private string? _Data   = "",
	                _Data1  = "",
	                _Data2  = "",
	                _Data3  = "",
	                _Data4  = "",
	                _Data5  = "",
	                _Data6  = "",
	                _Data7  = "",
	                _Data8  = "",
	                _Data9  = "",
	                _Data10 = "",
	                _Data11 = "",
	                _Data12 = "",
	                _Data13 = "",
	                _Data14 = "";

	protected void XlateOp( OPERATION op )
	{
		Operation = op switch
		            {
			            OPERATION.NEW       => "NEW",
			            OPERATION.MODIFY    => "MODIFY",
			            OPERATION.DELETE    => "DELETE",
			            OPERATION.FAILED    => "FAILED",
			            OPERATION.SIGN_IN   => "SIGN IN",
			            OPERATION.SIGN_OUT  => "SIGN OUT",
			            OPERATION.INFORM    => "INFORM",
			            OPERATION.STARTED   => "STARTED",
			            OPERATION.ENDED     => "ENDED",
			            OPERATION.FINALISED => "FINALISED",
			            _                   => "??????"
		            };
	}

	protected void Init( OPERATION op, string description, string data )
	{
		if( Context is null )
			throw new Exception( "AuditTrailBase: No request context" );

		TimeZoneOffset = Context.TimeZoneOffset;

		Description  = description;
		CombinedData = data;

		XlateOp( op );
	}


	public AuditTrailEntryBase( IRequestContext? context ) : base( context )
	{
	}

	public AuditTrailEntryBase( IRequestContext context, string partitionKey, string rowKey ) : base( context, partitionKey, rowKey )
	{
	}

	public AuditTrailEntryBase( IRequestContext context, OPERATION op, string description, string data )
		: base( context )
	{
		Init( op, description, data );
	}

	public AuditTrailEntryBase( IRequestContext context, string partitionKey, string rowKey, OPERATION op, string description, string data )
		: base( context, partitionKey, rowKey )
	{
		Init( op, description, data );
	}

	public AuditTrailEntryBase( IRequestContext context, OPERATION op, string program, string description, string data ) : this( context, op, description, data )
	{
		Program = program.IsNullOrWhiteSpace() ? UNKNOWN : program;
	}
}

public class AuditTrailBase : TableStorage
{
	protected static string MakeAuditTrailName( IRequestContext context, string prefix, string suffix )
	{
		var StorageName = new StringBuilder();

		foreach( var C in $"{prefix}AuditTrail{suffix}" )
		{
			if( C.IsAlphaNumeric() )
				StorageName.Append( C );
		}

		return MakeCarrierStorageName( context, StorageName.ToString() );
	}

	public static string DifferencesAsString( AuditTrailEntryBase.OPERATION op, List<PropertyDifferences> entries )
	{
		var Text = new StringBuilder();

		switch( op )
		{
		case AuditTrailEntryBase.OPERATION.NEW:
		case AuditTrailEntryBase.OPERATION.DELETE:
		case AuditTrailEntryBase.OPERATION.FINALISED:
			foreach( var Entry in entries )
				Text.Append( $"{Entry.ParentClass}{Entry.Info.Name}: {ValueToString( Entry.Value1 )}\r\n" );

			break;

		default:
			foreach( var Entry in entries )
				Text.Append( $"{Entry.ParentClass}{Entry.Info.Name}: {ValueToString( Entry.Value1 )} -> {ValueToString( Entry.Value2 )} \r\n" );

			break;
		}

		return Text.ToString().Trim();
	}

	public AuditTrailBase( IRequestContext context, string tableName, bool toIdsStorage = false ) : base( context, tableName, toIdsStorage )
	{
	}

	private static string ValueToString( object value )
	{
		const string NULL = "null";

		return value switch
		       {
			       null                        => NULL,
			       string Str when Str != NULL => $"\"{Str}\"",
			       DateTime Dt                 => Dt.ToString( "s" ),
			       DateTimeOffset Dto          => Dto.ToString( "s" ),
			       _                           => value.ToString() ?? ""
		       };
	}
}