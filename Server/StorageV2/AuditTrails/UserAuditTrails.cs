﻿namespace StorageV2.AuditTrails;

public class UserAuditTrailEntryEntry : AuditTrailEntryBase
{
	public UserAuditTrailEntryEntry() : base( null )
	{
	}

	public UserAuditTrailEntryEntry( IRequestContext context, OPERATION op, string program, string description, string data )
		: base( context, op, program, description, data )
	{
	}


	public UserAuditTrailEntryEntry( IRequestContext context, OPERATION op, string data )
		: base( context, op, "", data )
	{
	}
}

public class UserAuditTrail : AuditTrailBase
{
	public static void Add( string storageArea, UserAuditTrailEntryEntry entryEntry )
	{
		if( entryEntry.Context is { } Context )
		{
			var Ctx = Context.Clone();

			Tasks.RunVoid( () =>
			               {
				               // Primary audit trail
				               new UserAuditTrail( Ctx, storageArea, "" ).AddEntity( entryEntry );

				               // Primary who did the changes audit trail
				               new UserAuditTrail( Ctx, storageArea, entryEntry.Context!.UserStorageId ).AddEntity( entryEntry );
			               } );
		}
	}


	public static void LogProgram( IRequestContext context, string storageArea, string program, AuditTrailEntryBase.OPERATION op, string description = "" )
	{
		Add( storageArea, new UserAuditTrailEntryEntry( context, op, description ) { Program = program } );
	}

	public static void LogDifferences( IRequestContext context, string storageArea, string program, AuditTrailEntryBase.OPERATION op, string description, List<PropertyDifferences> entries )
	{
		var Ctx = context.Clone();

		Task.Run( () =>
		          {
			          Add( storageArea, new UserAuditTrailEntryEntry( Ctx, op, program, description, DifferencesAsString( op, entries ) ) { Program = program } );
		          } );
	}


	public static void LogDifferences( IRequestContext context, string storageArea, string program, string description, object value1, object value2 )
	{
		LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.MODIFY, description, Properties.GetDifferences( value1, value2 ) );
	}

	public static void LogDifferences( IRequestContext context, string storageArea, string program, string description, object value1 )
	{
		LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.NEW, description, Properties.GetValues( value1 ) );
	}

	public static void LogDeleted( IRequestContext context, string storageArea, string program, string description, object value1 )
	{
		LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.DELETE, description, Properties.GetValues( value1 ) );
	}

	public static void LogNew( IRequestContext context, string storageArea, string program, string description, object value1 )
	{
		LogDifferences( context, storageArea, program, AuditTrailEntryBase.OPERATION.NEW, description, Properties.GetValues( value1 ) );
	}

	public static Log GetLog( IRequestContext context, LogLookup lookup )
	{
		var Result = new Log();

		try
		{
			var FromDate = lookup.FromDate.ToMinRowKey();
			var ToDate   = lookup.ToDate.ToMaxRowKey();
			var ByUser   = lookup.Key.IsNotNullOrWhiteSpace();

			var Audit = new UserAuditTrail( context, context.GetStorageId( lookup.Log ), ByUser ? context.GetUserStorageId( lookup.Key ) : "" );

			Pageable<UserAuditTrailEntryEntry> Items;

			if( !ByUser )
				Items = Audit.Query<UserAuditTrailEntryEntry>( entity => ( string.Compare( entity.RowKey, FromDate, StringComparison.Ordinal ) >= 0 ) && ( string.Compare( entity.RowKey, ToDate, StringComparison.Ordinal ) <= 0 ) );
			else
			{
				Items = Audit.Query<UserAuditTrailEntryEntry>( entity => ( entity.PartitionKey == lookup.Key )
				                                                         && ( string.Compare( entity.RowKey, FromDate, StringComparison.Ordinal ) >= 0 ) && ( string.Compare( entity.RowKey, ToDate, StringComparison.Ordinal ) <= 0 ) );
			}

			Result.AddRange( Items.Select( item => new LogEntry
			                                       {
				                                       PartitionKey   = item.PartitionKey,
				                                       RowKey         = item.RowKey,
				                                       Timestamp      = item.Timestamp ?? DateTimeOffset.MinValue,
				                                       Operation      = item.Operation,
				                                       Program        = item.Program,
				                                       Description    = item.Description,
				                                       Data           = item.CombinedData,
				                                       TimeZoneOffset = item.TimeZoneOffset
			                                       } ) );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}

	public UserAuditTrail( IRequestContext context, string storageArea, string userName ) : base( context, MakeName( context, storageArea, userName ) )
	{
	}

	private static string MakeName( IRequestContext context, string storageArea, string userName )
	{
		var StorageArea = storageArea.IsNullOrWhiteSpace() ? "" : storageArea.Trim().Capitalise();
		var UserStorage = userName.IsNullOrWhiteSpace() ? StorageArea : $"{userName.Trim().Capitalise()}{StorageArea}";

		return MakeAuditTrailName( context, "User", UserStorage );
	}
}