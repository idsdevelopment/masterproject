﻿namespace StorageV2.AuditTrails;

public class TripAuditTrailEntry : AuditTrailEntryBase
{
	public TripAuditTrailEntry( IRequestContext context, string tripId, OPERATION op, string description, string data ) : base( context, tripId, MakeRowKey( context ), op, description, data )
	{
	}

	public TripAuditTrailEntry() : base( null )
	{
	}

	private static string MakeRowKey( IRequestContext context ) => $"{DateTime.UtcNow.ToRowKey()}--{context.UserName}";
}

public class TripAuditTrail : AuditTrailBase

{
	public static void Add( TripAuditTrailEntry entry )
	{
		var Ctx = entry.Context!.Clone();

		Tasks.RunVoid( () =>
		               {
			               var LockObject = SemaphoreQueue.SemaphoreQueueByTypeAndId<TripAuditTrailEntry>( Ctx.CarrierId.TrimToLower() );

			               try
			               {
				               new TripAuditTrail( Ctx, MakeName( Ctx ) ).AddEntity( entry );
			               }
			               finally
			               {
				               LockObject.Release();
			               }
		               } );
	}

	public static void LogDifferences( IRequestContext context, string program, string tripId, AuditTrailEntryBase.OPERATION op, List<PropertyDifferences> entries )
	{
		if( entries.Count > 0 )
		{
			var Ctx = context.Clone();

			Tasks.RunVoid( () =>
			               {
				               var Desc = op switch
				                          {
					                          AuditTrailEntryBase.OPERATION.DELETE    => "Delete trip",
					                          AuditTrailEntryBase.OPERATION.MODIFY    => "Modify trip",
					                          AuditTrailEntryBase.OPERATION.NEW       => "New trip",
					                          AuditTrailEntryBase.OPERATION.FINALISED => "Finalised trip",
					                          _                                       => "??????"
				                          };

				               Add( new TripAuditTrailEntry( Ctx, tripId, op, Desc, DifferencesAsString( op, entries ) )
				                    {
					                    Program = program
				                    } );
			               } );
		}
	}

	public static void LogNew( IRequestContext context, string program, string tripId, object value1 )
	{
		LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.NEW, Properties.GetValues( value1 ) );
	}

	public static void LogDifferences( IRequestContext context, string program, string tripId, object value1 )
	{
		LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.NEW, Properties.GetValues( value1 ) );
	}

	public static void LogDifferences( IRequestContext context, string program, string tripId, object value1, object value2 )
	{
		LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.MODIFY, Properties.GetDifferences( value1, value2 ) );
	}

	public static void LogDeleted( IRequestContext context, string program, string tripId, object value1 )
	{
		LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.DELETE, Properties.GetValues( value1 ) );
	}

	public static void LogFinalised( IRequestContext context, string program, string tripId, object value1 )
	{
		LogDifferences( context, program, tripId, AuditTrailEntryBase.OPERATION.FINALISED, Properties.GetValues( value1 ) );
	}

	public static Log GetLog( IRequestContext context, LogLookup lookup )
	{
		var Result = new Log();

		try
		{
			var Key = lookup.Key.Trim();

			if( !Key.IsNullOrWhiteSpace() )
			{
				var Audit = new TripAuditTrail( context, MakeName( context ) );

				Result.AddRange( from E in Audit.GetPartitionEntities<TripAuditTrailEntry>( Key )
				                 orderby E.Timestamp
				                 select new LogEntry
				                        {
					                        PartitionKey   = E.PartitionKey,
					                        RowKey         = E.RowKey,
					                        Timestamp      = E.Timestamp ?? DateTimeOffset.MinValue,
					                        Operation      = E.Operation,
					                        Program        = E.Program,
					                        Description    = E.Description,
					                        Data           = E.CombinedData,
					                        TimeZoneOffset = E.TimeZoneOffset
				                        } );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}

		return Result;
	}

	public TripAuditTrail( IRequestContext context, string tableName, bool toIdsStorage = false ) : base( context, tableName, toIdsStorage )
	{
	}

	private static string MakeName( IRequestContext context ) => MakeAuditTrailName( context, "Trip", "" );
}