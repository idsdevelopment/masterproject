﻿using System.IO;
using System.Text;

namespace StorageV2.Carrier;

/*
public class DriverLog : AppendBlobStorage
{
	public class GpsPoint
	{
		public DateTimeOffset DateTime;

		public double Latitude,
		              Longitude;

		public override string ToString() => $";{DateTime:O},{Latitude},{Longitude}";
	}

	public void Append( DateTimeOffset time, double latitude, double longitude, string program, string description, string logText )
	{
		try
		{
			var AppendData = $"{time:yyyy-MM-dd HH:mm:ss zzz}\t{latitude:N}\t{longitude:n}\t{program}\t{description}\t{logText}\r\n";

			// Don't use Blob.AppendText
			using var Stream = new MemoryStream( Encoding.UTF8.GetBytes( AppendData ) ) {Position = 0};
		}
		catch( Exception E )
		{
			Context.SystemLogException( E );
		}
	}

	public DriverLog( IRequestContext context, string prefix = "" ) : base( context, MakeCarrierStorageName( context, MakeLogName( prefix ) ), context.UserName.ToLower(), $"{DateTime.UtcNow:yyyy-MM-dd}.log" )
	{
	}

	private static string MakeLogName( string prefix )
	{
		if( prefix.IsNotNullOrWhiteSpace() )
			prefix = $"{prefix}-";

		return $"{prefix.TrimToLower()}DriverLogs";
	}
}
*/