﻿namespace StorageV2;

public class StorageAccount
{
	internal readonly string ConnectionString;

	protected readonly IRequestContext Context;


	public static async Task<StorageAccountResource?> CreateBlobStorageAccount( IRequestContext context )
	{
		try
		{
			var (_, ResourceGroup, Region) = await Resource.CreateResourceGroup( context );

			if( ResourceGroup is not null )
			{
				//first we need to define the StorageAccountCreateParameters
				var Sku        = new StorageSku( StorageSkuName.StandardGrs );
				var Kind       = StorageKind.Storage;
				var Parameters = new StorageAccountCreateOrUpdateContent( Sku, Kind, Region );

				//now we can create a storage account with defined account name and parameters
				if( ResourceGroup.GetStorageAccounts() is { } AccountCollection )
				{
					var                                  AccountName            = MakeStorageAccountName( context );
					ArmOperation<StorageAccountResource> AccountCreateOperation = await AccountCollection.CreateOrUpdateAsync( WaitUntil.Completed, AccountName, Parameters );

					if( AccountCreateOperation is { Value: { Data: not null } StorageAccount } )
						return StorageAccount;
				}
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( nameof( CreateBlobStorageAccount ), E );
		}
		return null;
	}

	public static string CombinePathAndName( string path, string name )
	{
		if( ( path != "" ) && !path.EndsWith( "/" ) )
			path += "/";

		return path + Uri.EscapeDataString( name );
	}

	public StorageAccount( IRequestContext context, bool toIdsStorage = false )
	{
		try
		{
			Context = context;

			string StorageAccount,
			       StorageKey;

			if( toIdsStorage )
			{
				var (Acnt, Key) = context.BlobStorageAccount;
				StorageAccount  = Acnt;
				StorageKey      = Key;
			}
			else
			{
				StorageAccount = context.StorageAccount;
				StorageKey     = context.StorageAccountKey;
			}

			ConnectionString = $"DefaultEndpointsProtocol=https;AccountName={StorageAccount};AccountKey={StorageKey}";
		}
		catch( Exception E )
		{
			Console.WriteLine( E );
			throw;
		}
	}

	private static string MakeStorageAccountName( IRequestContext context )
	{
		// Space in middle for  capitalisation
		var RetVal = $"{AzureSubscription.SITE_NAME} {context.CarrierId}".ToLower().ToAlphaNumeric();

		if( RetVal.Length > 24 )
			RetVal = RetVal.MaxLength( 24 );

		return RetVal;
	}
}