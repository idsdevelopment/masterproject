﻿using pdfcrowd;
using Timer = System.Timers.Timer;

namespace Pdf;

public sealed class Pdf
{
	private const string PDF_CROWD_USER_NAME = "IdsApp",
	                     PDF_CROWD_API_KEY   = "2630bdbfd68f99ff1aac02f0ae3c6e3a";

	private const int RATE_LIMIT_PER_MINUTE = 15,
	                  CONCURRENCY_LIMIT     = 1;


	private static readonly SemaphoreSlim Mutex     = new( CONCURRENCY_LIMIT ),
	                                      RateMutex = new( 1 );

	private static readonly HtmlToPdfClient Client = new( PDF_CROWD_USER_NAME, PDF_CROWD_API_KEY );

	private static readonly Timer RateTimer;


	static Pdf()
	{
		RateTimer = new Timer( ( 1 / ( (double)60 / RATE_LIMIT_PER_MINUTE ) ) * 1000 ) // Convert to Milliseconds
		            {
			            AutoReset = false
		            };

		RateTimer.Elapsed += ( _, _ ) =>
		                     {
			                     RateMutex.Release();
		                     };
	}


	public static byte[] HtmlToPdf( IRequestContext context, string html, Report.PAPER_SIZE paperSize, bool landscape )
	{
	#if NET7_0_OR_GREATER
		async Task<byte[]> Convert()
	#else
		byte[] Convert()
	#endif
		{
			try
			{
			#if NET7_0_OR_GREATER
				await Mutex.WaitAsync();
			#else
				Mutex.Wait();
			#endif

				try
				{
					Client.setPageSize( paperSize switch
					                    {
						                    Report.PAPER_SIZE.LETTER => "Letter",
						                    _                        => "A4"
					                    } );

					Client.setOrientation( landscape switch
					                       {
						                       true => "landscape",
						                       _    => "portrait"
					                       } );

					using var Stream = new MemoryStream();
					Client.convertStringToStream( html, Stream );
					return Stream.ToArray();
				}
				finally
				{
				#if NET7_0_OR_GREATER
					await RateMutex.WaitAsync();
				#else
					RateMutex.Wait();
				#endif
					RateTimer.Start();

					Mutex.Release();
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
			return Array.Empty<byte>();
		}

	#if NET7_0_OR_GREATER
		return Convert().Result;
	#else
		return Convert();
	#endif
	}
}