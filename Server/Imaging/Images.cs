﻿#nullable enable

using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace Imaging;

public class ImageStream : IDisposable
{
	private readonly Stream               Stream;
	private readonly System.Drawing.Image Image;

	internal ImageStream( Stream stream )
	{
		Stream          = stream;
		stream.Position = 0;
		Image           = System.Drawing.Image.FromStream( stream );
	}

	public void Dispose()
	{
		Image.Dispose();
		Stream.Dispose();
	}

	public static implicit operator System.Drawing.Image( ImageStream s ) => s.Image;
}

public sealed class GraphicsStream : ImageStream, IDisposable
{
	private readonly Graphics Graphics;
	private readonly Bitmap   Bitmap;

	internal GraphicsStream( Stream stream, int height, int width ) : base( stream )
	{
		Bitmap   = new Bitmap( width, height );
		Graphics = Graphics.FromImage( Bitmap );
	}

	public new void Dispose()
	{
		Graphics.Dispose();
		Bitmap.Dispose();
		base.Dispose();
	}

	public static implicit operator Graphics( GraphicsStream             s ) => s.Graphics;
	public static implicit operator Bitmap( GraphicsStream               s ) => s.Bitmap;
	public static implicit operator System.Drawing.Image( GraphicsStream s ) => (ImageStream)s;
}

public abstract class ImagesBase
{
	protected GraphicsStream? ToGraphicsStream( Protocol.Data.Image image, int height, int width )
	{
		try
		{
			return new GraphicsStream( new MemoryStream( image.Bytes ), height, width );
		}
		catch( Exception E )
		{
			Debug.WriteLine( E );
		}
		return null;
	}

	protected static byte[] ToBytes( GraphicsStream gStream )
	{
		using var Stream = new MemoryStream();
		var       Bitmap = (Bitmap)gStream;

		Bitmap.Save( Stream, ImageFormat.Jpeg );
		Stream.Position = 0;
		return Stream.ToArray();
	}
}

public sealed class Image : ImagesBase
{
	public void Resize( Protocol.Data.Image image, int height, int width )
	{
		if( ToGraphicsStream( image, height, width ) is { } GraphicsStream )
		{
			using( GraphicsStream )
			{
				System.Drawing.Image Photo   = GraphicsStream;
				Graphics             GrPhoto = GraphicsStream;
				GrPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

				GrPhoto.DrawImage( Photo,
				                   new Rectangle( 0, 0, height, width ),
				                   new Rectangle( 0, 0, Photo.Width, Photo.Height ),
				                   GraphicsUnit.Pixel );

				image.Bytes = ToBytes( GraphicsStream );
			}
		}
	}

	public void Resize( Images images, int height, int width )
	{
		foreach( var Image in images )
			Resize( Image, height, width );
	}
}