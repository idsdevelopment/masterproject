﻿#nullable enable

namespace Interfaces.Interfaces;

public interface IBroadcast
{
	public void Broadcast( TripUpdateMessage msg );
	public bool Broadcast( IEnumerable<TripUpdateMessage> msg );
	public TripUpdate Broadcast( TripUpdate t );
}