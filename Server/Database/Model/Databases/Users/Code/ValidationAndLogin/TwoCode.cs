﻿using Company = Protocol.Data.Company;
using ResellerCustomer = Database.Model.Databases.Users.ResellerCustomer;

namespace Database.Model.Database;

public static partial class Users
{
	public static string Take3( this string value ) => value.Substring( 0, Math.Min( 3, value.Length ) );

	public static bool CanFormInteger( this string value )
	{
		var Temp = new StringBuilder();

		foreach( var C in value )
		{
			if( C is >= '0' and <= '9' )
				Temp.Append( C );
		}

		return Temp.ToString().IsInteger();
	}

	public static (string CarrierId, string UserName, string AccountId, string Password, bool Enabled, bool Ok) GetResellerFromLoginCode( string loginCode )
	{
		var Retval = ( CarrierId: "", UserName: "", AccountId: "", Password: "", Enabled: false, Ok: false );

		using var Db = new UsersEntities();

	#if NET7_0_OR_GREATER
		var ResellerCustomers = Db.Entity.ResellerCustomers;
	#else
		var ResellerCustomers = Db.ResellerCustomers;
	#endif

		var Cust = ( from C in ResellerCustomers
		             where C.LoginCode == loginCode
		             select C ).FirstOrDefault();

		if( Cust is not null )
		{
			Retval.CarrierId = Cust.ResellerId;
			Retval.UserName  = Cust.UserName;
			Retval.Password  = Encryption.ToTimeLimitedToken( Cust.Password );
			Retval.AccountId = Cust.CustomerCode;
			Retval.Enabled   = Cust.Enabled;

			Retval.Ok = true;
		}

		return Retval;
	}

	public static bool HasResellerLoginCode( string loginCode )
	{
		using var Db = new UsersEntities();

	#if NET7_0_OR_GREATER
		var ResellerCustomers = Db.Entity.ResellerCustomers;
	#else
		var ResellerCustomers = Db.ResellerCustomers;
	#endif

		return ( from C in ResellerCustomers
		         where C.LoginCode == loginCode
		         select C ).FirstOrDefault().IsNotNull();
	}

	public static string GenerateResellerLoginCode( IRequestContext context, string firstName, string lastName, string addressLine1 )
	{
		try
		{
			firstName    = firstName.Trim();
			lastName     = lastName.Trim();
			addressLine1 = addressLine1.Trim();

			var F     = firstName.Take3();
			var L     = lastName.Take3();
			var Count = 0;
			var A     = "";

			using var Db = new UsersEntities();

		#if NET7_0_OR_GREATER
			var ResellerCustomers = Db.Entity.ResellerCustomers;
		#else
			var ResellerCustomers = Db.ResellerCustomers;
		#endif

			var First = true;

			while( true )
			{
				string Code;

				if( First )
				{
					First = false;
					Code  = firstName;
				}
				else
					Code = $"{F}{L}{A}";

				if( Count > 0 )
					Code += Count.ToString();

				var Rec = ( from R in ResellerCustomers
				            where R.LoginCode == Code
				            select R ).FirstOrDefault();

				if( Rec is null )
					return Code;

				// Try adding Address portion to Code
				if( string.IsNullOrEmpty( A ) )
				{
					var P = addressLine1.IndexOf( ' ' );

					if( P > 0 )
						A = addressLine1.Take3();

					if( A.CanFormInteger() ) // Skip Address Number
					{
					#if NET7_0_OR_GREATER
						A = addressLine1[ P.. ].Trim().Take3();
					#else
						A = addressLine1.Substring( P ).Trim().Take3();
					#endif

						if( string.IsNullOrEmpty( A ) )
							++Count;
					}
					else
						++Count;
				}
				else
					++Count;
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return "";
	}

	public static string GenerateResellerLoginCode( IRequestContext context, Company address )
	{
		string F,
		       L,
		       Name = address.CompanyName;

	#if NET8_0_OR_GREATER
		var CoName = Name.Split( ' ', StringSplitOptions.RemoveEmptyEntries );
	#else
		var CoName = Name.Split( [' '], StringSplitOptions.RemoveEmptyEntries );
	#endif
		var Len = CoName.Length;

		if( Len >= 2 )
		{
			F = CoName[ 0 ];
			L = CoName[ 1 ];
		}
		else if( CoName.Length >= 2 )
		{
			var Middle = CoName.Length / 2;

			F = Name.Substring( 0, Middle );
			L = Name.Substring( Middle );
		}
		else
		{
			F = address.CompanyName;
			L = new Random().Next( 100 ).ToString();
		}

		return GenerateResellerLoginCode( context, F, L, address.AddressLine1 );
	}

	public static (string Code, bool Ok) AddUpdateResellerLoginCode( IRequestContext context, AddUpdateCustomer company ) => AddUpdateResellerLoginCode( context, company.LoginEnabled, company.CustomerCode, company.SuggestedLoginCode, company.Company );

	public static (string Code, bool Ok) AddUpdateResellerLoginCode( IRequestContext context, bool loginEnabled, string customerCode, string suggestedCode, Company company )
	{
		var (PWord, POk) = Encryption.FromTimeLimitedToken( company.Password );

		if( POk )
		{
			try
			{
				string OriginalLoginCode;

				using var Db = new UsersEntities();

			#if NET7_0_OR_GREATER
				var ResellerCustomers = Db.Entity.ResellerCustomers;
			#else
				var ResellerCustomers = Db.ResellerCustomers;
			#endif

				var Rid = context.CarrierId;

				// Do modify via delete and add
				var Rec = ( from R in ResellerCustomers
				            where ( R.ResellerId == Rid ) && ( R.CustomerCode == customerCode )
				            select R ).FirstOrDefault();

				if( Rec is not null )
				{
					OriginalLoginCode = Rec.LoginCode;
					ResellerCustomers.Remove( Rec );
					Db.SaveChanges();
				}
				else
					OriginalLoginCode = "";

				var Code = suggestedCode;

				if( string.IsNullOrEmpty( Code ) || HasResellerLoginCode( Code ) )
					Code = GenerateResellerLoginCode( context, company ); // Code will be unique

				ResellerCustomers.Add( new ResellerCustomer
				                       {
					                       Enabled   = loginEnabled,
					                       LoginCode = Code,
					                       UserName  = company.UserName,
					                       Password  = PWord,

					                       ResellerId        = Rid,
					                       CustomerCode      = customerCode,
					                       OriginalLoginCode = OriginalLoginCode
				                       } );
				Db.SaveChanges();

				return ( Code, true );
			}
			catch( Exception E )
			{
				context.SystemLogException( E );
			}
		}
		return ( "", false );
	}

	public static bool DeleteResellerLoginCodeByCustomerCode( IRequestContext context, string resellerCustomerCode )
	{
		try
		{
			using var Db = new UsersEntities();

		#if NET7_0_OR_GREATER
			var ResellerCustomers = Db.Entity.ResellerCustomers;
		#else
			var ResellerCustomers = Db.ResellerCustomers;
		#endif

			var Rec = ( from Rc in ResellerCustomers
			            where ( Rc.ResellerId == context.CarrierId ) && ( Rc.CustomerCode == resellerCustomerCode )
			            select Rc ).FirstOrDefault();

			if( Rec is not null )
			{
				ResellerCustomers.Remove( Rec );
				Db.SaveChanges();

				return true;
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}

		return false;
	}

	public static bool DeleteAllResellerCustomers( IRequestContext context, string resellerId )
	{
		try
		{
			using var Db = new UsersEntities();

		#if NET7_0_OR_GREATER
			Db.Entity.Database.ExecuteSqlRaw( "DELETE FROM dbo.ResellerCustomers WHERE ResellerId = {0}", resellerId );
		#else
			Db.Database.ExecuteSqlCommand( "DELETE FROM dbo.ResellerCustomers WHERE ResellerId = {0}", resellerId );
		#endif

			return true;
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}
		return false;
	}
}