﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitScientific( FormulasParser.ScientificContext context )
		{
			var Num   = context.number();
			var Value = VisitNumber( Num );

			var Exp = context.EXPONENT();

			if( Exp is { } )
			{
				var Exponent = Exp.GetText().Trim();

				if( double.TryParse( $"1{Exponent}", out var Power ) )
					return (decimal)( (double)Value * Power );
			}

			if( context.MODULO() is { } )
				Value *= 0.01M;

			return Value;
		}
	}
}