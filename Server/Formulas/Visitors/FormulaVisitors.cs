﻿#nullable enable

using System.Collections.Generic;
using System.Globalization;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	public class Result
	{
		public  bool    IsString { get; private set; }
		private string  StringValue = "";
		private decimal DecimalValue;
		private bool    HasDecimal;

		public static implicit operator decimal( Result result )
		{
			if( result.HasDecimal )
				return result.DecimalValue;

			if( result.IsString )
			{
				if( decimal.TryParse( result.StringValue, out result.DecimalValue ) )
				{
					result.HasDecimal = true;

					return result.DecimalValue;
				}
			}

			return 0;
		}

		public static explicit operator double( Result result ) => (double)(decimal)result;

		public static implicit operator string( Result result ) => result.IsString ? result.StringValue : result.DecimalValue.ToString( CultureInfo.InvariantCulture );

		public static implicit operator Result( decimal value ) => new() { DecimalValue = value, HasDecimal = true };

		public static implicit operator Result( string value ) => new() { StringValue = value, IsString = true };
	}

	internal partial class FormulaVisitors : FormulasParserBaseVisitor<Result>
	{
		private readonly Dictionary<string, string> PropertyMap;
		private readonly object                     Record;

		private readonly Dictionary<string, Result> Variables = new();

		private bool HaveResult;

		internal FormulaVisitors( object record, Dictionary<string, string> propertyMap )
		{
			Record      = record;
			PropertyMap = propertyMap;
		}
	}
}