﻿#nullable enable

using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitLet( FormulasParser.LetContext context )
		{
			var Value = VisitConditionalExpression( context.conditionalExpression() );

			if( context.RESULT() is { } )
				HaveResult = true;
			else
			{
				var Id = context.IDENT().GetText().Trim();
				Variables[ Id ] = Value;
			}

			return Value;
		}
	}
}