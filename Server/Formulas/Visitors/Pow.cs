﻿#nullable enable

using System;
using Formulas.Grammar;

namespace Formulas.Visitors
{
	internal partial class FormulaVisitors
	{
		public override Result VisitPow( FormulasParser.PowContext context )
		{
			var Value = VisitUnaryMinus( context.unaryMinus() );

			var Pow = context.POW();

			if( Pow is { } )
			{
				var Power = VisitPow( context.pow() );

				try
				{
					return (decimal)Math.Pow( (double)Value, (double)Power );
				}
				catch
				{
					return 0;
				}
			}

			return Value;
		}
	}
}