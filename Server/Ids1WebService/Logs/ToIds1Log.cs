﻿using Newtonsoft.Json;
using StorageV2.Logs;

namespace Ids1WebService.Logs;

internal class ToIds1Log : WebLog
{
	private const string IDS1_LOG_NAME = "To_Ids1";

#if DEBUG
	private const string SERVER = "DEBUG_SERVER";
#else
	private const string SERVER = "LIVE_SERVER";
#endif

	private ToIds1Log( IRequestContext context, string functionName, object ids1Object )
	{
		try
		{
			BasePath  = IDS1_LOG_NAME;
			CarrierId = context.CarrierId;

			Program = $"{IDS1_LOG_NAME}/{SERVER}";

			var Now = DateTimeOffset.UtcNow;
			LogDateTime = Now;

			var Obj = ids1Object switch
			          {
				          string S => S,
				          not null => JsonConvert.SerializeObject( ids1Object, Formatting.Indented ),
				          _        => "NULL Object"
			          };

			var ObjectName = Obj.GetType().Name;
			Message = $".      [{Now.AsPacificStandardTime():s}]\r\nUser Name: {context.UserName}\r\nFunction: {functionName}\r\nObject Type: {ObjectName}\r\n{Obj}";
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	public static void Log( IRequestContext context, string functionName, object ids1Object )
	{
		var Cid = context.CarrierId.TrimToLower();

		var Ctx = context.Clone();

		Tasks.RunVoid( () =>
		               {
			               var LockObject = SemaphoreQueue.SemaphoreQueueByTypeAndId<ToIds1Log>( Cid );

			               try
			               {
				               var Log = new ToIds1Log( Ctx, functionName, ids1Object );
				               SOAPLog.Append( Cid, Log );
			               }
			               finally
			               {
				               LockObject.Release();
			               }
		               } );
	}
}