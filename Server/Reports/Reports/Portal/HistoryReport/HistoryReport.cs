﻿using Reports.Portal.HistoryReport;

namespace Reports;

internal sealed class HistoryReport : ReportsModule.Reports
{
	protected override string TemplateName => @"Portal\HistoryReport\HistoryReport.html";

	public HistoryReport( IRequestContext context, Report options ) : base( context, new HistoryReportDataSource( context, options ), options )
	{
	}
}