﻿// ReSharper disable InconsistentNaming

using Database.Model.Databases.Carrier;

namespace Reports.Reports.Standard.CallTakerStatistics;

internal sealed class CallTakerStatisticsDataSource : DataSource
{
    public sealed record Global(string FromDate, string ToDate);

    public sealed record CallTakerStatisticsItem(string Operator, string TotalJobs, string DailyAverage,
                                        string Before_9,
                                        string _9_10, string _10_11, string _11_12,
                                        string _12_1, string _1_2, string _2_3, string _3_4,
                                        string After_4)
    {
        public CallTakerStatisticsItem(string Operator, int TotalJobs, decimal DailyAverage,
                              int Before_9,
                              int _9_10, int _10_11, int _11_12,
                              int _12_1, int _1_2, int _2_3, int _3_4,
                              int After_4) : this(Operator, TotalJobs.ToString(), $"{DailyAverage:N2}",
                                                       Before_9.ToString(),
                                                       _9_10.ToString(),
                                                       _10_11.ToString(),
                                                       _11_12.ToString(),
                                                       _12_1.ToString(),
                                                       _1_2.ToString(),
                                                       _2_3.ToString(),
                                                       _3_4.ToString(),
                                                       After_4.ToString()
                                                     )
        {
        }
    }

    public CallTakerStatisticsDataSource(IRequestContext context, Report options)
    {
        try
        {
            var FromDate = options.DateTimeArg1.StartOfDay();
            var ToDate = options.DateTimeArg2.EndOfDay();

            Globals = new Global($"{FromDate.Date:D}", $"{ToDate.Date:D}");

            var ReportDays = Math.Abs((ToDate - FromDate).Days) + 1;

            using var Db = new CarrierDb(context);
            var E = Db.Entity;

            var Recs = (from T in E.Trips
                        where (T.CallTime >= FromDate) && (T.CallTime <= ToDate)
                        orderby T.CallTakerId
                        group T by T.CallTakerId
                         into Ct
                        select new { Operator = Ct.Key, Trips = Ct.ToList() }).ToList();

            int GrandTotalJobs = 0,
                GrandTotalBefore9 = 0,
                GrandTotal_9_10 = 0,
                GrandTotal_10_11 = 0,
                GrandTotal_11_12 = 0,
                GrandTotal_12_1 = 0,
                GrandTotal_1_2 = 0,
                GrandTotal_2_3 = 0,
                GrandTotal_3_4 = 0,
                GrandTotalAfter4 = 0;

            foreach (var Rec in Recs)
            {
                var Trips = Rec.Trips;
                var Jobs = Trips.Count;
                var DailyAvg = (decimal)Jobs / ReportDays;

                int Before9 = 0,
                    _9_10 = 0,
                    _10_11 = 0,
                    _11_12 = 0,
                    _12_1 = 0,
                    _1_2 = 0,
                    _2_3 = 0,
                    _3_4 = 0,
                    After4 = 0;

                foreach (var Trip in Trips)
                {
                    var Hour = Trip.CallTime.Hour;

                    switch (Hour)
                    {
                        case 9:
                            ++_9_10;
                            break;

                        case 10:
                            ++_10_11;
                            break;

                        case 11:
                            ++_11_12;
                            break;

                        case 12:
                            ++_12_1;
                            break;

                        case 13:
                            ++_1_2;
                            break;

                        case 14:
                            ++_2_3;
                            break;

                        case 15:
                            ++_3_4;
                            break;

                        default:
                            if (Hour < 9)
                                ++Before9;
                            else
                                ++After4;
                            break;
                    }
                }

                GrandTotalJobs += Jobs;
                GrandTotalBefore9 += Before9;
                GrandTotal_9_10 += _9_10;
                GrandTotal_10_11 += _10_11;
                GrandTotal_11_12 += _11_12;
                GrandTotal_12_1 += _12_1;
                GrandTotal_1_2 += _1_2;
                GrandTotal_2_3 += _2_3;
                GrandTotal_3_4 += _3_4;
                GrandTotalAfter4 += After4;

                Add(new CallTakerStatisticsItem(Rec.Operator, Jobs, DailyAvg,
                                        Before9,
                                        _9_10,
                                        _10_11,
                                        _11_12,
                                        _12_1,
                                        _1_2,
                                        _2_3,
                                        _3_4,
                                        After4
                                      ));
            }

            var GrandTotalDailyAvg = (decimal)GrandTotalJobs / ReportDays;

            Add(new CallTakerStatisticsItem("Totals", GrandTotalJobs, GrandTotalDailyAvg,
                                    GrandTotalBefore9,
                                    GrandTotal_9_10,
                                    GrandTotal_10_11,
                                    GrandTotal_11_12,
                                    GrandTotal_12_1,
                                    GrandTotal_1_2,
                                    GrandTotal_2_3,
                                    GrandTotal_3_4,
                                    GrandTotalAfter4
                                  ));
        }
        catch (Exception Exception)
        {
            context.SystemLogException(Exception);
        }
    }
}