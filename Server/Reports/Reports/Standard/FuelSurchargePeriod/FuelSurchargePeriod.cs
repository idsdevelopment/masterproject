﻿using Reports.Reports.Standard.FuelSurchargePeriod;
using Utils.Csv;

namespace Reports;

internal sealed class FuelSurchargePeriod : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\FuelSurchargePeriod\FuelSurchargePeriod.html"; 

	public FuelSurchargePeriod(IRequestContext context, Report options) : base(context, new FuelSurchargePeriodDataSource(context, options), options)
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (FuelSurchargePeriodDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "Fuel Surcharge Period Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            //AsDecimal

            Csv[4][0].AsString = "";
            var RowHeaders = Csv[5];
            RowHeaders[0].AsString = "Driver No";
            RowHeaders[1].AsString = "Driver Name";
            RowHeaders[2].AsString = "Total Shipments";
            RowHeaders[3].AsString = "Service Com'able";
            RowHeaders[4].AsString = "Service Comm %";
            RowHeaders[5].AsString = "Service Com Amt";
            RowHeaders[6].AsString = "Fuel Chg Com'able";
            RowHeaders[7].AsString = "Fuel Chg Comm %";
            RowHeaders[8].AsString = "Fuel Chg Com Amt";
            RowHeaders[9].AsString = "Total Comm";
            RowHeaders[10].AsString = "Comm Due";

            var i = 6;
            foreach (var source in Source)
            {
                var row = Csv[i];
                var s = (FuelSurchargePeriodDataSource.DriverLineItem)source;
                row[0].AsString = s.DriverNumber;
                row[1].AsString = $"{s.DriverFirstName} {s.DriverLastName}";
                row[2].AsString = s.TruckTotalJobs;
                row[3].AsString = s.TruckTotalServiceCommisionable;
                row[4].AsString = s.TruckTotalServiceComPercentage;
                row[5].AsString = s.TruckTotalServiceComAmount;
                row[6].AsString = s.TruckTotalFuelChargeCommissionable;
                row[7].AsString = s.TruckTotalFuelChargeComPercentage;
                row[8].AsString = s.TruckTotalFuelChargeComAmount;
                row[9].AsString = s.TruckTotalCom;
                row[10].AsString = s.TruckTotalCommissionDue;

                i++;
            }

            var TotalsRow = Csv[i];
            TotalsRow[0].AsString = "";
            TotalsRow[1].AsString = "Totals";
            TotalsRow[2].AsString = Globals.PeriodTotalJobs;
            TotalsRow[3].AsString = Globals.PeriodTotalServiceCommisionable;
            TotalsRow[4].AsString = Globals.PeriodTotalServiceComPercentage;
            TotalsRow[5].AsString = Globals.PeriodTotalServiceComAmount;
            TotalsRow[6].AsString = Globals.PeriodTotalFuelChargeCommissionable;
            TotalsRow[7].AsString = Globals.PeriodTotalFuelChargeComPercentage;
            TotalsRow[8].AsString = Globals.PeriodTotalFuelChargeComAmount;
            TotalsRow[9].AsString = Globals.PeriodTotalCom;
            TotalsRow[10].AsString = Globals.PeriodTotalCommissionDue;

        }

        return Csv.ToString();
    }
}