﻿using Reports.Reports.Standard.BulkInvoiceReport;

namespace Reports;

internal sealed class BulkInvoiceReport : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\InvoiceReport\BulkInvoiceReport.html";

	public BulkInvoiceReport( IRequestContext context, Report options ) : base( context, new BulkInvoiceReportDataSource( context, options ), options )
	{
	}
}