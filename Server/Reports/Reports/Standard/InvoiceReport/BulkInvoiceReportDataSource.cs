﻿// ReSharper disable InconsistentNaming

using System.Globalization;
using System.Net;
using Database.Model.Databases.Carrier;
using Signatures = StorageV2.Carrier.Signatures;


//using StorageV2.Carrier;


namespace Reports.Reports.Standard.BulkInvoiceReport;                                             

internal sealed class BulkInvoiceReportDataSource : DataSource
{
	public sealed record Global( string FromDate, string ToDate, string InvoiceNumber, string InvoiceDate, string Terms, string LastId,
	                             string CarrierName, string CarrierAddress, string CarrierCity, string CarrierPostal, string CarrierProvince, string CarrierTel, string CarrierFax, string CarrierEmail, string CarrierLogoString,
	                             string CompanyName, string CompanySuite, string CompanyAddress, string CompanyCity, string CompanyProv, string CompanyCountry, string CompanyPostal,
	                             string CompanyBillingName, string CompanyBillingSuite, string CompanyBillingAddress, string CompanyBillingCity, string CompanyBillingProv, string CompanyBillingCountry, string CompanyBillingPostal,
	                             string SubTotal, string GST, string Fuel, string Total );

	public sealed record InvoiceItem( string DelDate, string Driver, string Caller,
	                                  string ShipmentId, string Reference, string POD, string WaybillNumber,
	                                  string PUCompany, string PUAddress, string DELCompany, string DELAddress,
	                                  string Service, string Weight,
	                                  //string Charges, string Surcharges,
	                                  List<string> Charges, List<string> Surcharges,
	                                  string Taxes, string TotalCharge );

	public sealed record InvoiceWaybillItem( string ShipmentId, string PackageType, string ServiceLevel, string Reference, string Pieces,
	                                         string CallTime, string Other, //string Return, string MailDrop,
	                                         string Billing, string Driver, string PodRequired, string Weight, string Volume,
	                                         string PickupCompanyName, string PickupAddressAddressLine1,
	                                         string PickupAddressCity, string PickupAddressRegion, string PickupAddressCountry, string PickupAddressPostalCode,
	                                         string PickupContactName, string PickupContactPhone, string PickupNotes,
	                                         string DeliveryCompanyName, string DeliveryAddressAddressLine1,
	                                         string DeliveryAddressCity, string DeliveryAddressRegion, string DeliveryAddressCountry, string DeliveryAddressPostalCode,
	                                         string DeliveryContactName, string DeliveryContactPhone, string DeliveryNotes,
	                                         string POP, string POD, string PickupSignature, string DeliverySignature, string Disclaimer );

	public sealed record InvoiceReportItem( List<InvoiceItem> Invoices,
	                                        List<InvoiceWaybillItem> Waybills,
	                                        Global Total );


	public BulkInvoiceReportDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate = options.DateTimeArg1.Date.IsNotNull() ? options.DateTimeArg1.StartOfDay() : DateTime.Now;
			var ToDate   = options.DateTimeArg2.Date.IsNotNull() ? options.DateTimeArg2.EndOfDay() : DateTimeOffset.Now.EndOfDay();
			//.EndOfDay();
			//var SelectedInvoice = long.Parse(options.StringArg1);
			var BulkInvoices = options.StringArg1.Split( ',' ).ToList();

			foreach( var BI in BulkInvoices )
			{
				if( BI.IsNullOrEmpty() )
					continue;
				var       SelectedInvoice = long.Parse( BI );
				using var Db              = new CarrierDb( context );
				var       E               = Db.Entity;

				var Invoice = ( from T in E.Invoices
				                where T.InvoiceNumber == SelectedInvoice
				                orderby T.InvoiceFromDateTime
				                select T ).ToList().FirstOrDefault();

				var getinvoice       = new GetInvoices { BillingCustomerCode = Invoice?.BillingCompany ?? "Unknown Billing Company" };
				var invoicesandtrips = Db.GetInvoicesAndTrips( getinvoice );

				var Shipments = ( from T in invoicesandtrips
				                  where T.InvoiceNumber == SelectedInvoice
				                  orderby T.InvoiceDateTime
				                  select T.Trips ).ToList().FirstOrDefault();

				var Signatures = new Signatures( context );

			#region Logo
				string       LogoString;
				const string url = "http://www.internetdispatcher.com/banners/phoenix.jpg";

 #pragma warning disable SYSLIB0014
				using( var client = new WebClient() )
 #pragma warning restore SYSLIB0014
				{
					var dataBytes = client.DownloadData( new Uri( url ) );
					LogoString = Convert.ToBase64String( dataBytes );
				}
			#endregion

			#region Preferences - Carrier Info
				var Pref = Db.GetPreferences();

				string GetPreference( string description )
				{
					return ( from P in Pref
					         where P.Description.Contains( description )
					         select P.StringValue ).FirstOrDefault() ?? "";
				}

				var CarrierName    = GetPreference( "Company Name" );
				var CarrierSuite   = GetPreference( "Company Suite" );
				var CarrierAddress = GetPreference( "Company Street" );

				if( CarrierSuite.IsNotNullOrEmpty() )
					CarrierAddress = CarrierSuite + " - " + CarrierAddress;

				var CarrierCity     = GetPreference( "Company City" );
				var CarrierPostal   = GetPreference( "Company Prov" );
				var CarrierProvince = GetPreference( "Company Postal" );
				var CarrierTel      = GetPreference( "Company Phone" );
				var CarrierFax      = GetPreference( "Company Fax" );
				var CarrierEmail    = GetPreference( "Admin Email" );
				var Terms           = GetPreference( "Blurb at end" );

				var PODRequired = ( from P in Pref
				                    where P.Description.Contains( "Require Proof Of Delivery" )
				                    select P.Enabled ).FirstOrDefault();

				var Disclaimer = GetPreference( "Disclaimer" );

				// Billing type?
				// Invoices By delivered date i/o call date?
				// Invoices show addresses (2 lines per item)
				// Invoices Show due date in RED
				// Invoices trip count for single billing type?
				// show total, barcode on waybill 
			#endregion

			#region Company Info
				//Get billing and shipping address info to display for the 
				//account that placed the order

				//Get the account id, then it's primary company and its billing and shipping info
				var x              = invoicesandtrips[ 0 ].Trips[ 0 ].AccountId;
				var PrimaryCompany = Db.GetPrimaryCompanyByCustomerCode( x );

				var CompanyName    = PrimaryCompany.ShippingCompany.CompanyName;
				var CompanySuite   = PrimaryCompany.ShippingCompany.Suite;
				var CompanyAddress = PrimaryCompany.ShippingCompany.AddressLine1;
				var CompanyCity    = PrimaryCompany.ShippingCompany.City;
				var CompanyProv    = PrimaryCompany.ShippingCompany.Region;
				var CompanyCountry = PrimaryCompany.ShippingCompany.Country;
				var CompanyPostal  = PrimaryCompany.ShippingCompany.PostalCode;

				var CompanyBillingName    = PrimaryCompany.BillingCompany.CompanyName;
				var CompanyBillingSuite   = PrimaryCompany.BillingCompany.Suite;
				var CompanyBillingAddress = PrimaryCompany.BillingCompany.AddressLine1;
				var CompanyBillingCity    = PrimaryCompany.BillingCompany.City;
				var CompanyBillingProv    = PrimaryCompany.BillingCompany.Region;
				var CompanyBillingCountry = PrimaryCompany.BillingCompany.Country;
				var CompanyBillingPostal  = PrimaryCompany.BillingCompany.PostalCode;
			#endregion

			#region Charges
				// Generate the charge names applied, the values, the taxes and the total
				// Taxes and text fields will appear on invoice if the "ShowOnInvoice" option is selected in the charge wizard.
				// Unselecting it will still allow specific waybill charges and taxes to appear in other places in the invoice. 
				var AllCharges = Db.GetAllCharges();

				var _IncludeCharges = from AC in AllCharges
				                      let _Charge = AC.ChargeId
				                      where AC.DisplayOnInvoice
				                      select new
				                             {
					                             AC.ChargeId,
					                             Details = AC
				                             };
				var IncludeCharges = _IncludeCharges.ToDictionary( arg => arg.ChargeId, arg => arg.Details );

				var TotalFuel = 0.00m;

				//static (string Names, string Values, string TotalTaxString, decimal TotalTaxValue, decimal FuelTotal, decimal SubTotal, decimal ShipmentTotal, string WaybillCharge, decimal Other) 
				//static (List<string> Names, List<string> Values, string TotalTaxString, decimal TotalTaxValue, decimal FuelTotal, decimal SubTotal, decimal ShipmentTotal, string WaybillCharge, decimal Other)
				static (List<string> Names, List<string> Values, decimal FuelTotal, decimal ShipmentTotal, string WaybillCharge, decimal Other)
					BuildCharges( TripUpdate Shipment, Dictionary<string, Charge> IncludeCharges )
				{
					var ChargeNames   = new List<string>();
					var ChargeValues  = new List<string>();
					var FuelTotal     = 0.00m;
					var ShipmentTotal = Shipment.TotalAmount;
					// Does anything have to happen with these?
					//Shipment.DiscountAmount;
					//Shipment.TotalFixedAmount
					//Shipment.TotalPayrollAmount

					// Keep track of specific taxes/names for invoice total?
					// Not implemented as of June 2023 !TODO

					// Charges that appear on the Waybill (Phoenix - Standard?)
					var WaybillCharge = "";
					var Other         = 0m;

					ChargeNames.Add( "BASE" );
					ChargeValues.Add( Shipment.DeliveryAmount.ToString( "C" ) );

					var charges = Shipment.TripCharges;

					foreach( var C in charges )
					{
						// Pull out specific charge names for other fields in the invoice
						// For example: Phoenix uses waybill id and Other 
						// If these charges are marked as "ShowOnInvoice" in the charge wizard, they will also appear in the charges section
						if( C.ChargeId.Equals( "waybill", StringComparison.CurrentCultureIgnoreCase ) )
							WaybillCharge = C.Text;
						else if( C.ChargeId.Equals( "other", StringComparison.CurrentCultureIgnoreCase ) )
							Other = C.Value;

						// Only include non-zero charges to be displayed on invoice
						if( IncludeCharges.Keys.Contains( C.ChargeId, StringComparison.CurrentCultureIgnoreCase )
						    && ( C.Value != 0 ) )
						{
							ChargeValues.Add( C.Value.ToString( "C" ) );
							ChargeNames.Add( IncludeCharges[ C.ChargeId ].Label );

							// Accumulate Fuel Total (Not Saved) 
							if( IncludeCharges[ C.ChargeId ].IsFuelSurcharge )
								FuelTotal += C.Value;
						}
					}
					return ( ChargeNames, ChargeValues, FuelTotal, ShipmentTotal, WaybillCharge, Other );
				}
			#endregion

				var InvoiceItems = new List<InvoiceItem>();
				var WaybillItems = new List<InvoiceWaybillItem>();

				if( Shipments is not null )
				{
					foreach( var T in Shipments )
					{
						var charge = BuildCharges( T, IncludeCharges );
						TotalFuel += charge.FuelTotal;
						// Keep track of tax A vs tax B and their names?

						InvoiceItems.Add( new InvoiceItem(
						                                  $"{T.VerifiedTime:yyyy/MM/dd}",
						                                  T.Driver,
						                                  T.CallerName,
						                                  T.TripId,
						                                  T.Reference,
						                                  T.POD,
						                                  charge.WaybillCharge,
						                                  T.PickupCompanyName,
						                                  $"{T.PickupAddressAddressLine1}, {T.PickupAddressCity}",
						                                  T.DeliveryCompanyName,
						                                  $"{T.DeliveryAddressAddressLine1}, {T.DeliveryAddressCity}",
						                                  T.ServiceLevel,
						                                  T.Weight.ToString( "0.0" ),
						                                  charge.Names,
						                                  charge.Values,
						                                  T.TotalTaxAmount.ToString( "C" ),
						                                  charge.ShipmentTotal.ToString( "C" )
						                                 ) );

					#region Sigantures
						var Sigs = Signatures.GetSignatures( T.TripId );

						Signature? PickupSignature   = null,
						           DeliverySignature = null;

						foreach( var Signature in Sigs )
						{
							switch( Signature.Status )
							{
							case < STATUS.PICKED_UP when PickupSignature is null:
								PickupSignature = Signature;
								break;

							case STATUS.PICKED_UP:
								PickupSignature = Signature;
								break;

							default:
								DeliverySignature ??= Signature;
								break;
							}
						}

						string ConvertSig( Signature? sig )
						{
							return sig is not null ? new List<Signature> { sig }.ToPngBase64() : "";
						}
					#endregion

						WaybillItems.Add( new InvoiceWaybillItem( T.TripId,
						                                          T.PackageType,
						                                          T.ServiceLevel,
						                                          T.Reference,
						                                          T.Pieces.ToString( CultureInfo.InvariantCulture ),
						                                          $"{T.CallTime:yyyy/MM/dd hh:mm tt}",
						                                          // Return Trip - Not used.
						                                          // Mail Drop - Not used. 
						                                          charge.Other.ToString( "C" ),
						                                          T.AccountId, //shouldn't BillingAccountId be assigned? Came up blank 
						                                          T.Driver,
						                                          PODRequired ? "Yes" : "No",
						                                          T.Weight.ToString( "0" ),
						                                          T.Volume.ToString( "0" ),
						                                          T.PickupCompanyName,
						                                          T.PickupAddressAddressLine1,
						                                          T.PickupAddressCity,
						                                          T.PickupAddressRegion,
						                                          T.PickupAddressCountry,
						                                          T.PickupAddressPostalCode,
						                                          T.PickupContact,
						                                          T.PickupAddressPhone, // There are many phone numbers. List them all?
						                                          T.PickupNotes,
						                                          T.DeliveryCompanyName,
						                                          T.DeliveryAddressAddressLine1,
						                                          T.DeliveryAddressCity,
						                                          T.DeliveryAddressRegion,
						                                          T.DeliveryAddressCountry,
						                                          T.DeliveryAddressPostalCode,
						                                          T.DeliveryContact,
						                                          T.DeliveryAddressPhone, // There are many phone numbers. List them all?
						                                          T.DeliveryNotes,
						                                          T.POP,
						                                          T.POD,
						                                          ConvertSig( PickupSignature ),
						                                          ConvertSig( DeliverySignature ),
						                                          Disclaimer ) );
					}

					if( Invoice is not null )
					{
						var SubtotalDecimal = Invoice.TotalValue - Invoice.TotalTaxA - Invoice.TotalTaxB - TotalFuel;
						var TotalTax        = Invoice.TotalTaxA; // This is already saved on invoice
						// !TODO keep track of tax A and B and show both on invoice 
						var Total = Invoice.TotalValue;

						var Globals_ = new Global( $"{FromDate.Date:D}", $"{ToDate.Date:D}", $"{SelectedInvoice}", $"{Invoice.InvoiceDateTime.Date:yyyy/MM/dd}", Terms, Shipments[ Shipments.Count - 1 ].TripId,
						                           $"{CarrierName}", $"{CarrierAddress}", $"{CarrierCity}", $"{CarrierPostal}", $"{CarrierProvince}", $"{CarrierTel}", $"{CarrierFax}", $"{CarrierEmail}", $"{LogoString}",
						                           $"{CompanyName}", $"{CompanySuite}", $"{CompanyAddress}", $"{CompanyCity}", $"{CompanyProv}", $"{CompanyCountry}", $"{CompanyPostal}",
						                           $"{CompanyBillingName}", $"{CompanyBillingSuite}", $"{CompanyBillingAddress}", $"{CompanyBillingCity}", $"{CompanyBillingProv}", $"{CompanyBillingCountry}", $"{CompanyBillingPostal}",
						                           SubtotalDecimal.ToString( "C" ), TotalTax.ToString( "C" ), TotalFuel.ToString( "C" ), Total.ToString( "C" ) );

						Add( new InvoiceReportItem( InvoiceItems, WaybillItems, Globals_ ) );
					}
				}
			}
		}
		catch( Exception Exception )
		{
			// Add(new InvoiceReportItem(     
			//new List<InvoiceItem>() {
			//     new InvoiceItem("", "", "", "", "", "", "", "", "", "", "", "", "", new List<string>() {""}, new List<string>() { "" }, "", "") },
			//new List<InvoiceWaybillItem>() {
			//         new InvoiceWaybillItem("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")}));

			// Globals = new Global("", "", Exception.Message ?? "no message but there was an exception", options.StringArg1 ?? "There was no id ", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

			context.SystemLogException( Exception );
		}
	}
}