﻿// ReSharper disable InconsistentNaming

using Database.Model.Databases.Carrier;

namespace Reports.Reports.Standard.CallTakerDaily;                          

internal sealed class CallTakerDailyDataSource : DataSource
{
    public sealed record Global(string FromDate, string ToDate, string TotalJobs, string WebTotalJobs, string CSRTotalJobs, string Branch);

    public sealed record CallTakerDailyItem(string Operator, string TotalJobs)
    {
        public CallTakerDailyItem(string Operator, int TotalJobs) : this(Operator, TotalJobs.ToString())
        {
        }
    }

    public CallTakerDailyDataSource(IRequestContext context, Report options)
    {
        try
        {
            var FromDate = options.DateTimeArg1.Date;
            var ToDate = options.DateTimeArg2.EndOfDay();
            var Branch = "B"; // where is this stored?

            using var Db = new CarrierDb(context);
            var E = Db.Entity;

            var Recs = (from T in E.Trips
                        where (T.CallTime >= FromDate) && (T.CallTime <= ToDate)
                        orderby T.CallTakerId
                        group T by T.CallTakerId
                         into Ct
                        select new { Operator = Ct.Key, Trips = Ct.ToList() }).ToList();

            int GrandTotalJobs = 0,
                WebTotal = 0,
                CSRTotal = 0;
            foreach (var Rec in Recs)
            {
                var Trips = Rec.Trips;
                var Jobs = Trips.Count;

                GrandTotalJobs += Jobs;
                if (Rec.Operator.ToLower().Contains("portal") || Rec.Operator.ToLower().Contains("web"))
                {
                    WebTotal += Jobs;
                }
                else
                {
                    CSRTotal += Jobs;
                }

                Add(new CallTakerDailyItem(Rec.Operator, Jobs));
            }

            Add(new CallTakerDailyItem("Totals", GrandTotalJobs));
            Globals = new Global($"{FromDate.Date:D}", $"{ToDate.Date:D}", GrandTotalJobs.ToString(), WebTotal.ToString(), CSRTotal.ToString(), Branch);
        }
        catch (Exception Exception)
        {
            context.SystemLogException(Exception);
        }

    }

   

}