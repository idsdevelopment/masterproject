﻿using Database.Model.Databases.Carrier;

// ReSharper disable InconsistentNaming

namespace Reports.Reports.Standard.DriverManifest2;  

internal sealed class DriverArriveTimeReportDataSource : DataSource
{
	public sealed record Global( string FromDate, string ToDate );

	public sealed record Driver( string DriverFirstName, string DriverLastName, string Truck, string TotalCharges );

	public sealed record DriverLineItem( string ShipmentId,
	                                     string From = "", string To = "",
	                                     string Description = "", string Weight = "",
	                                     string PUArriveTime = "", string PUDepartTime = "", string PUTotalTime = "",
	                                     string DELArriveTime = "", string DELDepartTime = "", string DELTotalTime = "",
	                                     string Charges = "", string Reference = "" );

	public sealed record DriverManifestItem( Driver Driver,
	                                         List<DriverLineItem> Items );

	public DriverArriveTimeReportDataSource( IRequestContext context, Report options )
	{
		try
		{
			var FromDate      = options.DateTimeArg1.StartOfDay();
			var ToDate        = options.DateTimeArg2.EndOfDay();
			var SelectedTruck = options.StringArg1;

			Globals = new Global( $"{FromDate.Date:D}", $"{ToDate.Date:D}" );

			using var Db = new CarrierDb( context );

			// Get carrier name from Preferences (Fragile)

			// charges?
			decimal TotalChargesPerDriver;
			//

			var Staff = Db.GetStaff( STAFF_TYPE.CARRIER );

			var StaffIds = ( from S in Staff
			                 select S.StaffId.ToUpper() ).ToList();

			var Recs = ( from T in Db.SearchTripsByDelByStatus( FromDate, ToDate, STATUS.VERIFIED, STATUS.FINALISED )
			             let UDriver = T.Driver.ToUpper()
			             where StaffIds.Contains( UDriver )
			             orderby UDriver, T.PickupTime
			             group T by UDriver
			             into Ct
			             let Driver = Ct.Key
			             select new
			                    {
				                    Driver,
				                    StaffRec = Staff.FirstOrDefault( s => s.StaffId.Compare( Driver, StringComparison.OrdinalIgnoreCase ) == 0 ),
				                    Trips    = Ct.ToList()
			                    } ).ToList();

			var dateCutoff = new DateTimeOffset( 2000, 1, 1, 1, 1, 1, new TimeSpan( 1, 0, 0 ) );

			string CalcTime( DateTimeOffset? leave, DateTimeOffset arrive )
			{
				//return leave is not null && (leave != DateTimeOffset.MinValue) && (arrive != DateTimeOffset.MinValue)
				// Use a cutoff as there are many dates in Phoenix from 1753 which should return a N/A result

				// ReSharper disable once MergeSequentialChecks
				return leave is not null && ( leave > dateCutoff ) && ( arrive > dateCutoff )
					       ? $"{( leave - arrive ).Value.TotalMinutes:N1}"
					       : "N/A";
			}

			////static?
			//string BuildCharges(List<TripCharge> charges, string testtrip)
			//{
			//	// Look a json charges (Database.Model.Databases.MasterTemplate.Trip
			//	var cc = (testtrip.IsNullOrWhiteSpace() ? new List<TripCharge>() : JsonConvert.DeserializeObject<List<TripCharge>>(testtrip)) ?? new List<TripCharge>();
			//	var testcharges = "";
			//	var chargesTotal = 0.00m;
			//	foreach (var c in cc)
			//	{
			//		// check out what is inside it
			//		//charges = charges + ", " + c.ChargeId + ": " + c.Value.ToString();
			//		testcharges = testcharges + c.ChargeId + " " + c.Value.ToString() + " " + c.Text + "; ";
			//		chargesTotal += c.Value;
			//	}
			//	testcharges = testcharges + chargesTotal.ToString("0.00");

			//	// Look at Trip charges (Protocol.Data.Trip.TripCharges)
			//	var Result = new StringBuilder();
			//	//var Result = 0.00m;

			//	/// Charge for Driver Arrive Time Report will be Payroll/Commision 
			//	foreach (var C in charges)
			//		// This was just to see what is currently being stored in charges. 
			//		// The answer is not much. 
			//		Result.Append( $"{C.ChargeId} {C.Value} {C.Text}/ " );
			//	//Result += C.Value;

			//	TotalChargesPerDriver += chargesTotal;
			//	return Result.ToString(); //!= 0.00m ? Result.ToString("0.00") : "";
			//	//return testcharges;
			//}

			decimal SumCommCharges( decimal payroll )
			{
				TotalChargesPerDriver += payroll;
				return TotalChargesPerDriver;
			}

			static string ToTimeString( DateTimeOffset? dateTime )
			{
				return dateTime is not null ? $"{dateTime:yyyy/MM/dd hh:mm tt}" : "N/A";
			}

			foreach( var Rec in Recs )
			{
				if( ( SelectedTruck == "All" ) || ( Rec.Driver == SelectedTruck ) )
				{
					TotalChargesPerDriver = 0.00m;

					var ShipmentsItems = ( from T in Rec.Trips
					                       let Weight = $"{T.Weight:N0}"
					                       //let Charges = BuildCharges(T.TripCharges, testtrip)
					                       let Total = SumCommCharges( T.TotalPayrollAmount )
					                       select new DriverLineItem(
					                                                 T.TripId,
					                                                 $"{T.PickupCompanyName}, {T.PickupAddressCity}",
					                                                 $"{T.DeliveryCompanyName}, {T.DeliveryAddressCity}",
					                                                 T.PickupNotes,
					                                                 Weight,
					                                                 ToTimeString( T.PickupArriveTime ),
					                                                 ToTimeString( T.PickupTime ),
					                                                 CalcTime( T.PickupTime, T.PickupArriveTime ),
					                                                 ToTimeString( T.DeliveryArriveTime ),
					                                                 ToTimeString( T.VerifiedTime ),
					                                                 CalcTime( T.VerifiedTime, T.DeliveryArriveTime ),
					                                                 //T.TotalPayrollAmount.ToString("0.00"),
					                                                 T.TotalPayrollAmount.ToString( "C" ), // No matter the format chosen, it will not display trailing zeros to 2 decimal places! 
					                                                 //$"{1.23456789m:N2}",
					                                                 //Charges,
					                                                 T.Reference
					                                                )
					                     ).ToList();

					Add( new DriverManifestItem(
					                            new Driver( Rec.StaffRec.FirstName, Rec.StaffRec.LastName, Rec.Driver, TotalChargesPerDriver.ToString( "C" ) ),
					                            ShipmentsItems
					                           ) );
				}
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}