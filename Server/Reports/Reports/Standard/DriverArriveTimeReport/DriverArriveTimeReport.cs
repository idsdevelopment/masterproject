﻿using Reports.Reports.Standard.DriverManifest2;
using Utils.Csv;

namespace Reports;

internal sealed class DriverArriveTimeReport : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\DriverArriveTimeReport\DriverArriveTimeReport.html"; 

	public DriverArriveTimeReport(IRequestContext context, Report options) : base(context, new DriverArriveTimeReportDataSource(context, options), options)
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (DriverArriveTimeReportDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "Driver Arrive Time Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            var i = 4;
            foreach (var source in Source)
            {
                Csv[i][0].AsString = "";
                i++;

                var s = (DriverArriveTimeReportDataSource.DriverManifestItem)source;

                // Driver Info
                Csv[i][0].AsString = s.Driver.Truck;
                Csv[i][1].AsString = $"{s.Driver.DriverFirstName} {s.Driver.DriverLastName}";
                i++;

                // Headers
                var RowHeaders = Csv[i];
                RowHeaders[0].AsString = "Shipment Id";
                RowHeaders[1].AsString = "From";
                RowHeaders[2].AsString = "To";
                RowHeaders[3].AsString = "Description";
                RowHeaders[4].AsString = "Weight";
                RowHeaders[5].AsString = "PU Arrive Time";
                RowHeaders[6].AsString = "PU Depart Time";
                RowHeaders[7].AsString = "PU Total Time (min)";
                RowHeaders[8].AsString = "DEL Arrive Time";
                RowHeaders[9].AsString = "DEL Depart Time";
                RowHeaders[10].AsString = "Del Total Time (min)";
                RowHeaders[11].AsString = "Com'able Amount";
                RowHeaders[12].AsString = "PO";
                i++;

                // Data
                foreach (var da in s.Items) 
                {
                    var row = Csv[i];
                    row[0].AsString = da.ShipmentId;
                    row[1].AsString = da.From;
                    row[2].AsString = da.To;
                    row[3].AsString = da.Description;
                    row[4].AsString = da.Weight;
                    row[5].AsString = da.PUArriveTime;
                    row[6].AsString = da.PUDepartTime;
                    row[7].AsString = da.PUTotalTime;
                    row[8].AsString = da.DELArriveTime;
                    row[9].AsString = da.DELDepartTime;
                    row[10].AsString = da.DELTotalTime;
                    row[11].AsString = da.Charges;
                    row[12].AsString = da.Reference;

                    i++;
                }

                Csv[i][11].AsString = $"Total Commissionable \n Amount for {s.Driver.Truck}";
                Csv[i][12].AsString = s.Driver.TotalCharges;
                i++;
            }

        }

        return Csv.ToString();
    }
}