﻿

// ReSharper disable InconsistentNaming

using Database.Model.Databases.Carrier;

namespace Reports.Reports.Standard.CallTakerRevenue; 

internal sealed class CallTakerRevenueDataSource : DataSource 
{
    public sealed record Global(string FromDate, string ToDate, string Branch,
        string TotalJobs, string WebTotalJobs, string CSRTotalJobs,
        string TotalRevenue, string WebRevenue, string CSRRevenue);

    public sealed record CallTakerRevenueItem(string Operator, string TotalJobs)
    {
        public CallTakerRevenueItem(string Operator, int TotalJobs) : this(Operator, TotalJobs.ToString())
        {
        }
    }

    public CallTakerRevenueDataSource(IRequestContext context, Report options)
    {
        try
        {
            var FromDate = options.DateTimeArg1.StartOfDay();
            var ToDate = options.DateTimeArg2.EndOfDay();
            var Branch = "B"; // where is this stored?

            using var Db = new CarrierDb(context);
            var E = Db.Entity;

            //(string searchByTime, DateTimeOffset from, DateTimeOffset to,
            //                         STATUS statusFrom = STATUS.UNSET, STATUS statsTo = STATUS.UNKNOWN )
            //var Recs = (from T in E.Trips
            var Recs = (from T in Db.SearchTripsBy("CALL", FromDate, ToDate, STATUS.NEW, STATUS.FINALISED)
                        where (T.CallTime >= FromDate) && (T.CallTime <= ToDate)
                        orderby T.CallTakerId
                        group T by T.CallTakerId
                         into Ct
                        select new { Operator = Ct.Key, Trips = Ct.ToList() }).ToList();

            int GrandTotalJobs = 0,
                WebTotal = 0,
                CSRTotal = 0;

            // for charges in... revenue is everything EXCEPT taxes - confirm  
            decimal TotalRevenue = 0.00m;
            decimal WebRevenue = 0.00m;
            decimal CSRRevenue = 0.00m;
            //void SumRevenue(List<Database.Model.Databases.MasterTemplate.Trip> Trips, bool web)
            void SumRevenue(List<Trip> Trips, bool web)
            {
                foreach (var T in Trips)
                {
                    if (web)
                    {
                        WebRevenue += T.TotalFixedAmount;
                    }
                    else
                    {
                        CSRRevenue += T.TotalFixedAmount;
                    }
                }
            }

            foreach (var Rec in Recs)
            {
                var Trips = Rec.Trips;
                var Jobs = Trips.Count;

                GrandTotalJobs += Jobs;
                if (Rec.Operator.ToLower().Contains("portal") || Rec.Operator.ToLower().Contains("web"))
                {
                    WebTotal += Jobs;
                    SumRevenue(Trips, true);
                }
                else
                {
                    CSRTotal += Jobs;
                    SumRevenue(Trips, false);
                }
                Add(new CallTakerRevenueItem(Rec.Operator, Jobs));
            }

            TotalRevenue = WebRevenue + CSRRevenue;
            Add(new CallTakerRevenueItem("Totals", GrandTotalJobs));
            Globals = new Global($"{FromDate.Date:D}", $"{ToDate.Date:D}", Branch,
                GrandTotalJobs.ToString(), WebTotal.ToString(), CSRTotal.ToString(),
                TotalRevenue.ToString("C"), WebRevenue.ToString("C"), CSRRevenue.ToString("C"));
        }
        catch (Exception Exception)
        {
            context.SystemLogException(Exception);
        }

    }
}