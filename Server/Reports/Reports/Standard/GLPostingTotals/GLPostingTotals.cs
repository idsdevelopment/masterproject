﻿using Reports.Reports.Standard.GLPostingTotals;
using Utils.Csv;

namespace Reports;

internal sealed class GLPostingTotals : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\GLPostingTotals\GLPostingTotals.html";
	public GLPostingTotals( IRequestContext context, Report options ) : base( context, new GLPostingTotalsDataSource( context, options ), options )
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (GLPostingTotalsDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "GL Posting Totals";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            //AsDecimal

            Csv[4][0].AsString = "";
            var RowHeaders = Csv[5];
            RowHeaders[0].AsString = "GL Code";
            RowHeaders[1].AsString = "GL Description";
            RowHeaders[2].AsString = "Debit";
            RowHeaders[3].AsString = "Credit";

            var i = 6;
            foreach (var source in Source)
            {
                var row = Csv[i];
                var s = (GLPostingTotalsDataSource.GLItem)source;
                row[0].AsString = s.GLCode;
                row[1].AsString = s.GLDescription;
                row[2].AsString = s.Debit;
                row[3].AsString = s.Credit;

                i++;
            }

            var TotalsRow = Csv[i];
            TotalsRow[0].AsString = "";
            TotalsRow[1].AsString = "Accounts Receivable Control Total";
            TotalsRow[2].AsString = Globals.AccountsReceivableControlTotal;
            i++;

            TotalsRow = Csv[i];
            TotalsRow[0].AsString = "";
            TotalsRow[2].AsString = Globals.AccountsReceivableControlTotal;
            TotalsRow[3].AsString = Globals.AccountsReceivableControlTotal;
            i++;

            TotalsRow = Csv[i];
            TotalsRow[0].AsString = "Adjustments";
            TotalsRow[1].AsString = Globals.Adjustments;
            i++;
            TotalsRow = Csv[i];
            TotalsRow[0].AsString = "Payments";
            TotalsRow[1].AsString = Globals.Payments;
            i++;
            TotalsRow = Csv[i];
            TotalsRow[0].AsString = "Invoices";
            TotalsRow[1].AsString = Globals.TotalInvoices;
        }

        return Csv.ToString();
    }
}