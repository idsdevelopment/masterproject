﻿using Reports.Reports.Standard.GeneralTripReport;
using Utils.Csv;

namespace Reports;

internal sealed class GeneralTripReport : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\GeneralTripReport\GeneralTripReport.html";

	public GeneralTripReport( IRequestContext context, Report options ) : base( context, new GeneralTripReportDataSource( context, options ), options )
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (GeneralTripReportDataSource.Global)Source.Globals;

            Csv[0][0].AsString = "General Shipment Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            //AsDecimal

            Csv[4][0].AsString = "";
            var RowHeaders = Csv[5];
            RowHeaders[0].AsString = "Shipment Id";
            RowHeaders[1].AsString = "Date";
            RowHeaders[2].AsString = "Driver";
            RowHeaders[3].AsString = "ST";
            RowHeaders[4].AsString = "Service";
            RowHeaders[5].AsString = "Package";
            RowHeaders[6].AsString = "Shipper";
            RowHeaders[7].AsString = "PU Co";
            RowHeaders[8].AsString = "PU Ad";
            RowHeaders[9].AsString = "REF #";
            RowHeaders[10].AsString = "DEL Co";
            RowHeaders[11].AsString = "DEL Ad";
            RowHeaders[12].AsString = "PCS";
            RowHeaders[13].AsString = "WT";
            RowHeaders[14].AsString = "CHRG";

            var i = 6;
            foreach (var source in Source)
            {
                var row = Csv[i];
                var s = (GeneralTripReportDataSource.GeneralTripReportItem)source;
                row[0].AsString = s.ShipmentId;
                row[1].AsString = s.Date;
                row[2].AsString = s.Driver;
                row[3].AsString = s.ST;
                row[4].AsString = s.Service;
                row[5].AsString = s.Package;
                row[6].AsString = s.Shipper;
                row[7].AsString = s.PUCompany;
                row[8].AsString = s.PUAddress;
                row[9].AsString = s.Reference;
                row[10].AsString = s.DELCompany;
                row[11].AsString = s.DELAddress;
                row[12].AsString = s.Pieces;
                row[13].AsString = s.Weight;
                row[14].AsString = s.Charge;

                i++;
            }

            var TotalsRow = Csv[i];
            TotalsRow[0].AsString = "Total Trips";
            TotalsRow[1].AsString = Globals.TotalShipments;
            i++;

            TotalsRow = Csv[i];
            TotalsRow[0].AsString = "Total Charges";
            TotalsRow[1].AsString = Globals.TotalCharge;

        }

        return Csv.ToString();
    }
}