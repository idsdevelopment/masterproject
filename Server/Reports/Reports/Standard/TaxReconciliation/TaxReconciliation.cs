﻿using Reports.Reports.Standard.TaxReconciliation;
using Utils.Csv;

namespace Reports;

internal sealed class TaxReconciliation : ReportsModule.Reports
{
	protected override string TemplateName => @"Standard\TaxReconciliation\TaxReconciliation.html";
	public TaxReconciliation( IRequestContext context, Report options ) : base( context, new TaxReconciliationDataSource( context, options ), options )
	{
	}

    public override string ToCsv()
    {
        var Csv = new Csv();
        if (Data.Source is { } Source)
        {
            var Globals = (TaxReconciliationDataSource.Global)Source.Globals;

            Csv[0][0].AsString = $"{Globals.CarrierName} Tax Reconciliation Report";
            Csv[1][0].AsString = "";

            Csv[2][0].AsString = "Dates Selected:";
            Csv[2][1].AsString = $"{Globals.FromDate} - {Globals.ToDate}";
            Csv[3][0].AsString = "Printed On:";
            Csv[3][1].AsString = $"{DateTime.Now:D}";

            ////AsDecimal?

            Csv[4][0].AsString = "";
            var RowHeaders = Csv[5];
            RowHeaders[0].AsString = "Invoice Id";
            RowHeaders[1].AsString = "Period End";
            RowHeaders[2].AsString = "Created";
            RowHeaders[3].AsString = "Amount(incl. Tax)";
			RowHeaders[4].AsString = "Taxes";


			var i = 6;
			foreach (var source in Source)
			{
				var row = Csv[i];
				var s = (TaxReconciliationDataSource.TaxReconPerAccountItem)source;

				var AccountSummary = s.TaxReconAccount;
				row[0].AsString = $"Account: {AccountSummary.AccountName}({AccountSummary.AccountId})";

				row[3].AsString = AccountSummary.ActTotAmtIncTax;
				row[4].AsString = AccountSummary.ActTotTax;

				i++;
				
				foreach (var invoice in s.Items)
				{
					row = Csv[i];
					row[0].AsString = invoice.InvoiceId;
					row[1].AsString = invoice.PeriodEnd;
					row[2].AsString = invoice.Created;
					row[3].AsString = invoice.AmtIncTax;
					row[4].AsString = invoice.Tax;

					i++;
				}

				Csv[i][0] = "";
				i++;
				
			}

			var TotalsRow = Csv[i];
			TotalsRow[0].AsString = "All Accounts Total:";
			TotalsRow[3].AsString = Globals.TotalIncTax;
			TotalsRow[4].AsString = Globals.TotalTax;

		}

		return Csv.ToString();
    }
}