﻿namespace Reports.Pml;

// ReSharper disable once InconsistentNaming
#pragma warning disable
public class eBolReport : ReportsModule.Reports
#pragma warning restore
{
	public             eBolDataSource? DataSource   => Data.Source as eBolDataSource;
	protected override string          TemplateName => @"_Customers\Pml\eBol.html";

	public eBolReport( IRequestContext context, PAPER_SIZES paperSize = PAPER_SIZES.A4, ORIENTATION orientation = ORIENTATION.PORTRAIT ) : base( context, new eBolDataSource(), paperSize, orientation )
	{
	}


	public static void PML_eBOL( IRequestContext context, string tripId, string emailAddresses = "" )
	{
	}

/*
		public static async void PML_eBOL( IRequestContext context, string tripId, string emailAddresses = "" )
		{


			try
			{
				using var Db = new CarrierDb( context );

				var Trip = Db.SearchTrips( new SearchTrips
				                           {
					                           ByTripId = true,
					                           TripId   = tripId
				                           } );

				if( Trip.Count > 0 )
				{
					var FromEmail = ( from P in Db.GetPreferences()
					                  where P.DevicePreference == CarrierDb.ADMIN_EMAIL
					                  select P.StringValue ).FirstOrDefault();

					if( FromEmail.IsNullOrWhiteSpace() )
						FromEmail = "NoReply@idsapp.com";

					var T = Trip[ 0 ];

					if( emailAddresses.IsNullOrWhiteSpace() )
					{
						emailAddresses = T.PickupAddressEmailAddress.Trim();

						if( emailAddresses.IsNullOrWhiteSpace() )
							return;
					}

					var Sigs = new Signatures( context ).GetSignatures( T.TripId );

					var EBol = new eBolReport( context );
					EBol.DataSource?.AddTrip( T, "\\bin\\Reports\\Pml\\PmlLogo.png", Sigs );
					var Html = EBol.Print();
					var Aurt = T.BillingNotes.ToAurtNumber();

					var SClient = new Client( context, FromEmail, emailAddresses, $"PML Returns {Aurt}" );
					await SClient.SendHtmlAsPdf( $"{Aurt}.pdf", Html );
				}
			}
			catch( Exception Exception )
			{
				context.SystemLogException( Exception );
			}
		}
*/
}