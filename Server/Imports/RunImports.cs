﻿using Database.Model.Database;
using Imports.Pml;

namespace Imports;

public class RunImportsAndExports
{
	private static bool InImport;

	public static async Task Execute()
	{
		if( !InImport )
		{
			InImport = true;

			try
			{
				List<AIClient> ClientsToRun = [];

				var CarriersToImport = Users.ImportExport.GetCarriersToImportExport();

				foreach( var (CarrierId, _, _, ImportEnabled, ExportEnabled) in CarriersToImport )
				{
					AIClient? Client = CarrierId.TrimToLower() switch
					                   {
					                   #if DEBUG
						                   "pml" => new PmlClient(),
					                   #else
						                   "pmlplay" => new PmlPlayClient(),
					                   #endif
						                   _ => null
					                   };

					if( Client is not null )
					{
						Client.ExportEnabled = ExportEnabled;
						Client.ImportEnabled = ImportEnabled;

						ClientsToRun.Add( Client );
					}
				}

				if( ClientsToRun.Count > 0 )
				{
					Task RunClient( AIClient client )
					{
						return Task.Run( async () =>
						                 {
							                 try
							                 {
								                 await client.Lock();

								                 if( client.ImportEnabled )
									                 await client.Import();

								                 if( client.ExportEnabled )
									                 await client.Export();

								                 client.NextRunUtc = DateTime.UtcNow.AddMinutes( client.RunIntervalInMinutes );
							                 }
							                 finally
							                 {
								                 client.Release();
							                 }
						                 } );
					}

					await Task.WhenAll( ClientsToRun.Select( RunClient ) );
				}
			}
			catch
			{
			}
			finally
			{
				InImport = false;
			}
		}
	}
}