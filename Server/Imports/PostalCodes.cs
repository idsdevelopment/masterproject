﻿namespace Imports;

internal class PostalCodes
{
	public static string FixRegion( string countryCode, string region )
	{
		switch( countryCode.TrimToUpper() )
		{
		case "AU":
			if( region.Contains( "New", StringComparison.OrdinalIgnoreCase ) || region.Contains( "NSW", StringComparison.OrdinalIgnoreCase ) )
				return "NSW";

			if( region.Contains( "Victoria", StringComparison.OrdinalIgnoreCase ) || region.Contains( "VIC", StringComparison.OrdinalIgnoreCase ) )
				return "VIC";

			if( region.Contains( "Capital", StringComparison.OrdinalIgnoreCase ) || region.Contains( "ACT", StringComparison.OrdinalIgnoreCase ) )
				return "ACT";

			if( region.Contains( "Northern", StringComparison.OrdinalIgnoreCase ) || region.Contains( "NT", StringComparison.OrdinalIgnoreCase ) )
				return "NT";

			if( region.Contains( "Queensland", StringComparison.OrdinalIgnoreCase ) || region.Contains( "QLD", StringComparison.OrdinalIgnoreCase ) )
				return "QLD";

			if( region.Contains( "South", StringComparison.OrdinalIgnoreCase ) || region.Contains( "SA", StringComparison.OrdinalIgnoreCase ) )
				return "SA";

			if( region.Contains( "Tasmania", StringComparison.OrdinalIgnoreCase ) || region.Contains( "TAS", StringComparison.OrdinalIgnoreCase ) )
				return "TAS";

			if( region.Contains( "Western", StringComparison.OrdinalIgnoreCase ) || region.Contains( "WA", StringComparison.OrdinalIgnoreCase ) )
				return "WA";
			break;
		}
		return region;
	}

	public static string FixAuRegion( string region ) => FixRegion( "AU", region );
}