﻿using DatabaseCore.Model.Carrier._Customers.Pml;
using Imports.Pml.Order.StatusUpdate;
using Imports.Pml.Order.Update;
using Imports.Pml.ReturnOrderAck;
using Imports.Pml.Schemas.RemoveOrderFromQueue;
using request = Imports.Pml.Order.Create.request;
using requestOrder = Imports.Pml.Order.Create.requestOrder;
using requestOrderProduct = Imports.Pml.Order.Create.requestOrderProduct;
using TripCharge = Protocol.Data.TripCharge;

// ReSharper disable IdentifierTypo

namespace Imports.Pml;

public class PmlClient : AIClient
{
	private const string PML              = "pml",
	                     RETURN           = "Return",
	                     AUSTRALIAN_STORE = "AustraliaStore";

	private const ushort UPLIFT_SELLER_ID = 50146;

	public PmlClient() : base( PML )
	{
	}

	protected PmlClient( string carrierId ) : base( carrierId )
	{
	}

	public void RecreateXrefFromXml( IRequestContext context, string xml )
	{
		if( xml.IsNotNullOrWhiteSpace() )
		{
			var Trips = xml.ToTrips();

			foreach( var Trip in Trips )
			{
				SetTripId( Trip );
				var TripItems = Trip.Packages.SelectMany( p => p.Items ).ToList();
				new CarrierDb.PmlSalesForce( Trip.TripId, Trip.StringData1, Trip.StringData2, Trip.StringData3, TripItems ).Save( context );
			}
		}
	}

#region Virtuals
#region Urls
	public virtual string ReturnOrderUrl             => Constants.RETURN_ORDER_URL;
	public virtual string ReturnOrderUpdateUrl       => Constants.RETURN_ORDER_UPDATE_URL;
	public virtual string ReturnOrderStatusUpdateUrl => Constants.RETURN_ORDER_STATUS_UPDATE_URL;
	public virtual string ReturnOrderAcceptanceUrl   => Constants.RETURN_ORDER_ACCEPTANCE_URL;
#endregion

#region Property Overrides
	public override AUTHORISATION           Authorisation    => ( Constants.CLIENT_ID, Constants.SECRET_CODE );
	public override string                  Account          => PML;
	public override string                  AccountTimeZone  => TimeZones.AUSTRALIAN_EASTERN;
	public override Trip.AUTO_DISPATCH_ZONE AutoDispatchZone => Trip.AUTO_DISPATCH_ZONE.ZONE_4;
#endregion

#region Company
	public override bool UpdatePickupCompany  => true;
	public override bool UpdateBillingCompany => true;
#endregion

	private async Task<string> SendXmlAndGetResponse( HttpRequestMessage message, string xml )
	{
		try
		{
			message.Content                     = new StringContent( xml );
			message.Content.Headers.ContentType = new MediaTypeHeaderValue( "application/xml" );

			var Response = await Client.SendAsync( message );
			return await Response.Content.ReadAsStringAsync();
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return "";
	}


	private async Task<(bool Ok, string DataSent, string Response)> SendXmlCheckSuccess( HttpRequestMessage message, string xml )
	{
		try
		{
			var Response = await SendXmlAndGetResponse( message, xml );
			return ( Response.FromXmlSuccessResponse() is { IsSuccess: true }, xml, Response );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
			return ( false, xml, $"Exception: {Exception.Message}" );
		}
	}

	private async Task<(bool Ok, string DataSent, string Response)> SendXmlCheckAccepted( HttpRequestMessage message, string xml )
	{
		try
		{
			var Response = await SendXmlAndGetResponse( message, xml );
			return ( Response.FromXmlOrderAcceptedResponse() is { IsSuccess: true }, xml, Response );
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
			return ( false, xml, $"Exception: {Exception.Message}" );
		}
	}


#region GetOrders
	public override Task<(bool Ok, string DataSent, string Response)> AcknowledgeTripImported( Trip trip ) => SendXmlCheckSuccess( NewDeleteMessageBase( ReturnOrderUrl ), new DeleteOrders( trip.ReceiptHandle ).ToXml() );

	public override async Task<TripList> ImportTrips( IRequestContext context )
	{
		try
		{
			var Message  = NewGetMessageBase( ReturnOrderUrl );
			var Response = await Client.SendAsync( Message );
			var Content  = await Response.Content.ReadAsStringAsync();
			Content = WebUtility.HtmlDecode( Content ); // To Readable XML

			// Fix the XML
			const string XML_HEADER = "<?xml version='1.0' encoding='UTF-8'?>";

			Content = $"{XML_HEADER}{Content.Replace( XML_HEADER, "" )}";

			var Trips = Content.ToTrips();

			if( Trips.Count > 0 )
			{
				await using var Db = new CarrierDb( Context );

				var InventoryDictionary = Db.GetInventoryDictionary();

				foreach( var Trip in Trips )
				{
					SetTripId( Trip );

					var TripItems = Trip.Packages.SelectMany( p => p.Items ).ToList();

					foreach( var TripItem in TripItems )
					{
						// Fix the description
						if( InventoryDictionary.TryGetValue( TripItem.Barcode, out var Inventory ) )
							TripItem.Description = Inventory.Description;

						var Qty = Db.GetCartonQuantity( TripItem.Barcode );

						if( Qty is > 0 )
						{
							TripItem.IsCarton       = true;
							TripItem.CartonQuantity = Qty.Value;
						}
					}

					new CarrierDb.PmlSalesForce( Trip.TripId, Trip.StringData1, Trip.StringData2, Trip.StringData3, TripItems ).Save( Context );

					// Need to translate the pickup address account barcode to the actual location barcode.
					if( Trip.BoolData1 )
					{
						var LookupAccountId = Trip.PickupAddressBarcode;

						var Xref = Db.GetBarcodeFromAccountIdXref( LookupAccountId );

						Trip.PickupAddressBarcode = Xref is not null ? Xref.Id2 : "";

						if( Trip.PickupAddressBarcode.IsNotNullOrWhiteSpace() )
						{
							var (Ok, Company, _) = Db.GetCompanyByLocationBarcode( Trip.PickupAddressBarcode );

							if( Ok )
							{
								Trip.PickupCompanyId   = Company.CompanyId;
								Trip.PickupCompanyName = Company.CompanyName;
							}

							var PCo = Trip.PickupCompanyName.Trim(); // Match the old Mercury format

							if( !PCo.EndsWith( ')' ) )
								Trip.PickupCompanyName = $"{PCo} ({Trip.PickupAddressBarcode})";
						}
						else
						{
							var Cz            = Trip.CurrentZone;
							var NoAddressText = $"NO ACCOUNT ID: {LookupAccountId}";

							Trip.CurrentZone = Cz.IsNullOrWhiteSpace() ? NoAddressText : $"{NoAddressText}/{Cz}";
						}
					}

					Trip.TripCharges = [];

					void DoCharge( string chargeId, string chargeLabel, decimal fee )
					{
						if( fee != 0 )
						{
							var Charge = Db.GetCharge( chargeLabel );

							if( Charge is null )
							{
								Charge = new Charge
								         {
									         ChargeId   = chargeId,
									         Label      = chargeLabel,
									         ChargeType = Constants.REORDER_FEE_TYPE,

									         DisplayOnDriversScreen = true,
									         DisplayOnEntry         = true,
									         DisplayOnInvoice       = true,
									         DisplayOnWaybill       = true
								         };

								Db.AddUpdateCharges( [Charge] );
							}

							Trip.TripCharges.Add( new TripCharge
							                      {
								                      ChargeId = chargeId,
								                      Value    = fee,
								                      Quantity = 1,
								                      Text     = chargeLabel
							                      } );
						}
					}

					DoCharge( Constants.RESTOCKING_FEE_ID, Constants.RESTOCKING_FEE, Trip.RestockingFee );
					DoCharge( Constants.RETURN_ORDER_FEE_ID, Constants.RETURN_ORDER_FEE, Trip.ReturnOrderFee );

					// If it has cartons, we need to add the packets.
					var Packages = Trip.Packages; // Deserialise the packages from JSON

					var ToAdd = new Dictionary<string, TripItem>();

					foreach( var Package in Packages )
					{
						var Items = Package.Items;

						void AddItem( TripItem item )
						{
							var Barcode = item.Barcode;

							if( ToAdd.TryGetValue( Barcode, out var Existing ) )
								Existing.Original += item.Original;
							else
								ToAdd.Add( Barcode, item );
						}

						foreach( var Item in Items )
						{
							var Barcode = Item.Barcode;
							var (IsCarton, _, _, _, _) = Db.GetPacketFromCarton( Barcode );

							if( !IsCarton )
							{
								if( InventoryDictionary.TryGetValue( Barcode, out var Inventory ) )
									Item.Description = Inventory.Description;

								AddItem( Item );
							}
						}
					}

					var CartonsToAdd = new List<TripItem>();

					// Now add the cartons for the Droid to scan
					foreach( var Item in ToAdd.Values )
					{
						var (Ok, CartonCode, CartonDescription, CartonQuantity) = Db.GetFromCartonPacket( Item.Barcode );

						if( Ok && !ToAdd.ContainsKey( CartonCode ) )
						{
							var Pieces = Math.Truncate( Item.Original / CartonQuantity );

							var Carton = new TripItem
							             {
								             Barcode     = CartonCode,
								             ItemCode    = RETURN,
								             Description = CartonDescription,
								             Original    = Pieces,
								             Pieces      = Pieces,
								             Weight      = 1
							             };

							CartonsToAdd.Add( Carton );
						}
					}

					foreach( var Carton in CartonsToAdd )
						ToAdd.Add( Carton.Barcode, Carton );

					//Created the consolidated list of items
					var Itms = ( from I in ToAdd.Values
					             select I ).ToList();

					decimal TotalWeight = 0,
					        TotalPieces = 0;

					foreach( var Item in Itms )
					{
						TotalWeight += Item.Weight;
						TotalPieces += Item.Original;
					}

					Trip.Packages =
					[
						new TripPackage
						{
							PackageType = $"{Constants.SERVICE_LEVEL} ({Trip.Reference})",
							Items       = Itms,
							Weight      = TotalWeight,
							Pieces      = TotalPieces
						}
					];
				}
			}
			return Trips;
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}
		return [];
	}
#endregion


#region GetDetails
	private static string GetOrderNumber( string tripId ) => tripId.Replace( "IMP", "" ).TrimStart( '0' );

	private static (string SellerId, string Storefront, string OrderNumber, List<TripItem> Items) GetOrderDetails( CarrierDb.PmlSalesForce xref, TripUpdate trip )
	{
		var OrderId = GetOrderNumber( trip.TripId );
		var (_, SellerId, Storefront, _, Items) = xref;
		return ( SellerId, Storefront, OrderId, Items );
	}
#endregion

#region Update Orders
	private async Task<(bool Ok, List<string> LogData, string LogDataReference)> FinaliseTrip( CarrierDb.PmlSalesForce xref, TripUpdate trip )
	{
		var ResultXml = new List<string>();

		var (SellerId, Storefront, OrderId, OriginalItems) = GetOrderDetails( xref, trip );

		await using var Db = new CarrierDb( Context );

		var AggregatedPackets = new Dictionary<string, TripItem>();

		var Packages = trip.Packages;

		// Sum the items  (Should only be packets)
		foreach( var Package in Packages )
		{
			foreach( var Item in Package.Items )
			{
				var Barcode = Item.Barcode;

				if( AggregatedPackets.TryGetValue( Barcode, out var Existing ) )
					Existing.Pieces += Item.Pieces;
				else
					AggregatedPackets[ Barcode ] = Item;
			}
		}

		var OriginalValues = ( from O in OriginalItems
		                       select O ).ToDictionary( o => o.Barcode, o => o.Original );

		var AggregatedItems = new List<TripItem>();

		// Rebuild the cartons and packets
		foreach( var (PacketCode, Item) in AggregatedPackets )
		{
			if( Item.Pieces > 0 )
			{
				Item.Original = OriginalValues.GetValueOrDefault( PacketCode, Item.Pieces );
				AggregatedItems.Add( Item ); // Add the packets if any
			}
		}

		// Find the items in the original order that are not in the new order
		var MissingItems = new List<TripItem>();

		foreach( var OriginalItem in OriginalItems )
		{
			var Barcode = OriginalItem.Barcode;
			var Found   = false;

			foreach( var AggregatedItem in AggregatedItems )
			{
				if( AggregatedItem.Barcode == Barcode )
				{
					Found = true;
					break;
				}
			}

			if( !Found )
			{
				MissingItems.Add( new TripItem
				                  {
					                  Barcode     = OriginalItem.Barcode,
					                  ItemCode    = RETURN,
					                  Description = OriginalItem.Description,
					                  Original    = OriginalItem.Original,
					                  Pieces      = 0,
					                  Weight      = 1
				                  } );
			}
		}

		// Add the missing items
		AggregatedItems.AddRange( MissingItems );

		var PackageType = $"{Constants.SERVICE_LEVEL} ({OrderId})";

		Packages =
		[
			new TripPackage
			{
				PackageType = PackageType,
				Items       = AggregatedItems,
				Weight      = AggregatedItems.Sum( i => i.Weight ),
				Pieces      = AggregatedItems.Sum( i => i.Pieces )
			}
		];

		trip.Packages = Packages;

		var Items = ( from I in AggregatedItems
		              select new Order.Update.requestOrderProduct
		                     {
			                     sku      = $"{SellerId}_{I.Barcode}",
			                     quantity = (ushort)I.Pieces
		                     } ).ToArray();

		var Update = new Order.Update.request
		             {
			             order = new Order.Update.requestOrder
			                     {
				                     sellerId   = ushort.Parse( SellerId ),
				                     storefront = Storefront,
				                     extOrderId = $"{OrderId}",
				                     products   = Items
			                     }
		             };

		var Xml = Update.ToXml();

		var Result = await SendXmlCheckAccepted( NewPostMessageBase( ReturnOrderUpdateUrl ), Xml );

		if( Result.Ok )
		{
			ResultXml.Add( Xml );

			static bool QuantitiesDifferent( Dictionary<string, decimal> originalQuantities, List<TripItem> agreatedItems )
			{
				foreach( var Item in agreatedItems )
				{
					if( originalQuantities.TryGetValue( Item.Barcode, out var Original ) && ( Original != Item.Pieces ) )
						return true;
				}
				return false;
			}

			static bool HasExtra( Dictionary<string, decimal> originalQuantities, List<TripItem> agreatedItems )
			{
				foreach( var Item in agreatedItems )
				{
					if( !originalQuantities.ContainsKey( Item.Barcode ) )
						return true;
				}
				return false;
			}

			bool Adjusted()
			{
				return QuantitiesDifferent( OriginalValues, AggregatedItems ) || HasExtra( OriginalValues, AggregatedItems );
			}

			if( Adjusted() )
			{
				Result = Result with
				         {
					         Ok = true
				         };
			}
			else
			{
				var Confirmed = await OrderConfirmed( SellerId, Storefront, OrderId );

				if( Confirmed.Ok )
				{
					ResultXml.Add( Confirmed.Xml );

					Result = Result with
					         {
						         Ok = true
					         };
				}
			}

			if( Result.Ok )
			{
				var (_, Ok) = Db.AddUpdateTrip( trip ); // Update the package details

				if( Ok && Db.FinaliseTrip( Constants.PML_EXPORT, trip.TripId ) )
				{
					CarrierDb.PmlSalesForce.Delete( Context, trip );
					return ( true, ResultXml, OrderId );
				}
			}
		}
		return ( false, [Result.DataSent], Result.Response );
	}

	private static bool IsImportedTrip( Trip trip ) => trip is { Status4: STATUS3.IMPORTED };

	public override async Task<(bool Ok, List<string> LogData, string LogDataReference, string ErrorData)> ExportTripStatus( IRequestContext context, TripExport export, Trip? trip )
	{
		if( trip is not null && IsImportedTrip( trip ) )
		{
			string SellerId, Storefront, OrderId;

			var TripId = trip.TripId;

			var PmlTrip = new PmlTripExport( export );

			var IsFutile = PmlTrip.IsFutile;

			if( !IsFutile && export.Status is STATUS.DELETED )
			{
				SellerId   = export.String2;
				Storefront = export.String3;
				OrderId    = GetOrderNumber( export.Code );
			}
			else
			{
				var Xref = CarrierDb.PmlSalesForce.Get( context, TripId );

				if( Xref is null )
					return ( true, [], "", "" );

				( SellerId, Storefront, OrderId, _ ) = GetOrderDetails( Xref, trip );
			}

			if( IsFutile || export.Status is STATUS.DELETED or STATUS.LIMBO )
			{
				var Result = await OrderRejected( SellerId, Storefront, OrderId );

				if( IsFutile )
				{
					await using var Db = new CarrierDb( context );
					Db.DeleteTripAndLog( Constants.PML_EXPORT, TripId, true );
				}

				return ( Result.Ok, [Result.Xml], OrderId, "" );
			}
		}
		return ( true, [], "", "" );
	}


	private static bool IsUplift( Trip trip ) => trip.ServiceLevel.StartsWith( "UP", StringComparison.OrdinalIgnoreCase );
	private static bool IsUpliftItem( TripItem item ) => item.ItemCode.TrimToUpper() is "UPLIFT" or "UPPACK";

	public override async Task<(bool Ok, List<string> LogData, string LogDataReference, string ErrorData)> ExportTrip( IRequestContext context, TripExport export, Trip? tripToExport )
	{
		if( tripToExport is not null )
		{
			tripToExport.Program = Constants.PML_EXPORT;

			if( IsUplift( tripToExport ) )
			{
				var AggregatedPackets = new Dictionary<string, TripItem>();

				await using var Db = new CarrierDb( context );

				foreach( var Package in tripToExport.Packages )
				{
					foreach( var Item in Package.Items )
					{
						if( ( Item.Pieces > 0 ) && !IsUpliftItem( Item ) ) // Remove zero cartons
						{
							var Barcode = Item.Barcode;

							var (Ok, _, Packet, PacketDescription, CartonQuantity) = Db.GetPacketFromCarton( Barcode );

							if( Ok )
							{
								Barcode          = Packet;
								Item.Barcode     = Packet;
								Item.Description = PacketDescription;
								Item.Pieces      = CartonQuantity * Item.Pieces; // Number of packets in the carton
							}

							if( AggregatedPackets.TryGetValue( Barcode, out var Existing ) )
								Existing.Pieces += Item.Pieces;
							else
								AggregatedPackets[ Barcode ] = Item;
						}
					}
				}

				if( AggregatedPackets.Count > 0 )
				{
					var Gln = Db.GetPmlGln( tripToExport.PickupAddressBarcode );

					var Items = ( from I in AggregatedPackets
					              select new requestOrderProduct
					                     {
						                     sku      = $"{Constants.UPLIFT_CUSTOMER_CODE}_{I.Key}",
						                     quantity = (ushort)I.Value.Pieces
					                     } ).ToArray();

					var Update = new request
					             {
						             order = new requestOrder
						                     {
							                     sellerId              = UPLIFT_SELLER_ID,
							                     storefront            = AUSTRALIAN_STORE,
							                     customerDistributorId = $"{UPLIFT_SELLER_ID}_{Gln}",
							                     extOrderNumber        = $"{tripToExport.TripId}",
							                     orderReason           = "Uplift",
							                     products              = Items
						                     }
					             };

					var Xml    = Update.ToXml();
					var Result = await SendXmlCheckAccepted( NewPostMessageBase( ReturnOrderAcceptanceUrl ), Xml );

					if( Result.Ok )
					{
						var TripId = tripToExport.TripId;

						var Trip = Db.GeTrip( new GetTrip
						                      {
							                      TripId     = TripId,
							                      Signatures = false
						                      } );

						if( Trip is not null )
						{
							decimal TotalPieces = 0,
							        TotalWeight = 0;

							var ToAdd = new List<TripItem>();

							foreach( var Item in AggregatedPackets.Values )
							{
								ToAdd.Add( Item );
								TotalPieces += Item.Pieces;
								TotalWeight += Item.Weight;
							}

							tripToExport.Packages =
							[
								new TripPackage
								{
									PackageType = $"{Constants.SERVICE_LEVEL} ({TripId})",
									Items       = ToAdd,
									Weight      = TotalWeight,
									Pieces      = TotalPieces
								}
							];

							Db.AddUpdateTrip( tripToExport );
						}

						Db.FinaliseTrip( Constants.PML_EXPORT, TripId, true );
						return ( true, [Xml], TripId, "" );
					}
				}
			}
			else if( IsImportedTrip( tripToExport ) )
			{
				switch( export.SubType )
				{
				case TripExport.TRIP_DATA_SUB_TYPES.UPDATE when export.Status is STATUS.PICKED_UP or STATUS.LIMBO:
					var Xref = CarrierDb.PmlSalesForce.Get( context, export.Code );

					if( Xref is not null )
					{
						var (Ok, LogData, LogDataReference) = await FinaliseTrip( Xref, tripToExport );
						return ( Ok, LogData, LogDataReference, "" );
					}
					return ( false, [], export.Code, "Missing GLN" );
				}
			}
		}
		return ( true, [], "", "" );
	}
#endregion

#region Status Updates
	private async Task<(bool Ok, string Xml, string Response)> UpdateStatus( ushort sellerId, string storefront, string orderNumber, string status )
	{
		var Update = new OrderUpdates
		             {
			             OrderStatus = new OrderUpdatesOrderStatus
			                           {
				                           SellerId    = sellerId,
				                           Storefront  = storefront,
				                           OrderNumber = orderNumber,
				                           Status      = status
			                           }
		             };
		var Xml      = Update.ToXml();
		var Response = await SendXmlCheckAccepted( NewPostMessageBase( ReturnOrderStatusUpdateUrl ), Xml );
		return ( Response.Ok, Xml, Response.Response );
	}

	private Task<(bool Ok, string Xml, string Response)> UpdateStatus( string sellerId, string storefront, string orderNumber, string status ) => UpdateStatus( ushort.Parse( sellerId ), storefront, orderNumber, status );

	private Task<(bool Ok, string Xml, string Response)> Reconcile( string sellerId, string storefront, string orderNumber, Constants.RECONCILING status )
	{
		try
		{
			return UpdateStatus( sellerId, storefront, orderNumber, status switch
			                                                        {
				                                                        Constants.RECONCILING.RECONCILING_CONFIRMED => Constants.RECONCILING_CONFIRMED,
				                                                        Constants.RECONCILING.RECONCILING_ADJUSTED  => Constants.RECONCILING_ADJUSTED,
				                                                        Constants.RECONCILING.RECONCILING_REJECTED  => Constants.RECONCILING_REJECTED,
				                                                        _                                           => throw new ArgumentOutOfRangeException( nameof( status ), status, null )
			                                                        } );
		}
		catch( Exception Exception )
		{
			var Message = $"""
			               SellerId: {sellerId}, Storefront: {storefront}, OrderNumber: {orderNumber}, Reconciling Status: {status}"
			               Exception: {Exception.Message}
			               """;
			return Task.FromResult( ( false, "", Message ) );
		}
	}

	private Task<(bool Ok, string Xml, string Response)> OrderRejected( string sellerId, string storefront, string orderNumber ) => Reconcile( sellerId, storefront, orderNumber, Constants.RECONCILING.RECONCILING_REJECTED );

	private Task<(bool Ok, string Xml, string Response)> OrderConfirmed( string sellerId, string storefront, string orderNumber ) => Reconcile( sellerId, storefront, orderNumber, Constants.RECONCILING.RECONCILING_CONFIRMED );
#endregion

#region Messages
	private HttpRequestMessage NewMessage( HttpMethod method, string uri )
	{
		var Auth = Authorisation;

		return new HttpRequestMessage
		       {
			       RequestUri = new Uri( uri ),
			       Method     = method,
			       Headers =
			       {
				       { Constants.CLIENT_ID_KEY, Auth.ClientId },
				       { Constants.SECRET_CODE_KEY, Auth.SecretCode },
				       { Constants.CHANNEL_KEY, Constants.CHANNEL_VALUE }
			       }
		       };
	}

	public HttpRequestMessage NewMessageBase( HttpMethod method, string uri ) => NewMessage( method, uri );
	public HttpRequestMessage NewGetMessageBase( string uri ) => NewMessageBase( HttpMethod.Get, uri );
	public HttpRequestMessage NewDeleteMessageBase( string uri ) => NewMessageBase( HttpMethod.Delete, uri );
	public HttpRequestMessage NewPostMessageBase( string uri ) => NewMessageBase( HttpMethod.Post, uri );
#endregion
#endregion
}