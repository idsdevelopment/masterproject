﻿namespace Imports.Pml;

public class PmlPlayClient : PmlTestClient
{
	private const   string PML_PLAY = "PmlPlay";
	public override string Account => PML_PLAY;

	public PmlPlayClient() : base( PML_PLAY )
	{
	}
}