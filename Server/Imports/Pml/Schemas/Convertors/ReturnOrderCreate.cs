﻿// ReSharper disable InconsistentNaming

namespace Imports.Pml.Order.Create;

public partial class request
{
	public string ToXml() => Xml.ConvertObjectToXml( this );
}