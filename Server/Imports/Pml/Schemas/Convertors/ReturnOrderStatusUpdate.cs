﻿namespace Imports.Pml.Order.StatusUpdate;

public partial class OrderUpdates
{
	public string ToXml() => Xml.ConvertObjectToXml( this );
}