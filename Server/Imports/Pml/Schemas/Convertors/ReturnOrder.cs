﻿using Imports.Pml.ReturnOrder;

namespace Imports.Pml.Order;

public static class Extensions
{
	internal const decimal RETURN_ORDER_FEE = -5_000;

	private static readonly TimeZoneInfo SydneyTimeZone = TimeZoneInfo.FindSystemTimeZoneById( "AUS Eastern Standard Time" );

	private static DateTimeOffset ConvertToSydneyTime( DateTime dateTime )
	{
		var SydneyTime = TimeZoneInfo.ConvertTime( dateTime, SydneyTimeZone );
		return new DateTimeOffset( SydneyTime, SydneyTimeZone.GetUtcOffset( SydneyTime ) );
	}


	public static responsedata? ToResponseData( this string xml ) => Xml.ConvertXmlStringToObject<responsedata>( xml );

	public static responsedataMessage[] ToResponseDataMessages( this string xml ) => xml.ToResponseData()?.messages ?? [];

	public static responsedataMessageBody ToMessageBody( this responsedataMessage message ) => message.Body;

	public static responsedataMessageBodyOrder ToOrder( this responsedataMessage message ) => message.ToMessageBody().Order;

	public static Trip ToTrip( this responsedataMessageBodyOrder o )
	{
		var B             = o.BillToAddressStructure;
		var S             = o.ShipToAddressStructure;
		var PickupCompany = o.AccountName.AlphaNumericAndSpaces( allowTabs: false, pack: true, trim: true );

		var OrderNumber = o.OrderNumber.ToString();
		var TripIdBase  = $"{o.OrderNumber:D10}";

		var BStreet = o.BillToAddressStructure.BillingStreet.Trim();
		var SStreet = o.ShipToAddressStructure.ShippingStreet.Trim();

		var NeedToConvert = string.Compare( BStreet, SStreet, StringComparison.OrdinalIgnoreCase ) == 0;
		var Barcode       = NeedToConvert ? o.PMISAPSoldToNumber : SStreet;

		var RestockingFee = o.RestockingFee < RETURN_ORDER_FEE; // Negative number

		return new Trip
		       {
			       BoolData1            = NeedToConvert,
			       PickupAddressBarcode = Barcode,
			       PickupAccountId      = o.AccountNumber,
			       ServiceLevel         = Constants.SERVICE_LEVEL,

			       StringData1 = o.SellerId.ToString(),
			       StringData2 = o.Storefront,
			       StringData3 = o.CustomerSellerID.Trim(),

			       TripIdBase   = TripIdBase,
			       Reference    = OrderNumber,
			       BillingNotes = $"{OrderNumber}",

			       Status1 = STATUS.ACTIVE,

			       ReturnOrderFee = o.ReturnOrderFee, // Should be a negative values
			       RestockingFee  = o.RestockingFee,
			       Status3        = RestockingFee ? STATUS2.DONT_SEND_TO_DRIVER : STATUS2.UNSET,
			       CurrentZone    = RestockingFee ? "RESTOCKING FEE" : "",

			       AccountId   = Constants.Accounts.PRIMARY_ACCOUNT,
			       CallTakerId = Constants.Accounts.CALLTAKER,

			       CallTime = ConvertToSydneyTime( o.OrderDate ),

			       PickupCompanyName         = PickupCompany,
			       PickupAddressAddressLine1 = S.ShippingStreet,
			       PickupAddressAddressLine2 = "",
			       PickupAddressCity         = S.ShippingCity,
			       PickupAddressRegion       = PostalCodes.FixAuRegion( S.ShippingStateCode ),
			       PickupAddressPostalCode   = S.ShippingPostalCode,
			       PickupAddressCountry      = S.ShippingCountry,
			       PickupAddressCountryCode  = S.ShippingCountryCode,
			       PickupNotes               = o.PickupInstructions is null ? "" : o.PickupInstructions.Trim(),

			       DeliveryCompanyName         = Constants.Warehouse.COMPANY_NAME,
			       DeliveryAddressAddressLine1 = Constants.Warehouse.STREET,
			       DeliveryAddressAddressLine2 = "",
			       DeliveryAddressCity         = Constants.Warehouse.CITY,
			       DeliveryAddressRegion       = PostalCodes.FixAuRegion( Constants.Warehouse.REGION_CODE ), // Just To Be Safe
			       DeliveryAddressPostalCode   = Constants.Warehouse.POSTAL_CODE,
			       DeliveryAddressCountry      = Constants.Warehouse.COUNTRY,
			       DeliveryAddressCountryCode  = Constants.Warehouse.COUNTRY_CODE,

			       BillingAddressAddressLine1 = B.BillingStreet,
			       BillingAddressAddressLine2 = "",
			       BillingAddressCity         = B.BillingCity,
			       BillingAddressRegion       = PostalCodes.FixAuRegion( B.BillingStateCode ),
			       BillingAddressPostalCode   = B.BillingPostalCode,
			       BillingAddressCountry      = B.BillingCountry,
			       BillingAddressCountryCode  = B.BillingCountryCode,

			       Packages =
			       [
				       new TripPackage
				       {
					       PackageType = $"{Constants.SERVICE_LEVEL} ({OrderNumber})",

					       Items = ( from I in o.OrderItems
					                 orderby I.SKU
					                 group new
					                       {
						                       Sku = I.POMCode.ToString(),
						                       I.SKUName,
						                       I.Quantity,
						                       I.SubAmount
					                       } by I.SKU
					                 into Skus
					                 let First = Skus.FirstOrDefault()
					                 where First is not null
					                 let Quantity = Skus.Sum( i => i.Quantity )
					                 select new TripItem
					                        {
						                        ItemCode = Constants.SERVICE_LEVEL,

						                        Barcode     = First.Sku,
						                        Description = First.SKUName,
						                        Reference   = OrderNumber,

						                        Value    = First.SubAmount,
						                        Original = Quantity,
						                        Weight   = 1
					                        } ).ToList()
				       }
			       ]
		       };
	}

	public static Trip ToTrip( this responsedataMessage message )
	{
		var Trip = message.ToOrder().ToTrip();
		Trip.MessageId     = message.MessageId;
		Trip.ReceiptHandle = message.ReceiptHandle;
		return Trip;
	}


	public static TripList ToTrips( this string xml )
	{
		var Messages = xml.ToResponseDataMessages();

		return new TripList( from M in Messages
		                     select M.ToTrip() )
		       {
			       LogData = xml
		       };
	}
}