﻿namespace Imports.Pml;

public class PmlTestClient : PmlClient
{
	private const   string PML_TEST = "PmlTest";
	public override string Account => PML_TEST;

#region Urls
	public override string ReturnOrderUrl             => Constants.TESTING_RETURN_ORDER_URL;
	public override string ReturnOrderUpdateUrl       => Constants.TESTING_RETURN_ORDER_UPDATE_URL;
	public override string ReturnOrderStatusUpdateUrl => Constants.TESTING_RETURN_ORDER_STATUS_UPDATE_URL;
	public override string ReturnOrderAcceptanceUrl   => Constants.TESTING_RETURN_ORDER_ACCEPTANCE_URL;
#endregion

	public override AUTHORISATION Authorisation => ( Constants.TESTING_CLIENT_ID, Constants.TESTING_SECRET_CODE );

	protected override HttpClient Client => SslNoCheckClient;

	public PmlTestClient() : base( PML_TEST )
	{
	}

	public PmlTestClient( string carrierId ) : base( carrierId )
	{
	}
}