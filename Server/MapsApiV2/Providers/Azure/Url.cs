﻿namespace MapsApiV2.Providers.Azure;

internal class Keys
{
	public static string Key
	{
		get { return _Key ??= Users.Configuration.AzureMapsApiKey; }
	}

	private static string? _Key;

#region Azure Key
	private static  string? _FormatAndKey;
	internal static string  FormatAndKey => _FormatAndKey ??= $"json?subscription-key={Key}&api-version=1.0";
#endregion
}

internal class Url
{
	private const   string AZURE_MAPS_BASE_URI = "https://atlas.microsoft.com";
	public static   string RouteUri             => _RouteUri ??= $"{AZURE_MAPS_BASE_URI}/route/directions/{Keys.FormatAndKey}";
	internal static string StructuredAddressUri => _StructuredAddressUri ??= $"{AddressSearchBaseUri}/structured/{Keys.FormatAndKey}";
	internal static string AddressSearchUri     => _AddressSearchUri ??= $"{AddressSearchBaseUri}/{Keys.FormatAndKey}";

	private static string? _AddressSearchBaseUri;


	private static string? _AddressSearchUri;

	private static string? _StructuredAddressUri;

	private static string? _RouteUri;
	private static string  AddressSearchBaseUri => _AddressSearchBaseUri ??= $"{AZURE_MAPS_BASE_URI}/search/address";
}