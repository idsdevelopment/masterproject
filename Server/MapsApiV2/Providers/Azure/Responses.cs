﻿// ReSharper disable InconsistentNaming

#pragma warning disable IDE1006 // Name violations

namespace MapsApiV2.Providers.Azure;

public class Responses
{
#region Datasources
	public sealed class DataSourcesGeometry
	{
		public string id { get; init; } = "";
	}

	public sealed class DataSources
	{
		public DataSourcesGeometry geometry { get; init; } = new();
	}
#endregion

#region Addresses
	public sealed class SearchResultAddress
	{
		public string buildingNumber              { get; init; } = "";
		public string country                     { get; init; } = "";
		public string countryCode                 { get; init; } = "";
		public string countryCodeISO3             { get; init; } = "";
		public string countrySecondarySubdivision { get; init; } = "";
		public string countrySubdivision          { get; init; } = "";
		public string countrySubdivisionName      { get; init; } = "";
		public string countryTertiarySubdivision  { get; init; } = "";
		public string crossStreet                 { get; init; } = "";
		public string extendedPostalCode          { get; init; } = "";
		public string freeformAddress             { get; init; } = "";
		public string localName                   { get; init; } = "";
		public string municipality                { get; init; } = "";
		public string municipalitySubdivision     { get; init; } = "";
		public string postalCode                  { get; init; } = "";
		public int[]  routeNumbers                { get; init; } = Array.Empty<int>();
		public string street                      { get; init; } = "";
		public string streetName                  { get; init; } = "";
		public string streetNameAndNumber         { get; init; } = "";
		public string streetNumber                { get; init; } = "";
	}

#region Position
	public class CoordinateAbbreviated
	{
		public decimal lat { get; init; }
		public decimal lon { get; init; }
	}

	internal sealed class SearchSummaryGeoBias : CoordinateAbbreviated;
#endregion

	public sealed class SearchResultViewport
	{
		public CoordinateAbbreviated btmRightPoint { get; init; } = new();
		public CoordinateAbbreviated topLeftPoint  { get; init; } = new();
	}

#region Search Results
	public class SearchResultEntryPoint
	{
		public CoordinateAbbreviated position { get; init; } = new();
		public string                type     { get; init; } = "";
	}

	public class SearchAddressResultBase : SearchResultEntryPoint
	{
		public SearchResultAddress      address     { get; init; } = new();
		public SearchResultEntryPoint[] entryPoints { get; init; } = Array.Empty<SearchResultEntryPoint>();
		public string                   id          { get; init; } = "";
		public decimal                  score       { get; init; }
		public SearchResultViewport     viewport    { get; init; } = new();
	}

	public sealed class SearchAddressResult : SearchAddressResultBase
	{
		public DataSources dataSources { get; init; } = new();
	}

	public sealed class SearchAddressStructuredResult : SearchAddressResultBase
	{
		public SearchResultAddressRanges addressRanges { get; init; } = new();
		public decimal                   dist          { get; init; }
	}
#endregion

#region Summary
	internal class SearchAddressSummary
	{
		public int                  fuzzyLevel   { get; init; }
		public SearchSummaryGeoBias geoBias      { get; init; } = new();
		public int                  limit        { get; init; }
		public int                  numResults   { get; init; }
		public int                  offset       { get; init; }
		public string               query        { get; init; } = "";
		public int                  queryTime    { get; init; }
		public string               queryType    { get; init; } = "";
		public int                  totalResults { get; init; }
	}

	internal sealed class SearchAddressStructuredSummary : SearchAddressSummary;
#endregion


	internal sealed class SearchAddressResponse
	{
		public SearchAddressResult[] results { get; init; } = Array.Empty<SearchAddressResult>();
		public SearchAddressSummary  summary { get; init; } = new();
	}

	public sealed class SearchResultAddressRanges
	{
		public CoordinateAbbreviated from      { get; init; } = new();
		public string                rangeLeft { get; init; } = "";
	}

	internal sealed class SearchAddressStructuredResponse
	{
		public SearchAddressStructuredResult[] results { get; init; } = Array.Empty<SearchAddressStructuredResult>();
		public SearchAddressStructuredSummary  summary { get; init; } = new();
	}
#endregion

#region Route
	public sealed class RouteOptimizedWaypoint
	{
		public int optimizedIndex { get; init; }
		public int providedIndex  { get; init; }
	}

	public sealed class Coordinate
	{
		public decimal latitude  { get; init; }
		public decimal longitude { get; init; }
	}

	public sealed class RouteResultInstruction
	{
		public string     combinedMessage           { get; init; } = "";
		public string     countryCode               { get; init; } = "";
		public string     drivingSide               { get; init; } = "";
		public string     exitNumber                { get; init; } = "";
		public string     instructionType           { get; init; } = "";
		public string     junctionType              { get; init; } = "";
		public string     maneuver                  { get; init; } = "";
		public string     message                   { get; set; }  = "";
		public Coordinate point                     { get; init; } = new();
		public int        pointIndex                { get; init; }
		public bool       possibleCombineWithNext   { get; init; }
		public string[]   roadNumbers               { get; init; } = Array.Empty<string>();
		public string     roundaboutExitNumber      { get; init; } = "";
		public int        routeOffsetInMeters       { get; init; }
		public string     signpostText              { get; init; } = "";
		public string     stateCode                 { get; init; } = "";
		public string     street                    { get; init; } = "";
		public int        travelTimeInSeconds       { get; init; }
		public int        turnAngleInDecimalDegrees { get; init; }
	}

	public sealed class RouteResultGuidance
	{
		public RouteResultInstructionGroup[] instructionGroups { get; init; } = Array.Empty<RouteResultInstructionGroup>();
		public RouteResultInstruction[]      instructions      { get; init; } = Array.Empty<RouteResultInstruction>();
	}

	public sealed class RouteResultInstructionGroup
	{
		public int    firstInstructionIndex { get; init; }
		public int    groupLengthInMeters   { get; init; }
		public string groupMessage          { get; init; } = "";
		public int    lastInstructionIndex  { get; init; }
	}

	public sealed class RouteResultLegSummary
	{
		public string  arrivalTime                             { get; init; } = "";
		public decimal batteryConsumptionInkWh                 { get; init; }
		public string  departureTime                           { get; init; } = "";
		public decimal fuelConsumptionInLiters                 { get; init; }
		public int     historicTrafficTravelTimeInSeconds      { get; init; }
		public int     lengthInMeters                          { get; init; }
		public int     liveTrafficIncidentsTravelTimeInSeconds { get; init; }
		public int     noTrafficTravelTimeInSeconds            { get; init; }
		public int     trafficDelayInSeconds                   { get; init; }
		public int     travelTimeInSeconds                     { get; init; }
	}

	public sealed class RouteResultLeg
	{
		public Coordinate[]          points  { get; init; } = Array.Empty<Coordinate>();
		public RouteResultLegSummary summary { get; init; } = new();
	}

	public sealed class RouteResultSectionTecCause
	{
		public int mainCauseCode { get; init; }
		public int subCauseCode  { get; init; }
	}

	public sealed class RouteResultSectionTec
	{
		public RouteResultSectionTecCause[] causes     { get; init; } = Array.Empty<RouteResultSectionTecCause>();
		public int                          effectCode { get; init; }
	}

	public sealed class RouteResultSection
	{
		public int                   delayInSeconds      { get; init; }
		public int                   effectiveSpeedInKmh { get; init; }
		public int                   endPointIndex       { get; init; }
		public string                magnitudeOfDelay    { get; init; } = "";
		public string                sectionType         { get; init; } = "";
		public string                simpleCategory      { get; init; } = "";
		public int                   startPointIndex     { get; init; }
		public RouteResultSectionTec tec                 { get; init; } = new();
		public string                travelMode          { get; init; } = "";
	}

	public sealed class RouteDirectionsSummary
	{
		public string arrivalTime           { get; init; } = "";
		public string departureTime         { get; init; } = "";
		public int    lengthInMeters        { get; init; }
		public int    trafficDelayInSeconds { get; init; }
		public int    travelTimeInSeconds   { get; init; }
	}

	public sealed class RouteDirectionsResult
	{
		public RouteResultGuidance    guidance { get; init; } = new();
		public RouteResultLeg[]       legs     { get; init; } = Array.Empty<RouteResultLeg>();
		public RouteResultSection[]   sections { get; init; } = Array.Empty<RouteResultSection>();
		public RouteDirectionsSummary summary  { get; init; } = new();
	}

	public sealed class RouteResponseReportEffectiveSetting
	{
		public string key   { get; init; } = "";
		public string value { get; init; } = "";
	}

	public sealed class RouteResponseReport
	{
		public RouteResponseReportEffectiveSetting effectiveSettings { get; init; } = new();
	}

	public sealed class RouteDirectionsResponse
	{
		public string                   formatVersion      { get; init; } = "";
		public RouteOptimizedWaypoint[] optimizedWaypoints { get; init; } = Array.Empty<RouteOptimizedWaypoint>();

		public RouteResponseReport report { get; init; } = new();

		public RouteDirectionsResult[] routes { get; init; } = Array.Empty<RouteDirectionsResult>();
	}
#endregion
}