﻿using Utils.Xlate;
using Leg = Protocol.Data.Maps.Route.Leg;
using Point = MapsApiV2.DataTypes.MapDataTypes.Point;
using Route = Protocol.Data.Maps.Route.Route;
using RouteSegment = MapsApiV2.DataTypes.MapDataTypes.RouteSegment;
using Step = Protocol.Data.Maps.Route.Step;

namespace MapsApiV2;

public static class Extensions
{
	public static List<RouteSegment> Round6( this List<RouteSegment> segments )
	{
		foreach( var Entry in segments )
		{
			var P = Entry.From?.Coordinate;

			if( P is not null )
			{
				P.Latitude  = Maths.Round6( P.Latitude );
				P.Longitude = Maths.Round6( P.Longitude );
			}
		}
		return segments;
	}

	public static AddressData ToAddressData( this MapAddress address, string companyName = "", string vicinity = "" )
	{
		var (Suite, StreetNumber, Street) = address.AddressLine1.SplitAddress();

		Street = Street.FixAbbreviation();

		return new AddressData( address.Latitude, address.Longitude,
		                        companyName,
		                        Suite, StreetNumber, Street, vicinity,
		                        address.City, address.Region, address.RegionCode,
		                        address.PostalCode, address.Country, "" );
	}

	public static AddressData ToAddressData( this Company company )
	{
		var (Suite, StreetNumber, Street) = company.AddressLine1.SplitAddress();

		if( company.Suite == "" )
			company.Suite = Suite;

		Street = Street.FixAbbreviation();

		return new AddressData( company.Latitude, company.Longitude,
		                        company.CompanyName,
		                        company.Suite, StreetNumber, Street, company.Vicinity,
		                        company.City, company.Region, company.RegionCode,
		                        company.PostalCode, company.Country, company.CountryCode );
	}


	public static MapAddress ToMapAddress( this AddressData address )
	{
		var AddressLine1 = new StringBuilder()
		                   .Append( address.Suite.Trim() ).Append( " " )
		                   .Append( address.StreetNumber.Trim() ).Append( " " )
		                   .Append( address.Street.Trim() ).ToString().TrimToUpperMax100();

		var Cord = address.Coordinate;

		return new MapAddress
		       {
			       Latitude     = Cord.Latitude,
			       Longitude    = Cord.Longitude,
			       AddressLine1 = AddressLine1,
			       AddressLine2 = "",
			       City         = address.City.TrimToUpperMax50(),
			       Region       = address.State.TrimToUpperMax100(),
			       RegionCode   = address.StateCode.TrimToUpperMax50(),
			       Country      = address.Country.TrimToUpperMax50(),
			       PostalCode   = address.PostalCode.Trim().Max50()
		       };
	}

	public static AddressData ToAddressData( this CompanyMapAddress address, Point point )
	{
		var (_, StreetNumber, Street) = address.AddressLine1.SplitAddress();

		Street = Street.FixAbbreviation();

		return new AddressData( point.Latitude, point.Longitude,
		                        address.CompanyName,
		                        address.Suite, StreetNumber, Street, "",
		                        address.City, address.Region, address.RegionCode,
		                        address.PostalCode, address.Country, "" );
	}

	public static CompanyMapAddress ToCompanyMapAddress( this AddressData address ) => new( address );

	public static async Task<Company> CheckCoordinate( this Company company, IRequestContext context )
	{
		if( company is { Latitude: 0, Longitude: 0 } )
		{
			var Addr = await Routing.GetAddresses( context, [company] );

			if( Addr.Count > 0 )
			{
				var Point = Addr[ 0 ];
				company.Latitude  = Point.Latitude;
				company.Longitude = Point.Longitude;
			}
		}
		return company;
	}

	public static IList<OptimiseEntry> ToOptimiseEntries( this RouteOptimisationAddresses addresses, IRequestContext context )
	{
		var            Result = new List<OptimiseEntry>();
		OptimiseEntry? Prev   = null;

		foreach( var Company in addresses.Companies )
		{
			Company.CheckCoordinate( context ).Wait();

			var From = new Point( Company.Latitude, Company.Longitude );

			Point To;

			if( Prev is not null )
			{
				Prev.Point = Prev.Point with { To = From };
				To         = new Point( Company.Latitude, Company.Longitude );
			}

			else
				To = From;

			Result.Add( new OptimiseEntry
			            {
				            CompanyName = Company.CompanyName,
				            Point       = new PointPair( From, To ),
				            Object      = Company
			            } );
		}
		return Result;
	}

	public static Protocol.Data.Maps.Route.Point ToPoint( this Point? point )
	{
		return point switch
		       {
			       null => new Protocol.Data.Maps.Route.Point(),
			       _    => new Protocol.Data.Maps.Route.Point { Latitude = point.Latitude, Longitude = point.Longitude }
		       };
	}

	public static List<Step> ToSteps( this List<DataTypes.MapDataTypes.Step> steps )
	{
		var Result = new List<Step>();

		foreach( var S in steps )
		{
			Result.Add( new Step
			            {
				            Distance         = (decimal)( S.Distance?.Value ?? 0 ),
				            Duration         = (decimal)( S.Duration?.Value ?? 0 ),
				            StartLocation    = S.StartLocation.ToPoint(),
				            EndLocation      = S.EndLocation.ToPoint(),
				            HtmlInstructions = S.Instructions
			            } );
		}
		return Result;
	}

	public static Leg ToLeg( this DataTypes.MapDataTypes.Leg leg, ref int order ) => new()
	                                                                                 {
		                                                                                 Order         = order++,
		                                                                                 StartLocation = leg.StartLocation.ToPoint(),
		                                                                                 Steps         = leg.Steps.ToSteps()
	                                                                                 };

	public static Route ToRoute( this DataTypes.MapDataTypes.Route route )
	{
		var Result = new Route();
		var Order  = 0;

		foreach( var L in route.Legs )
			Result.Add( L.ToLeg( ref Order ) );

		return Result;
	}


	public static DistancesAndAddresses ToDistancesAndAddresses( this Directions directions )
	{
		var Result = new DistancesAndAddresses();

		foreach( var Route in directions.Routes )
		{
			Result.Add( new DistanceAndAddress
			            {
				            Route = Route.ToRoute()
			            } );
		}

		return Result;
	}
}