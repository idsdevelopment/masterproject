﻿namespace MapsApiV2.DataTypes.MapDataTypes;

public class Point
{
	public CompanyBase.ADDRESS_TYPE AddressAction { get; set; }

	public bool IsPickupCompany
	{
		get => ( AddressAction & CompanyBase.ADDRESS_TYPE.PICKUP ) != 0;
		set
		{
			if( value )
				AddressAction |= CompanyBase.ADDRESS_TYPE.PICKUP;
			else
				AddressAction &= ~CompanyBase.ADDRESS_TYPE.PICKUP;
		}
	}

	public bool IsDeliveryCompany
	{
		get => ( AddressAction & CompanyBase.ADDRESS_TYPE.DELIVERY ) != 0;
		set
		{
			if( value )
				AddressAction |= CompanyBase.ADDRESS_TYPE.DELIVERY;
			else
				AddressAction &= ~CompanyBase.ADDRESS_TYPE.DELIVERY;
		}
	}

	public decimal Latitude
	{
		get => _Latitude;
		set
		{
			HCode     = null;
			_Latitude = value;
		}
	}

	public decimal Longitude
	{
		get => _Longitude;
		set
		{
			HCode      = null;
			_Longitude = value;
		}
	}

	public string CompanyName { get; init; } = "";

	public bool IsValid => ( Latitude != decimal.MinValue ) && ( Longitude != decimal.MinValue );

	public DateTimeOffset? DueTime { get; set; }

	public int TimeAtStopInSeconds { get; set; }

	private decimal _Latitude  = decimal.MinValue,
	                _Longitude = decimal.MinValue;

	public Point()
	{
	}

	public Point( decimal latitude, decimal longitude )
	{
		Latitude  = latitude;
		Longitude = longitude;
	}

	public enum MEASUREMENT
	{
		KILOMETRES,
		MILES
	}

	public bool IsEqual( Point p ) => ( Latitude == p.Latitude ) && ( Longitude == p.Longitude );

#region Distance
	public decimal DistanceTo( Point p, MEASUREMENT measurement )
	{
		var Dist = Measurement.SimpleGpsDistance( Latitude, Longitude, p.Latitude, p.Longitude );

		return measurement switch
		       {
			       MEASUREMENT.KILOMETRES => Dist.KiloMetres,
			       _                      => Dist.Miles
		       };
	}

	public decimal DistanceTo( Point p ) => DistanceTo( p, MEASUREMENT.KILOMETRES );

	public Point Closest( IEnumerable<Point> points )
	{
		var Closest = this;
		var Dist    = decimal.MaxValue;

		var Points = new PointPair( this );

		foreach( var Point in points )
		{
			Points.To = Point;
			var Temp = Points.Distance;

			if( Temp < Dist )
			{
				Dist    = Temp;
				Closest = Point;
			}
		}
		return Closest;
	}

	public Point Furthest( IEnumerable<Point> points )
	{
		var Closest = this;
		var Dist    = decimal.MinValue;

		var Points = new PointPair( this );

		foreach( var Point in points )
		{
			Points.To = Point;
			var Temp = Points.Distance;

			if( Temp > Dist )
			{
				Dist    = Temp;
				Closest = Point;
			}
		}
		return Closest;
	}
#endregion

#region Overrides
	public override string ToString() => $"{Latitude},{Longitude}";
	public override bool Equals( object? obj ) => obj is Point P && ( Latitude == P.Latitude ) && ( Longitude == P.Longitude );
#endregion

#region Equality members
	private int? HCode;

	// ReSharper disable once NonReadonlyMemberInGetHashCode
	public override int GetHashCode() => HCode ??= HashCode.Combine( Latitude, Longitude );

	public static bool operator ==( Point? left, Point? right ) => Equals( left, right );
	public static bool operator !=( Point? left, Point? right ) => !Equals( left, right );
#endregion
}

public struct PointPair
{
	private Point _From = new(),
	              _To   = new();

	public Point From
	{
		get => _From;
		set
		{
			_From     = value;
			_Distance = null;
		}
	}

	public Point To
	{
		get => _To;
		set
		{
			_To       = value;
			_Distance = null;
		}
	}

	public decimal Distance => _Distance ??= From.DistanceTo( To );

	private decimal? _Distance;

	public PointPair()
	{
	}

	public PointPair( Point from, Point to )
	{
		_From = from;
		_To   = to;
	}

	public PointPair( Point from )
	{
		_From = from;
	}
}

public sealed class Points : List<Point>
{
	public static (List<Point> Left, List<Point> Right, List<Point> OnLine) SideOfLine( PointPair line, IEnumerable<Point> points )
	{
		var FromLat  = line.From.Latitude;
		var FromLong = line.From.Longitude;
		var ToLat    = line.To.Latitude;
		var ToLong   = line.To.Longitude;

		var Left  = new List<Point>();
		var Right = new List<Point>();
		var On    = new List<Point>();

		foreach( var P in points )
		{
			switch( Maths.SideOfLine( FromLat, FromLong, ToLat, ToLong, P.Latitude, P.Longitude ) )
			{
			case < 0:
				Left.Add( P );
				break;

			case > 0:
				Right.Add( P );
				break;

			default:
				On.Add( P );
				break;
			}
		}
		return ( Left, Right, On );
	}

	public (List<Point> Left, List<Point> Right, List<Point> OnLine) SideOfLine( PointPair line ) => SideOfLine( line, this );

	public static List<Point> SideOfLineLeft( PointPair line, IEnumerable<Point> points )
	{
		var (Left, _, OnLine) = SideOfLine( line, points );

		if( OnLine.Count > 0 )
			Left.AddRange( OnLine );

		return Left;
	}

	public List<Point> SideOfLineLeft( PointPair line ) => SideOfLineLeft( line, this );

	public static List<Point> SideOfLineRight( PointPair line, IEnumerable<Point> points )
	{
		var (_, Right, OnLine) = SideOfLine( line, points );

		if( OnLine.Count > 0 )
			Right.AddRange( OnLine );

		return Right;
	}

	public List<Point> SideOfLineRight( PointPair line ) => SideOfLineRight( line, this );
}

public sealed class AddressData
{
	public Company AsCompany
	{
		get
		{
			var StreetAndNumber = StreetNumber.Trim();

			if( StreetAndNumber != "" )
				StreetAndNumber += ' ';

			StreetAndNumber = $"{StreetAndNumber} {Street}".Pack();

			Company Co = new()
			             {
				             CompanyName  = CompanyName,
				             Suite        = Suite,
				             AddressLine1 = StreetAndNumber,
				             City         = City,
				             Vicinity     = Vicinity,
				             Region       = StateCode,
				             PostalCode   = PostalCode,
				             Country      = Country,
				             CountryCode  = CountryCode,

				             Latitude  = Coordinate.Latitude,
				             Longitude = Coordinate.Longitude
			             };
			return Co;
		}
	}

	public Point Coordinate { get; } = new();


	public string FormattedAddress { get; set; } = null!;

	public string MultiLineAddress => DisplayAddress.Replace( ",", "\r\n" );

	public string HtmlMultiLineAddress => DisplayAddress.Replace( ",", "<br>" );

	public string DbLookupKey
	{
		get
		{
			return _DbLookupKey ??= _DbLookupKey =
				                        // Has only 1 space  Spaces
				                        string.Join( "", new StringBuilder().Append( CompanyName )
				                                                            .Append( Suite ).Append( StreetNumber )
				                                                            .Append( Street ).Append( Vicinity ).Append( City )
				                                                            .Append( State ).Append( StateCode ).Append( PostalCode )
				                                                            .Append( Country )
				                                                            .Append( CountryCode )
				                                                            .ToString()
				                                                            .TrimToLower()
				                                                            .Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries ) ).Pack();
		}
	}

	public string DisplayAddress
	{
		get
		{
			if( _DisplayAddress is null )
			{
				var Result = new StringBuilder();

				var HasSuite = Suite != "";

				Result.Append( Suite );

				if( HasSuite )
					Result.Append( '/' );

				Result.Append( StreetNumber );

				if( Street != "" )
					Result.Append( ' ' ).Append( Street );

				if( City != "" )
					Result.Append( ' ' ).Append( City );

				Result.Append( ',' );

				var HaveState = State != "";

				if( HaveState )
					Result.Append( State );

				if( StateCode != "" )
				{
					if( HaveState )
						Result.Append( '(' ).Append( StateCode ).Append( ')' );
					else
						Result.Append( StateCode );
				}

				if( PostalCode != "" )
					Result.Append( ' ' ).Append( PostalCode );

				if( Country != "" )
					Result.Append( ',' ).Append( Country );

				_DisplayAddress = Result.ToString().Trim().Pack().Capitalise();
			}
			return _DisplayAddress;
		}
	}

	public string CompanyName  { get; init; }
	public string Suite        { get; init; }
	public string StreetNumber { get; init; }
	public string Street       { get; init; }
	public string Vicinity     { get; init; }
	public string City         { get; init; }
	public string State        { get; set; }
	public string StateCode    { get; set; }
	public string PostalCode   { get; init; }
	public string Country      { get; set; }
	public string CountryCode  { get; set; }

	private string? _DbLookupKey;

	private string? _DisplayAddress;


	public AddressData()
	{
		CompanyName = Suite = StreetNumber = Street = Vicinity = City = State = StateCode = PostalCode = Country = CountryCode = "";
	}

	public AddressData( string companyName, string suite, string streetNumber, string street, string vicinity, string city, string state, string stateCode, string postalCode, string country, string countryCode )
	{
		CompanyName  = companyName.TrimToUpper();
		Suite        = suite.TrimToUpper();
		StreetNumber = streetNumber.TrimToUpper();
		Street       = street.TrimToUpper();
		Vicinity     = vicinity.TrimToUpper();
		City         = city.TrimToUpper();
		State        = state.TrimToUpper();
		StateCode    = stateCode.TrimToUpper();
		PostalCode   = postalCode.TrimToUpper();
		Country      = country.TrimToUpper();
		CountryCode  = countryCode.TrimToUpper();
		Coordinate   = new Point();
	}

	public AddressData( decimal latitude, decimal longitude, string companyName, string suite, string streetNumber, string street, string vicinity, string city, string state, string stateCode, string postalCode, string country, string countryCode )
		: this( companyName, suite, streetNumber, street, vicinity, city, state, stateCode, postalCode, country, countryCode )
	{
		Coordinate = new Point( latitude, longitude );
	}

	public AddressData( decimal latitude, decimal longitude, AddressData addressData )
		: this( latitude, longitude,
		        addressData.CompanyName, addressData.Suite, addressData.StreetNumber, addressData.Street, addressData.Vicinity,
		        addressData.City, addressData.State, addressData.StateCode, addressData.PostalCode, addressData.Country, addressData.CountryCode )
	{
	}

	public AddressData( AddressData addressData, string companyName )
		: this( addressData.Coordinate.Latitude, addressData.Coordinate.Longitude, addressData )
	{
		CompanyName = companyName;
	}

	public AddressData( MapAddress a, string companyName ) : this( a.Latitude, a.Longitude, a.ToAddressData( companyName ) )
	{
	}
}

public sealed class DistanceOrDuration
{
	public string Text = "";
	public double Value;
}

public sealed class Step
{
	public DistanceOrDuration? Distance,
	                           Duration;

	public Point? EndLocation;
	public string Instructions = "";
	public Point? StartLocation;
}

public sealed class Leg
{
	public DistanceOrDuration? Distance,
	                           Duration;

	public string EndAddress = "";
	public Point? EndLocation;

	public int Order; // Used to reorder route

	public Point? OriginalStartLocation,
	              OriginalEndLocation;

	public string StartAddress = "";
	public Point? StartLocation;

	public List<Step> Steps = [];

	public static string EmptyJsonLeg( Point from, Point to ) => JsonConvert.SerializeObject( new Leg
	                                                                                          {
		                                                                                          StartLocation         = from,
		                                                                                          OriginalStartLocation = from,

		                                                                                          EndLocation         = from,
		                                                                                          OriginalEndLocation = to
	                                                                                          } );

	public static string EmptyJsonLeg( Point point ) => EmptyJsonLeg( point, point );
}

public sealed class Route
{
	public List<Leg> Legs = [];
}

public sealed class Directions
{
	public List<string> Json   = [];
	public List<Route>  Routes = [];

	public Directions()
	{
	}

	public Directions( Directions d )
	{
		Json   = [..d.Json];
		Routes = [..d.Routes];
	}
}

public sealed class AddressList : List<AddressData>;

public sealed class AddressCoordinate
{
	public AddressData? Address;

	public decimal Latitude,
	               Longitude;
}

public sealed class CompanyMapAddress
{
	public string CompanyName { get; set; }
	public string Suite       { get; set; }

	public string  AddressLine1 { get; set; }
	public string  AddressLine2 { get; set; }
	public string  City         { get; set; }
	public string  Region       { get; set; }
	public string  RegionCode   { get; set; }
	public string  Country      { get; set; }
	public string  PostalCode   { get; set; }
	public decimal Latitude     { get; set; }
	public decimal Longitude    { get; set; }

	public CompanyMapAddress( CompanyBase company, AddressCoordinate a )
	{
		Longitude    = a.Longitude;
		Latitude     = a.Latitude;
		PostalCode   = ( a.Address is not null ? a.Address.PostalCode : company.PostalCode ).TrimToUpper();
		CompanyName  = company.CompanyName.TrimToUpper();
		Suite        = company.Suite.TrimToUpper();
		AddressLine1 = company.AddressLine1.TrimToUpper();
		AddressLine2 = company.AddressLine2.TrimToUpper();
		City         = company.City.TrimToUpper();
		Region       = company.Region.TrimToUpper();
		RegionCode   = company.RegionCode.TrimToUpper();
		Country      = company.Country.TrimToUpper();
	}

	public CompanyMapAddress( string companyName, string suite, MapAddress m )
	{
		CompanyName = companyName.TrimToUpper();
		Suite       = suite.TrimToUpper();

		Longitude    = m.Longitude;
		Latitude     = m.Latitude;
		PostalCode   = m.PostalCode;
		AddressLine1 = m.AddressLine1;
		AddressLine2 = m.AddressLine2;
		City         = m.City;
		Region       = m.Region;
		RegionCode   = m.RegionCode;
		Country      = m.Country;
	}

	public CompanyMapAddress( AddressData address )
	{
		CompanyName = address.CompanyName.TrimToUpper();
		Suite       = address.Suite.TrimToUpper();

		Longitude    = address.Coordinate.Longitude;
		Latitude     = address.Coordinate.Latitude;
		PostalCode   = address.PostalCode.TrimToUpper();
		AddressLine1 = ( address.StreetNumber + " " + address.Street ).TrimToUpper();
		AddressLine2 = "";
		City         = address.City;
		Region       = address.State;
		RegionCode   = address.StateCode;
		Country      = address.Country;
	}
}

public sealed class RouteSegment
{
	public string       TripId    { get; init; } = "";
	public string       AccountId { get; init; } = "";
	public AddressData? Account   { get; init; }
	public AddressData? From      { get; init; }
	public AddressData? To        { get; init; }
	public string       Reference { get; init; } = "";
	public decimal      Pieces    { get; init; }
	public decimal      Weight    { get; init; }
	public double       Distance  { get; set; }
	public string       Notes     { get; init; } = "";
}

public sealed class OptimisedEntry
{
	public Point Point { get; internal set; } = new();

	public bool IsPickup
	{
		get => Point.IsPickupCompany;
		set => Point.IsPickupCompany = value;
	}

	public bool IsDelivery
	{
		get => Point.IsDeliveryCompany;
		set => Point.IsDeliveryCompany = value;
	}


	public string CompanyName => Point.CompanyName; // Mainly for debugging

	public int           Order;
	public RouteSegment? Segment;

	public AddressData? WorkingAddress;
}

public sealed class OptimisedList( List<OptimisedEntry> oList )
{
	public int Count => Route.Count;

	public readonly List<OptimisedEntry> Route = oList;

	public OptimisedList() : this( [] )
	{
	}

	public void Add( RouteSegment segment )
	{
		var F = segment.From;
		var T = segment.To;

		if( F is not null && T is not null )
		{
			Route.Add( new OptimisedEntry
			           {
				           Segment        = segment,
				           WorkingAddress = F,
				           Point          = F.Coordinate
			           } );

			Route.Add( new OptimisedEntry
			           {
				           Segment        = segment,
				           WorkingAddress = T,
				           Point          = T.Coordinate
			           } );
		}
	}

	public OptimisedEntry this[ int ndx ] => Route[ ndx ];
}