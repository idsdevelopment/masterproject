﻿using Point = MapsApiV2.DataTypes.MapDataTypes.Point;

namespace MapsApiV2.Abstracts;

public abstract class AMapsApi
{
	// Temp Class
	internal class RouteInstruction
	{
		public string               Instruction = "";
		public Responses.Coordinate Point       = null!;
	}

	private static readonly HttpClient      Client = new();
	public readonly         IRequestContext Context;

	protected async Task<(string? Value, HttpResponseHeaders? Headers)> GetResponse( string url )
	{
		(string? Value, HttpResponseHeaders? Headers) Result = ( null, null );

		try
		{
			var Response = await Client.GetAsync( url );

			// Ensure the request was successful
			if( Response.IsSuccessStatusCode )
			{
				// Read the response body as a string
				var Bytes = await Client.GetByteArrayAsync( url );
				Result.Value   = BytesToString( Bytes );
				Result.Headers = Response.Headers;
			}
		}
		catch( Exception E )
		{
			Console.WriteLine( E.Message );
		}
		return Result;
	}

	protected AMapsApi( IRequestContext context )
	{
		Context = context;
	}

#region Virtuals
	protected virtual string BytesToString( byte[] bytes ) => Encoding.ASCII.GetString( bytes );

#region Abstracts
	public abstract Task<(AddressList AddressList, bool RateLimited)> FindLocationByAddress( AddressData address );
	public abstract Task<Directions> GetDirections( RouteBase.TRAVEL_MODE mode, int timezoneOffsetInMinutes, List<Point> points, bool circular );
#endregion
#endregion
}