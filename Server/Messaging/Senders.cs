﻿using System.Diagnostics;
using MessagingStorage.Cleanups;

namespace MessagingStorage.Senders;

public static class Sender<T>
{
	public static void Send( IRequestContext context, string topic, string queue, List<T> packets, string board = "" )
	{
	#if !DISABLE_MESSAGING
		try
		{
			if( packets.Count > 0 )
			{
				topic = topic.FixQueueName();
				queue = queue.FixQueueName();

				if( queue.IsNullOrWhiteSpace() ) // Multi queue broadcast
				{
					var TopicTable = new MessageTopicTableStorage<T>( context );
					var Queues     = TopicTable.GetQueues( topic );
				#if DEBUG
					if( Queues.Count == 0 )
					{
						Debug.WriteLine( $"\r\n!!! Broadcast: {topic} Not Found !!!" );
					}
				#endif
					foreach( var Topic in Queues )
					{
						var Queue = Topic.Queue;

						if( Queue.IsNotNullOrWhiteSpace() ) // Safety check for recursion
						{
							Debug.WriteLine( $"\r\n~~~ Broadcast: {topic}, {Queue}" );
							Send( context, topic, Queue, packets, Topic.Board );
						}
					}
				}
				else
				{
					var Queue = MessageQueueStorage<T>.GetQueueStorage( context, topic, queue, board );

					foreach( var Packet in packets )
					{
						var Json = JsonConvert.SerializeObject( Packet );
						Queue.SendMessage( Json );
					}
				}
			}
		}
		catch( Exception E )
		{
			context.SystemLogException( E );
		}
		finally
		{
			Cleanup<T>.DoCleanup( context );
		}
	#endif
	}
}