﻿namespace MessagingStorage;

public class MessageQueueStorage<T> : QueueStorage
{
	public const string LAST_MODIFIED      = "LastModified",
	                    TIME_TO_LIVE_HOURS = "TTLH";

	public const int DEFAULT_TIME_TO_LIVE_HOURS = 24;


	public static MessageQueueStorage<T> GetQueueStorage( IRequestContext context, string topic, string queue, string board = "" )
	{
		var QueueName = Storage.MakeMessageStorageName( typeof( T ), topic, queue, board );
		var Queue     = new MessageQueueStorage<T>( context, QueueName );

		return Queue;
	}


	public static void Clear( IRequestContext context, string topic, string queue, string board = "" )
	{
		topic = topic.TrimToLower();
		queue = queue.TrimToLower();

		if( topic.IsNotNullOrWhiteSpace() && queue.IsNotNullOrWhiteSpace() )
		{
			var Queue = GetQueueStorage( context, topic, queue, board );
			Queue.Clear();
		}
	}

	public MessageQueueStorage( IRequestContext context ) : base( context )
	{
	}

	public MessageQueueStorage( IRequestContext context, string queueName ) : base( context, queueName )
	{
	}
}