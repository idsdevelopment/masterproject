﻿using PackageType = Protocol.Data.PackageType;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override PackageTypeList ResponseGetPackageTypes()
		{
			using var Db = new CarrierDb( Context );

			return Db.GerPackageTypes();
		}

		public override void ResponseAddUpdatePackageType( PackageType requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdatePackageType( requestObject );
		}
	}
}