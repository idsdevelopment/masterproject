﻿using Emails;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseCleanupSentEmails()
		{
			Database.Model.Databases.Users.Emails.SentEmails.Cleanup();
		}

		public override void ResponseSendEmail( Email requestObject )
		{
			using var Client = new Client( Context );
			Client.Send( requestObject );
		}
	}
}