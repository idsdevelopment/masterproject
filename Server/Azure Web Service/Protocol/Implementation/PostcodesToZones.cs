﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseAddUpdatePostalZones( PostcodesToZones requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdatePostalZones( requestObject );
		}

		public override void ResponseDeletePostalZones( string routeName, string postalCode )
		{
			using var Db = new CarrierDb( Context );
			Db.DeletePostalZones( routeName, postalCode );
		}

		public override PostcodesToZonesList ResponseGetPostalZones( string routeName, string postalCode, string zone )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetPostalZones( routeName, postalCode, zone );
		}
	}
}