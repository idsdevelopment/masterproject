﻿using Users = Schedules.Users;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseAddUpdateSchedule( Schedule requestObject ) => Users.AddUpdateSchedule( Context, requestObject );

		public override void ResponseDeleteSchedule( long requestObject )
		{
			Users.DeleteSchedule( Context, requestObject );
		}

		public override Protocol.Data.Schedules ResponseGetSchedules() => Users.GetSchedules( Context );

		public override bool ResponseStartSchedule()
		{
			Tasks.RunVoid( Users.Run );
			return true;
		}
	}
}