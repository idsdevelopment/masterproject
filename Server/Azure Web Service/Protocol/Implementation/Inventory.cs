﻿using Inventory = Protocol.Data.Inventory;

// ReSharper disable InconsistentNaming

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseAddUpdateCartons( InventoryList requestObject )
		{
			return Users.RunProcess( Context, context =>
			                                  {
				                                  using var Db = new CarrierDb( context );

				                                  foreach( var Carton in requestObject )
				                                  {
					                                  var BCode = Carton.Barcode.Trim();
					                                  var ICode = Carton.InventoryCode.Trim();
					                                  var Desc  = Carton.Description.Trim();
					                                  var CDesc = $"{Desc} (CT)";
					                                  var PDesc = $"{Desc} (PK)";

					                                  Db.AddUpdateCarton( BCode, CDesc, ICode, Desc, Carton.PackageQuantity );

					                                  Db.AddUpdateInventory( new Inventory
					                                                         {
						                                                         Description     = CDesc,
						                                                         Barcode         = BCode,
						                                                         InventoryCode   = BCode,
						                                                         PackageQuantity = Carton.PackageQuantity
					                                                         } );

					                                  Db.AddUpdateInventory( new Inventory
					                                                         {
						                                                         Description     = PDesc,
						                                                         Barcode         = ICode,
						                                                         InventoryCode   = ICode,
						                                                         PackageQuantity = 1
					                                                         } );
				                                  }
			                                  } );
		}

		public override void ResponseAddUpdateInventory( InventoryList requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdateInventory( requestObject );
		}

		public override void ResponseAddUpdateInventoryItem( Inventory requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdateInventory( requestObject );
		}

		public override void ResponseDeleteInventory( InventoryList requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.DeleteInventory( requestObject );
		}

		public override void ResponseDeleteInventoryItem( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.DeleteInventory( requestObject );
		}

		public override InventoryList ResponseGetInventory()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetInventoryList();
		}

		public override InventoryBarcode ResponseGetInventoryBarcode( string requestObject ) => Users.GetInventoryBarcode( requestObject );
	}
}