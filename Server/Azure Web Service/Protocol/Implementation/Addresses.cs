﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override AddressIdList ResponseCheckMissingAddressesBySecondaryId( AddressIdList requestObject )
		{
			using var Db = new CarrierDb( Context );

			return new AddressIdList( Db.FindMissingAddressesBySecondaryId( requestObject ) );
		}
	}
}