﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override LicenseKeys ResponseGetLicenseKeys() => Users.GetLicenseKeys( Context );
	}
}