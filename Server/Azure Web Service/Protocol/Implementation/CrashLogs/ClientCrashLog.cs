﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseClientCrashLog( CrashReport requestObject )
		{
			// Make sure request context has been initialised
			new RequestContext.RequestContext();

			ClientCrashLog.Add( requestObject );
		}
	}
}