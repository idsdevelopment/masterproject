﻿using ServiceLevel = Protocol.Data.ServiceLevel;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseAddUpdateServiceLevel( ServiceLevel requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdateServiceLevel( requestObject );
		}

		public override ServiceLevelColours ResponseGetServiceLevelColours()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetServiceLevelColours();
		}

		public override ServiceLevelList ResponseGetServiceLevels()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetServiceLevels();
		}

		public override ServiceLevelListDetailed ResponseGetServiceLevelsDetailed()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetServiceLevelsDetailed();
		}

		public override ServiceLevelResult ResponseGetServiceLevel( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetServiceLevel( requestObject );
		}
	}
}