﻿using Preference = Protocol.Data.Preference;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override ClientPreferences ResponseClientPreferences()
		{
			using var Db = new CarrierDb( Context );

			return Db.ClientPreferences();
		}

		public override DevicePreferences ResponseDevicePreferences()
		{
			using var Db = new CarrierDb( Context );

			return Db.DevicePreferences();
		}

		public override bool ResponseIsPasswordValid( string requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.IsPasswordValid( requestObject );
		}

		public override MenuPreference ResponseMenuPreferences()
		{
			using var Db = new CarrierDb( Context );

			return Db.MenuPreferences();
		}

		public override Preferences ResponsePreferences()
		{
			using var Db = new CarrierDb( Context );

			return Db.GetPreferences();
		}

		public override void ResponseUpdatePreference( Preference requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.UpdatePreference( requestObject );
		}
	}
}