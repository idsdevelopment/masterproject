﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseAddCustomerCompany( AddCustomerCompany requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.AddResellerCompany( requestObject );
		}

		public override bool ResponseDeleteCustomerCompany( DeleteCustomerCompany requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.DeleteCustomerCompany( requestObject );
			return true; // Return value used to slow down client
		}

		public override bool ResponseUpdateCustomerCompany( UpdateCustomerCompany requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.UpdateCustomerCompany( requestObject );
			return true; // Return value used to slow down client
		}

		public override CustomerCompaniesList ResponseGetCustomerCompanyAddressList( CompanyByAccountAndCompanyNameList requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCustomerCompanyAddress( requestObject );
		}

		public override long ResponseImportAccountLocationBarcodes( AccountToLocationBarcode requestObject )
		{
			return Users.RunProcess( Context, context =>
			                                  {
				                                  using var Db = new CarrierDb( context );

				                                  if( requestObject.Purge )
					                                  Db.PurgeAccountIdToLocationBarcode();

				                                  foreach( var (Key, Value) in requestObject.Dict )
					                                  Db.AddUpdateAccountIdToLocationBarcode( Key, Value );
			                                  } );
		}

		public override long ResponseImportSecondaryAccountLocationBarcodes( AccountToLocationBarcode requestObject )
		{
			return Users.RunProcess( Context, context =>
			                                  {
				                                  using var Db = new CarrierDb( context );

				                                  if( requestObject.Purge )
					                                  Db.PurgeSecondaryAccountIdToLocationBarcode();

				                                  foreach( var (Key, Value) in requestObject.Dict )
					                                  Db.AddUpdateSecondaryAccountIdToLocationBarcode( Key, Value );
			                                  } );
		}

		public override CompanyDetailList ResponseGetCustomerCompaniesDetailed( string customerName )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCustomerCompaniesDetailed( customerName );
		}

		public override CompanySummaryList ResponseGetCustomerCompaniesSummary( string companyName )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCustomerCompaniesSummary( companyName );
		}
	}
}