﻿using Newtonsoft.Json;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override string ResponseTwoCodeLogin( string pgm, string u, string p, string timeZoneOffset, string d )
		{
			var Info = ResponseGetResellerCustomerLoginCredentials( u, p );

			if( Info.Ok )
			{
				Info.Ok = false;

				var (PWord, Ok) = Encryption.FromTimeLimitedToken( p );

				if( Ok )
				{
					if( sbyte.TryParse( timeZoneOffset, out var TimeZoneOffset ) )
					{
						var Debug = d.ToBool();
						var Ip    = HttpContext.IpAddress();

						var CarrierId = Encryption.FromTimeLimitedToken( Info.CarrierId ).Value;
						var UserName  = Encryption.FromTimeLimitedToken( Info.UserName ).Value;

						var Ctx = RequestContext.RequestContext.ToRequestContext( CarrierId, UserName, PWord, TimeZoneOffset, Debug, Ip );
						Info.AuthToken = Ctx.ToAuthToken();
						Info.Ok        = true;
					}
				}
			}

			var Temp = JsonConvert.SerializeObject( Info );
			return Encryption.ToTimeLimitedToken( Temp );
		}

		public override string ResponseGenerateLoginCode( string firstName, string lastName, string addressLine1 ) => Users.GenerateResellerLoginCode( Context, firstName, lastName, addressLine1 );

		public override CustomerLogin ResponseGetResellerCustomerLoginCredentials( string u, string p )
		{
			var Result = new CustomerLogin();

			try
			{
				var UserName = Encryption.FromTimeLimitedToken( u );

				if( UserName.Ok )
				{
					var Password = Encryption.FromTimeLimitedToken( p );

					if( Password.Ok )
					{
						var (CarrierId, UName, AccountId, PWord, Enabled, Ok) = Users.GetResellerFromLoginCode( UserName.Value );

						if( Ok && CarrierId.IsNotNullOrWhiteSpace() )
						{
							( var Value, Ok ) = Encryption.FromTimeLimitedToken( PWord );

							if( Ok && ( Password.Value == Value ) )
							{
								Result.CarrierId = Encryption.ToTimeLimitedToken( CarrierId );
								Result.UserName  = Encryption.ToTimeLimitedToken( UName );
								Result.AccountId = Encryption.ToTimeLimitedToken( AccountId );
								Result.Enabled   = Enabled;
								Result.Ok        = true;
							}
						}
					}
				}
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}
			return Result;
		}

		public override bool ResponseHasLoginCode( string requestObject ) => Users.HasResellerLoginCode( requestObject );

		public string ResponseResellerCustomerLogin( string pgm, string c, string u, string p, string timeZoneOffset, string version, string isDebugServer )
		{
			if( !sbyte.TryParse( timeZoneOffset, out var TimeZoneOffset ) )
				throw new AbortConnectionException();

			var Debug = isDebugServer.ToBool();

			var TimeError = false;

			var Company = Encryption.FromTimeLimitedToken( c );

			if( Company.Ok )
			{
				var (CarrierId, UserName, _, Password, Enabled, Ok) = Users.GetResellerFromLoginCode( Company.Value );

				if( Ok && Enabled )
				{
					var UName = Encryption.FromTimeLimitedToken( u );

					if( UName.Ok )
					{
						if( string.Compare( UName.Value, UserName, StringComparison.InvariantCultureIgnoreCase ) == 0 )
						{
							( var PWord, Ok ) = Encryption.FromTimeLimitedToken( p );

							if( Ok )
							{
								var C = CarrierId.Trim();
								var U = UName.Value.Trim();

								if( PWord == Password )
								{
								#if NET5_0_OR_GREATER
									var Ip = HttpContext.IpAddress();
								#else
									var Ip = HttpContext.Request.UserHostAddress;
								#endif
									Context = RequestContext.RequestContext.ToRequestContext( C, U, PWord, TimeZoneOffset, Debug, Ip );

									Tasks.RunVoid( () =>
									               {
										               UserAuditTrail.Add( Context.GetStorageId( LOG.LOGIN ), new UserAuditTrailEntryEntry( Context, AuditTrailEntryBase.OPERATION.SIGN_IN, $"{pgm} - {version}" ) );
									               } );

									return Context.ToAuthToken();
								}
							}
							else
								TimeError = true;
						}
					}
					else
						TimeError = true;
				}
			}
			else
				TimeError = true;

			return TimeError ? TIME_ERROR : "";
		}
	}
}