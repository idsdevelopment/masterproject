﻿using AzureWebService.Protocol.Implementation.Forwarders;
using PulsarV3;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		private const string SESSION_ID = "Plugh_Xyzzy";

		// No Session Checking
		public override byte[] Index_Tpl_Forwarder( string path, HttpRequest request, HttpResponse response )
		{
			var Bytes = Cache.Read( path );

			var Page   = Encoding.UTF8.GetString( Bytes );
			var Pulsar = new Pulsar { Reader = Cache.Read };

			var Dirs = Pulsar.TemplateDirectories;
			Dirs.Add( "\\Templates" );

		#if DEBUG
			Pulsar.Assign( "DEBUG", true );
		#endif
			Page = Pulsar.Fetch( Page );

			return Encoding.UTF8.GetBytes( Page );
		}

		// No Session Checking
		public override byte[] Signin_Tpl_Forwarder( string path, HttpRequest request, HttpResponse response ) => Index_Tpl_Forwarder( path, request, response );

		public override byte[] Tpl_Default_Forwarder( string path, HttpRequest request, HttpResponse response )
		{
			var Cookie = request.Cookies[ SESSION_ID ];

			if( Cookie is not null )
			{
				try
				{
				#if NET5_0_OR_GREATER
					var Ctx = RequestContext.RequestContext.ToRequestContextNotTimeLimited( Cookie );
				#else
					var Ctx = RequestContext.RequestContext.ToRequestContextNotTimeLimited( Cookie.Value );
				#endif

					if( Ctx is not null )
						return Index_Tpl_Forwarder( path, request, response );
				}
				catch
				{
				}
			}

			return Array.Empty<byte>();
		}
	}
}