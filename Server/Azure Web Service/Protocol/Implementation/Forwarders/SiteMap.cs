﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		private const string SITE_MAP_XML = "<? xml version = \"1.0\" encoding=\"UTF-8\"?>\r\n"
		                                    + "<urlset xmlns = \"http://www.sitemaps.org/schemas/sitemap/0.9\" >\r\n"
		                                    + "</urlset>\r\n";

		public override byte[] Sitemap_Xml_Forwarder( string path, HttpRequest request, HttpResponse response ) => SITE_MAP_XML.ToUTF8Bytes();
	}
}