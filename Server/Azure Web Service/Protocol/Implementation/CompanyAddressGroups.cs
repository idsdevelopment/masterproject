﻿using CompanyAddressGroup = Protocol.Data.CompanyAddressGroup;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override bool ResponseAddUpdateCompanyAddressGroup( CompanyAddressGroup requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdateCompanyAddressGroup( requestObject );
			return true;	// Return value used to slow down client
		}

		public override bool ResponseAddCompanyAddressGroupEntry( CompanyAddressGroupEntry requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddCompanyAddressGroupEntry( requestObject );
			return true; // Return value used to slow down client
		}

        public override void ResponseDeleteCompanyAddressGroup( int requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.DeleteCompanyAddressGroup( requestObject );
		}

		public override bool ResponseDeleteCompanyAddressGroupEntry( CompanyAddressGroupEntry requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.DeleteCompanyAddressGroupEntry( requestObject );
			return true; // Return value used to slow down client
		}

        public override CompaniesWithinAddressGroups ResponseGetCompaniesWithinAddressGroup( int requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCompaniesWithinAddressGroup( requestObject );
		}

		public override CompaniesWithinAddressGroups ResponseGetCompaniesWithinAddressGroups( CompanyAddressGroupList requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCompaniesWithinAddressGroups( requestObject );
		}

		public override CompanyAddressGroups ResponseGetCompanyAddressGroups()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetCompanyAddressGroups();
		}
	}
}