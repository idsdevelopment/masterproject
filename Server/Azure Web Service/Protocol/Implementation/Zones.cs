﻿namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override bool ResponseAddUpdateZones( Zones requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.AddUpdateZones( requestObject, requestObject.ProgramName );
			return true;
		}

		public override bool ResponseDeleteZones( ZoneNameList requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.DeleteZones( requestObject, requestObject.ProgramName );
			return true;
		}

		public override Zones ResponseZones()
		{
			using var Db = new CarrierDb( Context );
			return Db.GetZones();
		}
	}
}