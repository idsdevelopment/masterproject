﻿using Invoice = Protocol.Data.Invoice;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseAddInvoice( Invoice requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.AddInvoice( requestObject );
		}

		public override Invoices ResponseGetInvoices( GetInvoices requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetInvoices( requestObject );
		}

		public override InvoiceAndTripsList ResponseGetInvoicesAndTrips( GetInvoices requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.GetInvoicesAndTrips( requestObject );
		}
	}
}