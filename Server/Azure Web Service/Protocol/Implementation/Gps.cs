﻿using StorageV2.Carrier;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override GpsPoints ResponseGetGpsPoints( GpsDriverDateRange requestObject ) => GpsLog.GetPoints( Context, requestObject );

		public override void ResponseGps( GpsPoints requestObject )
		{
			if( requestObject.Count > 0 )
			{
				var Points = ( from Ro in requestObject
				               select new GpsLog.GpsPoint { Longitude = Ro.Longitude, Latitude = Ro.Latitude, DateTime = Ro.LocalDateTime } ).ToList();
				new GpsLog( Context ).Append( Points );
			}
		}
	}
}