﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override StorageTest ResponseTestStorage( StorageTest requestObject ) => new Testing().Execute( Context, requestObject );
	}
}