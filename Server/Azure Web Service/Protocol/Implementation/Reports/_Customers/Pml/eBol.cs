﻿using Reports.Pml;

// ReSharper disable IdentifierTypo

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePML_ResendeBOL( string tripId, string emailAddress )
		{
			eBolReport.PML_eBOL( Context, tripId, emailAddress );
		}
	}
}