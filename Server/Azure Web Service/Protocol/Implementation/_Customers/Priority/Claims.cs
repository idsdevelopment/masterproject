﻿using Protocol.Data.Customers.Priority;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePickupByBarcode( PickupByBarcode requestObject )
		{
			using var Db = new CarrierDb( Context );
			Broadcast( Db.PriorityPickupByBarcode( requestObject ) );
		}
	}
}