﻿using Imports.Pml;
using Protocol.Data._Customers.Pml;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponsePml_AddUpdateXref( PmlXref requestObject )
		{
			using var Db = new CarrierDb( Context );
			Db.Pml_AddUpdateXref( requestObject );
		}

		public override PmlXref ResponsePml_GetXref( string requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.Pml_GetXref( requestObject );
		}

		public override void ResponsePml_RecreateXrefFromXml( string requestObject )
		{
			var Client = Context.CarrierId.TrimToLower() switch
			             {
				             "pmlplay" => new PmlPlayClient(),
				             "pml"     => new PmlClient(),
				             _         => null
			             };

			Client?.RecreateXrefFromXml( Context, requestObject );
		}
	}
}