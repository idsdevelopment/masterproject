﻿namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override TripIdListAndProgram ResponseFinaliseManuallyVerifiedSatchels( DateTimeOffset requestObject )
		{
			using var Db = new CarrierDb( Context );
			return Db.FinaliseManuallyVerifiedSatchels( requestObject );
		}
	}
}