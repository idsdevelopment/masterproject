﻿using Emails;
using Ids1WebService._Customers.Metro;

// ReSharper disable JoinDeclarationAndInitializer
// ReSharper disable RedundantArgumentDefaultValue

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		private static DateTime LastSent = DateTime.MinValue;

		public override void ResponseCheckMetroIds1Connection()
		{
		#if !DEBUG
			const string TO  = "sangelevski@metroscg.com",
			             CC  = "cwatton@MetroSCG.com;wshadbolt@MetroSCG.com;zfahidy@MetroSCG.com;rruparelia@MetroSCG.com",
			             BCC = "eddy@idsapp.com;terry@idsapp.com;rowan@idsapp.com";

			const string INTERNAL_TO  = "eddy@idsapp.com",
			             INTERNAL_CC  = "terry@idsapp.com;rowan@idsapp.com",
			             INTERNAL_BCC = "";
		#else
			const string DEBUG_TO = "terry@idsapp.com";
		#endif

			const string SUBJECT = "The Metro IDS1 Connection Status";

			var Result  = PingServers.Ping();
			var InError = Result != PingServers.PING_RESULT.OK;
			// Convert UTC time to Montreal time 
			var UtcNow = DateTime.UtcNow;

			// Get the Pacific Time Zone
			var EasternStandardTime = TimeZoneInfo.FindSystemTimeZoneById( "Eastern Standard Time" );
			var MontrealTime        = TimeZoneInfo.ConvertTimeFromUtc( UtcNow, EasternStandardTime );

			var SendToCustomer = MontrealTime is { Hour: 4, Minute: >= 45 and <= 55 }; // Polls every 10 minutes. Send to customer at 4:50am

			if ( InError || SendToCustomer )
			{
				if( !SendToCustomer && ( ( UtcNow - LastSent ).TotalMinutes < 30 ) ) // Don't send more than once every 30 minutes
					return;

				LastSent = UtcNow;

				var Body = $"""
				            IDS1 connection status. 

				            East1 Server:   {( ( Result & PingServers.PING_RESULT.EAST1_FAIL ) != 0 ? "FAILED" : "OK" )}
				            Drivers Server: {( ( Result & PingServers.PING_RESULT.DRIVERS_FAIL ) != 0 ? "FAILED" : "OK" )}
				            Users Server:   {( ( Result & PingServers.PING_RESULT.USERS_FAIL ) != 0 ? "FAILED" : "OK" )}
				            """;

				string To, Cc, Bcc;

			#if !DEBUG
				if( SendToCustomer )
				{
					To  = TO;
					Cc  = CC;
					Bcc = BCC;
				}
				else
				{
					To  = INTERNAL_TO;
					Cc  = INTERNAL_CC;
					Bcc = INTERNAL_BCC;
				}

			#else
				To = DEBUG_TO;
				Cc = "";
				Bcc = "";
			#endif

				Client.SendFrom( RequestContext.RequestContext.InternalContext,
				                 To,
				                 "support@idsapp.com",
				                 SUBJECT,
				                 Body, Cc, Bcc );
			}
		}
	}
}