﻿using Imports;

namespace Ids;

public partial class IdsServer : AIdsServer
{
	public partial class IdsServerImplementation
	{
		public override void ResponseCheckForImportsAndExports()
		{
			Tasks.RunVoid( async () =>
			               {
				               await RunImportsAndExports.Execute();
			               } );
		}

		public override bool ResponsePushTripToExport( string tripId )
		{
			using var Db = new CarrierDb( Context );
			return Db.PushTripToExport( tripId );
		}
	}
}