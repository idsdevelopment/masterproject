﻿using Newtonsoft.Json;
using Route = Protocol.Data.Route;

namespace Ids;

public partial class IdsServer
{
	public partial class IdsServerImplementation
	{
		public override long ResponseBeginRouteImport( string requestObject )
		{
			var Retval = Users.StartProcess( Context );

			var DbContext = Context.Clone();

			Task.Run( () =>
			          {
				          try
				          {
					          using var Db = new CarrierDb( DbContext );

					          Db.DeleteAllRoutes();
				          }
				          finally
				          {
					          Users.EndProcess( DbContext, Retval );
				          }
			          } );

			return Retval;
		}

		public override long ResponseImportRoutes( RouteAndCompanyAddresses requestObject )
		{
			var Retval = Users.StartProcess( Context );

			var DbContext = Context.Clone();

			Task.Run( () =>
			          {
				          try
				          {
					          using var Db = new CarrierDb( DbContext );

					          Db.AddUpdateRoute( requestObject );
				          }
				          finally
				          {
					          Users.EndProcess( DbContext, Retval );
				          }
			          } );

			return Retval;
		}

		public override void ResponseAddUpdateRoute( Route requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdateRoute( requestObject );
		}

		public override RouteLookupSummaryList ResponseRouteLookup( RouteLookup requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.RouteLookup( requestObject );
		}

		public override void ResponseUpdateRouteSchedule( RouteScheduleUpdate requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.UpdateRouteSchedule( requestObject );
		}

		public override RouteCompanySummaryList ResponseGetCompanySummaryInRoute( string customerCode, string routeName )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCompanySummaryInRoute( customerCode, routeName );
		}

		public override bool ResponseHasRoute( string customerCode, string routeName )
		{
			using var Db = new CarrierDb( Context );

			return Db.HasRoute( customerCode, routeName );
		}

		public override void ResponseAddRoute( string programName, string customerCode, string routeName )
		{
			using var Db = new CarrierDb( Context );

			Db.AddRoute( programName, customerCode, routeName );
		}

		public override void ResponseDeleteRoute( string programName, string customerCode, string routeName )
		{
			using var Db = new CarrierDb( Context );

			Db.DeleteRoute( programName, customerCode, routeName );
		}

		public override void ResponseRenameRoute( string programName, string customerCode, string oldRouteName, string newRouteName )
		{
			using var Db = new CarrierDb( Context );

			Db.RenameRoute( programName, customerCode, oldRouteName, newRouteName );
		}

		public override void ResponseRemoveCompaniesFromRoute( RouteCompanyList requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.RemoveCompaniesFromRoute( requestObject );
		}

		public override void ResponseAddCompaniesToRoute( RouteCompanyList requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddCompaniesToRoute( requestObject );
		}

		public override void ResponseUpdateOptionsInRoute( RouteUpdate requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.UpdateOptionsInRoute( requestObject );
		}

		public override RouteManifest ResponseGetRouteManifest( RouteManifestRequest requestObject )
		{
			var Result = new RouteManifest();

			try
			{
				var Manifest = new StorageV2.Carrier.RouteManifest( Context, requestObject.ManifestName, requestObject.LegNumber );
				var Json     = Manifest.ReadDirections();
				var Route    = JsonConvert.DeserializeObject<Protocol.Data.Maps.Route.Route>( Json ) ?? new Protocol.Data.Maps.Route.Route();
				Result.Route = Route;
			}
			catch( Exception Exception )
			{
				Context.SystemLogException( Exception );
			}

			return Result;
		}

		public override CustomerRoutesScheduleList ResponseGetRoutesForCustomers( CustomerCodeList requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetRoutesForCustomers( requestObject );
		}

		public override void ResponseEnableRouteForCustomer( EnableRouteForeCustomer requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.EnableRouteForCustomer( requestObject );
		}

		public override CustomersRoutesBasic ResponseGetCustomersRoutesBasic( CustomerCodeList requestObject )
		{
			using var Db = new CarrierDb( Context );

			return Db.GetCustomersRoutesBasic( requestObject );
		}

		public override void ResponseAddUpdateRenameRouteBasic( AddUpdateRenameRouteBasic requestObject )
		{
			using var Db = new CarrierDb( Context );

			Db.AddUpdateRenameRouteBasic( requestObject );
		}
	}
}