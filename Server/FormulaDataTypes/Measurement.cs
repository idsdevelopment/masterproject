﻿// ReSharper disable InconsistentNaming

using Utils;

namespace FormulaDataTypes
{
	public abstract class Measurement : FormulaBase
	{
		public decimal METRES
		{
			get => M.Metres;
			set => M.Metres = value;
		}

		public decimal KILOMETRES
		{
			get => M.KiloMetres;
			set => M.KiloMetres = value;
		}


		public decimal MILES
		{
			get => M.Miles;
			set => M.Miles = value;
		}

		private Utils.Measurement M;

		protected Measurement()
		{
		}

		protected Measurement( EvaluateFormula e ) : base( e )
		{
			var V = e.Variables;

			if( V.TryGetValue( nameof( KILOMETRES ), out var Dist ) && Dist.IsNotNullOrWhiteSpace() )
				KILOMETRES = e.GetVariableAsDecimal( nameof( KILOMETRES ) );

			else if( V.TryGetValue( nameof( MILES ), out Dist ) && Dist.IsNotNullOrWhiteSpace() )
				MILES = e.GetVariableAsDecimal( nameof( MILES ) );

			else
				METRES = e.GetVariableAsDecimal( nameof( METRES ) );
		}
	}
}