﻿// ReSharper disable InconsistentNaming

namespace FormulaDataTypes.Employee;

public class Shift : Measurement
{
	public string PACKAGE_TYPE  { get; set; }
	public string PICKUP_ZONE   { get; set; }
	public string DELIVERY_ZONE { get; set; }

	public Shift()
	{
		PACKAGE_TYPE  = "";
		PICKUP_ZONE   = "";
		DELIVERY_ZONE = "";
	}

	public Shift( EvaluateFormula e ) : base( e )
	{
		PACKAGE_TYPE  = e.GetVariableAsString( nameof( PACKAGE_TYPE ) );
		PICKUP_ZONE   = e.GetVariableAsString( nameof( PICKUP_ZONE ) );
		DELIVERY_ZONE = e.GetVariableAsString( nameof( DELIVERY_ZONE ) );
	}
}