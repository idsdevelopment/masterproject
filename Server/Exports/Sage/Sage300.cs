﻿#nullable enable

using Address = Database.Model.Databases.MasterTemplate.Address;
using Company = Database.Model.Databases.MasterTemplate.Company;
using Invoice = Protocol.Data.Invoice;
using Trip = Protocol.Data.Trip;

namespace Exports.Sage;

public class Sage300 : Export.ExportBase
{
	private const string ACCOUNT_PREF_PREFIX = "account_pref_charge_",
	                     SURCHARGE_PREFIX    = "surcharge_";

	private Csv ExportCsv { get; } = new();

	private Dictionary<string, Company> Companies { get; } = new();

	private Dictionary<string, int> DictChargeIdCols { get; } = new();

	private CarrierDb Db = null!;

	private MasterTemplateContext Entity = null!;

	public Sage300( IRequestContext context, ExportArgs args ) : base( context, args )
	{
	}

	public override string Export()
	{
		var StartingInvoiceNumber = Args.StringArg1.Trim();

		if( StartingInvoiceNumber.IsNotNullOrWhiteSpace() )
		{
			var Yes = long.TryParse( StartingInvoiceNumber, out var InvoiceNumber );

			if( Yes )
				BuildCsv( InvoiceNumber );
		}

		return ExportCsv.ToString();
	}

	private void BuildCsv( long startingInvoiceNumber )
	{
		// ReSharper disable once ConvertToUsingDeclaration
		using( Db = new CarrierDb( Context ) )
		{
			Entity = Db.Entity;

			BuildHeader();

			// Load invoices
			var Invoices = GetInvoices( startingInvoiceNumber );

			if( Invoices is { Count: > 0 } )
			{
				var RowNumber = 0;

				foreach( var Inv in Invoices )
				{
					var Trips = GetInvoiceTrips( Inv );

					if( Trips is { Count: > 0 } )
					{
						//foreach (string id in inv.TripIds )
						foreach( var Trip in Trips )
						{
							var Col = 0;
							++RowNumber;

							ExportCsv.AppendRow();
							var Row = ExportCsv[ RowNumber ];
							Row[ Col++ ] = Inv.InvoiceNumber.ToString();

							// 20230505_144214
							var Tmp = Inv.InvoiceDateTime.ToString( "yyyyMMdd_HHmmss" );
							Row[ Col++ ] = Tmp;

							Tmp          = Inv.InvoiceFrom.ToString( "yyyyMMdd_HHmmss" );
							Row[ Col++ ] = Tmp;

							Tmp          = Inv.InvoiceTo.ToString( "yyyyMMdd_HHmmss" );
							Row[ Col++ ] = Tmp;

							Row[ Col++ ] = Trip.TripId;

							var      BillingCompanyId = Inv.BillingCompanyName.Trim();
							Company? BillingCompany;

							if( Companies.TryGetValue( BillingCompanyId, out var Company1 ) )
								BillingCompany = Company1;
							else
							{
								BillingCompany = GetCompany( BillingCompanyId );
								Companies.Add( BillingCompanyId, BillingCompany! );
							}

							if( BillingCompany is not null )
							{
								Row[ Col++ ] = BillingCompany.CompanyNumber;
								Tmp          = Trip.CallTime?.ToString( "yyyy-MM-dd HH:mm" );

								if( Tmp is not null )
								{
									Row[ Col++ ] = Tmp;

									// Pickup
									Row[ Col++ ] = Trip.PickupCompanyName;
									Row[ Col++ ] = Trip.PickupAddressSuite;
									Row[ Col++ ] = Trip.PickupAddressAddressLine1;
									Row[ Col++ ] = Trip.PickupAddressCity;
									Row[ Col++ ] = Trip.PickupAddressRegion;
									Row[ Col++ ] = Trip.PickupAddressCountry;
									Row[ Col++ ] = Trip.PickupAddressPostalCode;
									Row[ Col++ ] = Trip.PickupZone;

									// Delivery
									Row[ Col++ ] = Trip.DeliveryCompanyName;
									Row[ Col++ ] = Trip.DeliveryAddressSuite;
									Row[ Col++ ] = Trip.DeliveryAddressAddressLine1;
									Row[ Col++ ] = Trip.DeliveryAddressCity;
									Row[ Col++ ] = Trip.DeliveryAddressRegion;
									Row[ Col++ ] = Trip.DeliveryAddressCountry;
									Row[ Col++ ] = Trip.DeliveryAddressPostalCode;
									Row[ Col++ ] = Trip.DeliveryZone;

									// Billing
									var Addr = GetCompanyAndAddress( BillingCompany.CompanyName );
									//Database.Model.Databases.MasterTemplate.Address? addr = GetResellerCompanyAddress(billingCompany.CompanyName);
									Row[ Col++ ] = BillingCompany.CompanyName;
									Row[ Col++ ] = Addr is null ? "" : Addr.Suite;
									Row[ Col++ ] = Addr is null ? "" : Addr.AddressLine1;
									Row[ Col++ ] = Addr is null ? "" : Addr.City;
									Row[ Col++ ] = Addr is null ? "" : Addr.Region;
									Row[ Col++ ] = Addr is null ? "" : Addr.Country;
									Row[ Col++ ] = Addr is null ? "" : Addr.PostalCode;
									Row[ Col++ ] = Addr is null ? "" : Addr.Phone;

									// Trip details
									Row[ Col++ ] = Trip.Pieces;
									Row[ Col++ ] = Trip.Weight;
									Row[ Col++ ] = Trip.ServiceLevel;
									Row[ Col++ ] = Trip.PackageType;

									// Totals
									Row[ Col++ ] = Trip.TotalFixedAmount.ToString( "F2" );
									Row[ Col++ ] = Trip.TotalTaxAmount.ToString( "F2" );
									Row[ Col++ ] = Trip.TotalAmount.ToString( "F2" );

									Row[ Col++ ] = Trip.Driver;
									Row[ Col++ ] = GetDriversName( Trip.Driver );

									// Send Invoice to
									Addr         = GetResellerCompanyAddress( BillingCompany.CompanyName );
									Row[ Col++ ] = Addr is null ? "" : Addr.Suite;        // "Send Invoice To";
									Row[ Col++ ] = Addr is null ? "" : Addr.AddressLine1; // "Send Address";
									Row[ Col++ ] = Addr is null ? "" : Addr.City;         // "City";
									Row[ Col++ ] = Addr is null ? "" : Addr.Region;       // "Prov/State";
									Row[ Col++ ] = Addr is null ? "" : Addr.Country;      // "Country";
									Row[ Col++ ] = Addr is null ? "" : Addr.PostalCode;   // "Postal Code/Zip";

									// Pickup and Verified times
									Tmp          = Trip.PickupTime?.ToString( "yyyy-MM-dd HH:mm" );
									Row[ Col++ ] = Tmp ?? "";
									Tmp          = Trip.VerifiedTime.ToString( "yyyy-MM-dd HH:mm" );
									Row[ Col++ ] = Tmp;

									// Waybill is stored as a charge on phoenix
									if( Trip.TripCharges is { Count: > 0 } )
									{
										var Tc = ( from T in Trip.TripCharges
										           where T.ChargeId.Trim().ToLower() == "waybill"
										           select T ).FirstOrDefault();
										Row[ Col ] = Tc != null ? Tc.Value.ToString( CultureInfo.InvariantCulture ) : string.Empty;
									}

									// Charges
									if( Trip.TripCharges is { Count: > 0 } )
									{
										foreach( var Tc in Trip.TripCharges )
										{
											if( Tc.ChargeId.Trim().ToLower() != "waybill" ) // Already listed
											{
												var (Id, Value) = GetBaseCharge( Tc );

												if( DictChargeIdCols.TryGetValue( Id, out var IdCol ) )
													Row[ IdCol ] = Value.ToString( "F2" );
											}
										}
									}
								}
							}
							else
								Row[ Col ] = $"Can't load company {BillingCompanyId}";
						}
					}
					else
					{
						ExportCsv.AppendRow();
						var Row = ExportCsv[ ++RowNumber ];
						Row[ 0 ] = $"Can't load trips for invoice {Inv.BillingCompanyName}";
					}
				}
			}
			else
			{
				var Col = 0;
				ExportCsv.AppendRow();
				var Row = ExportCsv[ 1 ];
				Row[ Col++ ] = $"startingInvoiceNumber '{startingInvoiceNumber}'";
				Row[ Col ]   = "No Invoices";
			}
		}
	}

	private void BuildHeader()
	{
		const string RAW_HEADER =
			"Inv No,Invoice Assign Date/Time,Period From Date,Invoice Date,Trip ID,Billing Account ID,Call Time,Pickup Company Name,Suite,Address,City,Prov/State,Country,Post Code/Zip,P/Z,Delivery Company,Suite,Address,City,Prov/State,Country,Post Code/Zip,DZ,Billing Company,Suite,Address,City,Prov/State,Cpuntry,Post Code/Zip, Phone,Pieces,Weight,Service Level,Package Type,Del Chg,GST/HST,Total Inc Tax,Driver,Driver Name,Billing Address Suite,Billing Address,City,Prov/State,Country,Post Code/Zip,Pickup Date/Time,Verified Date/Time,Waybill";

		var Row0    = ExportCsv[ 0 ];
		var Headers = RAW_HEADER.Split( ',' );
		var Col     = 0;

		for( ; Col < Headers.Length; Col++ )
			Row0[ Col ] = Headers[ Col ].Trim();
		var ChargeIds = GetChargeIds();

		foreach( var Id in ChargeIds )
		{
			var Tmp = Id.Trim().ToLower();

			if( ( Tmp != "waybill" ) && !Tmp.StartsWith( ACCOUNT_PREF_PREFIX ) && !Tmp.StartsWith( SURCHARGE_PREFIX ) ) // Ignore waybill - phoenix has a dedicated column
			{
				if( DictChargeIdCols.TryAdd( Id, Col ) )
					Row0[ Col++ ] = Id;
			}
		}
	}

	private List<Invoice> GetInvoices( long startingInvoiceNumber )
	{
		List<Invoice> Invoices = [];

		try
		{
			var Recs = ( from I in Entity.Invoices
			             where I.InvoiceNumber >= startingInvoiceNumber
			             select I ).ToList();

			if( Recs is { Count: > 0 } )
			{
				foreach( var Invoice in Recs )
					Invoices.Add( Invoice.ToInvoice() );
			}
		}
		catch( Exception Exception )
		{
			Context.SystemLogException( Exception );
		}

		return Invoices;
	}

	private List<string> GetChargeIds()
	{
		List<string> Ids = ( from C in Entity.ChargeDetails
		                     orderby C.ChargeId
		                     select C.ChargeId ).ToList();

		return Ids;
	}

	private List<Trip>? GetInvoiceTrips( Invoice invoice )
	{
		var Getinvoice       = new GetInvoices { BillingCustomerCode = invoice.BillingCompanyName };
		var Invoicesandtrips = Db.GetInvoicesAndTrips( Getinvoice );

		var Shipments = ( from T in Invoicesandtrips
		                  where T.InvoiceNumber == invoice.InvoiceNumber
		                  orderby T.InvoiceDateTime
		                  select T.Trips ).ToList().FirstOrDefault();

		return Shipments;
	}

	private Company? GetCompany( string name )
	{
		var Rec = ( from C in Entity.Companies
		            where C.CompanyName == name
		            select C ).FirstOrDefault();

		return Rec;
	}

	private Address? GetCompanyAndAddress( string name )
	{
		Address? Addr = null;

		var Rec = ( from C in Entity.Companies
		            where C.CompanyName == name
		            select C ).ToList();

		foreach( var C in Rec )
		{
			var Temp = ( from A in Entity.Addresses
			             where A.Id == C.PrimaryAddressId
			             select A ).FirstOrDefault();

			if( Temp is not null )
			{
				Addr = Temp;
				break;
			}
		}

		return Addr;
	}

	private Address? GetResellerCompanyAddress( string companyName )
	{
		Address? Addr = null;

		var ResellerCustomer = ( from C in Entity.ResellerCustomers
		                         where C.CompanyName == companyName
		                         select C ).FirstOrDefault();

		if( ResellerCustomer is not null )
		{
			var Billing = ( from C in Entity.Companies
			                where C.CompanyId == ResellerCustomer.BillingCompanyId
			                select C ).FirstOrDefault();

			if( Billing is not null )
			{
				Addr = ( from A in Entity.Addresses
				         where A.Id == Billing.PrimaryAddressId
				         select A ).FirstOrDefault();
			}
		}

		return Addr;
	}

	private string GetDriversName( string id )
	{
		var Staff = Entity.Staff;

		var Rec = ( from S in Staff
		            where S.StaffId == id
		            select S ).FirstOrDefault();

		return Rec is not null ? $"{Rec.FirstName} {Rec.LastName}" : "";
	}

	/// <summary>
	///     Map the account_pref charges to the overridden charge.
	///     If it isn't
	/// </summary>
	/// <param
	///     name="tc">
	/// </param>
	/// <returns>string id, decimal value</returns>
	private static (string, decimal) GetBaseCharge( TripCharge tc )
	{
		var Id     = tc.ChargeId;
		var Result = tc.Value;

		if( tc.ChargeId.StartsWith( ACCOUNT_PREF_PREFIX ) )
		{
			// account_pref_charge_andrewshereteastvan27_1tonhourly
			var Pieces = Id.Split( '_' );

			if( Pieces.Length >= 5 )
			{
				Id = Pieces[ 4 ];

				// Just in case the base chargeId contains '_'s
				if( Pieces.Length > 5 )
				{
					for( var I = 5; I < Pieces.Length; I++ )
						Id += "_" + Pieces[ I ];
				}
			}
		}

		return ( Id, Result );
	}
}