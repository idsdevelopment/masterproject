﻿using Amazon;
using StorageV2.Logs;
using Trip = Protocol.Data.Trip;

namespace Exports._Cutomers.Pml;

// ReSharper disable once InconsistentNaming
public class CBC2C
{
	private const string ACCESS_KEY  = "AKIAYZNC3WJTOEAU2EGH",
	                     SECRET_KEY  = "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",
	                     BUCKET_NAME = "dev-externalsource-elmaau-raw",
	                     FOLDER      = "v1/IDS";

	// ReSharper disable once InconsistentNaming
	private static readonly RegionEndpoint REGION = RegionEndpoint.EUWest1;

	private static readonly DateTimeOffset DateLimit = new( 2020, 1, 1, 0, 0, 0, TimeSpan.Zero );

	public static async void Export( IRequestContext context )
	{
		try
		{
			var UtcNow = DateTimeOffset.UtcNow;

			var EasternStandardTime = TimeZoneInfo.FindSystemTimeZoneById( "AUS Eastern Standard Time" );

			DateTimeOffset ToSydneyDateTime( DateTimeOffset utcDateTimeOffset )
			{
				return TimeZoneInfo.ConvertTime( utcDateTimeOffset, EasternStandardTime );
			}

			string ToSydneyDateAsString( DateTimeOffset utcDateTimeOffset )
			{
				return utcDateTimeOffset <= DateLimit ? "" : $"{ToSydneyDateTime( utcDateTimeOffset ):dd/MM/yyyy}";
			}

			var SydneyTime = ToSydneyDateTime( UtcNow );

		#if !DEBUG
			if( SydneyTime is { Hour: 0 } )
		#endif
			{
				var YesterdayDateTime = SydneyTime.AddDays( -1 );
				var YesterdayStart    = YesterdayDateTime.StartOfDay();
				var YesterdayEnd      = YesterdayStart.EndOfDay();

				YesterdayStart = YesterdayStart.ToUniversalTime();
				YesterdayEnd   = YesterdayEnd.ToUniversalTime();

				List<Trip> Filtered;

				await using( var Db = new CarrierDb( context ) )
				{
					var Trips = Db.SearchTripsByLastUpdatedAndStatus( YesterdayStart, YesterdayEnd, STATUS.NEW, STATUS.DELETED );

					Filtered = ( from T in Trips
					             where T.Status1 is STATUS.NEW or STATUS.ACTIVE or STATUS.DISPATCHED or STATUS.PICKED_UP or STATUS.FINALISED or STATUS.DELETED
					                   && ( T.TripId.StartsWith( "IMP", StringComparison.OrdinalIgnoreCase ) || T.TripId.StartsWith( "Z", StringComparison.OrdinalIgnoreCase ) )
					             select T ).ToList();

					foreach( var Trip in Filtered )
					{
						var TripItems = Trip.Packages.SelectMany( p => p.Items ).ToDictionary( item => item.Barcode, item => item );

						List<TripItem> ToAdd    = [],
						               ToRemove = [];

						foreach( var (Key, TripItem) in TripItems )
						{
							var (Ok, _, Packet, PacketDescription, Quantity) = Db.GetPacketFromCarton( Key );

							if( Ok )
							{
								var Original = TripItem.Original;
								var Pieces   = TripItem.Pieces;

								if( ( Original != 0 ) || ( Pieces != 0 ) )
								{
									Original *= Quantity;
									Pieces   *= Quantity;

									if( TripItems.TryGetValue( Packet, out var Item ) )
									{
										Item.Original += Original;
										Item.Pieces   += Pieces;
									}
									else if( ( Original != 0 ) || ( Pieces != 0 ) ) // Just in case
									{
										ToAdd.Add( new TripItem
										           {
											           Barcode     = Packet,
											           Description = PacketDescription,
											           Original    = Original,
											           Pieces      = Pieces
										           } );
									}
								}
								ToRemove.Add( TripItem );
							}
						}

						foreach( var Item in ToRemove )
							TripItems.Remove( Item.Barcode );

						foreach( var Item in ToAdd )
							TripItems.Add( Item.Barcode, Item );

						List<TripPackage> Packages =
						[
							new()
							{
								Items = ( from Item in TripItems.Values
								          select Item ).ToList()
							}
						];

						Trip.Packages = Packages;
					}
				}
				var Csv = new Csv();

				var Ndx = 0;
				var Row = Csv[ Ndx++ ]; // Header row

				Row[ 0 ]  = "Date";
				Row[ 1 ]  = "IDS trip ID";
				Row[ 2 ]  = "Order Number";
				Row[ 3 ]  = "Status";
				Row[ 4 ]  = "Modified Date";
				Row[ 5 ]  = "Location Barcode";
				Row[ 6 ]  = "Customer Name";
				Row[ 7 ]  = "Product Code";
				Row[ 8 ]  = "Product Desc";
				Row[ 9 ]  = "Original Pieces";
				Row[ 10 ] = "Scanned Pieces";
				Row[ 11 ] = "Variance";

				Row = Csv[ Ndx++ ];

				var UpdateRow = false;

				void DoCompanyInfo( Trip trip )
				{
					Row[ 0 ].AsString = $"{trip.CallTime:dd/MM/yyyy}";

					var TripId = trip.TripId;
					Row[ 1 ].AsString = TripId;

					if( TripId.StartsWith( "IMP" ) )
					{
						var OrderNumber = TripId.Replace( "IMP", "", StringComparison.OrdinalIgnoreCase ).TrimStart( '0' );
						Row[ 2 ].AsString = OrderNumber;
					}
					else
						Row[ 2 ].AsString = "";

					Row[ 3 ].AsString = trip.Status1.AsString();
					Row[ 4 ].AsString = ToSydneyDateAsString( trip.LastModified );
					Row[ 5 ].AsString = trip.PickupAddressBarcode;
					Row[ 6 ].AsString = trip.PickupCompanyName;
				}

				foreach( var Trip in Filtered )
				{
					if( UpdateRow )
						Row = Csv[ Ndx++ ];

					UpdateRow = true;

					DoCompanyInfo( Trip );

					foreach( var Package in Trip.Packages )
					{
						foreach( var Item in Package.Items )
						{
							DoCompanyInfo( Trip );

							var Original = Item.Original;
							var Pieces   = Item.Pieces;
							var Diff     = Original - Pieces;

							Row[ 7 ].AsString = Item.Barcode;
							Row[ 8 ].AsString = Item.Description;
							Row[ 9 ]          = $"{Original:N0}";
							Row[ 10 ]         = $"{Pieces:N0}";
							Row[ 11 ]         = Diff != 0 ? $"{Diff:N0}" : "";

							Row       = Csv[ Ndx++ ];
							UpdateRow = false;
						}
					}
				}

				var CsvName = $"{YesterdayDateTime:yyyy-MM-dd_hh-mm-ss}.csv";

				using var LogCsvStream = new MemoryStream();

				using( var CsvStream = new MemoryStream() )
				{
					Csv.Write( Csv, CsvStream, Encoding.ASCII );
					CsvStream.Position = 0;
					await CsvStream.CopyToAsync( LogCsvStream );
					CsvStream.Position = 0;
					// To AWS
					var AClient = new Client( context, ACCESS_KEY, SECRET_KEY, REGION );
					await AClient.SaveStreamAsync( CsvStream, BUCKET_NAME, $"{FOLDER}/{CsvName}" );
				}

				LogCsvStream.Position = 0;
				using var Reader = new StreamReader( LogCsvStream, Encoding.ASCII );
				var       Text   = await Reader.ReadToEndAsync();

				AmazonLogs.AddCsv( "CCB2B Csv Export", context.CarrierId, YesterdayStart, CsvName, Text );
			}
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}
}