﻿#nullable enable

using Exports.Sage;

namespace Exports;

public class Export
{
	public abstract class ExportBase
	{
		protected IRequestContext Context { get; }
		protected ExportArgs      Args    { get; }

		protected ExportBase( IRequestContext context, ExportArgs args )
		{
			Context = context;
			Args    = args;
		}

		public abstract string Export();
	}

	private ExportBase? ExportClass { get; }

	public Export( IRequestContext context, ExportArgs args )
	{
		try
		{
			ExportClass = Create( context, args );

			if( ExportClass is null )
				throw new Exception( $"Unknown export name: {args.ExportName}" );
		}
		catch( Exception Exception )
		{
			context.SystemLogException( Exception );
		}
	}

	private static ExportBase? Create( IRequestContext context, ExportArgs args )
	{
		return args.ExportName.NullTrim().ToLower() switch
			   {
				   "sage300" => new Sage300( context, args ),
				   _         => null
			   };
	}

	public string Run() => ExportClass?.Export() ?? "";
}