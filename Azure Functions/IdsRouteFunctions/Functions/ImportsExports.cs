﻿namespace IdsRouteFunctions.Functions;

internal class ImportsExports
{
	// Run once a minute
	[FunctionName( nameof( ImportsExports ) )]
	public static async Task Run( [TimerTrigger( "0 * * * * *" )] TimerInfo schedulesTimer, ILogger log )
	{
		CachedLogin.SetClientDelayed();
		await Azure.Client.RequestCheckForImportsAndExports();
	}
}