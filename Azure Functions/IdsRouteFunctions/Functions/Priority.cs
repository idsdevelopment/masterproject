namespace IdsRouteFunctions;

/*

#if !DEBUG || (DEBUG && PRIORITY)
public static class PriorityDriverCommissionFunction
{
	[FunctionName( nameof( PriorityDriverCommissionFunction ) )]
#if DEBUG
	public static async Task Run( [TimerTrigger( "0 * * * * *" )] TimerInfo myTimer, ILogger log )
#else
	public static async Task Run( [TimerTrigger( "0 5 * * * *" )] TimerInfo myTimer, ILogger log ) // 5 minutes past the hour
#endif
	{
		var EasternTime = TimeZones.EasternDateTime;

	#if !DEBUG
		if( EasternTime.Hour == 0 ) // Run at Midnight
	#endif
			await new DriverCommission().Execute( EasternTime.AddDays( -2 ).StartOfDay() );
	}
}
#endif
*/