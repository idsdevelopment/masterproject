﻿namespace IdsRouteFunctions;

public static class TripLimbo
{
	// Run once a minute
	[FunctionName( nameof( TripLimbo ) )]
	public static async Task Run( [TimerTrigger( "0 15 0 * * *" )] TimerInfo tripLimboTimer, ILogger log ) // 15 mins past midnight. Spread load
	{
		CachedLogin.SetClientDelayed();
		await Azure.Client.RequestProcessLimbo();
	}
}