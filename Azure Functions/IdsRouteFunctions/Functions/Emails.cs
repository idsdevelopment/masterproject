﻿namespace IdsRouteFunctions;

public static class Emails
{
	// Run once a minute
	[FunctionName( nameof( Emails ) )]
	public static async Task Run( [TimerTrigger( "30 * * * * *" )] TimerInfo emailsTimer, ILogger log ) // 30 second past the minute spread the load
	{
		CachedLogin.SetClientDelayed();
		await Azure.Client.RequestProcessEmails();
	}
}

public static class SentEmails
{
	// Run once a day at 8am UTC (midnight PST)
	[FunctionName( nameof( SentEmails ) )]
	public static async Task Run( [TimerTrigger( "0 0 8 * * *" )] TimerInfo emailsTimer, ILogger log )
	{
		CachedLogin.SetClientDelayed();
		await Azure.Client.RequestCleanupSentEmails();
	}
}