using IdsRouteFunctions.Pml;

namespace IdsRouteFunctions;

public static class CleanupOldVerifiedTrips
{
	[FunctionName( nameof( CleanupOldVerifiedTrips ) )]
	public static async Task Run( [TimerTrigger( "0 0 * * * *" )] TimerInfo cleanupOldVerifiedTripsTimer, ILogger log )
	{
		CachedLogin.SetClientDelayed();
		var OzTime = TimeZones.AusEasternStandardTime;
		await new Pml.Trips.CleanupOldVerifiedTrips().Execute( OzTime );
	}
}

public static class Ccb2BReport
{
	[FunctionName( nameof( Ccb2BReport ) )]
	public static async Task Run( [TimerTrigger( "0 1 * * * *" )] TimerInfo ccb2BReportTimer, ILogger log )
	{
		CachedLogin.SetClientDelayed();
		await Reports.Ccb2BReport();
	}
}