﻿namespace IdsRouteFunctions.Functions;

internal static class ScheduledTrips
{
	// Run once a minute
	[FunctionName( nameof( ScheduledTrips ) )]
	public static async Task Run( [TimerTrigger( "0 * * * * *" )] TimerInfo schedulesTimer, ILogger log )
	{
		CachedLogin.SetClientDelayed();
		await Azure.Client.RequestStartSchedule();
	}
}