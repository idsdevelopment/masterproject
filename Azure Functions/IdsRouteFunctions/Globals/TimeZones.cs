﻿namespace IdsRouteFunctions.Globals;

internal class TimeZones
{
	public static DateTimeOffset AusEasternStandardTime => TzNow( AusEasternStandardTimeInfo );
	public static DateTimeOffset EasternDateTime        => TzNow( EasternStandardTimeInfo );

	private static readonly TimeZoneInfo AusEasternStandardTimeInfo = TimeZoneInfo.FindSystemTimeZoneById( "AUS Eastern Standard Time" );
	private static readonly TimeZoneInfo EasternStandardTimeInfo    = TimeZoneInfo.FindSystemTimeZoneById( "Eastern Standard Time" );

	private static DateTimeOffset TzNow( TimeZoneInfo tzInfo ) => TimeZoneInfo.ConvertTime( DateTimeOffset.UtcNow, tzInfo );
}