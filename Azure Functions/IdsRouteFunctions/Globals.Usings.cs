﻿global using System;
global using Microsoft.Extensions.Logging;
global using IdsRouteFunctions.Globals;
global using Protocol.Data;
global using Utils;
global using System.Threading.Tasks;
global using AzureRemoteService;
global using Microsoft.Azure.WebJobs;