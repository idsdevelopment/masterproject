﻿namespace Priority;

internal static class Version
{
	public static readonly string VERSION = "Priority WebJobs 1.01";
}