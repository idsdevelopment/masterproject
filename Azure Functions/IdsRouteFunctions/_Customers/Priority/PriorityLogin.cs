﻿using IdsRouteFunctions;

namespace Priority;

internal class PriorityLogin
{
	public const string
	#if DEBUG
		CARRIER_ID = "prioritytest",
	#else
			CARRIER_ID = "priority",
	#endif
		USERNAME = "admin",
		PASSWORD = "window";

	// ReSharper disable once InconsistentNaming
	public static readonly string IDS_AUTHTOKEN = $"{CARRIER_ID}-{USERNAME}-{PASSWORD}";

	public static bool Login() => CachedLogin.Login( Version.VERSION, IDS_AUTHTOKEN );
}