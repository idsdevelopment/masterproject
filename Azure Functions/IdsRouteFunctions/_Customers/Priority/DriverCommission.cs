﻿#if TEST
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endif

namespace Priority.DriverCommission;

public class DriverCommission
{
	private const int MAX_TASKS                    = 5,
	                  COMMISSION_WINDOW_IN_MINUTES = 45;

	private const string PROGRAM    = "Driver Commission Web Job. VERSION 1.02",
	                     STAT       = "STAT",
	                     MULTI_STAT = "MULTISTAT",
	                     SWEEP      = "SWEEP",
	                     UNKNOWN    = "UNKNOWN";

	private static bool Running; // For debugging

	private readonly Dictionary<string, StaffShifts> CompanyShifts = new();

	public async Task Execute( DateTimeOffset date )
	{
		if( !Running )
		{
			try
			{
				Running = true;

				var Text = $"Starting Driver Commission for: {date:D}";
				Console.WriteLine( Text );

				if( PriorityLogin.Login() )
				{
					var Message = new WebLog
					              {
						              CarrierId   = PriorityLogin.CARRIER_ID.Capitalise(),
						              Program     = PROGRAM.Capitalise(),
						              Message     = Text,
						              LogDateTime = date
					              };

					await Azure.Client.RequestLogWebJob( Message );

					var StartTime = DateTime.UtcNow;

					try
					{
						while( true )
						{
							Thread.Sleep( 10_000 );

							// Now returns a single driver by account
							var Response = await Azure.Client.RequestPriorityGetUnCalculatedDriverCommission( date );

							/*
						var Response = await Azure.Client.RequestPriorityGetUnCalculatedDriverCommissionForDriver( new PriorityDriverCommissionRange
							                                                                                       {
							                                                                                           Driver = "10047"
							                                                                                       } );
*/
							if( Response is not null )
							{
								static bool IsUnknown( string text )
								{
									return text.Contains( UNKNOWN, StringComparison.OrdinalIgnoreCase );
								}

								// Remove unknown addresses
								var Unknowns = ( from R in Response
								                 where IsUnknown( R.PickupCompanyName )
								                       || IsUnknown( R.PickupAddressAddressLine1 ) || IsUnknown( R.PickupAddressAddressLine2 )
								                       || IsUnknown( R.PickupAddressCity ) || IsUnknown( R.DeliveryCompanyName )
								                       || IsUnknown( R.DeliveryAddressAddressLine1 )
								                       || IsUnknown( R.DeliveryAddressAddressLine2 ) || IsUnknown( R.DeliveryAddressCity )
								                 select R ).ToList();

								if( Unknowns.Count > 0 )
								{
									var BadIds = new List<string>();

									// Stop Looping
									foreach( var Unknown in Unknowns )
									{
										var TripId = Unknown.TripId;
										BadIds.Add( TripId );
										await Azure.Client.RequestSetCommissionCalculated( TripId );
									}

									Response = new TripList( from R in Response
									                         where !BadIds.Contains( R.TripId )
									                         select R );
								}

								if( Response.Count > 0 )
								{
									var R0     = Response[ 0 ];
									var Driver = R0.Driver.Trim();

									List<string> Accounts;

									lock( CompanyShifts )
									{
										Accounts = ( from R in Response
										             let A = R.AccountId
										             where !CompanyShifts.ContainsKey( A )
										             select A ).Distinct().ToList();
									}

									new Tasks.Task<string>().Run( Accounts, account =>
									                                        {
										                                        var CoShifts = Azure.Client.RequestGetCompanyShifts( account ).Result; // Run doesn't wait if async

										                                        lock( CompanyShifts )
											                                        CompanyShifts[ account ] = CoShifts;
									                                        } );

									bool InShift( string account, string serviceLevel, DateTimeOffset? pickupTime )
									{
										if( pickupTime is not null )
										{
											StaffShifts? Shifts;
											bool         Found;

											lock( CompanyShifts )
												Found = CompanyShifts.TryGetValue( account, out Shifts );

											if( Found && Shifts is not null )
												return ShiftExtensions.InShift( Shifts, serviceLevel, (DateTimeOffset)pickupTime );
										}
										return false;
									}

									var NotInSift = ( from T in Response
									                  where !InShift( T.AccountId, T.ServiceLevel, T.PickupTime )
									                  select T ).ToList();

									foreach( var Trip in NotInSift )
										await Azure.Client.RequestSetCommissionCalculated( Trip.TripId );

									var DriverTrips = ( from T in Response
									                    where InShift( T.AccountId, T.ServiceLevel, T.PickupTime )
									                    orderby T.PickupTime
									                    select T ).ToArray();

									if( DriverTrips.Length == 0 )
									{
										if( NotInSift.Count > 0 )
											Thread.Sleep( 3000 );
										continue;
									}

									var TripsInWindows = new List<List<Trip>>();

									DateTimeOffset UpperLimit = DateTimeOffset.MaxValue,
									               LowerLimit = DateTimeOffset.MinValue;

									List<Trip> ToAdd = new();
									var        First = true;

									foreach( var Trip in DriverTrips )
									{
										var PickupTime = Trip.PickupTime!.Value;

										void NewTimeFrame()
										{
											var Limit = PickupTime;

											LowerLimit = new DateTimeOffset( Limit.Year,
											                                 Limit.Month,
											                                 Limit.Day,
											                                 Limit.Hour,
											                                 Limit.Minute,
											                                 0,
											                                 0,
											                                 Limit.Offset );

											Limit = LowerLimit.AddMinutes( COMMISSION_WINDOW_IN_MINUTES );

											UpperLimit = new DateTimeOffset( Limit.Year,
											                                 Limit.Month,
											                                 Limit.Day,
											                                 Limit.Hour,
											                                 Limit.Minute,
											                                 59,
											                                 999,
											                                 Limit.Offset );

											ToAdd.Add( Trip );
										}

										if( First )
										{
											First = false;
											NewTimeFrame();
										}
										else if( ( PickupTime >= LowerLimit ) && ( PickupTime <= UpperLimit ) )
											ToAdd.Add( Trip );
										else
										{
											if( ToAdd.Count > 0 )
											{
												TripsInWindows.Add( ToAdd );
												ToAdd = new List<Trip>();
											}
											NewTimeFrame();
										}
									}

									if( ToAdd.Count > 0 )
										TripsInWindows.Add( ToAdd );

									foreach( var TripsInWindow in TripsInWindows )
									{
										var TripAndShiftByDestinationAddress = new Dictionary<string, List<(Trip, StaffShift)>>();

										for( var State = STATE.STAT; State < STATE.END; State++ )
										{
											static string FixAddress( string addr )
											{
												addr = addr.TrimToUpper().AlphaNumericAndSpaces( true, true );

												if( addr.LastIndexOf( " RD ", StringComparison.Ordinal ) >= 0 )
													return addr.ReplaceLastOccurrence( " RD ", " ROAD " );

												if( addr.LastIndexOf( " ST ", StringComparison.Ordinal ) >= 0 )
													return addr.ReplaceLastOccurrence( " ST ", " STREET " );

												if( addr.LastIndexOf( " AVE ", StringComparison.Ordinal ) >= 0 )
													return addr.ReplaceLastOccurrence( " AVE ", " AVENUE " );

												if( addr.LastIndexOf( " DR ", StringComparison.Ordinal ) >= 0 )
													return addr.ReplaceLastOccurrence( " DR ", " DRIVE " );

												if( addr.LastIndexOf( " RT ", StringComparison.Ordinal ) >= 0 )
													return addr.ReplaceLastOccurrence( " RT ", " ROUTE " );

												return addr.LastIndexOf( " LN ", StringComparison.Ordinal ) >= 0 ? addr.ReplaceLastOccurrence( " LN ", " LANE " ) : addr;
											}

											foreach( var TripInWindow in TripsInWindow )
											{
												// Filter Sweeps
												if( string.Compare( TripInWindow.ServiceLevel.Trim(), SWEEP, StringComparison.OrdinalIgnoreCase ) == 0 )
												{
													if( State != STATE.SWEEP )
														continue;
												}
												else
												{
													if( State == STATE.SWEEP )
														continue;
												}

												var Address = FixAddress( $"{TripInWindow.DeliveryAddressAddressLine1} {TripInWindow.DeliveryAddressCity}" );

												if( !TripAndShiftByDestinationAddress.TryGetValue( Address, out var TripShift ) )
													TripAndShiftByDestinationAddress[ Address ] = TripShift = new List<(Trip, StaffShift)>();

												StaffShifts? Shifts;
												bool         Found;

												lock( CompanyShifts )
													Found = CompanyShifts.TryGetValue( TripInWindow.AccountId, out Shifts );

												if( Found && Shifts is not null )
												{
													var PuTime = TripInWindow.PickupTime;

													if( PuTime is not null )
													{
														foreach( var Shift in Shifts )
														{
															if( ShiftExtensions.InShift( Shift, TripInWindow.ServiceLevel, (DateTimeOffset)PuTime ) )
															{
																TripShift.Add( ( TripInWindow, Shift ) );
																break;
															}
														}
													}
												}
											}

											if( TripAndShiftByDestinationAddress.Count == 0 )
												continue; // Next State

											var Addresses = new RouteOptimisationAddresses
											                {
												                TravelMode = RouteBase.TRAVEL_MODE.SHORTEST
												                             | RouteBase.TRAVEL_MODE.AVOID_FERRIES
												                             | RouteBase.TRAVEL_MODE.AVOID_UNPAVED_ROADS
											                };

											First = true;

											var PickupTime = DateTimeOffset.MaxValue;

											Trip? LastTrip  = null,
											      FirstTrip = null;

											void AddPickup( TripUpdate t )
											{
												Addresses.Companies.Add( new Company
												                         {
													                         CompanyName  = t.PickupCompanyName,
													                         Suite        = t.PickupAddressSuite,
													                         AddressLine1 = t.PickupAddressAddressLine1,
													                         AddressLine2 = t.PickupAddressAddressLine2,
													                         City         = t.PickupAddressCity,
													                         Region       = t.PickupAddressRegion,
													                         Country      = t.PickupAddressCountry,
													                         CountryCode  = t.PickupAddressCountryCode,
													                         Vicinity     = t.PickupAddressVicinity,
													                         Latitude     = (decimal)t.PickupLatitude,
													                         Longitude    = (decimal)t.PickupLongitude
												                         } );
											}

											void AddDelivery( TripUpdate t )
											{
												Addresses.Companies.Add( new Company
												                         {
													                         CompanyName  = t.DeliveryCompanyName,
													                         Suite        = t.DeliveryAddressSuite,
													                         AddressLine1 = t.DeliveryAddressAddressLine1,
													                         AddressLine2 = t.DeliveryAddressAddressLine2,
													                         City         = t.DeliveryAddressCity,
													                         Region       = t.DeliveryAddressRegion,
													                         Country      = t.DeliveryAddressCountry,
													                         CountryCode  = t.DeliveryAddressCountryCode,
													                         Vicinity     = t.DeliveryAddressVicinity,
													                         Latitude     = (decimal)t.DeliveryLatitude,
													                         Longitude    = (decimal)t.DeliveryLongitude
												                         } );
											}

											var TripIds = new List<string>();

											foreach( var T in TripsInWindow )
											{
												TripIds.Add( T.TripId );

												LastTrip = T;

												var PTime = T.PickupTime!.Value;

												if( PTime < PickupTime )
													PickupTime = PTime;

												if( First )
												{
													First = false;

													FirstTrip = T;
													AddPickup( T );
												}

												AddDelivery( T );
											}

											//Shouldn't happen
											if( FirstTrip is null || LastTrip is null )
												continue;

											if( State == STATE.SWEEP )
											{
												AddDelivery( LastTrip );
												AddPickup( FirstTrip );
											}

											var GroupNumber = $"{(int)State}{PickupTime:yyyyMMddhhmmss}";

											var Distances = await Azure.Client.RequestDirections( Addresses );

											if( Distances is null )
												continue;

											if( Distances.Count == 0 ) // Probably in Hawaii
											{
												foreach( var TripId in TripIds )
													await Azure.Client.RequestSetCommissionCalculated( TripId );
												continue;
											}

											//Correct STAT / MUTISTAT / SWEEP
											var    Updates = new List<TripServiceLevelUpdate>();
											string ServiceLevel;

											if( State == STATE.SWEEP )
												ServiceLevel = SWEEP;
											else
											{
												ServiceLevel = Distances.Count switch
												               {
													               1 => STAT,
													               _ => MULTI_STAT
												               };
											}

											// Fix the trips service level
											foreach( var Trip in TripsInWindow )
											{
												if( string.Compare( Trip.ServiceLevel, ServiceLevel, StringComparison.OrdinalIgnoreCase ) != 0 )
												{
													Updates.Add( new TripServiceLevelUpdate
													             {
														             Program      = PROGRAM,
														             ServiceLevel = ServiceLevel,
														             TripId       = Trip.TripId
													             } );
												}
											}

											if( Updates.Count > 0 )
											{
												async void UpdateTrip( TripServiceLevelUpdate update )
												{
													await Azure.Client.RequestUpdateTripServiceLevel( update );
												}

												new Tasks.Task<TripServiceLevelUpdate>().Run( Updates, UpdateTrip, MAX_TASKS );
											}

											var   SortOrder = 0;
											Trip? LastAdded = null;

											foreach( var Distance in Distances )
											{
												// Want Miles Rounded To Zero Decimals
												var Measurement = new Measurement
												                  {
													                  KiloMetres = Distance.DistanceInKilometres
												                  };

												Measurement.Miles = Math.Ceiling( Measurement.Miles ); // Round up
												var RoundedKs = Measurement.KiloMetres;

												// ReSharper disable once UnusedLocalFunctionReturnValue
												bool UpdateDriverCommission( (Trip TripId, StaffShift Shift) tripShift )
												{
													var (Trip, Shift) = tripShift;

													LastAdded = Trip;

													var Update = new UpdateDriverCommission
													             {
														             Formula   = Shift.Formula,
														             Reference = GroupNumber,

														             // ReSharper disable once AccessToModifiedClosure
														             SortOrder = SortOrder++,

														             AccountId = FirstTrip.AccountId,

														             StaffId      = Driver,
														             TripId       = Trip.TripId,
														             ServiceLevel = ServiceLevel,

														             DateTime = PickupTime,

														             // ReSharper disable once AccessToModifiedClosure
														             Distance = RoundedKs,

														             PickupCompany = Trip.PickupCompanyName,
														             PickupStreet  = Trip.PickupAddressAddressLine1,
														             PickupCity    = Trip.PickupAddressCity,

														             DeliveryCompany = Trip.DeliveryCompanyName,
														             DeliveryStreet  = Trip.DeliveryAddressAddressLine1,
														             DeliveryCity    = Trip.DeliveryAddressCity,

														             Route = Distance.Route
													             };

													return Azure.Client.RequestPriorityUpdateDriverCommission( Update ).Result;
												}

												var Address = FixAddress( $"{Distance.ToCompany.AddressLine1} {Distance.ToCompany.City}" );

												if( TripAndShiftByDestinationAddress.TryGetValue( Address, out var CoTrips ) )
												{
													foreach( var TripShift in CoTrips )
													{
														UpdateDriverCommission( TripShift );
														RoundedKs = 0;
													}
												}
												else if( ( State == STATE.SWEEP ) && LastAdded is not null )
												{
													StaffShifts? Shifts;
													bool         Found;

													lock( CompanyShifts )
														Found = CompanyShifts.TryGetValue( FirstTrip.AccountId, out Shifts );

													if( Found && Shifts is not null )
													{
														foreach( var Shift in Shifts )
														{
															if( string.Compare( Shift.ServiceLevel, ServiceLevel, StringComparison.OrdinalIgnoreCase ) == 0 )
															{
																UpdateDriverCommission( ( new Trip
																                          {
																	                          TripId                      = "--- RETURN TO BASE ---",
																	                          DeliveryCompanyName         = FirstTrip.PickupCompanyName.TrimToUpper(),
																	                          DeliveryAddressAddressLine1 = FirstTrip.PickupAddressAddressLine1.TrimToUpper(),
																	                          DeliveryAddressCity         = FirstTrip.PickupAddressCity.TrimToUpper(),

																	                          PickupCompanyName         = LastAdded.DeliveryCompanyName.TrimToUpper(),
																	                          PickupAddressAddressLine1 = LastAdded.DeliveryAddressAddressLine1.TrimToUpper(),
																	                          PickupAddressCity         = LastAdded.DeliveryAddressCity.TrimToUpper()
																                          }, Shift ) );
																break;
															}
														}
													}
												}
											}
										}
									}
								}
								else
									break;
							}
						}
					}
					catch( Exception E )
					{
						var Txt = $"Exception thrown in Driver Commission - {E}";
					#if TEST
					Assert.Fail( Mesge );
					#else
						Console.WriteLine( Txt );

						Message.Message = Txt;
						await Azure.Client.RequestLogWebJob( Message );
					#endif
					}
					var TimeTaken = DateTime.UtcNow - StartTime;

					var Mesge = $"Driver commission ran for: {TimeTaken:g}";
					Console.WriteLine( Mesge );

					Message.Message = Mesge;
					await Azure.Client.RequestLogWebJob( Message );
				}
				else
					Console.WriteLine( "Driver commission - Cannot Login" );
			}
			catch( Exception E )
			{
				var Message = $"Exception thrown in Driver Commission - {E}";
			#if TEST
					Assert.Fail( Message );
			#else
				Console.WriteLine( Message );
			#endif
			}
			finally
			{
				Running = false;
			}
		}
	}

	private enum STATE
	{
		STAT,
		SWEEP,
		END
	}
}