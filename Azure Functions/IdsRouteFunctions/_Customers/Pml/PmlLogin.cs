﻿using IdsRouteFunctions;

namespace Pml;

internal class PmlLogin
{
	public const string
	#if DEBUG
		CARRIER_ID = "pmltest",
	#else
		CARRIER_ID = "pml",
	#endif
		USERNAME = "foxlin789",
		PASSWORD = "390equator";

	// ReSharper disable once InconsistentNaming
	public static readonly string IDS_AUTHTOKEN = $"{CARRIER_ID}-{USERNAME}-{PASSWORD}";

	public static bool Login() => CachedLogin.Login( Version.VERSION, IDS_AUTHTOKEN );
}