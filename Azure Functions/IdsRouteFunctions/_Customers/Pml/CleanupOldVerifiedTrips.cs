using Pml;

namespace IdsRouteFunctions.Pml.Trips;

public class CleanupOldVerifiedTrips
{
	public async Task Execute( DateTimeOffset easterTime )
	{
		if( PmlLogin.Login() )
		{
			var Date = easterTime.EndOfDay().AddMonths( -1 );

			var Text = $"Finalising Satchels On/Before: {Date:f}\r\n\r\n";

			var IdListAndProgram = await Azure.Client.RequestFinaliseManuallyVerifiedSatchels( Date );

			var Count = IdListAndProgram.TripIds.Count;

			if( Count > 0 )
			{
				Text += $"Finalised Satchels ({Count}):\r\n";

				foreach( var TripId in IdListAndProgram.TripIds )
					Text += $"{TripId}\r\n";
			}
			else
				Text += "No Satchels Finalised\r\n";

			var Message = new WebLog
			              {
				              CarrierId   = PmlLogin.CARRIER_ID.Capitalise(),
				              Program     = IdListAndProgram.Program.Capitalise(),
				              Message     = Text,
				              LogDateTime = easterTime
			              };

			await Azure.Client.RequestLogWebJob( Message );
		}
	}
}