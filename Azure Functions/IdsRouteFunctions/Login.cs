﻿namespace IdsRouteFunctions;

public class CachedLogin
{
	private static readonly MemoryCache<string, bool> LoginCache = new();

	public static void SetClient( string version )
	{
		Azure.NoListeners = true;
		Azure.Slot        = Azure.SLOT.SCHEDULES;
		Azure.Client      = new Azure.InternalClient( version, "", "" );
	}

	public static void SetClient()
	{
		SetClient( "" );
	}

	public static void SetClientDelayed()
	{
		RandomDelay.Delay();
		SetClient( "" );
	}

	public static bool Login( string version, string authToken )
	{
		var RetVal = false;

		try
		{
			var (Ok, _) = LoginCache.TryGetValue( authToken );

			if( !Ok )
			{
				SetClient( version );

				var Parts = authToken.Split( new[] { '-' }, StringSplitOptions.None );

				if( Parts.Length == 3 )
				{
					var Status = Azure.LogIn( version, Parts[ 0 ], Parts[ 1 ], Parts[ 2 ] ).Result;

					if( Status.Status == Azure.AZURE_CONNECTION_STATUS.OK )
					{
						LoginCache.AddSliding( authToken, true, TimeSpan.FromHours( 1 ) );
						RetVal = true;
					}
				}
			}
			else
				RetVal = true;
		}
		finally
		{
			if( !RetVal )
				Console.WriteLine( $"Cannot Login - {version}" );
		}

		return RetVal;
	}
}