﻿namespace IdsRouteFunctions._Customers.Metro;

public static class CheckIds1Connection
{
	[FunctionName( nameof( CheckIds1Connection ) )]
	public static async Task Run( [TimerTrigger( "0 */10 * * * *" )] TimerInfo checkIds1ConnectionTimer, ILogger log )
	{
		CachedLogin.SetClientDelayed();
		await Azure.Client.RequestCheckMetroIds1Connection();
	}
}