﻿using Pml;

namespace IdsRouteFunctions.Pml;

public class Reports
{
	public static async Task Ccb2BReport()
	{
		if( PmlLogin.Login() )
			await Azure.Client.RequestPML_ExportToCCB2B();
	}
}