﻿namespace Protocol.Data;

public enum STAFF_TYPE
{
	CARRIER,
	CUSTOMER
}

public class StaffLookup : PreFetch
{
	public STAFF_TYPE St { get; set; } = STAFF_TYPE.CARRIER;

	[JsonIgnore]
	public STAFF_TYPE Type
	{
		get => St;
		set => St = value;
	}
}

public class StaffLookupSummary
{
	public STAFF_TYPE St { get; set; } = STAFF_TYPE.CARRIER;

	[JsonIgnore]
	public STAFF_TYPE Type
	{
		get => St;
		set => St = value;
	}

	public string S { get; set; } = "";

	[JsonIgnore]
	public string StaffId
	{
		get => S;
		set => S = value;
	}

	public string F { get; set; } = "";

	[JsonIgnore]
	public string FirstName
	{
		get => F;
		set => F = value;
	}

	public string L { get; set; } = "";

	[JsonIgnore]
	public string LastName
	{
		get => L;
		set => L = value;
	}

	public string A { get; set; } = "";

	[JsonIgnore]
	public string AddressLine1
	{
		get => A;
		set => A = value;
	}

	[JsonIgnore]
	public string FullName => $"{FirstName} {LastName}";
}

public class StaffLookupSummaryList : List<StaffLookupSummary>;

public class UpdateStaffMember : ProgramData
{
	public Staff Staff;

	public UpdateStaffMember(string programName, Staff staff) : base(programName)
	{
		Staff = staff;
	}
}

public class UpdateStaffMemberWithRolesAndZones : UpdateStaffMember
{
	public bool FixRoleNames { get; set; }
	public List<Role> Roles { get; set; } = [];
	public List<Zone> Zones { get; set; } = [];

	public UpdateStaffMemberWithRolesAndZones(string programName, Staff staff) : base(programName, staff)
	{
	}
}

public class Staff
{
	public STAFF_TYPE St { get; set; } = STAFF_TYPE.CARRIER;

	[JsonIgnore]
	public STAFF_TYPE Type
	{
		get => St;
		set => St = value;
	}

	public string Si { get; set; } = "";

	[JsonIgnore]
	public string StaffId
	{
		get => Si;
		set => Si = value;
	}


	public bool E { get; set; }

	[JsonIgnore]
	public bool Enabled
	{
		get => E;
		set => E = value;
	}


	public string P { get; set; } = "";

	[JsonIgnore]
	public string Password
	{
		get => P;
		set => P = value;
	}


	public string F { get; set; } = "";

	[JsonIgnore]
	public string FirstName
	{
		get => F;
		set => F = value;
	}


	public string L { get; set; } = "";

	[JsonIgnore]
	public string LastName
	{
		get => L;
		set => L = value;
	}


	public string M { get; set; } = "";

	[JsonIgnore]
	public string MiddleNames
	{
		get => M;
		set => M = value;
	}


	public string T { get; set; } = "";

	[JsonIgnore]
	public string TaxNumber
	{
		get => T;
		set => T = value;
	}


	public Address A { get; set; } = new();

	[JsonIgnore]
	public Address Address
	{
		get => A;
		set => A = value;
	}


	public string Ef { get; set; } = "";

	[JsonIgnore]
	public string EmergencyFirstName
	{
		get => Ef;
		set => Ef = value;
	}


	public string El { get; set; } = "";

	[JsonIgnore]
	public string EmergencyLastName
	{
		get => El;
		set => El = value;
	}


	public string Ep { get; set; } = "";

	[JsonIgnore]
	public string EmergencyPhone
	{
		get => Ep;
		set => Ep = value;
	}


	public string Em { get; set; } = "";

	[JsonIgnore]
	public string EmergencyMobile
	{
		get => Em;
		set => Em = value;
	}


	public string Ee { get; set; } = "";

	[JsonIgnore]
	public string EmergencyEmail
	{
		get => Ee;
		set => Ee = value;
	}


	public decimal M1 { get; set; }

	[JsonIgnore]
	public decimal MainCommission1
	{
		get => M1;
		set => M1 = value;
	}


	public decimal M2 { get; set; }

	[JsonIgnore]
	public decimal MainCommission2
	{
		get => M2;
		set => M2 = value;
	}


	public decimal M3 { get; set; }

	[JsonIgnore]
	public decimal MainCommission3
	{
		get => M3;
		set => M3 = value;
	}


	public decimal O1 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission1
	{
		get => O1;
		set => O1 = value;
	}


	public decimal O2 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission2
	{
		get => O2;
		set => O2 = value;
	}

	public decimal O3 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission3
	{
		get => O3;
		set => O3 = value;
	}

	public decimal O4 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission4
	{
		get => O4;
		set => O4 = value;
	}

	public decimal O5 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission5
	{
		get => O5;
		set => O5 = value;
	}

	public decimal O6 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission6
	{
		get => O6;
		set => O6 = value;
	}

	public decimal O7 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission7
	{
		get => O7;
		set => O7 = value;
	}

	public decimal O8 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission8
	{
		get => O8;
		set => O8 = value;
	}

	public decimal O9 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission9
	{
		get => O9;
		set => O9 = value;
	}

	public decimal O10 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission10
	{
		get => O10;
		set => O10 = value;
	}

	public decimal O11 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission11
	{
		get => O11;
		set => O11 = value;
	}

	public decimal O12 { get; set; }

	[JsonIgnore]
	public decimal OverrideCommission12
	{
		get => O12;
		set => O12 = value;
	}

	public bool Ok; // Used by lookups

	public Staff()
	{
	}

	public Staff(Staff s)
	{
		Ok = s.Ok;
		StaffId = s.StaffId;
		Enabled = s.Enabled;
		Password = s.Password;
		FirstName = s.FirstName;
		LastName = s.LastName;
		MiddleNames = s.MiddleNames;
		TaxNumber = s.TaxNumber;
		Address = s.Address;

		EmergencyFirstName = s.EmergencyFirstName;
		EmergencyLastName = s.EmergencyLastName;
		EmergencyPhone = s.EmergencyPhone;
		EmergencyMobile = s.EmergencyMobile;
		EmergencyEmail = s.EmergencyEmail;

		MainCommission1 = s.MainCommission1;
		MainCommission2 = s.MainCommission2;
		MainCommission3 = s.MainCommission3;

		OverrideCommission1 = s.OverrideCommission1;
		OverrideCommission2 = s.OverrideCommission2;
		OverrideCommission3 = s.OverrideCommission3;
		OverrideCommission4 = s.OverrideCommission4;
		OverrideCommission5 = s.OverrideCommission5;
		OverrideCommission6 = s.OverrideCommission6;
		OverrideCommission7 = s.OverrideCommission7;
		OverrideCommission8 = s.OverrideCommission8;
		OverrideCommission9 = s.OverrideCommission9;
		OverrideCommission10 = s.OverrideCommission10;
		OverrideCommission11 = s.OverrideCommission11;
		OverrideCommission12 = s.OverrideCommission12;
	}
}

public class StaffList : List<Staff>;

public class StaffAndRoles : Staff
{
	public List<Role> R { get; set; } = new Roles();

	[JsonIgnore]
	public List<Role> Roles
	{
		get => R;
		set => R = value;
	}

	public StaffAndRoles()
	{
	}

	public StaffAndRoles(Staff staff) : base(staff)
	{
	}

	public StaffAndRoles(Staff staff, List<Role> roles) : base(staff)
	{
		Roles = roles;
	}

	public StaffAndRoles(StaffAndRoles staffAndRoles) : base(staffAndRoles)
	{
		Roles.AddRange(staffAndRoles.Roles);
	}
}

public class StaffAndRolesList : List<StaffAndRoles>;