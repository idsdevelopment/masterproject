﻿namespace Protocol.Data;

public class Note
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string NoteId
	{
		get => N;
		set => N = value;
	}


	public string T { get; set; } = "";

	[JsonIgnore]
	public string Text
	{
		get => T;
		set => T = value;
	}
}

public class Notes : List<Note>;