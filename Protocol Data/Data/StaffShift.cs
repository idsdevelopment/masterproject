﻿namespace Protocol.Data;

public enum DAY_OF_WEEK : short
{
	ALL_DAYS  = -1,
	SUNDAY    = DayOfWeek.Sunday,
	MONDAY    = DayOfWeek.Monday,
	TUESDAY   = DayOfWeek.Tuesday,
	WEDNESDAY = DayOfWeek.Wednesday,
	THURSDAY  = DayOfWeek.Thursday,
	FRIDAY    = DayOfWeek.Friday,
	SATURDAY  = DayOfWeek.Saturday
}

public static class ShiftExtensions
{
	private const string ALL_DAYS = "Every Day";

	public static readonly List<string> DAY_OF_WEEK_AS_LIST =
	[
		ALL_DAYS,
		DAY_OF_WEEK.SUNDAY.AsString(),
		DAY_OF_WEEK.MONDAY.AsString(),
		DAY_OF_WEEK.TUESDAY.AsString(),
		DAY_OF_WEEK.WEDNESDAY.AsString(),
		DAY_OF_WEEK.THURSDAY.AsString(),
		DAY_OF_WEEK.FRIDAY.AsString(),
		DAY_OF_WEEK.SATURDAY.AsString()
	];

	public static DayOfWeek ToDayOfWeek( this DAY_OF_WEEK dow ) => dow == DAY_OF_WEEK.ALL_DAYS ? DayOfWeek.Sunday : (DayOfWeek)dow;

	public static string AsString( this DAY_OF_WEEK dow ) => dow == DAY_OF_WEEK.ALL_DAYS ? ALL_DAYS : ( (DayOfWeek)dow ).ToString();

	public static TimeSpan Duration( DAY_OF_WEEK startDay, TimeSpan startTime, DAY_OF_WEEK endDay, TimeSpan endTime )
	{
		if( ( startDay == DAY_OF_WEEK.ALL_DAYS ) || ( endDay == DAY_OF_WEEK.ALL_DAYS ) )
		{
			if( startTime > endTime )
				( startTime, endTime ) = ( endTime, startTime );
			return endTime - startTime;
		}

		var StartTime = new DateTime( startTime.Ticks ).AddDays( (int)startDay );
		var EndTime   = new DateTime( endTime.Ticks ).AddDays( (int)endDay );

		if( EndTime < StartTime )
			EndTime = EndTime.AddDays( 7 );

		return new TimeSpan( ( EndTime - StartTime ).Ticks );
	}

	public static TimeSpan Duration( int startDay, TimeSpan startTime, int endDay, TimeSpan endTime ) => Duration( (DAY_OF_WEEK)( startDay - 1 ), startTime, (DAY_OF_WEEK)( endDay - 1 ), endTime );

	public static bool InShift( DateTimeOffset time, DAY_OF_WEEK startDay, TimeSpan startTime, DAY_OF_WEEK endDay, TimeSpan endTime )
	{
		if( ( startDay == DAY_OF_WEEK.ALL_DAYS ) || ( endDay == DAY_OF_WEEK.ALL_DAYS ) )
		{
			var Time = time.TimeOfDay;
			return ( Time >= startTime ) && ( Time <= endTime );
		}

		var StartTime = new DateTime( startTime.Ticks ).AddDays( (int)startDay );
		var EndTime   = new DateTime( endTime.Ticks ).AddDays( (int)endDay );

		if( EndTime < StartTime )
			EndTime = EndTime.AddDays( 7 );

		return ( time >= StartTime ) && ( time <= EndTime );
	}

	public static bool InShift( StaffShifts shifts, string serviceLevel, DateTimeOffset dateTime )
	{
		foreach( var Shift in shifts )
		{
			if( InShift( Shift, serviceLevel, dateTime ) )
				return true;
		}
		return false;
	}

	public static bool InShift( DateTimeOffset time, int startDay, TimeSpan startTime, int endDay, TimeSpan endTime ) => InShift( time, (DAY_OF_WEEK)startDay, startTime, (DAY_OF_WEEK)endDay, endTime );

	public static bool InShift( StaffShift shift, string serviceLevel, DateTimeOffset dateTime ) => ( string.Compare( shift.ServiceLevel.Trim(), serviceLevel.Trim(), StringComparison.OrdinalIgnoreCase ) == 0 )
	                                                                                                && InShift( dateTime, shift.StartDay, shift.Start, shift.EndDay, shift.End );
}

public class StaffShift
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string ShiftName
	{
		get => N;
		set => N = value;
	}

	public string F { get; set; } = "";

	[JsonIgnore]
	public string Formula
	{
		get => F;
		set => F = value;
	}


	public DAY_OF_WEEK Sd { get; set; } = DAY_OF_WEEK.ALL_DAYS;

	[JsonIgnore]
	public DAY_OF_WEEK StartDay
	{
		get => Sd;
		set => Sd = value;
	}


	public TimeSpan S { get; set; }

	[JsonIgnore]
	public TimeSpan Start
	{
		get => S;
		set => S = value;
	}


	public DAY_OF_WEEK Ed { get; set; } = DAY_OF_WEEK.ALL_DAYS;

	[JsonIgnore]
	public DAY_OF_WEEK EndDay
	{
		get => Ed;
		set => Ed = value;
	}

	public TimeSpan E { get; set; }

	[JsonIgnore]
	public TimeSpan End
	{
		get => E;
		set => E = value;
	}


	public string L { get; set; } = "";

	[JsonIgnore]
	public string ServiceLevel
	{
		get => L;
		set => L = value;
	}
}

public class StaffShifts : List<StaffShift>;

public class StaffShiftUpdate : StaffShift
{
	public bool D { get; set; }

	[JsonIgnore]
	public bool Delete
	{
		get => D;
		set => D = value;
	}


	public string O { get; set; } = "";

	[JsonIgnore]
	public string OriginalName
	{
		get => O;
		set => O = value;
	}
}

public class StaffShiftsUpdate : List<StaffShiftUpdate>
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}
}

public class UpdateCompanyShifts
{
	public List<string> S { get; set; } = [];

	[JsonIgnore]
	public List<string> Shifts
	{
		get => S;
		set => S = value;
	}


	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}


	public string C { get; set; } = "";

	[JsonIgnore]
	public string CompanyName
	{
		get => C;
		set => C = value;
	}
}