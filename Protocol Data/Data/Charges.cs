﻿namespace Protocol.Data;

public class Charge
{
	public bool D { get; set; }

	[JsonIgnore]
	public bool DeleteCharge
	{
		get => D;
		set => D = value;
	}

	public string Ci { get; set; } = "";

	[JsonIgnore]
	public string ChargeId
	{
		get => Ci;
		set => Ci = value;
	}

	public string L { get; set; } = "";

	[JsonIgnore]
	public string Label
	{
		get => L;
		set => L = value;
	}

	public string Ai { get; set; } = "";

	[JsonIgnore]
	public string AccountId
	{
		get => Ai;
		set => Ai = value;
	}

	public short S { get; set; }

	[JsonIgnore]
	public short SortIndex
	{
		get => S;
		set => S = value;
	}

	public bool It { get; set; }

	[JsonIgnore]
	public bool IsText
	{
		get => It;
		set => It = value;
	}

	public decimal V { get; set; }

	[JsonIgnore]
	public decimal Value
	{
		get => V;
		set => V = value;
	}

	public string T { get; set; } = "";

	[JsonIgnore]
	public string Text
	{
		get => T;
		set => T = value;
	}

	public bool Ap { get; set; }

	[JsonIgnore]
	public bool AlwaysApplied
	{
		get => Ap;
		set => Ap = value;
	}

	public short Ct { get; set; }

	[JsonIgnore]
	public short ChargeType
	{
		get => Ct;
		set => Ct = value;
	}

	public short Dt { get; set; }

	[JsonIgnore]
	public short DisplayType
	{
		get => Dt;
		set => Dt = value;
	}

	public bool Db { get; set; }

	[JsonIgnore]
	public bool DisplayOnWaybill
	{
		get => Db;
		set => Db = value;
	}

	public bool Di { get; set; }

	[JsonIgnore]
	public bool DisplayOnInvoice
	{
		get => Di;
		set => Di = value;
	}

	public bool De { get; set; }

	[JsonIgnore]
	public bool DisplayOnEntry
	{
		get => De;
		set => De = value;
	}

	public bool Ds { get; set; }

	[JsonIgnore]
	public bool DisplayOnDriversScreen
	{
		get => Ds;
		set => Ds = value;
	}

	public bool Iw { get; set; }

	[JsonIgnore]
	public bool IsWeightCharge
	{
		get => Iw;
		set => Iw = value;
	}

	public bool Iv { get; set; }

	[JsonIgnore]
	public bool IsVolumeCharge
	{
		get => Iv;
		set => Iv = value;
	}

	public bool Im { get; set; }

	[JsonIgnore]
	public bool IsMultiplier
	{
		get => Im;
		set => Im = value;
	}

	public bool Id { get; set; }

	[JsonIgnore]
	public bool IsDiscountable
	{
		get => Id;
		set => Id = value;
	}

	public bool If { get; set; }

	[JsonIgnore]
	public bool IsFormula
	{
		get => If;
		set => If = value;
	}

	public bool Ix { get; set; }

	[JsonIgnore]
	public bool IsTax
	{
		get => Ix;
		set => Ix = value;
	}

	public decimal Mn { get; set; }

	[JsonIgnore]
	public decimal Min
	{
		get => Mn;
		set => Mn = value;
	}

	public decimal Mx { get; set; }

	[JsonIgnore]
	public decimal Max
	{
		get => Mx;
		set => Mx = value;
	}

	public string F { get; set; } = "";

	[JsonIgnore]
	public string Formula
	{
		get => F;
		set => F = value;
	}

	public bool Ex { get; set; }

	[JsonIgnore]
	public bool ExcludeFromTaxes
	{
		get => Ex;
		set => Ex = value;
	}

	public bool Fc { get; set; }

	[JsonIgnore]
	public bool IsFuelSurcharge
	{
		get => Fc;
		set => Fc = value;
	}

	public bool Es { get; set; }

	[JsonIgnore]
	public bool ExcludeFromFuelSurcharge
	{
		get => Es;
		set => Es = value;
	}

	public bool Io { get; set; }

	[JsonIgnore]
	public bool IsOverride
	{
		get => Io;
		set => Io = value;
	}

	public string Op { get; set; } = "";

	[JsonIgnore]
	public string OverridenPackage
	{
		get => Op;
		set => Op = value;
	}

	public bool Ep { get; set; }

	[JsonIgnore]
	public bool ExcludeFromPayroll
	{
		get => Ep;
		set => Ep = value;
	}
}

public class TripCharge
{
	public string Ci { get; set; } = "";

	[JsonIgnore]
	public string ChargeId
	{
		get => Ci;
		set => Ci = value;
	}

	public decimal Q { get; set; }

	[JsonIgnore]
	public decimal Quantity
	{
		get => Q;
		set => Q = value;
	}

	public decimal V { get; set; }

	[JsonIgnore]
	public decimal Value
	{
		get => V;
		set => V = value;
	}

	public string T { get; set; } = "";

	[JsonIgnore]
	public string Text
	{
		get => T;
		set => T = value;
	}
}

public class Charges : List<Charge>;