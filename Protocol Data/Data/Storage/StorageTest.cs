﻿namespace Protocol.Data;

public class StorageTest
{
	public ACTION A { get; set; } = ACTION.NONE;

	[JsonIgnore]
	public ACTION Action
	{
		get => A;
		set => A = value;
	}

	public STORAGE S { get; set; } = STORAGE.BLOB;

	[JsonIgnore]
	public STORAGE Storage
	{
		get => S;
		set => S = value;
	}


	public Dictionary<string, string> M { get; set; } = new();

	[JsonIgnore]
	public Dictionary<string, string> MetaData
	{
		get => M;
		set => M = value;
	}


	public string N { get; set; } = "";

	[JsonIgnore]
	public string Name
	{
		get => N;
		set => N = value;
	}


	public List<string> D { get; set; } = [];

	[JsonIgnore]
	public List<string> Data
	{
		get => D;
		set => D = value;
	}


	public bool O { get; set; }

	[JsonIgnore]
	public bool Ok
	{
		get => O;
		set => O = value;
	}


	public string E { get; set; } = "";

	[JsonIgnore]
	public string ErrorMessage
	{
		get => E;
		set => E = value;
	}

	public enum ACTION
	{
		NONE,
		PUT,
		GET,
		LIST,
		DELETE,
		DELETE_BLOB,
		DELETE_CONTAINER,
		PUT_METADATA,
		GET_METADATA
	}


	public enum STORAGE
	{
		BLOB,
		APPEND_BLOB,
		QUEUE,
		TABLE,
		SYSTEM_LOG
	}
}