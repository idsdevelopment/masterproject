﻿namespace Protocol.Data;

public class Zone
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string Name
	{
		get => N;
		set => N = value;
	}


	public string A { get; set; } = "";

	[JsonIgnore]
	public string Abbreviation
	{
		get => A;
		set => A = value;
	}


	public int S { get; set; }

	[JsonIgnore]
	public int SortIndex
	{
		get => S;
		set => S = value;
	}

	public int Id { get; set; }

	[JsonIgnore]
	public int ZoneId
	{
		get => Id;
		set => Id = value;
	}
}

public class Zones : List<Zone>
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}

	public Zones()
	{
	}

	public Zones( string programName )
	{
		ProgramName = programName;
	}
}

public class ZoneNameList : List<string>
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}
}