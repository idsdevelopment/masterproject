﻿namespace Protocol.Data;

public class MenuPreference
{
	public bool A { get; set; }

	[JsonIgnore]
	public bool AutoOpenMenuTab
	{
		get => A;
		set => A = value;
	}

	public bool P { get; set; }

	[JsonIgnore]
	public bool IsPml
	{
		get => P;
		set => P = value;
	}

	public bool Pr { get; set; }

	[JsonIgnore]
	public bool IsPriority
	{
		get => Pr;
		set => Pr = value;
	}

	public bool I { get; set; }

	[JsonIgnore]
	public bool HasInventory
	{
		get => I;
		set => I = value;
	}
}

public class Preference
{
	public const byte BOOL       = 0,
	                  STRING     = 1,
	                  INT        = 2,
	                  DECIMAL    = 3,
	                  DATE_TIME  = 4,
	                  LABEL      = 5,
	                  BLOCK_TEXT = 6;

	public int I { get; set; }

	[JsonIgnore]
	public int Id
	{
		get => I;
		set => I = value;
	}

	public byte Dt { get; set; }

	[JsonIgnore]
	public byte DataType
	{
		get => Dt;
		set => Dt = value;
	}

	public string De { get; set; } = "";

	[JsonIgnore]
	public string Description
	{
		get => De;
		set => De = value;
	}

	public string Dp { get; set; } = "";

	[JsonIgnore]
	public string DevicePreference
	{
		get => Dp;
		set => Dp = value;
	}

	public bool Io { get; set; }

	[JsonIgnore]
	public bool IdsOnly
	{
		get => Io;
		set => Io = value;
	}

	public bool E { get; set; }

	[JsonIgnore]
	public bool Enabled
	{
		get => E;
		set => E = value;
	}

	public int Do { get; set; }

	[JsonIgnore]
	public int DisplayOrder
	{
		get => Do;
		set => Do = value;
	}

	public string Sv { get; set; } = "";

	[JsonIgnore]
	public string StringValue
	{
		get => Sv;
		set => Sv = value;
	}

	public int Iv { get; set; }

	[JsonIgnore]
	public int IntValue
	{
		get => Iv;
		set => Iv = value;
	}

	public decimal Dv { get; set; }

	[JsonIgnore]
	public decimal DecimalValue
	{
		get => Dv;
		set => Dv = value;
	}

	public DateTimeOffset Tv { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTimeValue
	{
		get => Tv;
		set => Tv = value;
	}

	public Preference()
	{
	}

	public Preference( Preference p )
	{
		Id               = p.Id;
		DataType         = p.DataType;
		Description      = p.Description;
		DevicePreference = p.DevicePreference;
		IdsOnly          = p.IdsOnly;
		Enabled          = p.Enabled;
		DisplayOrder     = p.DisplayOrder;
		StringValue      = p.StringValue;
		IntValue         = p.IntValue;
		DecimalValue     = p.DecimalValue;
		DateTimeValue    = p.DateTimeValue;
	}
}

public class Preferences : List<Preference>;

public class DevicePreferences
{
	public bool Es { get; set; }

	[JsonIgnore]
	public bool ExtraSounds
	{
		get => Es;
		set => Es = value;
	}

	public bool Cr { get; set; }

	[JsonIgnore]
	public bool ConfirmRemoval
	{
		get => Cr;
		set => Cr = value;
	}

	public bool Co { get; set; }

	[JsonIgnore]
	public bool CompulsoryArriveTime
	{
		get => Co;
		set => Co = value;
	}

	public bool Pa { get; set; }

	[JsonIgnore]
	public bool PickupArriveTime
	{
		get => Pa;
		set => Pa = value;
	}

	public bool Da { get; set; }

	[JsonIgnore]
	public bool DeliveryArriveTime
	{
		get => Da;
		set => Da = value;
	}

	public bool Pt { get; set; }

	[JsonIgnore]
	public bool PickupWaitTime
	{
		get => Pt;
		set => Pt = value;
	}

	public bool Dt { get; set; }

	[JsonIgnore]
	public bool DeliveryWaitTime
	{
		get => Dt;
		set => Dt = value;
	}

	public bool Bu { get; set; }

	[JsonIgnore]
	public bool BatchUndeliverableAtLocation
	{
		get => Bu;
		set => Bu = value;
	}

	public bool B1 { get; set; }

	[JsonIgnore]
	public bool BatchLocation
	{
		get => B1;
		set => B1 = value;
	}

	public bool Ip { get; set; }

	[JsonIgnore]
	public bool IsPml
	{
		get => Ip;
		set => Ip = value;
	}

	public bool Ir { get; set; }

	[JsonIgnore]
	public bool IsPriority
	{
		get => Ir;
		set => Ir = value;
	}

	public bool Ma { get; set; } = true;

	[JsonIgnore]
	public bool PopPodMustBeAlpha
	{
		get => Ma;
		set => Ma = value;
	}

	public int Pl { get; set; }

	[JsonIgnore]
	public int PopPodMinLength
	{
		get => Pl;
		set => Pl = value;
	}


	public bool Dw { get; set; }

	[JsonIgnore]
	public bool DecimalWeight
	{
		get => Dw;
		set => Dw = value;
	}


	public bool Pp { get; set; }

	[JsonIgnore]
	public bool RequirePop
	{
		get => Pp;
		set => Pp = value;
	}

	public bool Ps { get; set; }

	[JsonIgnore]
	public bool RequirePopSignature
	{
		get => Ps;
		set => Ps = value;
	}

	public bool Rp { get; set; }

	[JsonIgnore]
	public bool RequirePopPicture
	{
		get => Rp;
		set => Rp = value;
	}

	public bool Dp { get; set; }

	[JsonIgnore]
	public bool RequirePod
	{
		get => Dp;
		set => Dp = value;
	}

	public bool Ds { get; set; }

	[JsonIgnore]
	public bool RequirePodSignature
	{
		get => Ds;
		set => Ds = value;
	}

	public bool Rpp { get; set; }

	[JsonIgnore]
	public bool RequirePodPicture
	{
		get => Rpp;
		set => Rpp = value;
	}

	public bool Nt { get; set; }

	[JsonIgnore]
	public bool CreateNewTripForPieces
	{
		get => Nt;
		set => Nt = value;
	}

	public bool Pw { get; set; }

	[JsonIgnore]
	public bool EditPiecesWeight
	{
		get => Pw;
		set => Pw = value;
	}

	public bool Er { get; set; }

	[JsonIgnore]
	public bool EditReference
	{
		get => Er;
		set => Er = value;
	}

	public bool L { get; set; }

	[JsonIgnore]
	public bool ByLocation
	{
		get => L;
		set => L = value;
	}

	public bool P { get; set; }

	[JsonIgnore]
	public bool ByPiece
	{
		get => P;
		set => P = value;
	}

	public bool M { get; set; }

	[JsonIgnore]
	public bool AllowManualBarcodeInput
	{
		get => M;
		set => M = value;
	}

	public bool U { get; set; }

	[JsonIgnore]
	public bool AllowUndeliverable
	{
		get => U;
		set => U = value;
	}

	public bool B { get; set; }

	[JsonIgnore]
	public bool UndeliverableBackToDispatch
	{
		get => B;
		set => B = value;
	}

	public bool D { get; set; }

	[JsonIgnore]
	public bool DeleteCannotPickup
	{
		get => D;
		set => D = value;
	}

	public bool Ao { get; set; }

	[JsonIgnore]
	public bool AllowOverScans
	{
		get => Ao;
		set => Ao = value;
	}

	public bool Au { get; set; }

	[JsonIgnore]
	public bool AllowUnderScans
	{
		get => Au;
		set => Au = value;
	}

	public bool Dy { get; set; }

	[JsonIgnore]
	public bool DynamsoftBarcodeReader
	{
		get => Dy;
		set => Dy = value;
	}

	public bool Ep { get; set; }

	[JsonIgnore]
	public bool EnablePickupGeoFence
	{
		get => Ep;
		set => Ep = value;
	}

	public bool Ed { get; set; }

	[JsonIgnore]
	public bool EnableDeliveryGeoFence
	{
		get => Ed;
		set => Ed = value;
	}

	public int R { get; set; }

	[JsonIgnore]
	public int DeliveryGeoFenceRadius
	{
		get => R;
		set => R = value;
	}

	public string Dd { get; set; } = "";

	[JsonIgnore]
	public string DeviceDisclaimer
	{
		get => Dd;
		set => Dd = value;
	}

	public bool S { get; set; }

	[JsonIgnore]
	public bool ScanShipments
	{
		get => S;
		set => S = value;
	}

	public bool V { get; set; }

	[JsonIgnore]
	public bool EnhancedVisuals
	{
		get => V;
		set => V = value;
	}
}

public class ClientPreferences
{
	public bool C { get; set; }

	[JsonIgnore]
	public bool Coral
	{
		get => C;
		set => C = value;
	}

	public bool P { get; set; }

	[JsonIgnore]
	public bool Pml
	{
		get => P;
		set => P = value;
	}
}