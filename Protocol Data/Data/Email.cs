﻿namespace Protocol.Data;

public class Email
{
	public string To   { get; set; } = "";
	public string From { get; set; } = "";

	public List<string> Cc      { get; set; } = [];
	public List<string> Bcc     { get; set; } = [];
	public string       Subject { get; set; } = "";
	public string       Body    { get; set; } = "";
	public string       Html    { get; set; } = "";

	public bool ConvertHtmlToPdf { get; set; } = true;

	public List<Attachment> Attachments { get; set; } = [];

	public class Attachment
	{
		public string Name { get; set; } = "";

		public byte[] Bytes { get; set; } = [];

		public TYPE Type { get; set; } = TYPE.TEXT;

		public enum TYPE
		{
			TEXT,
			PDF,
			CSV
		}
	}
}