﻿namespace Protocol.Data;

public class ProgramData
{
	[JsonProperty]
	public string ProgramName { get; }

	public ProgramData( string programName )
	{
		ProgramName = programName;
	}

	private ProgramData()
	{
		ProgramName = "Not Specified";
	}
}