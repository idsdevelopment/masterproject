﻿namespace Protocol.Data;

public class CustomerLogin
{
	public string A { get; set; } = "";

	[JsonIgnore]
	public string AuthToken
	{
		get => A;
		set => A = value;
	}

	public bool O { get; set; }

	[JsonIgnore]
	public bool Ok
	{
		get => O;
		set => O = value;
	}

	public string C { get; set; } = "";

	[JsonIgnore]
	public string CarrierId
	{
		get => C;
		set => C = value;
	}

	public string Ac { get; set; } = "";

	[JsonIgnore]
	public string AccountId
	{
		get => Ac;
		set => Ac = value;
	}

	public string U { get; set; } = "";

	[JsonIgnore]
	public string UserName
	{
		get => U;
		set => U = value;
	}

	public bool E { get; set; } = true;

	[JsonIgnore]
	public bool Enabled
	{
		get => E;
		set => E = value;
	}
}

public class SignOut
{
	public uint H { get; set; }
	public byte M { get; set; }
	public byte S { get; set; }

	[JsonIgnore]
	public uint Hours
	{
		get => H;
		set => H = value;
	}

	[JsonIgnore]
	public byte Minutes
	{
		get => M;
		set => M = value;
	}

	[JsonIgnore]
	public byte Seconds
	{
		get => S;
		set => S = value;
	}

	[JsonIgnore]
	public TimeSpan Time
	{
		set
		{
			H = (uint)value.Hours;
			M = (byte)value.Minutes;
			S = (byte)value.Seconds;
		}
	}
}