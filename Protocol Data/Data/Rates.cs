﻿namespace Protocol.Data;

public class Rate
{
	public string Fz { get; set; } = "";

	[JsonIgnore]
	public string FromZone
	{
		get => Fz;
		set => Fz = value;
	}


	public string Tz { get; set; } = "";

	[JsonIgnore]
	public string ToZone
	{
		get => Tz;
		set => Tz = value;
	}


	public string S { get; set; } = "";

	[JsonIgnore]
	public string ServiceLevel
	{
		get => S;
		set => S = value;
	}


	public string P { get; set; } = "";

	[JsonIgnore]
	public string PackageType
	{
		get => P;
		set => P = value;
	}


	public string Z { get; set; } = "";

	[JsonIgnore]
	public string Zone
	{
		get => Z;
		set => Z = value;
	}


	public string V { get; set; } = "";

	[JsonIgnore]
	public string VehicleType
	{
		get => V;
		set => V = value;
	}


	public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTime
	{
		get => D;
		set => D = value;
	}


	public decimal H { get; set; }

	[JsonIgnore]
	public decimal Height
	{
		get => H;
		set => H = value;
	}


	public decimal W { get; set; }

	[JsonIgnore]
	public decimal Width
	{
		get => W;
		set => W = value;
	}


	public decimal Dp { get; set; }

	[JsonIgnore]
	public decimal Depth
	{
		get => Dp;
		set => Dp = value;
	}


	public decimal We { get; set; }

	[JsonIgnore]
	public decimal Weight
	{
		get => We;
		set => We = value;
	}
}

public enum RATE_ERROR
{
	UNKNOWN,
	OK,
	INVALID_SERVICE_LEVEL,
	DAY_NOT_VALID,
	TIME_NOT_VALID,
	INVALID_PACKAGE_TYPE,
	NO_RATES,
	INVALID_FROM_ZONE,
	INVALID_TO_ZONE,
	CYCLIC_CUTOFF,
	INVALID_CUTOFF_TYPE
}

public class RateResult
{
	public RATE_ERROR S { get; set; } = RATE_ERROR.UNKNOWN;

	[JsonIgnore]
	public RATE_ERROR Status
	{
		get => S;
		set => S = value;
	}


	public decimal V { get; set; }

	[JsonIgnore]
	public decimal Value
	{
		get => V;
		set => V = value;
	}
}