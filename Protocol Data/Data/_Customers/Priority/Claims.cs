﻿namespace Protocol.Data.Customers.Priority;

public class PickupByBarcode
{
	public string A { get; set; } = "";

	[JsonIgnore]
	public string AccountId
	{
		get => A;
		set => A = value;
	}


	public string D { get; set; } = "";

	[JsonIgnore]
	public string Driver
	{
		get => D;
		set => D = value;
	}


	public string S { get; set; } = "";

	[JsonIgnore]
	public string ServiceLevel
	{
		get => S;
		set => S = value;
	}


	public string R { get; set; } = "";

	[JsonIgnore]
	public string Route
	{
		get => R;
		set => R = value;
	}


	public List<string> L { get; set; } = [];

	[JsonIgnore]
	public List<string> TripIds
	{
		get => L;
		set => L = value;
	}


	public string P { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => P;
		set => P = value;
	}
}