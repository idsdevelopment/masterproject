﻿namespace Protocol.Data._Customers.Pml;

public class VerifySatchel
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}


	public string U { get; set; } = "";

	[JsonIgnore]
	public string UserName
	{
		get => U;
		set => U = value;
	}


	public string T { get; set; } = "";

	[JsonIgnore]
	public string TripId
	{
		get => T;
		set => T = value;
	}


	public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTime
	{
		get => D;
		set => D = value;
	}


	public double La { get; set; }

	[JsonIgnore]
	public double Latitude
	{
		get => La;
		set => La = value;
	}


	public double Lo { get; set; }

	[JsonIgnore]
	public double Longitude
	{
		get => Lo;
		set => Lo = value;
	}
}

public class Satchel
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}


	public string S { get; set; } = "";

	[JsonIgnore]
	public string SatchelId
	{
		get => S;
		set => S = value;
	}


	public string D { get; set; } = "";

	[JsonIgnore]
	public string Driver
	{
		get => D;
		set => D = value;
	}


	public List<string> T { get; set; } = [];

	[JsonIgnore]
	public List<string> TripIds
	{
		get => T;
		set => T = value;
	}


	public List<TripPackage> Pa { get; set; } = [];

	[JsonIgnore]
	public List<TripPackage> Packages
	{
		get => Pa;
		set => Pa = value;
	}


	public DateTimeOffset Pu { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset PickupTime
	{
		get => Pu;
		set => Pu = value;
	}


	public double PLa { get; set; }

	[JsonIgnore]
	public double PickupLatitude
	{
		get => PLa;
		set => PLa = value;
	}


	public double Plo { get; set; }

	[JsonIgnore]
	public double PickupLongitude
	{
		get => Plo;
		set => Plo = value;
	}


	public string Po { get; set; } = "";

	[JsonIgnore]

	// ReSharper disable once InconsistentNaming
	public string POP
	{
		get => Po;
		set => Po = value;
	}


	public Signature Si { get; set; } = new();

	[JsonIgnore]
	public Signature Signature
	{
		get => Si;
		set => Si = value;
	}
}