﻿namespace Protocol.Data._Customers.Pml;

public class PmlRoute
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string PostalCode
	{
		get => P;
		set => P = value;
	}


	public string O { get; set; } = "";

	[JsonIgnore]
	public string Operation
	{
		get => O;
		set => O = value;
	}


	public string R { get; set; } = "";

	[JsonIgnore]
	public string Region
	{
		get => R;
		set => R = value;
	}


	public string I { get; set; } = "";

	[JsonIgnore]
	public string IdsRoute
	{
		get => I;
		set => I = value;
	}
}

public class PmlRoutes : List<PmlRoute>
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => P;
		set => P = value;
	}
}

public class PmlDeleteRoutes : List<string>
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => P;
		set => P = value;
	}
}

public class PmlDeletePostCodes : List<string>
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => P;
		set => P = value;
	}
}