﻿// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace Protocol.Data;

public class ServiceLevelList : List<string>;

public class ServiceLevelListDetailed : List<ServiceLevel>;

public class ServiceLevelColour
{
	public string S { get; set; } = "";

	[JsonIgnore]
	public string ServiceLevel
	{
		get => S;
		set => S = value;
	}


	public uint B { get; set; }

	[JsonIgnore]
	public uint Background_ARGB
	{
		get => B;
		set => B = value;
	}


	public uint F { get; set; }

	[JsonIgnore]
	public uint Foreground_ARGB
	{
		get => F;
		set => F = value;
	}
}

public class ServiceLevelColours : List<ServiceLevelColour>;

public class ServiceLevel
{
	public string O { get; set; } = "";

	[JsonIgnore]
	public string OldName
	{
		get => O;
		set => O = value;
	}


	public string N { get; set; } = "";

	[JsonIgnore]
	public string NewName
	{
		get => N;
		set => N = value;
	}


	public uint Bg { get; set; } = uint.MaxValue;

	[JsonIgnore]
	public uint BackgroundARGB // A == MBS, B == LSB
	{
		get => Bg;
		set => Bg = value;
	}


	public uint Fg { get; set; } = (uint)0xff << 24;

	[JsonIgnore]
	public uint ForegroundARGB // A == MBS, B == LSB
	{
		get => Fg;
		set => Fg = value;
	}


	public bool M { get; set; }

	[JsonIgnore]
	public bool Monday
	{
		get => M;
		set => M = value;
	}


	public bool T { get; set; }

	[JsonIgnore]
	public bool Tuesday
	{
		get => T;
		set => T = value;
	}


	public bool W { get; set; }

	[JsonIgnore]
	public bool Wednesday
	{
		get => W;
		set => W = value;
	}


	public bool Th { get; set; }

	[JsonIgnore]
	public bool Thursday
	{
		get => Th;
		set => Th = value;
	}


	public bool F { get; set; }

	[JsonIgnore]
	public bool Friday
	{
		get => F;
		set => F = value;
	}


	public bool Sa { get; set; }

	[JsonIgnore]
	public bool Saturday
	{
		get => Sa;
		set => Sa = value;
	}


	public bool Su { get; set; }

	[JsonIgnore]
	public bool Sunday
	{
		get => Su;
		set => Su = value;
	}


	public TimeSpan St { get; set; } = Time.MinDayTimeSpan;

	[JsonIgnore]
	public TimeSpan StartTime
	{
		get => St;
		set => St = value;
	}


	public TimeSpan Et { get; set; } = Time.MaxDayTimeSpan;

	[JsonIgnore]
	public TimeSpan EndTime
	{
		get => Et;
		set => Et = value;
	}


	public TimeSpan Dh { get; set; } = Time.MaxDayTimeSpan;

	[JsonIgnore]
	public TimeSpan DeliveryHours
	{
		get => Dh;
		set => Dh = value;
	}


	public TimeSpan Wh { get; set; } = Time.MaxDayTimeSpan;

	[JsonIgnore]
	public TimeSpan WarningHours
	{
		get => Wh;
		set => Wh = value;
	}


	public TimeSpan Ch { get; set; } = Time.MaxDayTimeSpan;

	[JsonIgnore]
	public TimeSpan CriticalHours
	{
		get => Ch;
		set => Ch = value;
	}


	public TimeSpan Bh { get; set; } = Time.MaxDayTimeSpan;

	[JsonIgnore]
	public TimeSpan BeyondHours
	{
		get => Bh;
		set => Bh = value;
	}


	public List<string> Pt { get; set; } = [];

	[JsonIgnore]
	public List<string> PackageTypes
	{
		get => Pt;
		set => Pt = value;
	}


	public ushort So { get; set; }

	[JsonIgnore]
	public ushort SortOrder
	{
		get => So;
		set => So = value;
	}

	public int Id { get; set; }

	[JsonIgnore]
	public int ServiceLevelId
	{
		get => Id;
		set => Id = value;
	}
}

public class ServiceLevelResult
{
	public bool K { get; set; }

	[JsonIgnore]
	public bool Ok
	{
		get => K;
		set => K = value;
	}


	public ServiceLevel S { get; set; } = new();

	[JsonIgnore]
	public ServiceLevel ServiceLevel
	{
		get => S;
		set => S = value;
	}
}