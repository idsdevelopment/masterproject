﻿namespace Protocol.Data;

public static class Extensions
{
	public static bool IsEmpty( this Address address ) => string.IsNullOrEmpty( address.AddressLine1.Trim() ) && string.IsNullOrEmpty( address.AddressLine2.Trim() );
}

public class AddressValidation : AddressBase
{
	public string C1 { get; set; } = "";

	[JsonIgnore]
	public string CompanyName
	{
		get => C1;
		set => C1 = value;
	}

	public AddressValidation()
	{
	}

	public AddressValidation( AddressValidation a ) : base( a )
	{
		CompanyName = a.CompanyName;
	}
}

public class AddressValidationList : List<AddressValidation>
{
	public AddressValidationList()
	{
	}

	public AddressValidationList( IEnumerable<AddressValidation> addresses )
	{
		AddRange( addresses );
	}
}

public class AddressValidList : List<AddressValid>
{
	public AddressValidList()
	{
	}

	public AddressValidList( IEnumerable<AddressValidation> l )
	{
		AddRange( from A in l
		          select new AddressValid( A ) );
	}
}

public class AddressValid : AddressValidation
{
	public bool Va { get; set; }

	[JsonIgnore]
	public bool IsValid
	{
		get => Va;
		set => Va = value;
	}

	public AddressValid()
	{
	}

	public AddressValid( AddressValid a ) : base( a )
	{
		IsValid = a.IsValid;
	}

	public AddressValid( AddressValidation a ) : base( a )
	{
	}
}

public class AddressBase
{
	public decimal Lt { get; set; }

	[JsonIgnore]
	public decimal Latitude
	{
		get => Lt;
		set => Lt = value;
	}

	public decimal Lg { get; set; }

	[JsonIgnore]
	public decimal Longitude
	{
		get => Lg;
		set => Lg = value;
	}

	public string S { get; set; } = "";

	[JsonIgnore]
	public string Suite
	{
		get => S;
		set => S = value;
	}

	public string A1 { get; set; } = "";

	[JsonIgnore]
	public string AddressLine1
	{
		get => A1;
		set => A1 = value;
	}

	public string A2 { get; set; } = "";

	[JsonIgnore]
	public string AddressLine2
	{
		get => A2;
		set => A2 = value;
	}

	public string V { get; set; } = "";

	[JsonIgnore]
	public string Vicinity
	{
		get => V;
		set => V = value;
	}

	public string C { get; set; } = "";

	[JsonIgnore]
	public string City
	{
		get => C;
		set => C = value;
	}

	public string R { get; set; } = "";

	[JsonIgnore]
	public string Region
	{
		get => R;
		set => R = value;
	}


	public string Rc { get; set; } = "";

	[JsonIgnore]
	public string RegionCode
	{
		get => Rc;
		set => Rc = value;
	}


	public string Pc { get; set; } = "";

	[JsonIgnore]
	public string PostalCode
	{
		get => Pc;
		set => Pc = value;
	}

	public string Co { get; set; } = "";

	[JsonIgnore]
	public string Country
	{
		get => Co;
		set => Co = value;
	}

	public string Cc { get; set; } = "";

	[JsonIgnore]
	public string CountryCode
	{
		get => Cc;
		set => Cc = value;
	}

	public AddressBase()
	{
	}

	public AddressBase( AddressBase a )
	{
		Suite        = a.Suite;
		AddressLine1 = a.AddressLine1;
		AddressLine2 = a.AddressLine2;
		Vicinity     = a.Vicinity;
		City         = a.City;
		Region       = a.Region;
		PostalCode   = a.PostalCode;
		Country      = a.Country;
		CountryCode  = a.CountryCode;
		Longitude    = a.Longitude;
		Longitude    = a.Longitude;
		RegionCode   = a.RegionCode;
		Region       = a.Region;
	}
}

public class Address : AddressBase
{
	public string Si { get; set; } = "";

	[JsonIgnore]
	public string SecondaryId
	{
		get => Si;
		set => Si = value;
	}

	public string B { get; set; } = "";

	[JsonIgnore]
	public string Barcode
	{
		get => B;
		set => B = value;
	}

	public string Pb { get; set; } = "";

	[JsonIgnore]
	public string PostalBarcode
	{
		get => Pb;
		set => Pb = value;
	}

	public string P { get; set; } = "";

	[JsonIgnore]
	public string Phone
	{
		get => P;
		set => P = value;
	}

	public string P1 { get; set; } = "";

	[JsonIgnore]
	public string Phone1
	{
		get => P1;
		set => P1 = value;
	}

	public string F { get; set; } = "";

	[JsonIgnore]
	public string Fax
	{
		get => F;
		set => F = value;
	}

	public string M { get; set; } = "";

	[JsonIgnore]
	public string Mobile
	{
		get => M;
		set => M = value;
	}

	public string M1 { get; set; } = "";

	[JsonIgnore]
	public string Mobile1
	{
		get => M1;
		set => M1 = value;
	}

	public string Cn { get; set; } = "";

	[JsonIgnore]
	public string ContactName
	{
		get => Cn;
		set => Cn = value;
	}

	public string E { get; set; } = "";

	[JsonIgnore]
	public string EmailAddress
	{
		get => E;
		set => E = value;
	}

	public string E1 { get; set; } = "";

	/// <summary>
	///     Remapped to InvoiceEmailAddresses
	/// </summary>
	[JsonIgnore]
	public string EmailAddress1
	{
		get => E1;
		set => E1 = value;
	}

	[JsonIgnore]
	public string InvoiceEmailAddresses
	{
		get => EmailAddress1;
		set => EmailAddress1 = value;
	}

	public string E2 { get; set; } = "";

	[JsonIgnore]
	public string EmailAddress2
	{
		get => E2;
		set => E2 = value;
	}

	public string N { get; set; } = "";

	[JsonIgnore]
	public string Notes
	{
		get => N;
		set => N = value;
	}

	public string Z { get; set; } = "";

	[JsonIgnore]
	public string ZoneName
	{
		get => Z;
		set => Z = value;
	}

	public Address()
	{
	}

	public Address( Address a ) : base( a )
	{
		SecondaryId   = a.SecondaryId;
		Barcode       = a.Barcode;
		PostalBarcode = a.PostalBarcode;
		Phone         = a.Phone;
		Phone1        = a.Phone1;
		Fax           = a.Fax;
		Mobile        = a.Mobile;
		Mobile1       = a.Mobile1;
		EmailAddress  = a.EmailAddress;
		EmailAddress1 = a.EmailAddress1;
		EmailAddress2 = a.EmailAddress2;
		Notes         = a.Notes;
		Latitude      = a.Latitude;
		Longitude     = a.Longitude;
		ZoneName      = a.ZoneName;
		Region        = a.Region;
		RegionCode    = a.RegionCode;
		Country       = a.Country;
		CountryCode   = a.CountryCode;
	}
}

public class Addresses : List<Address>
{
	public Addresses()
	{
	}

	public Addresses( IEnumerable<Address> addresses )
	{
		AddRange( addresses );
	}
}

public class AddressIdList : List<string>
{
	public AddressIdList()
	{
	}

	public AddressIdList( IEnumerable<string> list )
	{
		AddRange( list );
	}
}