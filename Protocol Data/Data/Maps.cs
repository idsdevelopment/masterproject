﻿namespace Protocol.Data
{
	namespace Maps.Route
	{
		public class Point
		{
			public decimal La { get; set; }

			[JsonIgnore]
			public decimal Latitude
			{
				get => La;
				set => La = value;
			}


			public decimal Lo { get; set; }

			[JsonIgnore]
			public decimal Longitude
			{
				get => Lo;
				set => Lo = value;
			}
		}


		public class Step
		{
			public decimal Di { get; set; }

			[JsonIgnore]
			public decimal Distance
			{
				get => Di;
				set => Di = value;
			}


			public decimal Du { get; set; }

			[JsonIgnore]
			public decimal Duration
			{
				get => Du;
				set => Du = value;
			}


			public Point S { get; set; } = new();

			[JsonIgnore]
			public Point StartLocation
			{
				get => S;
				set => S = value;
			}


			public Point E { get; set; } = new();

			[JsonIgnore]
			public Point EndLocation
			{
				get => E;
				set => E = value;
			}


			public string I { get; set; } = "";

			[JsonIgnore]
			public string HtmlInstructions
			{
				get => I;
				set => I = value;
			}
		}

		public class Leg : Step
		{
			public List<Step> St { get; set; } = [];

			[JsonIgnore]
			public List<Step> Steps
			{
				get => St;
				set => St = value;
			}


			public int O { get; set; }

			[JsonIgnore]
			public int Order
			{
				get => O;
				set => O = value;
			}
		}

		public class Route : List<Leg>;
	}


	public class DistanceAndAddress
	{
		public Company F { get; set; } = new();

		[JsonIgnore]
		public Company FromCompany
		{
			get => F;
			set => F = value;
		}


		public Company T { get; set; } = new();

		[JsonIgnore]
		public Company ToCompany
		{
			get => T;
			set => T = value;
		}


		public decimal D { get; set; }

		[JsonIgnore]
		public decimal DistanceInKilometres
		{
			get => D;
			set => D = value;
		}


		public Maps.Route.Route R { get; set; } = [];

		[JsonIgnore]
		public Maps.Route.Route Route
		{
			get => R;
			set => R = value;
		}
	}

	public class DistancesAndAddresses : List<DistanceAndAddress>;


	public class Distances : List<double>
	{
		[JsonIgnore]
		public List<double> AsKilometres => this.Select( distance => distance ).ToList();

		[JsonIgnore]
		public List<double> AsMiles
		{
			get { return this.Select( distance => distance * 0.621371 ).ToList(); }
		}
	}

	public class MapApiKeys
	{
		public string B { get; set; } = "";

		[JsonIgnore]
		public string BingMapsKey
		{
			get => B;
			set => B = value;
		}


		public string G { get; set; } = "";

		[JsonIgnore]
		public string GoogleMapsKey
		{
			get => G;
			set => G = value;
		}


		public string A { get; set; } = "";

		[JsonIgnore]
		public string AzureMapKey
		{
			get => A;
			set => A = value;
		}
	}
}