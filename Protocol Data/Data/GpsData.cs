﻿namespace Protocol.Data;

public class GpsPoint
{
	public double La { get; set; }

	[JsonIgnore]
	public double Latitude
	{
		get => La;
		set => La = value;
	}


	public double Lo { get; set; }

	[JsonIgnore]
	public double Longitude
	{
		get => Lo;
		set => Lo = value;
	}


	public DateTimeOffset L { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset LocalDateTime
	{
		get => L;
		set => L = value;
	}
}

public class GpsPoints : List<GpsPoint>
{
	public GpsPoints()
	{
	}

	public GpsPoints( IEnumerable<GpsPoint> collection ) : base( collection )
	{
	}
}

public class GpsDriverDateRange
{
	public string D { get; set; } = "";

	[JsonIgnore]
	public string Driver
	{
		get => D;
		set => D = value;
	}


	public DateTimeOffset F { get; set; } = DateTimeOffset.Now;

	[JsonIgnore]
	public DateTimeOffset FromDateTime
	{
		get => F;
		set => F = value;
	}


	public DateTimeOffset T { get; set; } = DateTimeOffset.Now;

	[JsonIgnore]
	public DateTimeOffset ToDateTime
	{
		get => T;
		set => T = value;
	}
}