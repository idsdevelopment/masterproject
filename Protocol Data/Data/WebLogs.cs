﻿namespace Protocol.Data;

public class WebLog
{
	public string B { get; set; } = "";

	[JsonIgnore]
	public string BasePath
	{
		get => B;
		set => B = value;
	}


	public string C { get; set; } = "";

	[JsonIgnore]
	public string CarrierId
	{
		get => C;
		set => C = value;
	}


	public string P { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => P;
		set => P = value;
	}


	public string M { get; set; } = "";

	[JsonIgnore]
	public string Message
	{
		get => M;
		set => M = value;
	}


	public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset LogDateTime
	{
		get => D;
		set => D = value;
	}
}

public class WebLogList : List<string>;

public class WebLogCarrierList : List<string>;

public class WebLogSearch
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CarrierId
	{
		get => C;
		set => C = value;
	}


	public string Lt { get; set; } = "";

	[JsonIgnore]
	public string LogType
	{
		get => Lt;
		set => Lt = value;
	}


	public DateTimeOffset F { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset FromDateTime
	{
		get => F;
		set => F = value;
	}


	public DateTimeOffset T { get; set; } = DateTimeOffset.MaxValue;

	[JsonIgnore]
	public DateTimeOffset ToDateTime
	{
		get => T;
		set => T = value;
	}


	public string S { get; set; } = "";

	[JsonIgnore]
	public string SearchString
	{
		get => S;
		set => S = value;
	}
}