﻿namespace Protocol.Data;

public class AccountToLocationBarcode
{
	public Dictionary<string, string> Dict { get; set; } = new();

	public bool P { get; set; }

	[JsonIgnore]
	public bool Purge
	{
		get => P;
		set => P = value;
	}

	[JsonIgnore]
	public string this[ string key ]
	{
		get => Dict[ key ];
		set => Dict[ key ] = value;
	}
}

public class CompanyNameList : List<string>;

public class CompanyBase : Address
{
	public long I { get; set; }

	[JsonIgnore]
	public long CompanyId
	{
		get => I;
		set => I = value;
	}

	public string Cn1 { get; set; } = "";

	[JsonIgnore]
	public string CompanyName
	{
		get => Cn1;
		set => Cn1 = value;
	}

	public string L { get; set; } = "";

	[JsonIgnore]
	public string LocationBarcode
	{
		get => L;
		set => L = value;
	}

	public bool En { get; set; } = true;

	[JsonIgnore]
	public bool Enabled
	{
		get => En;
		set => En = value;
	}

	public string Bt { get; set; } = "";

	[JsonIgnore]
	public string BillToCompany
	{
		get => Bt;
		set => Bt = value;
	}

	public short Bp { get; set; }

	[JsonIgnore]
	public short BillingPeriod
	{
		get => Bp;
		set => Bp = value;
	}

	public decimal Cl { get; set; }

	[JsonIgnore]
	public decimal CreditLimit
	{
		get => Cl;
		set => Cl = value;
	}

	public bool Em { get; set; }

	[JsonIgnore]
	public bool EmailInvoice
	{
		get => Em;
		set => Em = value;
	}

	public string Io { get; set; } = "";

	[JsonIgnore]
	public string InvoiceOverride
	{
		get => Io;
		set => Io = value;
	}

	public bool Mt { get; set; } = true;

	[JsonIgnore]
	public bool MultipleTripsOnInvoice
	{
		get => Mt;
		set => Mt = value;
	}

	public ADDRESS_TYPE A { get; set; } = ADDRESS_TYPE.PICKUP;

	[JsonIgnore]
	public bool IsPickupCompany
	{
		get => ( A & ADDRESS_TYPE.PICKUP ) != 0;
		set
		{
			if( value )
				A |= ADDRESS_TYPE.PICKUP;
			else
				A &= ~ADDRESS_TYPE.PICKUP;
		}
	}

	[JsonIgnore]
	public bool IsDeliveryCompany
	{
		get => ( A & ADDRESS_TYPE.DELIVERY ) != 0;
		set
		{
			if( value )
				A |= ADDRESS_TYPE.DELIVERY;
			else
				A &= ~ADDRESS_TYPE.DELIVERY;
		}
	}


	public CompanyBase()
	{
	}

	public CompanyBase( Address address ) : base( address )
	{
	}

	[Flags]
	public enum ADDRESS_TYPE
	{
		UNSPECIFIED         = 0,
		PICKUP              = 1,
		DELIVERY            = 1 << 1,
		PICKUP_AND_DELIVERY = PICKUP | DELIVERY
	}
}

public class Company : CompanyBase
{
	public string Cu { get; set; } = "";

	[JsonIgnore]
	public string CompanyNumber
	{
		get => Cu;
		set => Cu = value;
	}

	public string U { get; set; } = "";

	[JsonIgnore]
	public string UserName
	{
		get => U;
		set => U = value;
	}

	public string Ps { get; set; } = "";

	[JsonIgnore]
	public string Password
	{
		get => Ps;
		set => Ps = value;
	}


	public Company()
	{
	}

	public Company( Address address ) : base( address )
	{
	}
}

public class CompanyUpdate : Company
{
	public string Pg { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => Pg;
		set => Pg = value;
	}
}

public class CompanyAddress : CompanyBase
{
	public string CompanyNumber { get; set; } = "";
}

public class Companies : List<Company>
{
	public Companies()
	{
	}

	public Companies( IEnumerable<Company> companies )
	{
		AddRange( companies );
	}
}

public class CompanyByAccountAndCompanyName
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public string Cn { get; set; } = "";

	[JsonIgnore]
	public string CompanyName
	{
		get => Cn;
		set => Cn = value;
	}

	public long Ci { get; set; } = -1;

	[JsonIgnore]
	public long CompanyId
	{
		get => Ci;
		set => Ci = value;
	}

	public string D { get; set; } = "";

	[JsonIgnore]
	public string DisplayCompanyName
	{
		get => D;
		set => D = value;
	}

	public string L { get; set; } = "";

	[JsonIgnore]
	public string LocationBarcode
	{
		get => L;
		set => L = value;
	}

	public CompanyByAccountAndCompanyName()
	{
	}

	public CompanyByAccountAndCompanyName( CompanyByAccountAndCompanyName c )
	{
		CompanyName     = c.CompanyName;
		CustomerCode    = c.CustomerCode;
		LocationBarcode = c.LocationBarcode;
	}
}

public class CompanyByAccountAndCompanyNameList : List<CompanyByAccountAndCompanyName>
{
	public CompanyByAccountAndCompanyNameList()
	{
	}

	public CompanyByAccountAndCompanyNameList( IEnumerable<CompanyByAccountAndCompanyName> list )
	{
		AddRange( list );
	}
}

public class CompanyByAccountSummary : CompanyByAccountAndCompanyName
{
	// Used by customer lookup
	public bool F { get; set; }

	[JsonIgnore]
	public bool Found
	{
		get => F;
		set => F = value;
	}

	public bool E { get; set; } = true;

	[JsonIgnore]
	public bool Enabled
	{
		get => E;
		set => E = value;
	}

	public string S { get; set; } = "";

	[JsonIgnore]
	public string Suite
	{
		get => S;
		set => S = value;
	}

	public string A1 { get; set; } = "";

	[JsonIgnore]
	public string AddressLine1
	{
		get => A1;
		set => A1 = value;
	}

	public string A2 { get; set; } = "";

	[JsonIgnore]
	public string AddressLine2
	{
		get => A2;
		set => A2 = value;
	}

	public string C1 { get; set; } = "";

	[JsonIgnore]
	public string City
	{
		get => C1;
		set => C1 = value;
	}

	public string R { get; set; } = "";

	[JsonIgnore]
	public string Region
	{
		get => R;
		set => R = value;
	}

	public string P { get; set; } = "";

	[JsonIgnore]
	public string PostalCode
	{
		get => P;
		set => P = value;
	}

	public string Cc { get; set; } = "";

	[JsonIgnore]
	public string CountryCode
	{
		get => Cc;
		set => Cc = value;
	}

	public CompanyByAccountSummary()
	{
	}

	public CompanyByAccountSummary( CompanyByAccountSummary c ) : base( c )
	{
		Found        = c.Found;
		Suite        = c.Suite;
		AddressLine1 = c.AddressLine1;
		AddressLine2 = c.AddressLine2;
		City         = c.City;
		Region       = c.Region;
		PostalCode   = c.PostalCode;
		CountryCode  = c.CountryCode;
	}
}

public class CompanySummaryList : List<CompanyByAccountSummary>;

public class CompanyDetail
{
	public Company C { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => C;
		set => C = value;
	}

	public CompanyAddress A { get; set; } = new();

	[JsonIgnore]
	public CompanyAddress Address
	{
		get => A;
		set => A = value;
	}
}

public class CompanyDetailList : List<CompanyDetail>;

public class AddUpdatePrimaryCompany : ProgramData
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public string S { get; set; } = "";

	[JsonIgnore]
	public string SuggestedLoginCode
	{
		get => S;
		set => S = value;
	}

	public string U { get; set; } = "";

	[JsonIgnore]
	public string UserName
	{
		get => U;
		set => U = value;
	}

	public string P { get; set; } = "";

	[JsonIgnore]
	public string Password
	{
		get => P;
		set => P = value;
	}

	public Company Co { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => Co;
		set => Co = value;
	}

	public Company Bc { get; set; } = new();

	[JsonIgnore]
	public Company BillingCompany
	{
		get => Bc;
		set => Bc = value;
	}

	public Company Sc { get; set; } = new();

	[JsonIgnore]
	public Company ShippingCompany
	{
		get => Sc;
		set => Sc = value;
	}

	public bool E { get; set; }

	[JsonIgnore]
	public bool LoginEnabled
	{
		get => E;
		set => E = value;
	}

	public AddUpdatePrimaryCompany( string programName ) : base( programName )
	{
	}
}

public class PrimaryCompany
{
	public bool O { get; set; }

	[JsonIgnore]
	public bool Ok // Company Was Found
	{
		get => O;
		set => O = value;
	}

	[JsonIgnore]
	public bool Found // Company Was Found
	{
		get => O;
		set => O = value;
	}

	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public string S { get; set; } = "";

	[JsonIgnore]
	public string LoginCode
	{
		get => S;
		set => S = value;
	}

	public string U { get; set; } = "";

	[JsonIgnore]
	public string UserName
	{
		get => U;
		set => U = value;
	}

	public string P { get; set; } = "";

	[JsonIgnore]
	public string Password
	{
		get => P;
		set => P = value;
	}

	public bool E { get; set; }

	[JsonIgnore]
	public bool LoginEnabled
	{
		get => E;
		set => E = value;
	}

	public Company Co { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => Co;
		set => Co = value;
	}

	public Company Bc { get; set; } = new();

	[JsonIgnore]
	public Company BillingCompany
	{
		get => Bc;
		set => Bc = value;
	}

	public Company Sc { get; set; } = new();

	[JsonIgnore]
	public Company ShippingCompany
	{
		get => Sc;
		set => Sc = value;
	}
}