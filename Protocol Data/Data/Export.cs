﻿namespace Protocol.Data;

public class ExportArgs
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string ExportName
	{
		get => N;
		set => N = value;
	}


	public string S1 { get; set; } = "";

	[JsonIgnore]
	public string StringArg1
	{
		get => S1;
		set => S1 = value;
	}
}