﻿namespace Protocol.Data;

public class PostcodesToZonesBase
{
	public string R { get; set; } = "";

	[JsonIgnore]
	public string RouteName
	{
		get => R;
		set => R = value;
	}


	public string P { get; set; } = "";

	[JsonIgnore]
	public string PostalCode
	{
		get => P;
		set => P = value;
	}


	public string Z1 { get; set; } = "";

	[JsonIgnore]
	public string Zone1
	{
		get => Z1;
		set => Z1 = value;
	}


	public string Z2 { get; set; } = "";

	[JsonIgnore]
	public string Zone2
	{
		get => Z2;
		set => Z2 = value;
	}


	public string Z3 { get; set; } = "";

	[JsonIgnore]
	public string Zone3
	{
		get => Z3;
		set => Z3 = value;
	}


	public string Z4 { get; set; } = "";

	[JsonIgnore]
	public string Zone4
	{
		get => Z4;
		set => Z4 = value;
	}
}

public class PostcodesToZones : PostcodesToZonesBase
{
	public string Pr { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => Pr;
		set => Pr = value;
	}


	public string A1 { get; set; } = "";

	[JsonIgnore]
	public string AbbreviationZone1
	{
		get => A1;
		set => A1 = value;
	}

	public string A2 { get; set; } = "";

	[JsonIgnore]
	public string AbbreviationZone2
	{
		get => A2;
		set => A2 = value;
	}

	public string A3 { get; set; } = "";

	[JsonIgnore]
	public string AbbreviationZone3
	{
		get => A3;
		set => A3 = value;
	}

	public string A4 { get; set; } = "";

	[JsonIgnore]
	public string AbbreviationZone4
	{
		get => A4;
		set => A4 = value;
	}
}

public class PostcodesToZonesList : List<PostcodesToZonesBase>;