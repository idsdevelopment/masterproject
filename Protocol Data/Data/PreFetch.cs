﻿namespace Protocol.Data;

public class PreFetch
{
	public PREFETCH Fetch         { get; set; } = PREFETCH.FIRST;
	public int      PreFetchCount { get; set; }
	public int      Offset        { get; set; }
	public string   Key           { get; set; } = "";

	public enum PREFETCH
	{
		FIRST,
		LAST,
		FIND_RANGE,
		FIND_MATCH,
		OFFSET
	}
}