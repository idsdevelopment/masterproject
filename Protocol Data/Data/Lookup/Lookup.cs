﻿namespace Protocol.Data;

public class RecordLookupType
{
	public RECORD_TYPE R { get; set; } = RECORD_TYPE.COMPANY;

	[JsonIgnore]
	public RECORD_TYPE RecordType
	{
		get => R;
		set => R = value;
	}

	public int C { get; set; } = -1;

	[JsonIgnore]
	public int Count
	{
		get => C;
		set => C = value;
	}

	public enum RECORD_TYPE
	{
		COMPANY
	}
}

public class RecordLookup : RecordLookupType
{
	public int O { get; set; } = -1;

	[JsonIgnore]
	public int Offset
	{
		get => O;
		set => O = value;
	}
}

public class RecordLookupResponse : RecordLookupType
{
	public List<Company> Co { get; set; } = [];

	[JsonIgnore]
	public List<Company> CompanyList
	{
		get => Co;
		set => Co = value;
	}
}