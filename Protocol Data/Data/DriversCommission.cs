﻿namespace Protocol.Data;

public class Commission
{
	public decimal D { get; set; }

	[JsonIgnore]
	public decimal DistanceInMetres
	{
		get => D;
		set => D = value;
	}
}

public class Shift
{
}

public class DriversCommission : Dictionary<string, List<Shift>>
{
}

public class DriverCommissionStartDate
{
	public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset StartDate
	{
		get => D;
		set => D = value;
	}
}